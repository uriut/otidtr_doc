TRUNCATE TABLE MB_TYPESTOCK;

INSERT INTO MB_TYPESTOCK (TYPESTOCK_CD, TYPESTOCK_TX) VALUES('10', '良品');
INSERT INTO MB_TYPESTOCK (TYPESTOCK_CD, TYPESTOCK_TX) VALUES('20', '不良品');
INSERT INTO MB_TYPESTOCK (TYPESTOCK_CD, TYPESTOCK_TX) VALUES('90', 'その他');

COMMIT;
