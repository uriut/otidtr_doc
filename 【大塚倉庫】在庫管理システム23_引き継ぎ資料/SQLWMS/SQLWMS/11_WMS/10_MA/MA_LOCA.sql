-- Copyright (C) Seaos Corporation 2014 All rights reserved
--************************************************
-- ロケーションマスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------

--TYPELOCA_CD 10:通常棚,15:店頭販売,20:破損棚,25:処分予定,40:使用期限切れ棚,50:取り置き棚,70:返品予定棚,90:仮置き棚

DROP TABLE MA_LOCA CASCADE CONSTRAINTS;
CREATE TABLE MA_LOCA
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	LOCA_CD						VARCHAR2(20)								NOT NULL,			-- ロケーション
	LOCA_TX						VARCHAR2(50)								NOT NULL,			-- ロケーション名　WH_CD + FLOOR_CD + AREA_CD + LINE_CD + SHELF_CD + ORDER_CD(01固定)
	WH_CD						VARCHAR(2)									NOT NULL,			-- 倉庫コード　当面は浦安で固定
	FLOOR_CD					VARCHAR(2)									NOT NULL,			-- フロアコード　当面は3Fで固定
	AREA_CD						VARCHAR(3)									NOT NULL,			-- エリアコード
	LINE_CD						VARCHAR(3)									NOT NULL,			-- 列コード
	SHELF_CD					VARCHAR(3)									NOT NULL,			-- 棚コード
	LAYER_CD					VARCHAR(2)									NOT NULL,			-- 段コード
	ORDER_CD					VARCHAR(2)									NOT NULL,			-- 番コード
	TYPELOCA_CD					VARCHAR2(10)		DEFAULT '10'			NOT NULL,			-- ロケーション種別コード
	TYPELOCABLOCK_CD			VARCHAR2(10)		DEFAULT 'C1'			NOT NULL,			-- ロケーションブロックコード　01:ネステナ、02:平置き
	LINEUNIT_CD					VARCHAR(2)			DEFAULT 0				NOT NULL,			-- ラインユニットコード
	LINEUNIT_NR					NUMBER(2)			DEFAULT 0				NOT NULL,			-- ラインユニット
	HEQUIP_CD					VARCHAR2(10)		DEFAULT '00'			NOT NULL,			-- 格納什器コード　パレット固定
	CASE_CD						VARCHAR2(10)		DEFAULT '00'			NOT NULL,			-- 標準ケースコード
	PALLETDIV_NR				NUMBER(3,0)			DEFAULT 0				NOT NULL,			-- パレット分割数
	SHIPVOLUME_CD				VARCHAR2(10)		DEFAULT '00'			NOT NULL,			-- 出荷荷姿コード  ケース成り出荷とパレット出荷のエリアを識別するコード　01:ケース成り、02:パレット成り、03:ネステナ
	ABC_ID						NUMBER(2,0)			DEFAULT 6				NOT NULL,			-- ロケーションＡＢＣＩＤ（0で作成すると格納時に引きあたらないので、6:Fで作成）
	ABCSUB_ID					NUMBER(4,0)			DEFAULT 2				NOT NULL,			-- ロケーションＡＢＣＳＵＢＩＤ （初期値は下段設定　1:上段、2:下段）
	ARRIVPRIORITY_NR			NUMBER(6,0)			DEFAULT 0				NOT NULL,			-- 入庫優先順位
	SHIPPRIORITY_NR				NUMBER(6,0)			DEFAULT 0				NOT NULL,			-- 出庫優先順位
	INVENT_FL					NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 棚卸対象フラグ
	NOTUSE_FL					NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 未使用フラグ（入庫引当で利用）
	NOTSHIP_FL					NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 出荷不可フラグ（出荷引当で利用）
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザコード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザ名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザコード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザ名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MA_LOCA
	ADD CONSTRAINT PK_MA_LOCA
	PRIMARY KEY(LOCA_CD)
;
--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_LOCA_MLC ON MA_LOCA
(
	LOCA_TX
)
;
