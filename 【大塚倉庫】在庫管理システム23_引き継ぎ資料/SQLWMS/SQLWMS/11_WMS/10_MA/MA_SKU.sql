-- Copyright (C) Seaos Corporation 2014 All rights reserved
--************************************************
-- ＳＫＵマスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MA_SKU CASCADE CONSTRAINTS;
CREATE TABLE MA_SKU
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	SKU_CD						VARCHAR2(40)								NOT NULL,			-- 商品コード
	WAREHOUSESKU_CD				VARCHAR2(40)								NOT NULL,			-- 物流コード
	EXTERIOR_CD					VARCHAR2(40)								NOT NULL,			-- 外装コード
	SIZE_TX						VARCHAR2(40)								NOT NULL,			-- 規格容量ｶﾅ・ケース
	SKUBARCODE_TX				VARCHAR2(40)								NOT NULL,			-- JANコード ケース
	SKUBARCODEBOARD_TX			VARCHAR2(40)								NOT NULL,			-- JANコード ボール
	SKUBARCODEBARA_TX			VARCHAR2(40)								NOT NULL,			-- JANコード バラ
	ITFCODE_TX					VARCHAR2(40)								NOT NULL,			-- ITFコード
	SKU_TX						VARCHAR(100)								NOT NULL,			-- 商品名
	SKU2_TX						VARCHAR(100)								NOT NULL,			-- 商品名カナ
	MANUFACTURERSKU_TX			VARCHAR2(40)								NOT NULL,			-- 電略
	OWNER_CD					VARCHAR2(40)								NOT NULL,			-- 荷主コード
	TYPEBILL1_CD				VARCHAR2(40)								NOT NULL,			-- 請求区分
	TYPEBILL2_CD				VARCHAR2(40)								NOT NULL,			-- 請求区分2
	GS1_CD						VARCHAR2(40)		DEFAULT ' '				NOT NULL,			-- GS1コード
	PCSPERCASE_NR				NUMBER(9,0)									NOT NULL,			-- ケース入数
	DEPOISTPRICE_NR				NUMBER(10,2)		DEFAULT 0						,			-- 寄託金額（銭）
	STORAGECHARGEA_NR			NUMBER(10,2)		DEFAULT 0						,			-- 保管量(甲)
	STORAGECHARGEB_NR			NUMBER(10,2)		DEFAULT 0						,			-- 保管量(乙)
	STEVEDORAGEA_NR				NUMBER(10,2)		DEFAULT 0						,			-- 荷役料(甲)
	STEVEDORAGEB_NR				NUMBER(10,2)		DEFAULT 0						,			-- 荷役料(乙)
	NESTAINERINSTOCKRATE_NR		NUMBER(3,2)			DEFAULT 0						,			-- 格納割合-ネステナ	ネステナに格納する割合(格納引き当て時にネステナに割り当てる割合)
	CASESTOCKRATE_NR			NUMBER(3,2)			DEFAULT 0						,			-- 格納割合-ケース成り	ケース成りエリアに格納する割合(格納引き当て時にネステナに割り当てる割合)
	TYPEPACKAGING_CD			VARCHAR2(10)		DEFAULT ''						,			-- 出荷荷姿				ピック引き当て時に荷姿ごとに引き当てがされる
	LENGTH_NR					NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 商品サイズ・縦(mm)
	WIDTH_NR					NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 商品サイズ・横(mm)
	HEIGHT_NR					NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 商品サイズ・高さ(mm)
	WEIGHT_NR					NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 商品サイズ・重量(g)
	ABC_ID						NUMBER(2,0)			DEFAULT 0				NOT NULL,			-- ロケーションＡＢＣＩＤ 
	ABCSUB_ID					NUMBER(4,0)			DEFAULT 0				NOT NULL,			-- ロケーションＡＢＣＳＵＢＩＤ 
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザコード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザ名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザコード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザ名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MA_SKU
	ADD CONSTRAINT PK_MA_SKU
	PRIMARY KEY(SKU_CD)
;
--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_SKUBARCODE_MSKU ON MA_SKU
(
	SKUBARCODE_TX
)
;
CREATE INDEX IX_SKUBARCODEBOARD_MSKU ON MA_SKU
(
	SKUBARCODEBOARD_TX
)
;
CREATE INDEX IX_SKUBARCODEBARA_MSKU ON MA_SKU
(
	SKUBARCODEBARA_TX
)
;
CREATE INDEX IX_ITFCODE_MSKU ON MA_SKU
(
	ITFCODE_TX
)
;

