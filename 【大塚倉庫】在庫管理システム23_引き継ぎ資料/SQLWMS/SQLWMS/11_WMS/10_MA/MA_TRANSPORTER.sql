-- Copyright (C) Seaos Corporation 2014 All rights reserved
--************************************************
-- 配送業者マスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MA_TRANSPORTER CASCADE CONSTRAINTS;
CREATE TABLE MA_TRANSPORTER
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	TRANSPORTER_CD				VARCHAR2(40)								NOT NULL,			-- 配送業者コード　ホストから連携される(6桁)
	TRANSPORTER_TX				VARCHAR2(40)		DEFAULT ' '				NOT NULL,			-- 配送業者名
	TRANSPORTERGROUP_CD			VARCHAR2(40)		DEFAULT ' '				NOT NULL,			-- 配送業者グループコード　出荷データの運送業者CDをピッキング用に集約。Total picのキーとなる
	TRANSPORTERGROUP_TX			VARCHAR2(40)		DEFAULT ' '				NOT NULL,			-- 配送業者グループ名
	TOTALPIC_FL					NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- トータルピックフラグ　トータルピック：1、軒先別：0
	PICKORDER_NR				NUMBER(3,0)			DEFAULT 0						,			-- ピック指示を作成する順番
	BERTHTIME_TX				VARCHAR2(12)		DEFAULT '0000'					,			-- 接車時間
	SHIPBERTH_CD				VARCHAR2(20)		DEFAULT NULL					,			-- 接車時間
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザコード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザ名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザコード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザ名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MA_TRANSPORTER
	ADD CONSTRAINT PK_MA_TRANSPORTER
	PRIMARY KEY(TRANSPORTER_CD)
;
