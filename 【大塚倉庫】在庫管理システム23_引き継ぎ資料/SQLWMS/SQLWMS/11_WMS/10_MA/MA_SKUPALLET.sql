-- Copyright (C) Seaos Corporation 2014 All rights reserved
--************************************************
-- 商品パレットマスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MA_SKUPALLET CASCADE CONSTRAINTS;
CREATE TABLE MA_SKUPALLET
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	ID							NUMBER(10,0)								NOT NULL,			-- キー
	SKU_CD						VARCHAR2(40)								NOT NULL,			-- 荷主コード+荷主商品コード 荷主商品コードは大塚の場合は035から始まるコード
	MANUFACTURER_CD				VARCHAR2(40)		DEFAULT NULL					,			-- 工場区分
	CASEPERPALLET_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積み付け数
	PALLETLAYER_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット段数
	CASEPERLAYER_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット回し数
	PALLETCAPACITY_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積載数　パレット段数 * パレット回し数
	STACKINGNUMBER_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 保管段数
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザコード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザ名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザコード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザ名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MA_SKUPALLET
	ADD CONSTRAINT PK_MA_SKUPALLET
	PRIMARY KEY(ID)
;
--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_SKU_MSKUPALLET ON MA_SKUPALLET
(
	SKU_CD
)
;

