-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- ABCマスタ
--************************************************

--格納引当でABC順に引当を行います

--------------------------------------------------
-- テーブル作成
--------------------------------------------------
DROP TABLE MA_ABC CASCADE CONSTRAINTS;
CREATE TABLE MA_ABC
(-- COLUMN								TYPE				DEFAULT				NULL				COMMENT
	ABC_ID								NUMBER(2,0)			DEFAULT 0			NOT NULL,			-- ABCID
	ABC_TX								VARCHAR2(100)		DEFAULT ' '			NOT NULL,			-- ABC名称 
	ABC_CD								VARCHAR2(1)			DEFAULT NULL				,			-- ABCコード
	-- 管理項目
	UPD_DT								DATE				DEFAULT NULL				,			-- 更新日時
	UPDUSER_CD							VARCHAR2(20)		DEFAULT ' '			NOT NULL,			-- 更新社員コード
	UPDUSER_TX							VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新社員名
	ADD_DT								DATE				DEFAULT SYSDATE		NOT NULL,			-- 登録日時
	ADDUSER_CD							VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員コード
	ADDUSER_TX							VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD						VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR						NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 更新カウンター
	STRECORD_ID							NUMBER(2,0)			DEFAULT 0			NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MA_ABC
	ADD CONSTRAINT PK_MA_ABC
	PRIMARY KEY(ABC_ID)
;
--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_ABC_MABC ON MA_ABC
(
	ABC_CD
)
;
