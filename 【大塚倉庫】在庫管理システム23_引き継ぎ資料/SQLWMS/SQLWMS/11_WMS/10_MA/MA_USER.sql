-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- ユーザマスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MA_USER CASCADE CONSTRAINTS;
CREATE TABLE MA_USER
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	USER_CD						VARCHAR2(10)							NOT NULL,			-- ユーザコード
	USER_TX						VARCHAR2(50)							NOT NULL,			-- ユーザ名
	PASSWORD_TX					VARCHAR2(100)							NOT NULL,			-- パスワード(HASH)
	USERADMINLEVEL_NR			NUMBER(3,0)			DEFAULT 0			NOT NULL,			-- ユーザ管理者レベル
	-- 管理項目
	UPD_DT						DATE				DEFAULT NULL				,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT ''					,			-- 更新社員コード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT ''					,			-- 更新社員名
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員コード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT -1			NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MA_USER
	ADD CONSTRAINT PK_MA_USER
	PRIMARY KEY(USER_CD)
;
