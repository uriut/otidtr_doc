-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- ABCSUBマスタ
--************************************************

--MA_ABCマスタと合わせて利用します
--格納引当時にABCSUB_CDが完全一致するロケを検索します

--------------------------------------------------
-- テーブル作成
--------------------------------------------------
DROP TABLE MA_ABCSUB CASCADE CONSTRAINTS;
CREATE TABLE MA_ABCSUB
(-- COLUMN								TYPE				DEFAULT				NULL				COMMENT
	ABCSUB_ID							NUMBER(4,0)			DEFAULT 0			NOT NULL,			-- ABCSUBID
	ABCSUB_TX							VARCHAR2(100)		DEFAULT ' '			NOT NULL,			-- ABCSUB名称 
	ABCSUB_CD							VARCHAR2(20)		DEFAULT NULL				,			-- ABCSUBコード
	-- 管理項目
	UPD_DT								DATE				DEFAULT NULL				,			-- 更新日時
	UPDUSER_CD							VARCHAR2(20)		DEFAULT ' '			NOT NULL,			-- 更新社員コード
	UPDUSER_TX							VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新社員名
	ADD_DT								DATE				DEFAULT SYSDATE		NOT NULL,			-- 登録日時
	ADDUSER_CD							VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員コード
	ADDUSER_TX							VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD						VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR						NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 更新カウンター
	STRECORD_ID							NUMBER(2,0)			DEFAULT 0			NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;
--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MA_ABCSUB
	ADD CONSTRAINT PK_MA_ABCSUB
	PRIMARY KEY(ABCSUB_ID)
;
--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_ABCSUB_MABCSUB ON MA_ABCSUB
(
	ABCSUB_CD
)
;
