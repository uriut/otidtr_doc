-- Copyright (C) Seaos Corporation 2014 All rights reserved
--************************************************
-- 納品先マスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MA_DELIVERYDESTINATION CASCADE CONSTRAINTS;
CREATE TABLE MA_DELIVERYDESTINATION
(-- COLUMN							TYPE				DEFAULT					NULL				COMMENT
	DELIVERYDESTINATION_CD			VARCHAR2(40)								NOT NULL,			-- 納品先コード
	DELIVERYDESTINATION_TX			VARCHAR2(40)		DEFAULT ' '				NOT NULL,			-- 納品先名
	DELIVERYDESTINATIONPOST_TX		VARCHAR2(7)			DEFAULT ' '				NOT NULL,			-- 郵便番号
	DELIVERYDESTINATIONADDRESS1_TX	VARCHAR2(8)			DEFAULT ' '				NOT NULL,			-- 都道府県
	DELIVERYDESTINATIONADDRESS2_TX	VARCHAR2(24)		DEFAULT ' '				NOT NULL,			-- 市区町村
	DELIVERYDESTINATIONADDRESS3_TX	VARCHAR2(24)		DEFAULT ' '				NOT NULL,			-- 番地
	DIRECTDELIVERY_CD				VARCHAR2(40)								NOT NULL,			-- 直送先コード  納品先コードを一定のロジックで変換（2-2-1,2,3納品先マスタ＃.xlsxを参照)店別のピッキングリストのkeyになる
	-- 管理項目
	UPD_DT							DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD						VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザコード
	UPDUSER_TX						VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザ名
	ADD_DT							DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD						VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザコード
	ADDUSER_TX						VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザ名
	UPDPROGRAM_CD					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR					NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID						NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MA_DELIVERYDESTINATION
	ADD CONSTRAINT PK_MA_DELIVERYDESTINATION
	PRIMARY KEY(DELIVERYDESTINATION_CD)
;
