-- Copyright (C) Seaos Corporation 2014 All rights reserved
--************************************************
-- 仕入先マスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MA_SUPPLIER CASCADE CONSTRAINTS;
CREATE TABLE MA_SUPPLIER
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	SUPPLIER_CD					VARCHAR2(40)								NOT NULL,			-- 仕入先コード  外販のみコードがIFされる、工場、SPはシステムで採番する
	TYPESUPPLIER_CD				VARCHAR2(40)		DEFAULT ' '				NOT NULL,			-- 仕入先種類  01:外販 02:工場 03:SP(ストックポイント)
	SUPPLIER_TX					VARCHAR(100)		DEFAULT ' '				NOT NULL,			-- 仕入先名
	SUPPLIERPOST_TX				VARCHAR2(7)			DEFAULT ' '				NOT NULL,			-- 郵便番号
	SUPPLIERADDRESS_TX			VARCHAR2(240)		DEFAULT ' '				NOT NULL,			-- 住所
	SUPPLIERMEMO_TX				VARCHAR2(200)		DEFAULT ' '						,			-- 仕入先備考
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザコード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザ名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザコード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザ名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MA_SUPPLIER
	ADD CONSTRAINT PK_MA_SUPPLIER
	PRIMARY KEY(SUPPLIER_CD)
;
