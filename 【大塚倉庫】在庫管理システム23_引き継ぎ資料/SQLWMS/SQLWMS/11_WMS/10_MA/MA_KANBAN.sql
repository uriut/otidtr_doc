-- Copyright (C) Seaos Corporation 2014 All rights reserved
--************************************************
-- かんばんマスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MA_KANBAN CASCADE CONSTRAINTS;
CREATE TABLE MA_KANBAN
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	KANBAN_CD					VARCHAR2(20)								NOT NULL,			-- かんばんコード
	KANBAN_TX					VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- かんばん名
	TYPEKANBAN_CD				VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- かんばんタイプ  格納かんばん、出荷かんばんの区分
	COLOR_TX					VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- 色
	AREA_TX						VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- エリア
	LINEUNIT_CD					VARCHAR2(2)			DEFAULT 0				NOT NULL,			-- ラインコード
	LINEUNIT_NR					NUMBER(2)			DEFAULT 0				NOT NULL,			-- ラインユニット
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザコード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザ名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザコード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザ名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MA_KANBAN
	ADD CONSTRAINT PK_MA_KANBAN
	PRIMARY KEY(KANBAN_CD)
;
