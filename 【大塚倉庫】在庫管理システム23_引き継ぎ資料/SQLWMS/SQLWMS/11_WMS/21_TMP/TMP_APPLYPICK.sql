-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 指示書付番
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_APPLYPICK CASCADE CONSTRAINTS;
CREATE TABLE TMP_APPLYPICK
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	TRN_ID						NUMBER(10,0)							NOT NULL,			-- トランザクションID
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 追加日
	--
	SKU_CD						VARCHAR2(20)							NOT NULL,			-- SKUコード
	TRANSPORTER_CD				VARCHAR2(40)							NOT NULL,
	DTNAME_TX					VARCHAR2(180)		DEFAULT ' '			NOT NULL,
	DTADDRESS1_TX				VARCHAR2(180)		DEFAULT ' '			NOT NULL,
	DTADDRESS2_TX				VARCHAR2(180)		DEFAULT ' '			NOT NULL,
	DTADDRESS3_TX				VARCHAR2(180)		DEFAULT ' '			NOT NULL,
	DTADDRESS4_TX				VARCHAR2(180)		DEFAULT ' '			NOT NULL,
	DIRECTDELIVERY_CD			VARCHAR2(40)		DEFAULT ' '			NOT NULL,			-- 直送先コード 
	DELIDATE_TX      			VARCHAR2(20)		DEFAULT ' '			NOT NULL,           -- 着荷指定日   
	TOTALPIC_FL					NUMBER(1,0)			DEFAULT 1			NOT NULL,			-- トータルピックフラグ　トータルピック：1、軒先別：0
	MAXLOADFACTOR_NR			NUMBER(3,2)								NOT NULL,
	ORDER_ID					NUMBER(2,0)								NOT NULL,			
	CASE_NR	  					NUMBER(10,0)							NOT NULL,
	WEIGHT_NR 					NUMBER(15,5)							NOT NULL,
	VOLUME_NR 					NUMBER(15,5)							NOT NULL,
	CAPACITY_NR					NUMBER(15,5)							NOT NULL
)
;
