-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 選択ID
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_SELECTID CASCADE CONSTRAINTS;
CREATE TABLE TMP_SELECTID
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	TRN_ID						NUMBER(10,0)							NOT NULL,			-- トランザクションID
	SELECT_ID					NUMBER(10,0)							NOT NULL,			-- 選択ID
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL			-- 登録日時
)
;
--主キーなし
