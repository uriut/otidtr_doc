-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 配送業者アップロード用一時テーブル
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_TRANSPORTER CASCADE CONSTRAINT;
CREATE TABLE TMP_TRANSPORTER
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	TRN_ID						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- トランザクションID
	SUCD						VARCHAR2(18)								NOT NULL,			-- 業者コード
	SUCD_NM						VARCHAR2(60)										,			-- 業者名
	SUCD_S						VARCHAR2(18)										,			-- 集約業者コード
	SUCD_S_NM					VARCHAR2(60)										,			-- 集約業者名
	KIZAICD						VARCHAR2(6)											,			-- 機材コード
	SORT						VARCHAR2(9)											,			-- １〜999　定義されていない業者は999
	STIME						VARCHAR2(12)										,			-- 接車時間（HHMM）
	YOBI_FLG1					VARCHAR2(60)										,			-- 予備フラグ１
	YOBI_FLG2					VARCHAR2(24)										,			-- 予備フラグ２
	YOBI_FLG3					VARCHAR2(24)										,			-- 予備フラグ３
	YOHAKU						VARCHAR2(37)													-- 余白
)
;

--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_SUCD_TMT ON TMP_TRANSPORTER
(
	SUCD
)
;
