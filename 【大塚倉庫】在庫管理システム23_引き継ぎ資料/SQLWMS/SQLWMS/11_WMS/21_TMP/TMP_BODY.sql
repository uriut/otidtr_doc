-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- IFアップロード用一時テーブル
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_BODY CASCADE CONSTRAINT;
CREATE TABLE TMP_BODY
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	TRN_ID						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- トランザクションID
	END_FL						NUMBER(1,0)									NOT NULL,			-- 完了フラグ
	ADD_DT						DATE										NOT NULL,			-- 追加日
	--
	HBKBN						VARCHAR2(6)									NOT NULL,			-- HB区分　「BD」固定
	SPSUCD						VARCHAR2(15)								NOT NULL,			-- SP業者CD
	SPSEQ						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- SPシーケンスNO1
	DKBN						VARCHAR2(6)									NOT NULL,			-- データ区分　HS:入荷、SH:出荷、
	ENTKBN						VARCHAR2(3)									NOT NULL,			-- エントリー区分
	SHIFT						VARCHAR2(6)									NOT NULL,			-- シフト区分
	HNNINUSHI					VARCHAR2(9)									NOT NULL,			-- 販売荷主
	NYMD						NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 荷主会計日付
	SYMD						NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 倉庫会計日付
	TYUNO						NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 納品日
	DENNO						VARCHAR2(45)								NOT NULL,			-- 伝票番号
	GYO							NUMBER(3,0)			DEFAULT 0				NOT NULL,			-- 明細行
	L_RETU						NUMBER(2,0)			DEFAULT 0				NOT NULL,			-- 明細行列
	BCD							VARCHAR2(64)								NOT NULL,			-- 物流コード
	NSYOCD						VARCHAR2(57)								NOT NULL,			-- 荷主商品コード
	DRK							VARCHAR2(60)								NOT NULL,			-- 電略
	SEINAK						VARCHAR2(150)								NOT NULL,			-- 商品名（ｶﾅ）
	SEINAN						VARCHAR2(240)								NOT NULL,			-- 商品名
	KIKAKUK						VARCHAR2(90)								NOT NULL,			-- 規格（ｶﾅ）
	KIKAKUN						VARCHAR2(120)								NOT NULL,			-- 規格
	KHKBN						NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 梱/端数区分
	KHKBN_NM					VARCHAR(60)									NOT NULL,			-- 単位名称
	MGKBN						NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 未合格区分
	MITATSU						VARCHAR2(3)									NOT NULL,			-- 未達区分
	SRY							NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 数量
	SRYK						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 梱数量
	SRYH						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 端数
	JSRY						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 受注数量
	HTYUNO1						VARCHAR2(45)								NOT NULL,			-- 発注NO1
	HTYUNO2						VARCHAR2(45)										,			-- 発注NO2　未使用
	SYUKA1						VARCHAR2(12)								NOT NULL,			-- 出荷場所1
	SYUKA2						VARCHAR2(12)										,			-- 出荷場所2　未使用
	SKCD1						VARCHAR2(18)								NOT NULL,			-- 倉庫コード1
	SKCD_NM1					VARCHAR2(60)								NOT NULL,			-- 倉庫名称1
	SKCD2						VARCHAR2(18)										,			-- 倉庫コード2　未使用
	SKCD_NM2					VARCHAR2(60)										,			-- 倉庫名称2　未使用
	LOTSRY						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- ロット数量
	LOT_FLG						VARCHAR2(3)									NOT NULL,			-- ロット表示フラグ　1:ロット表示あり
	LOT							VARCHAR2(60)								NOT NULL,			-- ロット
	LOTSUB						VARCHAR2(3)									NOT NULL,			-- サブロット
	LOTL						VARCHAR2(6)									NOT NULL,			-- ラインロット
	LOTC						VARCHAR2(6)									NOT NULL,			-- キャンペーンフラグ
	LOTP						VARCHAR2(12)								NOT NULL,			-- 生産プラント
	FCKBN						VARCHAR2(15)								NOT NULL,			-- 工場記号
	FCKBNN						VARCHAR2(15)								NOT NULL,			-- 工場略号
	LOTK						VARCHAR2(6)									NOT NULL,			-- 管理区分
	LOTS						VARCHAR2(3)									NOT NULL,			-- 出荷区分
	KAHIKBN						VARCHAR2(3)									NOT NULL,			-- 可否区分
	LOTDT						VARCHAR2(3)									NOT NULL,			-- ロット記帳義務
	KIGEN						VARCHAR2(30)								NOT NULL,			-- 賞味期限
	ZSRYK						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 残庫梱数量
	ZSRYH						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 残庫端数
	KAN1						NUMBER(7,0)			DEFAULT 0				NOT NULL,			-- 入目１(ケースの中のバラ数)
	KAN2						NUMBER(7,0)			DEFAULT 0				NOT NULL,			-- 入目２(バラ)
	KAN3						NUMBER(7,0)			DEFAULT 0				NOT NULL,			-- 入目３(中間の中のバラ数)
	KAN4						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 実入目
	SKBN						VARCHAR2(6)									NOT NULL,			-- 請求区分
	HOREI						VARCHAR2(3)									NOT NULL,			-- 保冷区分
	BUNRUI1						VARCHAR2(6)											,			-- 商品分類1　未使用
	BUNRUI2						VARCHAR2(6)											,			-- 商品分類2　未使用
	KATA						VARCHAR2(6)									NOT NULL,			-- 型コード
	TATE						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 縦
	YOKO						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 横
	TAKA						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 高さ
	WGT							NUMBER(9,3)			DEFAULT 0				NOT NULL,			-- 梱重量(ケース重量)g
	MWGT						NUMBER(11,3)		DEFAULT 0				NOT NULL,			-- 明細重量(ロット単位の明細重量)kg
	YOSEKI						NUMBER(9,5)			DEFAULT 0						,			-- 容積　未使用
	UNWGT						NUMBER(9,3)			DEFAULT 0						,			-- 運賃重量(ケースあたり重量)g　未使用
	JANCD						VARCHAR2(45)										,			-- JANコード　未使用
	SSYOCD						VARCHAR2(60)										,			-- 卸商品コード　未使用
	SYOCD1						VARCHAR2(60)								NOT NULL,			-- 商品コード1
	GSYOCD						VARCHAR2(30)								NOT NULL,			-- 外装商品コード
	PLSRY						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ＰＬ積数
	LFLOOR						VARCHAR2(3)											,			-- フロア　未使用
	LZONE						VARCHAR2(3)											,			-- ゾーン　未使用
	LAREA						VARCHAR2(3)											,			-- エリア　未使用
	LRETSU						VARCHAR2(6)											,			-- 列　未使用
	LGYO						VARCHAR2(6)											,			-- 行　未使用
	LDAN						VARCHAR2(6)											,			-- 段　未使用
	SEIKBN1						VARCHAR2(3)											,			-- 製造区分1　未使用
	SEIKBN2						VARCHAR2(3)											,			-- 製造区分2　未使用
	SEIKBN3						VARCHAR2(3)											,			-- 製造区分3　未使用
	SEIFLG1						VARCHAR2(6)											,			-- 製造フラグ1　未使用
	SEIFLG2						VARCHAR2(6)											,			-- 製造フラグ2　未使用
	SEIFLG3						VARCHAR2(6)											,			-- 製造フラグ3　未使用
	MBIKO						VARCHAR2(120)										,			-- 明細備考　未使用
	MBIKO_NM					VARCHAR2(180)										,			-- 明細備考　未使用
	FLG1						VARCHAR2(6)									NOT NULL,			-- 予備フラグ1
	FLG2						VARCHAR2(6)											,			-- 予備フラグ2　未使用
	FLG3						VARCHAR2(6)														-- 予備フラグ3　未使用
)
;

--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_SPSUCD_TMB ON TMP_BODY
(
	SPSUCD
)
;
CREATE INDEX IX_SPSEQ_TMB ON TMP_BODY
(
	SPSEQ
)
;
CREATE INDEX IX_DKBN_TMB ON TMP_BODY
(
	DKBN
)
;
CREATE INDEX IX_ENTKBN_TMB ON TMP_BODY
(
	ENTKBN
)
;
CREATE INDEX IX_SHIFT_TMB ON TMP_BODY
(
	SHIFT
)
;
CREATE INDEX IX_HNNINUSHI_TMB ON TMP_BODY
(
	HNNINUSHI
)
;
CREATE INDEX IX_NYMD_TMB ON TMP_BODY
(
	NYMD
)
;
CREATE INDEX IX_SYMD_TMB ON TMP_BODY
(
	SYMD
)
;
CREATE INDEX IX_TYUNO_TMB ON TMP_BODY
(
	TYUNO
)
;
CREATE INDEX IX_DENNO_TMB ON TMP_BODY
(
	DENNO
)
;
