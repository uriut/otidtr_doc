-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 選択コード
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_SELECTCD CASCADE CONSTRAINTS;
CREATE TABLE TMP_SELECTCD
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	TRN_ID						NUMBER(10,0)							NOT NULL,			-- トランザクションID
	SELECT_CD					VARCHAR2(30)							NOT NULL,			-- 選択コード
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL			-- 登録日時
)
;
--主キーなし
