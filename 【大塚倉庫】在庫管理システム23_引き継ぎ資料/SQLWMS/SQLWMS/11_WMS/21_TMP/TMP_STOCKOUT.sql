-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 欠品照会
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_STOCKOUT CASCADE CONSTRAINTS;
CREATE TABLE TMP_STOCKOUT
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	TRN_ID						NUMBER(10,0)							NOT NULL,			-- トランザクションID
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 追加日
	--
	SKU_CD						VARCHAR2(20)							NOT NULL,			-- SKUコード
	LOT_TX						VARCHAR2(40)							NOT NULL,			-- ロット
	SHIPPCS_NR					NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 出荷指示数
	STOCKPCS_NR        			NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 在庫数
	STOCKOUT_CD					CHAR(2)				DEFAULT ''			NOT NULL			-- 欠品理由コード
	--REASON_TX					VARCHAR2(40)		DEFAULT ''			NOT NULL			-- 欠品理由
)
;
