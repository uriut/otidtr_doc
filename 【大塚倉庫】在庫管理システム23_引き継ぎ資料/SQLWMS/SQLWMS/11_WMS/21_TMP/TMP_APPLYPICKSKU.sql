-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 指示書付番
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_APPLYPICKSKU CASCADE CONSTRAINTS;
CREATE TABLE TMP_APPLYPICKSKU
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	TRN_ID						NUMBER(10,0)							NOT NULL,			-- トランザクションID
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 追加日
	--
	SKU_CD						VARCHAR2(20)							NOT NULL,			-- SKUコード
	CASE_NR	  					NUMBER(10,0)							NOT NULL
)
;
