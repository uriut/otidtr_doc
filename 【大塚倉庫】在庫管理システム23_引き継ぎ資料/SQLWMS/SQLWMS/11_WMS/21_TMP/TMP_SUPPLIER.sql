-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 仕入先アップロード用一時テーブル
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_SUPPLIER CASCADE CONSTRAINT;
CREATE TABLE TMP_SUPPLIER
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	TRN_ID						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- トランザクションID
	SKCHKCD						VARCHAR2(18)								NOT NULL,			-- 仕入先コード
	NAME						VARCHAR2(180)										,			-- 仕入先名称
	JYUSYO						VARCHAR2(240)										,			-- 仕入先住所
	YUBIN						VARCHAR2(30)										,			-- 郵便番号
	KCD							VARCHAR2(15)										,			-- 地区コード
	CCDK						VARCHAR2(18)										,			-- 管理事業所
	KBN_FLG						VARCHAR2(3)											,			-- 管理フラグ
	SKECD						VARCHAR2(6)											,			-- 管理フラグが"1"の時の事業所コード
	SKCD						VARCHAR2(9)											,			-- 管理フラグが"1"の時の倉庫コード
	SAGYO_FLG					VARCHAR2(3)											,			-- 作業フラグ
	YOBIFLG1					VARCHAR2(6)											,			-- 予備フラグ2
	YOBIFLG2					VARCHAR2(6)											,			-- 予備フラグ2
	YOBIFLG3					VARCHAR2(6)											,			-- 予備フラグ3
	YOBIFLG4					VARCHAR2(6)											,			-- 予備フラグ4
	YOBIFLG5					VARCHAR2(6)											,			-- 予備フラグ5
	YOHAKU						VARCHAR2(93)													-- 余白
)
;
--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_SKCHKCD_TMSUP ON TMP_SUPPLIER
(
	SKCHKCD
)
;
