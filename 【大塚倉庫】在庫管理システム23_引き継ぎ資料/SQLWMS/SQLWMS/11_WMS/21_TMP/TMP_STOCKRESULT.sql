-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 在庫実績アップロード用一時テーブル
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_STOCKRESULT CASCADE CONSTRAINT;
CREATE TABLE TMP_STOCKRESULT
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	TRN_ID						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- トランザクションID
	MITA						VARCHAR2(3)									NOT NULL,			-- 未達区分　M:補給中（予定、現物在庫なし）
	NINUSHI						VARCHAR2(9)									NOT NULL,			-- 荷主コード　KK1：大塚製薬、FD1：大塚食品
	SYOCD						VARCHAR2(45)								NOT NULL,			-- 商品コード
	SKECD						VARCHAR2(6)									NOT NULL,			-- 事業所コード　04:東京支店
	MGKBN						VARCHAR2(3)									NOT NULL,			-- 未合区分　0:合格、1:未合格
	LOT							VARCHAR2(30)								NOT NULL,			-- ロット
	LOTW						VARCHAR2(3)									NOT NULL,			-- 詰合せ区分　"1"固定
	LOTK						VARCHAR2(3)									NOT NULL,			-- 管理区分
	LOTS						VARCHAR2(3)									NOT NULL,			-- 出荷止め区分　△：良品
	FCKBN						VARCHAR2(3)									NOT NULL,			-- 工場区分
	SKCD						VARCHAR2(9)									NOT NULL,			-- 倉庫コード
	BCD							VARCHAR2(24)								NOT NULL,			-- 倉庫商品コード
	DRK							VARCHAR2(30)								NOT NULL,			-- 電略　商品略称
	KAN1						NUMBER(7,0)			DEFAULT 0				NOT NULL,			-- 入目　ケース入目
	NYMD						NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 入庫日　未使用
	ZZANK						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 前日在庫ケース
	ZZANH						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 前日在庫バラ
	INK							NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 入庫ケース当日入庫数ケース
	INH							NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 入庫バラ　当日入庫数バラ
	OUTK						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 出庫ケース　当日出庫数ケース
	OUTH						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 出庫バラ　当日出庫数バラ
	TZANK						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 当日在庫ケース　前日在庫ケース＋入庫ケース−出庫ケース
	TZANH						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 当日在庫　バラ前日在庫バラ＋入庫バラ−出庫バラ
	OUTY1K						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 翌日出庫ケース
	OUTY1H						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 翌日出庫バラ
	OUTY2K						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 翌々日出庫ケース
	OUTY2H						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 翌々日出庫バラ
	KZANK						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 残庫ケース　引当可能数ケース
	KZANH						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 残庫バラ　引当可能数バラ
	KCD_NM						VARCHAR2(30)								NOT NULL,			-- 倉庫名
	SYORIYMD					VARCHAR2(24)								NOT NULL,			-- ホスト処理日
	SYORITMS					VARCHAR2(24)								NOT NULL,			-- ホスト処理時間
	YOHAKU						VARCHAR2(26)													-- 余白
)
;
--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_MITAD_TMSR ON TMP_STOCKRESULT
(
	MITA
)
;
