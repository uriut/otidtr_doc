-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- IFアップロード用一時テーブル
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_HEAD2 CASCADE CONSTRAINT;
CREATE TABLE TMP_HEAD2
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	TRN_ID						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- トランザクションID
	END_FL						NUMBER(1,0)									NOT NULL,			-- 完了フラグ
	ADD_DT						DATE										NOT NULL,			-- 追加日 
	--
	HBKBN						VARCHAR2(6)									NOT NULL,			-- HB区分　「H2」固定
	SPSUCD						VARCHAR2(15)								NOT NULL,			-- SP業者CD
	SPSEQ						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- SPシーケンスNO1
	DKBN						VARCHAR2(6)									NOT NULL,			-- データ区分　HS:入荷、SH:出荷、
	ENTKBN						VARCHAR2(3)									NOT NULL,			-- エントリー区分
	SHIFT						VARCHAR2(6)									NOT NULL,			-- シフト区分
	HNNINUSHI					VARCHAR2(9)									NOT NULL,			-- 販売荷主
	NYMD						NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 荷主会計日付
	SYMD						NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 倉庫会計日付
	TYUNO						NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 納品日
	DENNO						VARCHAR2(45)								NOT NULL,			-- 伝票番号
	MAKERCD						VARCHAR2(30)								NOT NULL,			-- メーカーコード
	MAKER_NM					VARCHAR2(90)								NOT NULL,			-- メーカー名称
	COMCD						VARCHAR2(15)								NOT NULL,			-- 会社コード
	COM_NM						VARCHAR2(90)								NOT NULL,			-- 会社名称
	SITENCD						VARCHAR2(12)								NOT NULL,			-- 支店コード
	SITEN_NM					VARCHAR2(90)								NOT NULL,			-- 支店名称
	SYUTYOCD					VARCHAR2(12)								NOT NULL,			-- 出張所コード
	SYUTYO_NM					VARCHAR2(90)								NOT NULL,			-- 出張所名称
	SYUTYO_KA					VARCHAR2(3)									NOT NULL,			-- 出張所課
	SYUTYO_FIL					VARCHAR2(30)								NOT NULL,			-- 出張所余白
	SITEN_JYUSYO				VARCHAR2(180)								NOT NULL,			-- 支店住所
	SITEN_TEL					VARCHAR2(15)								NOT NULL,			-- 支店TEL
	UTIKAE_FLG					VARCHAR2(3)									NOT NULL,			-- 打ち変えフラグ
	CCD							VARCHAR2(45)								NOT NULL,			-- 直送先CD
	NOHCD						VARCHAR2(45)								NOT NULL,			-- 納品先CD
	NOHCD_NM1					VARCHAR2(120)								NOT NULL,			-- 納品先名称1
	NOHCD_NM2					VARCHAR2(120)								NOT NULL,			-- 納品先名称2
	NOHCD_NM3					VARCHAR2(120)								NOT NULL,			-- 納品先名称3
	KCD							NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 地区コード
	KEN1						NUMBER(2,0)			DEFAULT 0				NOT NULL,			-- 県コード
	KEN2						VARCHAR2(3)									NOT NULL,			-- 離島区分
	YUBIN						VARCHAR2(30)								NOT NULL,			-- 郵便番号
	NOHCD_JYUSYO1				VARCHAR2(120)								NOT NULL,			-- 納品先住所1
	NOHCD_JYUSYO2				VARCHAR2(120)								NOT NULL,			-- 納品先住所2
	NOHCD_JYUSYO3				VARCHAR2(120)								NOT NULL,			-- 納品先住所3
	TEL							VARCHAR2(45)								NOT NULL,			-- 納品先TEL
	DAICD						VARCHAR2(45)								NOT NULL,			-- 代理店コード
	DAICD_NM1					VARCHAR2(120)								NOT NULL,			-- 代理店名称1
	DAICD_NM2					VARCHAR2(120)								NOT NULL,			-- 代理店名称2
	DAICD_NM3					VARCHAR2(120)								NOT NULL,			-- 代理店名称3
	NIJICD						VARCHAR2(45)								NOT NULL,			-- 二次店コード
	NIJICD_NM1					VARCHAR2(120)								NOT NULL,			-- 二次店名称1
	NIJICD_NM2					VARCHAR2(120)								NOT NULL,			-- 二次店名称2
	NIJICD_NM3					VARCHAR2(120)								NOT NULL,			-- 二次店名称3
	SANCD						VARCHAR2(45)								NOT NULL,			-- 三次店コード
	SANCD_NM1					VARCHAR2(120)								NOT NULL,			-- 三次店名称1
	SANCD_NM2					VARCHAR2(120)								NOT NULL,			-- 三次店名称2
	SANCD_NM3					VARCHAR2(90)								NOT NULL,			-- 三次店名称3
	CVS_FLG						VARCHAR2(6)									NOT NULL,			-- コンビニフラグ　CV:コンビニ卸
	YUSEN1						VARCHAR2(6)									NOT NULL,			-- 優先フラグ1
	YUSEN2						VARCHAR2(6)									NOT NULL,			-- 優先フラグ2
	HEAD2_FIL					VARCHAR2(9)														-- 余白
)
;

--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_SPSUCD_TMH2 ON TMP_HEAD2
(
	SPSUCD
)
;
CREATE INDEX IX_SPSEQ_TMH2 ON TMP_HEAD2
(
	SPSEQ
)
;
CREATE INDEX IX_DKBN_TMH2 ON TMP_HEAD2
(
	DKBN
)
;
CREATE INDEX IX_ENTKBN_TMH2 ON TMP_HEAD2
(
	ENTKBN
)
;
CREATE INDEX IX_SHIFT_TMH2 ON TMP_HEAD2
(
	SHIFT
)
;
CREATE INDEX IX_HNNINUSHI_TMH2 ON TMP_HEAD2
(
	HNNINUSHI
)
;
CREATE INDEX IX_NYMD_TMH2 ON TMP_HEAD2
(
	NYMD
)
;
CREATE INDEX IX_SYMD_TMH2 ON TMP_HEAD2
(
	SYMD
)
;
CREATE INDEX IX_TYUNO_TMH2 ON TMP_HEAD2
(
	TYUNO
)
;
CREATE INDEX IX_DENNO_TMH2 ON TMP_HEAD2
(
	DENNO
)
;
