-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 出荷引当
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_APPLYSHIP CASCADE CONSTRAINTS;
CREATE TABLE TMP_APPLYSHIP
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	TRN_ID						NUMBER(10,0)							NOT NULL,			-- トランザクションID
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 追加日
	--
	SHIPH_ID					NUMBER(10,0)							NOT NULL,			-- 
	SHIPD_ID					NUMBER(10,0)							NOT NULL,			-- 
	SKU_CD						VARCHAR2(20)							NOT NULL,			-- SKUコード
	LOT_TX						VARCHAR2(40)							NOT NULL,			-- ロット
	TYPESTOCK_CD				VARCHAR2(2)			DEFAULT '10'		NOT NULL,			-- 在庫タイプコード
	ORDERPALLET_NR				NUMBER(10,0)							NOT NULL,			-- 注文パレット数
	ORDERCASE_NR				NUMBER(10,0)							NOT NULL,			-- 注文ケース数
	ORDERBARA_NR				NUMBER(10,0)							NOT NULL,			-- 注文バラ数
	ORDERPCS_NR					NUMBER(10,0)							NOT NULL,			-- 注文個数
	APPLYPCS_NR					NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 引当個数 20140503追加
	SHORTPALLET_NR				NUMBER(10,0)							NOT NULL,			-- 欠品パレット数
	SHORTCASE_NR				NUMBER(10,0)							NOT NULL,			-- 欠品ケース数
	SHORTBARA_NR				NUMBER(10,0)							NOT NULL,			-- 欠品バラ数
	SHORTPCS_NR					NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 欠品数
	SHORT_FL					NUMBER(1,0)			DEFAULT 0			NOT NULL			-- （オーダー単位の）欠品フラグ
)
;
