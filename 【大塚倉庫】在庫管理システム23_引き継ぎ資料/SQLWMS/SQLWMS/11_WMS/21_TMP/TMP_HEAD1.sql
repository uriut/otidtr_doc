-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- IFアップロード用一時テーブル
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TMP_HEAD1 CASCADE CONSTRAINT;
CREATE TABLE TMP_HEAD1
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	TRN_ID						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- トランザクションID
	END_FL						NUMBER(1,0)									NOT NULL,			-- 完了フラグ
	ADD_DT						DATE										NOT NULL,			-- 追加日
	--
	HBKBN						VARCHAR2(6)									NOT NULL,			-- HB区分　「H1」固定
	SPSUCD						VARCHAR2(15)								NOT NULL,			-- SP業者CD
	SPSEQ						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- SPシーケンスNO1
	DKBN						VARCHAR2(6)									NOT NULL,			-- データ区分　HS:入荷、SH:出荷、
	ENTKBN						VARCHAR2(3)									NOT NULL,			-- エントリー区分
	SHIFT						VARCHAR2(6)									NOT NULL,			-- シフト区分
	HNNINUSHI					VARCHAR2(9)									NOT NULL,			-- 販売荷主
	NYMD						NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 荷主会計日付
	SYMD						NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 倉庫会計日付
	TYUNO						NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- 納品日
	DENNO						VARCHAR2(45)								NOT NULL,			-- 伝票番号
	WKBN						VARCHAR2(6)									NOT NULL,			-- 伝票作成区分
	GYO							NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 明細行
	IYMD						VARCHAR2(45)								NOT NULL,			-- 伝票印字日付
	NID							VARCHAR2(12)								NOT NULL,			-- 荷主取引区分
	NID_NM						VARCHAR2(60)								NOT NULL,			-- 荷主取引名称
	SKID						VARCHAR2(12)								NOT NULL,			-- 倉庫取引区分
	SKID_NM						VARCHAR(60)									NOT NULL,			-- 倉庫取引名称
	ATSUKAI_NM					VARCHAR(60)									NOT NULL,			-- 荷扱会社
	SKECD						VARCHAR2(12)								NOT NULL,			-- 倉庫営業所コード
	SKECD_NM					VARCHAR2(90)								NOT NULL,			-- 倉庫営業所名称
	SKECD_KCD					NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 倉庫営業所地区コード
	SKECD_KEN1					NUMBER(2,0)			DEFAULT 0				NOT NULL,			-- 倉庫営業所県コード
	SKECD_KEN2					VARCHAR2(3)									NOT NULL,			-- 倉庫営業所離島区分
	SKECD_JYUSYO				VARCHAR2(180)								NOT NULL,			-- 倉庫営業所住所
	SKECD_TEL					VARCHAR2(15)								NOT NULL,			-- 倉庫営業所TEL
	SYUKACD1					VARCHAR(15)									NOT NULL,			-- 出荷場所コード
	SYUKACD2					VARCHAR(15)											,			-- 出荷場所コード2　未使用
	SUCD1						VARCHAR(30)									NOT NULL,			-- 業者コード1
	SUCD1_NM					VARCHAR2(60)								NOT NULL,			-- 業者名1
	SUCD2						VARCHAR2(30)								NOT NULL,			-- 業者コード2
	SUCD2_NM					VARCHAR2(60)										,			-- 業者名2　未使用
	KYOHAICD					VARCHAR2(45)										,			-- 共配CD　未使用
	PKEN_FLG					VARCHAR2(3)									NOT NULL,			-- Pケンフラグ
	PKENCD						VARCHAR2(30)								NOT NULL,			-- Pケンコード
	PKENCD2						VARCHAR2(30)										,			-- Pケンコード2　未使用
	HATNO1						VARCHAR2(45)								NOT NULL,			-- 発注NO1
	HATNO2						VARCHAR2(45)										,			-- 発注NO2　未使用
	BIKO1						VARCHAR2(120)								NOT NULL,			-- 備考1
	BIKO2						VARCHAR2(120)								NOT NULL,			-- 備考2
	BIKO3						VARCHAR2(120)										,			-- 備考3　未使用
	BIKO_NM1					VARCHAR2(120)										,			-- 備考1全角　未使用
	BIKO_NM2					VARCHAR2(120)								NOT NULL,			-- 備考2全角
	SYOHEAD						VARCHAR2(30)								NOT NULL,			-- 商品コード識別
	TIME						VARCHAR2(45)										,			-- 着荷時間　未使用
	SBIKO						VARCHAR2(120)										,			-- 倉庫備考　未使用
	SBIKO_NM					VARCHAR2(120)										,			-- 倉庫備考全角　未使用
	SYABAN						VARCHAR2(9)											,			-- 車番　未使用
	KIZAI1						VARCHAR2(9)											,			-- 機材1　未使用
	KIZAI2						VARCHAR2(9)											,			-- 機材2　未使用
	DEN_G_SRY					NUMBER(9,0)									NOT NULL,			-- 伝票合計数量梱数
	G_SRY						NUMBER(9,0)									NOT NULL,			-- 納品合計梱数
	DEN_G_WGT					NUMBER(11,2)		DEFAULT 0				NOT NULL,			-- 伝票合計重量
	G_WGT						NUMBER(11,2)		DEFAULT 0				NOT NULL,			-- 納品合計重量
	JYMD						NUMBER(8,0)			DEFAULT 0						,			-- 実着日　未使用
	SNINUSHI					VARCHAR2(6)											,			-- 生産荷主　未使用
	HEAD1_FIL					VARCHAR2(216)										,			-- 余白
	WYMD						NUMBER(8,0)			DEFAULT 0				NOT NULL,			-- データ作成日
	WTIME						NUMBER(6,0)			DEFAULT 0				NOT NULL			-- データ作成時間
)
;

--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_SPSUCD_TMH1 ON TMP_HEAD1
(
	SPSUCD
)
;
CREATE INDEX IX_SPSEQ_TMH1 ON TMP_HEAD1
(
	SPSEQ
)
;
CREATE INDEX IX_DKBN_TMH1 ON TMP_HEAD1
(
	DKBN
)
;
CREATE INDEX IX_ENTKBN_TMH1 ON TMP_HEAD1
(
	ENTKBN
)
;
CREATE INDEX IX_SHIFT_TMH1 ON TMP_HEAD1
(
	SHIFT
)
;
CREATE INDEX IX_HNNINUSHI_TMH1 ON TMP_HEAD1
(
	HNNINUSHI
)
;
CREATE INDEX IX_NYMD_TMH1 ON TMP_HEAD1
(
	NYMD
)
;
CREATE INDEX IX_SYMD_TMH1 ON TMP_HEAD1
(
	SYMD
)
;
CREATE INDEX IX_TYUNO_TMH1 ON TMP_HEAD1
(
	TYUNO
)
;
CREATE INDEX IX_DENNO_TMH1 ON TMP_HEAD1
(
	DENNO
)
;
