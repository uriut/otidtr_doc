-- Copyright (C) Seaos Corporation 2013 All rights reserved

--************************************************
-- 入荷前Web用の一時テーブル
--************************************************
--------------------------------------------------
--              テーブル作成
--------------------------------------------------
DROP TABLE TMP_ARRIVWEB CASCADE CONSTRAINTS;
CREATE TABLE TMP_ARRIVWEB
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	TRN_ID						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- トランザクションID
	ARRIV_ID					NUMBER(10,0)								NOT NULL,			-- 入荷予定ID
	ARRIVORDERNO_CD				VARCHAR2(60)								NOT NULL,			-- 入荷予定番号（発注番号） 返品時はオーダー番号
	ARRIVORDERDETNO_CD			VARCHAR2(10)		DEFAULT '1'				NOT NULL,			-- 入荷予定明細番号（行No） 返品時はオーダー明細番号
	TYPEARRIV_CD				VARCHAR2(20)		DEFAULT '10'			NOT NULL,			-- 入荷予定タイプコード 10:通常  20:即出荷  30:返品
	STARRIV_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL,			-- 入荷予定状態ID
	-- 
	SKU_CD						VARCHAR2(60)								NOT NULL,			-- SKUコード			荷主コード+荷主商品コード
	LOT_TX						VARCHAR2(60)		DEFAULT NULL					,			-- ロット				ホストから来る/画面入力
	ARRIVPLAN_DT				DATE				DEFAULT NULL					,			-- 入荷予定日			ホストのBODY1.納品日をセットする
	ARRIVPLANCASE_NR			NUMBER(9,0)			DEFAULT 0						,			-- 入荷予定ケース数		入荷予定はパレット単位で作成する
	ARRIVPLANPCS_NR				NUMBER(9,0)			DEFAULT 0						,			-- 入荷予定数
	-- Web項目
	CARNUMBER_CD				VARCHAR2(60)		DEFAULT NULL			NOT NULL,			-- 車番				入荷前工程webでセットする
	SLIPNO_TX					VARCHAR2(60)		DEFAULT ''				NOT NULL,			-- 入荷伝票番号		一つの車番に対して複数の伝票が紐付くことがある。伝票番号はホストの中ではユニークではないので読みかえる必要がある
	ARRIVPLAN2_DT				DATE				DEFAULT NULL			NOT NULL,			-- 入荷予定時刻			入荷前工程webで入力される入荷予定時刻
	DRIVERTEL_TX				VARCHAR2(45)		DEFAULT ''				NOT NULL,			-- ドライバー電話番号	入荷前工程webで入力されるドライバの電話番号
	DRIVER_TX					VARCHAR2(45)		DEFAULT ''						,			-- ドライバー名	入荷前工程webで入力されるドライバ名
	BERTH_TX					VARCHAR2(45)		DEFAULT ''				NOT NULL,			-- 入荷指定バース		入荷前工程webで入力されるバースの電話番号
	SAMEDAYSHIP_FL				NUMBER(1,0)			DEFAULT 0				NOT NULL			-- 即出荷フラグ
)
;

--------------------------------------------------
--              一意キー設定
--------------------------------------------------
ALTER TABLE TMP_ARRIVWEB
	ADD CONSTRAINT PK_TMP_ARRIVWEB
	PRIMARY KEY(ARRIV_ID)
;

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_ARRIVORDERNO_TAW ON TMP_ARRIVWEB
(
    ARRIVORDERNO_CD
)
;
CREATE INDEX IX_SKU_TAW ON TMP_ARRIVWEB
(
    SKU_CD
)
;
