-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- SKUAbv[hpêe[u
--************************************************
--------------------------------------------------
--				e[uì¬
--------------------------------------------------
DROP TABLE TMP_SKU CASCADE CONSTRAINT;
CREATE TABLE TMP_SKU
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	TRN_ID						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- gUNVID
	NINUSHI						VARCHAR2(9)									NOT NULL,			-- ×åR[h
	SYOCD						VARCHAR2(45)								NOT NULL,			-- ¤iR[h
	FCKBN						VARCHAR2(15)								NOT NULL,			-- Hêæª
	BCD							VARCHAR2(24)								NOT NULL,			-- ¨¬R[h
	PL_T						NUMBER(4,0)			DEFAULT 0				NOT NULL,			-- pbgÏ
	PL_M						NUMBER(4,0)			DEFAULT 0				NOT NULL,			-- pbgñµ
	PL_D						NUMBER(4,0)			DEFAULT 0				NOT NULL,			-- pbgi
	B_PL_T						NUMBER(4,0)			DEFAULT 0				NOT NULL,			-- pbgÏ
	B_PL_M						NUMBER(4,0)			DEFAULT 0				NOT NULL,			-- pbgñµ
	B_PL_D						NUMBER(4,0)			DEFAULT 0				NOT NULL,			-- pbgi
	PL_H						NUMBER(4,0)			DEFAULT 0				NOT NULL,			-- ÛÇi
	PL_G						VARCHAR2(9)			DEFAULT 0				NOT NULL,			-- pbgÊO[v
	NISUGATA					VARCHAR2(6)									NOT NULL,			-- ×p
	YOHAKU						VARCHAR2(135)										,			-- ]
	NINUSHI2					VARCHAR2(9)									NOT NULL,			-- ×åR[h2@¢gp
	SYOCD2						VARCHAR2(45)								NOT NULL,			-- ¤iR[h2@¢gp
	GSYOCD						VARCHAR2(45)								NOT NULL,			-- OR[h
	SEINAN						VARCHAR2(180)								NOT NULL,			-- ¤i¼Ì
	SEINAK						VARCHAR2(150)								NOT NULL,			-- ¤i¼Ì¶Å
	KIKAKUK						VARCHAR2(120)								NOT NULL,			-- KieÊEP[X
	KIKAKUB						VARCHAR2(120)								NOT NULL,			-- KieÊE{[
	KIKAKUH						VARCHAR2(120)								NOT NULL,			-- KieÊEo
	KIKAKUKK					VARCHAR2(45)								NOT NULL,			-- KieÊ¶ÅEP[X
	KIKAKUBK					VARCHAR2(45)								NOT NULL,			-- KieÊ¶ÅE{[
	KIKAKUHK					VARCHAR2(45)								NOT NULL,			-- KieÊ¶ÅEo
	KAN1						NUMBER(7,0)			DEFAULT 0				NOT NULL,			-- üÚEP[X
	KAN2						NUMBER(7,0)			DEFAULT 0				NOT NULL,			-- üÚE{[
	KAN3						NUMBER(7,0)			DEFAULT 0				NOT NULL,			-- üÚEo
	TATEK						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- cEP[X
	YOKOK						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ¡EP[X
	TAKAK						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ³EP[X
	TATEB						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- cE{[
	YOKOB						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ¡E{[
	TAKAB						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ³E{[
	TATEH						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- cEo
	YOKOH						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ¡Eo
	TAKAH						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ³Eo
	WGT							NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- dÊEP[X
	WGTB						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- dÊE{[
	WGTH						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- dÊEo
	JANCD_K						VARCHAR2(15)								NOT NULL,			-- i`mR[h@P[X
	JANCD_B						VARCHAR2(15)								NOT NULL,			-- i`mR[h@{[
	JANCD_H						VARCHAR2(15)								NOT NULL,			-- i`mR[h@o
	ITFCD						VARCHAR2(20)								NOT NULL,			-- hseR[h
	GS1CD						VARCHAR2(30)								NOT NULL,			-- frPR[h
	YOBI1CD						VARCHAR2(45)								NOT NULL,			-- \õR[hP
	YOBI2CD						VARCHAR2(45)								NOT NULL,			-- \õR[hQ
	BCD2						VARCHAR2(24)								NOT NULL,			-- ¨¬R[hQ@¢gp
	NSYOCD						VARCHAR2(45)								NOT NULL,			-- ×å¤iR[h
	DRK							VARCHAR2(30)								NOT NULL,			-- dª
	SKBN						VARCHAR2(3)									NOT NULL,			-- ¿æª
	WGT_G						VARCHAR2(6)									NOT NULL,			-- dÊO[v
	YOSEKI_RITSU				NUMBER(6,4)			DEFAULT 0				NOT NULL,			-- eÏä¦
	YUKO						VARCHAR2(6)			DEFAULT 0				NOT NULL,			-- LøúÀ
	HOK1						NUMBER(7,1)			DEFAULT 0				NOT NULL,			-- ÛÇ¿b
	HOK2						NUMBER(7,1)			DEFAULT 0				NOT NULL,			-- ÛÇ¿³
	HOK3						NUMBER(7,1)			DEFAULT 0				NOT NULL,			-- ÛÇ¿¸
	NYK1						NUMBER(7,1)			DEFAULT 0				NOT NULL,			-- ×ð¿b
	NYK2						NUMBER(7,1)			DEFAULT 0				NOT NULL,			-- ×ð¿³
	NYK3						NUMBER(7,1)			DEFAULT 0				NOT NULL,			-- ×ð¿¸
	KITAKU						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- ñõàz
	TON							NUMBER(9,7)			DEFAULT 0				NOT NULL,			-- eÏg
	SAISU						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- Ë
	FCKBN2						VARCHAR2(15)								NOT NULL,			-- Hêæª
	HOREI						VARCHAR2(3)									NOT NULL,			-- Ûâæª
	KIKEN						VARCHAR2(3)									NOT NULL,			-- ë¯æª
	YOBI_KBN1					VARCHAR2(6)									NOT NULL,			-- \õæªP
	YOBI_KBN2					VARCHAR2(6)									NOT NULL,			-- \õæªQ
	YOBI_KBN3					VARCHAR2(6)									NOT NULL,			-- \õæªR
	KATA						VARCHAR2(6)									NOT NULL,			-- ^R[h
	KATA2						VARCHAR2(6)									NOT NULL,			-- ^R[hQ
	YOHAKU2						VARCHAR2(90)										,			-- ]
	WYMD						NUMBER(8,0)			DEFAULT 0						,			-- o^ú
	RYMD						NUMBER(8,0)			DEFAULT 0						,			-- C³ú
	DYMD						NUMBER(8,0)			DEFAULT 0						,			-- íú
	MAN 						VARCHAR2(30)													-- o^Ò
)
;

--------------------------------------------------
--              êÓL[Ýè
--------------------------------------------------

--------------------------------------------------
--              CfbNXÝè
--------------------------------------------------
CREATE INDEX IX_SKU_TMS ON TMP_SKU
(
	NINUSHI,NSYOCD
)
;
