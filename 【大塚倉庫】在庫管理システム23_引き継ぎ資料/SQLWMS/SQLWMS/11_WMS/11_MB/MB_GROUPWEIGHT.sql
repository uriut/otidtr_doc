-- Copyright (C) Seaos Corporation 2014 All rights reserved
--************************************************
-- 重量マスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MB_GROUPWEIGHT CASCADE CONSTRAINTS;
CREATE TABLE MB_GROUPWEIGHT
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	GROUP_ID					NUMBER(10,0)							NOT NULL,			-- グループID
	TOTALPIC_FL					NUMBER(2,0)								NOT NULL,			-- トータルピックフラグ
	MINWEIGHT_NR				NUMBER(20,0)							NOT NULL,			-- 最低重量
	MAXWEIGHT_NR				NUMBER(20,0)							NOT NULL,			-- 最大重量
	ORDER_ID					NUMBER(2,0)								NOT NULL,			-- ピック順番ID
	-- 管理項目
	UPD_DT						DATE				DEFAULT NULL				,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT ' '			NOT NULL,			-- 更新社員コード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新社員名
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員コード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0			NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;
--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MB_GROUPWEIGHT
	ADD CONSTRAINT PK_MB_GROUPWEIGHT
	PRIMARY KEY(GROUP_ID)
;
