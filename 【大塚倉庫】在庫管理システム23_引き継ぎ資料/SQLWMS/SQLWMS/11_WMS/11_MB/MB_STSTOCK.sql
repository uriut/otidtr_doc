-- Copyright (C) Seaos Corporation 2014 All rights reserved
--************************************************
-- 在庫状態マスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MB_STSTOCK CASCADE CONSTRAINTS;
CREATE TABLE MB_STSTOCK
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	STSTOCK_ID					NUMBER(2,0)								NOT NULL,			-- 状態ID
	STSTOCK_TX					VARCHAR2(50)							NOT NULL,			-- 状態名
	-- 管理項目
	UPD_DT						DATE				DEFAULT NULL				,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT ' '			NOT NULL,			-- 更新社員コード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新社員名
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員コード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0			NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;
--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MB_STSTOCK
	ADD CONSTRAINT PK_MB_STSTOCK
	PRIMARY KEY(STSTOCK_ID)
;
