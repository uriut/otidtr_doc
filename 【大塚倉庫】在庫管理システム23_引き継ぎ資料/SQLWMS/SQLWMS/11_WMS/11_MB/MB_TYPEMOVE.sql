-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 移動種別マスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MB_TYPEMOVE CASCADE CONSTRAINTS;
CREATE TABLE MB_TYPEMOVE
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	TYPEMOVE_CD					VARCHAR2(10)							NOT NULL,			-- 種別コード
	TYPEMOVE_TX					VARCHAR2(50)							NOT NULL,			-- 種別名
	-- 管理項目
	UPD_DT						DATE				DEFAULT NULL				,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT ' '			NOT NULL,			-- 更新社員コード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新社員名
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員コード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0			NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;
--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MB_TYPEMOVE
	ADD CONSTRAINT PK_MB_TYPEMOVE
	PRIMARY KEY(TYPEMOVE_CD)
;
