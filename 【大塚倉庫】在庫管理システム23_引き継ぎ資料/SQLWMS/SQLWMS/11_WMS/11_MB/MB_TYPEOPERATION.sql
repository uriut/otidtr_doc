-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 業務区分マスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MB_TYPEOPERATION;
CREATE TABLE MB_TYPEOPERATION
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	TYPEOPERATION_CD			VARCHAR2(10)							NOT NULL,			-- 業務区分コード
	TYPEOPERATION_TX			VARCHAR2(50)							NOT NULL,			-- 業務区分名
	PLUS_FL						NUMBER(1,0)			DEFAULT 1			NOT NULL,			-- 数量＋可能フラグ
	MINUS_FL					NUMBER(1,0)			DEFAULT 1			NOT NULL,			-- 数量−可能フラグ
	STOCKADD_FL					NUMBER(1,0)			DEFAULT 1			NOT NULL,			-- 在庫登録画面選択フラグ
	STOCKUPD_FL					NUMBER(1,0)			DEFAULT 1			NOT NULL,			-- 在庫修正画面選択フラグ
	NEEDSEND_FL					NUMBER(1,0)			DEFAULT 1			NOT NULL,			-- 要送信フラグ
	OPERATIONTYPE_CD			VARCHAR2(30)		DEFAULT 'DEFAULT'	NOT NULL,			-- 送信用業務区分（StockChangedReason）
	-- 管理項目
	UPD_DT						DATE				DEFAULT NULL				,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT ' '			NOT NULL,			-- 更新社員コード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新社員名
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員コード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0			NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;
--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MB_TYPEOPERATION
	ADD CONSTRAINT PK_MB_TYPEOPERATION
	PRIMARY KEY(TYPEOPERATION_CD)
;
