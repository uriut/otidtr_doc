-- Copyright (C) Seaos Corporation 2014 All rights reserved
--************************************************
-- トータルピックマスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MB_TOTALPIC CASCADE CONSTRAINTS;
CREATE TABLE MB_TOTALPIC
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	TOTALPIC_FL					NUMBER(1,0)									NOT NULL,			-- トータルピックフラグ
	TOTALPIC_TX					VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- 名称
	LAYER_NR					NUMBER(2,0)			DEFAULT 0				NOT NULL,			-- 段数
	ORDER_NR					NUMBER(2,0)			DEFAULT 0				NOT NULL,			-- 番数
	-- 管理項目
	UPD_DT						DATE				DEFAULT NULL					,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT ''						,			-- 更新ユーザコード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT ''						,			-- 更新ユーザ名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザコード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザ名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MB_TOTALPIC
	ADD CONSTRAINT PK_MB_TOTALPIC
	PRIMARY KEY(TOTALPIC_FL)
;
--------------------------------------------------
--              インデックス設定
--------------------------------------------------
