-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 入荷種別マスタ
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE MB_TYPEARRIV CASCADE CONSTRAINTS;
CREATE TABLE MB_TYPEARRIV
(-- COLUMN						TYPE				DEFAULT				NULL				COMMENT
	TYPEARRIV_CD				VARCHAR2(10)							NOT NULL,			-- 入荷種別コード
	TYPEARRIV_TX				VARCHAR2(50)							NOT NULL,			-- 入荷種別名
	-- 管理項目
	UPD_DT						DATE				DEFAULT NULL				,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT ' '			NOT NULL,			-- 更新社員コード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新社員名
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員コード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0			NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE MB_TYPEARRIV
	ADD CONSTRAINT PK_MB_TYPEARRIV
	PRIMARY KEY(TYPEARRIV_CD)
;
