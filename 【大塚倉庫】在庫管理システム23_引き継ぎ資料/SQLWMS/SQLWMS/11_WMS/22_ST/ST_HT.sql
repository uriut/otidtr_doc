-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- ＨＴ状態テーブル
--************************************************

--ＨＴ初期通信時にＨＴ状態テーブルは自動登録されます
--20140612 DHCP端末に対応するため端末名を追加
--20140613 DHCP端末の更新完了でIPアドレス追加

--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE ST_HT;
CREATE TABLE ST_HT
(-- COLUMN				TYPE				DEFAULT				NULL			COMMENT
	HT_ID				NUMBER(6,0)			DEFAULT 0			NOT NULL,		-- HT番号
	USER_CD				VARCHAR2(8)			DEFAULT ' '			NOT NULL,		-- 社員コード（未使用）
	IPADDRESS_CD		VARCHAR2(15)		DEFAULT ' '			NOT NULL,		-- ＩＰアドレス（DHCPの場合、ログイン時セット）
	DEVICE_CD			VARCHAR2(20)		DEFAULT ' '			NOT NULL,		-- 端末名（DHCPの場合、自動更新で利用）
	DHCP_FL				NUMBER(1,0)			DEFAULT 0			NOT NULL,		-- ＤＨＣＰフラグ（DHCPの場合、ログイン時セット）
	ONLINE_FL			NUMBER(1,0)			DEFAULT 0			NOT NULL,		-- オンラインフラグ
	APPUPD_FL			NUMBER(1,0)			DEFAULT 0			NOT NULL,		-- HTアプリ更新フラグ（0：更新しない、1:更新する）
	UPD_DT				DATE				DEFAULT NULL
)
;
