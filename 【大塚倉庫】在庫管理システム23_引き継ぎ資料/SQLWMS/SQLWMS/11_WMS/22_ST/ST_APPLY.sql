REM************************************************
REM 引当状態テーブル
REM************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE ST_APPLY;
CREATE TABLE ST_APPLY
(-- COLUMN					TYPE				DEFAULT			NULL		COMMENT
	SHIPAPPLY_FL			NUMBER(1,0)			DEFAULT 0		NOT NULL,	-- 出荷引当
	MOVEAPPLY_FL			NUMBER(1,0)			DEFAULT 0		NOT NULL,	-- 移動
	AMOVEAPPLY_FL			NUMBER(1,0)			DEFAULT 0		NOT NULL,	-- 積み増し
	INVENTA_FL				NUMBER(1,0)			DEFAULT 0		NOT NULL,	-- 棚卸
	MASTER_FL				NUMBER(1,0)			DEFAULT 0		NOT NULL	-- マスタメンテ
)
;

DELETE ST_APPLY;
INSERT INTO ST_APPLY (
	SHIPAPPLY_FL	,
	MOVEAPPLY_FL	,
	AMOVEAPPLY_FL	,
	INVENTA_FL		,
	MASTER_FL
) VALUES (
	0,
	0,
	0,
	0,
	0
);
