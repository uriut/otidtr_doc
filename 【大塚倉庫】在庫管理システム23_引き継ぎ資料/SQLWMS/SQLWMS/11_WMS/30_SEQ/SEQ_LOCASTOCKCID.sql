-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- �����ςݍ݌ɂh�c
--************************************************
DROP SEQUENCE		SEQ_LOCASTOCKCID;
CREATE SEQUENCE		SEQ_LOCASTOCKCID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		9999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
