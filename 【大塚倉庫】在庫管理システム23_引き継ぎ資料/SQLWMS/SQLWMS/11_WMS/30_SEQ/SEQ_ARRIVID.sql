-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- ���ח\��h�c
--************************************************
DROP SEQUENCE		SEQ_ARRIVID;
CREATE SEQUENCE		SEQ_ARRIVID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		9999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
