-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- ���Ƀf�[�^�h�c
--************************************************
DROP SEQUENCE		SEQ_INSTOCKID;
CREATE SEQUENCE		SEQ_INSTOCKID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		9999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
