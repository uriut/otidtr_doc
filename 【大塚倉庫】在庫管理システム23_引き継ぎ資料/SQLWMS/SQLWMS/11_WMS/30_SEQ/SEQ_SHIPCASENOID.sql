-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- �o�׃P�[�X�ԍ��h�c
--************************************************
DROP SEQUENCE		SEQ_SHIPCASENOID;
CREATE SEQUENCE		SEQ_SHIPCASENOID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
