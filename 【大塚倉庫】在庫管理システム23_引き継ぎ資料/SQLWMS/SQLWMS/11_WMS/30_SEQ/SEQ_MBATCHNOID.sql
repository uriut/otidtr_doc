-- ************************************************
-- 移動引当バッチＮＯ
-- ************************************************
DROP SEQUENCE		SEQ_MBATCHNOID;
CREATE SEQUENCE		SEQ_MBATCHNOID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		99999
	MINVALUE		1
	CYCLE
	CACHE			10
	ORDER;
