-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- �ړ��h�c
--************************************************
DROP SEQUENCE		SEQ_MOVEID;
CREATE SEQUENCE		SEQ_MOVEID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		9999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
