-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- �s�b�L���O�h�c
--************************************************
DROP SEQUENCE		SEQ_PICKID;
CREATE SEQUENCE		SEQ_PICKID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		9999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
