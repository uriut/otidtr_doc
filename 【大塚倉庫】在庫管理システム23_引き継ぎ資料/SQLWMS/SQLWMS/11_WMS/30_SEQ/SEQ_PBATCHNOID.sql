-- ************************************************
-- ピックバッチ番号ＩＤ
-- ************************************************
DROP SEQUENCE		SEQ_PBATCHNOID;
CREATE SEQUENCE		SEQ_PBATCHNOID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		99999
	MINVALUE		1
	CYCLE
	CACHE			10
	ORDER;
