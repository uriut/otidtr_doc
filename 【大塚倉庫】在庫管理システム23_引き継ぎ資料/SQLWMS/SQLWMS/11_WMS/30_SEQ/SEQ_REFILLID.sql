-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- ��[�h�c
--************************************************
DROP SEQUENCE		SEQ_REFILLID;
CREATE SEQUENCE		SEQ_REFILLID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		9999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
