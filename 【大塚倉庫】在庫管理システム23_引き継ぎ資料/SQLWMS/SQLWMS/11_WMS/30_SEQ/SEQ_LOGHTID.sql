-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- �g�s���O�h�c
--************************************************
DROP SEQUENCE		SEQ_LOGHTID;
CREATE SEQUENCE		SEQ_LOGHTID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		9999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
