-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- パレット明細番号ＩＤ
--************************************************
DROP SEQUENCE		SEQ_PALLETDETNOID;
CREATE SEQUENCE		SEQ_PALLETDETNOID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
