-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- ��[�w�����ԍ��h�c
--************************************************
DROP SEQUENCE		SEQ_SUPPNOID;
CREATE SEQUENCE		SEQ_SUPPNOID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
