-- ************************************************
-- �o�הԍ��h�c
-- ************************************************
DROP SEQUENCE		SEQ_SHIPNOID;
CREATE SEQUENCE		SEQ_SHIPNOID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		999999999
	MINVALUE		1
	CYCLE
	CACHE			100
	ORDER;
