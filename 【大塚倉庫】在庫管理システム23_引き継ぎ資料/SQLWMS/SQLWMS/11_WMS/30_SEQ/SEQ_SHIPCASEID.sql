-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- �o�׃P�[�X�h�c
--************************************************
DROP SEQUENCE		SEQ_SHIPCASEID;
CREATE SEQUENCE		SEQ_SHIPCASEID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		9999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
