-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- トランザクションＩＤ
--************************************************
DROP SEQUENCE		SEQ_TRNID;
CREATE SEQUENCE		SEQ_TRNID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		9999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
