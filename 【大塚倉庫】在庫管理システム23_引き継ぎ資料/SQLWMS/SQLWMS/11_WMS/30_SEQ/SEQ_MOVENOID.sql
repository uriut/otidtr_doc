-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 移動指示書番号ＩＤ
--************************************************
DROP SEQUENCE		SEQ_MOVENOID;
CREATE SEQUENCE		SEQ_MOVENOID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		999999999
	MINVALUE		1
	CYCLE
	CACHE			50
	ORDER;
