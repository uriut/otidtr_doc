-- ************************************************
-- �o�ו�[�o�b�`�m�n
-- ************************************************
DROP SEQUENCE		SEQ_SBATCHNOID;
CREATE SEQUENCE		SEQ_SBATCHNOID
	START WITH		&1
	INCREMENT BY	1
	MAXVALUE		99999
	MINVALUE		1
	CYCLE
	CACHE			10
	ORDER;
