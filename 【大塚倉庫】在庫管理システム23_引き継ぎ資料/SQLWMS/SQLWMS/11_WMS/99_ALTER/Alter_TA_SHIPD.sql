--1.コピー元テーブル作成
CREATE TABLE BK_TA_SHIPD AS
SELECT * FROM TA_SHIPD
;

--2.件数チェック
SELECT COUNT(*) FROM TA_SHIPD;
SELECT COUNT(*) FROM BK_TA_SHIPD;

--3.元テーブルの削除
DROP TABLE TA_SHIPD; 

--4.入替用新規テーブル作成
--
spool C:\SQL\OTID\SQLWMS\11_WMS\20_T\CREATE.log

REM---------------------                                    TABLESPACE          PCTFREE   PCTUSED   INITIAL   NEXT      NAME
@C:\SQL\OTID\SQLWMS\11_WMS\20_T\TA_SHIPD;

spool off;
--

--************************************************
-- 出荷指示データ明細
--************************************************
--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------

--5.バックアップテーブルから入替用新規テーブルにインサート

INSERT INTO TA_SHIPD (
	SHIPD_ID					,
	SHIPH_ID					,
	TRN_ID						,
	TYPESHIP_CD					,
	TYPESHIP_TX					,
	START_DT					,
	END_DT						,
	STSHIP_ID					,
	-- 出荷指示
	ORDERNO_CD					,
	ORDERNOSUB_CD				,
	ORDERDETNO_CD				,
	SKU_CD						,
	LOT_TX						,
	KANBAN_CD					,
	CASEPERPALLET_NR			,
	PALLETLAYER_NR				,
	STACKINGNUMBER_NR			,
	SKU_TX						,
	TYPELOCA_CD					,
	STOCKTYPE_CD				,
	PALLETCAPACITY_NR			,
	ORDERPALLET_NR				,
	ORDERCASE_NR				,
	ORDERBARA_NR				,
	ORDERPCS_NR					,
	UNITPRICE_NR				,
	UNITADDPOINT_NR				,
	BONUS_FL					,
	UNITSALEDISCOUNT_NR			,
	DETCOMMENT_TX				,
	TYPESHIPAPPLY_CD			,
	TYPESHIPROUTE_CD			,
	SKUTYPE_CD					,
	SUPPLYTYPE_CD				,
	SUPPLIER_CD					,
	SETSKUGROUPSEQ_NR			,
	PRINTDETTYPE_CD				,
	PRINTPICKLISTTYPE_CD		,
	-- 引当結果
	APPLY_DT					,
	APPLYUSER_CD				,
	APPLYUSER_TX				,
	--
	BATCHNO_CD					,
	TYPEAPPLY_CD				,
	TYPEBLOCK_CD				,
	TYPECARRY_CD				,
	-- 管理項目
	UPD_DT						,
	UPDUSER_CD					,
	UPDUSER_TX					,
	ADD_DT						,
	ADDUSER_CD					,
	ADDUSER_TX					,
	UPDPROGRAM_CD				,
	UPDCOUNTER_NR				,
	STRECORD_ID					
)
SELECT
	SHIPD_ID					,
	SHIPH_ID					,
	TRN_ID						,
	TYPESHIP_CD					,
	' '							,	-- TYPESHIP_TX
	START_DT					,
	END_DT						,
	STSHIP_ID					,
	-- 出荷指示
	ORDERNO_CD					,
	ORDERNOSUB_CD				,
	ORDERDETNO_CD				,
	SKU_CD						,
	LOT_TX						,
	KANBAN_CD					,
	CASEPERPALLET_NR			,
	PALLETLAYER_NR				,
	STACKINGNUMBER_NR			,
	SKU_TX						,
	TYPELOCA_CD					,
	STOCKTYPE_CD				,
	PALLETCAPACITY_NR			,
	ORDERPALLET_NR				,
	ORDERCASE_NR				,
	ORDERBARA_NR				,
	ORDERPCS_NR					,
	UNITPRICE_NR				,
	UNITADDPOINT_NR				,
	BONUS_FL					,
	UNITSALEDISCOUNT_NR			,
	DETCOMMENT_TX				,
	TYPESHIPAPPLY_CD			,
	TYPESHIPROUTE_CD			,
	SKUTYPE_CD					,
	SUPPLYTYPE_CD				,
	SUPPLIER_CD					,
	SETSKUGROUPSEQ_NR			,
	PRINTDETTYPE_CD				,
	PRINTPICKLISTTYPE_CD		,
	-- 引当結果
	APPLY_DT					,
	APPLYUSER_CD				,
	APPLYUSER_TX				,
	--
	BATCHNO_CD					,
	TYPEAPPLY_CD				,
	TYPEBLOCK_CD				,
	TYPECARRY_CD				,
	-- 管理項目
	UPD_DT						,
	UPDUSER_CD					,
	UPDUSER_TX					,
	ADD_DT						,
	ADDUSER_CD					,
	ADDUSER_TX					,
	UPDPROGRAM_CD				,
	UPDCOUNTER_NR				,
	STRECORD_ID					
FROM
	BK_TA_SHIPD
;


--6.件数チェック
SELECT COUNT(*) FROM TA_SHIPD;
SELECT COUNT(*) FROM BK_TA_SHIPD;


--7.問題なければ確定
COMMIT;

--8.バックアップテーブル削除
DROP TABLE BK_TA_SHIPD;
