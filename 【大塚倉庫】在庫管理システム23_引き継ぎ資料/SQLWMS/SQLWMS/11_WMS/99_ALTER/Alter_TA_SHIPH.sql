--1.コピー元テーブル作成
CREATE TABLE BK_TA_SHIPH AS
SELECT * FROM TA_SHIPH
;

--2.件数チェック
SELECT COUNT(*) FROM TA_SHIPH;
SELECT COUNT(*) FROM BK_TA_SHIPH;

--3.元テーブルの削除
DROP TABLE TA_SHIPH; 

--4.入替用新規テーブル作成
--
spool C:\SQL\OTID\SQLWMS\11_WMS\20_T\CREATE.log

REM---------------------                                    TABLESPACE          PCTFREE   PCTUSED   INITIAL   NEXT      NAME
@C:\SQL\OTID\SQLWMS\11_WMS\20_T\TA_SHIPH;

spool off;
--

--************************************************
-- 出荷指示データ明細
--************************************************
--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------

--5.バックアップテーブルから入替用新規テーブルにインサート

INSERT INTO TA_SHIPH (
	SHIPH_ID					,
	TRN_ID						,
	TYPESHIP_CD					,
	TYPESHIP_TX					,
	START_DT					,
	END_DT						,
	STSHIP_ID					,
	-- 出荷指示
	ORDERNO_CD					,
	ORDERNOSUB_CD				,
	CUSTOMERORDERNO_CD			,
	CUSTOMER_CD					,
	ORDERTYPE_CD				,
	ENTRYTYPE_CD				,
	ORDERDATE_DT				,
	SALESSHOP_CD				,
	OWNER_CD					,
	CHANNELTYPE_CD				,
	TRANSPORTER1_CD				,
	TRANSPORTER2_CD				,
	DELIVERYDESTINATION_CD		,
	DTNAME_TX					,
	DTPOSTAL_CD					,
	DTADDRESS1_TX				,
	DTADDRESS2_TX				,
	DTADDRESS3_TX				,
	DTADDRESS4_TX				,
	DIRECTDELIVERY_CD			,
	TYPEPLALLET_CD				,
	SHIPBERTH_CD				,
	SHIPPLAN_DT					,
	SLIPNO_TX					,
	CARRIER_CD					,
	DELITYPE_CD					,
	DELIDATE_TX					,
	DELITIME_CD					,
	CHARGETYPE_CD				,
	GIFTTYPE_CD					,
	TYPETEMPERATURE_CD			,
	TOTALITEMPRICE_NR			,
	TOTALPRICE_NR				,
	TOTALTAX_NR					,
	DELICHARGE_NR				,
	GIFTCHARGE_NR				,
	CODCHARGE_NR				,
	DISCOUNT_NR					,
	SALEDISCOUNT_NR				,
	COUPON_CD					,
	COUPONDISCOUNT_NR			,
	USEPOINT_NR					,
	ADDPOINT_NR					,
	CUSTOMERCOMMENT_TX			,
	CSCOMMENT_TX				,
	OPERATORCOMMENT_TX			,
	DETCOUNT_NR					,
	-- 納品書
	INVOICE_FL					,
	INVOICECOMMENT_TX			,
	PRTINVOICECOUNT_NR			,
	PRTINVOICE_DT				,
	PRTINVOICEUSER_CD			,
	PRTINVOICEUSER_TX			,
	-- 出荷伝票
	DELIBOX_FL					,
	DELIITEM_TX					,
	DELICOMMENT_TX				,
	PRTDELICOUNT_NR				,
	PRTDELI_DT					,
	PRTDELIUSER_CD				,
	PRTDELIUSER_TX				,
	--
	PACKNO1_CD					,
	PACKNO2_CD					,
	PACKNO3_CD					,
	PACKNO4_CD					,
	PACKNO5_CD					,
	PACKNO6_CD					,
	PACKNO7_CD					,
	PACKNO8_CD					,
	PACKNO9_CD					,
	PACKNO10_CD					,
	-- 管理項目
	UPD_DT						,
	UPDUSER_CD					,
	UPDUSER_TX					,
	ADD_DT						,
	ADDUSER_CD					,
	ADDUSER_TX					,
	UPDPROGRAM_CD				,
	UPDCOUNTER_NR				,
	STRECORD_ID					
)
SELECT
	SHIPH_ID					,
	TRN_ID						,
	TYPESHIP_CD					,
	' '							,	-- TYPESHIP_TX
	START_DT					,
	END_DT						,
	STSHIP_ID					,
	-- 出荷指示
	ORDERNO_CD					,
	ORDERNOSUB_CD				,
	CUSTOMERORDERNO_CD			,
	CUSTOMER_CD					,
	ORDERTYPE_CD				,
	ENTRYTYPE_CD				,
	ORDERDATE_DT				,
	SALESSHOP_CD				,
	OWNER_CD					,
	CHANNELTYPE_CD				,
	TRANSPORTER1_CD				,
	TRANSPORTER2_CD				,
	DELIVERYDESTINATION_CD		,
	DTNAME_TX					,
	DTPOSTAL_CD					,
	DTADDRESS1_TX				,
	DTADDRESS2_TX				,
	DTADDRESS3_TX				,
	DTADDRESS4_TX				,
	DIRECTDELIVERY_CD			,
	TYPEPLALLET_CD				,
	SHIPBERTH_CD				,
	SHIPPLAN_DT					,
	SLIPNO_TX					,
	CARRIER_CD					,
	DELITYPE_CD					,
	DELIDATE_TX					,
	DELITIME_CD					,
	CHARGETYPE_CD				,
	GIFTTYPE_CD					,
	TYPETEMPERATURE_CD			,
	TOTALITEMPRICE_NR			,
	TOTALPRICE_NR				,
	TOTALTAX_NR					,
	DELICHARGE_NR				,
	GIFTCHARGE_NR				,
	CODCHARGE_NR				,
	DISCOUNT_NR					,
	SALEDISCOUNT_NR				,
	COUPON_CD					,
	COUPONDISCOUNT_NR			,
	USEPOINT_NR					,
	ADDPOINT_NR					,
	CUSTOMERCOMMENT_TX			,
	CSCOMMENT_TX				,
	OPERATORCOMMENT_TX			,
	DETCOUNT_NR					,
	-- 納品書
	INVOICE_FL					,
	INVOICECOMMENT_TX			,
	PRTINVOICECOUNT_NR			,
	PRTINVOICE_DT				,
	PRTINVOICEUSER_CD			,
	PRTINVOICEUSER_TX			,
	-- 出荷伝票
	DELIBOX_FL					,
	DELIITEM_TX					,
	DELICOMMENT_TX				,
	PRTDELICOUNT_NR				,
	PRTDELI_DT					,
	PRTDELIUSER_CD				,
	PRTDELIUSER_TX				,
	--
	PACKNO1_CD					,
	PACKNO2_CD					,
	PACKNO3_CD					,
	PACKNO4_CD					,
	PACKNO5_CD					,
	PACKNO6_CD					,
	PACKNO7_CD					,
	PACKNO8_CD					,
	PACKNO9_CD					,
	PACKNO10_CD					,
	-- 管理項目
	UPD_DT						,
	UPDUSER_CD					,
	UPDUSER_TX					,
	ADD_DT						,
	ADDUSER_CD					,
	ADDUSER_TX					,
	UPDPROGRAM_CD				,
	UPDCOUNTER_NR				,
	STRECORD_ID					
FROM
	BK_TA_SHIPH
;


--6.件数チェック
SELECT COUNT(*) FROM TA_SHIPH;
SELECT COUNT(*) FROM BK_TA_SHIPH;


--7.問題なければ確定
COMMIT;

--8.バックアップテーブル削除
DROP TABLE BK_TA_SHIPH;
