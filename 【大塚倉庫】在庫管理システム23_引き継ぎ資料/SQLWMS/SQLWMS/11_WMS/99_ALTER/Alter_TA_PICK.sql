--1.コピー元テーブル作成
CREATE TABLE BK_TA_PICK AS
SELECT * FROM TA_PICK
;

--2.件数チェック
SELECT COUNT(*) FROM TA_PICK;
SELECT COUNT(*) FROM BK_TA_PICK;

--3.元テーブルの削除
DROP TABLE TA_PICK; 

--4.入替用新規テーブル作成
--
spool C:\SQL\OTID\SQLWMS\11_WMS\20_T\CREATE.log

REM---------------------                                    TABLESPACE          PCTFREE   PCTUSED   INITIAL   NEXT      NAME
@C:\SQL\OTID\SQLWMS\11_WMS\20_T\TA_PICK;

spool off;
--

--************************************************
-- 出荷指示データ明細
--************************************************
--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------

--5.バックアップテーブルから入替用新規テーブルにインサート

INSERT INTO TA_PICK (
	PICK_ID						,
	TRN_ID						,
	PICKNO_CD					,
	TYPEPICK_CD					,
	START_DT					,
	END_DT						,
	STPICK_ID					,
	-- 出荷指示                  
	SHIPH_ID					,
	SHIPD_ID					,
	ORDERNO_CD					,
	ORDERNOSUB_CD				,
	ORDERDETNO_CD				,
	-- SKU                       
	SKU_CD						,
	LOT_TX						,
	KANBAN_CD					,
	CASEPERPALLET_NR			,
	PALLETLAYER_NR				,
	PALLETCAPACITY_NR			,
	STACKINGNUMBER_NR			,
	TYPEPLALLET_CD				,
	PALLETDETNO_CD				,
	TRANSPORTER_CD				,
	DELIVERYDESTINATION_CD		,
	-- 軒先ピック対応            
	DIRECTDELIVERY_CD			,
	DELIDATE_TX      			,
	TOTALPIC_FL					,
	--                           
	SHIPBERTH_CD				,
	SHIPPLAN_DT					,
	BERTHTIME_TX				,
	SLIPNO_TX					,
	--                           
	USELIMIT_DT					,
	STOCK_DT					,
	AMOUNT_NR					,
	PALLET_NR					,
	CASE_NR						,
	BARA_NR						,
	PCS_NR						,
	-- 元ロケ                    
	DEFER_FL					,
	SRCTYPESTOCK_CD				,
	SRCLOCA_CD					,
	SRCSTART_DT					,
	SRCEND_DT					,
	SRCHT_ID					,
	SRCHTUSER_CD				,
	SRCHTUSER_TX				,
	SRCLOCASTOCKC_ID			,
	-- 検品                      
	CHECK_FL					,
	CHECKCOUNT_NR				,
	CHECKSTART_DT				,
	CHECKEND_DT					,
	CHECKHT_ID					,
	CHECKHTUSER_CD				,
	CHECKHTUSER_TX				,
	-- 出荷完了                  
	SHIPEND_FL					,
	SHIPENDCOUNT_NR				,
	SHIPENDSTART_DT				,
	SHIPENDEND_DT				,
	SHIPENDHT_ID				,
	SHIPENDHTUSER_CD			,
	SHIPENDHTUSER_TX			,
	-- 納品書                    
	PRTPACKLIST_FL				,
	-- 印刷                      
	PRINT_FL					,
	PRTCOUNT_NR					,
	PRT_DT						,
	PRTUSER_CD					,
	PRTUSER_TX					,
	--                           
	WRAPGROUP_ID				,
	WRAPAPPLY_NR				,
	--                           
	BATCHNO_CD					,
	TYPEAPPLY_CD				,
	TYPEBLOCK_CD				,
	TYPECARRY_CD				,
	-- 管理項目                  
	UPD_DT						,
	UPDUSER_CD					,
	UPDUSER_TX					,
	ADD_DT						,
	ADDUSER_CD					,
	ADDUSER_TX					,
	UPDPROGRAM_CD				,
	UPDCOUNTER_NR				,
	STRECORD_ID					 
)
SELECT
	PICK_ID						,
	TRN_ID						,
	PICKNO_CD					,
	TYPEPICK_CD					,
	START_DT					,
	END_DT						,
	STPICK_ID					,
	-- 出荷指示                  
	SHIPH_ID					,
	SHIPD_ID					,
	ORDERNO_CD					,
	ORDERNOSUB_CD				,
	ORDERDETNO_CD				,
	-- SKU                       
	SKU_CD						,
	LOT_TX						,
	KANBAN_CD					,
	CASEPERPALLET_NR			,
	PALLETLAYER_NR				,
	PALLETCAPACITY_NR			,
	STACKINGNUMBER_NR			,
	TYPEPLALLET_CD				,
	PALLETDETNO_CD				,
	TRANSPORTER_CD				,
	DELIVERYDESTINATION_CD		,
	-- 軒先ピック対応            
	DIRECTDELIVERY_CD			,
	' ' DELIDATE_TX    			,
	0   TOTALPIC_FL				,
	--                           
	SHIPBERTH_CD				,
	SHIPPLAN_DT					,
	BERTHTIME_TX				,
	SLIPNO_TX					,
	--                           
	USELIMIT_DT					,
	STOCK_DT					,
	AMOUNT_NR					,
	PALLET_NR					,
	CASE_NR						,
	BARA_NR						,
	PCS_NR						,
	-- 元ロケ                    
	DEFER_FL					,
	SRCTYPESTOCK_CD				,
	SRCLOCA_CD					,
	SRCSTART_DT					,
	SRCEND_DT					,
	SRCHT_ID					,
	SRCHTUSER_CD				,
	SRCHTUSER_TX				,
	SRCLOCASTOCKC_ID			,
	-- 検品                      
	CHECK_FL					,
	CHECKCOUNT_NR				,
	CHECKSTART_DT				,
	CHECKEND_DT					,
	CHECKHT_ID					,
	CHECKHTUSER_CD				,
	CHECKHTUSER_TX				,
	-- 出荷完了                  
	SHIPEND_FL					,
	SHIPENDCOUNT_NR				,
	SHIPENDSTART_DT				,
	SHIPENDEND_DT				,
	SHIPENDHT_ID				,
	SHIPENDHTUSER_CD			,
	SHIPENDHTUSER_TX			,
	-- 納品書                    
	PRTPACKLIST_FL				,
	-- 印刷                      
	PRINT_FL					,
	PRTCOUNT_NR					,
	PRT_DT						,
	PRTUSER_CD					,
	PRTUSER_TX					,
	--                           
	WRAPGROUP_ID				,
	WRAPAPPLY_NR				,
	--                           
	BATCHNO_CD					,
	TYPEAPPLY_CD				,
	TYPEBLOCK_CD				,
	TYPECARRY_CD				,
	-- 管理項目                  
	UPD_DT						,
	UPDUSER_CD					,
	UPDUSER_TX					,
	ADD_DT						,
	ADDUSER_CD					,
	ADDUSER_TX					,
	UPDPROGRAM_CD				,
	UPDCOUNTER_NR				,
	STRECORD_ID					 
FROM
	BK_TA_PICK 
;


--6.件数チェック
SELECT COUNT(*) FROM TA_PICK;
SELECT COUNT(*) FROM BK_TA_PICK;


--7.問題なければ確定
COMMIT;

--8.バックアップテーブル削除
DROP TABLE BK_TA_PICK;
