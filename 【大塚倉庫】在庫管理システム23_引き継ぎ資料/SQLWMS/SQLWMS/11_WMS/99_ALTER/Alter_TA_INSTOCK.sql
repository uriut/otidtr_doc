--1.コピー元テーブル作成
CREATE TABLE BK_TA_INSTOCK AS
SELECT * FROM TA_INSTOCK
;

--2.件数チェック
SELECT COUNT(*) FROM TA_INSTOCK;
SELECT COUNT(*) FROM BK_TA_INSTOCK;

--3.元テーブルの削除
DROP TABLE TA_INSTOCK; 

--4.入替用新規テーブル作成
--
spool C:\SQL\OTID\SQLWMS\11_WMS\20_T\CREATE.log

REM---------------------                                    TABLESPACE          PCTFREE   PCTUSED   INITIAL   NEXT      NAME
@C:\SQL\OTID\SQLWMS\11_WMS\20_T\TA_INSTOCK;

spool off;
--

--************************************************
-- 出荷指示データ明細
--************************************************
--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------

--5.バックアップテーブルから入替用新規テーブルにインサート

INSERT INTO TA_INSTOCK (
	INSTOCK_ID				,
	TRN_ID					,
	INSTOCKNO_CD			,
	TYPEINSTOCK_CD			,
	TYPESTOCK_CD			,
	START_DT				,
	END_DT					,
	STINSTOCK_ID			,
	-- 入荷予定
	ARRIV_ID				,
	-- 計上
	APPLYINSTOCKNPCS_NR		,
	APPLYINSTOCK_DT			,
	APPLYINSTOCKUSER_CD		,
	APPLYINSTOCKUSER_TX		,
	SKU_CD					,
	LOT_TX					,
	KANBAN_CD				,
	CASEPERPALLET_NR		,
	PALLETLAYER_NR			,
	PALLETCAPACITY_NR		,
	STACKINGNUMBER_NR		,
	TYPEPLALLET_CD			,
	STOCK_DT				,
	AMOUNT_NR				,
	PALLET_NR				,
	CASE_NR					,
	BARA_NR					,
	PCS_NR					,
	-- 先ロケ
	DESTTYPESTOCK_CD		,
	DESTLOCA_CD				,
	RESULTLOCA_CD			,
	SHIPVOLUME_CD			,
	DESTSTART_DT			,
	DESTEND_DT				,
	DESTHT_ID				,
	DESTHTUSER_CD			,
	DESTHTUSER_TX			,
	DESTLOCASTOCKC_ID		,
	-- 印刷
	PRTCOUNT_NR				,
	PRT_DT					,
	PRTUSER_CD				,
	PRTUSER_TX				,
	BATCHNO_CD				,
	--
	TYPEAPPLY_CD			,
	TYPEBLOCK_CD			,
	TYPECARRY_CD			,
	-- 入荷実績
	ARRIVNEEDSEND_FL		,
	ARRIVSENDEND_FL			,
	ARRIVSEND_DT			,
	--
	INSTOCKNEEDSEND_FL		,
	INSTOCKSENDEND_FL		,
	INSTOCKSEND_DT			,
	-- 管理項目
	UPD_DT					,
	UPDUSER_CD				,
	UPDUSER_TX				,
	ADD_DT					,
	ADDUSER_CD				,
	ADDUSER_TX				,
	UPDPROGRAM_CD			,
	UPDCOUNTER_NR			,
	STRECORD_ID				
)
SELECT
	INSTOCK_ID				,
	TRN_ID					,
	INSTOCKNO_CD			,
	TYPEINSTOCK_CD			,
	TYPESTOCK_CD			,
	START_DT				,
	END_DT					,
	STINSTOCK_ID			,
	-- 入荷予定
	ARRIV_ID				,
	-- 計上
	APPLYINSTOCKNPCS_NR		,
	APPLYINSTOCK_DT			,
	APPLYINSTOCKUSER_CD		,
	APPLYINSTOCKUSER_TX		,
	SKU_CD					,
	LOT_TX					,
	KANBAN_CD				,
	CASEPERPALLET_NR		,
	PALLETLAYER_NR			,
	0						,
	STACKINGNUMBER_NR		,
	TYPEPLALLET_CD			,
	STOCK_DT				,
	AMOUNT_NR				,
	0						,
	CASE_NR					,
	BARA_NR					,
	PCS_NR					,
	-- 先ロケ
	DESTTYPESTOCK_CD		,
	DESTLOCA_CD				,
	RESULTLOCA_CD			,
	SHIPVOLUME_CD			,
	DESTSTART_DT			,
	DESTEND_DT				,
	DESTHT_ID				,
	DESTHTUSER_CD			,
	DESTHTUSER_TX			,
	DESTLOCASTOCKC_ID		,
	-- 印刷
	PRTCOUNT_NR				,
	PRT_DT					,
	PRTUSER_CD				,
	PRTUSER_TX				,
	BATCHNO_CD				,
	--
	TYPEAPPLY_CD			,
	TYPEBLOCK_CD			,
	TYPECARRY_CD			,
	-- 入荷実績
	ARRIVNEEDSEND_FL		,
	ARRIVSENDEND_FL			,
	ARRIVSEND_DT			,
	--
	INSTOCKNEEDSEND_FL		,
	INSTOCKSENDEND_FL		,
	INSTOCKSEND_DT			,
	-- 管理項目
	UPD_DT					,
	UPDUSER_CD				,
	UPDUSER_TX				,
	ADD_DT					,
	ADDUSER_CD				,
	ADDUSER_TX				,
	UPDPROGRAM_CD			,
	UPDCOUNTER_NR			,
	STRECORD_ID				
FROM
	BK_TA_INSTOCK
;


--6.件数チェック
SELECT COUNT(*) FROM TA_INSTOCK;
SELECT COUNT(*) FROM BK_TA_INSTOCK;


--7.問題なければ確定
COMMIT;

--8.バックアップテーブル削除
DROP TABLE BK_TA_INSTOCK;
