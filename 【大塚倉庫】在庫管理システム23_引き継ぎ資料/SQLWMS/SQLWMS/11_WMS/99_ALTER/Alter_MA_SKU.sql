--1.コピー元テーブル作成
DROP TABLE BK_MA_SKU;
CREATE TABLE BK_MA_SKU AS
SELECT * FROM MA_SKU
;

--2.件数チェック
SELECT COUNT(*) FROM MA_SKU;
SELECT COUNT(*) FROM BK_MA_SKU;

--3.元テーブルの削除
DROP TABLE MA_SKU; 

--4.入替用新規テーブル作成
--
spool C:\SQL\OTID\SQLWMS\11_WMS\10_MA\CREATE.log

REM---------------------                                    TABLESPACE          PCTFREE   PCTUSED   INITIAL   NEXT      NAME
@C:\SQL\OTID\SQLWMS\11_WMS\10_MA\MA_SKU;

spool off;
--

--************************************************
-- 出荷指示データ明細
--************************************************
--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------

--5.バックアップテーブルから入替用新規テーブルにインサート

INSERT INTO MA_SKU (
	SKU_CD						,
	WAREHOUSESKU_CD				,
	EXTERIOR_CD					,
	SIZE_TX						,
	SKUBARCODE_TX				,
	SKUBARCODEBOARD_TX			,
	SKUBARCODEBARA_TX			,
	ITFCODE_TX					,
	SKU_TX						,
	SKU2_TX						,
	MANUFACTURERSKU_TX			,
	OWNER_CD					,
	TYPEBILL1_CD				,
	TYPEBILL2_CD				,
	GS1_CD						,
	PCSPERCASE_NR				,
	DEPOISTPRICE_NR				,
	STORAGECHARGEA_NR			,
	STORAGECHARGEB_NR			,
	STEVEDORAGEA_NR				,
	STEVEDORAGEB_NR				,
	NESTAINERINSTOCKRATE_NR		,
	CASESTOCKRATE_NR			,
	TYPEPACKAGING_CD			,
	LENGTH_NR					,
	WIDTH_NR					,
	HEIGHT_NR					,
	WEIGHT_NR					,
	ABC_ID						,
	ABCSUB_ID					,
	-- 管理項目                 ,
	UPD_DT						,
	UPDUSER_CD					,
	UPDUSER_TX					,
	ADD_DT						,
	ADDUSER_CD					,
	ADDUSER_TX					,
	UPDPROGRAM_CD				,
	UPDCOUNTER_NR				,
	STRECORD_ID					
)
SELECT
	SKU_CD						,
	WAREHOUSESKU_CD				,
	EXTERIOR_CD					,
	SIZE_TX						,
	SKUBARCODE_TX				,
	SKUBARCODEBOARD_TX			,
	SKUBARCODEBARA_TX			,
	ITFCODE_TX					,
	SKU_TX						,
	SKU2_TX						,
	MANUFACTURERSKU_TX			,
	OWNER_CD					,
	TYPEBILL1_CD				,
	TYPEBILL2_CD				,
	GS1_CD						,
	PCSPERCASE_NR				,
	DEPOISTPRICE_NR				,
	STORAGECHARGEA_NR			,
	STORAGECHARGEB_NR			,
	STEVEDORAGEA_NR				,
	STEVEDORAGEB_NR				,
	NESTAINERINSTOCKRATE_NR		,
	CASESTOCKRATE_NR			,
	TYPEPACKAGING_CD			,
	LENGTH_NR					,
	WIDTH_NR					,
	HEIGHT_NR					,
	WEIGHT_NR					,
	0 ABC_ID					,
	CASE
		WHEN WEIGHT_NR >= 6000 THEN 2	-- 下段
		ELSE 1							-- 上段
	END AS ABCSUB_ID			,
	-- 管理項目                 ,
	UPD_DT						,
	UPDUSER_CD					,
	UPDUSER_TX					,
	ADD_DT						,
	ADDUSER_CD					,
	ADDUSER_TX					,
	UPDPROGRAM_CD				,
	UPDCOUNTER_NR				,
	STRECORD_ID					
FROM
	BK_MA_SKU
;


--6.件数チェック
SELECT COUNT(*) FROM MA_SKU;
SELECT COUNT(*) FROM BK_MA_SKU;


--7.問題なければ確定
COMMIT;

--8.バックアップテーブル削除
DROP TABLE BK_MA_SKU;
