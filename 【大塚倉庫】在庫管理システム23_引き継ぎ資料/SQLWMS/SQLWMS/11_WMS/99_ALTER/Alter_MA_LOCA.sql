--1.コピー元テーブル作成
DROP TABLE BK_MA_LOCA;
CREATE TABLE BK_MA_LOCA AS
SELECT * FROM MA_LOCA
;

--2.件数チェック
SELECT COUNT(*) FROM MA_LOCA;
SELECT COUNT(*) FROM BK_MA_LOCA;

--3.元テーブルの削除
DROP TABLE MA_LOCA; 

--4.入替用新規テーブル作成
--
spool C:\SQL\OTID\SQLWMS\11_WMS\10_MA\CREATE.log

REM---------------------                                    TABLESPACE          PCTFREE   PCTUSED   INITIAL   NEXT      NAME
@C:\SQL\OTID\SQLWMS\11_WMS\10_MA\MA_LOCA;

spool off;
--

--************************************************
-- 出荷指示データ明細
--************************************************
--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------

--5.バックアップテーブルから入替用新規テーブルにインサート

INSERT INTO MA_LOCA (
	LOCA_CD				,		
	LOCA_TX				,		
	WH_CD				,		
	FLOOR_CD			,		
	AREA_CD				,		
	LINE_CD				,		
	SHELF_CD			,		
	LAYER_CD			,		
	ORDER_CD			,		
	TYPELOCA_CD			,		
	TYPELOCABLOCK_CD	,		
	LINEUNIT_CD			,		
	LINEUNIT_NR			,		
	HEQUIP_CD			,		
	CASE_CD				,		
	PALLETDIV_NR		,		
	SHIPVOLUME_CD		,		
	ABC_ID				,		
	ABCSUB_ID			,		
	ARRIVPRIORITY_NR	,		
	SHIPPRIORITY_NR		,		
	INVENT_FL			,		
	NOTUSE_FL			,		
	NOTSHIP_FL			,		
	-- 管理項目         ,        
	UPD_DT				,		
	UPDUSER_CD			,		
	UPDUSER_TX			,		
	ADD_DT				,		
	ADDUSER_CD			,		
	ADDUSER_TX			,		
	UPDPROGRAM_CD		,		
	UPDCOUNTER_NR		,		
	STRECORD_ID					
)
SELECT
  	LOCA_CD				,		
	LOCA_TX				,		
	WH_CD				,		
	FLOOR_CD			,		
	AREA_CD				,		
	LINE_CD				,		
	SHELF_CD			,		
	LAYER_CD			,		
	ORDER_CD			,		
	TYPELOCA_CD			,		
	TYPELOCABLOCK_CD	,		
	LINEUNIT_CD			,		
	LINEUNIT_NR			,		
	HEQUIP_CD			,		
	CASE_CD				,		
	PALLETDIV_NR		,		
	SHIPVOLUME_CD		,		
	99 ABC_ID			,		
	2 ABCSUB_ID			,		
	ARRIVPRIORITY_NR	,		
	SHIPPRIORITY_NR		,		
	INVENT_FL			,		
	NOTUSE_FL			,		
	NOTSHIP_FL			,		
	-- 管理項目         ,        
	UPD_DT				,		
	UPDUSER_CD			,		
	UPDUSER_TX			,		
	ADD_DT				,		
	ADDUSER_CD			,		
	ADDUSER_TX			,		
	UPDPROGRAM_CD		,		
	UPDCOUNTER_NR		,		
	STRECORD_ID					
FROM
	BK_MA_LOCA
;


--6.件数チェック
SELECT COUNT(*) FROM MA_LOCA;
SELECT COUNT(*) FROM BK_MA_LOCA;


--7.問題なければ確定
COMMIT;

--8.バックアップテーブル削除
DROP TABLE BK_MA_LOCA;
