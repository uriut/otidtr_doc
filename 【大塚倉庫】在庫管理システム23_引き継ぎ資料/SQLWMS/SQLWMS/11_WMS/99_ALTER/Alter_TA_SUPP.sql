--1.コピー元テーブル作成
CREATE TABLE BK_TA_SUPP AS
SELECT * FROM TA_SUPP
;

--2.件数チェック
SELECT COUNT(*) FROM TA_SUPP;
SELECT COUNT(*) FROM BK_TA_SUPP;

--3.元テーブルの削除
DROP TABLE TA_SUPP; 

--4.入替用新規テーブル作成
--
spool C:\SQL\OTID\SQLWMS\11_WMS\20_T\CREATE.log

REM---------------------                                    TABLESPACE          PCTFREE   PCTUSED   INITIAL   NEXT      NAME
@C:\SQL\OTID\SQLWMS\11_WMS\20_T\TA_SUPP;

spool off;
--

--************************************************
-- 出荷指示データ明細
--************************************************
--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------

--5.バックアップテーブルから入替用新規テーブルにインサート

INSERT INTO TA_SUPP (
	SUPP_ID				,
	TRN_ID				,
	SUPPNO_CD			,
	TYPESUPP_CD			,
	START_DT			,
	END_DT				,
	STSUPP_ID			,
	-- 出荷指示         ,
	SHIPH_ID			,
	SHIPD_ID			,
	-- ピック           ,
	PICK_ID				,
	--                  ,
	SKU_CD				,
	LOT_TX				,
	KANBAN_CD			,
	CASEPERPALLET_NR	,
	PALLETLAYER_NR		,
	PALLETCAPACITY_NR	,
	STACKINGNUMBER_NR	,
	TYPEPLALLET_CD		,
	USELIMIT_DT			,
	STOCK_DT			,
	AMOUNT_NR			,
	PALLET_NR			,
	CASE_NR				,
	BARA_NR				,
	PCS_NR				,
	-- 元ロケ           ,
	SRCTYPESTOCK_CD		,
	SRCLOCA_CD			,
	SRCSTART_DT			,
	SRCEND_DT			,
	SRCHT_ID			,
	SRCHTUSER_CD		,
	SRCHTUSER_TX		,
	SRCLOCASTOCKC_ID	,
	-- 先ロケ           ,
	DESTTYPESTOCK_CD	,
	DESTLOCA_CD			,
	DESTSTART_DT		,
	DESTEND_DT			,
	DESTHT_ID			,
	DESTHTUSER_CD		,
	DESTHTUSER_TX		,
	DESTLOCASTOCKC_ID	,
	-- 印刷             ,
	PRTCOUNT_NR			,
	PRT_DT				,
	PRTUSER_CD			,
	PRTUSER_TX			,
	--                  ,
	BATCHNO_CD			,
	TYPEAPPLY_CD		,
	TYPEBLOCK_CD		,
	TYPECARRY_CD		,
	-- 管理項目         ,
	UPD_DT				,
	UPDUSER_CD			,
	UPDUSER_TX			,
	ADD_DT				,
	ADDUSER_CD			,
	ADDUSER_TX			,
	UPDPROGRAM_CD		,
	UPDCOUNTER_NR		,
	STRECORD_ID			
)
SELECT
	SUPP_ID				,
	TRN_ID				,
	SUPPNO_CD			,
	TYPESUPP_CD			,
	START_DT			,
	END_DT				,
	STSUPP_ID			,
	-- 出荷指示         ,
	SHIPH_ID			,
	SHIPD_ID			,
	-- ピック           ,
	PICK_ID				,
	--                  ,
	SKU_CD				,
	LOT_TX				,
	KANBAN_CD			,
	CASEPERPALLET_NR	,
	PALLETLAYER_NR		,
	0					,
	STACKINGNUMBER_NR	,
	TYPEPLALLET_CD		,
	USELIMIT_DT			,
	STOCK_DT			,
	AMOUNT_NR			,
	0					,
	CASE_NR				,
	BARA_NR				,
	PCS_NR				,
	-- 元ロケ           ,
	SRCTYPESTOCK_CD		,
	SRCLOCA_CD			,
	SRCSTART_DT			,
	SRCEND_DT			,
	SRCHT_ID			,
	SRCHTUSER_CD		,
	SRCHTUSER_TX		,
	SRCLOCASTOCKC_ID	,
	-- 先ロケ           ,
	DESTTYPESTOCK_CD	,
	DESTLOCA_CD			,
	DESTSTART_DT		,
	DESTEND_DT			,
	DESTHT_ID			,
	DESTHTUSER_CD		,
	DESTHTUSER_TX		,
	DESTLOCASTOCKC_ID	,
	-- 印刷             ,
	PRTCOUNT_NR			,
	PRT_DT				,
	PRTUSER_CD			,
	PRTUSER_TX			,
	--                  ,
	BATCHNO_CD			,
	TYPEAPPLY_CD		,
	TYPEBLOCK_CD		,
	TYPECARRY_CD		,
	-- 管理項目         ,
	UPD_DT				,
	UPDUSER_CD			,
	UPDUSER_TX			,
	ADD_DT				,
	ADDUSER_CD			,
	ADDUSER_TX			,
	UPDPROGRAM_CD		,
	UPDCOUNTER_NR		,
	STRECORD_ID			
FROM
	BK_TA_SUPP
;


--6.件数チェック
SELECT COUNT(*) FROM TA_SUPP;
SELECT COUNT(*) FROM BK_TA_SUPP;


--7.問題なければ確定
COMMIT;

--8.バックアップテーブル削除
DROP TABLE BK_TA_SUPP;
