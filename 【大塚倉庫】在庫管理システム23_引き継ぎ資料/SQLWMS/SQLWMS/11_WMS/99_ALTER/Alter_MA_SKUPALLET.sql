--1.コピー元テーブル作成
CREATE TABLE BK_MA_SKUPALLET AS
SELECT * FROM MA_SKUPALLET
;

--2.件数チェック
SELECT COUNT(*) FROM MA_SKUPALLET;
SELECT COUNT(*) FROM BK_MA_SKUPALLET;

--3.元テーブルの削除
DROP TABLE MA_SKUPALLET; 

--4.入替用新規テーブル作成
--
spool C:\SQL\OTID\SQLWMS\11_WMS\20_T\CREATE.log

REM---------------------                                    TABLESPACE          PCTFREE   PCTUSED   INITIAL   NEXT      NAME
@C:\SQL\OTID\SQLWMS\11_WMS\20_T\MA_SKUPALLET;

spool off;
--

--************************************************
-- 出荷指示データ明細
--************************************************
--------------------------------------------------
--              一意キー設定
--------------------------------------------------

--------------------------------------------------
--              インデックス設定
--------------------------------------------------

--5.バックアップテーブルから入替用新規テーブルにインサート

INSERT INTO MA_SKUPALLET (
	ID					,
	SKU_CD				,
	MANUFACTURER_CD		,
	CASEPERPALLET_NR	,
	PALLETLAYER_NR		,
	CASEPERLAYER_NR		,
	PALLETCAPACITY_NR	,
	STACKINGNUMBER_NR	,
	-- 管理項目
	UPD_DT				,
	UPDUSER_CD			,
	UPDUSER_TX			,
	ADD_DT				,
	ADDUSER_CD			,
	ADDUSER_TX			,
	UPDPROGRAM_CD		,
	UPDCOUNTER_NR		,
	STRECORD_ID			
)
SELECT
	BK.ID					,
	MS.SKU_CD				,
	BK.MANUFACTURER_CD		,
	BK.CASEPERPALLET_NR		,
	BK.PALLETLAYER_NR		,
	BK.CASEPERLAYER_NR		,
	BK.PALLETLAYER_NR * BK.CASEPERLAYER_NR	,
	BK.STACKINGNUMBER_NR	,
	-- 管理項目
	BK.UPD_DT				,
	BK.UPDUSER_CD			,
	BK.UPDUSER_TX			,
	BK.ADD_DT				,
	BK.ADDUSER_CD			,
	BK.ADDUSER_TX			,
	BK.UPDPROGRAM_CD		,
	BK.UPDCOUNTER_NR		,
	BK.STRECORD_ID			
FROM
	BK_MA_SKUPALLET BK,
	MA_SKU MS
WHERE
	BK.SKU_CD = SUBSTR(MS.SKU_CD,4,20)
;


--6.件数チェック
SELECT COUNT(*) FROM MA_SKUPALLET;
SELECT COUNT(*) FROM BK_MA_SKUPALLET;


--7.問題なければ確定
COMMIT;

--8.バックアップテーブル削除
DROP TABLE BK_MA_SKUPALLET;
