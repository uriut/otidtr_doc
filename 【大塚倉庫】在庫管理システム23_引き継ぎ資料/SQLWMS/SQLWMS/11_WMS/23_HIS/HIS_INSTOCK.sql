-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 入庫データ（履歴）
--************************************************
--------------------------------------------------
--              テーブル作成
--------------------------------------------------
DROP TABLE HIS_INSTOCK CASCADE CONSTRAINTS;
CREATE TABLE HIS_INSTOCK
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	INSTOCK_ID					NUMBER(10,0)								NOT NULL,			-- 入庫ID
	TRN_ID						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- トランザクションID
	INSTOCKNO_CD				VARCHAR2(20)								NOT NULL,			-- 入庫指示NO
	TYPEINSTOCK_CD				VARCHAR2(2)			DEFAULT '10'			NOT NULL,			-- 入庫タイプコード
	TYPESTOCK_CD				VARCHAR2(2)			DEFAULT '10'			NOT NULL,			-- 在庫状態タイプコード '10'良品・'20'不良品・'40'返品
	START_DT					DATE				DEFAULT NULL					,			-- 開始日
	END_DT						DATE				DEFAULT NULL					,			-- 終了日
	STINSTOCK_ID				NUMBER(2,0)			DEFAULT -1				NOT NULL,			-- 入庫ステータス
	-- 入荷予定
	ARRIV_ID					NUMBER(10,0)								NOT NULL,			-- 入荷予定ID
	-- 計上
	APPLYINSTOCKNPALLET_NR		NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 計上パレット数
	APPLYINSTOCKNCASE_NR		NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 計上ケース数
	APPLYINSTOCKNBARA_NR		NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 計上バラ数
	APPLYINSTOCKNPCS_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 計上数
	APPLYINSTOCK_DT				DATE				DEFAULT SYSDATE					,			-- 計上日時
	APPLYINSTOCKUSER_CD			VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- 計上社員コード
	APPLYINSTOCKUSER_TX			VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- 計上社員名
	SKU_CD						VARCHAR2(40)								NOT NULL,			-- SKUコード
	LOT_TX						VARCHAR2(40)								NOT NULL,			-- ロット
	KANBAN_CD					VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- かんばん番号
	CASEPERPALLET_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積み付け数
	PALLETLAYER_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット段数
	PALLETCAPACITY_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積載数（パレット段数*パレット回し数）　2014/04/17追加
	STACKINGNUMBER_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 保管段数
	TYPEPLALLET_CD				VARCHAR(10)			DEFAULT ' '				NOT NULL,			-- パレットタイプ
	STOCK_DT					DATE				DEFAULT '2000-01-01'	NOT NULL,			-- 入庫日 
	AMOUNT_NR					NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 入数 標準ケース・メーカー箱の入数
	PALLET_NR					NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- パレット数　2014/04/17追加
	CASE_NR						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ケース数
	BARA_NR						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- バラ数
	PCS_NR						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- ＰＣＳ
	-- 先ロケ
	DESTTYPESTOCK_CD			VARCHAR2(2)			DEFAULT '10'			NOT NULL,			-- 在庫タイプコード
	DESTLOCA_CD					VARCHAR2(20)		DEFAULT ''						,			-- 先ロケーションコード 
	RESULTLOCA_CD				VARCHAR2(20)		DEFAULT ''						,			-- 入庫結果ロケーションコード
	SHIPVOLUME_CD				VARCHAR2(10)		DEFAULT ''						,			-- 出荷荷姿コード
	DESTSTART_DT				DATE				DEFAULT NULL					,			-- 先開始時刻 
	DESTEND_DT					DATE				DEFAULT NULL					,			-- 先終了時刻 
	DESTHT_ID					NUMBER(6,0)			DEFAULT 0				NOT NULL,			-- 先HT
	DESTHTUSER_CD				VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- 先HT社員コード
	DESTHTUSER_TX				VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- 先HT社員名
	DESTLOCASTOCKC_ID			NUMBER(10,0)								NOT NULL,
	-- 印刷
	PRTCOUNT_NR					NUMBER(9,0)			DEFAULT 0						,			-- 印刷回数
	PRT_DT						DATE				DEFAULT NULL					,			-- 印刷日
	PRTUSER_CD					VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- 印刷社員コード 
	PRTUSER_TX					VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- 印刷社員名
	BATCHNO_CD					VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- バッチＮｏ． 
	--
	TYPEAPPLY_CD				VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- 引当タイプコード
	TYPEBLOCK_CD				VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- ブロックタイプ
	TYPECARRY_CD				VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- 運搬タイプコード 
	-- 入荷実績
	ARRIVNEEDSEND_FL			NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 送信フラグ（1：送信対象）
	ARRIVSENDEND_FL				NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 送信完了フラグ（1：送信完了）
	ARRIVSEND_DT				DATE				DEFAULT NULL					,			-- 送信日時
	--
	INSTOCKNEEDSEND_FL			NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 送信フラグ（1：送信対象）
	INSTOCKSENDEND_FL			NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 送信完了フラグ（1：送信完了）
	INSTOCKSEND_DT				DATE				DEFAULT NULL					,			-- 送信日時
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザコード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザ名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザコード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザ名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL,			-- レコード状態（-2:削除、-1:作成中、0:通常）
	REGIST_DT					DATE										NOT NULL			-- 履歴登録日時
)
;

--------------------------------------------------
--				一意キー設定
--------------------------------------------------
ALTER TABLE HIS_INSTOCK
	ADD CONSTRAINT PK_HIS_INSTOCK
	PRIMARY KEY(INSTOCK_ID)
;
--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_INSTOCKNO_HIS ON HIS_INSTOCK
(
	INSTOCKNO_CD
)
;

CREATE INDEX IX_SKU_HIS ON HIS_INSTOCK
(
	SKU_CD
)
;

CREATE INDEX IX_DESTLOCA_HIS ON HIS_INSTOCK
(
	DESTLOCA_CD
)
;
