-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 補充(履歴)
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE HIS_SUPP CASCADE CONSTRAINTS;
CREATE TABLE HIS_SUPP
(-- COLUMN					TYPE				DEFAULT					NULL				COMMENT
	SUPP_ID					NUMBER(10,0)								NOT NULL,			-- 補充ID
	TRN_ID					NUMBER(10,0)								NOT NULL,			-- トランザクションID
	SUPPNO_CD				VARCHAR2(10)								NOT NULL,			-- 補充指示NO
	TYPESUPP_CD				VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- 補充タイプコード（10：緊急、20：一次ピック）
	START_DT				DATE				DEFAULT NULL					,			-- 開始日
	END_DT					DATE				DEFAULT NULL					,			-- 終了日
	STSUPP_ID				NUMBER(2,0)			DEFAULT -1				NOT NULL,			-- 補充ステータス
	-- 出荷指示
	SHIPH_ID				NUMBER(10,0)								NOT NULL,			-- 出荷指示ヘッダID
	SHIPD_ID				NUMBER(10,0)								NOT NULL,			-- 出荷指示明細ID
	-- ピック
	PICK_ID					NUMBER(10,0)								NOT NULL,           -- ピックID
	--
	SKU_CD					VARCHAR2(40)								NOT NULL,			-- SKUコード
	LOT_TX					VARCHAR2(40)		DEFAULT ' '				NOT NULL,			-- ロット
	SRCKANBAN_CD			VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- かんばん番号(補充元)
	DESTKANBAN_CD			VARCHAR2(20)		DEFAULT NULL					,			-- かんばん番号(補充先)
	CASEPERPALLET_NR		NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積み付け数
	PALLETLAYER_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット段数
	PALLETCAPACITY_NR		NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積載数（パレット段数*パレット回し数）　2014/04/17追加
	STACKINGNUMBER_NR		NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 保管段数
	TYPEPLALLET_CD			VARCHAR2(10)		DEFAULT ' '				NOT NULL,			-- パレットタイプ
	USELIMIT_DT				DATE				DEFAULT '2099-01-01'	NOT NULL,			-- 使用期限
	STOCK_DT				DATE				DEFAULT '2000-01-01'	NOT NULL,			-- 入庫日
	AMOUNT_NR				NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 入数 標準ケース・メーカー箱の入数
	PALLET_NR				NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- パレット数
	CASE_NR					NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ケース数
	BARA_NR					NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- バラ数
	PCS_NR					NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- ＰＣＳ
	-- 元ロケ
	SRCTYPESTOCK_CD			VARCHAR2(10)		DEFAULT '00'			NOT NULL,			-- 在庫タイプコード
	SRCLOCA_CD				VARCHAR2(21)		DEFAULT ' '				NOT NULL,			-- 元ロケーションコード
	SRCSTART_DT				DATE				DEFAULT NULL					,			-- 元開始時刻
	SRCEND_DT				DATE				DEFAULT NULL					,			-- 元終了時刻
	SRCHT_ID				NUMBER(6,0)			DEFAULT 0				NOT NULL,			-- 元HT
	SRCHTUSER_CD			VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- 元HT社員コード
	SRCHTUSER_TX			VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- 元HT社員名
	SRCLOCASTOCKC_ID		NUMBER(10,0)								NOT NULL,			-- 元引当済み在庫ID
	SRCPALLET_NR			NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ピック完了パレット数
	SRCCASE_NR				NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ピック完了ケース数
	SRCBARA_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- ピック完了バラ数
	SRCPCS_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- ピック完了ＰＣＳ
	-- 先ロケ
	DESTTYPESTOCK_CD		VARCHAR2(10)		DEFAULT '00'			NOT NULL,			-- 在庫タイプコード
	DESTLOCA_CD				VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- 先ロケーションコード
	DESTSTART_DT			DATE				DEFAULT NULL					,			-- 先開始時刻
	DESTEND_DT				DATE				DEFAULT NULL					,			-- 先終了時刻
	DESTHT_ID				NUMBER(6,0)			DEFAULT 0				NOT NULL,			-- 先HT
	DESTHTUSER_CD			VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- 先HT社員コード
	DESTHTUSER_TX			VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- 先HT社員名
	DESTLOCASTOCKC_ID		NUMBER(10,0)								NOT NULL,			-- 先引当済み在庫ID
	DESTPALLET_NR			NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 補充完了パレット数
	DESTCASE_NR				NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 補充完了ケース数
	DESTBARA_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 補充完了バラ数
	DESTPCS_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 補充完了ＰＣＳ
	-- 印刷
	PRTCOUNT_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 印刷回数
	PRT_DT					DATE				DEFAULT NULL					,			-- 印刷日
	PRTUSER_CD				VARCHAR2(8)			DEFAULT ' '				NOT NULL,			-- 印刷社員コード
	PRTUSER_TX				VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- 印刷社員名
	--
	BATCHNO_CD				VARCHAR2(10)		DEFAULT ' '				NOT NULL,			-- バッチＮｏ．
	TYPEAPPLY_CD			VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- 引当タイプコード
	TYPEBLOCK_CD			VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- ブロックタイプ
	TYPECARRY_CD			VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- 運搬タイプコード
	-- 管理項目
	UPD_DT					DATE				DEFAULT NULL					,			-- 更新日時
	UPDUSER_CD				VARCHAR2(20)		DEFAULT ''						,			-- 更新社員コード
	UPDUSER_TX				VARCHAR2(50)		DEFAULT ''						,			-- 更新社員名
	ADD_DT					DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD				VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録社員コード
	ADDUSER_TX				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD			VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR			NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID				NUMBER(2,0)			DEFAULT 0				NOT NULL,			-- レコード状態（-2:削除、-1:作成中、0:通常）
	REGIST_DT				DATE										NOT NULL
)
;
--------------------------------------------------
--              一意キー設定
--------------------------------------------------
ALTER TABLE HIS_SUPP
	ADD CONSTRAINT PK_HIS_SUPP
	PRIMARY KEY(SUPP_ID)
;
