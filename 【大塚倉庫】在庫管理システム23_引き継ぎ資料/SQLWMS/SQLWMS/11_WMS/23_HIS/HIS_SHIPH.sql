-- Copyright (C) Seaos Corporation 2013 All rights reserved
-- ************************************************
-- 出荷指示ヘッダ(履歴)
-- ************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE HIS_SHIPH CASCADE CONSTRAINTS;
CREATE TABLE HIS_SHIPH
(-- COLUMN                      TYPE                DEFAULT             NULL                COMMENT
	SHIPH_ID					NUMBER(10,0)							NOT NULL,			-- ITF出荷指示データヘッダID
	TRN_ID						NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- トランザクションID
	TYPESHIP_CD					VARCHAR2(20)		DEFAULT 'SHIP'		NOT NULL,			-- 出荷指示タイプコード
	TYPESHIP_TX					VARCHAR2(50)		DEFAULT 'SHIP'		NOT NULL,			-- 出荷指示タイプ名（追加）
	START_DT					DATE				DEFAULT NULL				,			-- 開始日
	END_DT						DATE				DEFAULT NULL				,			-- 終了日
	STSHIP_ID					NUMBER(2,0)			DEFAULT 0			NOT NULL,			-- 出荷状態ID		引当完了,ピック中、検品完了、積込完了
	-- 出荷指示
	ORDERNO_CD					VARCHAR2(50)							NOT NULL,			-- 受注番号
	ORDERNOSUB_CD				VARCHAR2(20)		DEFAULT '0'			NOT NULL,			-- 枝番
	CUSTOMERORDERNO_CD			VARCHAR2(50)		DEFAULT NULL				,			-- 顧客側の発注番号
	CUSTOMER_CD					VARCHAR2(50)		DEFAULT NULL				,			-- 顧客番号：新規追加
	ORDERTYPE_CD				VARCHAR2(20)		DEFAULT '100'		NOT NULL,			-- 伝票種別
	ENTRYTYPE_CD				VARCHAR2(20)		DEFAULT '900'		NOT NULL,			-- 入力種別コード
	ORDERDATE_DT				DATE				DEFAULT NULL				,			-- 受注日時
	SALESSHOP_CD				VARCHAR2(20)		DEFAULT NULL				,			-- 受注店舗コード
	OWNER_CD					VARCHAR(120)							NOT NULL,			-- 荷主コード
	CHANNELTYPE_CD				VARCHAR2(20)		DEFAULT NULL				,			-- 販路種別コード
	TRANSPORTER1_CD				VARCHAR2(40)		DEFAULT ' '					,			-- 配送業者コード	HEAD1.SUCD1をセットする
	TRANSPORTER2_CD				VARCHAR2(40)		DEFAULT ' '					,			-- 配送業者コード2	HEAD1.SUCD2をセットする。作業単位は配送業者コード2
	DELIVERYDESTINATION_CD		VARCHAR2(40)		DEFAULT ' '					,			-- 納品先コード		HEAD2.NOHCDをセットする
	DTNAME_TX					VARCHAR2(180)							NOT NULL,			-- HEAD2.NOHCD-NM1をセットする
	DTPOSTAL_CD					VARCHAR2(10)		DEFAULT ' '			NOT NULL,			-- HEAD2.YUBINをセットする
	DTADDRESS1_TX				VARCHAR2(180)		DEFAULT ' '			NOT NULL,			-- HEAD2.NOHCD-JYUSYO1をセットする
	DTADDRESS2_TX				VARCHAR2(180)		DEFAULT ' '			NOT NULL,			-- HEAD2.NOHCD-JYUSYO2をセットする
	DTADDRESS3_TX				VARCHAR2(180)		DEFAULT ' '					,			-- 未使用
	DTADDRESS4_TX				VARCHAR2(180)		DEFAULT ' '					,			-- 未使用
	DIRECTDELIVERY_CD			VARCHAR2(40)		DEFAULT ' '					,			-- 直送先コード		ホストから来るHEAD2.CCD
	TYPEPLALLET_CD				VARCHAR2(10)		DEFAULT ' '					,			-- パレットタイプ	BODY1.flg1にセットされる
	SHIPBERTH_CD				VARCHAR2(40)		DEFAULT ' '					,			-- 出荷バース番号	出荷バース番号。車両管理システムで入力される
	SHIPPLAN_DT					DATE				DEFAULT NULL				,			-- 出荷予定時刻		出荷予定時刻。車両管理システムで入力される
	SLIPNO_TX					VARCHAR2(40)		DEFAULT ' '					,			-- 出荷伝票番号		一つの車番に対して複数の伝票が紐付くことがある。伝票番号はホストの中ではユニークではないので読みかえる必要がある
	CARRIER_CD					VARCHAR2(20)		DEFAULT '1001'		NOT NULL,			-- 配送キャリアコード
	DELITYPE_CD					VARCHAR2(20)		DEFAULT '100'		NOT NULL,			-- 配送種別コード
	DELIDATE_TX					VARCHAR2(20)		DEFAULT NULL				,			-- 着荷指定日		ホストのBODY1-TYUNO(納品日)をセットする
	DELITIME_CD					VARCHAR2(20)		DEFAULT '00'		NOT NULL,			-- 着荷指定時刻条件
	CHARGETYPE_CD				VARCHAR2(20)		DEFAULT ' '			NOT NULL,			-- 決済種別
	GIFTTYPE_CD					VARCHAR2(20)		DEFAULT NULL				,			-- ギフト種別
	TYPETEMPERATURE_CD			VARCHAR2(20)		DEFAULT NULL				,			-- 配送温度種別
	TOTALITEMPRICE_NR			NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 商品金額合計
	TOTALPRICE_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 請求金額
	TOTALTAX_NR					NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 税額
	DELICHARGE_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 送料
	GIFTCHARGE_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- ギフト手数料
	CODCHARGE_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 代引き手数料
	DISCOUNT_NR					NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 値引き額合計
	SALEDISCOUNT_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- セール値引き額
	COUPON_CD					VARCHAR2(200)		DEFAULT NULL				,			-- クーポンコード
	COUPONDISCOUNT_NR			NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- クーポン値引き額
	USEPOINT_NR					NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- ポイント利用額
	ADDPOINT_NR					NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- ポイント付与額
	CUSTOMERCOMMENT_TX			VARCHAR2(4000)		DEFAULT NULL				,			-- 顧客コメント
	CSCOMMENT_TX				VARCHAR2(4000)		DEFAULT NULL				,			-- CSコメント
	OPERATORCOMMENT_TX			VARCHAR2(4000)		DEFAULT NULL				,			-- 社内コメント
	DETCOUNT_NR					NUMBER(9,0)			DEFAULT 0			NOT NULL,			-- 明細数
	-- 納品書
	INVOICE_FL					NUMBER(1,0)			DEFAULT 0			NOT NULL,			-- 納品書有無フラグ
	INVOICECOMMENT_TX			VARCHAR2(4000)		DEFAULT NULL				,			-- 納品書印字用コメント
	PRTINVOICECOUNT_NR			NUMBER(9,0)			DEFAULT 0			NOT NULL,			-- 納品書印刷回数
	PRTINVOICE_DT				DATE				DEFAULT NULL				,			-- 納品書印刷日
	PRTINVOICEUSER_CD			VARCHAR2(8)			DEFAULT NULL				,			-- 納品書印刷社員コード
	PRTINVOICEUSER_TX			VARCHAR2(50)		DEFAULT NULL				,			-- 納品書印刷社員名
	-- 出荷伝票
	DELIBOX_FL					NUMBER(1,0)			DEFAULT 0			NOT NULL,			-- 出荷伝票宅配ボックスフラグ
	DELIITEM_TX					VARCHAR2(300)		DEFAULT NULL				,			-- 出荷伝票品名
	DELICOMMENT_TX				VARCHAR2(4000)		DEFAULT NULL				,			-- 出荷伝票印字用コメント
	PRTDELICOUNT_NR				NUMBER(9,0)			DEFAULT 0			NOT NULL,			-- 出荷伝票印刷回数
	PRTDELI_DT					DATE				DEFAULT NULL				,			-- 出荷伝票印刷日
	PRTDELIUSER_CD				VARCHAR2(8)			DEFAULT NULL				,			-- 出荷伝票印刷社員コード
	PRTDELIUSER_TX				VARCHAR2(50)		DEFAULT NULL				,			-- 出荷伝票印刷
	--
	PACKNO1_CD					VARCHAR2(20)		DEFAULT NULL				,			-- 同送品番号(1)
	PACKNO2_CD					VARCHAR2(20)		DEFAULT NULL				,			-- 同送品番号(2)
	PACKNO3_CD					VARCHAR2(20)		DEFAULT NULL				,			-- 同送品番号(3)
	PACKNO4_CD					VARCHAR2(20)		DEFAULT NULL				,			-- 同送品番号(4)
	PACKNO5_CD					VARCHAR2(20)		DEFAULT NULL				,			-- 同送品番号(5)
	PACKNO6_CD					VARCHAR2(20)		DEFAULT NULL				,			-- 同送品番号(6)
	PACKNO7_CD					VARCHAR2(20)		DEFAULT NULL				,			-- 同送品番号(7)
	PACKNO8_CD					VARCHAR2(20)		DEFAULT NULL				,			-- 同送品番号(8)
	PACKNO9_CD					VARCHAR2(20)		DEFAULT NULL				,			-- 同送品番号(9)
	PACKNO10_CD					VARCHAR2(20)		DEFAULT NULL				,			-- 同送品番号(10)
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 更新社員コード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 更新社員名
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員コード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0			NOT NULL,			-- レコード状態（-2:削除、-1:作成中、0:通常）
	REGIST_DT					DATE									NOT NULL
)
;
--------------------------------------------------
--              一意キー設定
--------------------------------------------------
ALTER TABLE HIS_SHIPH
	ADD CONSTRAINT PK_HIS_SHIPH
	PRIMARY KEY(SHIPH_ID)
;
--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_SKU_HSH ON HIS_SHIPH
(
    ORDERNO_CD
)
;
