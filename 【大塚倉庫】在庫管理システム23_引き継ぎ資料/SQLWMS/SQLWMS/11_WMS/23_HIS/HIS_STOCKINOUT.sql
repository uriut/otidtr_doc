-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 在庫受払(履歴)
--************************************************
--------------------------------------------------
--              テーブル作成
--------------------------------------------------
DROP TABLE HIS_STOCKINOUT CASCADE CONSTRAINTS;
CREATE TABLE HIS_STOCKINOUT
(-- COLUMN                  TYPE                DEFAULT             NULL                COMMENT
	STOCKINOUT_ID			NUMBER(10,0)								NOT NULL,	-- 在庫更新ID 
	WH_CD					VARCHAR2(3)			DEFAULT ' '				NOT NULL,	-- 倉庫コード
	TARGETWH_CD				VARCHAR2(3)			DEFAULT ' '				NOT NULL,	-- 対象倉庫コード（倉庫間移動の対象）
	LOCA_CD					VARCHAR2(20)								NOT NULL,	-- ロケーションコード 
	SKU_CD					VARCHAR2(40)								NOT NULL,	-- ＳＫＵコード 
	LOT_TX					VARCHAR2(40)		DEFAULT ' '				NOT NULL,
	KANBAN_CD				VARCHAR2(20)		DEFAULT ' '				NOT NULL,
	CASEPERPALLET_NR		NUMBER(9,0)			DEFAULT 0				NOT NULL,
	PALLETLAYER_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,
	PALLETCAPACITY_NR		NUMBER(9,0)			DEFAULT 0				NOT NULL,	-- パレット積載数（パレット段数*パレット回し数）　2014/04/17追加
	STACKINGNUMBER_NR		NUMBER(9,0)			DEFAULT 0				NOT NULL,
	TYPEPLALLET_CD			VARCHAR2(10)		DEFAULT ' '				NOT NULL,
	USELIMIT_DT				DATE				DEFAULT '2099-01-01'	NOT NULL,	-- 使用期限 
	BATCHNO_CD				VARCHAR2(10)		DEFAULT ' '				NOT NULL,	-- バッチNO
	STOCK_DT				DATE				DEFAULT '2000-01-01'	NOT NULL,	-- 入庫日 
	AMOUNT_NR				NUMBER(5,0)			DEFAULT 0				NOT NULL,	-- 入数 標準ケース・メーカー箱の入数
	PALLET_NR				NUMBER(5,0)			DEFAULT 0				NOT NULL,	-- パレット数 　2014/04/17
	CASE_NR					NUMBER(5,0)			DEFAULT 0				NOT NULL,	-- ケース数 
	BARA_NR					NUMBER(9,0)			DEFAULT 0				NOT NULL,	-- バラ数 
	PCS_NR					NUMBER(9,0)			DEFAULT 0				NOT NULL,	-- ＰＣＳ 
	TYPESTOCK_CD			VARCHAR2(2)			DEFAULT '00'			NOT NULL,	-- 在庫タイプコード 
	TYPELOCA_CD				VARCHAR2(10)		DEFAULT '00'			NOT NULL,	-- ロケーション種別コード
	TYPEOPERATION_CD		VARCHAR2(3)									NOT NULL,	-- 業務区分
	TYPEREF_CD				VARCHAR2(10)		DEFAULT ' '				NOT NULL,	-- 参照タイプ（システム上の参照先）
	REFNO_ID				NUMBER(10,0)		DEFAULT 0				NOT NULL,	-- 参照ID
	MEMO_TX					VARCHAR2(1024)		DEFAULT ' '						,	-- 備考
	--
	NEEDSEND_FL				NUMBER(1,0)			DEFAULT 0				NOT NULL,	-- 送信対象フラグ
	SENDEND_FL				NUMBER(1,0)			DEFAULT 0				NOT NULL,	-- 送信済みフラグ
	SEND_DT					DATE				DEFAULT NULL					,	-- 送信日時
	-- 管理項目
	UPD_DT					DATE				DEFAULT SYSDATE			NOT NULL,	-- 更新日時
	UPDUSER_CD				VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,	-- 更新社員コード
	UPDUSER_TX				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,	-- 更新社員名
	ADD_DT					DATE				DEFAULT SYSDATE			NOT NULL,	-- 登録日時
	ADDUSER_CD				VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,	-- 登録社員コード
	ADDUSER_TX				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,	-- 登録社員名
	UPDPROGRAM_CD			VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,	-- 更新プログラムコード
	UPDCOUNTER_NR			NUMBER(10,0)		DEFAULT 0				NOT NULL,	-- 更新カウンター
	STRECORD_ID				NUMBER(2,0)			DEFAULT 0				NOT NULL,	-- レコード状態（-2:削除、-1:作成中、0:通常）
	REGIST_DT				DATE										NOT NULL
)
;
--------------------------------------------------
--              一意キー設定
--------------------------------------------------
ALTER TABLE HIS_STOCKINOUT
	ADD CONSTRAINT PK_HIS_STOCKINOUT
	PRIMARY KEY(STOCKINOUT_ID)
;
