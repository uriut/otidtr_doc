-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- HTログ
--************************************************
--------------------------------------------------
--              テーブル作成
--------------------------------------------------
DROP TABLE TA_LOGHT CASCADE CONSTRAINTS;
CREATE TABLE TA_LOGHT
(-- COLUMN                      TYPE                DEFAULT             NULL                COMMENT
	LOGHT_ID					NUMBER(10,0)							NOT NULL,			-- HTログID 
	APP_CD						VARCHAR2(2)								NOT NULL,			-- アプリコード
	HT_ID						NUMBER(6,0)								NOT NULL,			-- HTID
	HTUSER_CD					VARCHAR2(8)								NOT NULL,			-- HT社員コード 
	TYPEHT_CD					VARCHAR2(2)								NOT NULL,			-- HT分類コード
	TYPEHTOPE_CD				VARCHAR2(2)								NOT NULL,			-- HT業務区分コード
	TYPEHTCMD_CD				VARCHAR2(2)								NOT NULL,			-- HTコマンド区分コード
	WR_CD						VARCHAR2(1)								NOT NULL,			-- ログ読み・書き
	DATA_TX						VARCHAR2(3000)							NOT NULL,			-- ログ
	-- 管理項目
	UPD_DT						DATE				DEFAULT NULL				,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT ' '			NOT NULL,			-- 更新社員コード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新社員名
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員コード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT ' '			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0			NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;
