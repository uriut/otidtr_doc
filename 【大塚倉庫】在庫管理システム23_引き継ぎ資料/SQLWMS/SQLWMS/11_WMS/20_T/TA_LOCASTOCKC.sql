-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 引当済み在庫
--************************************************
--------------------------------------------------
--              テーブル作成
--------------------------------------------------
DROP TABLE TA_LOCASTOCKC CASCADE CONSTRAINTS;
CREATE TABLE TA_LOCASTOCKC
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	LOCASTOCKC_ID				NUMBER(10,0)								NOT NULL,			-- 引当済み在庫ID
	TRN_ID						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- トランザクションID
	STSTOCK_ID					NUMBER(2,0)			DEFAULT -1				NOT NULL,			-- 在庫ステータス
	LOCA_CD						VARCHAR2(20)								NOT NULL,			-- ロケーションコード
	SKU_CD						VARCHAR2(40)								NOT NULL,			-- ＳＫＵコード
	LOT_TX						VARCHAR2(40)		DEFAULT ' '				NOT NULL,			-- ロット
	KANBAN_CD					VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- かんばん番号
	CASEPERPALLET_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積み付け数
	PALLETLAYER_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット段数
	PALLETCAPACITY_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積載数（パレット段数*パレット回し数）　2014/04/17追加
	STACKINGNUMBER_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 保管段数
	TYPEPLALLET_CD				VARCHAR2(10)		DEFAULT ' '				NOT NULL,			-- パレットタイプ
	USELIMIT_DT					DATE				DEFAULT '2099-01-01'	NOT NULL,			-- 使用期限 
    BATCHNO_CD					VARCHAR2(10)		DEFAULT ' '				NOT NULL,			-- バッチＮｏ．
	STOCK_DT					DATE				DEFAULT '2000-01-01'	NOT NULL,			-- 入庫日 
	AMOUNT_NR					NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 入数 標準ケース・メーカー箱の入数
	PALLET_NR					NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- パレット数
	CASE_NR						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ケース数
	BARA_NR						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- バラ数
	PCS_NR						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- ＰＣＳ
	TYPESTOCK_CD				VARCHAR2(2)			DEFAULT '10'			NOT NULL,			-- 在庫タイプコード
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新社員コード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新社員名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録社員コード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--              一意キー設定
--------------------------------------------------
ALTER TABLE TA_LOCASTOCKC
	ADD CONSTRAINT PK_TA_LOCASTOCKC
	PRIMARY KEY(LOCASTOCKC_ID)
;

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_LOCA_TLC ON TA_LOCASTOCKC
(
    LOCA_CD
)
;
CREATE INDEX IX_SKU_TLC ON TA_LOCASTOCKC
(
    SKU_CD
)
;
CREATE INDEX IX_KANBAN_TLC ON TA_LOCASTOCKC
(
    KANBAN_CD
)
;
