-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 引当済かんばん
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TA_KANBANP CASCADE CONSTRAINTS;
CREATE TABLE TA_KANBANP
(-- COLUMN					TYPE				DEFAULT					NULL				COMMENT
	KANBANP_ID				NUMBER(10,0)								NOT NULL,			-- 引当済かんばんID
	STKANBAN_CD				VARCHAR2(2)			DEFAULT '01'					,			-- かんばんステータス
	TYPEPKANBAN_CD			VARCHAR2(20)		DEFAULT ' '						,			-- かんばんタイプ
	KANBAN_CD				VARCHAR2(20)		DEFAULT ' '						,			-- かんばん番号
	WH_CD					VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- 倉庫コード
	FLOOR_CD				VARCHAR2(2)			DEFAULT ' '				NOT NULL,			-- フロアコード
	AREA_CD					VARCHAR2(3)			DEFAULT ' '				NOT NULL,			-- エリアコード
	LINE_CD					VARCHAR2(2)			DEFAULT ' '				NOT NULL,			-- 列コード
	SKU_CD					VARCHAR2(40)										,			-- 商品コード
	LOT_TX					VARCHAR2(40)										,			-- ロット
	CASEPERPALLET_NR		NUMBER(9,0)			DEFAULT 0						,			-- パレット積み付け数
	PALLETLAYER_NR			NUMBER(9,0)			DEFAULT 0						,			-- パレット段数
	STACKINGNUMBER_NR		NUMBER(9,0)			DEFAULT 0						,			-- 保管段数
	TYPEPLALLET_CD			VARCHAR2(10)										,			-- パレットタイプ
	-- 管理項目
	UPD_DT					DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD				VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新社員コード
	UPDUSER_TX				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新社員名
	ADD_DT					DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD				VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録社員コード
	ADDUSER_TX				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD			VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR			NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID				NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--              一意キー設定
--------------------------------------------------
ALTER TABLE TA_KANBANP
	ADD CONSTRAINT PK_TA_KANBANP
	PRIMARY KEY(KANBANP_ID)
;

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_KANBAN_TKP ON TA_KANBANP
(
    KANBAN_CD
)
;
