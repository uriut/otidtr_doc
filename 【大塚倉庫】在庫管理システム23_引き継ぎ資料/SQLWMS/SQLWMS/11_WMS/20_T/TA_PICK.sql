-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- ピッキング
--************************************************
--------------------------------------------------
--              テーブル作成
--------------------------------------------------
DROP TABLE TA_PICK CASCADE CONSTRAINTS;
CREATE TABLE TA_PICK
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	PICK_ID						NUMBER(10,0)								NOT NULL,			-- ピックID
	TRN_ID						NUMBER(10,0)								NOT NULL,			-- トランザクションID
	PICKNO_CD					VARCHAR2(20)								NOT NULL,			-- ピックNo.
	TYPEPICK_CD					VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- ピックタイプコード
	START_DT					DATE				DEFAULT NULL					,			-- 開始日
	END_DT						DATE				DEFAULT NULL					,			-- 終了日
	STPICK_ID					NUMBER(2,0)			DEFAULT -1				NOT NULL,			-- ピック状態ID
	-- 出荷指示
	SHIPH_ID					NUMBER(10,0)								NOT NULL,			-- 出荷指示ヘッダID
	SHIPD_ID					NUMBER(10,0)								NOT NULL,			-- 出荷指示明細ID
	ORDERNO_CD					VARCHAR2(20)								NOT NULL,			-- 受注番号　ラッピング対応用に追加
	ORDERNOSUB_CD				VARCHAR2(20)								NOT NULL,			-- 枝番　ラッピング対応用に追加
	ORDERDETNO_CD				VARCHAR2(20)								NOT NULL,			-- 受注明細番号　ラッピング対応用に追加
	-- SKU
	SKU_CD						VARCHAR2(20)								NOT NULL,			-- SKUコード
	LOT_TX						VARCHAR2(20)								NOT NULL,			-- ロット
	KANBAN_CD					VARCHAR2(20)								NOT NULL,			-- かんばん番号
	CASEPERPALLET_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積み付け数
	PALLETLAYER_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット段数
	PALLETCAPACITY_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積載数（パレット段数*パレット回し数）　2014/04/17追加
	STACKINGNUMBER_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 保管段数
	TYPEPLALLET_CD				VARCHAR2(20)		DEFAULT ''						,			-- パレットタイプ
	PALLETDETNO_CD				VARCHAR2(20)		DEFAULT ''						,			-- パレット明細番号
	TRANSPORTER_CD				VARCHAR2(40)		DEFAULT ''						,			-- パレット明細番号
	DELIVERYDESTINATION_CD		VARCHAR2(40)		DEFAULT ''						,			-- 納品先コード
	-- 軒先ピック対応
	DTNAME_TX					VARCHAR2(180)		DEFAULT ' '						,
	DTADDRESS1_TX				VARCHAR2(180)		DEFAULT ' '						,
	DTADDRESS2_TX				VARCHAR2(180)		DEFAULT ' '						,
	DTADDRESS3_TX				VARCHAR2(180)		DEFAULT ' '						,
	DTADDRESS4_TX				VARCHAR2(180)		DEFAULT ' '						,
	DIRECTDELIVERY_CD			VARCHAR2(40)		DEFAULT ''						,			-- 直送先コード
	DELIDATE_TX      			VARCHAR2(20)		DEFAULT ''						,           -- 着荷指定日   
	TOTALPIC_FL					NUMBER(1,0)			DEFAULT 1				NOT NULL,			-- トータルピックフラグ　トータルピック：1、軒先別：0
	--
	SHIPBERTH_CD				VARCHAR2(40)		DEFAULT ''						,			-- 出荷バース番号
	SHIPPLAN_DT					DATE				DEFAULT NULL					,			-- 出荷予定時刻	
	BERTHTIME_TX				VARCHAR2(12)		DEFAULT '0000'					,			-- 接車時間
	SLIPNO_TX					VARCHAR2(40)		DEFAULT ''						,			-- 出荷伝票番号
	--
	USELIMIT_DT					DATE				DEFAULT '2099-01-01'	NOT NULL,			-- 使用期限
	STOCK_DT					DATE				DEFAULT '2000-01-01'	NOT NULL,			-- 入庫日
	AMOUNT_NR					NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- 入数 標準ケース・メーカー箱の入数
	PALLET_NR					NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- パレット数　2014/04/17追加
	CASE_NR						NUMBER(5,0)			DEFAULT 0				NOT NULL,			-- ケース数
	BARA_NR						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- バラ数
	PCS_NR						NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- ＰＣＳ
	-- 元ロケ
	DEFER_FL					NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 保留中フラグ
	SRCTYPESTOCK_CD				VARCHAR2(10)		DEFAULT '00'			NOT NULL,			-- 在庫タイプコード
	SRCLOCA_CD					VARCHAR2(21)		DEFAULT ' '				NOT NULL,			-- 元ロケーションコード
	SRCSTART_DT					DATE				DEFAULT NULL					,			-- 元開始時刻
	SRCEND_DT					DATE				DEFAULT NULL					,			-- 元終了時刻
	SRCHT_ID					NUMBER(6,0)			DEFAULT 0				NOT NULL,			-- 元HT
	SRCHTUSER_CD				VARCHAR2(20)		DEFAULT ' '				NOT NULL,			-- 元HT社員コード
	SRCHTUSER_TX				VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- 元HT社員名
	SRCLOCASTOCKC_ID			NUMBER(10,0)								NOT NULL,			-- 元引当済み在庫ID
	-- 検品
	CHECK_FL					NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 検品フラグ
	CHECKCOUNT_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 検品回数
	CHECKSTART_DT				DATE				DEFAULT NULL					,			-- 検品開始時刻
	CHECKEND_DT					DATE				DEFAULT NULL					,			-- 検品終了時刻
	CHECKHT_ID					NUMBER(6,0)			DEFAULT 0				NOT NULL,			-- 検品ＨＴ
	CHECKHTUSER_CD				VARCHAR2(8)			DEFAULT ' '				NOT NULL,			-- 検品ＨＴ社員コード
	CHECKHTUSER_TX				VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- 検品ＨＴ社員名
	-- 出荷完了
	SHIPEND_FL					NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 出荷完了フラグ
	SHIPENDCOUNT_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 出荷完了回数
	SHIPENDSTART_DT				DATE				DEFAULT NULL					,			-- 出荷完了開始時刻
	SHIPENDEND_DT				DATE				DEFAULT NULL					,			-- 出荷完了終了時刻
	SHIPENDHT_ID				NUMBER(6,0)			DEFAULT 0				NOT NULL,			-- 出荷完了ＨＴ
	SHIPENDHTUSER_CD			VARCHAR2(8)			DEFAULT ' '				NOT NULL,			-- 出荷完了ＨＴ社員コード
	SHIPENDHTUSER_TX			VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- 出荷完了ＨＴ社員名
	-- 納品書
	PRTPACKLIST_FL				NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 納品書印刷フラグ
	-- 印刷
	PRINT_FL					NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 印刷待ちフラグ
	PRTCOUNT_NR					NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 印刷回数
	PRT_DT						DATE				DEFAULT NULL					,			-- 印刷日
	PRTUSER_CD					VARCHAR2(8)			DEFAULT ' '				NOT NULL,			-- 印刷社員コード
	PRTUSER_TX					VARCHAR2(50)		DEFAULT ' '				NOT NULL,			-- 印刷社員名
	--
	WRAPGROUP_ID				NUMBER(9,0)			DEFAULT 0						,
	WRAPAPPLY_NR				NUMBER(9,0)			DEFAULT 0						,
	--
	BATCHNO_CD					VARCHAR2(10)		DEFAULT ' '				NOT NULL,			-- バッチNO
	TYPEAPPLY_CD				VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- 引当タイプコード
	TYPEBLOCK_CD				VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- ブロックタイプ（未使用）
	TYPECARRY_CD				VARCHAR2(2)			DEFAULT '00'			NOT NULL,			-- 運搬タイプコード（00:補充無、10：緊急補充有）
	-- 管理項目
	UPD_DT						DATE				DEFAULT NULL					,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT ''						,			-- 更新社員コード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT ''						,			-- 更新社員名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録社員コード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;
--------------------------------------------------
--              一意キー設定
--------------------------------------------------
ALTER TABLE TA_PICK
	ADD CONSTRAINT PK_TA_PICK
	PRIMARY KEY(PICK_ID)
;

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_SKU_TAP ON TA_PICK
(
    SKU_CD
)
;

