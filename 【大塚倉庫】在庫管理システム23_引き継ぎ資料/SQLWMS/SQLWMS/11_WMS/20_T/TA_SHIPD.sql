-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 出荷指示明細
--************************************************
--------------------------------------------------
--				テーブル作成
--------------------------------------------------
DROP TABLE TA_SHIPD CASCADE CONSTRAINTS;
CREATE TABLE TA_SHIPD
(-- COLUMN                      TYPE                DEFAULT             NULL                COMMENT
	SHIPD_ID					NUMBER(10,0)							NOT NULL,			-- ID
	SHIPH_ID					NUMBER(10,0)							NOT NULL,			-- ヘッダID
	TRN_ID						NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- トランザクションID
	TYPESHIP_CD					VARCHAR2(20)		DEFAULT 'SHIP'		NOT NULL,			-- 出荷分類コード
	TYPESHIP_TX					VARCHAR2(50)		DEFAULT 'SHIP'		NOT NULL,			-- 出荷分類名（追加）
	START_DT					DATE				DEFAULT NULL				,			-- 開始日
	END_DT						DATE				DEFAULT NULL				,			-- 終了日
	STSHIP_ID					NUMBER(2,0)			DEFAULT 0			NOT NULL,			-- 出荷状態ID
	-- 出荷指示
	ORDERNO_CD					VARCHAR2(50)							NOT NULL,			-- 受注番号
	ORDERNOSUB_CD				VARCHAR2(20)		DEFAULT '0'			NOT NULL,			-- 枝番
	ORDERDETNO_CD				VARCHAR2(20)							NOT NULL,			-- 受注明細番号
	SKU_CD						VARCHAR2(40)							NOT NULL,			-- 商品コード
	LOT_TX						VARCHAR2(40)		DEFAULT ''					,			-- ロット
	KANBAN_CD					VARCHAR2(20)		DEFAULT ''					,			-- かんばん番号
	CASEPERPALLET_NR			NUMBER(9,0)			DEFAULT 0			NOT NULL,			-- パレット積み付け数
	PALLETLAYER_NR				NUMBER(9,0)			DEFAULT 0			NOT NULL,			-- パレット段数
	STACKINGNUMBER_NR			NUMBER(9,0)			DEFAULT 0			NOT NULL,			-- 保管段数
	SKU_TX						VARCHAR2(200)							NOT NULL,			-- 商品名
	TYPELOCA_CD					VARCHAR2(20)		DEFAULT '10'		NOT NULL,			-- ロケーション種別コード
	STOCKTYPE_CD				VARCHAR2(20)		DEFAULT '10'		NOT NULL,			-- 在庫種別コード
	PALLETCAPACITY_NR			NUMBER(9,0)			DEFAULT 0			NOT NULL,			-- パレット積載数
	ORDERPALLET_NR				NUMBER(5,0)			DEFAULT 0			NOT NULL,			-- 注文パレット数
	ORDERCASE_NR				NUMBER(5,0)			DEFAULT 0			NOT NULL,			-- 注文ケース数
	ORDERBARA_NR				NUMBER(9,0)			DEFAULT 0			NOT NULL,			-- 注文バラ数
	ORDERPCS_NR					NUMBER(10,0)							NOT NULL,			-- 注文数
	UNITPRICE_NR				NUMBER(10,2)		DEFAULT 0			NOT NULL,			-- 販売単価
	UNITADDPOINT_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 付与ポイント単価
	BONUS_FL					NUMBER(1,0)			DEFAULT 0			NOT NULL,			-- おまけフラグ
	UNITSALEDISCOUNT_NR			NUMBER(10,2)		DEFAULT 0			NOT NULL,			-- セール値引き額
	DETCOMMENT_TX				VARCHAR2(4000)		DEFAULT ' '			NOT NULL,			-- 明細コメント
	TYPESHIPAPPLY_CD			VARCHAR2(20)		DEFAULT 'APPLY'		NOT NULL,			-- 出荷引当種別コード
	TYPESHIPROUTE_CD			VARCHAR2(20)		DEFAULT 'WAREHOUSE'	NOT NULL,			-- 出荷経路種別コード
	SKUTYPE_CD					VARCHAR2(20)		DEFAULT 'GOODS'		NOT NULL,			-- 商品種別コード
	SUPPLYTYPE_CD				VARCHAR2(20)		DEFAULT 'DEFAULT'	NOT NULL,			-- 仕入種別コード
	SUPPLIER_CD					VARCHAR2(20)		DEFAULT 'DEFAULT'	NOT NULL,			-- 仕入先コード
	SETSKUGROUPSEQ_NR			NUMBER(7,0)			DEFAULT NULL				,			-- セット商品グループ連番
	PRINTDETTYPE_CD				VARCHAR2(20)		DEFAULT 'PRINT'		NOT	NULL,			-- 納品明細印字フラグ
	PRINTPICKLISTTYPE_CD		VARCHAR2(20)		DEFAULT 'PRINT'		NOT	NULL,			-- ピッキングリスト印字フラグ
	-- 引当結果
	APPLY_DT					DATE				DEFAULT NULL				,			-- 引当日時
	APPLYUSER_CD				VARCHAR2(20)		DEFAULT NULL				,			-- 引当ユーザコード
	APPLYUSER_TX				VARCHAR2(50)		DEFAULT NULL				,			-- 引当ユーザ名
	--
	BATCHNO_CD					VARCHAR2(20)		DEFAULT NULL				,			-- バッチNO（引当時のTRNID）
	TYPEAPPLY_CD				VARCHAR2(20)		DEFAULT NULL				,			-- 引当タイプコード
	TYPEBLOCK_CD				VARCHAR2(20)		DEFAULT NULL				,			-- ブロックタイプ（未使用）
	TYPECARRY_CD				VARCHAR2(20)		DEFAULT NULL				,			-- 運搬タイプコード（未使用）
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 更新社員コード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 更新社員名
	ADD_DT						DATE				DEFAULT SYSDATE		NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員コード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 登録社員名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'		NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0			NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0			NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;
--------------------------------------------------
--              一意キー設定
--------------------------------------------------
ALTER TABLE TA_SHIPD
	ADD CONSTRAINT PK_TA_SHIPD
	PRIMARY KEY(SHIPD_ID)
;

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_SKU_TSD ON TA_SHIPD
(
    SKU_CD
)
;
