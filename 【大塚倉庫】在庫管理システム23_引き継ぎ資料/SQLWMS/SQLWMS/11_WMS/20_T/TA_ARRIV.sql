-- Copyright (C) Seaos Corporation 2013 All rights reserved

--************************************************
-- 入荷予定
--************************************************
--------------------------------------------------
--              テーブル作成
--------------------------------------------------
DROP TABLE TA_ARRIV CASCADE CONSTRAINTS;
CREATE TABLE TA_ARRIV
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	ARRIV_ID					NUMBER(10,0)								NOT NULL,			-- 入荷予定ID
	TRN_ID						NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- トランザクションID
	ARRIVORDERNO_CD				VARCHAR2(60)								NOT NULL,			-- 入荷予定番号（発注番号） 返品時はオーダー番号
	ARRIVORDERDETNO_CD			VARCHAR2(10)		DEFAULT '1'				NOT NULL,			-- 入荷予定明細番号（行No） 返品時はオーダー明細番号
	TYPEARRIV_CD				VARCHAR2(20)		DEFAULT '10'			NOT NULL,			-- 入荷予定タイプコード
	STARRIV_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL,			-- 入荷予定状態ID
	-- 発店
	SUPPLIER_CD					VARCHAR2(60)		DEFAULT NULL					,			-- 発店コード		HEAD1.SKECDをセットする
	SUPPLIER_TX					VARCHAR2(300)		DEFAULT NULL					,			-- 発店名			HEAD1.SKECD-NMをセットする
	-- 着店
	OFFICE_CD					VARCHAR2(60)		DEFAULT NULL					,			-- 着店コード		HEAD2.CCDをセットする
	OFFICE_TX					VARCHAR2(300)		DEFAULT NULL					,			-- 着店名			HEAD2.NOHCD-NM1をセットする
	--
	MANUFACTURER_CD				VARCHAR2(60)		DEFAULT NULL					,			-- 工場区分			出荷元の工場区分。ポカリの段数の識別に利用
	TRANSPORTER_CD				VARCHAR2(60)		DEFAULT NULL					,			-- 配送業者コード	ホストのHEAD1-SUCD1にセットされる
	TRANSPORTER_TX				VARCHAR2(60)		DEFAULT NULL					,			-- 配送業者名
	CARNUMBER_CD				VARCHAR2(60)		DEFAULT NULL					,			-- 車番				入荷前工程webでセットする
	SLIPNO_TX					VARCHAR2(60)		DEFAULT ' '				NOT NULL,			-- 入荷伝票番号		一つの車番に対して複数の伝票が紐付くことがある。伝票番号はホストの中ではユニークではないので読みかえる必要がある
	SLIPDATE_DT					DATE										NOT NULL,			-- 入荷伝票日付
	OWNER_CD					VARCHAR(120)								NOT NULL,			-- 販売荷主コード
	--
	SKU_CD						VARCHAR2(60)								NOT NULL,			-- SKUコード			荷主コード+荷主商品コード
	LOT_TX						VARCHAR2(60)		DEFAULT NULL					,			-- ロット				ホストから来る/画面入力
	CASEPERPALLET_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積み付け数	商品コード及び工場区分をキーにしてWMSでセット
	PALLETLAYER_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット段数
	CASEPERLAYER_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット回し数
	PALLETCAPACITY_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- パレット積載数（パレット段数*パレット回し数）　2014/04/17追加
	STACKINGNUMBER_NR			NUMBER(9,0)			DEFAULT 0				NOT NULL,			-- 保管段数
	TYPEPLALLET_CD				VARCHAR2(30)		DEFAULT NULL					,			-- パレットタイプ		JPRのバレット/ビールパレット。ホストではbdy1のflg1にセットされる。現物は異なる場合があるので実績時は修正機能が必要
	ARRIVPLAN_DT				DATE				DEFAULT NULL					,			-- 入荷予定日			ホストのBODY1.納品日をセットする
	ARRIVPLAN2_DT				DATE				DEFAULT NULL					,			-- 入荷予定時刻			入荷前工程webで入力される入荷予定時刻
	ARRIVPLANPALLET_NR			NUMBER(5,0)			DEFAULT 0						,			-- 入荷予定パレット数	　2014/04/17追加
	ARRIVPLANCASE_NR			NUMBER(5,0)			DEFAULT 0						,			-- 入荷予定ケース数
	ARRIVPLANBARA_NR			NUMBER(9,0)			DEFAULT 0						,			-- 入荷予定バラ数		　2014/04/17追加
	ARRIVPLANPCS_NR				NUMBER(9,0)			DEFAULT 0						,			-- 入荷予定数
	DRIVERTEL_TX				VARCHAR2(45)		DEFAULT ''						,			-- ドライバー電話番号	入荷前工程webで入力されるドライバの電話番号
	DRIVER_TX					VARCHAR2(45)		DEFAULT ''						,			-- ドライバー名	入荷前工程webで入力されるドライバ名
	BERTH_TX					VARCHAR2(45)		DEFAULT ''						,			-- 入荷指定バース		入荷前工程webで入力されるバースの電話番号
	SAMEDAYSHIP_FL				NUMBER(1,0)			DEFAULT 0				NOT NULL,			-- 即出荷フラグ　1:即出荷
	MEMO_TX						VARCHAR2(200)		DEFAULT ''						,			-- 備考	BODY1.MBIKOをセットする
	CHECK_DT					DATE				DEFAULT NULL					,			-- 検品日
	ARRIVEND_DT					DATE				DEFAULT NULL					,			-- 受付日
	ARRIVENDUSER_CD				VARCHAR2(60)		DEFAULT NULL					,			-- 受付社員コード
	ARRIVENDUSER_TX				VARCHAR2(150)		DEFAULT NULL					,			-- 受付社員名
	CHECK_TX					VARCHAR2(150)		DEFAULT NULL					,			-- 検品名
	CHECKUSER_CD				VARCHAR2(60)		DEFAULT NULL					,			-- 検品社員コード 
	CHECKUSER_TX				VARCHAR2(150)		DEFAULT NULL					,			-- 検品社員名
	CHECKHT_ID					NUMBER(6,0)			DEFAULT 0				NOT NULL,			-- 検品ハンディID
	KANBAN_CD					VARCHAR(60)			DEFAULT NULL					,			-- かんばん番号			検品時にパレットにつけるカンバンの番号。カンバンはエリアごとに色を分ける。行ごとに共通の番号
	CHECKHTPCS_NR				NUMBER(9,0)			DEFAULT 0						,			-- ＨＴ検品数
	ARRIVRSLT_DT				DATE				DEFAULT NULL					,			-- 計上日
	ARRIVRSLTUSER_CD			VARCHAR2(60)		DEFAULT NULL					,			-- 計上社員コード
	ARRIVRSLTUSER_TX			VARCHAR2(150)		DEFAULT NULL					,			-- 計上社員名
	ARRIVRSLTHT_ID				NUMBER(6,0)			DEFAULT 0				NOT NULL,			-- 計上ハンディID
	GOODPCS_NR					NUMBER(9,0)			DEFAULT 0						,			-- 計上数（良品）
	NGPCS_NR					NUMBER(9,0)			DEFAULT 0						,			-- 計上数（不良品）
	SAMPLEPCS_NR				NUMBER(9,0)			DEFAULT 0						,			-- 計上数（サンプル）
	TYPEENTRY_CD				VARCHAR(30)			DEFAULT NULL					,			-- 入力区分				ホストとのIF 現場で入力した予定(補給/外販の区分に利用)01:ホスト連携 02:手入力
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザコード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザ名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザコード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザ名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--              一意キー設定
--------------------------------------------------
ALTER TABLE TA_ARRIV
	ADD CONSTRAINT PK_TA_ARRIV
	PRIMARY KEY(ARRIV_ID)
;

--------------------------------------------------
--              インデックス設定
--------------------------------------------------
CREATE INDEX IX_SKU_TAR ON TA_ARRIV
(
    SKU_CD
)
;
