-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 棚卸
--************************************************
--------------------------------------------------
--              テーブル作成
--------------------------------------------------
DROP TABLE TA_STOCKTAKING CASCADE CONSTRAINTS;
CREATE TABLE TA_STOCKTAKING
(-- COLUMN                  TYPE                DEFAULT             NULL                COMMENT
	STOCKTAKING_ID			NUMBER(10,0)							NOT NULL	,	-- 在庫更新ID 
	MANAGER_NUM				VARCHAR2(20 BYTE)									, 	-- 管理番号
--	WH_CD					VARCHAR2(3)			DEFAULT ' '			NOT NULL	,	-- 倉庫コード
	STATUS_TX				VARCHAR2(20)		DEFAULT '未作業'					,	-- 棚卸ステータス（フェーズ1の場合は「未作業」、「未完了」、「完了」）	
	LOCA_CD					VARCHAR2(20)							NOT NULL	,	-- ロケーションコード 
	KANBAN_CD				VARCHAR2(20)										,	-- かんばん番号
	EXTERIOR_CD				VARCHAR2(40)										,	-- 外装
	SKU_TX					VARCHAR(100)										,	-- 商品名
	SIZE_TX					VARCHAR2(40)		        						,	-- 規格 
	LOT_TX					VARCHAR2(40)										,	-- ロット
	PALLETCAPACITY_NR 		NUMBER(9,0)											,   -- パレット積み付け数
	UPD_DT 					DATE 				DEFAULT SYSDATE					,	-- 棚卸データ作成日時 
	SEND_DT 				DATE												,   -- 棚卸データ作業日時
	PALLET_NR				NUMBER(5,0)											,	-- パレット数 
	CASE_NR					NUMBER(5,0)											,	-- ケース数 
	BARA_NR					NUMBER(9,0)											,	-- バラ数 
	PCS_NR					NUMBER(9,0)											,	-- ピース数 
	INPUTPALLET_NR 			NUMBER(5,0)											,   -- 入力されたパレット数
	INPUTCASE_NR 			NUMBER(5,0)											, 　　-- 入力されたケース数
	INPUTBARA_NR 			NUMBER(9,0)											, 　　-- 入力されたバラ数
	INPUTPCS_NR 			NUMBER(9,0)											, 　　-- 入力されたピース数
	OPERATOR_TX 			VARCHAR2(20 BYTE) 	DEFAULT ''						, 　　-- 作業者名
	AUTHORIZER_TX 			VARCHAR2(20 BYTE) 	DEFAULT ''						,   -- 承認者名
	RESULT_TX 				VARCHAR2(20 BYTE)									,   -- 棚卸作業結果
	MEMO_TX 				VARCHAR2(1024 BYTE)										--　メモ欄
--	UPDPROGRAM_CD			VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,	-- 更新プログラムコード
--	UPDCOUNTER_NR			NUMBER(10,0)		DEFAULT 0				NOT NULL,	-- 更新カウンター
--	STRECORD_ID				NUMBER(2,0)			DEFAULT 0				NOT NULL	-- レコード状態（-2:削除、-1:作成中、0:通常）
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------
--              一意キー設定
--------------------------------------------------
ALTER TABLE TA_STOCKTAKING
	ADD CONSTRAINT PK_TA_STOCKTAKING
	PRIMARY KEY(STOCKTAKING_ID)
;