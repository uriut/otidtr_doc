-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- 在庫チェック
--************************************************
--------------------------------------------------
--              テーブル作成
--------------------------------------------------
DROP TABLE Z_CHECKSTOCK CASCADE CONSTRAINTS;
CREATE TABLE Z_CHECKSTOCK
(-- COLUMN                  TYPE                DEFAULT             NULL                COMMENT
	SKU_CD					VARCHAR2(40)								NOT NULL,	-- ＳＫＵコード 
	EXTERIOR_CD				VARCHAR2(40)								NOT NULL,	-- 外装コード
	AMOUNT_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,	-- 入数
	LOT_TX					VARCHAR2(40)		DEFAULT ' '				NOT NULL,	-- ロット
	WMSPCS_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,	-- WMSPCS
	HOSTPCS_NR				NUMBER(9,0)			DEFAULT 0				NOT NULL,	-- HOSTPCS
	ADD_DT					DATE										NOT NULL	-- 登録日
)
;
--------------------------------------------------
--              一意キーなし
--------------------------------------------------
