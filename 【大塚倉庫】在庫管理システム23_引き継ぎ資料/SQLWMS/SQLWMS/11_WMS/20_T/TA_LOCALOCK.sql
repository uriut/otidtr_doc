-- Copyright (C) Seaos Corporation 2013 All rights reserved
--************************************************
-- ロケーションロック
--************************************************
--------------------------------------------------
--              テーブル作成
--------------------------------------------------
DROP TABLE TA_LOCALOCK CASCADE CONSTRAINTS;
CREATE TABLE TA_LOCALOCK
(-- COLUMN						TYPE				DEFAULT					NULL				COMMENT
	LOCA_CD						VARCHAR2(20)								NOT NULL,			-- ロケーションコード 
	-- 管理項目
	UPD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 更新日時
	UPDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザコード
	UPDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新ユーザ名
	ADD_DT						DATE				DEFAULT SYSDATE			NOT NULL,			-- 登録日時
	ADDUSER_CD					VARCHAR2(20)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザコード
	ADDUSER_TX					VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 登録ユーザ名
	UPDPROGRAM_CD				VARCHAR2(50)		DEFAULT 'ADMIN'			NOT NULL,			-- 更新プログラムコード
	UPDCOUNTER_NR				NUMBER(10,0)		DEFAULT 0				NOT NULL,			-- 更新カウンター
	STRECORD_ID					NUMBER(2,0)			DEFAULT 0				NOT NULL			-- レコード状態（-2:削除、-1:作成中、0:通常）
)
;

--------------------------------------------------
--              一意キー設定
--------------------------------------------------
ALTER TABLE TA_LOCALOCK
	ADD CONSTRAINT PK_TA_LOCALOCK
	PRIMARY KEY(LOCA_CD)
;
