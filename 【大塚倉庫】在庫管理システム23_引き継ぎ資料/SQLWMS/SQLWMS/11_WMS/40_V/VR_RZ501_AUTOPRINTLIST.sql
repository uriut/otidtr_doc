-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- ピッキング指示書(デバッグ用)
--*******************************************
DROP VIEW VR_RZ501_AUTOPRINTLIST;
CREATE VIEW VR_RZ501_AUTOPRINTLIST
(
	PALLETDETNO_CD				,
	PICKNO_CD					,
	PRINTER_TX						-- todo
) AS
SELECT
	'L999999999'				,
	'P999999999'				,
	' '							
FROM
	DUAL
;
