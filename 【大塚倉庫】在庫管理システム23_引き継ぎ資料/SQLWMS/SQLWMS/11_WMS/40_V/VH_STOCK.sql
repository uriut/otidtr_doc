-- Copyright (C) Seaos Corporation 2014 All rights reserved
-- *******************************************
-- HT用在庫照会
-- *******************************************

--HTからカンバンスキャンで在庫情報呼び出しをする為に使用
--SKU、ITF、JANから検索する場合でも使用出来るように（商品検索画面）

DROP VIEW VH_STOCK;
CREATE VIEW VH_STOCK
(
	LOCA_CD					,	-- ロケーション
	LOCA_TX					,	-- ロケーション名
	KANBAN_CD				,	-- かんばん番号
	SKU_CD					,	-- ＳＫＵコード
	SKUBARCODE_TX			,	-- JANコード
	ITFCODE_TX				,	-- ITFコード
	SKU_TX					,	-- SKU名
	SKU2_TX					,	-- 商品名（半角ver）
	EXTERIOR_CD				,	-- 外装コード
	SIZE_TX					,	-- 規格
	LOT_TX					,	-- ロット
	TOTALPALLET_NR			,	-- 総パレット数
	TOTALCASE_NR			,	-- 総ケース数
	TOTALBARA_NR			,	-- 総バラ数
	TOTALPCS_NR				,	-- 総ＰＣＳ数
	PPALLET_NR				,	-- 引当可能パレット数
	PCASE_NR				,	-- 引当可能ケース数
	PBARA_NR				,	-- 引当可能バラ数
	PPCS_NR					,	-- 引当可能ＰＣＳ
	CPALLET_NR				,	-- 引当済みパレット数
	CCASE_NR				,	-- 引当済みケース数
	CBARA_NR				,	-- 引当済みバラ数
	CPCS_NR					,	-- 引当済みＰＣＳ
	STOCK_DT				,	-- 入庫日 
	USELIMIT_DT				,	-- 使用期限
	TYPESTOCK_CD			,	-- 在庫タイプコード
	TYPELOCA_CD				,	-- ロケーションタイプコード
	TYPELOCA_TX				,	-- ロケーションタイプ名
	SHIPVOLUME_CD			,	-- 出荷荷姿コード
	SHIPVOLUME_TX			,	-- 出荷荷姿名
	STSTOCK_ID				,	-- 在庫状態ID
	STSTOCK_TX				,	-- 在庫状態名
	AMOUNT_NR				,	-- 入数 標準ケース・メーカー箱の入数
	LINEUNIT_NR				,	-- ラインユニット
	PALLETCAPACITY_NR		,	-- パレット積み付け数
	PALLETLAYER_NR				-- パレット段数
) AS
	SELECT
		MLC.LOCA_CD,                                    -- ロケ
		MLC.LOCA_TX,                                    -- ロケ
		LS2.KANBAN_CD,                                  -- カンバンコード
		LS2.SKU_CD,                                     -- 商品コード
		LS2.SKUBARCODE_TX,                              -- JANコード
		LS2.ITFCODE_TX,                                 -- ITFコード
		LS2.SKU_TX,                                     -- 商品名
		LS2.SKU2_TX,                                    -- 商品名（半角ver）
		LS2.EXTERIOR_CD,                                -- 外装コード
		LS2.SIZE_TX,                                    -- 規格
		LS2.LOT_TX,                                     -- ロット
		LS2.TOTALPALLET_NR,                             -- パレット数	総数
		LS2.TOTALCASE_NR,                               -- ケース数		総数
		LS2.TOTALBARA_NR,                               -- バラ数		総数
		LS2.TOTALPCS_NR,                                -- PCS数			総数
		LS2.PPALLET_NR,                                 -- パレット数	引当可能
		LS2.PCASE_NR,                                   -- ケース数		引当可能
		LS2.PBARA_NR,                                   -- バラ		   	引当可能
		LS2.PPCS_NR,                                    -- PCS			引当可能
		LS2.CPALLET_NR,                                 -- パレット数	引当済
		LS2.CCASE_NR,                                   -- ケース数		引当済
		LS2.CBARA_NR,                                   -- バラ		   	引当済
		LS2.CPCS_NR,                                    -- PCS		    引当済
		LS2.STOCK_DT,                                   -- 入庫日
		TO_CHAR(LS2.USELIMIT_DT, 'YYYY/MM/DD'),         -- 使用期限
		LS2.TYPESTOCK_CD,                               -- 在庫タイプコード
		MLC.TYPELOCA_CD,                                -- ロケーション種別コード
		MTL.TYPELOCA_TX,                                -- ロケーション種別
		MLC.SHIPVOLUME_CD,                              -- 出荷荷姿コード
		CASE 
			WHEN MLC.SHIPVOLUME_CD = '01' 
			THEN 'ケース成り' 
			WHEN MLC.SHIPVOLUME_CD = '02' 
			THEN 'パレット成り' 
			WHEN MLC.SHIPVOLUME_CD = '03' 
			THEN 'ネステナ' 
			END AS SHIPVOLUME_TX,                          -- 出荷荷姿名
		LS2.STSTOCK_ID,                                 -- 在庫ステータス
		LS2.STSTOCK_TX,                                 -- 在庫状態名
		LS2.AMOUNT_NR,                                  -- 入数
		MLC.LINEUNIT_NR,                                -- ラインユニット
		LS2.PALLETCAPACITY_NR,                          -- パレット積み付け数
		LS2.PALLETLAYER_NR                              -- パレット段数
	FROM
		( 
		SELECT
			TLS.LOCA_CD, 
			TLS.SKU_CD, 
			TLS.KANBAN_CD, 
			TLS.LOT_TX, 
			TLS.STOCK_DT, 
			TLS.USELIMIT_DT, 
			SUM(TLS.TOTALPALLET_NR) AS TOTALPALLET_NR, 
			SUM(TLS.TOTALCASE_NR)   AS TOTALCASE_NR, 
			SUM(TLS.TOTALBARA_NR)   AS TOTALBARA_NR, 
			SUM(TLS.TOTALPCS_NR)    AS TOTALPCS_NR, 
			SUM(TLS.PPALLET_NR)     AS PPALLET_NR, 
			SUM(TLS.PCASE_NR)       AS PCASE_NR, 
			SUM(TLS.PBARA_NR)       AS PBARA_NR, 
			SUM(TLS.PPCS_NR)        AS PPCS_NR, 
			SUM(TLS.CPALLET_NR)     AS CPALLET_NR, 
			SUM(TLS.CCASE_NR)       AS CCASE_NR, 
			SUM(TLS.CBARA_NR)       AS CBARA_NR, 
			SUM(TLS.CPCS_NR)        AS CPCS_NR, 
			TLS.TYPESTOCK_CD, 
			MTS.TYPESTOCK_TX, 
			TLS.STSTOCK_ID, 
			MSS.STSTOCK_TX, 
			TLS.SKU_TX, 
			TLS.SKU2_TX, 
			TLS.SKUBARCODE_TX, 
			TLS.ITFCODE_TX, 
			TLS.AMOUNT_NR, 
			TLS.PALLETCAPACITY_NR, 
			TLS.PALLETLAYER_NR, 
			TLS.EXTERIOR_CD, 
			TLS.SIZE_TX 
		FROM
			( 
			SELECT
				TLP.LOCA_CD, 
				TLP.SKU_CD, 
				TLP.KANBAN_CD, 
				TLP.LOT_TX, 
				TLP.STOCK_DT, 
				TLP.USELIMIT_DT, 
				TLP.PALLET_NR      AS TOTALPALLET_NR, 
				TLP.CASE_NR        AS TOTALCASE_NR, 
				TLP.BARA_NR        AS TOTALBARA_NR, 
				TLP.PCS_NR         AS TOTALPCS_NR, 
				TLP.PALLET_NR      AS PPALLET_NR, 
				TLP.CASE_NR        AS PCASE_NR, 
				TLP.BARA_NR        AS PBARA_NR, 
				TLP.PCS_NR         AS PPCS_NR, 
				0                  AS CPALLET_NR, 
				0                  AS CCASE_NR, 
				0                  AS CBARA_NR, 
				0                  AS CPCS_NR, 
				TLP.TYPESTOCK_CD, 
				0                  AS STSTOCK_ID, 
				MSK.SKU_TX, 
				MSK.SKU2_TX, 
				MSK.SKUBARCODE_TX, 
				MSK.ITFCODE_TX, 
				MSK.PCSPERCASE_NR  AS AMOUNT_NR, 
				MSK.EXTERIOR_CD, 
				MSK.SIZE_TX, 
				TLP.PALLETCAPACITY_NR, 
				TLP.PALLETLAYER_NR 
			FROM
				TA_LOCASTOCKP TLP, 
				MA_SKU MSK 
			WHERE
				TLP.SKU_CD = MSK.SKU_CD 
			UNION ALL 
			SELECT
				TLC.LOCA_CD, 
				TLC.SKU_CD, 
				TLC.KANBAN_CD, 
				TLC.LOT_TX, 
				TLC.STOCK_DT, 
				TLC.USELIMIT_DT, 
				SUM(TLC.PALLET_NR) AS TOTALPALLET_NR, 
				SUM(TLC.CASE_NR)   AS TOTALCASE_NR, 
				SUM(TLC.BARA_NR)   AS TOTALBARA_NR, 
				SUM(TLC.PCS_NR)    AS TOTALPCS_NR, 
				0                  AS PPALLET_NR, 
				0                  AS PCASE_NR, 
				0                  AS PBARA_NR, 
				0                  AS PPCS_NR, 
				SUM(TLC.PALLET_NR) AS CPALLET_NR, 
				SUM(TLC.CASE_NR)   AS CCASE_NR, 
				SUM(TLC.BARA_NR)   AS CBARA_NR, 
				SUM(TLC.PCS_NR)    AS CPCS_NR, 
				TLC.TYPESTOCK_CD, 
				TLC.STSTOCK_ID, 
				MSK.SKU_TX, 
				MSK.SKU2_TX, 
				MSK.SKUBARCODE_TX, 
				MSK.ITFCODE_TX, 
				MSK.PCSPERCASE_NR  AS AMOUNT_NR, 
				MSK.EXTERIOR_CD, 
				MSK.SIZE_TX, 
				TLC.PALLETCAPACITY_NR, 
				TLC.PALLETLAYER_NR 
			FROM
				TA_LOCASTOCKC TLC, 
				MA_SKU MSK, 
				MB_STSTOCK MSS 
			WHERE
				TLC.SKU_CD = MSK.SKU_CD AND
				TLC.STSTOCK_ID <= PS_DEFINE.ST_STOCK19_PICKE 
			GROUP BY
				TLC.LOCA_CD, 
				TLC.SKU_CD, 
				TLC.KANBAN_CD, 
				TLC.LOT_TX, 
				TLC.STOCK_DT, 
				TLC.USELIMIT_DT, 
				TLC.TYPESTOCK_CD, 
				TLC.STSTOCK_ID, 
				MSK.SKU_TX, 
				MSK.SKU2_TX, 
				MSK.SKUBARCODE_TX, 
				MSK.ITFCODE_TX, 
				MSK.PCSPERCASE_NR, 
				MSK.EXTERIOR_CD, 
				MSK.SIZE_TX, 
				TLC.PALLETCAPACITY_NR, 
				TLC.PALLETLAYER_NR, 
				MSK.EXTERIOR_CD, 
				MSK.SIZE_TX
			) TLS, 
			MB_TYPESTOCK MTS, 
			MB_STSTOCK MSS 
		WHERE
			TLS.TYPESTOCK_CD = MTS.TYPESTOCK_CD(+) AND
			TLS.STSTOCK_ID = MSS.STSTOCK_ID(+) 
		GROUP BY
			TLS.LOCA_CD, 
			TLS.SKU_CD, 
			TLS.KANBAN_CD, 
			TLS.LOT_TX, 
			TLS.STOCK_DT, 
			TLS.USELIMIT_DT, 
			TLS.TYPESTOCK_CD, 
			MTS.TYPESTOCK_TX, 
			TLS.STSTOCK_ID, 
			MSS.STSTOCK_TX, 
			TLS.SKU_TX, 
			TLS.SKU2_TX,
			TLS.SKUBARCODE_TX, 
			TLS.ITFCODE_TX, 
			TLS.AMOUNT_NR, 
			TLS.PALLETCAPACITY_NR, 
			TLS.PALLETLAYER_NR, 
			TLS.EXTERIOR_CD, 
			TLS.SIZE_TX
		) LS2 
		FULL OUTER JOIN MA_LOCA MLC 
			ON LS2.LOCA_CD = MLC.LOCA_CD 
		LEFT OUTER JOIN MB_TYPELOCA MTL 
			ON MLC.TYPELOCA_CD = MTL.TYPELOCA_CD 
		LEFT OUTER JOIN MB_TYPELOCABLOCK MTB 
			ON MLC.TYPELOCABLOCK_CD = MTB.TYPELOCABLOCK_CD 
	ORDER BY
		MLC.LOCA_CD; 


