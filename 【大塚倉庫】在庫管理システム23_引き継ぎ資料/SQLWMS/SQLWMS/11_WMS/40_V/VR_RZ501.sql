-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- �s�b�L���O�w����
--*******************************************
DROP VIEW VR_RZ501;
CREATE VIEW VR_RZ501
(
	PICKNO_CD					,
	PALLETDETNO_CD				,
	ROW_NR						,
	STPICK_ID					,
	TYPEPICK_CD					,
	TYPEPALLET_CD				,
	TYPEPALLET_TX				,
	TRANSPORTER_CD				,
	TRANSPORTER_TX				,
	TYPESHIP_TX					,
	DTNAME_TX					,
	DTADDRESS_TX				,
	SHIPBERTH_CD				,
	SHIPPLAN_TX					,
	--
	SRCLOCA_CD					,
	SRCLOCA_TX					,
	KANBAN_CD					,
	SKU_CD						,
	SKU_TX						,
	LOT_TX						,
	EXTERIOR_CD					,
	SIZE_TX						,
	AMOUNT_NR					,
	PALLETCAPACITY_NR			,
	PALLET_NR					,
	CASE_NR						,
	BARA_NR						,
	PCS_NR						,
	SKUWEIGHT_NR				,	-- ���גP�ʂ̏d��(g)
	PRINTER_TX					
) AS
SELECT
	'P999999999'				,
	'L999999999'				,
	1							,
	1							,
	'30'						,
	'01'						,
	'JPR'						,
	'040500'					,
	'�n�C�G�X�T�[�r�X'			,
	'�ǉ�'						,
	'�[�i�於�w�w�w�w�w�w�w�w�w�w',
	'�[�i��Z���w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w',
	'777'						,
	'23:59:59'					,
	--
	'0103A01039020101'			,
	'0103A010390201'			,
	'ZZ999'						,
	'KK13349'					,
	'�|�J���X�G�b�g�R�S�O�l�k�X�`�[��',
	'XXXXXXXXXXXXXXXXXXXX'		,
	'3349'						,
	'340ML*24'					,
	24							,
	90							,
	999							,
	99999						,
	99999						,
	4657815						,
	1							,
	' '							
FROM
	DUAL
;
