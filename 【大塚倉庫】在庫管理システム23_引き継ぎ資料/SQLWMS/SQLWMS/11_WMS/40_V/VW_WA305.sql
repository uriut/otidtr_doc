-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- ロケーションマスタ
--*******************************************
DROP VIEW VW_WA305;
CREATE VIEW VW_WA305
(
	LOCA_CD					,	/* ロケーション */
	LOCA_TX					,	/* ロケーション名 */
	WH_CD					,	/* 倉庫コード */
	FLOOR_CD				,	/* フロアコード */
	AREA_CD					,	/* エリアコード */
	LINE_CD					,	/* 列コード */
	SHELF_CD				,	/* 棚コード */
	LAYER_CD				,	/* 段コード */
	ORDER_CD				,	/* 番コード */
	TYPELOCA_CD				,	/* ロケーション種別コード */
	TYPELOCA_TX				,	/* ロケーション種別名 */
	TYPELOCABLOCK_CD		,	/* ロケーションブロックコード */
	TYPELOCABLOCK_TX		,	/* ロケーションブロック名 */
	LINEUNIT_CD				,	/* ラインユニット */
	LINEUNIT_NR				,	/* ラインユニット */
	HEQUIP_CD				,	/* 格納什器コード */
	HEQUIP_TX				,	/* 格納什器 */
	CASE_CD					,	/* 標準ケースコード */
	CASE_TX					,	/* 標準ケース */
	PALLETDIV_NR			,	/* パレット分割数 */
	SHIPVOLUME_CD			,	/* 出荷荷姿コード */
	SHIPVOLUME_TX			,	/* 出荷荷姿 */
	ABC_ID					,	/* ロケーションＡＢＣＩＤ */
	ABC_CD					,	/* ロケーションＡＢＣ */
	ABCSUB_ID				,	/* ロケーションＡＢＣＳＵＢＩＤ */
	ABCSUB_TX				,	/* ロケーションＡＢＣＳＵＢ */
	ARRIVPRIORITY_NR		,	/* 入庫優先順位 */
	SHIPPRIORITY_NR			,	/* 出庫優先順位 */
	INVENT_FL				,	/* 棚卸対象フラグ */
	NOTUSE_FL				,	/* 未使用フラグ（入庫引当で利用） */
	NOTSHIP_FL				,	/* 出荷不可フラグ */
	ADD_DT					,	/* 登録日時 */
	ADDUSER_CD				,	/* 登録ユーザコード */
	ADDUSER_TX				,	/* 登録ユーザ名 */
	UPD_DT					,	/* 更新日時 */
	UPDUSER_CD				,	/* 更新ユーザコード */
	UPDUSER_TX				,	/* 更新ユーザ名 */
	UPDPROGRAM_CD			,	/* 更新プログラムコード */
	UPDCOUNTER_NR			,	/* 更新カウンター */
	STRECORD_ID					/* レコード状態 */
) AS
SELECT
	MAL.LOCA_CD					,	/* ロケーション */
	MAL.LOCA_TX					,	/* ロケーション名 */
	MAL.WH_CD					,	/* 倉庫コード */
	MAL.FLOOR_CD				,	/* フロアコード */
	MAL.AREA_CD					,	/* エリアコード */
	MAL.LINE_CD					,	/* 列コード */
	MAL.SHELF_CD				,	/* 棚コード */
	MAL.LAYER_CD				,	/* 段コード */
	MAL.ORDER_CD				,	/* 番コード */
	MAL.TYPELOCA_CD				,	/* ロケーション種別コード */
	MBTL.TYPELOCA_TX			,	/* ロケーション種別名 */
	MAL.TYPELOCABLOCK_CD		,	/* ロケーションブロックコード */
	MBTB.TYPELOCABLOCK_TX		,	/* ロケーションブロック名 */
	MAL.LINEUNIT_CD				,	/* ラインユニット */
	MAL.LINEUNIT_NR				,	/* ラインユニット */
	MAL.HEQUIP_CD				,	/* 格納什器コード */
	MAHE.HEQUIP_TX				,	/* 格納什器 */
	MAL.CASE_CD					,	/* 標準ケースコード */
	MBCA.CASE_TX				,	/* 標準ケース */
	MAL.PALLETDIV_NR			,	/* パレット分割数 */
	MAL.SHIPVOLUME_CD			,	/* 出荷荷姿コード */
	MBSH.SHIPVOLUME_TX			,	/* 出荷荷姿 */
	MAL.ABC_ID					,	/* ロケーションＡＢＣＩＤ */
	MAAB.ABC_CD					,	/* ロケーションＡＢＣ */
	MAL.ABCSUB_ID				,	/* ロケーションＡＢＣＳＵＢＩＤ */
	MAAS.ABCSUB_TX				,	/* ロケーションＡＢＣＳＵＢ */
	MAL.ARRIVPRIORITY_NR		,	/* 入庫優先順位 */
	MAL.SHIPPRIORITY_NR			,	/* 出庫優先順位 */
	MAL.INVENT_FL				,	/* 棚卸対象フラグ */
	MAL.NOTUSE_FL				,	/* 未使用フラグ（入庫引当で利用） */
	MAL.NOTSHIP_FL				,	/* 出荷不可フラグ */
	MAL.ADD_DT					,	/* 登録日時 */
	MAL.ADDUSER_CD				,	/* 登録ユーザコード */
	MAL.ADDUSER_TX				,	/* 登録ユーザ名 */
	MAL.UPD_DT					,	/* 更新日時 */
	MAL.UPDUSER_CD				,	/* 更新ユーザコード */
	MAL.UPDUSER_TX				,	/* 更新ユーザ名 */
	MAL.UPDPROGRAM_CD			,	/* 更新プログラムコード */
	MAL.UPDCOUNTER_NR			,	/* 更新カウンター */
	MAL.STRECORD_ID					/* レコード状態 */
FROM
	MA_LOCA MAL
		LEFT OUTER JOIN MB_TYPELOCA MBTL
			ON MAL.TYPELOCA_CD = MBTL.TYPELOCA_CD
		LEFT OUTER JOIN MB_TYPELOCABLOCK MBTB
			ON MAL.TYPELOCABLOCK_CD = MBTB.TYPELOCABLOCK_CD
		LEFT OUTER JOIN MA_HEQUIP MAHE
			ON MAL.HEQUIP_CD = MAHE.HEQUIP_CD
		LEFT OUTER JOIN MB_CASE MBCA
			ON MAL.CASE_CD = MBCA.CASE_CD
		LEFT OUTER JOIN MB_SHIPVOLUME MBSH
			ON MAL.SHIPVOLUME_CD = MBSH.SHIPVOLUME_CD
		LEFT OUTER JOIN MA_ABC MAAB
			ON MAL.ABC_ID = MAAB.ABC_ID
		LEFT OUTER JOIN MA_ABCSUB MAAS
			ON MAL.ABCSUB_ID = MAAS.ABCSUB_ID
;
