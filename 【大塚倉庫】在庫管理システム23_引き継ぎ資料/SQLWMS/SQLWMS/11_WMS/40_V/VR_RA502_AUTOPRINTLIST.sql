-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- ケース指示書リスト
--*******************************************
DROP VIEW VR_RA502_AUTOPRINTLIST;
CREATE VIEW VR_RA502_AUTOPRINTLIST
(
	PALLETDETNO_CD				,
	PICKNO_CD					,
	TRANSPORTER2_CD				,
	PICKORDER_NR				,
	TOTALPIC_FL					,
	PRINTER_TX						-- todo
) AS
SELECT
	TP.PALLETDETNO_CD			,
	TP.PICKNO_CD				,
	TP.TRANSPORTER_CD			,
	MIN(MT.PICKORDER_NR) AS PICKORDER_NR,
	TP.TOTALPIC_FL				,
	' '							
FROM
	(
	SELECT
		PALLETDETNO_CD	,
		PICKNO_CD		,
		TYPEPICK_CD		,
		TRANSPORTER_CD	,
		MIN(STPICK_ID) AS STPICK_ID,
		MAX(PRINT_FL) AS PRINT_FL	,
		TOTALPIC_FL
	FROM
		TA_PICK
	GROUP BY
		PALLETDETNO_CD	,
		PICKNO_CD		,
		TYPEPICK_CD		,
		TRANSPORTER_CD	,
		TOTALPIC_FL
	) TP
		LEFT OUTER JOIN MA_TRANSPORTER MT
			ON TP.TRANSPORTER_CD = MT.TRANSPORTERGROUP_CD
WHERE
	(
		TP.STPICK_ID = PS_DEFINE.ST_PICK1_PRINTOK	OR
		TP.PRINT_FL = 1
	) 											AND -- todo
	TP.TYPEPICK_CD = '99'						AND -- todo
	NVL(TP.TOTALPIC_FL,0) = 1						-- todo
GROUP BY
	TP.PALLETDETNO_CD			,
	TP.PICKNO_CD				,
	TP.TRANSPORTER_CD			,
	TP.TOTALPIC_FL
ORDER BY
	MIN(MT.PICKORDER_NR)		,
	TP.PALLETDETNO_CD			,
	TP.PICKNO_CD
;
