-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- 出荷指示照会
--*******************************************
DROP VIEW VW_WA501;
CREATE VIEW VW_WA501
(
	SHIPH_ID						,	/* ITF出荷指示データヘッダID */
	STSHIP_ID						,	/* 出荷状態コード */
	STSHIP_TX						,	/* 出荷状態 */
	TYPESHIP_CD						,	
	TYPESHIP_TX						,	
	TRANSPORTER1_CD					,	/* 配送業者コード */
	TRANSPORTERGROUP_TX				,	/* 配送業者グループ名 */
	TRANSPORTER_TX					,	/* 配送業者名 */
	DTPOSTAL_CD						,	/* 地区コード */
	DELIVERYDESTINATION_CD			,	/* 納品先コード */
	DTNAME_TX						,	/* 納品先名	*/
	TOTALPIC_TX						,	/* トータルピック */
	SLIPNO_TX						,	/* 伝票番号 */
	END_DT							,	/* 出荷日 */
	SHIPPLAN_DT						,	/* 出荷予定時刻 */
	SHIPBERTH_CD					,	/* 指定バース */
	DIRECTDELIVERY_CD				,	/* 倉庫直送コード */
	APPLY_DT						,	/* 引当完了日時 */
	APPLYUSER_CD					,	/* 引当者コード */
	APPLYUSER_TX					,	/* 引当者名 */
	CHECKEND_DT						,	/* 検品完了日時 */
	CHECKHTUSER_CD					,	/* 検品者コード */
	CHECKHTUSER_TX						/* 検品者名 */
) AS
SELECT
	TASH.SHIPH_ID					,	/* ITF出荷指示データヘッダID */
	TASH.STSHIP_ID					,	/* 出荷状態コード */
	MST.STSHIP_TX					,	/* 出荷状態 */
	TASH.TYPESHIP_CD				,	
	TASH.TYPESHIP_TX				,	
	TASH.TRANSPORTER1_CD			,	/* 配送業者コード */
	MAT.TRANSPORTERGROUP_TX			,	/* 配送業者グループ名 */
	MAT.TRANSPORTER_TX				,	/* 配送業者名 */
	TASH.DTPOSTAL_CD				,	/* 地区コード */
	TASH.DELIVERYDESTINATION_CD		,	/* 納品先コード */
	TASH.DTNAME_TX					,	/* 納品先名	*/
	MBT.TOTALPIC_TX					,	/* トータルピック */
	TASH.SLIPNO_TX					,	/* 伝票番号 */
	TASH.END_DT						,	/* 出荷日 */
	TASH.SHIPPLAN_DT				,	/* 出荷予定時刻 */
	TASH.SHIPBERTH_CD				,	/* 指定バース */
	TASH.DIRECTDELIVERY_CD			,	/* 倉庫直送コード */
	MAX(TAP.ADD_DT)			AS APPLY_DT		,	/* 引当完了日時 */
	MAX(TAP.ADDUSER_CD)		AS APPLYUSER_CD	,	/* 引当者コード */
	MAX(TAP.ADDUSER_TX)		AS APPLYUSER_TX	,	/* 引当者名 */
	MAX(TAP.CHECKEND_DT)	AS CHECKEND_DT	,	/* 検品完了日時 */
	MAX(TAP.CHECKHTUSER_CD)	AS CHECKHTUSER_CD,	/* 検品者コード */
	MAX(TAP.CHECKHTUSER_TX)	AS CHECKHTUSER_TX	/* 検品者名 */
FROM
	TA_SHIPH TASH
		LEFT OUTER JOIN TA_PICK TAP
			ON TASH.SHIPH_ID = TAP.SHIPH_ID
		LEFT OUTER JOIN MA_TRANSPORTER MAT
			ON TRIM(TASH.TRANSPORTER1_CD) = MAT.TRANSPORTER_CD
		LEFT OUTER JOIN MB_STSHIP MST
			ON TASH.STSHIP_ID = MST.STSHIP_ID
		LEFT OUTER JOIN MB_TOTALPIC MBT
			ON MAT.TOTALPIC_FL = MBT.TOTALPIC_FL
GROUP BY
	TASH.SHIPH_ID					,	/* ITF出荷指示データヘッダID */
	TASH.STSHIP_ID					,	/* 出荷状態コード */
	MST.STSHIP_TX					,	/* 出荷状態 */
	TASH.TYPESHIP_CD				,	
	TASH.TYPESHIP_TX				,	
	TASH.TRANSPORTER1_CD			,	/* 配送業者コード */
	MAT.TRANSPORTERGROUP_TX			,	/* 配送業者グループ名 */
	MAT.TRANSPORTER_TX				,	/* 配送業者名 */
	TASH.DTPOSTAL_CD				,	/* 地区コード */
	TASH.DELIVERYDESTINATION_CD		,	/* 納品先コード */
	TASH.DTNAME_TX					,	/* 納品先名	*/
	MBT.TOTALPIC_TX					,	/* トータルピック */
	TASH.SLIPNO_TX					,	/* 伝票番号 */
	TASH.END_DT						,	/* 出荷日 */
	TASH.SHIPPLAN_DT				,	/* 出荷予定時刻 */
	TASH.SHIPBERTH_CD				,	/* 指定バース */
	TASH.DIRECTDELIVERY_CD				/* 倉庫直送コード */
;
