-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- ユーザマスタ
--*******************************************
DROP VIEW VA_USER;
CREATE VIEW VA_USER
(
	USER_CD	,
	USER_TX
) AS
SELECT
	USER_CD	,
	USER_TX
FROM
	MA_USER;

--権限付与
--GRANT SELECT ON VA_USER 	TO ITF;
--GRANT SELECT ON VA_USER 	TO WMS;
