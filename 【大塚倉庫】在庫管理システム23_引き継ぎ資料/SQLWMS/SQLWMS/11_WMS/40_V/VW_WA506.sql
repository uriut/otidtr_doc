-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- 出荷指示　配送業者別一覧
--*******************************************
DROP VIEW VW_WA506;
CREATE VIEW VW_WA506
(
	STPICK_ID					,	/* ピック状態ID */
	TRANSPORTER1_CD				,	/* 配送業者コード */
	TRANSPORTER_TX				,	/* 配送業者名 */
	TRANSPORTERGROUP_CD			,	/* 配送業者グループコード */
	TRANSPORTERGROUP_TX			,	/* 配送業者グループ名 */
	DELIVERYDESTINATION_CD		,	/* 納品先コード */
	DTNAME_TX					,	/* 納品先名 */
	ADD_DT						,	/* 登録日時(受信日時) */
	PALLETDETNO_COUNT			,	/* パレット明細数 */
	SKU_COUNT					,	/* 合計SKU数 */
	AMOUNT_NR						/* 合計ケース数 */
) AS
SELECT
	COALESCE(MIN(TAP.STPICK_ID) ,0)
		AS STPICK_ID				,	/* ピック状態ID */
	TASH.TRANSPORTER1_CD			,	/* 配送業者コード */
	MAT.TRANSPORTER_TX				,	/* 配送業者名 */
	MAT.TRANSPORTERGROUP_CD			,	/* 配送業者グループコード */
	MAT.TRANSPORTERGROUP_TX			,	/* 配送業者グループ名 */
	CASE
		WHEN MAT.TOTALPIC_FL = 0 THEN
			TASH.DELIVERYDESTINATION_CD
		ELSE
			'-'
	END as DELIVERYDESTINATION_CD	,	/* 納品先コード */
	CASE
		WHEN MAT.TOTALPIC_FL = 0 THEN
			TASH.DTNAME_TX
		ELSE
			'-'
	END as DTNAME_TX				,	/* 納品先名 */
	TASH.ADD_DT						,	/* 登録日時(受信日時) */
	COUNT(DISTINCT TAP.PALLETDETNO_CD)
		AS PALLETDETNO_COUNT		,	/* パレット明細数 */
	COUNT(DISTINCT TAP.SKU_CD)
		AS SKU_COUNT				,	/* 合計SKU数 */
	COALESCE(SUM(TAP.AMOUNT_NR), 0)
		AS AMOUNT_NR					/* 合計ケース数 */
FROM
	TA_SHIPH TASH
		LEFT OUTER JOIN MA_TRANSPORTER MAT
			ON TRIM(TASH.TRANSPORTER1_CD) = MAT.TRANSPORTER_CD
		LEFT OUTER JOIN TA_PICK TAP
			ON TASH.SHIPH_ID = TAP.SHIPH_ID
GROUP BY
	TASH.TRANSPORTER1_CD			,	/* 配送業者コード */
	MAT.TRANSPORTER_TX				,	/* 配送業者名 */
	MAT.TRANSPORTERGROUP_CD			,	/* 配送業者グループコード */
	MAT.TRANSPORTERGROUP_TX			,	/* 配送業者グループ名 */
	MAT.TOTALPIC_FL					,	/* トータルピックフラグ */
	TASH.DELIVERYDESTINATION_CD		,	/* 納品先コード */
	TASH.DTNAME_TX					,	/* 納品先名	*/
	TASH.ADD_DT							/* 登録日時(受信日時) */
;
