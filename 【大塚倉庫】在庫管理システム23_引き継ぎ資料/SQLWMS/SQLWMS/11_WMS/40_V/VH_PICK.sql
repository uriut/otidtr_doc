-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- ピッキング指示書
--*******************************************
DROP VIEW VH_PICK;
CREATE VIEW VH_PICK
(
	PICKNO_CD					,
	PALLETDETNO_CD				,
	ROW_NR						,
	STPICK_ID					,
	TYPEPICK_CD					,
	TYPEPALLET_CD				,
	TYPEPALLET_TX				,
	TRANSPORTER_CD				,
	TRANSPORTER_TX				,
	TRANSPORTER2_TX				,	-- 暫定対応
	DTNAME_TX					,
	DTADDRESS_TX				,
	SHIPBERTH_CD				,
	SHIPPLAN_TX					,
	--
	SRCLOCA_CD					,
	SRCLOCA_TX					,
	KANBAN_CD					,
	SKU_CD						,
	SKU_TX						,
	SKU2_TX						,
	LOT_TX						,
	EXTERIOR_CD					,
	SIZE_TX						,
	AMOUNT_NR					,
	PALLETCAPACITY_NR			,
	PALLET_NR					,
	CASE_NR						,
	BARA_NR						,
	PCS_NR						,
	DEFER_FL					,
	SKUWEIGHT_NR				,	-- 明細単位の重量(g)
	SRCHTUSER_CD				,
	SRCHTUSER_TX				,
	PRINTER_TX					,
	SORTGROUP_ID				,
	SORTWEIGHT_ID
) AS
SELECT
	SUB.PICKNO_CD				,
	SUB.PALLETDETNO_CD			,
	ROW_NUMBER() OVER (PARTITION BY SUB.PALLETDETNO_CD ORDER BY SUB.SORTGROUP_ID, SUB.SORTWEIGHT_ID DESC, SUB.SRCLOCA_CD) AS ROW_NR,	-- ピック順
	SUB.STPICK_ID				,
	SUB.TYPEPICK_CD				,
	SUB.TYPEPALLET_CD			,
	SUB.TYPEPALLET_TX			,
	SUB.TRANSPORTER_CD			,
	SUB.TRANSPORTER_TX			,
	SUB.TRANSPORTER_TX			,
	SUB.DTNAME_TX				,
	NVL(TRIM(SUB.DTADDRESS1_TX) || TRIM(SUB.DTADDRESS2_TX) || TRIM(SUB.DTADDRESS3_TX) || TRIM(SUB.DTADDRESS4_TX),' '),
	SUB.SHIPBERTH_CD			,
	SUB.SHIPPLAN_TX				,
	--
	SUB.SRCLOCA_CD				,
	SUB.SRCLOCA_TX				,
	SUB.KANBAN_CD				,
	SUB.SKU_CD					,
	SUB.SKU_TX					,
	SUB.SKU2_TX					,
	SUB.LOT_TX					,
	SUB.EXTERIOR_CD				,
	SUB.SIZE_TX					,
	SUB.AMOUNT_NR				,
	SUB.PALLETCAPACITY_NR		,
	SUB.PALLET_NR + TRUNC(SUB.CASE_NR / SUB.PALLETCAPACITY_NR)	,
	MOD(SUB.CASE_NR,SUB. PALLETCAPACITY_NR) + TRUNC(SUB.BARA_NR / SUB.AMOUNT_NR),
	MOD(SUB.BARA_NR,SUB.AMOUNT_NR),
	SUB.PCS_NR					,
	SUB.DEFER_FL				,
	SUB.SKUWEIGHT_NR			,
	SUB.SRCHTUSER_CD			,
	SUB.SRCHTUSER_TX			,
	' '							,
	SUB.SORTGROUP_ID			, 
	SUB.SORTWEIGHT_ID
FROM
	(
	SELECT
		TP.PICKNO_CD				,
		TP.PALLETDETNO_CD			,
		TP2.STPICK_ID				,
		TP.TYPEPICK_CD				,
		TP.TYPEPLALLET_CD AS TYPEPALLET_CD,
		CASE TP.TYPEPLALLET_CD
			WHEN '01' THEN 'JPR'
			WHEN '02' THEN 'ﾋﾞｰﾙ'
		ELSE ' '
		END AS TYPEPALLET_TX		,
		TP.TRANSPORTER_CD			,
		MT.TRANSPORTERGROUP_TX AS TRANSPORTER_TX,
		TP.TOTALPIC_FL				,
		' ' AS DTNAME_TX			,	-- todo
		' ' AS DTADDRESS1_TX		,	-- todo
		' ' AS DTADDRESS2_TX		,	-- todo
		' ' AS DTADDRESS3_TX		,	-- todo
		' ' AS DTADDRESS4_TX		,	-- todo
		TP.SHIPBERTH_CD				,
		TO_CHAR(TP.SHIPPLAN_DT,'HH24:MI:SS') AS SHIPPLAN_TX,
		--
		TP.SRCLOCA_CD				,
		ML.LOCA_TX AS SRCLOCA_TX	,
		TP.KANBAN_CD				,
		TP.SKU_CD					,
		MS.SKU_TX					,
		MS.SKU2_TX					,
		TP.LOT_TX					,
		MS.EXTERIOR_CD				,
		MS.SIZE_TX					,
		MS.PCSPERCASE_NR AS AMOUNT_NR,	-- 入数
		TP.PALLETCAPACITY_NR		,
		-- MS.WEIGHT_NR				,	-- ケース重量
		SUM(TP.PALLET_NR) AS PALLET_NR,
		SUM(TP.CASE_NR) AS CASE_NR	,
		SUM(TP.BARA_NR) AS BARA_NR	,
		SUM(TP.PCS_NR) AS PCS_NR	,
		MAX(TP.DEFER_FL) AS DEFER_FL,
		( SUM(TP.PCS_NR) / MS.PCSPERCASE_NR ) * MS.WEIGHT_NR AS SKUWEIGHT_NR,	-- PCS数から重量を取得
		TP.SRCHTUSER_CD				,
		TP.SRCHTUSER_TX				,
		CASE         
			WHEN SUM(TP.CASE_NR) >= 21 OR TP.TOTALPIC_FL = 0 THEN 1 --軒先ピックは常に重量降順
			ELSE 2
		END AS SORTGROUP_ID			,
		CASE         
			WHEN SUM(TP.CASE_NR) >= 21 OR TP.TOTALPIC_FL = 0 THEN ( SUM(TP.PCS_NR) / MS.PCSPERCASE_NR ) * MS.WEIGHT_NR --軒先ピックは常に重量降順
			ELSE 0
		END AS SORTWEIGHT_ID
	FROM
		TA_PICK TP
			LEFT OUTER JOIN (
							SELECT
								TRANSPORTERGROUP_CD,
								TRANSPORTERGROUP_TX
							FROM
								MA_TRANSPORTER
							GROUP BY
								TRANSPORTERGROUP_CD,
								TRANSPORTERGROUP_TX
							) MT
				ON TP.TRANSPORTER_CD = MT.TRANSPORTERGROUP_CD
			LEFT OUTER JOIN MA_SKU MS
				ON TP.SKU_CD = MS.SKU_CD
			LEFT OUTER JOIN MA_LOCA ML
				ON TP.SRCLOCA_CD = ML.LOCA_CD
			LEFT OUTER JOIN (
							SELECT
								PALLETDETNO_CD	,
								SKU_CD			,
								LOT_TX			,
								SRCLOCA_CD		,
								MIN(STPICK_ID) AS STPICK_ID
							FROM
								TA_PICK
							GROUP BY
								PALLETDETNO_CD	,
								SKU_CD			,
								LOT_TX			,
								SRCLOCA_CD
							) TP2
				ON TP.PALLETDETNO_CD = TP2.PALLETDETNO_CD AND
					TP.SKU_CD = TP2.SKU_CD AND
					TP.LOT_TX = TP2.LOT_TX AND
					TP.SRCLOCA_CD = TP2.SRCLOCA_CD
	GROUP BY
		TP.PICKNO_CD				,
		TP.PALLETDETNO_CD			,
		TP.TYPEPICK_CD				,
		TP2.STPICK_ID				,
		TP.TYPEPLALLET_CD			,
		CASE TP.TYPEPLALLET_CD
			WHEN '01' THEN 'JPR'
			WHEN '02' THEN 'ﾋﾞｰﾙ'
		ELSE ' '
		END							,
		TP.TRANSPORTER_CD			,
		MT.TRANSPORTERGROUP_TX		,
		TP.TOTALPIC_FL				,
		TP.SHIPBERTH_CD				,
		TO_CHAR(TP.SHIPPLAN_DT,'HH24:MI:SS'),
		--
		TP.SRCLOCA_CD				,
		ML.LOCA_TX					,
		TP.KANBAN_CD				,
		TP.SKU_CD					,
		MS.SKU_TX					,
		MS.SKU2_TX					,
		TP.LOT_TX					,
		MS.EXTERIOR_CD				,
		MS.SIZE_TX					,
		MS.PCSPERCASE_NR			,	-- 入数
		TP.PALLETCAPACITY_NR		,
		MS.WEIGHT_NR				,
		TP.SRCHTUSER_CD				,
		TP.SRCHTUSER_TX
	) SUB
--WHERE
--	SUB.TOTALPIC_FL = 1	-- todo
ORDER BY
	SUB.PICKNO_CD				,
	SUB.PALLETDETNO_CD			,
	SUB.SORTGROUP_ID 			,
	SUB.SORTWEIGHT_ID DESC		, --ケース数が21以上のもの:従来通り個数 * ケース当たり重量の降順
	SUB.SRCLOCA_CD				  --ケース数が20以下のもの:ロケーションコードの昇順
;
