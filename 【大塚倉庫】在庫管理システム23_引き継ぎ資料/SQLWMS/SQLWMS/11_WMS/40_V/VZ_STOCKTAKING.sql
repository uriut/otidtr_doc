
  CREATE OR REPLACE FORCE VIEW "WMS"."VZ_STOCKTAKING" ("MANAGER_NUM", "STATUS_TX", "LOCA_CD_C", "LOCA_CD", "KANBAN_CD", "SIZE_TX", "EXTERIOR_CD", "LOT_TX", "PALLETCAPACITY_NR", "SKU_TX", "UPD_DT", "SEND_DT", "PALLET_NR", "INPUTPALLET_NR", "CASE_NR", "INPUTCASE_NR", "BARA_NR", "INPUTBARA_NR", "RESULT_TX", "OPERATOR_TX", "AUTHORIZER_TX") AS 
	SELECT
		MANAGER_NUM							, 	-- 在庫更新ID
		STATUS_TX							,	-- 棚卸ステータス（フェーズ1の場合は「未作業」、「未完了」、「完了」）
		SUBSTR(LOCA_CD,0,7) AS LOCA_CD_C  	,	-- ロケ列コード
		LOCA_CD								,	-- ロケコード
		KANBAN_CD							,	-- かんばん番号
		SIZE_TX     						,	-- 規格
		EXTERIOR_CD							,	-- 外装
		LOT_TX								,	-- ロット
		PALLETCAPACITY_NR 					,	-- パレット積み付け数
		SKU_TX								,	-- 商品名
		UPD_DT								,	-- 棚卸データ作成日時
		SEND_DT 							,	-- 棚卸データ作業日時
		PALLET_NR							,	-- パレット数
		INPUTPALLET_NR						,	-- 入力されたパレット数
		CASE_NR								,	-- ケース数
		INPUTCASE_NR						,	-- 入力されたケース数
		BARA_NR								,	-- バラ数
		INPUTBARA_NR						,	-- 入力されたバラ数
		RESULT_TX							,	-- 棚卸作業結果
		OPERATOR_TX							,	-- 作業者名
		AUTHORIZER_TX							-- 承認者名
	FROM
		TA_STOCKTAKING
	GROUP BY
		MANAGER_NUM							,	
		STATUS_TX							,
    SUBSTR(LOCA_CD,0,7)						,
		LOCA_CD								,
		KANBAN_CD							,
		SIZE_TX     						,
		EXTERIOR_CD							,
		LOT_TX								,
		PALLETCAPACITY_NR 					,
		SKU_TX								,
		UPD_DT								,
		SEND_DT 							,
		PALLET_NR							,
		INPUTPALLET_NR						,
		CASE_NR								,
		INPUTCASE_NR						,
		BARA_NR								,
		INPUTBARA_NR						,
		RESULT_TX							,
		OPERATOR_TX							,
		AUTHORIZER_TX
    ORDER BY
		LOCA_CD								;
 
