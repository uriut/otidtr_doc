-- *******************************************
-- WA706 受払
-- *******************************************
DROP VIEW VW_WA706;
CREATE VIEW VW_WA706
(
	STOCKINOUT_ID			,	-- 在庫更新ID 
	LOCA_CD					,	-- ロケーションコード
	LOCA_TX					,	-- ロケーションコード名
	SKU_CD					,	-- SKUコード
	EXTERIOR_CD				,	-- 外装コード
	LOT_TX					,	-- ロット
	SKU_TX					,	-- 商品名
	USELIMIT_DT				,	-- 使用期限
	STOCK_DT				,	-- 格納日
	CASEPERPALLET_NR		,	-- 入数 パレット
	AMOUNT_NR				,	-- 入数 標準ケース・メーカー箱の入数
	PALLET_NR				,	-- パレット数
	CASE_NR					,	-- ケース数
	BARA_NR					,	-- バラ数
	PCS_NR					,	-- PCS数
	TYPESTOCK_CD			,	-- 在庫タイプコード
	TYPESTOCK_TX			,	-- 在庫タイプ
	TYPEOPERATION_CD		,	-- 業務区分コード
	TYPEOPERATION_TX		,	-- 業務区分
	TYPEREF_CD				,	-- 参照タイプコード（システム上の参照先）
	TYPEREF_TX				,	-- 参照タイプコード（システム上の参照先）
	REFNO_ID				,	-- 参照ID
	MEMO_TX					,	-- 備考
	-- 管理項目
	ADD_DT					,	-- 登録日時
	ADD_TX					,	-- 登録日時（表示用）
	ADDUSER_CD				,	-- 登録社員コード
	ADDUSER_TX					-- 登録社員名
) AS
SELECT
	TSIO.STOCKINOUT_ID			,
	TSIO.LOCA_CD				,
	MLO.LOCA_TX					,
	TSIO.SKU_CD					,
	MSK.EXTERIOR_CD				,
	TSIO.LOT_TX					,
	MSK.SKU_TX					,
	TSIO.USELIMIT_DT			,
	TSIO.STOCK_DT				,
	TSIO.CASEPERPALLET_NR		,
	TSIO.AMOUNT_NR				,
	TSIO.PALLET_NR				,
	TSIO.CASE_NR				,
	TSIO.BARA_NR				,
	TSIO.PCS_NR					,
	TSIO.TYPESTOCK_CD			,
	MTS.TYPESTOCK_TX			,
	TSIO.TYPEOPERATION_CD		,
	MTO.TYPEOPERATION_TX		,
	TSIO.TYPEREF_CD				,
	MTR.TYPEREF_TX				,
	TSIO.REFNO_ID				,
	TSIO.MEMO_TX				,
	-- 管理項目
	TSIO.ADD_DT					,
	TO_CHAR(TSIO.ADD_DT, 'YYYY/MM/DD HH24:MI:SS'),
	TSIO.ADDUSER_CD				,
	TSIO.ADDUSER_TX				
FROM
	TA_STOCKINOUT TSIO,
	MB_TYPESTOCK MTS,
	MB_TYPEOPERATION MTO,
	MB_TYPEREF MTR,
	MA_SKU MSK,
	MA_LOCA MLO
WHERE
	TSIO.TYPESTOCK_CD = MTS.TYPESTOCK_CD (+) AND
	TSIO.TYPEOPERATION_CD = MTO.TYPEOPERATION_CD (+) AND
	TSIO.TYPEREF_CD = MTR.TYPEREF_CD (+) AND
	TSIO.SKU_CD = MSK.SKU_CD AND
	TSIO.LOCA_CD = MLO.LOCA_CD (+)
;
