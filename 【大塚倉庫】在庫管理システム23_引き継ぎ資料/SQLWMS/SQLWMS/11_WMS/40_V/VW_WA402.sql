-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- 入荷予定詳細照会
--*******************************************
DROP VIEW VW_WA402;
CREATE VIEW VW_WA402
(
	ARRIV_ID					,	/* 入荷予定ID */
	ARRIVORDERNO_CD				,	/* 入荷予定番号(発注番号) */
	STARRIV_ID					,	/* 入荷予定状態ID */
	STARRIV_TX					,	/* 入荷予定状態 */
	SLIPNO_TX					,	/* 伝票番号 */
	ARRIVPLAN_DT				,	/* 入荷予定日 */
	ARRIVPLANTIME_TX			,	/* 入荷予定時刻 */
	SKU_CD						,	/* 統一商品コード */
	EXTERIOR_CD					,	/* 外装コード */
	SKUBARCODE_TX				,	/* JANコード */
	ITFCODE_TX					,	/* ITFコード */
	SKU_TX						,	/* 商品名 */
	PCSPERCASE_NR				,	/* 規格 */
	SIZE_TX						,	/* 規格 */
	LOT_TX						,	/* 製造ロット */
	ARRIVPLANBARA_NR			,	/* バラ数 */
	ARRIVPLANCASE_NR			,	/* ケース数 */
	ARRIVPLANPALLET_NR			,	/* パレット数 */
	TYPEARRIV_CD				,	/* 入荷予定タイプコード(即出荷の判定用) */
	SAMEDAYSHIP_FL				,	/* 即出荷フラグ */
	SAMEDAYSHIP_TX				,	/* 即出荷フラグ(表示用) */
	CHECK_DT					,	/* 検品完了日時 */
	CHECKUSER_CD				,	/* 検品者コード */
	CHECKUSER_TX				,	/* 検品者名 */
	INSTOCKLOT_TX				,	
	APPLYINSTOCKNPALLET_NR		,
	APPLYINSTOCKNCASE_NR		,
	ARRIVEND_DT					,	/* 受付日 */
	ARRIVENDUSER_CD				,	/* 受付者コード */
	ARRIVENDUSER_TX				,	/* 受付者名 */
	STOCK_DT					,	/* 入庫日時 */
	STOCKUSER_CD				,	/* 入庫者コード */
	STOCKUSER_TX				,	/* 入庫者名 */
	SUPPLIER_CD					,	/* 仕入先コード */
	SUPPLIER_TX					,	/* 仕入先名 */
	TRANSPORTER_CD				,	/* 配送業者コード */
	TRANSPORTER_TX				,	/* 配送業者名 */
	CARNUMBER_CD				,	/* 車番 */
	ADD_DT								/* 登録日時 */
) AS
SELECT
	MAX(TAR.ARRIV_ID) AS ARRIV_ID	,	/* 入荷予定ID */
	TAR.ARRIVORDERNO_CD				,	/* 入荷予定番号(発注番号) */
	TAR.STARRIV_ID					,	/* 入荷予定状態ID */
	MSA.STARRIV_TX					,	/* 入荷予定状態 */
	TAR.SLIPNO_TX					,	/* 伝票番号 */
	MAX(TAR.ARRIVPLAN_DT) AS ARRIVPLAN_DT,	/* 入荷予定日 */
	TO_CHAR(MAX(TAR.ARRIVPLAN2_DT),'hh24:mi:ss') 
				AS ARRIVPLANTIME_TX	,	/* 入荷予定時刻 */
	TAR.SKU_CD						,	/* 統一商品コード */
	MSK.EXTERIOR_CD					,	/* 外装コード */
	MSK.SKUBARCODE_TX				,	/* JANコード */
	MSK.ITFCODE_TX					,	/* ITFコード */
	MSK.SKU_TX						,	/* 商品名 */
	MSK.PCSPERCASE_NR				,	/* 規格 */
	MSK.SIZE_TX						,	/* 規格 */
	TAR.LOT_TX						,	/* 製造ロット */
	SUM(TAR.ARRIVPLANBARA_NR) AS ARRIVPLANBARA_NR			,	/* バラ数 */
	SUM(TAR.ARRIVPLANCASE_NR) AS	ARRIVPLANCASE_NR		,	/* ケース数 */
	SUM(TAR.ARRIVPLANPALLET_NR) AS ARRIVPLANPALLET_NR		,	/* パレット数 */
	TAR.TYPEARRIV_CD				,	/* 入荷予定タイプコード(即出荷の判定用) */
	TAR.SAMEDAYSHIP_FL				,	/* 即出荷フラグ */
	DECODE(TAR.SAMEDAYSHIP_FL, 0, '', 1, '即出荷')
		AS SAMEDAYSHIP_TX			,	/* 即出荷フラグ(表示用) */
	MAX(TAR.CHECK_DT) AS CHECK_DT	,	/* 検品完了日時 */
	TAR.CHECKUSER_CD				,	/* 検品者コード */
	TAR.CHECKUSER_TX				,	/* 検品者名 */
	CASE 								/* 検品ロット*/
		WHEN TIN.STINSTOCK_ID >= 0 THEN
			TIN.LOT_TX
		ELSE
			'-'
	END as INSTOCKLOT_TX			,
	CASE								/* 検品パレット数*/
		WHEN TIN.STINSTOCK_ID >= 0 THEN
			TIN.APPLYINSTOCKNPALLET_NR			
		ELSE
			NULL
	END as APPLYINSTOCKNPALLET_NR	,
	CASE								/* 検品ケース数*/
		WHEN TIN.STINSTOCK_ID >= 0 THEN
			TIN.APPLYINSTOCKNCASE_NR
		ELSE
			NULL
	END as APPLYINSTOCKNCASE_NR		,
	MAX(TAR.ARRIVEND_DT)	 AS ARRIVEND_DT	,	/* 受付日 */
	TAR.ARRIVENDUSER_CD				,	/* 受付者コード */
	TAR.ARRIVENDUSER_TX				,	/* 受付者名 */
	MAX(TIN.DESTEND_DT) AS STOCK_DT	,	/* 入庫日時 */
	TIN.DESTHTUSER_CD AS STOCKUSER_CD,	/* 入庫者コード */
	TIN.DESTHTUSER_TX AS STOCKUSER_TX,	/* 入庫者名 */
	TAR.SUPPLIER_CD					,	/* 仕入先コード */
	TAR.SUPPLIER_TX					,	/* 仕入先名 */
	TAR.TRANSPORTER_CD				,	/* 配送業者コード */
	TAR.TRANSPORTER_TX				,	/* 配送業者名 */
	TAR.CARNUMBER_CD				,	/* 車番 */
	MAX(TAR.ADD_DT) AS ADD_DT			/* 登録日時 */
FROM
	TA_ARRIV TAR
		LEFT OUTER JOIN MA_SKU MSK
			ON TAR.SKU_CD = MSK.SKU_CD
		LEFT OUTER JOIN MB_STARRIV MSA
			ON TAR.STARRIV_ID = MSA.STARRIV_ID
		LEFT OUTER JOIN (
						SELECT
							ARRIV_ID	,
							STINSTOCK_ID,
							LOT_TX		,
							APPLYINSTOCKNPALLET_NR	,
							APPLYINSTOCKNCASE_NR	,
							DESTEND_DT				,
							DESTHTUSER_CD			,
							DESTHTUSER_TX
						FROM
							TA_INSTOCK
						WHERE
							STINSTOCK_ID <> -2
							) TIN
			ON TAR.ARRIV_ID = TIN.ARRIV_ID
GROUP BY
	TAR.ARRIVORDERNO_CD				,	/* 入荷予定番号(発注番号) */
	TAR.STARRIV_ID					,	/* 入荷予定状態ID */
	MSA.STARRIV_TX					,	/* 入荷予定状態 */
	TAR.SLIPNO_TX					,	/* 伝票番号 */
	TAR.SKU_CD						,	/* 統一商品コード */
	MSK.EXTERIOR_CD					,	/* 外装コード */
	MSK.SKUBARCODE_TX				,	/* JANコード */
	MSK.ITFCODE_TX					,	/* ITFコード */
	MSK.SKU_TX						,	/* 商品名 */
	MSK.PCSPERCASE_NR				,	/* 規格 */
	MSK.SIZE_TX						,	/* 規格 */
	TAR.LOT_TX						,	/* 製造ロット */
	TAR.TYPEARRIV_CD				,	/* 入荷予定タイプコード(即出荷の判定用) */
	TAR.SAMEDAYSHIP_FL				,	/* 即出荷フラグ */
	TAR.CHECKUSER_CD				,	/* 検品者コード */
	TAR.CHECKUSER_TX				,	/* 検品者名 */
	TAR.ARRIVENDUSER_CD				,	/* 受付者コード */
	TAR.ARRIVENDUSER_TX				,	/* 受付者名 */
	TIN.DESTHTUSER_CD				,	/* 入庫者コード */
	TIN.DESTHTUSER_TX				,	/* 入庫者名 */
	TIN.LOT_TX						,	/* 検品ロット*/
	TIN.APPLYINSTOCKNPALLET_NR		,	/* 検品パレット数 */
	TIN.APPLYINSTOCKNCASE_NR		,	/* 検品ケース数 */
	TIN.STINSTOCK_ID				,	/* 格納状態 */
	TAR.SUPPLIER_CD					,	/* 仕入先コード */
	TAR.SUPPLIER_TX					,	/* 仕入先名 */
	TAR.TRANSPORTER_CD				,	/* 配送業者コード */
	TAR.TRANSPORTER_TX				,	/* 配送業者名 */
	TAR.CARNUMBER_CD					/* 車番 */
ORDER BY
	TAR.ARRIVORDERNO_CD	,
	TAR.STARRIV_ID		,
	TAR.SKU_CD			,
	TAR.LOT_TX
;
