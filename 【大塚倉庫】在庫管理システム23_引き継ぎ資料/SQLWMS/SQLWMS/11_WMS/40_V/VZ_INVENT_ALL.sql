-- Copyright (C) Seaos Corporation 2014 All rights reserved
-- *******************************************
-- 棚卸表（総棚）
-- *******************************************

DROP VIEW VZ_INVENT_ALL ;
CREATE VIEW VZ_INVENT_ALL
(
	LOCA_TX					,	-- ロケーション名
	EXTERIOR_CD				,	-- 外装コード
	SKU_TX					,	-- 商品名
	KANBAN_CD				,	-- かんばん番号
	LOT_TX					,	-- ロット
	PALLETCAPACITY_NR		,	-- パレット積み付け数
	IMPUTPALLET_TX			,
	IMPUTCASE_TX			,
	PALLET_NR				,
	CASE_NR					,
	ANSWERCASE_NR			,
	NES_FL						-- 1:ネス、0:その他
) AS
	SELECT
		CASE
			WHEN ML.SHIPVOLUME_CD <> '03' THEN SUBSTR(ML.LOCA_TX,7,11)
			ELSE SUBSTR(ML.LOCA_TX,7,15)
		END AS LOCA_TX		,
		MS.EXTERIOR_CD		,
		MS.SKU2_TX			,
		LP.KANBAN_CD		,
		LP.LOT_TX			,
		LP.PALLETCAPACITY_NR,
		'' AS IMPUTPALLET_TX,	-- 入力用
		'' AS IMPUTCASE_TX	,	-- 入力用
		SUM(LP.PALLET_NR)	,	-- 総パレット数
		SUM(LP.CASE_NR)		,	-- 総ケース数
		CASE
			WHEN SUM(LP.PALLET_NR) = 0 THEN SUM(LP.CASE_NR)
			ELSE (SUM(LP.PALLET_NR) * LP.PALLETCAPACITY_NR) + SUM(LP.CASE_NR)
		END ANSWERCASE_NR	,	-- 答え
		CASE
			WHEN ML.SHIPVOLUME_CD <> '03' THEN 0
			ELSE 1
 		END NES_FL
	FROM
		MA_LOCA ML
			LEFT OUTER JOIN (
					SELECT
						LOCA_CD				,
						KANBAN_CD			,
						SKU_CD				,
						LOT_TX				,
						PALLETCAPACITY_NR	,
						PALLET_NR			,
						CASE_NR				,
						BARA_NR				,
						PCS_NR
					FROM
						TA_LOCASTOCKP
					UNION ALL
					SELECT
						LOCA_CD				,
						KANBAN_CD			,
						SKU_CD				,
						LOT_TX				,
						PALLETCAPACITY_NR	,
						SUM(PALLET_NR) AS PALLET_NR	,
						SUM(CASE_NR) AS CASE_NR		,
						SUM(BARA_NR) AS BARA_NR		,
						SUM(PCS_NR) AS PCS_NR
					FROM
						TA_LOCASTOCKC
					WHERE
						STSTOCK_ID = 0
					GROUP BY
						LOCA_CD				,
						KANBAN_CD			,
						SKU_CD				,
						LOT_TX				,
						PALLETCAPACITY_NR
			) LP
				ON ML.LOCA_CD = LP.LOCA_CD
			LEFT OUTER JOIN MA_SKU MS
				ON LP.SKU_CD = MS.SKU_CD
	GROUP BY
		ML.LOCA_CD			,
		CASE
			WHEN ML.SHIPVOLUME_CD <> '03' THEN SUBSTR(ML.LOCA_TX,7,11)
			ELSE SUBSTR(ML.LOCA_TX,7,15)
		END					,
		MS.EXTERIOR_CD		,
		MS.SKU2_TX			,
		LP.KANBAN_CD		,
		LP.LOT_TX			,
		LP.PALLETCAPACITY_NR,
		ML.SHIPVOLUME_CD
	ORDER BY
		ML.LOCA_CD
;
