-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- SKUマスタ
--*******************************************
DROP VIEW VW_WA301;
CREATE VIEW VW_WA301
(
	SKU_CD						,	/* 商品コード */
	WAREHOUSESKU_CD				,	/* 物流コード */
	EXTERIOR_CD					,	/* 外装コード */
	SIZE_TX						,	/* 規格容量カナ・ケース */
	SKUBARCODE_TX				,	/* JANコード */
	ITFCODE_TX					,	/* ITFコード */
	SKU_TX						,	/* 商品名 */
	MANUFACTURERSKU_TX			,	/* 電略 */
	OWNER_CD					,	/* 荷主コード */
	TYPEBILL1_CD				,	/* 請求区分 */
	TYPEBILL2_CD				,	/* 請求区分2 */
	GS1_CD						,	/* GS1コード */
	PCSPERCASE_NR				,	/* ケース入数 */
	DEPOISTPRICE_NR				,	/* 寄託金額（銭） */
	STORAGECHARGEA_NR			,	/* 保管量(甲) */
	STORAGECHARGEB_NR			,	/* 保管量(乙) */
	STEVEDORAGEA_NR				,	/* 荷役料(甲) */
	STEVEDORAGEB_NR				,	/* 荷役料(乙) */
	NESTAINERINSTOCKRATE_NR		,	/* 格納割合-ネステナ */
	CASESTOCKRATE_NR			,	/* 格納割合-ケース成り */
	TYPEPACKAGING_CD			,	/* 出荷荷姿 */
	LENGTH_NR					,	/* ケース商品サイズ・縦(mm) */
	WIDTH_NR					,	/* ケース商品サイズ・横(mm) */
	HEIGHT_NR					,	/* ケース商品サイズ・高さ(mm) */
	WEIGHT_NR					,	/* ケース商品サイズ・重量(g) */
	ABC_ID						,	/* ロケーションABCID */
	ABC_CD						,	/* ロケーションABC */
	ABCSUB_ID					,	/* 重量グループID */
	ABCSUB_TX					,	/* 重量グループ */
	ADD_DT						,	/* 登録日時 */
	ADDUSER_CD					,	/* 登録ユーザコード */
	ADDUSER_TX					,	/* 登録ユーザ名 */
	UPD_DT						,	/* 更新日時 */
	UPDUSER_CD					,	/* 更新ユーザコード */
	UPDUSER_TX					,	/* 更新ユーザ名 */
	UPDPROGRAM_CD				,	/* 更新プログラムコード */
	UPDCOUNTER_NR				,	/* 更新カウンター */
	STRECORD_ID						/* レコード状態 */
) AS
SELECT
	MSK.SKU_CD						,	/* 商品コード */
	MSK.WAREHOUSESKU_CD				,	/* 物流コード */
	MSK.EXTERIOR_CD					,	/* 外装コード */
	MSK.SIZE_TX						,	/* 規格容量カナ・ケース */
	MSK.SKUBARCODE_TX				,	/* JANコード */
	MSK.ITFCODE_TX					,	/* ITFコード */
	MSK.SKU_TX						,	/* 商品名 */
	MSK.MANUFACTURERSKU_TX			,	/* 電略 */
	MSK.OWNER_CD					,	/* 荷主コード */
	MSK.TYPEBILL1_CD				,	/* 請求区分 */
	MSK.TYPEBILL2_CD				,	/* 請求区分2 */
	MSK.GS1_CD						,	/* GS1コード */
	MSK.PCSPERCASE_NR				,	/* ケース入数 */
	MSK.DEPOISTPRICE_NR				,	/* 寄託金額（銭） */
	MSK.STORAGECHARGEA_NR			,	/* 保管量(甲) */
	MSK.STORAGECHARGEB_NR			,	/* 保管量(乙) */
	MSK.STEVEDORAGEA_NR				,	/* 荷役料(甲) */
	MSK.STEVEDORAGEB_NR				,	/* 荷役料(乙) */
	MSK.NESTAINERINSTOCKRATE_NR		,	/* 格納割合-ネステナ */
	MSK.CASESTOCKRATE_NR			,	/* 格納割合-ケース成り */
	MSK.TYPEPACKAGING_CD			,	/* 出荷荷姿 */
	MSK.LENGTH_NR					,	/* ケース商品サイズ・縦(mm) */
	MSK.WIDTH_NR					,	/* ケース商品サイズ・横(mm) */
	MSK.HEIGHT_NR					,	/* ケース商品サイズ・高さ(mm) */
	MSK.WEIGHT_NR					,	/* ケース商品サイズ・重量(g) */
	MSK.ABC_ID						,	/* ロケーションABCID */
	MAABC.ABC_CD					,	/* ロケーションABC */
	MSK.ABCSUB_ID					,	/* 重量グループID */
	MASUB.ABCSUB_TX					,	/* 重量グループ */
	MSK.ADD_DT						,	/* 登録日時 */
	MSK.ADDUSER_CD					,	/* 登録ユーザコード */
	MSK.ADDUSER_TX					,	/* 登録ユーザ名 */
	MSK.UPD_DT						,	/* 更新日時 */
	MSK.UPDUSER_CD					,	/* 更新ユーザコード */
	MSK.UPDUSER_TX					,	/* 更新ユーザ名 */
	MSK.UPDPROGRAM_CD				,	/* 更新プログラムコード */
	MSK.UPDCOUNTER_NR				,	/* 更新カウンター */
	MSK.STRECORD_ID						/* レコード状態 */
FROM
	MA_SKU MSK
	LEFT OUTER JOIN MA_ABC MAABC
		ON MSK.ABC_ID = MAABC.ABC_ID
	LEFT OUTER JOIN MA_ABCSUB MASUB
		ON MSK.ABCSUB_ID = MASUB.ABCSUB_ID
;
