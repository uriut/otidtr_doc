
	SELECT
		SUBSTR(UPD_DT,0,8) AS MANAGER_NUM	,	
		'未作業' AS STATUS		,
		LOCA_CD			,
		KANBAN_CD		,
		EXTERIOR_CD		,
		SKU_TX			,
		SIZE_TX			,
		LOT_TX			,
		UPD_DT			,
		SEND_DT 		,
		PALLET_NR		,
		CASE_NR			,
		BARA_NR			,
		PCS_NR			,
		'' AS INPUTPALLET_TX,	-- 入力用
		'' AS INPUTCASE_TX	,	-- 入力用
		'' AS INPUTBARA_TX	,	-- 入力用
		'' AS INPUTPCS_TX	,	-- 入力用
		UPDUSER_CD	
	FROM
		TA_STOCKTAKING
	GROUP BY
		SUBSTR(UPD_DT,0,8)	,	
		'未作業' 			,
		LOCA_CD				,
		KANBAN_CD			,	
		EXTERIOR_CD			,
		SKU_TX				,
		SIZE_TX				,
		LOT_TX				,
		UPD_DT 				,
		SEND_DT 			,
		PALLET_NR			,
		CASE_NR				,
		BARA_NR				,
		PCS_NR				,
		UPDUSER_CD	
	ORDER BY
		LOCA_CD
 