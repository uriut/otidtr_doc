-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- 配送業者マスタ
--*******************************************
DROP VIEW VW_WA351;
CREATE VIEW VW_WA351
(
	TRANSPORTER_CD		,	/* 配送業者コード */
	TRANSPORTER_TX		,	/* 配送業者名 */
	TRANSPORTERGROUP_CD	,	/* 配送業者グループコード */
	TRANSPORTERGROUP_TX	,	/* 配送業者グループ名 */
	TOTALPIC_FL			,	/* トータルピックフラグ */
	TOTALPIC_TX			,	/* ピック名 */
	PICKORDER_NR		,	/* 引当優先順位 */
	ADD_DT				,	/* 登録日時 */
	ADDUSER_CD			,	/* 登録ユーザコード */
	ADDUSER_TX			,	/* 登録ユーザ名 */
	UPD_DT				,	/* 更新日時 */
	UPDUSER_CD			,	/* 更新ユーザコード */
	UPDUSER_TX			,	/* 更新ユーザ名 */
	UPDPROGRAM_CD		,	/* 更新プログラムコード */
	UPDCOUNTER_NR		,	/* 更新カウンター */
	STRECORD_ID				/* レコード状態 */
) AS
SELECT
	MTR.TRANSPORTER_CD		,	/* 配送業者コード */
	MTR.TRANSPORTER_TX		,	/* 配送業者名 */
	MTR.TRANSPORTERGROUP_CD	,	/* 配送業者グループコード */
	MTR.TRANSPORTERGROUP_TX	,	/* 配送業者グループ名 */
	MTR.TOTALPIC_FL			,	/* トータルピックフラグ */
	MBT.TOTALPIC_TX			,	/* ピック名 */
	MTR.PICKORDER_NR		,	/* 引当優先順位 */
	MTR.ADD_DT				,	/* 登録日時 */
	MTR.ADDUSER_CD			,	/* 登録ユーザコード */
	MTR.ADDUSER_TX			,	/* 登録ユーザ名 */
	MTR.UPD_DT				,	/* 更新日時 */
	MTR.UPDUSER_CD			,	/* 更新ユーザコード */
	MTR.UPDUSER_TX			,	/* 更新ユーザ名 */
	MTR.UPDPROGRAM_CD		,	/* 更新プログラムコード */
	MTR.UPDCOUNTER_NR		,	/* 更新カウンター */
	MTR.STRECORD_ID				/* レコード状態 */
FROM
	MA_TRANSPORTER MTR
		LEFT OUTER JOIN MB_TOTALPIC MBT
			ON MTR.TOTALPIC_FL = MBT.TOTALPIC_FL
;
