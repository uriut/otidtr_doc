--Copyright (C) Seaos Corporation 2014 All rights reserved
-- *******************************************
-- WA524 緊急補充指示照会
-- *******************************************
DROP VIEW VW_WA503;
CREATE VIEW VW_WA503
(
	SUPPNO_CD			,	-- 補充指示NO
	TYPESUPP_CD			,	-- 補充タイプコード（10：緊急、20：一次ピック）
	TYPESUPP_TX			,	-- 補充タイプ名（10：緊急、20：一次ピック）
	START_DT			,	-- 開始日
	END_DT				,	-- 終了日
	STSUPP_ID			,	-- 補充ステータス
	STSUPP_TX			,	-- 補充ステータス名
	SKU_CD				,	-- SKUコード
	SKU_TX				,	-- 商品名
	SIZE_TX				,	-- 規格
	LOT_TX				,	-- ロット
	KANBAN_CD			,	-- かんばん番号
	PALLET_NR			,	-- パレット数
	CASE_NR				,	-- ケース数
	BARA_NR				,	-- バラ数
	PCS_NR				,	-- PCS数
	AMOUNT_NR			,	-- ケース入数
	PALLETCAPACITY_NR	,	-- パレット積み付け数
	-- 元ロケ
	SRCTYPESTOCK_CD		,	-- 在庫タイプコード
	SRCLOCA_CD			,	-- 元ロケーションコード
	SRCLOCA_TX			,	-- 元ロケーション名
	SRCPALLET_NR		,	-- 出庫済みパレット数
	SRCCASE_NR			,	-- 出庫済みケース数
	SRCBARA_NR			,	-- 出庫済みバラ数
	SRCPCS_NR			,	-- 出庫済みPCS数
	SRCSTART_DT			,	-- 元開始時刻
	SRCEND_DT			,	-- 元終了時刻
	SRCHT_ID			,	-- 元HT
	SRCHTUSER_CD		,	-- 元HT社員コード
	SRCHTUSER_TX		,	-- 元HT社員名
	SRCLOCASTOCKC_ID	,	-- 元引当済み在庫ID
	-- 先ロケ
	DESTTYPESTOCK_CD	,	-- 在庫タイプコード
	DESTLOCA_CD			,	-- 先ロケーションコード
	DESTLOCA_TX			,	-- 先ロケーション名
	DESTPALLET_NR		,	-- 入庫済みパレット数
	DESTCASE_NR			,	-- 入庫済みケース数
	DESTBARA_NR			,	-- 入庫済みバラ数
	DESTPCS_NR			,	-- 入庫済みPCS数
	DESTSTART_DT		,	-- 先開始時刻
	DESTEND_DT			,	-- 先終了時刻
	DESTHT_ID			,	-- 先HT
	DESTHTUSER_CD		,	-- 先HT社員コード
	DESTHTUSER_TX		,	-- 先HT社員名
	DESTLOCASTOCKC_ID	,	-- 先引当済み在庫ID
	--
	ADD_DT				,	-- 登録日時
	ADDUSER_CD			,	-- 登録社員コード
	ADDUSER_TX				-- 登録社員名
) AS
SELECT
	TASP.SUPPNO_CD			,
	TASP.TYPESUPP_CD		,
	MBTS.TYPESUPP_TX		,
	TASP.START_DT			,
	TASP.END_DT				,
	TASP.STSUPP_ID			,
	MBSS.STSUPP_TX			,
	TASP.SKU_CD				,
	REPLACE(REPLACE(MASK.SKU_TX, Chr(13), ''), Chr(10), ''),
	MASK.SIZE_TX			,
	TASP.LOT_TX				,
	TASP.SRCKANBAN_CD		,	-- 元かんばん番号
	TASP.PALLET_NR			,
	TASP.CASE_NR			,
	TASP.BARA_NR			,
	TASP.PCS_NR				,
	TASP.AMOUNT_NR			,
	TASP.PALLETCAPACITY_NR	,
	-- 元ロケ
	TASP.SRCTYPESTOCK_CD	,
	TASP.SRCLOCA_CD			,
	NVL(MALC1.LOCA_TX, ' ')	,
	TASP.SRCPALLET_NR		,
	TASP.SRCCASE_NR			,
	TASP.SRCBARA_NR			,
	TASP.SRCPCS_NR			,
	TASP.SRCSTART_DT		,
	TASP.SRCEND_DT			,
	TASP.SRCHT_ID			,
	TASP.SRCHTUSER_CD		,
	TASP.SRCHTUSER_TX		,
	TASP.SRCLOCASTOCKC_ID	,
	-- 先ロケ
	TASP.DESTTYPESTOCK_CD	,
	TASP.DESTLOCA_CD		,
	NVL(MALC2.LOCA_TX, ' ')	,
	TASP.DESTPALLET_NR		,
	TASP.DESTCASE_NR		,
	TASP.DESTBARA_NR		,
	TASP.DESTPCS_NR			,
	TASP.DESTSTART_DT		,
	TASP.DESTEND_DT			,
	TASP.DESTHT_ID			,
	TASP.DESTHTUSER_CD		,
	TASP.DESTHTUSER_TX		,
	TASP.DESTLOCASTOCKC_ID	,
	--
	TASP.ADD_DT				,
	TASP.ADDUSER_CD			,
	TASP.ADDUSER_TX			
FROM
	(
	SELECT
		TASP.SUPPNO_CD						,
		TASP.TYPESUPP_CD					,
		MIN(TASP.START_DT) AS START_DT		,
		MAX(TASP.END_DT) AS END_DT			,
		MIN(TASP.STSUPP_ID) AS STSUPP_ID	,
		TASP.SKU_CD							,
		TASP.LOT_TX							,
		TASP.SRCKANBAN_CD					,	-- 元かんばん番号
		SUM(TASP.PALLET_NR) + TRUNC(SUM(TASP.CASE_NR) / TASP.PALLETCAPACITY_NR) AS PALLET_NR,
		MOD(SUM(TASP.CASE_NR),TASP.PALLETCAPACITY_NR) AS CASE_NR	,
		SUM(TASP.BARA_NR) AS BARA_NR		,
		SUM(TASP.PCS_NR) AS PCS_NR			,
		TASP.AMOUNT_NR						,
		TASP.PALLETCAPACITY_NR				,
		-- 元ロケ
		TASP.SRCTYPESTOCK_CD				,
		TASP.SRCLOCA_CD						,
		SUM(TASP.SRCPALLET_NR) + TRUNC(SUM(TASP.SRCCASE_NR)/TASP.PALLETCAPACITY_NR) AS SRCPALLET_NR,
		MOD(SUM(TASP.SRCCASE_NR),TASP.PALLETCAPACITY_NR) AS SRCCASE_NR	,
		SUM(TASP.SRCBARA_NR) AS SRCBARA_NR		,
		SUM(TASP.SRCPCS_NR) AS SRCPCS_NR		,
		MIN(TASP.SRCSTART_DT) AS SRCSTART_DT	,
		MAX(TASP.SRCEND_DT) AS SRCEND_DT		,
		MIN(TASP.SRCHT_ID) AS SRCHT_ID			,
		TASP.SRCHTUSER_CD						,
		TASP.SRCHTUSER_TX						,
		MIN(TASP.SRCLOCASTOCKC_ID) AS SRCLOCASTOCKC_ID,
		-- 先ロケ
		TASP.DESTTYPESTOCK_CD	,
		TASP.DESTLOCA_CD		,
		SUM(TASP.DESTPALLET_NR) + TRUNC(SUM(TASP.DESTCASE_NR)/TASP.PALLETCAPACITY_NR) AS DESTPALLET_NR,
		MOD(SUM(TASP.DESTCASE_NR),TASP.PALLETCAPACITY_NR) AS DESTCASE_NR	,
		SUM(TASP.DESTBARA_NR) AS DESTBARA_NR		,
		SUM(TASP.DESTPCS_NR) AS DESTPCS_NR			,
		MIN(TASP.DESTSTART_DT) AS DESTSTART_DT		,
		MAX(TASP.DESTEND_DT) AS DESTEND_DT			,
		MIN(TASP.DESTHT_ID) AS DESTHT_ID			,
		TASP.DESTHTUSER_CD		,
		TASP.DESTHTUSER_TX		,
		MIN(TASP.DESTLOCASTOCKC_ID)	AS DESTLOCASTOCKC_ID,
		--
		MIN(TASP.ADD_DT) AS ADD_DT		,
		TASP.ADDUSER_CD			,
		TASP.ADDUSER_TX			
	FROM
		TA_SUPP TASP
	GROUP BY
		TASP.SUPPNO_CD			,
		TASP.TYPESUPP_CD		,
		TASP.SKU_CD				,
		TASP.LOT_TX				,
		TASP.SRCKANBAN_CD		,	-- 元かんばん番号
		TASP.AMOUNT_NR			,
		TASP.PALLETCAPACITY_NR	,
		-- 元ロケ
		TASP.SRCTYPESTOCK_CD	,
		TASP.SRCLOCA_CD			,
		TASP.SRCHTUSER_CD		,
		TASP.SRCHTUSER_TX		,
		-- 先ロケ
		TASP.DESTTYPESTOCK_CD	,
		TASP.DESTLOCA_CD		,
		TASP.DESTHTUSER_CD		,
		TASP.DESTHTUSER_TX		,
		--
		TASP.ADDUSER_CD			,
		TASP.ADDUSER_TX			
	) TASP					,
	MA_LOCA		MALC1		,
	MA_LOCA		MALC2		,
	MA_SKU		MASK		,
	MB_STSUPP	MBSS		,
	MB_TYPESUPP MBTS
WHERE
	TASP.SKU_CD			= MASK.SKU_CD(+)	AND
	TASP.STSUPP_ID		= MBSS.STSUPP_ID(+)	AND
	TASP.SRCLOCA_CD		= MALC1.LOCA_CD(+)	AND
	TASP.DESTLOCA_CD	= MALC2.LOCA_CD(+)	AND
	TASP.TYPESUPP_CD	= MBTS.TYPESUPP_CD(+) AND
	TASP.TYPESUPP_CD	= '10'
ORDER BY
	TASP.SUPPNO_CD	,
	TASP.SRCLOCA_CD
;
