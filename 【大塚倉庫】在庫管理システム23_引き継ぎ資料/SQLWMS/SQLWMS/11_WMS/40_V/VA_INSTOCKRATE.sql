-- Copyright (C) Seaos Corporation 2014 All rights reserved
--*******************************************
-- 格納割合
--*******************************************
DROP VIEW VA_INSTOCKRATE;
CREATE VIEW VA_INSTOCKRATE
(
	SKU_CD						,	-- 商品コード
	SKU_TX						,	-- 商品名
	NESTAINERINSTOCKRATE_NR		,	-- 格納割合-ネステナ
	CASESTOCKRATE_NR			,	-- 格納割合-ケース成り
	CASESHIPBYCASEPICK_NR		,	-- ケース成りの平均出荷数(ケース/日)
	CASESHIPBYPALLETPICK_NR		,	-- パレット成りの平均出荷数(ケース/日)
	PALLETSHIPBYCASEPICK_NR		,	-- ケース成りの平均出荷数(パレット/日)
	PALLETSHIPBYPALLETPICK_NR	,	-- パレット成りの平均出荷数(パレット/日)
	PALLETCAPACITY_NR				-- パレット積み付け数
) AS
	SELECT
		TPD.SKU_CD											,
		MS.SKU_TX											,
		TRUNC(STDDEV(TPD.ORDERCASE_NR)* 1.96 * SQRT(COUNT(*)) / (STDDEV(TPD.ORDERCASE_NR)* 1.96 * SQRT(COUNT(*)) + SUM(TOTALCASE_NR)),2)	,
		TRUNC(SUM(ORDERCASE_NR)                               / (STDDEV(TPD.ORDERCASE_NR)* 1.96 * SQRT(COUNT(*)) + SUM(TOTALCASE_NR)),2)	,
		TRUNC(AVG(ORDERCASE_NR),2)							,
		TRUNC(AVG(TOTALCASE_NR) - AVG(ORDERCASE_NR),2)		,
		TRUNC(AVG(ORDERCASE_NR)/PALLETCAPACITY_NR,2)							,
		TRUNC((AVG(TOTALCASE_NR) - AVG(ORDERCASE_NR))/PALLETCAPACITY_NR,2)		,
		PALLETCAPACITY_NR
	FROM
	(
		SELECT
			SKU_CD									,		-- 商品コード
			TO_CHAR(ADD_DT, 'YYYY-MM-DD') AS ADD_DT	,		-- 作業日
			SUM(											-- ケース成り出荷ケース数
				CASE 
					WHEN TYPEPICK_CD = 30 THEN PCS_NR/AMOUNT_NR
					ELSE 0
				END
			) AS ORDERCASE_NR,
			SUM(											-- 総出荷ケース数
				PCS_NR/AMOUNT_NR
			) AS TOTALCASE_NR,
			PALLETCAPACITY_NR								-- パレット積み付け数
		FROM
			TA_PICK
		WHERE 
			ADD_DT >= 
			(
				SELECT
					MIN(ADD_DT)
				FROM
				(
					SELECT DISTINCT
						TO_CHAR(ADD_DT, 'YYYY-MM-DD') AS ADD_DT
					FROM
						TA_PICK
					ORDER BY
						ADD_DT DESC
				)
				WHERE
					ROWNUM <= 12
			)
		GROUP BY
			SKU_CD								,
			TO_CHAR(ADD_DT, 'YYYY-MM-DD')		,
			PALLETCAPACITY_NR
		ORDER BY
			SKU_CD,
			TO_CHAR(ADD_DT, 'YYYY-MM-DD')
	) TPD
	LEFT OUTER JOIN MA_SKU MS
		on TPD.SKU_CD = MS.SKU_CD
	GROUP BY
		TPD.SKU_CD				,
		MS.SKU_TX				,
		TPD.PALLETCAPACITY_NR	
	ORDER BY
		TPD.SKU_CD
;