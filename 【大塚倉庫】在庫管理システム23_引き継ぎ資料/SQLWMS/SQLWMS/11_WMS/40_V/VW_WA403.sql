-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- 入荷予定照会（商品別)
--*******************************************
DROP VIEW VW_WA403;
CREATE VIEW VW_WA403
(
	SLIPNO_TX			,	/* 伝票番号 */
	ARRIVPLAN_DT		,	/* 入荷予定日 */
	EXTERIOR_CD			,	/* 外装コード */
	SKU_TX				,	/* 商品名 */
	LOT_TX				,	/* 製造ロット */
	PALLET_NR			,	/* 入荷予定パレット数 */
	CASE_NR				,	/* 入荷予定ケース数 */
	BARA_NR				,	/* 入荷予定バラ数 */
	UNCHECK_PALLET_NR	,	/* 未検品パレット数 */
	UNCHECK_CASE_NR		,	/* 未検品ケース数 */
	UNCHECK_BARA_NR		,	/* 未検品バラ数 */
	CHECK_PALLET_NR		,	/* 検品済みパレット数 */
	CHECK_CASE_NR		,	/* 検品済みケース数 */
	CHECK_BARA_NR		,	/* 検品済みバラ数 */
	STARRIV_ID			,	/* 入荷予定状態ID */
	STARRIV_TX				/* 入荷予定状態 */
) AS
SELECT
	TAR.SLIPNO_TX			,	/* 伝票番号 */
	TAR.ARRIVPLAN_DT		,	/* 入荷予定日 */
	TAR.EXTERIOR_CD			,	/* 外装コード */
	TAR.SKU_TX				,	/* 商品名 */
	TAR.LOT_TX				,	/* 製造ロット */
	TAR.PALLET_NR			,	/* 入荷予定パレット数 */
	TAR.CASE_NR				,	/* 入荷予定ケース数 */
	TAR.BARA_NR				,	/* 入荷予定バラ数 */
	TAR.PALLET_NR - TAR.CHECK_PALLET_NR	AS UNCHECK_PALLET_NR,	/* 未検品パレット数 */
	TAR.CASE_NR   - TAR.CHECK_CASE_NR	AS UNCHECK_CASE_NR	,	/* 未検品ケース数 */
	TAR.BARA_NR   - TAR.CHECK_BARA_NR	AS UNCHECK_BARA_NR	,	/* 未検品バラ数 */
	TAR.CHECK_PALLET_NR		,	/* 検品済みパレット数 */
	TAR.CHECK_CASE_NR		,	/* 検品済みケース数 */
	TAR.CHECK_BARA_NR		,	/* 検品済みバラ数 */
	TAR.STARRIV_ID			,	/* 入荷予定状態ID */
	MSA.STARRIV_TX				/* 入荷予定状態 */
FROM
	(
	SELECT
		TAR.SLIPNO_TX		,	/* 伝票番号 */
		TAR.ARRIVPLAN_DT	,	/* 入荷予定日 */
		MSK.EXTERIOR_CD		,	/* 外装コード */
		MSK.SKU_TX			,	/* 商品名 */
		TAR.LOT_TX			,	/* 製造ロット */
		COALESCE(SUM(TAR.ARRIVPLANPALLET_NR), 0)	AS PALLET_NR		,	/* 入荷予定パレット数 */
		COALESCE(SUM(TAR.ARRIVPLANCASE_NR), 0)		AS CASE_NR			,	/* 入荷予定ケース数 */
		COALESCE(SUM(TAR.ARRIVPLANBARA_NR), 0)		AS BARA_NR			,	/* 入荷予定バラ数 */
		COALESCE(SUM(TIN.CHECK_PALLET_NR), 0)		AS CHECK_PALLET_NR	,	/* 検品済みパレット数 */
		COALESCE(SUM(TIN.CHECK_CASE_NR), 0)			AS CHECK_CASE_NR	,	/* 検品済みケース数 */
		COALESCE(SUM(TIN.CHECK_BARA_NR), 0)			AS CHECK_BARA_NR	,	/* 検品済みバラ数 */
		MIN(TAR.STARRIV_ID)							AS STARRIV_ID			/* 入荷予定状態ID */
	FROM
		TA_ARRIV TAR
			LEFT OUTER JOIN (
				SELECT	TIN.ARRIV_ID	,	/* 入荷予定ID */
						SUM(TIN.APPLYINSTOCKNPALLET_NR)	AS CHECK_PALLET_NR	,	/* 検品済みパレット数 */
						SUM(TIN.APPLYINSTOCKNCASE_NR)	AS CHECK_CASE_NR	,	/* 検品済みケース数 */
						SUM(TIN.APPLYINSTOCKNBARA_NR)	AS CHECK_BARA_NR		/* 検品済みバラ数 */
				FROM	TA_INSTOCK TIN
				WHERE	COALESCE(TIN.STINSTOCK_ID, 0) >= 0
				GROUP BY ARRIV_ID
			) TIN
				ON	TAR.ARRIV_ID = TIN.ARRIV_ID
			LEFT OUTER JOIN MA_SKU MSK
				ON TAR.SKU_CD = MSK.SKU_CD
	GROUP BY
		TAR.SLIPNO_TX		,	/* 伝票番号 */
		TAR.ARRIVPLAN_DT	,	/* 入荷予定日 */
		MSK.EXTERIOR_CD		,	/* 外装コード */
		MSK.SKU_TX			,	/* 商品名 */
		TAR.LOT_TX				/* 製造ロット */
	) TAR
	LEFT OUTER JOIN MB_STARRIV MSA
		ON TAR.STARRIV_ID = MSA.STARRIV_ID
;
