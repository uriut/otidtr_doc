-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- ŻÉf[^iąVc³ńĖj
--*******************************************
DROP VIEW VZ_STOCK;
CREATE VIEW VZ_STOCK
(
	SKU_CD				,
	WAREHOUSESKU_CD		,
	EXTERIOR_CD			,
	SKU_TX				,
	MANUFACTURERSKU_TX	,
	PALLETCAPACITY_NR	,
	AMOUNT_NR			,
	PALLET_NR			,
	CASE_NR				,
	PCS_NR
) AS
SELECT
	LP.SKU_CD				,
	MS.WAREHOUSESKU_CD		,
	MS.EXTERIOR_CD			,
	MS.SKU2_TX				,
	MS.MANUFACTURERSKU_TX	,
	LP.PALLETCAPACITY_NR	,
	LP.AMOUNT_NR			,
	TRUNC((SUM(LP.PCS_NR) / LP.AMOUNT_NR) / LP.PALLETCAPACITY_NR) AS PALLET_NR,
	MOD((SUM(LP.PCS_NR) / LP.AMOUNT_NR),LP.PALLETCAPACITY_NR) AS CASE_NR,
	SUM(LP.PCS_NR) AS PCS_NR
FROM
	(
	SELECT
		SKU_CD				,
		PALLETCAPACITY_NR	,
		AMOUNT_NR			,
		SUM(PCS_NR) AS PCS_NR
	FROM
		TA_LOCASTOCKP
	GROUP BY
		SKU_CD				,
		PALLETCAPACITY_NR	,
		AMOUNT_NR
	UNION ALL
	SELECT
		SKU_CD				,
		PALLETCAPACITY_NR	,
		AMOUNT_NR			,
		SUM(PCS_NR) AS PCS_NR
	FROM
		TA_LOCASTOCKC
	WHERE
		STSTOCK_ID = 0
	GROUP BY
		SKU_CD				,
		PALLETCAPACITY_NR	,
		AMOUNT_NR
	) LP
		LEFT OUTER JOIN MA_SKU MS
			ON LP.SKU_CD = MS.SKU_CD
GROUP BY
	LP.SKU_CD				,
	MS.WAREHOUSESKU_CD		,
	MS.EXTERIOR_CD			,
	MS.SKU2_TX				,
	MS.MANUFACTURERSKU_TX	,
	LP.PALLETCAPACITY_NR	,
	LP.AMOUNT_NR
ORDER BY
	LP.SKU_CD
;
