-- Copyright (C) Seaos Corporation 2014 All rights reserved
-- *******************************************
-- 作業実績分析−ピック（パレット成り詳細）P3
-- *******************************************

DROP VIEW VW_PAL_AVG_PICK_TIME;
CREATE VIEW VW_PAL_AVG_PICK_TIME
(
	ADD_DT1				,	--作業日
	TYPE_OF_PICK		,	--ピックタイプ
	NUM_OF_DETAIL		,	--指示書枚数
	NUM_CASE_OF_PAL		,	--ピックされたケース数
	AVG_PAL_TIME
) AS
	SELECT
		P3.ADD_DT1				,	--作業日
		P3.TYPE_OF_PICK			,	--ピックタイプ
		P3.NUM_OF_DETAIL		,	--指示書枚数
		P3.NUM_CASE_OF_PAL		,	--ピックされたケース数
		P3.AVG_PAL_TIME
	FROM
	(
		SELECT
			AVG_PAL_PRT_TIME.ADD_DT1		,	
			AVG_PAL_PRT_TIME.TYPE_OF_PICK	,	
			COUNT_PAL_CASE.NUM_OF_DETAIL	,	
			COUNT_PAL_CASE.NUM_CASE_OF_PAL	,	
	--		TRUNC( 
	--			(AVG_PAL_PRT_TIME.PRT_TIME + AVG_PAL_INSPECTION_TIME.INS_TIME)*24
	--		) ||'時'||
			TRUNC(
				(
					(AVG_PAL_PRT_TIME.PRT_TIME + AVG_PAL_INSPECTION_TIME.INS_TIME)*24	
					-
					TRUNC(
						(AVG_PAL_PRT_TIME.PRT_TIME + AVG_PAL_INSPECTION_TIME.INS_TIME)*24		
					)
				)*60
			)	|| '分' ||
			TRUNC(
				(
					(
						(AVG_PAL_PRT_TIME.PRT_TIME + AVG_PAL_INSPECTION_TIME.INS_TIME)*24
						-
						TRUNC( 
								(AVG_PAL_PRT_TIME.PRT_TIME + AVG_PAL_INSPECTION_TIME.INS_TIME)*24
						) 
					)*60
					-
					TRUNC(
						(
							(AVG_PAL_PRT_TIME.PRT_TIME + AVG_PAL_INSPECTION_TIME.INS_TIME)*24		
							-
							TRUNC( 
								(AVG_PAL_PRT_TIME.PRT_TIME + AVG_PAL_INSPECTION_TIME.INS_TIME)*24
							) 
						)*60
					) 
				)*60
			)  || '秒' AS AVG_PAL_TIME		--パレット平均ピック時間
		FROM
		(
			SELECT
				ADD_DT1,
				TYPE_OF_PICK,
				PRT_TIME
			FROM
			(
				SELECT
				ADD_DT1,
				CASE
					WHEN  (TYPEPICK_CD=40 OR TYPEPICK_CD=50) THEN 'パレット'
				END TYPE_OF_PICK,
				AVG(CASE
						WHEN (PRT_DT2 IS NULL) THEN 0
						ELSE(PRT_DT2 - PRT_DT1)
					END			
				) AS PRT_TIME											　
				FROM
				(		
					SELECT
					PRT1.ADD_DT1,
					PRT1.SRCHTUSER_CD1,
					PRT1.PRT_DT1,
					PRT1.TYPEPICK_CD,
					PRT2.ADD_DT2,
					PRT2.SRCHTUSER_CD2,
					PRT2.PRT_DT2
					FROM
					(
						SELECT
							ADD_DT AS ADD_DT1,
							SRCHTUSER_CD AS SRCHTUSER_CD1,
							PRT_DT AS PRT_DT1,
							TYPEPICK_CD,
							PALLETDETNO_CD,
							SRCLOCA_CD,
							(EMP_ID + 1 ) AS EMP_ID_NEW
						FROM
						(
							SELECT
							ADD_DT,
							SRCHTUSER_CD,
							PRT_DT,
							TYPEPICK_CD,				
							PALLETDETNO_CD,
							SRCLOCA_CD,
							ROW_NUMBER()
							OVER (ORDER BY add_dt) AS emp_id
							FROM
							(
								SELECT													
									SUB.ADD_DT,
									SUB.SRCHTUSER_CD,
									SUB.PRT_DT,
									SUB.TYPEPICK_CD,				
									SUB.PALLETDETNO_CD,
									SUB.SRCLOCA_CD
								FROM
								(
									SELECT
										TO_CHAR(TP.ADD_DT,'YYYY-MM-DD') AS ADD_DT,
										TP.SRCHTUSER_CD			,
										TP.PRT_DT				,
										TP.PALLETDETNO_CD		,
										TP.TYPEPICK_CD			,
										TP.SRCLOCA_CD			
									FROM
										TA_PICK TP
									GROUP BY
										TO_CHAR(TP.ADD_DT,'YYYY-MM-DD'),
										TP.SRCHTUSER_CD			,
										TP.PRT_DT				,
										TP.PALLETDETNO_CD		,
										TP.TYPEPICK_CD			,
										TP.SRCLOCA_CD			
								) SUB
								WHERE
									SUB.TYPEPICK_CD IN ('40','50')		--	AND -- todo
								GROUP BY
									SUB.ADD_DT,
									SUB.SRCHTUSER_CD,
									SUB.PRT_DT,
									SUB.TYPEPICK_CD,
									SUB.PALLETDETNO_CD,
									SUB.SRCLOCA_CD
							)
							GROUP BY
								ADD_DT,
								SRCHTUSER_CD,
								PRT_DT,					
								TYPEPICK_CD,				
								PALLETDETNO_CD,
								SRCLOCA_CD
						)
						ORDER BY
						(EMP_ID + 1 ) ASC					
					) PRT1
						LEFT OUTER JOIN						
						(
						SELECT
							ADD_DT AS ADD_DT2,
							SRCHTUSER_CD AS SRCHTUSER_CD2,
							PRT_DT AS PRT_DT2,
							TYPEPICK_CD,				
							PALLETDETNO_CD,
							SRCLOCA_CD,
							ROW_NUMBER()
							OVER (ORDER BY add_dt) AS emp_id
						FROM
						(
							SELECT	
								SUB.ADD_DT,
								SUB.SRCHTUSER_CD,
								SUB.PRT_DT,
								SUB.TYPEPICK_CD,				
								SUB.PALLETDETNO_CD,
								SUB.SRCLOCA_CD
							FROM
							(
								SELECT
									TO_CHAR(TP.ADD_DT,'YYYY-MM-DD') AS ADD_DT,
									TP.SRCHTUSER_CD			,
									TP.PRT_DT,
									TP.PALLETDETNO_CD			,
									TP.TYPEPICK_CD				,
									TP.SRCLOCA_CD				
								FROM
									TA_PICK TP
								GROUP BY
								TO_CHAR(TP.ADD_DT,'YYYY-MM-DD'),
								TP.SRCHTUSER_CD			,
								TP.CHECKSTART_DT,
								TP.CHECKEND_DT,
								TP.PRT_DT,
								TP.PICKNO_CD				,
								TP.PALLETDETNO_CD			,
								TP.TYPEPICK_CD				,
								TP.SRCLOCA_CD				
							) SUB
							WHERE
								SUB.TYPEPICK_CD IN ('40','50')		--	AND -- todo
							GROUP BY
								SUB.ADD_DT,
								SUB.SRCHTUSER_CD,
								SUB.PRT_DT,
								SUB.TYPEPICK_CD,				
								SUB.PALLETDETNO_CD,
								SUB.SRCLOCA_CD
						)
						ORDER BY
						(EMP_ID + 1 ) ASC	
						) PRT2					
							ON						
								PRT1.EMP_ID_NEW=PRT2.EMP_ID				AND
								PRT1.ADD_DT1=PRT2.ADD_DT2 				AND
								PRT1.SRCHTUSER_CD1=PRT2.SRCHTUSER_CD2	
				)
				WHERE
					ADD_DT1 = ADD_DT2					AND
					SRCHTUSER_CD1 = SRCHTUSER_CD2		AND	
					((PRT_DT2 - PRT_DT1) < 0.002083)    -- 3分以内ものを対象する
				GROUP BY
					ADD_DT1,  
					CASE
					  WHEN  (TYPEPICK_CD=40 OR TYPEPICK_CD=50) THEN 'パレット'
					END
				ORDER BY
					ADD_DT1 DESC
			)
		) AVG_PAL_PRT_TIME
			LEFT OUTER JOIN
			(		
				SELECT
					ADD_DT,
					CASE
						WHEN  (TYPEPICK_CD=40 OR TYPEPICK_CD=50) THEN 'パレット'
					END TYPE_OF_PICK,
					COUNT(PALLETDETNO_CD) AS NUM_OF_DETAIL,
					SUM(PALLETCAPACITY_NR) AS NUM_CASE_OF_PAL
				FROM
				(
					SELECT
						SUB.ADD_DT,
						SUB.PRT_DT,
						SUB.TYPEPICK_CD,
						SUB.PALLETDETNO_CD,
						SUB.PALLETCAPACITY_NR,
						SUB.SRCLOCA_CD
					FROM
					(
						SELECT
							TO_CHAR(TP.ADD_DT,'YYYY-MM-DD') AS ADD_DT,
							TP.PRT_DT,
							TP.PALLETDETNO_CD			,
							TP.TYPEPICK_CD				,
							TP.SRCLOCA_CD				,
							TP.PALLETCAPACITY_NR		
						FROM
						TA_PICK TP
						GROUP BY
						TO_CHAR(TP.ADD_DT,'YYYY-MM-DD'),
						TP.SRCHTUSER_CD			,
						TP.CHECKSTART_DT,
						TP.CHECKEND_DT,
						TP.PRT_DT,					
						TP.PICKNO_CD				,
						TP.PALLETDETNO_CD			,
						TP.TYPEPICK_CD				,
						TP.SRCLOCA_CD				,
						TP.PALLETCAPACITY_NR		
					) SUB
					WHERE
					SUB.TYPEPICK_CD IN ('40','50')			--AND -- todo
				--	SUB.TOTALPIC_FL = 1		 						-- トータルピック
					GROUP BY
					SUB.ADD_DT,
					SUB.PRT_DT,
					SUB.TYPEPICK_CD,				
					SUB.PALLETDETNO_CD,	
					SUB.PALLETCAPACITY_NR,
					SUB.SRCLOCA_CD
				)
				GROUP BY
				ADD_DT,
				CASE
					WHEN  (TYPEPICK_CD=40 OR TYPEPICK_CD=50) THEN 'パレット'
				END
		) COUNT_PAL_CASE
			ON AVG_PAL_PRT_TIME.ADD_DT1 = COUNT_PAL_CASE.ADD_DT 			AND
			   AVG_PAL_PRT_TIME.TYPE_OF_PICK = COUNT_PAL_CASE.TYPE_OF_PICK
				LEFT OUTER JOIN
				(		
					SELECT
					ADD_DT,
					AVG(CHECKEND_DT-CHECKSTART_DT) AS INS_TIME
					FROM
					(
						SELECT						 
							SUB.ADD_DT,
							SUB.SRCHTUSER_CD,
							SUB.CHECKSTART_DT,
							SUB.CHECKEND_DT,
							SUB.TYPEPICK_CD,				
							SUB.PALLETDETNO_CD,
							SUB.SRCLOCA_CD
						FROM
						(
							SELECT
								TO_CHAR(TP.ADD_DT,'YYYY-MM-DD') AS ADD_DT,
								TP.SRCHTUSER_CD			,
								TP.CHECKSTART_DT,
								TP.CHECKEND_DT,
								TP.PALLETDETNO_CD			,
								TP.TYPEPICK_CD				,
								TP.SRCLOCA_CD				,
								TP.PALLETCAPACITY_NR		
							FROM
							TA_PICK TP
							GROUP BY
							TO_CHAR(TP.ADD_DT,'YYYY-MM-DD'),
							TP.SRCHTUSER_CD			,
							TP.CHECKSTART_DT,
							TP.CHECKEND_DT,
							TP.PALLETDETNO_CD			,
							TP.TYPEPICK_CD				,
							TP.SRCLOCA_CD				,
							TP.PALLETCAPACITY_NR		
						) SUB
						WHERE
							SUB.TYPEPICK_CD IN ('40','50')		--	AND -- todo
						GROUP BY
							SUB.ADD_DT,
							SUB.SRCHTUSER_CD,
							SUB.CHECKSTART_DT,
							SUB.CHECKEND_DT,
							SUB.TYPEPICK_CD,				
							SUB.PALLETDETNO_CD,
							SUB.SRCLOCA_CD								
					)
					WHERE		
					TO_CHAR(CHECKEND_DT,'YYYY-MM-DD')	=	TO_CHAR(CHECKSTART_DT,'YYYY-MM-DD')			AND						
					(CHECKEND_DT - CHECKSTART_DT) <	0.000347222  										AND	 --パレット検品時間30秒以内なら数える
					PALLETDETNO_CD = PALLETDETNO_CD
					GROUP BY
					ADD_DT
					ORDER BY 
					ADD_DT desc		
				) AVG_PAL_INSPECTION_TIME
					ON	AVG_PAL_INSPECTION_TIME.ADD_DT = COUNT_PAL_CASE.ADD_DT
				ORDER BY
				AVG_PAL_PRT_TIME.ADD_DT1 DESC
	) P3