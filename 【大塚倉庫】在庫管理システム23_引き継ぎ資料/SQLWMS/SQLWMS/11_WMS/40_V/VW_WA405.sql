-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- 格納指示照会
--*******************************************
DROP VIEW VW_WA405;
CREATE VIEW VW_WA405
(
	INSTOCK_ID					,
	STINSTOCK_ID				,	/* 格納状態コード */
	STINSTOCK_TX				,	/* 格納状態 */
	INSTOCKNO_CD				,	/* 入庫指示NO */
	SKU_CD						,	/* 統一商品コード */
	EXTERIOR_CD					,	/* 外装コード */
	SKU_TX						,	/* 商品名 */
	SIZE_TX						,	/* 規格 */
	LOT_TX						,	/* 製造ロット */
	DESTLOCA_CD					,	/* ロケーション */
	DESTLOCA_TX					,	/* ロケーション名 */
	TYPELOCABLOCK_TX			,	/* ロケーションブロック */
	KANBAN_CD					,	/* かんばん番号 */
	COLOR_TX					,	/* かんばん番号の色 */
	HTMLCOLOR_TX				,	/* かんばん番号の色 */
	BARA_NR						,	/* バラ数 */
	CASE_NR						,	/* ケース数 */
	PALLET_NR					,	/* パレット数 */
	CHECK_DT					,	/* 検品完了日時 */
	CHECKUSER_CD				,	/* 検品作業者コード */
	CHECKUSER_TX				,	/* 検品作業者名 */
	DESTSTART_DT				,	/* 格納開始日時 */
	DESTEND_DT					,	/* 格納終了日時 */
	DESTHTUSER_CD				,	/* 格納作業者コード */
	DESTHTUSER_TX					/* 格納作業者名 */
) AS
SELECT
	TAI.INSTOCK_ID					,
	TAI.STINSTOCK_ID				,	/* 格納状態コード */
	MST.STINSTOCK_TX				,	/* 格納状態 */
	TAI.INSTOCKNO_CD				,	/* 入庫指示NO */
	TAI.SKU_CD						,	/* 統一商品コード */
	MSK.EXTERIOR_CD					,	/* 外装コード */
	MSK.SKU_TX						,	/* 商品名 */
	MSK.SIZE_TX						,	/* 規格 */
	TAI.LOT_TX						,	/* 製造ロット */
	TAI.DESTLOCA_CD					,	/* ロケーション */
	MAL.LOCA_TX						,	/* ロケーション名 */
	MTB.TYPELOCABLOCK_TX			,	/* ロケーションブロック */
	TAI.KANBAN_CD					,	/* かんばん番号 */
	MAK.COLOR_TX					,	/* かんばん番号の色 */
	MBA.HTMLCOLOR_TX				,	/* かんばん番号の色 */
	TAI.BARA_NR						,	/* バラ数 */
	TAI.CASE_NR						,	/* ケース数 */
	TAI.PALLET_NR					,	/* パレット数 */
	TAA.CHECK_DT	AS CHECK_DT		,	/* 検品完了日時 */
	TAA.CHECKUSER_CD				,	/* 検品作業者コード */
	TAA.CHECKUSER_TX				,	/* 検品作業者名 */
	TAI.DESTSTART_DT AS DESTSTART_DT,	/* 格納開始日時 */
	TAI.DESTEND_DT AS DESTEND_DT	,	/* 格納終了日時 */
	TAI.DESTHTUSER_CD				,	/* 格納作業者コード */
	TAI.DESTHTUSER_TX					/* 格納作業者名 */
FROM
	TA_INSTOCK TAI
		LEFT OUTER JOIN MA_SKU MSK
			ON TAI.SKU_CD = MSK.SKU_CD
		LEFT OUTER JOIN MB_STINSTOCK MST
			ON TAI.STINSTOCK_ID = MST.STINSTOCK_ID
		LEFT OUTER JOIN MA_LOCA MAL
			ON TAI.DESTLOCA_CD = MAL.LOCA_CD
		LEFT OUTER JOIN MB_TYPELOCABLOCK MTB
			ON MTB.TYPELOCABLOCK_CD = MAL.TYPELOCABLOCK_CD
		LEFT OUTER JOIN MA_KANBAN MAK
			ON TAI.KANBAN_CD = MAK.KANBAN_CD
		LEFT OUTER JOIN MB_AREA MBA
			ON MAK.AREA_TX = MBA.AREA_CD
		LEFT OUTER JOIN TA_ARRIV TAA
			ON TAI.ARRIV_ID = TAA.ARRIV_ID
GROUP BY
	TAI.INSTOCK_ID					,
	TAI.STINSTOCK_ID				,	/* 格納状態コード */
	MST.STINSTOCK_TX				,	/* 格納状態 */
	TAI.INSTOCKNO_CD				,	/* 入庫指示NO */
	TAI.SKU_CD						,	/* 統一商品コード */
	MSK.EXTERIOR_CD					,	/* 外装コード */
	MSK.SKU_TX						,	/* 商品名 */
	MSK.SIZE_TX						,	/* 規格 */
	TAI.LOT_TX						,	/* 製造ロット */
	TAI.DESTLOCA_CD					,	/* ロケーション */
	MAL.LOCA_TX						,	/* ロケーション名 */
	MTB.TYPELOCABLOCK_TX			,	/* ロケーションブロック */
	TAI.KANBAN_CD					,	/* かんばん番号 */
	TAI.KANBAN_CD					,	/* かんばん番号 */
	MAK.COLOR_TX					,	/* かんばん番号の色 */
	MBA.HTMLCOLOR_TX				,	/* かんばん番号の色 */
	TAI.PALLET_NR					,	/* パレット数 */
	TAI.BARA_NR						,	/* バラ数 */
	TAI.CASE_NR						,	/* ケース数 */
	TAA.CHECKUSER_CD				,	/* 検品作業者コード */
	TAA.CHECKUSER_TX				,	/* 検品作業者名 */
	TAA.CHECK_DT					,	/* 検品完了日時 */
	TAI.DESTHTUSER_CD				,	/* 格納作業者コード */
	TAI.DESTSTART_DT 				,	/* 格納開始日時 */
	TAI.DESTEND_DT 					,	/* 格納終了日時 */
	TAI.DESTHTUSER_TX					/* 格納作業者名 */
;
