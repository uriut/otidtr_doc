-- Copyright (C) Seaos Corporation 2014 All rights reserved
-- *******************************************
-- 作業実績分析―ピック（ケース成り分布）P4
-- *******************************************

DROP VIEW VW_CASE_DISTRIBUTION;
CREATE VIEW VW_CASE_DISTRIBUTION
(
	ADD_DT,				--作業日
	TYPE_OF_PICK,		--ピックタイプ
	LOCA_OF_BLOCK,		--計算対象外の指示書枚数
	NUM_OF_DETAIL,		--有効ピック明細数（ピック時間が3分以内の物を対象する）
	PT90,				--ピック時間 <= 90秒
	PT90_150,			--90秒  < ピック時間 <= 150秒
	PT150_210,			--150秒  < ピック時間 <= 210秒
	PT210				--210秒  < ピック時間 
) AS 
	SELECT
		P4.ADD_DT,				--作業日
		P4.TYPE_OF_PICK,		--ピックタイプ
		P4.LOCA_OF_BLOCK,		--計算対象外の指示書枚数
		P4.NUM_OF_DETAIL,		--有効ピック明細数（ピック時間が3分以内の物を対象する）
		P4.PT90,				--ピック時間 <= 90秒
		P4.PT90_150,			--90秒  < ピック時間 <= 150秒
		P4.PT150_210,			--150秒  < ピック時間 <= 210秒
		P4.PT210				--210秒  < ピック時間 
	FROM
	(
		SELECT
			ADD_DT,		
			CASE
				WHEN  (TYPEPICK_CD=30) THEN 'ケース'
			END TYPE_OF_PICK,		
			CASE
				WHEN
				TYPEPICK_CD=30 THEN
				CASE
					WHEN
					(substr(SRCLOCA_CD,13,2)= '00'				OR
					substr(SRCLOCA_CD,13,2)= '11') THEN '平積み'
					ELSE  'ネステナ2段以上'
				END
			END LOCA_OF_BLOCK,	
			COUNT(PALLETDETNO_CD) AS NUM_OF_DETAIL,
			COUNT(
				CASE 
					WHEN ( TEST_C1 <= 0.00104166666666667 )  THEN 1												--0:01:30<=TEST_C1
				END
			) AS PT90,
			COUNT(
				CASE 
					WHEN (　0.00104166666666667 < TEST_C1) AND (TEST_C1 <= 0.00173611111111111) THEN 1			--0:01:30<TEST_C1<=0:02:30    平均ピック時間は2分位のため、前後30秒をずらし、間隔を取った
				END
			) AS PT90_150,
			COUNT(
				CASE 
					WHEN (　0.00173611111111111 < TEST_C1) AND (TEST_C1 <= 0.00243055555555556) THEN 1			--0:02:30<TEST_C1<=0:03:30
				END
			) AS PT150_210,		
			COUNT(
				CASE 
					WHEN (　0.00243055555555556 < TEST_C1) THEN 1             									--0:03:30<TEST_C1
				END
			) AS PT210
		FROM
		(
			SELECT
				ADD_DT,
				SRCHTUSER_CD,
				CHECKSTART_DT,
				CHECKEND_DT,
				TYPEPICK_CD,
				PALLETDETNO_CD,
				SRCLOCA_CD,
				(CHECKEND_DT - CHECKSTART_DT) AS TEST_C1
			FROM
			(
				SELECT						
					SUB.ADD_DT,
					SUB.SRCHTUSER_CD,
					SUB.CHECKSTART_DT,
					SUB.CHECKEND_DT,
					SUB.TYPEPICK_CD,				
					SUB.PALLETDETNO_CD,
					SUB.SRCLOCA_CD
				FROM
				(
					SELECT
					TO_CHAR(TP.ADD_DT,'YYYY-MM-DD') AS ADD_DT,
					TP.SRCHTUSER_CD			,
					TP.CHECKSTART_DT,
					TP.CHECKEND_DT,
					TP.PALLETDETNO_CD			,
					TP.TYPEPICK_CD				,
					--
					TP.SRCLOCA_CD				
					FROM
					TA_PICK TP
						LEFT OUTER JOIN (
							SELECT
							TRANSPORTERGROUP_CD,
							TRANSPORTERGROUP_TX
							FROM
							MA_TRANSPORTER
							GROUP BY
							TRANSPORTERGROUP_CD,
							TRANSPORTERGROUP_TX
						) MT
						ON TP.TRANSPORTER_CD = MT.TRANSPORTERGROUP_CD
						LEFT OUTER JOIN MA_SKU MS
						ON TP.SKU_CD = MS.SKU_CD
						LEFT OUTER JOIN MA_LOCA ML
						ON TP.SRCLOCA_CD = ML.LOCA_CD
						LEFT OUTER JOIN (
							SELECT
							PALLETDETNO_CD,
							MIN(STPICK_ID) AS STPICK_ID
							FROM
							TA_PICK
							GROUP BY
							PALLETDETNO_CD
						) TP2
						ON TP.PALLETDETNO_CD = TP2.PALLETDETNO_CD
					GROUP BY
						TO_CHAR(TP.ADD_DT,'YYYY-MM-DD'),
						TP.SRCHTUSER_CD			,
						TP.CHECKSTART_DT,
						TP.CHECKEND_DT,
						TP.PALLETDETNO_CD			,
						TP.TYPEPICK_CD				,
						TP.SRCLOCA_CD				
				) SUB
				WHERE
					SUB.TYPEPICK_CD IN ('30')			
				GROUP BY
					SUB.ADD_DT,
					SUB.SRCHTUSER_CD,
					SUB.CHECKSTART_DT,
					SUB.CHECKEND_DT,
					SUB.TYPEPICK_CD,				
					SUB.PALLETDETNO_CD,
					SUB.SRCLOCA_CD
					ORDER BY
				SUB.ADD_DT,
				SUB.SRCHTUSER_CD											   
			)
		)
		GROUP BY
		ADD_DT,
		CASE
			WHEN  (TYPEPICK_CD=30) THEN 'ケース'
		END,
		CASE
			WHEN
			TYPEPICK_CD=30 THEN
			CASE
				WHEN
				(substr(SRCLOCA_CD,13,2)= '00' 			OR
				substr(SRCLOCA_CD,13,2)= '11') THEN '平積み'
				ELSE  'ネステナ2段以上'
			END
		END
		ORDER BY
		ADD_DT DESC					  
	) P4