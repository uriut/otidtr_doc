-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- ピッキングデータ（星野さん依頼）
--*******************************************
DROP VIEW VZ_PICK_ORDER;
CREATE VIEW VZ_PICK_ORDER
(
	DATE_TX						,	-- 出荷日
	ORDERCOUNT_NR				,	-- 指示書枚数
	PORDERCOUNT_NR				,	-- パレット成り指示書枚数
	CORDERCOUNT_NR				,	-- ケース成り指示書枚数
	ORDERCOUNT2_NR				,	-- 複数ロケピック指示書
	LOCACOUNT_NR				,	-- ロケーション数
	SKUCOUNT_NR					,	-- SKU数
	PALLET_NR					,	-- パレット数
	CASE_NR						,	-- ケース数
	TOTALCASE_NR				,	-- 総ケース数
	TOTALPCS_NR					,	-- 総PCS数
	SUPPNCOUNT_NR				,	-- 緊急補充件数
	SUPPCASE_NR						-- 緊急補充ケース数
) AS
SELECT
	SUB1.DATE_TX				,	-- 出荷日
	SUB1.PALLETDETNOCOUNT_NR	,	-- 指示書枚数
	SUB2.PALLETCOUNT_NR			,	-- パレット成り指示書枚数
	SUB3.CASECOUNT_NR			,	-- ケース成り指示書枚数
	NVL(SUB4.PALLETDETNOCOUNT_NR,0),	-- 複数ロケピック指示書
	SUB5.LOCACOUNT_NR			,	-- ロケーション数
	SUB5.SKUCOUNT_NR			,	-- SKU数
	SUB5.PALLET_NR				,	-- パレット数
	SUB5.CASE_NR				,	-- ケース数
	SUB5.TOTALCASE_NR			,	-- 総ケース数
	SUB5.TOTALPCS_NR			,	-- 総PCS数
	NVL(SUB6.SUPPCOUNT_NR,0)	,	-- 緊急補充件数
	NVL(SUB6.SUPPCASE_NR,0)			-- 緊急補充ケース数
FROM
	(
	SELECT
		TO_CHAR(TMP.ADD_DT,'YYYY/MM/DD') AS DATE_TX		,
		COUNT(TMP.PALLETDETNO_CD) AS PALLETDETNOCOUNT_NR
	FROM
		(
		SELECT
			TRUNC(ADD_DT) AS ADD_DT	,
			PALLETDETNO_CD	,
			TYPEPICK_CD
		FROM
			TA_PICK
		WHERE
			TOTALPIC_FL = 0 AND
			STPICK_ID <> -2
		GROUP BY
			TRUNC(ADD_DT)	,
			PALLETDETNO_CD	,
			TYPEPICK_CD
		) TMP
	GROUP BY
		TO_CHAR(TMP.ADD_DT,'YYYY/MM/DD')
	) SUB1		-- 指示書件数
		LEFT OUTER JOIN 
						(
						SELECT
							TO_CHAR(TMP.ADD_DT,'YYYY/MM/DD') AS DATE_TX	,
							MAX(TMP.TYPEPICK_CD)						,
							COUNT(TMP.PALLETDETNO_CD) AS PALLETCOUNT_NR
						FROM
							(
							SELECT
								TRUNC(ADD_DT) AS ADD_DT	,
								PALLETDETNO_CD	,
								TYPEPICK_CD
							FROM
								TA_PICK
							WHERE
								TOTALPIC_FL = 0 AND
								TYPEPICK_CD <> '30' AND
								STPICK_ID <> -2
							GROUP BY
								TRUNC(ADD_DT)	,
								PALLETDETNO_CD	,
								TYPEPICK_CD
							) TMP
						GROUP BY
							TO_CHAR(TMP.ADD_DT,'YYYY/MM/DD')
						) SUB2		-- 指示書件数（パレット成り）
			ON SUB1.DATE_TX = SUB2.DATE_TX
		LEFT OUTER JOIN 
						(
						SELECT
							TO_CHAR(TMP.ADD_DT,'YYYY/MM/DD') AS DATE_TX	,
							MAX(TMP.TYPEPICK_CD)		,
							COUNT(TMP.PALLETDETNO_CD) AS CASECOUNT_NR
						FROM
							(
							SELECT
								TRUNC(ADD_DT) AS ADD_DT	,
								PALLETDETNO_CD	,
								TYPEPICK_CD
							FROM
								TA_PICK
							WHERE
								TOTALPIC_FL = 0 AND
								TYPEPICK_CD = '30' AND
								STPICK_ID <> -2
							GROUP BY
								TRUNC(ADD_DT)	,
								PALLETDETNO_CD	,
								TYPEPICK_CD
							) TMP
						GROUP BY
							TO_CHAR(TMP.ADD_DT,'YYYY/MM/DD')
						) SUB3		-- 指示書件数（ケース成り）
			ON SUB1.DATE_TX = SUB3.DATE_TX
		LEFT OUTER JOIN 
						(
						SELECT
							TO_CHAR(TMP2.ADD_DT,'YYYY/MM/DD') AS DATE_TX,
							COUNT(TMP2.PALLETDETNO_CD) AS PALLETDETNOCOUNT_NR
						FROM
							(
							SELECT
								TMP.ADD_DT			,
								TMP.PALLETDETNO_CD	,
								TMP.SKU_CD			,
								TMP.LOT_TX			
							FROM
								(
								SELECT
									TRUNC(ADD_DT) AS ADD_DT	,
									PALLETDETNO_CD	,
									TYPEPICK_CD		,
									SRCLOCA_CD		,
									SKU_CD			,
									LOT_TX			,
									SUM(PCS_NR)
								FROM
									TA_PICK
								WHERE
									TOTALPIC_FL = 0 AND
									TYPEPICK_CD = '30' AND
									STPICK_ID <> -2
								GROUP BY
									TRUNC(ADD_DT)	,
									PALLETDETNO_CD	,
									TYPEPICK_CD		,
									SRCLOCA_CD		,
									SKU_CD			,
									LOT_TX
								) TMP
							GROUP BY
								TMP.ADD_DT			,
								TMP.PALLETDETNO_CD	,
								TMP.SKU_CD			,
								TMP.LOT_TX			
							HAVING
								COUNT(TMP.SRCLOCA_CD) > 1
							) TMP2
						GROUP BY
							TO_CHAR(TMP2.ADD_DT,'YYYY/MM/DD')
						) SUB4		-- 複数ロケピック指示書
			ON SUB1.DATE_TX = SUB4.DATE_TX
		LEFT OUTER JOIN 
						(
						SELECT
							SUB2.DATE_TX							,
							COUNT(SUB2.SKU_CD) AS SKUCOUNT_NR		,
							SUM(SUB2.LOCACOUNT_NR) AS LOCACOUNT_NR	,
							SUM(SUB2.PALLET_NR) AS PALLET_NR		,
							SUM(SUB2.CASE_NR) AS CASE_NR			,
							SUM(SUB2.TOTALCASE_NR) AS TOTALCASE_NR	,
							SUM(SUB2.PCS_NR) AS TOTALPCS_NR
						FROM
							(
							SELECT
								TO_CHAR(SUB.ADD_DT,'YYYY/MM/DD') AS DATE_TX		,
								SUB.SKU_CD										,
								SUB.SKU_TX										,
								SUB.LOT_TX										,
								SUB.PALLETCAPACITY_NR							,
								SUB.PCSPERCASE_NR								,
								COUNT(SUB.SRCLOCA_CD) AS LOCACOUNT_NR  			,
								TRUNC(SUM(SUB.CASE_NR) / SUB.PALLETCAPACITY_NR) AS PALLET_NR	,
								MOD(SUM(SUB.CASE_NR),SUB.PALLETCAPACITY_NR) AS CASE_NR		,
								SUM(SUB.CASE_NR) AS TOTALCASE_NR				,
								SUM(SUB.PCS_NR) AS PCS_NR
							FROM
								(
								SELECT
									TRUNC(TP.ADD_DT) AS ADD_DT					,
									TP.SRCLOCA_CD								,
									TP.SKU_CD									,
									MS.SKU_TX									,
									TP.LOT_TX									,
									TP.PALLETCAPACITY_NR						,
									MS.PCSPERCASE_NR							,
									SUM(TP.PCS_NR) / MS.PCSPERCASE_NR AS CASE_NR,
									SUM(TP.PCS_NR) AS PCS_NR
								FROM
									TA_PICK TP
										LEFT OUTER JOIN MA_SKU MS
											ON TP.SKU_CD = MS.SKU_CD
								WHERE
									TP.TOTALPIC_FL = 0 AND
									TP.STPICK_ID <> -2
								GROUP BY
									TRUNC(TP.ADD_DT)	,
									TP.SRCLOCA_CD		,
									TP.SKU_CD			,
									MS.SKU_TX			,
									TP.LOT_TX			,
									TP.PALLETCAPACITY_NR,
									MS.PCSPERCASE_NR
								) SUB
							GROUP BY
								TO_CHAR(SUB.ADD_DT,'YYYY/MM/DD'),
								SUB.SKU_CD		,
								SUB.SKU_TX		,
								SUB.LOT_TX		,
								SUB.PALLETCAPACITY_NR,
								SUB.PCSPERCASE_NR
							) SUB2
						GROUP BY
							SUB2.DATE_TX
						) SUB5
			ON SUB1.DATE_TX = SUB5.DATE_TX
		LEFT OUTER JOIN 
						(
						SELECT
							TO_CHAR(TMP.ADD_DT,'YYYY/MM/DD') AS DATE_TX	,
							COUNT(TMP.SUPPNO_CD) AS SUPPCOUNT_NR		,
							SUM(CASE_NR) AS SUPPCASE_NR
						FROM
							(
							SELECT
								TRUNC(ADD_DT) AS ADD_DT	,
								SUPPNO_CD				,
								SUM(CASE_NR) AS CASE_NR
							FROM
								TA_SUPP
							WHERE
								STSUPP_ID <> -2 AND
								TYPESUPP_CD = '10'	--緊急補充
							GROUP BY
								TRUNC(ADD_DT)	,
								SUPPNO_CD
							) TMP
						GROUP BY
							TO_CHAR(TMP.ADD_DT,'YYYY/MM/DD')
						) SUB6		-- 補充件数
			ON SUB1.DATE_TX = SUB6.DATE_TX
ORDER BY
	SUB1.DATE_TX
;
