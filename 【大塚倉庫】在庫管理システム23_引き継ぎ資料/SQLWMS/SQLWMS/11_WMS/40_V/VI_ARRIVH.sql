-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- ü×wb_[
--*******************************************
DROP VIEW VI_ARRIVH;
CREATE VIEW VI_ARRIVH
(
	TRN_ID			,	-- gUNVID
	END_FL			,	--
	ORDERNO_CD		,	-- L[
	HBKBN			,	-- HBæª@uH1vÅè
	SPSUCD			,	-- SPÆÒCD
	SPSEQ			,	-- SPV[PXNO1
	DKBN			,	-- f[^æª@HS:ü×ASH:o×A
	ENTKBN			,	-- Gg[æª
	SHIFT			,	-- Vtgæª
	HNNINUSHI		,	-- Ì×å
	NYMD			,	-- ×åïvút
	SYMD			,	-- qÉïvút
	TYUNO			,	-- [iú
	DENNO			,	-- `[Ô
	WKBN			,	-- `[ì¬æª
	GYO				,	-- ¾×s
	IYMD			,	-- `[óút
	NID				,	-- ×åæøæª
	NID_NM			,	-- ×åæø¼Ì
	SKID			,	-- qÉæøæª
	SKID_NM			,	-- qÉæø¼Ì
	ATSUKAI_NM		,	-- ×µïÐ
	SKECD			,	-- qÉcÆR[h
	SKECD_NM		,	-- qÉcÆ¼Ì
	SKECD_KCD		,	-- qÉcÆnæR[h
	SKECD_KEN1		,	-- qÉcÆ§R[h
	SKECD_KEN2		,	-- qÉcÆ£æª
	SKECD_JYUSYO	,	-- qÉcÆZ
	SKECD_TEL		,	-- qÉcÆTEL
	SYUKACD1		,	-- o×êR[h
	SUCD1			,	-- ÆÒR[h1
	SUCD1_NM		,	-- ÆÒ¼1
	SUCD2			,	-- ÆÒR[h2
	PKEN_FLG		,	-- PPtO
	PKENCD			,	-- PPR[h
	HATNO1			,	-- ­NO1
	BIKO1			,	-- õl1
	BIKO2			,	-- õl2
	BIKO_NM2		,	-- õl2Sp
	SYOHEAD			,	-- ¤iR[h¯Ê
	DEN_G_SRY		,	-- `[vÊ«
	G_SRY			,	-- [iv«
	DEN_G_WGT		,	-- `[vdÊ
	G_WGT			,	-- [ivdÊ
	WYMD			,	-- f[^ì¬ú
	WTIME			,	-- f[^ì¬Ô
	MAKERCD			,	-- [J[R[h
	MAKER_NM		,	-- [J[¼Ì
	COMCD			,	-- ïÐR[h
	COM_NM			,	-- ïÐ¼Ì
	SITENCD			,	-- xXR[h
	SITEN_NM		,	-- xX¼Ì
	SYUTYOCD		,	-- o£R[h
	SYUTYO_NM		,	-- o£¼Ì
	SYUTYO_KA		,	-- o£Û
	SITEN_JYUSYO	,	-- xXZ
	SITEN_TEL		,	-- xXTEL
	UTIKAE_FLG		,	-- Å¿Ï¦tO
	CCD				,	-- ¼æCD
	NOHCD			,	-- [iæCD
	NOHCD_NM1		,	-- [iæ¼Ì1
	NOHCD_NM2		,	-- [iæ¼Ì2
	NOHCD_NM3		,	-- [iæ¼Ì3
	KCD				,	-- næR[h
	KEN1			,	-- §R[h
	KEN2			,	-- £æª
	YUBIN			,	-- XÖÔ
	NOHCD_JYUSYO1	,	-- [iæZ1
	NOHCD_JYUSYO2	,	-- [iæZ2
	NOHCD_JYUSYO3	,	-- [iæZ3
	TEL				,	-- [iæTEL
	DAICD			,	-- ãXR[h
	DAICD_NM1		,	-- ãX¼Ì1
	DAICD_NM2		,	-- ãX¼Ì2
	DAICD_NM3		,	-- ãX¼Ì3
	NIJICD			,	-- ñXR[h
	NIJICD_NM1		,	-- ñX¼Ì1
	NIJICD_NM2		,	-- ñX¼Ì2
	NIJICD_NM3		,	-- ñX¼Ì3
	SANCD			,	-- OXR[h
	SANCD_NM1		,	-- OX¼Ì1
	SANCD_NM2		,	-- OX¼Ì2
	SANCD_NM3		,	-- OX¼Ì3
	CVS_FLG			,	-- RrjtO@CV:Rrjµ
	YUSEN1			,	-- DætO1
	YUSEN2				-- DætO2
) AS
	SELECT
		TMP1.TRN_ID			,	-- gUNVID
		TMP1.END_FL			,	-- 
		TRIM(TMP1.SKID) || TRIM(TMP1.DENNO) || TRIM(TMP1.SYMD) || TRIM(TMP1.HNNINUSHI),		-- L[(todo)
		TMP1.HBKBN			,	-- HBæª@uH1vÅè
	    TMP1.SPSUCD			,	-- SPÆÒCD
	    TMP1.SPSEQ			,	-- SPV[PXNO1
	    TMP1.DKBN			,	-- f[^æª@HS:ü×ASH:o×A
	    TMP1.ENTKBN			,	-- Gg[æª
	    TMP1.SHIFT			,	-- Vtgæª
	    TMP1.HNNINUSHI		,	-- Ì×å
	    TMP1.NYMD			,	-- ×åïvút
	    TMP1.SYMD			,	-- qÉïvút
	    TMP1.TYUNO			,	-- [iú
	    TMP1.DENNO			,	-- `[Ô
	    TMP1.WKBN			,	-- `[ì¬æª
	    TMP1.GYO			,	-- ¾×s
	    TMP1.IYMD			,	-- `[óút
	    TMP1.NID			,	-- ×åæøæª
	    TMP1.NID_NM			,	-- ×åæø¼Ì
	    TMP1.SKID			,	-- qÉæøæª
	    TMP1.SKID_NM		,	-- qÉæø¼Ì
	    TMP1.ATSUKAI_NM		,	-- ×µïÐ
	    TMP1.SKECD			,	-- qÉcÆR[h
	    TMP1.SKECD_NM		,	-- qÉcÆ¼Ì
	    TMP1.SKECD_KCD		,	-- qÉcÆnæR[h
	    TMP1.SKECD_KEN1		,	-- qÉcÆ§R[h
	    TMP1.SKECD_KEN2		,	-- qÉcÆ£æª
	    TMP1.SKECD_JYUSYO	,	-- qÉcÆZ
	    TMP1.SKECD_TEL		,	-- qÉcÆTEL
	    TMP1.SYUKACD1		,	-- o×êR[h
	    TMP1.SUCD1			,	-- ÆÒR[h1
	    TMP1.SUCD1_NM		,	-- ÆÒ¼1
	    TMP1.SUCD2			,	-- ÆÒR[h2
	    TMP1.PKEN_FLG		,	-- PPtO
	    TMP1.PKENCD			,	-- PPR[h
	    TMP1.HATNO1			,	-- ­NO1
	    TMP1.BIKO1			,	-- õl1
	    TMP1.BIKO2			,	-- õl2
	    TMP1.BIKO_NM2		,	-- õl2Sp
	    TMP1.SYOHEAD		,	-- ¤iR[h¯Ê
	    TMP1.DEN_G_SRY		,	-- `[vÊ«
	    TMP1.G_SRY			,	-- [iv«
	    TMP1.DEN_G_WGT		,	-- `[vdÊ
	    TMP1.G_WGT			,	-- [ivdÊ
	    TMP1.WYMD			,	-- f[^ì¬ú
	    TMP1.WTIME			,	-- f[^ì¬Ô
	    TMP2.MAKERCD		,	-- [J[R[h
	    TMP2.MAKER_NM		,	-- [J[¼Ì
	    TMP2.COMCD			,	-- ïÐR[h
	    TMP2.COM_NM			,	-- ïÐ¼Ì
	    TMP2.SITENCD		,	-- xXR[h
	    TMP2.SITEN_NM		,	-- xX¼Ì
	    TMP2.SYUTYOCD		,	-- o£R[h
	    TMP2.SYUTYO_NM		,	-- o£¼Ì
	    TMP2.SYUTYO_KA		,	-- o£Û
	    TMP2.SITEN_JYUSYO	,	-- xXZ
	    TMP2.SITEN_TEL		,	-- xXTEL
	    TMP2.UTIKAE_FLG		,	-- Å¿Ï¦tO
	    TMP2.CCD			,	-- ¼æCD
	    TMP2.NOHCD			,	-- [iæCD
	    TMP2.NOHCD_NM1		,	-- [iæ¼Ì1
	    TMP2.NOHCD_NM2		,	-- [iæ¼Ì2
	    TMP2.NOHCD_NM3		,	-- [iæ¼Ì3
	    TMP2.KCD			,	-- næR[h
	    TMP2.KEN1			,	-- §R[h
	    TMP2.KEN2			,	-- £æª
	    TMP2.YUBIN			,	-- XÖÔ
	    TMP2.NOHCD_JYUSYO1	,	-- [iæZ1
	    TMP2.NOHCD_JYUSYO2	,	-- [iæZ2
	    TMP2.NOHCD_JYUSYO3	,	-- [iæZ3
	    TMP2.TEL			,	-- [iæTEL
	    TMP2.DAICD			,	-- ãXR[h
	    TMP2.DAICD_NM1		,	-- ãX¼Ì1
	    TMP2.DAICD_NM2		,	-- ãX¼Ì2
	    TMP2.DAICD_NM3		,	-- ãX¼Ì3
	    TMP2.NIJICD			,	-- ñXR[h
	    TMP2.NIJICD_NM1		,	-- ñX¼Ì1
	    TMP2.NIJICD_NM2		,	-- ñX¼Ì2
	    TMP2.NIJICD_NM3		,	-- ñX¼Ì3
	    TMP2.SANCD			,	-- OXR[h
	    TMP2.SANCD_NM1		,	-- OX¼Ì1
	    TMP2.SANCD_NM2		,	-- OX¼Ì2
	    TMP2.SANCD_NM3		,	-- OX¼Ì3
	    TMP2.CVS_FLG		,	-- RrjtO@CV:Rrjµ
	    TMP2.YUSEN1			,	-- DætO1
	    TMP2.YUSEN2				-- DætO2
	FROM
		TMP_HEAD1 TMP1,
		TMP_HEAD2 TMP2
	WHERE
		TMP1.SPSUCD		= TMP2.SPSUCD		AND
		TMP1.SPSEQ		= TMP2.SPSEQ		AND
		TMP1.DKBN		= TMP2.DKBN			AND
		TMP1.ENTKBN		= TMP2.ENTKBN		AND
		TMP1.SHIFT		= TMP2.SHIFT		AND
		TMP1.HNNINUSHI	= TMP2.HNNINUSHI	AND
		TMP1.NYMD		= TMP2.NYMD			AND
		TMP1.SYMD		= TMP2.SYMD			AND
		TMP1.TYUNO		= TMP2.TYUNO		AND
		TMP1.DENNO		= TMP2.DENNO		AND
		TMP1.DKBN		= 'HS'
;

