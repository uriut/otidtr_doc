-- Copyright (C) Seaos Corporation 2014 All rights reserved
-- *******************************************
-- 作業実績分析−ピック（パレット成り分布）P5
-- *******************************************
	--CREATE OR REPLACE FORCE VIEW "WMS"."TEST_VR_TP_P5" ("ADD_DT1", "TYPE_OF_PICK", "NUM_OF_UNANALYZED", "NUM_OF_DETAIL", "PT30S", "PT30S60S", "PT60S90S", "PT90S120S", "PT120S180S", "PT3M15M", "PT15MUP") AS 
DROP VIEW VW_PAL_DISTRIBUTION;
CREATE VIEW VW_PAL_DISTRIBUTION
(
		ADD_DT1				,   --作業日
		TYPE_OF_PICK		,   --ピックタイプ
		NUM_OF_UNANALYZED	,	--計算対象外の指示書枚数
		NUM_OF_DETAIL		,   --有効ピック明細数（ピック時間が3分以内の物を対象する）
		PT30S				,   --ピック時間 <= 30秒
		PT30S60S			,   --30秒  < ピック時間 <= 60秒
		PT60S90S			,   --60秒  < ピック時間 <= 90秒
		PT90S120S			,   --90秒  < ピック時間 <= 120秒
		PT120S180S			,    --120秒 < ピック時間 <= 180秒
		PT3M15M				,
		PT15MUP				

) AS
	
	SELECT
		P5.ADD_DT1				,   --作業日
		P5.TYPE_OF_PICK			,   --ピックタイプ
		P5D.NUM_OF_UNANALYZED	,	--計算対象外の指示書枚数
		P5D.NUM_OF_DETAIL		,   --有効ピック明細数（ピック時間が3分以内の物を対象する）
		P5.PT30S				,   --ピック時間 <= 30秒
		P5.PT30S60S				,   --30秒  < ピック時間 <= 60秒
		P5.PT60S90S				,   --60秒  < ピック時間 <= 90秒
		P5.PT90S120S			,   --90秒  < ピック時間 <= 120秒
		P5.PT120S180S			,    --120秒 < ピック時間 <= 180秒
		P5.PT3M15M				,
		P5.PT15MUP				
	FROM
	(
		SELECT
			ADD_DT1,
			CASE
			  WHEN  (TYPEPICK_CD=40 OR TYPEPICK_CD=50) THEN 'パレット'
			END TYPE_OF_PICK,
			COUNT(
				CASE 
					WHEN TEST_pal_prt_time <= 0.000347222222222222 THEN 1 				--0:00:30    30秒より小さい
				END
			) AS PT30S,						--PT30=PICKING TIME FROM ZERO TO 30 SECONDS
			COUNT(
				CASE 
					WHEN (0.000347222222222222 < TEST_pal_prt_time) AND (TEST_pal_prt_time <= 0.000347222222222222*2) THEN 1			--0:00:30から0:01:00
				END
			) AS PT30S60S,
			COUNT(
				CASE 
					WHEN (0.000347222222222222*2 < TEST_pal_prt_time) AND (TEST_pal_prt_time <=0.000347222222222222*3) THEN 1
				END
			) AS PT60S90S,
			COUNT(
				CASE 
					WHEN (0.000347222222222222*3 < TEST_pal_prt_time) AND (TEST_pal_prt_time <= 0.000347222222222222*4) THEN 1
				END
			) AS PT90S120S,
			COUNT(
				CASE 
					WHEN (0.000347222222222222*4 < TEST_pal_prt_time) AND (TEST_pal_prt_time <= 0.000347222222222222*6) THEN 1
				END
			) AS PT120S180S,
			COUNT(
				CASE 
					WHEN ( 0.000347222222222222*6 < TEST_pal_prt_time) AND (TEST_pal_prt_time <= 0.000347222222222222*30) THEN 1
				END
			) AS PT3M15M,	  
			COUNT(
				CASE 
					WHEN ( 0.000347222222222222*30 < TEST_pal_prt_time) THEN 1
				END
			) AS PT15MUP
		FROM
		(
			SELECT
				ADD_DT1,
				TYPEPICK_CD,
				SRCHTUSER_CD1,
				SRCHTUSER_CD2,
				PALLETDETNO_CD,
				PRT_DT1,
				PRT_DT2,
				(PRT_DT2-PRT_DT1) AS TEST_pal_prt_time
			FROM
			(		
				SELECT
				PRT1.ADD_DT1,
				PRT1.SRCHTUSER_CD1,
				PRT1.PRT_DT1,
				PRT1.TYPEPICK_CD,
				PRT1.PALLETDETNO_CD,
				PRT1.SRCLOCA_CD,
				PRT1.EMP_ID_new,
				PRT2.ADD_DT2,
				PRT2.SRCHTUSER_CD2,
				PRT2.PRT_DT2,
				PRT2.EMP_ID
				FROM
				(
					SELECT
						ADD_DT AS ADD_DT1,
						SRCHTUSER_CD AS SRCHTUSER_CD1,
						PRT_DT AS PRT_DT1,
						TYPEPICK_CD,				
						PALLETDETNO_CD,
						SRCLOCA_CD,
						(EMP_ID + 1 ) AS EMP_ID_NEW				
					FROM
					(
						SELECT
							ADD_DT,
							SRCHTUSER_CD,
							PRT_DT,
							TYPEPICK_CD,				
							PALLETDETNO_CD,
							SRCLOCA_CD,
							ROW_NUMBER()
							OVER (ORDER BY add_dt) AS emp_id
						FROM
						(
							SELECT
								SUB.ADD_DT,
								SUB.SRCHTUSER_CD,
								SUB.PRT_DT,
								SUB.TYPEPICK_CD,
								SUB.PALLETDETNO_CD,
								SUB.SRCLOCA_CD
							FROM
							(
								SELECT
									TO_CHAR(TP.ADD_DT,'YYYY-MM-DD') AS ADD_DT,
									TP.SRCHTUSER_CD			,
									TP.PRT_DT,
									TP.PALLETDETNO_CD			,
									TP.TYPEPICK_CD				,
									TP.SRCLOCA_CD				
								FROM
									TA_PICK TP
								GROUP BY
									TO_CHAR(TP.ADD_DT,'YYYY-MM-DD'),
									TP.SRCHTUSER_CD			,
									TP.CHECKSTART_DT,
									TP.CHECKEND_DT,
									TP.PRT_DT,
									TP.PICKNO_CD				,
									TP.PALLETDETNO_CD			,
									TP.TYPEPICK_CD				,
									TP.SRCLOCA_CD				
							) SUB
							WHERE
								SUB.TYPEPICK_CD IN ('40','50')		--	AND -- todo
							GROUP BY
								SUB.ADD_DT,
								SUB.SRCHTUSER_CD,
								SUB.PRT_DT,
								SUB.TYPEPICK_CD,				
								SUB.PALLETDETNO_CD,
								SUB.SRCLOCA_CD
						)
						GROUP BY
							ADD_DT,
							SRCHTUSER_CD,
							PRT_DT,					
							TYPEPICK_CD,				
							PALLETDETNO_CD,
							SRCLOCA_CD
					)
					ORDER BY
					(EMP_ID + 1 ) ASC
				) PRT1
					LEFT OUTER JOIN
					(
						SELECT
							ADD_DT AS ADD_DT2,
							SRCHTUSER_CD AS SRCHTUSER_CD2,
							PRT_DT AS PRT_DT2,
							TYPEPICK_CD,				
							PALLETDETNO_CD,
							SRCLOCA_CD,
							ROW_NUMBER()
							OVER (ORDER BY add_dt) AS emp_id
						FROM
						(
							SELECT
							SUB.ADD_DT,
							SUB.SRCHTUSER_CD,
							SUB.PRT_DT,
							SUB.TYPEPICK_CD,
							SUB.PALLETDETNO_CD,
							SUB.SRCLOCA_CD
							FROM
							(
								SELECT
									TO_CHAR(TP.ADD_DT,'YYYY-MM-DD') AS ADD_DT,
									TP.SRCHTUSER_CD			,
									TP.PRT_DT,
									TP.PALLETDETNO_CD			,
									TP.TYPEPICK_CD				,
									TP.SRCLOCA_CD				
								FROM
								TA_PICK TP
								GROUP BY
									TO_CHAR(TP.ADD_DT,'YYYY-MM-DD'),
									TP.SRCHTUSER_CD			,
									TP.CHECKSTART_DT,
									TP.CHECKEND_DT,
									TP.PRT_DT,
									TP.PALLETDETNO_CD			,
									TP.TYPEPICK_CD				,
									TP.SRCLOCA_CD				
							) SUB
							WHERE
								SUB.TYPEPICK_CD IN ('40','50')			--AND -- todo
								GROUP BY
								SUB.ADD_DT,
								SUB.SRCHTUSER_CD,
								SUB.PRT_DT,
								SUB.TYPEPICK_CD,				
								SUB.PALLETDETNO_CD,
								SUB.SRCLOCA_CD
						)
						ORDER BY
						(EMP_ID + 1 ) ASC
							
					) PRT2							
					ON						
						PRT1.EMP_ID_NEW=PRT2.EMP_ID 				AND
						PRT1.ADD_DT1=PRT2.ADD_DT2 					AND
						PRT1.SRCHTUSER_CD1=PRT2.SRCHTUSER_CD2
			)
		)	
		GROUP BY
		ADD_DT1,	
		CASE
			WHEN  (TYPEPICK_CD=40 OR TYPEPICK_CD=50) THEN 'パレット'
		END		 
		ORDER BY
		ADD_DT1 DESC	 
	) P5
		LEFT OUTER JOIN
		(
			SELECT
			TO_CHAR(TP.ADD_DT,'YYYY-MM-DD') AS ADD_DT,
			COUNT(DISTINCT TP.PALLETDETNO_CD) AS NUM_OF_DETAIL,
			COUNT(DISTINCT TP.SRCHTUSER_CD) AS NUM_OF_UNANALYZED
			FROM
			TA_PICK TP
			WHERE
			TYPEPICK_CD IN ('40', '50')
			GROUP BY
			TO_CHAR(TP.ADD_DT,'YYYY-MM-DD')
		) P5D
			ON	P5D.ADD_DT = P5.ADD_DT1
			ORDER BY
			ADD_DT1 DESC