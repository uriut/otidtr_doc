-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- 出荷指示詳細照会
--*******************************************
DROP VIEW VW_WA502;
CREATE VIEW VW_WA502
(
	SHIPH_ID					,	/* ITF出荷指示データヘッダID */
	STSHIP_ID					,	/* 出荷状態コード */
	STSHIP_TX					,	/* 出荷状態 */
	TRANSPORTER1_CD				,	/* 配送業者コード */
	TRANSPORTERGROUP_TX			,	/* 配送業者グループ名 */
	TRANSPORTER_TX				,	/* 配送業者名 */
	DTPOSTAL_CD					,	/* 地区コード */
	DELIVERYDESTINATION_CD		,	/* 納品先コード */
	DTNAME_TX					,	/* 納品先名	*/
	TOTALPIC_TX					,	/* トータルピック */
	SLIPNO_TX					,	/* 伝票番号 */
	END_DT						,	/* 出荷日(条件用) */
	SHIPPLAN_DT					,	/* 出荷予定時刻(条件用) */
	DIRECTDELIVERY_CD			,	/* 倉庫直送コード(条件用) */
	ORDERDETNO_CD				,	/* 伝票行 */
	SKU_CD						,	/* SKUコード */
	ITFCODE_TX					,	/* ITFコード */
	LOT_TX						,	/* 製造ロット */
	SKU_TX						,	/* 商品名 */
	EXTERIOR_CD					,	/* 外装コード */
	SIZE_TX						,	/* サイズ */
	BARA_NR						,	/* バラ数 */
	CASE_NR						,	/* ケース数 */
	PALLET_NR						/* パレット数 */
) AS
SELECT
	TASH.SHIPH_ID					,	/* ITF出荷指示データヘッダID */
	TASH.STSHIP_ID					,	/* 出荷状態コード */
	MST.STSHIP_TX					,	/* 出荷状態 */
	TASH.TRANSPORTER1_CD			,	/* 配送業者コード */
	MAT.TRANSPORTERGROUP_TX			,	/* 配送業者グループ名 */
	MAT.TRANSPORTER_TX				,	/* 配送業者名 */
	TASH.DTPOSTAL_CD				,	/* 地区コード */
	TASH.DELIVERYDESTINATION_CD		,	/* 納品先コード */
	TASH.DTNAME_TX					,	/* 納品先名	*/
	MBT.TOTALPIC_TX					,	/* トータルピック */
	TASH.SLIPNO_TX					,	/* 伝票番号 */
	TASH.END_DT						,	/* 出荷日(条件用) */
	TASH.SHIPPLAN_DT				,	/* 出荷予定時刻(条件用) */
	TASH.DIRECTDELIVERY_CD			,	/* 倉庫直送コード(条件用) */
	TASD.ORDERDETNO_CD				,	/* 伝票行 */
	TASD.SKU_CD						,	/* SKUコード */
	MSK.ITFCODE_TX					,	/* ITFコード */
	TASD.LOT_TX						,	/* 製造ロット */
	MSK.SKU_TX						,	/* 商品名 */
	MSK.EXTERIOR_CD					,	/* 外装コード */
	MSK.SIZE_TX						,	/* サイズ */
	COALESCE(SUM(TASD.ORDERBARA_NR),0) AS BARA_NR		,	/* バラ数 */
	COALESCE(SUM(TASD.ORDERCASE_NR),0) AS CASE_NR		,	/* ケース数 */
	COALESCE(SUM(TASD.ORDERPALLET_NR),0) AS PALLET_NR		/* パレット数 */
FROM
	TA_SHIPH TASH
		LEFT OUTER JOIN TA_SHIPD TASD
			ON TASH.SHIPH_ID = TASD.SHIPH_ID
		LEFT OUTER JOIN MA_TRANSPORTER MAT
			ON TRIM(TASH.TRANSPORTER1_CD) = MAT.TRANSPORTER_CD
		LEFT OUTER JOIN MA_SKU MSK
			ON TASD.SKU_CD = MSK.SKU_CD
		LEFT OUTER JOIN MB_STSHIP MST			
			ON TASH.STSHIP_ID = MST.STSHIP_ID	
		LEFT OUTER JOIN MB_TOTALPIC MBT
			ON MAT.TOTALPIC_FL = MBT.TOTALPIC_FL
GROUP BY
	TASH.SHIPH_ID					,	/* ITF出荷指示データヘッダID */
	TASH.STSHIP_ID					,	/* 出荷状態コード */
	MST.STSHIP_TX					,	/* 出荷状態 */
	TASH.TRANSPORTER1_CD			,	/* 配送業者コード */
	MAT.TRANSPORTERGROUP_TX			,	/* 配送業者グループ名 */
	MAT.TRANSPORTER_TX				,	/* 配送業者名 */
	TASH.DTPOSTAL_CD				,	/* 地区コード */
	TASH.DELIVERYDESTINATION_CD		,	/* 納品先コード */
	TASH.DTNAME_TX					,	/* 納品先名	*/
	MBT.TOTALPIC_TX					,	/* トータルピック */
	TASH.SLIPNO_TX					,	/* 伝票番号 */
	TASH.END_DT						,	/* 出荷日(条件用) */
	TASH.SHIPPLAN_DT				,	/* 出荷予定時刻(条件用) */
	TASH.DIRECTDELIVERY_CD			,	/* 倉庫直送コード(条件用) */
	TASD.ORDERDETNO_CD				,	/* 伝票行 */
	TASD.SKU_CD						,	/* SKUコード */
	MSK.ITFCODE_TX					,	/* ITFコード */
	TASD.LOT_TX						,	/* 製造ロット */
	MSK.SKU_TX						,	/* 商品名 */
	MSK.EXTERIOR_CD					,	/* 外装コード */
	MSK.SIZE_TX							/* サイズ */
;
