-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- SKUパレットマスタ
--*******************************************
DROP VIEW VW_WA331;
CREATE VIEW VW_WA331
(
	ID					,	/* プライマリキー */
	SKU_CD				,	/* 商品コード */
	EXTERIOR_CD			,	/* 外装コード */
	SKU_TX				,	/* 商品名 */
	MANUFACTURER_CD		,	/* 工場区分 */
	CASEPERPALLET_NR	,	/* パレット積み付け数 */
	PALLETLAYER_NR		,	/* パレット段数 */
	CASEPERLAYER_NR		,	/* パレット回し数 */
	PALLETCAPACITY_NR	,	/* パレット積載数（※データ定義書に記載なし） */
	STACKINGNUMBER_NR	,	/* 保管段数 */
	UPD_DT				,	/* 登録日時 */
	UPDUSER_CD			,	/* 登録ユーザコード */
	UPDUSER_TX			,	/* 登録ユーザ名 */
	ADD_DT				,	/* 更新日時 */
	ADDUSER_CD			,	/* 更新ユーザコード */
	ADDUSER_TX			,	/* 更新ユーザ名 */
	UPDPROGRAM_CD		,	/* 更新プログラムコード */
	UPDCOUNTER_NR		,	/* 更新カウンター */
	STRECORD_ID				/* レコード状態 */
) AS
SELECT
	MSP.ID					,	/* プライマリキー */
	MSP.SKU_CD				,	/* 商品コード */
	MSK.EXTERIOR_CD			,	/* 外装コード */
	MSK.SKU_TX				,	/* 商品名 */
	MSP.MANUFACTURER_CD		,	/* 工場区分 */
	MSP.CASEPERPALLET_NR	,	/* パレット積み付け数 */
	MSP.PALLETLAYER_NR		,	/* パレット段数 */
	MSP.CASEPERLAYER_NR		,	/* パレット回し数 */
	MSP.PALLETCAPACITY_NR	,	/* パレット積載数（※データ定義書に記載なし） */
	MSP.STACKINGNUMBER_NR	,	/* 保管段数 */
	MSP.UPD_DT				,	/* 登録日時 */
	MSP.UPDUSER_CD			,	/* 登録ユーザコード */
	MSP.UPDUSER_TX			,	/* 登録ユーザ名 */
	MSP.ADD_DT				,	/* 更新日時 */
	MSP.ADDUSER_CD			,	/* 更新ユーザコード */
	MSP.ADDUSER_TX			,	/* 更新ユーザ名 */
	MSP.UPDPROGRAM_CD		,	/* 更新プログラムコード */
	MSP.UPDCOUNTER_NR		,	/* 更新カウンター */
	MSP.STRECORD_ID				/* レコード状態 */
FROM
	MA_SKUPALLET MSP
		LEFT OUTER JOIN MA_SKU MSK
			ON MSP.SKU_CD = MSK.SKU_CD
;
