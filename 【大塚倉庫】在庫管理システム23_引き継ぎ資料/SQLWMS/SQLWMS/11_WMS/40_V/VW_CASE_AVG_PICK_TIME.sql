-- Copyright (C) Seaos Corporation 2014 All rights reserved
-- *******************************************
-- 作業実績分析−ピック（パレット成り詳細）P2
-- *******************************************
DROP VIEW VW_CASE_AVG_PICK_TIME;
CREATE VIEW VW_CASE_AVG_PICK_TIME
(
	ADD_DT				,		--作業日
	TYPE_OF_PICK		,		--ピックタイプ
	LOCA_OF_BLOCK		,		--ピックされたロケ
	NUM_DETAILS			,		--明細数
	TOTAL_CASE_NUM		,		--ピックされたケース数
	AVG_CASE_PICK				--ピック平均時間　　（平均ピック時間は明細毎の品物ピックにかかった時間のみ。移動時間は指示書毎にかかる時間のため、対象外とする）
) AS
	SELECT
		BASIC.ADD_DT,
		BASIC.TYPE_OF_PICK,
		BASIC.LOCA_OF_BLOCK,
		BASIC.NUM_DETAILS,
		AVE_T.TOTAL_CASE_NUM,
		AVE_T.AVG_CASE_PICK
	FROM
	(
		SELECT
			ADD_DT,
			CASE
				WHEN  (TYPEPICK_CD=30) THEN 'ケース'
			END TYPE_OF_PICK,
			CASE
				WHEN
				TYPEPICK_CD=30 THEN
				CASE
					WHEN
					(substr(SRCLOCA_CD,13,2)= '00' OR
					substr(SRCLOCA_CD,13,2)= '11') THEN '平積み'
				ELSE  'ネステナ2段以上'
				END
			END LOCA_OF_BLOCK,
			COUNT( PALLETDETNO_CD) AS NUM_DETAILS
		FROM
		(
			SELECT
				SUB.ADD_DT,
				SUB.PALLETDETNO_CD			,
				SUB.TYPEPICK_CD				,
				SUB.SRCLOCA_CD				
			FROM
			(
				SELECT
					TO_CHAR(TP.ADD_DT,'YYYY-MM-DD') AS ADD_DT,			
					TP.PALLETDETNO_CD			,
					TP.TYPEPICK_CD				,
					TP.SRCLOCA_CD				
				FROM
					TA_PICK TP
				GROUP BY
					TO_CHAR(TP.ADD_DT,'YYYY-MM-DD'),
					TP.PALLETDETNO_CD			,
					TP.TYPEPICK_CD				,
					TP.SRCLOCA_CD				
			) SUB
		WHERE
			SUB.TYPEPICK_CD = '30'
	)
	GROUP BY
		ADD_DT,
		CASE
			WHEN  (TYPEPICK_CD=30) THEN 'ケース'
		END,
		CASE WHEN
			TYPEPICK_CD=30 THEN
			CASE
				WHEN
				(substr(SRCLOCA_CD,13,2)= '00' OR
				substr(SRCLOCA_CD,13,2)= '11') THEN '平積み'
				ELSE  'ネステナ2段以上'
			END
		END
	ORDER BY
		ADD_DT DESC
	) BASIC				  
		LEFT OUTER JOIN
		( 	
			SELECT
			TO_CHAR(TAPI.ADD_DT,'YYYY-MM-DD') AS add_dt,
			CASE
				WHEN  (TAPI.TYPEPICK_CD=30) THEN 'ケース'
			END TYPE_OF_PICK,
			CASE
				WHEN
				TAPI.TYPEPICK_CD=30 THEN
				CASE
					WHEN
					(substr(TAPI.SRCLOCA_CD,13,2)= '00'	OR
					substr(TAPI.SRCLOCA_CD,13,2)= '11') THEN '平積み'
					ELSE  'ネステナ2段以上'
				END
			END LOCA_OF_BLOCK,
			SUM(CASE
					WHEN (TAPI.TYPEPICK_CD=40) THEN TAPI.CASEPERPALLET_NR    
					WHEN (TAPI.TYPEPICK_CD=30　OR TAPI.TYPEPICK_CD=50) THEN TAPI.CASE_NR
				END) AS TOTAL_CASE_NUM,			
			TRUNC(
				(
					AVG(( TAPI.CHECKEND_DT-TAPI.CHECKSTART_DT)*24)
					-
					TRUNC(
						AVG(( TAPI.CHECKEND_DT-TAPI.CHECKSTART_DT)*24)
					) 						
				)*60
			) || '分' ||
			TRUNC(
				(
					(
						AVG(( TAPI.CHECKEND_DT-TAPI.CHECKSTART_DT)*24)
						-
						TRUNC(
							AVG(( TAPI.CHECKEND_DT-TAPI.CHECKSTART_DT)*24)
						) 
					)*60
					-
					TRUNC(
						(
							AVG(( TAPI.CHECKEND_DT-TAPI.CHECKSTART_DT)*24)
							-
							TRUNC(
								AVG(( TAPI.CHECKEND_DT-TAPI.CHECKSTART_DT)*24)
							) 
						)*60
					) 
				)*60
			) || '秒'	AS AVG_CASE_PICK
			FROM
				TA_PICK TAPI
			WHERE
				TAPI.CHECKHTUSER_TX = TAPI.CHECKHTUSER_TX	AND
				TAPI.TYPEPICK_CD = 30 						AND
				(TAPI.CHECKEND_DT - TAPI.CHECKSTART_DT) < 0.002083*5       --15分以内のものを対象する
			GROUP BY
				TO_CHAR(TAPI.ADD_DT,'YYYY-MM-DD'),
				CASE
					WHEN  (TAPI.TYPEPICK_CD=30) THEN 'ケース'
				END, 
				CASE
					WHEN
					TAPI.TYPEPICK_CD=30 THEN
					CASE
						WHEN
						(substr(TAPI.SRCLOCA_CD,13,2)= '00' OR
						substr(TAPI.SRCLOCA_CD,13,2)= '11') THEN '平積み'
						ELSE  'ネステナ2段以上'
					END
				END
			ORDER BY
				TO_CHAR(TAPI.ADD_DT,'YYYY-MM-DD') DESC
		) AVE_T
		ON  BASIC.ADD_DT=AVE_T.ADD_DT				AND
			BASIC.TYPE_OF_PICK=AVE_T.TYPE_OF_PICK	AND
			BASIC.LOCA_OF_BLOCK=AVE_T.LOCA_OF_BLOCK