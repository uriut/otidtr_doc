-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- 仕入先マスタ
--*******************************************
DROP VIEW VW_WA341;
CREATE VIEW VW_WA341
(
	SUPPLIER_CD				,	/* 仕入先コード */
	TYPESUPPLIER_CD			,	/* 仕入先種類 */
	TYPESUPPLIER_TX			,	/* 仕入先種類名 */
	SUPPLIER_TX				,	/* 仕入先名 */
	SUPPLIERPOST_TX			,	/* 郵便番号 */
	SUPPLIERADDRESS_TX		,	/* 仕入れ先住所 */
	SUPPLIERMEMO_TX			,	/* 仕入先備考 */
	ADD_DT					,	/* 登録日時 */
	ADDUSER_CD				,	/* 登録ユーザコード */
	ADDUSER_TX				,	/* 登録ユーザ名 */
	UPD_DT					,	/* 更新日時 */
	UPDUSER_CD				,	/* 更新ユーザコード */
	UPDUSER_TX				,	/* 更新ユーザ名 */
	UPDPROGRAM_CD			,	/* 更新プログラムコード */
	UPDCOUNTER_NR			,	/* 更新カウンター */
	STRECORD_ID					/* レコード状態 */
) AS
SELECT
	MSU.SUPPLIER_CD			,	/* 仕入先コード */
	MSU.TYPESUPPLIER_CD		,	/* 仕入先種類 */
	DECODE(MSU.TYPESUPPLIER_CD, '01', '外販', '02', '工場', '03', 'ＳＰ')
		AS TYPESUPPLIER_TX	,	/* 仕入先種類名 */
	MSU.SUPPLIER_TX			,	/* 仕入先名 */
	MSU.SUPPLIERPOST_TX		,	/* 郵便番号 */
	MSU.SUPPLIERADDRESS_TX	,	/* 仕入れ先住所 */
	MSU.SUPPLIERMEMO_TX		,	/* 仕入先備考 */
	MSU.ADD_DT				,	/* 登録日時 */
	MSU.ADDUSER_CD			,	/* 登録ユーザコード */
	MSU.ADDUSER_TX			,	/* 登録ユーザ名 */
	MSU.UPD_DT				,	/* 更新日時 */
	MSU.UPDUSER_CD			,	/* 更新ユーザコード */
	MSU.UPDUSER_TX			,	/* 更新ユーザ名 */
	MSU.UPDPROGRAM_CD		,	/* 更新プログラムコード */
	MSU.UPDCOUNTER_NR		,	/* 更新カウンター */
	MSU.STRECORD_ID				/* レコード状態 */
FROM
	MA_SUPPLIER MSU
;
