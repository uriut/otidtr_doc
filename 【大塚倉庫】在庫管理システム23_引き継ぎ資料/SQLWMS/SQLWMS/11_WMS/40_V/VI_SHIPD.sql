-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- o×w¦¾×
--*******************************************
DROP VIEW VI_SHIPD;
CREATE VIEW VI_SHIPD
(
	TRN_ID			,	-- gUNVID
	END_FL			,	-- 
	ORDERNO_CD		,	-- L[
	HBKBN			,	-- HBæª
	SPSUCD			,	-- SPÆÒCD
	SPSEQ			,	-- SPV[PXNO1
	DKBN			,	-- f[^æª@HS:ü×ASH:o×A
	ENTKBN			,	-- Gg[æª
	SHIFT			,	-- Vtgæª
	HNNINUSHI		,	-- Ì×å
	NYMD			,	-- ×åïvút
	SYMD			,	-- qÉïvút
	TYUNO			,	-- [iú
	DENNO			,	-- `[Ô
	SKID			,	-- qÉæª
	SKID_NM			,	-- qÉæª
	GYO				,	-- ¾×s
	L_RETU			,	-- ¾×sñ
	BCD				,	-- ¨¬R[h
	SKU_CD			,	-- Ì×å + ×å¤iR[h
	NSYOCD			,	-- ×å¤iR[h
	DRK				,	-- dª
	SEINAK			,	-- ¤i¼i¶Åj
	SEINAN			,	-- ¤i¼
	KIKAKUK			,	-- Kii¶Åj
	KIKAKUN			,	-- Ki
	KHKBN			,	-- «/[æª
	KHKBN_NM		,	-- PÊ¼Ì
	MGKBN			,	-- ¢iæª
	MITATSU			,	-- ¢Bæª
	SRY				,	-- Ê
	CASE_NR			,
	BARA_NR			,
	PCS_NR			,
	SRYK			,	-- «Ê
	SRYH			,	-- [
	JSRY			,	-- óÊ
	HTYUNO1			,	-- ­NO1
	SYUKA1			,	-- o×ê1
	SKCD1			,	-- qÉR[h1
	SKCD_NM1		,	-- qÉ¼Ì1
	LOTSRY			,	-- bgÊ
	LOT_FLG			,	-- bg\¦tO@1:bg\¦ è
	LOT				,	-- bg
	LOTSUB			,	-- Tubg
	LOTL			,	-- Cbg
	LOTC			,	-- Ly[tO
	LOTP			,	-- ¶Yvg
	FCKBN			,	-- HêL
	FCKBNN			,	-- Hêª
	LOTK			,	-- Çæª
	LOTS			,	-- o×æª
	KAHIKBN			,	-- ÂÛæª
	LOTDT			,	-- bgL `±
	KIGEN			,	-- Ü¡úÀ
	ZSRYK			,	-- cÉ«Ê
	ZSRYH			,	-- cÉ[
	KAN1			,	-- üÚP(P[XÌÌo)
	KAN2			,	-- üÚQ(o)
	KAN3			,	-- üÚR(ÔÌÌo)
	KAN4			,	-- ÀüÚ
	SKBN			,	-- ¿æª
	HOREI			,	-- Ûâæª
	KATA			,	-- ^R[h
	TATE			,	-- c
	YOKO			,	-- ¡
	TAKA			,	-- ³
	WGT				,	-- «dÊ(P[XdÊ)g
	MWGT			,	-- ¾×dÊ(bgPÊÌ¾×dÊ)kg
	SYOCD1			,	-- ¤iR[h1
	GSYOCD			,	-- O¤iR[h
	PLSRY			,	-- okÏ
	FLG1			,	-- \õtO1
	MBIKO				-- õl
) AS
	SELECT
		TMP1.TRN_ID			,	-- gUNVID
		TMP1.END_FL			,	-- 
		TRIM(TMP1.SKID) || TRIM(TMP1.DENNO) || TRIM(TMP1.SYMD),		-- L[itodoj
		TMP1.HBKBN			,	-- HBæª
	    TMP1.SPSUCD			,	-- SPÆÒCD
	    TMP1.SPSEQ			,	-- SPV[PXNO1
	    TMP1.DKBN			,	-- f[^æª@HS:ü×ASH:o×A
	    TMP1.ENTKBN			,	-- Gg[æª
	    TMP1.SHIFT			,	-- Vtgæª
	    TMP1.HNNINUSHI		,	-- Ì×å
	    TMP1.NYMD			,	-- ×åïvút
	    TMP1.SYMD			,	-- qÉïvút
	    TMP1.TYUNO			,	-- [iú
	    TMP1.DENNO			,	-- `[Ô
	    TMP1.SKID			,
	    TMP1.SKID_NM		,
	    TMP2.GYO			,	-- ¾×s
	    TMP2.L_RETU			,	-- ¾×sñ
	    TMP2.BCD			,	-- ¨¬R[h
	    TRIM(TMP1.HNNINUSHI) || TRIM(TMP2.NSYOCD) AS SKU_CD,
	    TMP2.NSYOCD			,	-- ×å¤iR[h
	    TMP2.DRK			,	-- dª
	    TMP2.SEINAK			,	-- ¤i¼i¶Åj
	    TMP2.SEINAN			,	-- ¤i¼
	    TMP2.KIKAKUK		,	-- Kii¶Åj
	    TMP2.KIKAKUN		,	-- Ki
	    TMP2.KHKBN			,	-- «/[æª
	    TMP2.KHKBN_NM		,	-- PÊ¼Ì
	    TMP2.MGKBN			,	-- ¢iæª
	    TMP2.MITATSU		,	-- ¢Bæª
	    TMP2.SRY			,	-- Ê
	    -- 
	    CASE
	    	WHEN TMP2.KHKBN = 0 THEN -- 0:P[X
	    		TMP2.SRY
	    	WHEN TMP2.KHKBN = 1 THEN -- 1:o
	    		((TMP2.SRY) - MOD(TMP2.SRY,TMP2.KAN1)) / TMP2.KAN1
	    	ELSE 0
	    END AS CASE_NR		,
	    CASE
	    	WHEN TMP2.KHKBN = 0 THEN -- 0:P[X
	    		0
	    	WHEN TMP2.KHKBN = 1 THEN -- 1:o
	    		MOD(TMP2.SRY,TMP2.KAN1)
	    	ELSE 0
	    END AS BARA_NR		,
	    CASE
	    	WHEN TMP2.KHKBN = 0 THEN -- 0:P[X
	    		TMP2.SRY * TMP2.KAN1
	    	WHEN TMP2.KHKBN = 1 THEN -- 1:o
	    		TMP2.SRY
	    	ELSE 0
	    END AS PCS_NR		,
	    TMP2.SRYK			,	-- «Ê
	    TMP2.SRYH			,	-- [
	    TMP2.JSRY			,	-- óÊ
	    TMP2.HTYUNO1		,	-- ­NO1
	    TMP2.SYUKA1			,	-- o×ê1
	    TMP2.SKCD1			,	-- qÉR[h1
	    TMP2.SKCD_NM1		,	-- qÉ¼Ì1
	    TMP2.LOTSRY			,	-- bgÊ
	    TMP2.LOT_FLG		,	-- bg\¦tO@1:bg\¦ è
	    TMP2.LOT			,	-- bg
	    TMP2.LOTSUB			,	-- Tubg
	    TMP2.LOTL			,	-- Cbg
	    TMP2.LOTC			,	-- Ly[tO
	    TMP2.LOTP			,	-- ¶Yvg
	    TMP2.FCKBN			,	-- HêL
	    TMP2.FCKBNN			,	-- Hêª
	    TMP2.LOTK			,	-- Çæª
	    TMP2.LOTS			,	-- o×æª
	    TMP2.KAHIKBN		,	-- ÂÛæª
	    TMP2.LOTDT			,	-- bgL `±
	    TMP2.KIGEN			,	-- Ü¡úÀ
	    TMP2.ZSRYK			,	-- cÉ«Ê
	    TMP2.ZSRYH			,	-- cÉ[
	    TMP2.KAN1			,	-- üÚP(P[XÌÌo)
	    TMP2.KAN2			,	-- üÚQ(o)
	    TMP2.KAN3			,	-- üÚR(ÔÌÌo)
	    TMP2.KAN4			,	-- ÀüÚ
	    TMP2.SKBN			,	-- ¿æª
	    TMP2.HOREI			,	-- Ûâæª
	    TMP2.KATA			,	-- ^R[h
	    TMP2.TATE			,	-- c
	    TMP2.YOKO			,	-- ¡
	    TMP2.TAKA			,	-- ³
	    TMP2.WGT			,	-- «dÊ(P[XdÊ)g
	    TMP2.MWGT			,	-- ¾×dÊ(bgPÊÌ¾×dÊ)kg
	    TMP2.SYOCD1			,	-- ¤iR[h1
	    TMP2.GSYOCD			,	-- O¤iR[h
	    TMP2.PLSRY			,	-- okÏ
	    TMP2.FLG1			,	-- \õtO1
	    TMP2.MBIKO				-- õl
	FROM
		TMP_HEAD1 TMP1,
		TMP_BODY TMP2
	WHERE
		TMP1.SPSUCD		= TMP2.SPSUCD		AND
		TMP1.SPSEQ		= TMP2.SPSEQ		AND
		TMP1.DKBN		= TMP2.DKBN			AND
		TMP1.ENTKBN		= TMP2.ENTKBN		AND
		TMP1.SHIFT		= TMP2.SHIFT		AND
		TMP1.HNNINUSHI	= TMP2.HNNINUSHI	AND
		TMP1.NYMD		= TMP2.NYMD			AND
		TMP1.SYMD		= TMP2.SYMD			AND
		TMP1.TYUNO		= TMP2.TYUNO		AND
		TMP1.DENNO		= TMP2.DENNO		AND
		TMP1.DKBN		= 'SH'
;

