-- Copyright (C) Seaos Corporation 2012 All rights reserved
-- *******************************************
-- ロケーション別在庫照会
-- *******************************************
DROP VIEW VW_WA701;
CREATE VIEW VW_WA701
(
	LOCA_CD					,	-- ロケーション
	LOCA_TX					,	-- ロケーション名
	WH_CD					,	-- 倉庫コード 
	FLOOR_CD				,	-- フロアーコード 
	AREA_CD					,	-- エリアコード 
	LINE_CD					,	-- 列コード 
	SHELF_CD				,	-- 棚コード 
	LAYER_CD				,	-- 段コード 
	ORDER_CD				,	-- 番コード 
	TYPELOCABLOCK_CD		,	-- ロケーションブロックコード
	TYPELOCABLOCK_TX		,	-- ロケーションブロック名
	HEQUIP_CD				,	-- 格納什器コード
	ARRIVPRIORITY_NR		,	-- 入庫優先順位
	SHIPPRIORITY_NR			,	-- 出庫優先順位
	INVENT_FL				,	-- 棚卸対象フラグ
	NOTUSE_TX				,	-- ロケーション使用／未使用
	NOTSHIP_TX				,	-- 出荷引当　可／不可
	SKU_CD					,	-- ＳＫＵコード
	KANBAN_CD				,	-- かんばん番号
	LOT_TX					,	-- ロット
	EXTERIOR_CD				,	-- 外装コード
	SIZE_TX					,	-- 規格
	TOTALPALLET_NR			,	-- 総パレット数
	TOTALCASE_NR			,	-- 総ケース数
	TOTALBARA_NR			,	-- 総バラ数
	TOTALPCS_NR				,	-- 総ＰＣＳ数
	PPALLET_NR				,	-- 引当可能パレット数
	PCASE_NR				,	-- 引当可能ケース数
	PBARA_NR				,	-- 引当可能バラ数
	PPCS_NR					,	-- 引当可能ＰＣＳ
	CPALLET_NR				,	-- 引当済みパレット数
	CCASE_NR				,	-- 引当済みケース数
	CBARA_NR				,	-- 引当済みバラ数
	CPCS_NR					,	-- 引当済みＰＣＳ
	TYPESTOCK_CD			,	-- 在庫タイプコード
	TYPESTOCK_TX			,	-- 在庫タイプ名
	TYPELOCA_CD				,	-- ロケーションタイプコード
	TYPELOCA_TX				,	-- ロケーションタイプ名
	SHIPVOLUME_CD			,	-- 出荷荷姿コード
	SHIPVOLUME_TX			,	-- 出荷荷姿名
	STSTOCK_ID				,	-- 在庫状態ID
	STSTOCK_TX				,	-- 在庫状態名
	SKU_TX					,	-- SKU名
	SKUBARCODE_TX			,	-- JANコード
	ITFCODE_TX				,	-- ITFコード
	AMOUNT_NR				,	-- 入数 標準ケース・メーカー箱の入数
	LINEUNIT_NR				,	-- ラインユニット
	CASEPERPALLET_NR		,
	PALLETLAYER_NR			,
	LOCAEMPTY_FL			,	-- 空きロケフラグ 1:空き 0:使用済
	ABC_ID					,	-- ロケーションABCID
	ABC_CD					,	-- ロケーションABC
	ABCSUB_ID				,	-- 重量グループID
	ABCSUB_TX					-- 重量グループ
) AS
	SELECT
		MLC.LOCA_CD 										,
		MLC.LOCA_TX 										,
		MLC.WH_CD 											,
		MLC.FLOOR_CD 										,
		MLC.AREA_CD 										,
		MLC.LINE_CD 										,
		MLC.SHELF_CD 										,
		MLC.LAYER_CD 										,
		MLC.ORDER_CD 										,
		MLC.TYPELOCABLOCK_CD 								,
		MTB.TYPELOCABLOCK_TX 								,
		MLC.HEQUIP_CD 										,
		MLC.ARRIVPRIORITY_NR 								,
		MLC.SHIPPRIORITY_NR 								,
		MLC.INVENT_FL 										,
		DECODE(MLC.NOTUSE_FL,0, '入荷可', 1, '入荷不可')	,
		DECODE(MLC.NOTSHIP_FL,0, '出荷可', 1, '出荷不可')	,
		LS2.SKU_CD 											,
		LS2.KANBAN_CD 										,
		LS2.LOT_TX 											,
		LS2.EXTERIOR_CD 									,
		LS2.SIZE_TX 										,
		LS2.TOTALPALLET_NR 									,
		LS2.TOTALCASE_NR 									,
		LS2.TOTALBARA_NR 									,
		LS2.TOTALPCS_NR 									,
		LS2.PPALLET_NR 										,
		LS2.PCASE_NR 										,
		LS2.PBARA_NR 										,
		LS2.PPCS_NR 										,
		LS2.CPALLET_NR 										,
		LS2.CCASE_NR 										,
		LS2.CBARA_NR 										,
		LS2.CPCS_NR 										,
		LS2.TYPESTOCK_CD 									,
		LS2.TYPESTOCK_TX 									,
		MLC.TYPELOCA_CD 									,
		MTL.TYPELOCA_TX 									,
		MLC.SHIPVOLUME_CD 									,
		MBSH.SHIPVOLUME_TX 									,
		LS2.STSTOCK_ID										,
		LS2.STSTOCK_TX 										,
		REPLACE(REPLACE(LS2.SKU_TX, Chr(13), ''),Chr(10), ''),
		LS2.SKUBARCODE_TX 									,
		LS2.ITFCODE_TX 										,
		LS2.AMOUNT_NR 										,
		MLC.LINEUNIT_NR 									,
		LS2.CASEPERPALLET_NR 								,
		LS2.PALLETLAYER_NR									,
		CASE
			WHEN LS2.SKU_CD IS NULL AND TMP.LOCA_CD IS NULL THEN 1 --空きロケ
			ELSE 0												--使用ロケ
		END AS LOCAEMPTY_FL									,
		MLC.ABC_ID											,	-- ロケーションABCID
		MAABC.ABC_CD										,	-- ロケーションABC
		MLC.ABCSUB_ID										,	-- 重量グループID
		MASUB.ABCSUB_TX											-- 重量グループ
	FROM
		(
		SELECT
	 		TLS.LOCA_CD 							,
	 		TLS.SKU_CD 								,
	 		TLS.KANBAN_CD 							,
	 		TLS.LOT_TX 								,
	 		SUM(TLS.TOTALPALLET_NR) AS TOTALPALLET_NR,
	 		SUM(TLS.TOTALCASE_NR)   AS TOTALCASE_NR	,
	 		SUM(TLS.TOTALBARA_NR)   AS TOTALBARA_NR	,
	 		SUM(TLS.TOTALPCS_NR)    AS TOTALPCS_NR	,
	 		SUM(TLS.PPALLET_NR)     AS PPALLET_NR	,
	 		SUM(TLS.PCASE_NR)       AS PCASE_NR		,
	 		SUM(TLS.PBARA_NR)       AS PBARA_NR		,
	 		SUM(TLS.PPCS_NR)        AS PPCS_NR		,
	 		SUM(TLS.CPALLET_NR)     AS CPALLET_NR	,
	 		SUM(TLS.CCASE_NR)       AS CCASE_NR		,
	 		SUM(TLS.CBARA_NR)       AS CBARA_NR		,
	 		SUM(TLS.CPCS_NR)        AS CPCS_NR		,
	 		TLS.TYPESTOCK_CD 						,
	 		MTS.TYPESTOCK_TX 						,
	 		TLS.STSTOCK_ID 							,
	 		MSS.STSTOCK_TX 							,
	 		TLS.SKU_TX 								,
	 		TLS.SKUBARCODE_TX 						,
	 		TLS.ITFCODE_TX 							,
	 		TLS.AMOUNT_NR 							,
	 		TLS.CASEPERPALLET_NR 					,
	 		TLS.PALLETLAYER_NR 						,
	 		TLS.EXTERIOR_CD 						,
	 		TLS.SIZE_TX
		FROM
			(
			SELECT
				TLP.LOCA_CD 					,
				TLP.SKU_CD 						,
				TLP.KANBAN_CD 					,
				TLP.LOT_TX 						,
				TLP.PALLET_NR AS TOTALPALLET_NR	,
				TLP.CASE_NR   AS TOTALCASE_NR	,
				TLP.BARA_NR   AS TOTALBARA_NR	,
				TLP.PCS_NR    AS TOTALPCS_NR	,
				TLP.PALLET_NR AS PPALLET_NR		,
				TLP.CASE_NR   AS PCASE_NR		,
				TLP.BARA_NR   AS PBARA_NR		,
				TLP.PCS_NR    AS PPCS_NR		,
				0             AS CPALLET_NR		,
				0             AS CCASE_NR		,
				0             AS CBARA_NR		,
				0             AS CPCS_NR		,
				TLP.TYPESTOCK_CD 				,
				0 AS STSTOCK_ID					,
				MSK.SKU_TX 						,
				MSK.SKUBARCODE_TX 				,
				MSK.ITFCODE_TX 					,
				MSK.PCSPERCASE_NR AS AMOUNT_NR	,
				MSK.EXTERIOR_CD 				,
				MSK.SIZE_TX 					,
				TLP.CASEPERPALLET_NR			,
				TLP.PALLETLAYER_NR
			FROM
				TA_LOCASTOCKP TLP,
				MA_SKU MSK
			WHERE
				TLP.SKU_CD = MSK.SKU_CD
		UNION ALL
		SELECT
			TLC.LOCA_CD							,
			TLC.SKU_CD							,
			TLC.KANBAN_CD 						,
			TLC.LOT_TX 							,
			SUM(TLC.PALLET_NR) AS TOTALPALLET_NR,
			SUM(TLC.CASE_NR)   AS TOTALCASE_NR	,
			SUM(TLC.BARA_NR)   AS TOTALBARA_NR	,
			SUM(TLC.PCS_NR)    AS TOTALPCS_NR	,
			0                  AS PPALLET_NR	,
			0                  AS PCASE_NR		,
			0                  AS PBARA_NR		,
			0                  AS PPCS_NR		,
			SUM(TLC.PALLET_NR) AS CPALLET_NR	,
			SUM(TLC.CASE_NR)   AS CCASE_NR		,
			SUM(TLC.BARA_NR)   AS CBARA_NR		,
			SUM(TLC.PCS_NR)    AS CPCS_NR		,
			TLC.TYPESTOCK_CD 					,
			TLC.STSTOCK_ID 						,
			MSK.SKU_TX 							,
			MSK.SKUBARCODE_TX 					,
			MSK.ITFCODE_TX 						,
			MSK.PCSPERCASE_NR AS AMOUNT_NR 		,
			MSK.EXTERIOR_CD 					,
			MSK.SIZE_TX 						,
			TLC.CASEPERPALLET_NR 				,
			TLC.PALLETLAYER_NR
		FROM TA_LOCASTOCKC TLC,
			MA_SKU MSK
		WHERE
			TLC.SKU_CD		= MSK.SKU_CD					AND
			TLC.STSTOCK_ID	<= PS_DEFINE.ST_STOCK19_PICKE
		GROUP BY
			TLC.LOCA_CD 			,
			TLC.SKU_CD 				,
			TLC.KANBAN_CD 			,
			TLC.LOT_TX 				,
			TLC.TYPESTOCK_CD 		,
			TLC.STSTOCK_ID 			,
			MSK.SKU_TX 				,
			MSK.SKUBARCODE_TX 		,
			MSK.ITFCODE_TX 			,
			MSK.PCSPERCASE_NR 		,
			MSK.EXTERIOR_CD 		,
			MSK.SIZE_TX 			,
			TLC.CASEPERPALLET_NR	,
			TLC.PALLETLAYER_NR 		,
			MSK.EXTERIOR_CD 		,
			MSK.SIZE_TX
		) TLS				,
		MB_TYPESTOCK MTS	,
		MB_STSTOCK MSS
	WHERE
		TLS.TYPESTOCK_CD	= MTS.TYPESTOCK_CD(+)	AND
		TLS.STSTOCK_ID		= MSS.STSTOCK_ID(+)
	GROUP BY
		TLS.LOCA_CD 		,
		TLS.SKU_CD 			,
		TLS.KANBAN_CD 		,
		TLS.LOT_TX 			,
		TLS.TYPESTOCK_CD 	,
		MTS.TYPESTOCK_TX 	,
		TLS.STSTOCK_ID 		,
		MSS.STSTOCK_TX 		,
		TLS.SKU_TX 			,
		TLS.SKUBARCODE_TX 	,
		TLS.ITFCODE_TX 		,
		TLS.AMOUNT_NR 		,
		TLS.CASEPERPALLET_NR,
		TLS.PALLETLAYER_NR 	,
		TLS.EXTERIOR_CD 	,
		TLS.SIZE_TX
	    ) LS2
	FULL OUTER JOIN MA_LOCA MLC
		ON LS2.LOCA_CD = MLC.LOCA_CD
	LEFT OUTER JOIN MB_TYPELOCA MTL
		ON MLC.TYPELOCA_CD = MTL.TYPELOCA_CD
	LEFT OUTER JOIN MB_TYPELOCABLOCK MTB
		ON MLC.TYPELOCABLOCK_CD = MTB.TYPELOCABLOCK_CD
	LEFT OUTER JOIN MB_SHIPVOLUME MBSH
		ON MLC.SHIPVOLUME_CD = MBSH.SHIPVOLUME_CD
	LEFT OUTER JOIN
		(SELECT
				LOCA_CD
			FROM
				TA_LOCASTOCKC 
			WHERE
				STSTOCK_ID > PS_DEFINE.ST_STOCK19_PICKE --移動先ロケを探す
			GROUP BY
				LOCA_CD
		) TMP	
		ON MLC.LOCA_CD = TMP.LOCA_CD
	LEFT OUTER JOIN MA_ABC MAABC
		ON MLC.ABC_ID = MAABC.ABC_ID
	LEFT OUTER JOIN MA_ABCSUB MASUB
		ON MLC.ABCSUB_ID = MASUB.ABCSUB_ID
	ORDER BY
		MLC.LOCA_CD
;
