-- Copyright (C) Seaos Corporation 2013 All rights reserved
--*******************************************
-- かんばんマスタ
--*******************************************
DROP VIEW VW_WA361;
CREATE VIEW VW_WA361
(
	KANBAN_CD				,	/* かんばんコード */
	KANBAN_TX				,	/* かんばん名 */
	TYPEKANBAN_CD			,	/* かんばんタイプ */
	TYPEKANBAN_TX			,	/* かんばんタイプ名 */
	COLOR_CD				,	/* 色 */
	COLOR_TX				,	/* 色 */
	HTMLCOLOR_TX			,	/* カラーコード */
	AREA_TX					,	/* エリア */
	LINEUNIT_CD				,	/* ラインユニット(コード) */
	LINEUNIT_NR				,	/* ラインユニット数 */
	ADD_DT					,	/* 登録日時 */
	ADDUSER_CD				,	/* 登録ユーザコード */
	ADDUSER_TX				,	/* 登録ユーザ名 */
	UPD_DT					,	/* 更新日時 */
	UPDUSER_CD				,	/* 更新ユーザコード */
	UPDUSER_TX				,	/* 更新ユーザ名 */
	UPDPROGRAM_CD			,	/* 更新プログラムコード */
	UPDCOUNTER_NR			,	/* 更新カウンター */
	STRECORD_ID					/* レコード状態 */
) AS
SELECT
	MKA.KANBAN_CD			,	/* かんばんコード */
	MKA.KANBAN_TX			,	/* かんばん名 */
	MKA.TYPEKANBAN_CD		,	/* かんばんタイプ */
	DECODE(MKA.TYPEKANBAN_CD, '01', '入荷', '02', '出荷')
		AS TYPEKANBAN_TX	,	/* かんばんタイプ名 */
	MBA.COLOR_CD			,	/* 色 */
	MBA.COLOR_TX			,	/* 色 */
	MBA.HTMLCOLOR_TX		,	/* カラーコード */
	MKA.AREA_TX				,	/* エリア */
	MKA.LINEUNIT_CD			,	/* ラインユニット(コード) */
	MKA.LINEUNIT_NR			,	/* ラインユニット数 */
	MKA.ADD_DT				,	/* 登録日時 */
	MKA.ADDUSER_CD			,	/* 登録ユーザコード */
	MKA.ADDUSER_TX			,	/* 登録ユーザ名 */
	MKA.UPD_DT				,	/* 更新日時 */
	MKA.UPDUSER_CD			,	/* 更新ユーザコード */
	MKA.UPDUSER_TX			,	/* 更新ユーザ名 */
	MKA.UPDPROGRAM_CD		,	/* 更新プログラムコード */
	MKA.UPDCOUNTER_NR		,	/* 更新カウンター */
	MKA.STRECORD_ID				/* レコード状態 */
FROM
	MA_KANBAN MKA
		LEFT OUTER JOIN MB_AREA MBA
			ON MKA.AREA_TX = MBA.AREA_CD
;
