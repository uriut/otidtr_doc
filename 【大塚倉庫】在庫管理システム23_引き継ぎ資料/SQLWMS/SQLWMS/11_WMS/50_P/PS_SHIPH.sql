-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_SHIP AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_SHIP';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************************************/
	/*	出荷引当前チェック																*/
	/************************************************************************************/
	PROCEDURE ChkApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);
	/************************************************************************************/
	/*	出荷引当開始更新																*/
	/************************************************************************************/
	PROCEDURE StartApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);
	/************************************************************************************/
	/*	出荷引当終了更新																*/
	/************************************************************************************/
	PROCEDURE EndApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);
END PS_SHIP;
/
SHOW ERRORS
