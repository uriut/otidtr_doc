-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PI_STOCKRESULT AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PI_STOCKRESULT';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************/
	/* アップロード完了（在庫実績）                             */
	/************************************************************/
	PROCEDURE EndUploadStockResult(
		iTrnID						IN TMP_STOCKRESULT.TRN_ID%TYPE
	);
	
END PI_STOCKRESULT;
/
SHOW ERRORS
