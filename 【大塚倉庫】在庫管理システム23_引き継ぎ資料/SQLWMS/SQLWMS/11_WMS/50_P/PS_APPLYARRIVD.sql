-- Copyright (C) Seaos Corporation 2014 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_APPLYARRIV AS 
	/************************************************************************************/
	/*	入荷予定消込																	*/
	/************************************************************************************/
	PROCEDURE ApplyArriv(
		iTrnID				IN NUMBER			,
		irowSku				IN MA_SKU%ROWTYPE	,
		irowArriv			IN TA_ARRIV%ROWTYPE	,
		iPcsNR				IN NUMBER			, --計上数
		iUserCD				IN VARCHAR2			,
		iUserTX				IN VARCHAR2			,
		iProgramCD			IN VARCHAR2			
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyArriv';

		CURSOR curArrivAdd (                                         
			SlipNoTX	TA_ARRIV.SLIPNO_TX%TYPE, 
			SkuCD		TA_ARRIV.SKU_CD%TYPE,	
			LotTx		TA_ARRIV.LOT_TX%TYPE              
		) IS                                                      
			SELECT                                                
				TAR.*                                                 
			FROM                                                  
				TA_ARRIV TAR,
				MA_SKU	 MSK
			WHERE                                                 
				TAR.SKU_CD 	   = MSK.SKU_CD AND
				TAR.SLIPNO_TX  = SlipNoTX   AND
				TAR.SKU_CD	   = SkuCD	    AND
				TAR.LOT_TX	   = LotTx		AND
				TAR.STARRIV_ID IN ( 3, 4 )	AND
				TAR.ARRIVPLANPCS_NR / MSK.PCSPERCASE_NR / TAR.PALLETCAPACITY_NR < 1
			ORDER BY
				TAR.ARRIVPLANPCS_NR;

		CURSOR curArrivSub (                                         
			SlipNoTX	TA_ARRIV.SLIPNO_TX%TYPE, 
			SkuCD		TA_ARRIV.SKU_CD%TYPE
			--LotTx		TA_ARRIV.LOT_TX%TYPE              
		) IS                                                      
			SELECT                                                
				TAR.*                                                 
			FROM                                                  
				TA_ARRIV TAR,
				MA_SKU	 MSK
			WHERE                                                 
				TAR.SKU_CD 	   = MSK.SKU_CD AND
				TAR.SLIPNO_TX  = SlipNoTX   AND
				TAR.SKU_CD	   = SkuCD	    AND
				TAR.STARRIV_ID IN ( 3, 4 )	AND
				TAR.ARRIVPLANPCS_NR / MSK.PCSPERCASE_NR / TAR.PALLETCAPACITY_NR < 1
			ORDER BY
				TAR.ARRIVPLANPCS_NR;

		rowArriv			TA_ARRIV%ROWTYPE;
		rowArrivAdd			TA_ARRIV%ROWTYPE;
		rowArrivSub			TA_ARRIV%ROWTYPE;
		numApplyPcsNR		NUMBER(5,0) := 0;
		numPalletNR			NUMBER(5,0) := 0;
		numCaseNR			NUMBER(5,0) := 0;
		numBaraNR			NUMBER(9,0) := 0;
		numModNR			NUMBER(9,0) := 0; --余り分
		numAddNR			NUMBER(9,0) := 0; --合算分
		numSubNR			NUMBER(9,0) := 0; --減算分
		numRowNR			NUMBER(9,0) := 0; --入荷予定行数

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		numModNR := irowArriv.ARRIVPLANPCS_NR - iPcsNR;

		IF numModNR > 0 THEN

			IF iPcsNR / irowSku.PCSPERCASE_NR / irowArriv.PALLETCAPACITY_NR < 1 THEN	

				IF irowArriv.GOODPCS_NR > 0 THEN
					--計上されていないはず
					RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：入荷消込エラー１');
				END IF;

				--既存分
				PS_STOCK.GetUnitPcsNr(
					irowArriv.SKU_CD			,
					iPcsNR						,
					irowArriv.PALLETCAPACITY_NR	,
					numPalletNR					,
					numCaseNR					,
					numBaraNR
				);

				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:aaa ' ||
					irowArriv.SKU_CD || ' ' || iPcsNR || ' ' || irowArriv.PALLETCAPACITY_NR || ' ' || numPalletNR || ' ' || numCaseNR || ' ' || numBaraNR
				);

				UPDATE
					TA_ARRIV
				SET
					ARRIVPLANPALLET_NR = numPalletNR,
					ARRIVPLANCASE_NR   = numCaseNR  ,
					ARRIVPLANBARA_NR   = numBaraNR  ,
					ARRIVPLANPCS_NR	   = iPcsNR		,
					STARRIV_ID		   = PS_DEFINE.ST_ARRIV4_INSTOCKCHK_E
				WHERE
					ARRIV_ID = irowArriv.ARRIV_ID;

				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:bbb ' || numModNR);

				OPEN curArrivAdd(irowArriv.SLIPNO_TX, irowArriv.SKU_CD, irowArriv.LOT_TX);          
				LOOP                              
					FETCH curArrivAdd INTO rowArrivAdd; 
					EXIT WHEN curArrivAdd%NOTFOUND;  
					numRowNR := numRowNR + 1;										  

					IF numRowNR > 1 THEN
						--１パレット未満の行数は１行以下のはず
						RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：入荷消込エラー２');
					END IF;

					IF rowArrivAdd.GOODPCS_NR > 0 THEN
						--計上されていないはず
						RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：入荷消込エラー３');
					END IF;

					IF TRUNC( ( rowArrivAdd.ARRIVPLANPCS_NR + numModNR ) / irowSku.PCSPERCASE_NR / rowArrivAdd.PALLETCAPACITY_NR ) >= 1 THEN
						numAddNR := irowSku.PCSPERCASE_NR * rowArrivAdd.PALLETCAPACITY_NR; --完パレ
						numModNR := MOD( ( rowArrivAdd.ARRIVPLANPCS_NR + numModNR ) / irowSku.PCSPERCASE_NR, rowArrivAdd.PALLETCAPACITY_NR ) * irowSku.PCSPERCASE_NR;
					ELSE
						numAddNR := rowArrivAdd.ARRIVPLANPCS_NR + numModNR;
						numModNR := 0;
					END IF;

					--合算分
					PS_STOCK.GetUnitPcsNr(
						rowArrivAdd.SKU_CD			 ,
						numAddNR					 ,
						rowArrivAdd.PALLETCAPACITY_NR,
						numPalletNR					 ,
						numCaseNR					 ,
						numBaraNR
					);

					PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:ccc ' ||
						rowArrivAdd.SKU_CD || ' ' || numAddNR || ' ' || rowArrivAdd.PALLETCAPACITY_NR || ' ' || numPalletNR || ' ' || numCaseNR || ' ' || numBaraNR
					);

					UPDATE
						TA_ARRIV
					SET
						ARRIVPLANPALLET_NR = numPalletNR,
						ARRIVPLANCASE_NR   = numCaseNR  ,
						ARRIVPLANBARA_NR   = numBaraNR  ,
						ARRIVPLANPCS_NR	   = numAddNR
					WHERE
						ARRIV_ID = rowArrivAdd.ARRIV_ID;

				END LOOP;                         
				CLOSE curArrivAdd;                   

				IF numModNR > 0 THEN

					--新規登録分
					PS_STOCK.GetUnitPcsNr(
						irowArriv.SKU_CD			,
						numModNR					,
						irowArriv.PALLETCAPACITY_NR	,
						numPalletNR					,
						numCaseNR					,
						numBaraNR
					);

					PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:ddd ' ||
						irowArriv.SKU_CD || ' ' || numModNR || ' ' || irowArriv.PALLETCAPACITY_NR || ' ' || numPalletNR || ' ' || numCaseNR || ' ' || numBaraNR
					);

					rowArriv := irowArriv;
					rowArriv.ARRIVPLANPALLET_NR := numPalletNR;
					rowArriv.ARRIVPLANCASE_NR	:= numCaseNR;  
					rowArriv.ARRIVPLANBARA_NR	:= numBaraNR; 
					rowArriv.ARRIVPLANPCS_NR	:= numModNR;
					rowArriv.CHECKHT_ID			:= 0;
					rowArriv.STARRIV_ID			:= 3;

					DevArriv(
						iTrnID	  ,
						rowArriv  ,
						iUserCD	  ,
						iUserTX	  ,
						iProgramCD			
					);

				END IF;

			END IF;

		ELSIF numModNR < 0 THEN

			IF irowArriv.GOODPCS_NR > 0 THEN
				--計上されていないはず
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：入荷消込エラー１１');
			END IF;

			--既存分
			PS_STOCK.GetUnitPcsNr(
				irowArriv.SKU_CD			,
				iPcsNR						,
				irowArriv.PALLETCAPACITY_NR	,
				numPalletNR					,
				numCaseNR					,
				numBaraNR
			);

			PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:eee ' ||
				irowArriv.SKU_CD || ' ' || iPcsNR || ' ' || irowArriv.PALLETCAPACITY_NR || ' ' || numPalletNR || ' ' || numCaseNR || ' ' || numBaraNR
			);

			UPDATE
				TA_ARRIV
			SET
				ARRIVPLANPALLET_NR = numPalletNR,
				ARRIVPLANCASE_NR   = numCaseNR  ,
				ARRIVPLANBARA_NR   = numBaraNR  ,
				ARRIVPLANPCS_NR	   = iPcsNR		,
				STARRIV_ID		   = PS_DEFINE.ST_ARRIV4_INSTOCKCHK_E
			WHERE
				ARRIV_ID = irowArriv.ARRIV_ID;

			numApplyPcsNR := ABS(numModNR);

			OPEN curArrivSub(irowArriv.SLIPNO_TX, irowArriv.SKU_CD); --ロット指定なし
			LOOP                              
				FETCH curArrivSub INTO rowArrivSub; 
				EXIT WHEN curArrivSub%NOTFOUND;  
				EXIT WHEN numApplyPcsNR	<= 0;

				IF rowArrivSub.GOODPCS_NR > 0 THEN
					--計上されていないはず
					RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：入荷消込エラー１２');
				END IF;

				IF rowArrivSub.ARRIVPLANPCS_NR >= numApplyPcsNR THEN
					numSubNR := rowArrivSub.ARRIVPLANPCS_NR - numApplyPcsNR;
					numApplyPcsNR := 0;
				ELSE
					numSubNR := 0;
					numApplyPcsNR := numApplyPcsNR - rowArrivSub.ARRIVPLANPCS_NR;
				END IF;

				--減算分
				PS_STOCK.GetUnitPcsNr(
					rowArrivSub.SKU_CD			 ,
					numSubNR					 ,
					rowArrivSub.PALLETCAPACITY_NR,
					numPalletNR					 ,
					numCaseNR					 ,
					numBaraNR
				);

				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:fff ' ||
					rowArrivSub.SKU_CD || ' ' || numSubNR || ' ' || rowArrivSub.PALLETCAPACITY_NR || ' ' || numPalletNR || ' ' || numCaseNR || ' ' || numBaraNR
				);

				IF numSubNR > 0 THEN
					UPDATE
						TA_ARRIV
					SET
						ARRIVPLANPALLET_NR = numPalletNR,
						ARRIVPLANCASE_NR   = numCaseNR  ,
						ARRIVPLANBARA_NR   = numBaraNR  ,
						ARRIVPLANPCS_NR	   = numSubNR
					WHERE
						ARRIV_ID = rowArrivSub.ARRIV_ID;
				ELSE
					--入荷数０の場合は削除する
					DELETE
						TA_ARRIV
					WHERE
						ARRIV_ID = rowArrivSub.ARRIV_ID;
				END IF;

			END LOOP;                         
			CLOSE curArrivSub;                   

			IF numApplyPcsNR > 0 THEN
				--入荷予定総数チェックがあるため発生しないはずだが・・
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：入荷予定が消し込めません。' ||
																 '伝票番号 = [' || irowArriv.SLIPNO_TX || '], ' || 
																 'SKUコード = [' || irowArriv.SKU_CD || '], ' || 
																 '消込予定数 = [' || ABS(numModNR) || '], ' || 
																 '消込残数 = [' || numApplyPcsNR || ']');
			END IF;
		END IF;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyArriv;
	/************************************************************************************/
	/*	入荷予定分割																	*/
	/************************************************************************************/
	PROCEDURE DevArriv(
		iTrnID				IN NUMBER			,
		irowArriv			IN TA_ARRIV%ROWTYPE	,
		iUserCD				IN VARCHAR2			,
		iUserTX				IN VARCHAR2			,
		iProgramCD			IN VARCHAR2			
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DevArriv';
		numArrivID		NUMBER(10,0);
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		SELECT
			SEQ_ARRIVID.NEXTVAL INTO numArrivID
		FROM
			DUAL;

		--登録
		INSERT INTO TA_ARRIV (
			ARRIV_ID					,    
			TRN_ID						,    
			ARRIVORDERNO_CD				,    
			ARRIVORDERDETNO_CD			,    
			TYPEARRIV_CD				,    
			STARRIV_ID					,    
			SUPPLIER_CD					,    
			SUPPLIER_TX					,    
			OFFICE_CD					,    
			OFFICE_TX					,    
			MANUFACTURER_CD				,    
			TRANSPORTER_CD				,    
			TRANSPORTER_TX				,    
			CARNUMBER_CD				,    
			SLIPNO_TX					,    
			SLIPDATE_DT					,    
			OWNER_CD					,    
			SKU_CD						,    
			LOT_TX						,    
			CASEPERPALLET_NR			,    
			PALLETLAYER_NR				,    
			CASEPERLAYER_NR				,    
			PALLETCAPACITY_NR			,    
			STACKINGNUMBER_NR			,    
			TYPEPLALLET_CD				,    
			ARRIVPLAN_DT				,    
			ARRIVPLAN2_DT				,    
			ARRIVPLANPALLET_NR			,    
			ARRIVPLANCASE_NR			,    
			ARRIVPLANBARA_NR			,    
			ARRIVPLANPCS_NR				,    
			DRIVERTEL_TX				,    
			DRIVER_TX					,    
			BERTH_TX					,    
			SAMEDAYSHIP_FL				,    
			MEMO_TX						,    
			CHECK_DT					,    
			ARRIVEND_DT					,    
			ARRIVENDUSER_CD				,    
			ARRIVENDUSER_TX				,    
			CHECK_TX					,    
			CHECKUSER_CD				,    
			CHECKUSER_TX				,    
			CHECKHT_ID					,    
			KANBAN_CD					,    
			CHECKHTPCS_NR				,    
			ARRIVRSLT_DT				,    
			ARRIVRSLTUSER_CD			,    
			ARRIVRSLTUSER_TX			,	 
			ARRIVRSLTHT_ID				,    
			GOODPCS_NR					,    
			NGPCS_NR					,    
			SAMPLEPCS_NR				,    
			TYPEENTRY_CD				,    
			UPD_DT						,    
			UPDUSER_CD					,    
			UPDUSER_TX					,    
			ADD_DT						,    
			ADDUSER_CD					,    
			ADDUSER_TX					,    
			UPDPROGRAM_CD				,    
			UPDCOUNTER_NR				,    
			STRECORD_ID					     
		) VALUES (
			numArrivID					,
			iTrnID						,
			irowArriv.ARRIVORDERNO_CD	,
			irowArriv.ARRIVORDERDETNO_CD,
			irowArriv.TYPEARRIV_CD		,
			irowArriv.STARRIV_ID		,
			irowArriv.SUPPLIER_CD		,
			irowArriv.SUPPLIER_TX		,
			irowArriv.OFFICE_CD			,
			irowArriv.OFFICE_TX			,
			irowArriv.MANUFACTURER_CD	,
			irowArriv.TRANSPORTER_CD	,
			irowArriv.TRANSPORTER_TX	,
			irowArriv.CARNUMBER_CD		,
			irowArriv.SLIPNO_TX			,
			irowArriv.SLIPDATE_DT		,
			irowArriv.OWNER_CD			,
			irowArriv.SKU_CD			,
			irowArriv.LOT_TX			,
			irowArriv.CASEPERPALLET_NR	,
			irowArriv.PALLETLAYER_NR	,
			irowArriv.CASEPERLAYER_NR	,
			irowArriv.PALLETCAPACITY_NR	,
			irowArriv.STACKINGNUMBER_NR	,
			irowArriv.TYPEPLALLET_CD	,
			irowArriv.ARRIVPLAN_DT		,
			irowArriv.ARRIVPLAN2_DT		,
			irowArriv.ARRIVPLANPALLET_NR,
			irowArriv.ARRIVPLANCASE_NR	,
			irowArriv.ARRIVPLANBARA_NR	,
			irowArriv.ARRIVPLANPCS_NR	,
			irowArriv.DRIVERTEL_TX		,
			irowArriv.DRIVER_TX			,
			irowArriv.BERTH_TX			,
			irowArriv.SAMEDAYSHIP_FL	,
			irowArriv.MEMO_TX			,
			irowArriv.CHECK_DT			,
			irowArriv.ARRIVEND_DT		,
			irowArriv.ARRIVENDUSER_CD	,
			irowArriv.ARRIVENDUSER_TX	,
			irowArriv.CHECK_TX			,
			irowArriv.CHECKUSER_CD		,
			irowArriv.CHECKUSER_TX		,
			irowArriv.CHECKHT_ID		,
			irowArriv.KANBAN_CD			,
			irowArriv.CHECKHTPCS_NR		,
			irowArriv.ARRIVRSLT_DT		,
			irowArriv.ARRIVRSLTUSER_CD	,
			irowArriv.ARRIVRSLTUSER_TX	,
			irowArriv.ARRIVRSLTHT_ID	,
			irowArriv.GOODPCS_NR		,
			irowArriv.NGPCS_NR			,
			irowArriv.SAMPLEPCS_NR		,
			irowArriv.TYPEENTRY_CD		,
			irowArriv.UPD_DT			,
			irowArriv.UPDUSER_CD		,
			irowArriv.UPDUSER_TX		,
			irowArriv.ADD_DT			,
			irowArriv.ADDUSER_CD		,
			irowArriv.ADDUSER_TX		,
			irowArriv.UPDPROGRAM_CD		,
			irowArriv.UPDCOUNTER_NR		,
			irowArriv.STRECORD_ID		 
		);

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END DevArriv;

END PS_APPLYARRIV;
/
SHOW ERRORS
