-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_APPLYPICK AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_APPLYPICK';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************************************/
	/*	ピッキング引当																	*/
	/************************************************************************************/
	PROCEDURE ApplyPickMain(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyPickDevPallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyPickOnePallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	); 

	PROCEDURE ApplyPickSumPallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyPickCaseLoca(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyPickVolume(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyPickCase(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyPickVolumeSearchTotal(
		iTrnID				IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTransporterCD		IN	VARCHAR2,
		iCapacityNR			IN	NUMBER	,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyPickVolumeSearchOrder(
		iTrnID				IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTransporterCD		IN	VARCHAR2,
		iDtNameTX			IN	VARCHAR2,    
		iDtAddress1TX		IN	VARCHAR2,
		iDtAddress2TX		IN	VARCHAR2,
		iDtAddress3TX		IN	VARCHAR2,
		iDtAddress4TX		IN	VARCHAR2,
		iDirectDeliveryCD 	IN	VARCHAR2, --直送先コード
		iDeliDateTX			IN	VARCHAR2, --着荷指定日
		iCapacityNR			IN	NUMBER	,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyPickMixSku(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyPickMixInit(
		iTrnID				IN	NUMBER
	);

	PROCEDURE ApplyPickMixSearchTotal(
		iTrnID				IN	NUMBER	,
		iPalletDetNoCD		IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iTransporterCD		IN	VARCHAR2,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iOrderID			IN	NUMBER	, --重量グループ別に付番
		iCapacityNR			IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyPickMixSearchOrder(
		iTrnID				IN	NUMBER	,
		iPalletDetNoCD		IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iTransporterCD		IN	VARCHAR2,
		iDtNameTX			IN	VARCHAR2,    
		iDtAddress1TX		IN	VARCHAR2,
		iDtAddress2TX		IN	VARCHAR2,
		iDtAddress3TX		IN	VARCHAR2,
		iDtAddress4TX		IN	VARCHAR2,
		iDirectDeliveryCD 	IN	VARCHAR2, --直送先コード
		iDeliDateTX			IN	VARCHAR2, --着荷指定日
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iOrderID			IN	NUMBER	, --重量グループ別に付番
		iCapacityNR			IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyPickSkuAdd(
		iTrnID				IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iCaseNR				IN	NUMBER
	);

	PROCEDURE ApplyPickSkuCnt(
		iTrnID				IN	NUMBER,
		iSkuCD				IN	VARCHAR2,
		oSkuCnt				OUT	NUMBER
	);

	PROCEDURE ConvCaseToPallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE SetPickNo(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

END PS_APPLYPICK;
/
SHOW ERRORS
