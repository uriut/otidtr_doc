create or replace PACKAGE BODY PI_SHIP AS
	/************************************************************************************/
	/*																					*/
	/*									Public											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************************************/
	/*	出荷指示登録																	*/
	/************************************************************************************/
	PROCEDURE EndUploadShip (
		iTrnID				IN	NUMBER
	) IS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'EndUploadShip';
		--ヘッダ一覧
		CURSOR curHead (
			TrnID	VI_SHIPH.TRN_ID%TYPE
		) IS
			SELECT
				ORDERNO_CD
			FROM
				VI_SHIPH
			WHERE
				TRN_ID		= TrnID	AND
				DKBN		= 'SH'	AND	-- 出荷指示
				END_FL		= 0
			GROUP BY
				ORDERNO_CD
			ORDER BY
				ORDERNO_CD;
		rowHead		curHead%ROWTYPE;
		-- ヘッダカーソル
		CURSOR curTmpShipH (
			TrnID			VI_SHIPH.TRN_ID%TYPE	,
			OrderNoCD		VI_SHIPH.ORDERNO_CD%TYPE
		) IS
			SELECT
				ORDERNO_CD
			FROM
				VI_SHIPH
			WHERE
				TRN_ID		= TrnID	AND
				DKBN		= 'SH'	AND	-- 出荷指示
				END_FL		= 0		AND
				ORDERNO_CD	= OrderNoCD
			ORDER BY
				ORDERNO_CD;
		rowTmpShipH		curTmpShipH%ROWTYPE;
		-- 明細カーソル
		CURSOR curTmpShipD (
			TrnID			VI_SHIPD.TRN_ID%TYPE		,
			OrderNoCD		VI_SHIPD.ORDERNO_CD%TYPE
		) IS
			SELECT
				BD.TRN_ID					,
				BD.ORDERNO_CD				,
				BD.SKID AS TYPESHIP_CD		,
				BD.SKID_NM AS TYPESHIP_TX	,
				BD.GYO AS ORDERDETNO_CD		,
				BD.SKU_CD					,
				BD.FCKBN AS MANUFACTURER_CD	,
				BD.LOT AS LOT_TX			,
				BD.SEINAN AS SKU_TX			,
				BD.KAN1 AS AMOUNT_NR		,
				CASE
					WHEN NVL(MSP.PALLETCAPACITY_NR,0) <> 0 THEN
						(BD.CASE_NR - MOD(BD.CASE_NR,NVL(MSP.PALLETCAPACITY_NR,0))) / NVL(MSP.PALLETCAPACITY_NR,0)
					ELSE 0
				END AS PALLET_NR			,
				CASE
					WHEN BD.KHKBN = 0 THEN
						CASE
							WHEN NVL(MSP.PALLETCAPACITY_NR,0) <> 0 THEN
								MOD(BD.CASE_NR,NVL(MSP.PALLETCAPACITY_NR,0))
							ELSE BD.CASE_NR
						END
					WHEN BD.KHKBN = 1 THEN
						CASE
							WHEN NVL(MSP.PALLETCAPACITY_NR,0) <> 0 THEN
								MOD(BD.CASE_NR,NVL(MSP.PALLETCAPACITY_NR,0))
							ELSE BD.CASE_NR
						END
					ELSE BD.CASE_NR
				END AS CASE_NR				,
				BD.BARA_NR AS BARA_NR		,
				BD.PCS_NR AS ORDERPCS_NR
			FROM
				VI_SHIPD BD,
				MA_SKUPALLET MSP
			WHERE
				BD.SKU_CD		= MSP.SKU_CD (+)			AND
				BD.FCKBN		= MSP.MANUFACTURER_CD (+)	AND
				BD.TRN_ID		= TrnID						AND
				BD.END_FL		= 0							AND
				BD.ORDERNO_CD	= OrderNoCD;
		rowTmpShipD		curTmpShipD%ROWTYPE;
		-- 取込済み
		CURSOR curShip (
			OrderNoCD		TA_SHIPH.ORDERNO_CD%TYPE
		) IS
			SELECT
				ORDERNO_CD
			FROM
				TA_SHIPH
			WHERE
				ORDERNO_CD	= OrderNoCD;
		rowShip			curShip%ROWTYPE;
		-- 未引当
		CURSOR curNotApply (
			TrnID			TA_SHIPH.TRN_ID%TYPE
		) IS
			SELECT
				SHIPH_ID
			FROM
				TA_SHIPH
			WHERE
				-- TRN_ID		= TrnID							AND
				(
				STSHIP_ID	= PS_DEFINE.ST_SHIP0_NOTAPPLY	OR
				STSHIP_ID	= PS_DEFINE.ST_SHIP2_SFAILITEM
				)
			ORDER BY
				SHIPH_ID;
		rowNotApply		curNotApply%ROWTYPE;
		numHeaderID		NUMBER(10,0) := 0;	-- ITFヘッダID
		numDetID		NUMBER(10,0) := 0;	-- ITF明細ID
		chrGiftTypeCd	VARCHAR2(30);
		numProxyShipFL	NUMBER(1,0) := 0;	--代行出荷なら1
		rowSkuPallet	MA_SKUPALLET%ROWTYPE;
		numPalletNR		NUMBER(5,0) := 0;	-- パレット数
		numCaseNR		NUMBER(5,0) := 0;	-- ケース数
		numBaraNR		NUMBER(9,0) := 0;	-- バラ数
		chrLocaCD		MA_LOCA.LOCA_CD%TYPE;
		chrKanbanCD		TA_LOCASTOCKP.KANBAN_CD%TYPE;
		numRowNR		NUMBER;
		numPcsNR		NUMBER;
		numTotalPcsNR	NUMBER;
		
	BEGIN
		-- 主処理
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 初期化
		-- TMP_HEAD1Hカーソルオープン
		OPEN curHead(iTrnID);
		LOOP
			-- 初期化
			numHeaderID := 0;
			-- ヘッダーキー取得
			FETCH curHead INTO rowHead;
			EXIT WHEN curHead%NOTFOUND;
			PLOG.DEBUG(logCtx, 
				'ORDERNO_CD=[' || rowHead.ORDERNO_CD || ']'
			);
			-- ヘッダー取得
			OPEN curTmpShipH(iTrnID, rowHead.ORDERNO_CD);
			FETCH curTmpShipH INTO rowTmpShipH;
			IF curTmpShipH%NOTFOUND THEN
					RAISE_APPLICATION_ERROR (PS_DEFINE.EXCEPTION_SYS,'予期せぬエラー：curTmpShipH%NOTFOUND');
			END IF;
			CLOSE curTmpShipH;
			-- 取込済み
			OPEN curShip(rowHead.ORDERNO_CD);
			FETCH curShip INTO rowShip;
			IF curShip%FOUND THEN
					RAISE_APPLICATION_ERROR (PS_DEFINE.EXCEPTION_SYS,'既に取込済みです');
			END IF;
			CLOSE curShip;
			-- SHIPH_ID取得
			SELECT
				SEQ_SHIPHID.NEXTVAL INTO numHeaderID
			FROM
				DUAL;
			-- 代行出荷であるか
			numProxyShipFL := 0;
			-- TA_SHIPHに登録
			INSERT INTO TA_SHIPH (
				SHIPH_ID				,
				TRN_ID					,
				TYPESHIP_CD				,
				TYPESHIP_TX				,
				STSHIP_ID				,
				-- 出荷指示
				ORDERNO_CD				,
				ORDERNOSUB_CD			,
				CUSTOMERORDERNO_CD		,
				CUSTOMER_CD				,
				ORDERTYPE_CD			,
				ENTRYTYPE_CD			,
				ORDERDATE_DT			,
				SALESSHOP_CD			,
				OWNER_CD				,
				CHANNELTYPE_CD			,
				TRANSPORTER1_CD			,
				TRANSPORTER2_CD			,
				DELIVERYDESTINATION_CD	,
				DTNAME_TX				,
				DTPOSTAL_CD				,
				DTADDRESS1_TX			,
				DTADDRESS2_TX			,
				DTADDRESS3_TX			,
				DTADDRESS4_TX			,
				DIRECTDELIVERY_CD		,
				TYPEPLALLET_CD			,
				SHIPBERTH_CD			,
				SHIPPLAN_DT				,
				SLIPNO_TX				,
				CARRIER_CD				,
				DELITYPE_CD				,
				DELIDATE_TX				,
				DELITIME_CD				,
				CHARGETYPE_CD			,
				GIFTTYPE_CD				,
				TYPETEMPERATURE_CD		,
				TOTALITEMPRICE_NR		,
				TOTALPRICE_NR			,
				TOTALTAX_NR				,
				DELICHARGE_NR			,
				GIFTCHARGE_NR			,
				CODCHARGE_NR			,
				DISCOUNT_NR				,
				SALEDISCOUNT_NR			,
				COUPON_CD				,
				COUPONDISCOUNT_NR		,
				USEPOINT_NR				,
				ADDPOINT_NR				,
				CUSTOMERCOMMENT_TX		,
				CSCOMMENT_TX			,
				OPERATORCOMMENT_TX		,
				DETCOUNT_NR				,
				-- 納品書
				INVOICE_FL				,
				INVOICECOMMENT_TX		,
				PRTINVOICECOUNT_NR		,
				-- 出荷伝票
				DELIBOX_FL				,
				PRTDELICOUNT_NR			,
				-- 管理項目
				UPD_DT					,
				UPDUSER_CD				,
				UPDUSER_TX				,
				ADD_DT					,
				ADDUSER_CD				,
				ADDUSER_TX				,
				UPDPROGRAM_CD			,
				UPDCOUNTER_NR			,
				STRECORD_ID				
			)
				SELECT
					numHeaderID				,
					HD.TRN_ID				,
					--'SHIP'					,	-- TYPESHIP_CD
					HD.SKID					,	-- TYPESHIP_CD
					HD.SKID_NM				,	-- TYPESHIP_TX
					0						,	-- STSHIP_ID
					-- 出荷指示
					HD.ORDERNO_CD			,	-- ORDERNO_CD
					'0'						,	-- ORDERNOSUB_CD
					' '						,	-- CUSTOMERORDERNO_CD
					' '						,	-- CUSTOMER_CD
					' '						,	-- ORDERTYPE_CD
					' '						,	-- ENTRYTYPE_CD
					NULL					,	-- ORDERDATE_DT
					' '						,	-- SALESSHOP_CD
					TRIM(HD.HNNINUSHI)		,	-- OWNER_CD
					' '						,	-- CHANNELTYPE_CD
					HD.SUCD1				,	-- TRANSPORTER1_CD
					HD.SUCD2				,	-- TRANSPORTER2_CD
					HD.NOHCD				,	-- DELIVERYDESTINATION_CD
					HD.NOHCD_NM1 || HD.NOHCD_NM2,	-- DTNAME_TX
					HD.YUBIN				,	-- DTPOSTAL_CD
					HD.NOHCD_JYUSYO1		,	-- DTADDRESS1_TX
					HD.NOHCD_JYUSYO2		,	-- DTADDRESS2_TX
					HD.NOHCD_JYUSYO3		,	-- DTADDRESS3_TX
					' '						,	-- DTADDRESS4_TX
					HD.CCD					,	-- DIRECTDELIVERY_CD
					HD.SNINUSHI				,	-- TYPEPLALLET_CD　20140513 SNINUSHIをセット
					' '						,	-- SHIPBERTH_CD
					NULL					,	-- SHIPPLAN_DT
					HD.DENNO				,	-- SLIPNO_TX
					' '						,	-- CARRIER_CD
					' '						,	-- DELITYPE_CD
					TO_CHAR(TO_DATE(HD.TYUNO),'yyyy/MM/dd'),	-- DELIDATE_TX
					'00'					,	-- DELITIME_CD
					' '						,	-- GIFTTYPE_CD
					' '						,	-- GIFTTYPE_CD
					' '						,	-- TYPETEMPERATURE_CD
					0						,	-- TOTALITEMPRICE_NR
					0						,	-- TOTALPRICE_NR
					0						,	-- TOTALTAX_NR
					0						,	-- DELICHARGE_NR
					0						,	-- GIFTCHARGE_NR
					0						,	-- CODCHARGE_NR
					0						,	-- DISCOUNT_NR
					0						,	-- SALEDISCOUNT_NR
					' '						,	-- COUPON_CD
					0						,	-- COUPONDISCOUNT_NR
					0						,	-- USEPOINT_NR
					0						,	-- ADDPOINT_NR
					' '						,	-- CUSTOMERCOMMENT_TX
					' '						,	-- CSCOMMENT_TX
					' '						,	-- OPERATORCOMMENT_TX
					HD.GYO					,	-- DETCOUNT_NR
					-- 納品書
					0 						,	-- INVOICE_FL
					' '						,	-- INVOICECOMMENT_TX
					0						,	-- PRTINVOICECOUNT_NR
					-- 出荷伝票
					0						,	-- DELIBOX_FL
					0						,	-- PRTDELICOUNT_NR
					--
					SYSDATE					,
					'BATCH'					,
					'BATCH'					,
					SYSDATE					,
					'BATCH'					,
					'BATCH'					,
					'BATCH'					,
					0						,
					0
				FROM
					VI_SHIPH HD
				WHERE
					HD.TRN_ID		= iTrnID			AND
					HD.ORDERNO_CD	= rowTmpShipH.ORDERNO_CD;
			-- 明細カーソルオープン
			OPEN curTmpShipD(
				iTrnID					,
				rowTmpShipH.ORDERNO_CD
			);
			LOOP
				-- 行データ取得
				FETCH curTmpShipD INTO rowTmpShipD;
				EXIT WHEN curTmpShipD%NOTFOUND;
				PLOG.DEBUG(logCtx, 
					'ORDERNO_CD=[' || rowTmpShipH.ORDERNO_CD || ']'
				);
				--初期化
				numDetID := 0;
				numPalletNR := 0;
				numCaseNR := 0;
				numBaraNR := 0;
				numTotalPcsNR := 0;
				-- マスタ取得
				PS_MASTER.GetSkuPallet(rowTmpShipD.SKU_CD, rowTmpShipD.MANUFACTURER_CD, rowSkuPallet);
				-- かんばん番号取得
				PS_STOCK.GetKanban(
					rowTmpShipD.SKU_CD	,
					rowTmpShipD.LOT_TX	,
					chrLocaCD			,
					chrKanbanCD
					);
					
				-- 1パレ単位に登録
				-- パレット
				numRowNR := rowTmpShipD.PALLET_NR;
				FOR i IN 1..numRowNR LOOP
					-- 初期化
					numPcsNR := 0;
					-- SHIPD_IDを取得
					SELECT
						SEQ_SHIPDID.NEXTVAL INTO numDetID
					FROM
						DUAL;
					-- PCS数
					numPcsNR := rowSkuPallet.PALLETCAPACITY_NR * rowTmpShipD.AMOUNT_NR;
					-- TA_SHIPDに登録
					INSERT INTO TA_SHIPD (
						SHIPD_ID			,
						SHIPH_ID			,
						TRN_ID				,
						TYPESHIP_CD			,
						TYPESHIP_TX			,
						STSHIP_ID			,
						-- 出荷指示
						ORDERNO_CD			,
						ORDERNOSUB_CD		,
						ORDERDETNO_CD		,
						SKU_CD				,
						LOT_TX				,
						KANBAN_CD			,
						CASEPERPALLET_NR	,
						PALLETLAYER_NR		,
						STACKINGNUMBER_NR	,
						SKU_TX				,
						TYPELOCA_CD			,
						STOCKTYPE_CD		,
						PALLETCAPACITY_NR	,
						ORDERPALLET_NR		,
						ORDERCASE_NR		,
						ORDERBARA_NR		,
						ORDERPCS_NR			,
						UNITPRICE_NR		,
						UNITADDPOINT_NR		,
						BONUS_FL			,
						UNITSALEDISCOUNT_NR	,
						DETCOMMENT_TX		,
						TYPESHIPAPPLY_CD	,
						TYPESHIPROUTE_CD	,
						SKUTYPE_CD			,
						SUPPLYTYPE_CD		,
						SUPPLIER_CD			,
						PRINTDETTYPE_CD		,
						PRINTPICKLISTTYPE_CD,
						--
						UPD_DT				,
						UPDUSER_CD			,
						UPDUSER_TX			,
						ADD_DT				,
						ADDUSER_CD			,
						ADDUSER_TX			,
						UPDPROGRAM_CD		,
						UPDCOUNTER_NR		,
						STRECORD_ID
					) VALUES (
						numDetID				,
						numHeaderID				,
						rowTmpShipD.TRN_ID		,	-- TRN_ID
						--'SHIP'					,	-- TYPESHIP_CD
						rowTmpShipD.TYPESHIP_CD	,	-- TYPESHIP_CD
						rowTmpShipD.TYPESHIP_TX	,	-- TYPESHIP_TX
						PS_DEFINE.ST_SHIP0_NOTAPPLY	,
						-- 出荷指示
						rowTmpShipD.ORDERNO_CD	,	-- ORDERNO_CD
						'0'						,	-- ORDERNOSUB_CD
						rowTmpShipD.ORDERDETNO_CD,	-- ORDERDETNO_CD
						rowTmpShipD.SKU_CD		,	-- SKU_CD
						rowTmpShipD.LOT_TX		,	-- LOT_TX
						chrKanbanCD				,	-- KANBAN_CD
						rowSkuPallet.CASEPERPALLET_NR	,	-- CASEPERPALLET_NR
						rowSkuPallet.PALLETLAYER_NR		,	-- PALLETLAYER_NR
						rowSkuPallet.STACKINGNUMBER_NR	,	-- STACKINGNUMBER_NR
						rowTmpShipD.SKU_TX		,	-- SKU_TX
						'10'					,	-- TYPELOCA_CD
						'10'					,	-- STOCKTYPE_CD
						rowSkuPallet.PALLETCAPACITY_NR	,	-- PALLETCAPACITY_NR
						1						,	-- ORDERPALLET_NR
						0						,	-- ORDERCASE_NR
						0						,	-- ORDERBARA_NR
						numPcsNR				,	-- ORDERPCS_NR
						0						,	-- UNITPRICE_NR
						0						,	-- UNITADDPOINT_NR
						0						,	-- BONUS_FL
						0						,	-- UNITSALEDISCOUNT_NR
						' '						,	-- DETCOMMENT_TX
						'APPLY'					,	-- TYPESHIPAPPLY_CD
						'WAREHOUSE'				,	-- TYPESHIPROUTE_CD
						' '						,	-- SKUTYPE_CD
						' '						,	-- SUPPLYTYPE_CD
						' '						,	-- SUPPLIER_CD
						'PRINT'					,	-- TS.PRINTDETTYPE_CD
						'PRINT'					,	-- TS.PRINTPICKLISTTYPE_CD
						--
						SYSDATE					,
						'BATCH'					,
						'BATCH'					,
						SYSDATE					,
						'BATCH'					,
						'BATCH'					,
						'BATCH'					,
						0						,
						0
					);
					numTotalPcsNR := numTotalPcsNR + numPcsNR;
				END LOOP;
				-- 端数
				IF numTotalPcsNR <> rowTmpShipD.ORDERPCS_NR THEN
					-- SHIPD_IDを取得
					SELECT
						SEQ_SHIPDID.NEXTVAL INTO numDetID
					FROM
						DUAL;
					-- TA_SHIPDに登録
					INSERT INTO TA_SHIPD (
						SHIPD_ID			,
						SHIPH_ID			,
						TRN_ID				,
						TYPESHIP_CD			,
						TYPESHIP_TX			,
						STSHIP_ID			,
						-- 出荷指示
						ORDERNO_CD			,
						ORDERNOSUB_CD		,
						ORDERDETNO_CD		,
						SKU_CD				,
						LOT_TX				,
						KANBAN_CD			,
						CASEPERPALLET_NR	,
						PALLETLAYER_NR		,
						STACKINGNUMBER_NR	,
						SKU_TX				,
						TYPELOCA_CD			,
						STOCKTYPE_CD		,
						PALLETCAPACITY_NR	,
						ORDERPALLET_NR		,
						ORDERCASE_NR		,
						ORDERBARA_NR		,
						ORDERPCS_NR			,
						UNITPRICE_NR		,
						UNITADDPOINT_NR		,
						BONUS_FL			,
						UNITSALEDISCOUNT_NR	,
						DETCOMMENT_TX		,
						TYPESHIPAPPLY_CD	,
						TYPESHIPROUTE_CD	,
						SKUTYPE_CD			,
						SUPPLYTYPE_CD		,
						SUPPLIER_CD			,
						PRINTDETTYPE_CD		,
						PRINTPICKLISTTYPE_CD,
						--
						UPD_DT				,
						UPDUSER_CD			,
						UPDUSER_TX			,
						ADD_DT				,
						ADDUSER_CD			,
						ADDUSER_TX			,
						UPDPROGRAM_CD		,
						UPDCOUNTER_NR		,
						STRECORD_ID
					) VALUES (
						numDetID				,
						numHeaderID				,
						rowTmpShipD.TRN_ID		,	-- TRN_ID
						--'SHIP'					,	-- TYPESHIP_CD
						rowTmpShipD.TYPESHIP_CD	,	-- TYPESHIP_CD
						rowTmpShipD.TYPESHIP_TX	,	-- TYPESHIP_TX
						PS_DEFINE.ST_SHIP0_NOTAPPLY	,
						-- 出荷指示
						rowTmpShipD.ORDERNO_CD	,	-- ORDERNO_CD
						'0'						,	-- ORDERNOSUB_CD
						rowTmpShipD.ORDERDETNO_CD,	-- ORDERDETNO_CD
						rowTmpShipD.SKU_CD		,	-- SKU_CD
						rowTmpShipD.LOT_TX		,	-- LOT_TX
						chrKanbanCD				,	-- KANBAN_CD
						rowSkuPallet.CASEPERPALLET_NR	,	-- CASEPERPALLET_NR
						rowSkuPallet.PALLETLAYER_NR		,	-- PALLETLAYER_NR
						rowSkuPallet.STACKINGNUMBER_NR	,	-- STACKINGNUMBER_NR
						rowTmpShipD.SKU_TX		,	-- SKU_TX
						'10'					,	-- TYPELOCA_CD
						'10'					,	-- STOCKTYPE_CD
						rowSkuPallet.PALLETCAPACITY_NR	,	-- PALLETCAPACITY_NR
						0						,	-- ORDERPALLET_NR
						rowTmpShipD.CASE_NR		,	-- ORDERCASE_NR
						rowTmpShipD.BARA_NR		,	-- ORDERBARA_NR
						rowTmpShipD.ORDERPCS_NR - numTotalPcsNR	,	-- ORDERPCS_NR
						0						,	-- UNITPRICE_NR
						0						,	-- UNITADDPOINT_NR
						0						,	-- BONUS_FL
						0						,	-- UNITSALEDISCOUNT_NR
						' '						,	-- DETCOMMENT_TX
						'APPLY'					,	-- TYPESHIPAPPLY_CD
						'WAREHOUSE'				,	-- TYPESHIPROUTE_CD
						' '						,	-- SKUTYPE_CD
						' '						,	-- SUPPLYTYPE_CD
						' '						,	-- SUPPLIER_CD
						'PRINT'					,	-- TS.PRINTDETTYPE_CD
						'PRINT'					,	-- TS.PRINTPICKLISTTYPE_CD
						--
						SYSDATE					,
						'BATCH'					,
						'BATCH'					,
						SYSDATE					,
						'BATCH'					,
						'BATCH'					,
						'BATCH'					,
						0						,
						0
					);
				END IF;
			END LOOP;
			-- TMP_HEAD1Dカーソルクローズ
			CLOSE curTmpShipD;
		END LOOP;
		CLOSE curHead;
		COMMIT;
		
		-- 引当
		--OPEN curNotApply(iTrnID);
		--LOOP
		--	FETCH curNotApply INTO rowNotApply;
		--	EXIT WHEN curNotApply%NOTFOUND;
		--	-- 選択ID登録
		--	PI_SHIP.InsSelectID(iTrnID, rowNotApply.SHIPH_ID);
		--END LOOP;
		--CLOSE curNotApply;
		---- 
		--PI_SHIP.ApplyShip(iTrnID,'BATCH','BATCH', 'AutoApplyShip');
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curHead%ISOPEN THEN CLOSE curHead; END IF;
			IF curTmpShipH%ISOPEN THEN CLOSE curTmpShipH; END IF;
			IF curTmpShipD%ISOPEN THEN CLOSE curTmpShipD; END IF;
			IF curShip%ISOPEN THEN CLOSE curShip; END IF;
			IF curNotApply%ISOPEN THEN CLOSE curNotApply; END IF;
			RAISE;
	END EndUploadShip;
	/************************************************************************************/
	/*	選択ID追加																		*/
	/************************************************************************************/
	PROCEDURE InsSelectID(
		iTrnID			IN NUMBER,
		iSelectID		IN NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsSelectID';
		numCount		NUMBER;
	BEGIN
		SELECT
			COUNT(*)
		INTO
			numCount
		FROM
			TMP_SELECTID
		WHERE
			TRN_ID		= iTrnID AND
			SELECT_ID	= iSelectID;
		IF numCount = 0 THEN
			INSERT INTO TMP_SELECTID (
				TRN_ID		,
				SELECT_ID	,
				ADD_DT
			) VALUES (
				iTrnID		,
				iSelectID	,
				SYSDATE
			);
		END IF;
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsSelectID;
	/************************************************************************************/
	/*	出荷引当																		*/
	/************************************************************************************/
	PROCEDURE ApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyShip';
		
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || '], ' || 'iUserCD = [' || iUserCD || ']'
		);
		-- 引当状態確認
		PS_STATUS.ChkApply;
		-- 引当状態更新
		PS_STATUS.SetShipApply;
		COMMIT;
		BEGIN
			-- 出荷ステータスチェック
			PS_SHIP.ChkApplyShip(iTrnID, iUserCD, iUserTX, iProgramCD);
			-- 出荷状態開始更新（出荷引当用一時テーブル作成）
			PS_SHIP.StartApplyShip(iTrnID, iUserCD, iUserTX, iProgramCD);
--			-- 引当
			PS_APPLYSHIP.ApplyShipMain(iTrnID, iUserCD, iUserTX, iProgramCD);
			-- ピッキング指示番号付与
			PS_APPLYPICK.ApplyPickMain(iTrnID, iUserCD, iUserTX, iProgramCD);
			-- 緊急補充引当
			PS_APPLYSUPP.ApplyESupp(iTrnID, iUserCD, iUserTX, iProgramCD);
			-- 補充指示書有効
			PS_SUPP.SuppDataOK(iTrnID);
			-- ピック指示書有効
			PS_PICK.PickDataOK(iTrnID);
			-- 出荷状態完了更新
			PS_SHIP.EndApplyShip(iTrnID, iUserCD, iUserTX, iProgramCD);
			-- 引当状態更新
			PS_STATUS.RstShipApply;
			COMMIT;
		EXCEPTION
			WHEN OTHERS THEN
				ROLLBACK;
				-- 引当状態更新
				PS_STATUS.RstShipApply;
				COMMIT;
				RAISE;
		END;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || '], ' || 'iUserCD = [' || iUserCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyShip;
END PI_SHIP;
/
SHOW ERRORS
