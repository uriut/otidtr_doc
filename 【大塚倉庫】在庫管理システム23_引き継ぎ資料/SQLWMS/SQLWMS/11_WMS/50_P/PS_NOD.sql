-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_NO AS
	/************************************************************************************/
	/*																					*/
	/*									Private											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************************************/
	/*																					*/
	/*									Public											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************/
	/*	トランザクションID取得									*/
	/************************************************************/
	PROCEDURE GetTrnId (
		oTrnId			OUT	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetTrnId';
		numTrnId		NUMBER(10,0);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_TRNID.NEXTVAL INTO numTrnId
		FROM
			DUAL;
		oTrnId := numTrnId;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetTrnId;
	/************************************************************/
	/*	入荷予定番号取得										*/
	/************************************************************/
	PROCEDURE GetArrivNo (
		oArrivNoCD	OUT	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetArrivNo';
		numArrivNoID	NUMBER(9,0);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_ARRIVNOID.NEXTVAL INTO numArrivNoID
		FROM
			DUAL;
		oArrivNoCD := 'A' || SUBSTRB(TO_CHAR(numArrivNoID, '099999999'), 2, 9);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetArrivNo;
	/************************************************************/
	/*	格納指示書番号取得										*/
	/************************************************************/
	PROCEDURE GetInStockNo (
		oInStockNoCD	OUT	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetInStockNo';
		numInStockNoID	NUMBER(9,0);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_INSTOCKNOID.NEXTVAL INTO numInStockNoID
		FROM
			DUAL;
		oInStockNoCD := 'I' || SUBSTRB(TO_CHAR(numInStockNoID, '099999999'), 2, 9);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetInStockNo;
	/************************************************************/
	/*	ピッキング指示書番号取得								*/
	/************************************************************/
	PROCEDURE GetPickNo (
		oPickNoCD		OUT	VARCHAR2
	) AS 
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetPickNo';
		numPickNoID		NUMBER(9,0);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_PICKNOID.NEXTVAL INTO numPickNoID
		FROM
			DUAL;
		oPickNoCD := 'P' || SUBSTRB(TO_CHAR(numPickNoID, '099999999'), 2, 9);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetPickNo;
	/************************************************************/
	/*	パレット明細番号取得									*/
	/************************************************************/
	PROCEDURE GetPalletDetNo (
		oPalletDetNoCD		OUT	VARCHAR2
	) AS 
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetPalletDetNo';
		numPalletDetNoID		NUMBER(9,0);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_PALLETDETNOID.NEXTVAL INTO numPalletDetNoID
		FROM
			DUAL;
		oPalletDetNoCD := 'L' || SUBSTRB(TO_CHAR(numPalletDetNoID, '099999999'), 2, 9);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetPalletDetNo;
	/************************************************************/
	/*	補充指示書番号取得										*/
	/************************************************************/
	PROCEDURE GetRefillNo (
		oRefillNoCD		OUT	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetRefillNo';
		numRefillNoID	NUMBER(9,0);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_REFILLNOID.NEXTVAL INTO numRefillNoID
		FROM
			DUAL;
		oRefillNoCD := 'R' || SUBSTRB(TO_CHAR(numRefillNoID, '099999999'), 2, 9);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetRefillNo;
	/************************************************************/
	/*	移動指示書番号取得										*/
	/************************************************************/
	PROCEDURE GetMoveNo (
		oMoveNoCD		OUT	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetMoveNo';
		numMoveNoID		NUMBER(9,0);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_MOVENOID.NEXTVAL INTO numMoveNoID
		FROM
			DUAL;
		oMoveNoCD := 'M' || SUBSTRB(TO_CHAR(numMoveNoID,'099999999'), 2, 9);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetMoveNo;
	/************************************************************/
	/*	補充指示書番号取得										*/
	/************************************************************/
	PROCEDURE GetSuppNo (
		oSuppNoCD		OUT	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetSuppNo';
		numSuppNoID		NUMBER(9,0);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_SUPPNOID.NEXTVAL INTO numSuppNoID
		FROM
			DUAL;
		oSuppNoCD := 'S' || SUBSTRB(TO_CHAR(numSuppNoID,'099999999'), 2, 9);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetSuppNo;
	/************************************************************/
	/*	出荷ケース番号取得										*/
	/************************************************************/
	PROCEDURE GetShipCaseNo (
		oShipCaseNoCD	OUT	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetShipCaseNo';
		numShipCaseNoID	NUMBER(9,0);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_SHIPCASENOID.NEXTVAL INTO numShipCaseNoID
		FROM
			DUAL;
		oShipCaseNoCD := 'X' || SUBSTRB(TO_CHAR(numShipCaseNoID,'099999999'), 2, 9);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetShipCaseNo;
	/************************************************************************************/
	/*	入庫バッチ番号取得																*/
	/************************************************************************************/
	PROCEDURE GetIBatchNo(
		oIBatchNoCD	OUT VARCHAR2
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'GetIBatchNo';
		numRowPoint 	NUMBER := 0;
		numIBatchNoID	NUMBER(5,0);
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_IBATCHNOID.NEXTVAL INTO numIBatchNoID
		FROM
			DUAL;
		oIBatchNoCD := 'I' || TO_CHAR(SYSDATE,'MMDD') || SUBSTRB(TO_CHAR(numIBatchNoID,'09999'),2,5);

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetIBatchNo;
	/************************************************************************************/
	/*	出荷番号取得																	*/
	/************************************************************************************/
	PROCEDURE GetShipNo(
		oShipNoCD	OUT VARCHAR2
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'GetShipNo';
		numRowPoint 	NUMBER := 0;
		numShipNoID		NUMBER(9,0);
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_SHIPNOID.NEXTVAL INTO numShipNoID
		FROM
			DUAL;
		oShipNoCD := 'S' || SUBSTRB(TO_CHAR(numShipNoID,'099999999'),2,9);

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetShipNo;
	/************************************************************************************/
	/*	出荷補充バッチ番号取得															*/
	/************************************************************************************/
	PROCEDURE GetSBatchNo(
		oSBatchNoCD	OUT VARCHAR2
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'GetSBatchNo';
		numRowPoint 	NUMBER := 0;
		numSBatchNoID	NUMBER(5,0);
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_SBATCHNOID.NEXTVAL INTO numSBatchNoID
		FROM
			DUAL;
		oSBatchNoCD := 'S' || TO_CHAR(SYSDATE,'MMDD') || SUBSTRB(TO_CHAR(numSBatchNoID,'0999'),2,5);

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetSBatchNo;
	/************************************************************************************/
	/*	出荷ピックバッチ番号取得														*/
	/************************************************************************************/
	PROCEDURE GetPBatchNo(
		oPBatchNoCD	OUT VARCHAR2
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'GetPBatchNo';
		numRowPoint 	NUMBER := 0;
		numPBatchNoID	NUMBER(5,0);
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_PBATCHNOID.NEXTVAL INTO numPBatchNoID
		FROM
			DUAL;
		oPBatchNoCD := 'P' || TO_CHAR(SYSDATE,'MMDD') || SUBSTRB(TO_CHAR(numPBatchNoID,'09999'),2,5);

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetPBatchNo;
	/************************************************************************************/
	/*	移動バッチ番号取得																*/
	/************************************************************************************/
	PROCEDURE GetMBatchNo(
		oMBatchNoCD	OUT VARCHAR2
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'GetMBatchNo';
		numRowPoint 	NUMBER := 0;
		numMBatchNoID	NUMBER(5,0);
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');

		SELECT
			SEQ_MBATCHNOID.NEXTVAL INTO numMBatchNoID
		FROM
			DUAL;

		oMBatchNoCD := 'M' || TO_CHAR(SYSDATE,'MMDD') || SUBSTRB(TO_CHAR(numMBatchNoID,'09999'),2,5);

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetMBatchNo;
END PS_NO;
/
SHOW ERRORS
