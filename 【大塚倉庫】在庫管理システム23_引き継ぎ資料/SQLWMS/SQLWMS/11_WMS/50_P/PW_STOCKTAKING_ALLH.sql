-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PW_TAKING_ALL IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PW_TAKING_ALL';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************************************/
	/*	全棚棚卸																			*/
	/************************************************************************************/
	
	PROCEDURE StocktakingAll(
		--	iPalletDetNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	
--		iProgramCD		IN VARCHAR2
	);
END	PW_TAKING_ALL;
/
SHOW ERRORS
