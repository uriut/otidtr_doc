create or replace
PACKAGE BODY PW_MASTER IS
	/************************************************************************************/
	/*	SKUマスタ更新																	*/
	/************************************************************************************/
	PROCEDURE UpdSku(
		iSkuCD					IN VARCHAR2	,
		iNestainerInStockRateNR	IN NUMBER	,
		iCaseStockRateNR		IN NUMBER	,
		iAbcID					IN NUMBER	,	-- 1:A, 2:B, 3:C, 4:D, 5:E, 6:F, 0:Z
		iAbcSubID				IN NUMBER	,	-- 1:上, 2:下
		iUserCD					IN VARCHAR2	,
		iProgramCD				IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdSku';
		/* SKUマスタ*/
		CURSOR curSku(SkuCD MA_SKU.SKU_CD%TYPE) IS
			SELECT
				SKU_CD
			FROM
				MA_SKU
			WHERE
				SKU_CD = SkuCD
			FOR UPDATE;
		rowSku		curSku%ROWTYPE;
		rowUser		VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		/* マスタ取得*/
		PS_MASTER.GetUser(iUserCD, rowUser);
		/* マスタチェック*/
		OPEN curSku(iSkuCD);
		FETCH curSku INTO rowSku;
		IF curSku%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'SKU=[' || iSkuCD || ']がみつかりません。');
		END IF;
		CLOSE curSku;
		
		IF iNestainerInStockRateNR < 0.0 OR iNestainerInStockRateNR > 1.0 
		OR iCaseStockRateNR        < 0.0 OR iCaseStockRateNR        > 1.0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '格納割合は 0.00 〜 1.00 の範囲内で指定して下さい。');
		END IF;
		
		UPDATE MA_SKU
		SET
			NESTAINERINSTOCKRATE_NR	= iNestainerInStockRateNR	,
			CASESTOCKRATE_NR		= iCaseStockRateNR			,
			ABC_ID					= iAbcID					,
			ABCSUB_ID				= iAbcSubID					,
			--
			UPD_DT					= SYSDATE					,
			UPDUSER_CD				= iUserCD		 			,
			UPDUSER_TX				= rowUser.USER_TX			,
			UPDPROGRAM_CD			= iProgramCD				,
			UPDCOUNTER_NR			= UPDCOUNTER_NR + 1
		WHERE
			SKU_CD = iSkuCD;
		
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curSku%ISOPEN			THEN CLOSE curSku; END IF;
			RAISE;
	END UpdSku;
	/************************************************************************************/
	/*	ロケーションマスタ登録															*/
	/************************************************************************************/
	PROCEDURE AddLoca(
		iWhCD				IN VARCHAR2	,
		iFloorCD			IN VARCHAR2	,
		iAreaCD				IN VARCHAR2	,
		iLineCD				IN VARCHAR2	,
		iShelfCD			IN VARCHAR2	,
		iLayerCD			IN VARCHAR2	,
		iOrderCD			IN VARCHAR2	,
		iTypeLocaBlockCD	IN VARCHAR2	,
		iLineUnitCD			IN VARCHAR2	,
		iLineUnitNR			IN NUMBER	,
		iTypeLocaCD			IN VARCHAR2	,
		iHEquipCD			IN VARCHAR2	,
		iCaseCD				IN VARCHAR2	,
		iPalletDivNR		IN NUMBER	,
		iShipVolumeCD		IN VARCHAR2	,
		iAbcID				IN NUMBER	,
		iAbcSubID			IN NUMBER	,
		iArrivPriorityNR	IN NUMBER	,
		iShipPriorityNR		IN NUMBER	,
		iNotUseFL			IN NUMBER	,
		iNotShipFL			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'AddLoca';

		CURSOR curLoca(LocaCD MA_LOCA.LOCA_CD%TYPE) IS
			SELECT
				LOCA_CD
			FROM
				MA_LOCA
			WHERE
				LOCA_CD LIKE TRIM(LocaCD) || '%' ;
		rowLoca		curLoca%ROWTYPE;

		rowUser				VA_USER%ROWTYPE;
		rowTypeLocaBlock	MB_TYPELOCABLOCK%ROWTYPE;
		rowTypeLoca			MB_TYPELOCA%ROWTYPE;

		numLayer	NUMBER(2,0);
		numOrder	NUMBER(2,0);
		/*従来用*/
		chrFloorCD	MA_LOCA.FLOOR_CD%TYPE;
		chrLineCD	MA_LOCA.LINE_CD%TYPE;
		chrLayerCD	MA_LOCA.LAYER_CD%TYPE;
		chrOrderCD	MA_LOCA.ORDER_CD%TYPE;
		chrLocaCD	MA_LOCA.LOCA_CD%TYPE;
		chrLocaTX	MA_LOCA.LOCA_TX%TYPE;
		wkCount		NUMBER;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		/* マスタ取得*/
		PS_MASTER.GetUser(iUserCD,rowUser);
		/* マスタチェック*/
		OPEN curLoca(iWhCD || iFloorCD || iAreaCD || iLineCD || iShelfCD || iLayerCD || iOrderCD);
		FETCH curLoca INTO rowLoca;
		IF curLoca%FOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーション[' || iWhCD || '-' || iFloorCD || '-' || iAreaCD || '-' || iLineCD || '-' || iShelfCD || '-' || iLayerCD || '-' || iOrderCD || ']はすでに登録済みです。');
		END IF;
		CLOSE curLoca;
		
		/* バリデーション*/
		/* 同一のwh_cd, floor_cd, area_cd, line_cd, shelf_cdに対して、layer_cdは一つのみ登録可能である。*/
		/* 2014-06-17 チェック削除（シリンダが異なるロケーションの登録を可能とする） */
		/*
		SELECT	COUNT(*) INTO wkCount
		FROM	MA_LOCA
		WHERE	WH_CD		= iWhCD
		AND		FLOOR_CD	= iFloorCD
		AND		AREA_CD		= iAreaCD
		AND		LINE_CD		= iLineCD
		AND		SHELF_CD	= iShelfCD
		AND		LAYER_CD	<> iLayerCD;
		IF wkCount >= 1 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーション[' || iWhCD || '-' || iFloorCD || '-' || iAreaCD || '-' || iLineCD || '-' || iShelfCD || ']内に、別のシリンダーコードのデータが既に登録されておりますので、登録できません。');
		END IF;
		*/
		
		/* ロケーションブロックコードチェック*/
		PS_MASTER.GetTypeLocaBlock(iTypeLocaBlockCD,rowTypeLocaBlock);
		/* ロケーションタイプコードチェック*/
		PS_MASTER.GetTypeLoca(iTypeLocaCD,rowTypeLoca);
		/**/
				chrFloorCD	:= LPAD(iFloorCD,2,'0');

				chrLocaCD	:= iWhCD || chrFloorCD || iAreaCD || iLineCD || iShelfCD || iLayerCD || iOrderCD;
				chrLocaTX	:= iWhCD || '-' || chrFloorCD || '-' || iAreaCD || '-' || iLineCD || '-' || iShelfCD || '-' || iLayerCD || '-' || iOrderCD;
				
				/* ロケーション追加*/
				INSERT INTO MA_LOCA (
					LOCA_CD				,	/* ロケーション*/
					LOCA_TX				,	/* ロケーション名　WH_CD + FLOOR_CD + AREA_CD + LINE_CD + SHELF_CD + ORDER_CD(01固定)*/
					WH_CD				,	/* 倉庫コード　当面は浦安で固定*/
					FLOOR_CD			,	/* フロアコード　当面は3Fで固定*/
					AREA_CD				,	/* エリアコード*/
					LINE_CD				,	/* 列コード*/
					SHELF_CD			,	/* 棚コード*/
					LAYER_CD			,	/* 段コード*/
					ORDER_CD			,	/* 番コード*/
					TYPELOCA_CD			,	/* ロケーション種別コード*/
					TYPELOCABLOCK_CD	,	/* ロケーションブロックコード　01:ネステナ、02:平置き*/
					LINEUNIT_CD			,	/* ラインユニット*/
					LINEUNIT_NR			,	/* ラインユニット*/
					HEQUIP_CD			,	/* 格納什器コード　パレット固定*/
					CASE_CD				,	/* 標準ケースコード*/
					PALLETDIV_NR		,	/* パレット分割数*/
					SHIPVOLUME_CD		,	/* 出荷荷姿コード  ケース成り出荷とパレット出荷のエリアを識別するコード　01:ケース成り、02:パレット成り、03:ネステナ*/
					ABC_ID				,	/* ロケーションＡＢＣＩＤ*/
					ABCSUB_ID			,	/* ロケーションＡＢＣＳＵＢＩＤ*/
					ARRIVPRIORITY_NR	,	/* 入庫優先順位*/
					SHIPPRIORITY_NR		,	/* 出庫優先順位*/
					INVENT_FL			,	/* 棚卸対象フラグ*/
					NOTUSE_FL			,	/* 未使用フラグ（入庫引当で利用）*/
					NOTSHIP_FL			,	/* 出荷不可フラグ */
					/* 管理項目*/
					ADD_DT				,	/* 登録日時*/
					ADDUSER_CD			,	/* 登録ユーザコード*/
					ADDUSER_TX			,	/* 登録ユーザ名*/
					UPDPROGRAM_CD		,	/* 更新プログラムコード*/
					UPDCOUNTER_NR		,	/* 更新カウンター*/
					STRECORD_ID			 	/* レコード状態（-2:削除、-1:作成中、0:通常）*/
				) VALUES (
					chrLocaCD				,
					chrLocaTX				,
					iWhCD					,
					chrFloorCD				,
					iAreaCD					,
					iLineCD					,
					iShelfCD				,
					iLayerCD				,
					iOrderCD				,
					iTypeLocaCD				,
					iTypeLocaBlockCD		,
					iLineUnitCD				,
					iLineUnitNR				,
					iHEquipCD				,
					iCaseCD					,
					iPalletDivNR			,
					iShipVolumeCD			,
					iAbcID					,
					iAbcSubID				,
					iArrivPriorityNR		,
					iShipPriorityNR			,
					0						,
					iNotUseFL				,
					iNotShipFL				,
					/**/
					SYSDATE					,
					iUserCD					,
					rowUser.USER_TX			,
					iProgramCD				,
					0						,
					PS_DEFINE.ST_RECORD0_NORMAL
				);
				/* ロケーションロック追加*/
				INSERT INTO TA_LOCALOCK (
					LOCA_CD			,
					/**/
					ADD_DT			,
					ADDUSER_CD		,
					ADDUSER_TX		,
					UPDPROGRAM_CD	,
					UPDCOUNTER_NR	,
					STRECORD_ID
				) VALUES (
					chrLocaCD		,
					/**/
					SYSDATE			,
					iUserCD			,
					rowUser.USER_TX	,
					iProgramCD		,
					0				,
					PS_DEFINE.ST_RECORD0_NORMAL
				);
		
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLoca%ISOPEN THEN CLOSE curLoca; END IF;
			RAISE;
	END AddLoca;
	/************************************************************************************/
	/*	ロケーションマスタ更新				棚単位										*/
	/************************************************************************************/
	PROCEDURE UpdLoca(
		iLocaShelfCD		IN VARCHAR2	,
		iTypeLocaBlockCD	IN VARCHAR2	,
		iLineUnitCD			IN VARCHAR2	,
		iLineUnitNR			IN NUMBER	,
		iTypeLocaCD			IN VARCHAR2	,
		iCaseCD				IN VARCHAR2	,
		iPalletDivNR		IN NUMBER	,
		iShipVolumeCD		IN VARCHAR2	,
		iAbcID				IN NUMBER	,
		iAbcSubID			IN NUMBER	,
		iArrivPriorityNR	IN NUMBER	,
		iShipPriorityNR		IN NUMBER	,
		iNotUseFL			IN NUMBER	,
		iNotShipFL			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdLoca';
		/* ロケーションマスタ*/
		CURSOR curLoca(LocaShelfCD VARCHAR2) IS
			SELECT
				LOCA_CD
			FROM
				MA_LOCA
			WHERE
				LOCA_CD LIKE LocaShelfCD || '%'
			FOR UPDATE;
		rowLoca		curLoca%ROWTYPE;
		/* ロケーションブロック*/
		CURSOR curTypeBlockCd(LocaShelfCD VARCHAR2) IS
			SELECT
				MLC.TYPELOCABLOCK_CD
			FROM
				MA_LOCA		MLC
			WHERE
				MLC.LOCA_CD LIKE LocaShelfCD || '%';
		rowTypeBlockCd		curTypeBlockCd%ROWTYPE;
		/* 在庫ありロケ*/
		CURSOR curStock(LocaShelfCD VARCHAR2) IS
			SELECT
				MLC.LOCA_CD
			FROM
				MA_LOCA			MLC,
				TA_LOCASTOCKP	TLP,
				TA_LOCASTOCKC	TLC
			WHERE
				MLC.LOCA_CD	= TLP.LOCA_CD(+)		AND
				MLC.LOCA_CD	= TLC.LOCA_CD(+)		AND
				MLC.LOCA_CD LIKE LocaShelfCD || '%'	AND
				(
					TLP.PCS_NR IS NOT NULL	OR
					TLC.PCS_NR IS NOT NULL
				);
		rowStock		curStock%ROWTYPE;

		rowUser				VA_USER%ROWTYPE;
		rowTypeLocaBlock	MB_TYPELOCABLOCK%ROWTYPE;
		rowTypeLoca			MB_TYPELOCA%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		/* マスタ取得*/
		PS_MASTER.GetUser(iUserCD, rowUser);
		/* マスタチェック*/
		OPEN curLoca(iLocaShelfCD);
		FETCH curLoca INTO rowLoca;
		IF curLoca%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーション[' || iLocaShelfCD || ']がみつかりません。');
		END IF;
		/* ロケーションブロックコードチェック*/
		PS_MASTER.GetTypeLocaBlock(iTypeLocaBlockCD, rowTypeLocaBlock);
		OPEN curTypeBlockCd(iLocaShelfCD);
		FETCH curTypeBlockCd INTO rowTypeBlockCd;
		IF rowTypeBlockCd.TYPELOCABLOCK_CD != rowTypeLocaBlock.TYPELOCABLOCK_CD THEN
			OPEN curStock(iLocaShelfCD);
			FETCH curStock INTO rowStock;
			IF curStock%FOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '在庫のあるロケーションはロケーションブロックコードを変更できません');
			END IF;
			CLOSE curStock;
		END IF;
		CLOSE curTypeBlockCd;

		UPDATE MA_LOCA
		SET
			TYPELOCABLOCK_CD	= iTypeLocaBlockCD	,
			LINEUNIT_CD			= iLineUnitCD		,
			LINEUNIT_NR			= iLineUnitNR		,
			TYPELOCA_CD			= iTypeLocaCD		,
			CASE_CD				= iCaseCD			,
			PALLETDIV_NR		= iPalletDivNR		,
			SHIPVOLUME_CD		= iShipVolumeCD		,
			ABC_ID				= iAbcID			,
			ABCSUB_ID			= iAbcSubID			,
			ARRIVPRIORITY_NR	= iArrivPriorityNR	,
			SHIPPRIORITY_NR		= iShipPriorityNR	,
			NOTUSE_FL			= iNotUseFL			,
			NOTSHIP_FL			= iNotShipFL		,
			/**/
			UPD_DT				= SYSDATE			,
			UPDUSER_CD			= iUserCD		 	,
			UPDUSER_TX			= rowUser.USER_TX	,
			UPDPROGRAM_CD		= iProgramCD		,
			UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
		WHERE
			LOCA_CD LIKE iLocaShelfCD || '%';
		CLOSE curLoca;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLoca%ISOPEN			THEN CLOSE curLoca; END IF;
			IF curTypeBlockCd%ISOPEN	THEN CLOSE curTypeBlockCd; END IF;
			IF curStock%ISOPEN			THEN CLOSE curStock; END IF;
			RAISE;
	END UpdLoca;
	/************************************************************************************/
	/*	ロケーションマスタ更新				ロケ単位									*/
	/************************************************************************************/
	PROCEDURE UpdLocation(
		iLocaCD				IN VARCHAR2	,
		iTypeLocaBlockCD	IN VARCHAR2	,
		iLineUnitCD			IN VARCHAR2	,
		iLineUnitNR			IN NUMBER	,
		iTypeLocaCD			IN VARCHAR2	,
		iCaseCD				IN VARCHAR2	,
		iPalletDivNR		IN NUMBER	,
		iShipVolumeCD		IN VARCHAR2	,
		iAbcID				IN NUMBER	,
		iAbcSubID			IN NUMBER	,
		iArrivPriorityNR	IN NUMBER	,
		iShipPriorityNR		IN NUMBER	,
		iNotUseFL			IN NUMBER	,
		iNotShipFL			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdLocation';
		/* ロケーションマスタ*/
		CURSOR curLoca(LocaCD VARCHAR2) IS
			SELECT
				LOCA_CD
			FROM
				MA_LOCA
			WHERE
				LOCA_CD = LocaCD
			FOR UPDATE;
		rowLoca		curLoca%ROWTYPE;
		/* ロケーションブロック*/
		CURSOR curTypeBlockCd(LocaCD VARCHAR2) IS
			SELECT
				MLC.TYPELOCABLOCK_CD
			FROM
				MA_LOCA		MLC
			WHERE
				MLC.LOCA_CD = LocaCD;
		rowTypeBlockCd		curTypeBlockCd%ROWTYPE;
		/* 在庫ありロケ*/
		CURSOR curStock(LocaCD VARCHAR2) IS
			SELECT
				MLC.LOCA_CD
			FROM
				MA_LOCA			MLC,
				TA_LOCASTOCKP	TLP,
				TA_LOCASTOCKC	TLC
			WHERE
				MLC.LOCA_CD	= TLP.LOCA_CD(+)		AND
				MLC.LOCA_CD	= TLC.LOCA_CD(+)		AND
				MLC.LOCA_CD = LocaCD	AND
				(
					TLP.PCS_NR IS NOT NULL	OR
					TLC.PCS_NR IS NOT NULL
				);
		rowStock		curStock%ROWTYPE;

		rowUser				VA_USER%ROWTYPE;
		rowTypeLocaBlock	MB_TYPELOCABLOCK%ROWTYPE;
		rowTypeLoca			MB_TYPELOCA%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		/* マスタ取得*/
		PS_MASTER.GetUser(iUserCD, rowUser);
		/* マスタチェック*/
		OPEN curLoca(iLocaCD);
		FETCH curLoca INTO rowLoca;
		IF curLoca%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーション[' || iLocaCD || ']がみつかりません。');
		END IF;
		/* ロケーションブロックコードチェック*/
		PS_MASTER.GetTypeLocaBlock(iTypeLocaBlockCD, rowTypeLocaBlock);
		OPEN curTypeBlockCd(iLocaCD);
		FETCH curTypeBlockCd INTO rowTypeBlockCd;
		IF rowTypeBlockCd.TYPELOCABLOCK_CD != rowTypeLocaBlock.TYPELOCABLOCK_CD THEN
			OPEN curStock(iLocaCD);
			FETCH curStock INTO rowStock;
			IF curStock%FOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '在庫のあるロケーションはロケーションブロックコードを変更できません');
			END IF;
			CLOSE curStock;
		END IF;
		CLOSE curTypeBlockCd;
		UPDATE MA_LOCA
		SET
			TYPELOCABLOCK_CD	= iTypeLocaBlockCD	,
			LINEUNIT_CD			= iLineUnitCD		,
			LINEUNIT_NR			= iLineUnitNR		,
			TYPELOCA_CD			= iTypeLocaCD		,
			CASE_CD				= iCaseCD			,
			PALLETDIV_NR		= iPalletDivNR		,
			SHIPVOLUME_CD		= iShipVolumeCD		,
			ABC_ID				= iAbcID			,
			ABCSUB_ID			= iAbcSubID			,
			ARRIVPRIORITY_NR	= iArrivPriorityNR	,
			SHIPPRIORITY_NR		= iShipPriorityNR	,
			NOTUSE_FL			= iNotUseFL			,
			NOTSHIP_FL			= iNotShipFL		,
			/**/
			UPD_DT				= SYSDATE			,
			UPDUSER_CD			= iUserCD		 	,
			UPDUSER_TX			= rowUser.USER_TX	,
			UPDPROGRAM_CD		= iProgramCD		,
			UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
		WHERE
			LOCA_CD = iLocaCD;
		CLOSE curLoca;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLoca%ISOPEN			THEN CLOSE curLoca; END IF;
			IF curTypeBlockCd%ISOPEN	THEN CLOSE curTypeBlockCd; END IF;
			IF curStock%ISOPEN			THEN CLOSE curStock; END IF;
			RAISE;
	END UpdLocation;
	/************************************************************************************/
	/*	ロケーション削除																*/
	/************************************************************************************/
	PROCEDURE DelLoca(
		iLocaShelfCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DelLoca';
		CURSOR curLoca(LocaShelfCD VARCHAR2) IS
			SELECT
				LOCA_CD
			FROM
				MA_LOCA
			WHERE
				LOCA_CD LIKE LocaShelfCD || '%';
		rowLoca			curLoca%ROWTYPE;
		/* 空きロケ検索*/
		CURSOR curStock(LocaShelfCD VARCHAR2) IS
			SELECT
				MLC.LOCA_CD
			FROM
				MA_LOCA			MLC,
				TA_LOCASTOCKP	TLP,
				TA_LOCASTOCKC	TLC
			WHERE
				MLC.LOCA_CD	= TLP.LOCA_CD(+)		AND
				MLC.LOCA_CD	= TLC.LOCA_CD(+)		AND
				MLC.LOCA_CD	LIKE LocaShelfCD || '%'	AND
				(
					TLP.PCS_NR IS NOT NULL OR
					TLC.PCS_NR IS NOT NULL
				);
		rowStock		curStock%ROWTYPE;

		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		/* マスタ取得*/
		PS_MASTER.GetUser(iUserCD, rowUser);
		/* 引当状態確認*/
		PS_STATUS.ChkApply;
		BEGIN
			/* 引当状態更新*/
			PS_STATUS.SetMaster;
			COMMIT;
			/* マスタチェック*/
			OPEN curLoca(iLocaShelfCD);
			FETCH curLoca INTO rowLoca;
			IF curLoca%NOTFOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーションコード = [' || iLocaShelfCD || ']がみつかりません。');
			END IF;
			CLOSE curLoca;
			/* 在庫のあるロケかチェック*/
			OPEN curStock(iLocaShelfCD);
			FETCH curStock INTO rowStock;
			IF curStock%FOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '在庫のあるロケーションは削除できません');
			END IF;
			CLOSE curStock;
			/* ロケーションマスタ削除*/
			DELETE MA_LOCA
			WHERE
				LOCA_CD LIKE iLocaShelfCD || '%';
			/* LOCALOCK削除*/
			DELETE TA_LOCALOCK
			WHERE
				LOCA_CD LIKE iLocaShelfCD || '%';
			/* マスタ修正ステータスリセット更新*/
			PS_STATUS.RstMaster;
			COMMIT;
		EXCEPTION
			WHEN OTHERS THEN
				ROLLBACK;
				/* マスタ修正ステータスリセット更新*/
				PS_STATUS.RstMaster;
				COMMIT;
				RAISE;
		END;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLoca%ISOPEN	THEN CLOSE curLoca; END IF;
			IF curStock%ISOPEN	THEN CLOSE curStock; END IF;
			RAISE;
	END DelLoca;
	/************************************************************************************/
	/*	かんぱんマスタ登録																*/
	/************************************************************************************/
	PROCEDURE AddKanban(
		iKanbanCD			IN VARCHAR2	,
		iKanbanTX			IN VARCHAR2	,
		iTypeKanbanCD		IN VARCHAR2	,
		iColorTX			IN VARCHAR2	,
		iAreaTX				IN VARCHAR2	,
		iLineUnitCD			IN VARCHAR2	,
		iLineUnitNR			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'AddKanban';

		CURSOR curKanban(KanbanCD MA_KANBAN.KANBAN_CD%TYPE) IS
			SELECT
				KANBAN_CD
			FROM
				MA_KANBAN
			WHERE
				KANBAN_CD = TRIM(KanbanCD);
		rowKanban		curKanban%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');

		IF iKanbanCD IS Null THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばん番号が指定されていません。[' || iKanbanCD || ']');
		END IF;
		/**/
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'USER_CD=[ ' || iUserCD || ' ]' );
		/* マスタ取得*/
		PS_MASTER.GetUser(iUserCD,rowUser);
		/* マスタチェック*/
		OPEN curKanban(iKanbanCD);
		FETCH curKanban INTO rowKanban;
		IF curKanban%FOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばん番号[' || iKanbanCD || ']はすでに登録済みです。');
		END IF;
		CLOSE curKanban;

		/* かんばん追加*/
		INSERT INTO MA_KANBAN (
			KANBAN_CD		,	/* かんばんコード*/
			KANBAN_TX		,	/* かんばん名*/
			TYPEKANBAN_CD	,	/* かんばんタイプ*/
			COLOR_TX		,	/* 色*/
			AREA_TX			,	/* エリア*/
			LINEUNIT_CD		,	/* ラインユニット(コード) */
			LINEUNIT_NR		,	/* ラインユニット(数値) */
			/* 管理項目*/
			ADD_DT			,	/* 登録日時*/
			ADDUSER_CD		,	/* 登録ユーザコード*/
			ADDUSER_TX		,	/* 登録ユーザ名*/
			UPDPROGRAM_CD	,	/* 更新プログラムコード*/
			UPDCOUNTER_NR	,	/* 更新カウンター*/
			STRECORD_ID			/* レコード状態（-2:削除、-1:作成中、0:通常）*/
		) VALUES (
			iKanbanCD		,
			iKanbanTX		,
			iTypeKanbanCD	,
			iColorTX		,
			iAreaTX			,
			iLineUnitCD		,
			iLineUnitNR		,
			/**/
			SYSDATE			,
			iUserCD			,
			rowUser.USER_TX	,
			iProgramCD		,
			0				,
			PS_DEFINE.ST_RECORD0_NORMAL
		);
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curKanban%ISOPEN THEN CLOSE curKanban; END IF;
			RAISE;
	END AddKanban;
	/************************************************************************************/
	/*	かんぱんマスタ更新																*/
	/************************************************************************************/
	PROCEDURE UpdKanban(
		iOldKanbanCD		IN VARCHAR2	,
		iNewKanbanCD		IN VARCHAR2	,
		iKanbanTX			IN VARCHAR2	,
		iTypeKanbanCD		IN VARCHAR2	,
		iColorTX			IN VARCHAR2	,
		iAreaTX				IN VARCHAR2	,
		iLineUnitCD			IN VARCHAR2	,
		iLineUnitNR			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdKanban';

		CURSOR curKanban(KanbanCD MA_KANBAN.KANBAN_CD%TYPE) IS
			SELECT
				KANBAN_CD
			FROM
				MA_KANBAN
			WHERE
				KANBAN_CD = TRIM(KanbanCD);
		rowKanban		curKanban%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');

		IF iOldKanbanCD IS Null THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '更新前かんばん番号が指定されていません。[' || iOldKanbanCD || ']');
		END IF;
		IF iNewKanbanCD IS Null THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '更新後かんばん番号が指定されていません。[' || iNewKanbanCD || ']');
		END IF;

		/**/
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'USER_CD=[ ' || iUserCD || ' ]' );
		/* マスタ取得*/
		PS_MASTER.GetUser(iUserCD,rowUser);
		/* マスタチェック*/
		OPEN curKanban(iOldKanbanCD);
		FETCH curKanban INTO rowKanban;
		IF curKanban%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばん番号[' || iOldKanbanCD || ']のデータが存在しません。');
		END IF;
		CLOSE curKanban;

		IF iOldKanbanCD <> iNewKanbanCD THEN
			/* マスタチェック*/
			OPEN curKanban(iNewKanbanCD);
			FETCH curKanban INTO rowKanban;
			IF curKanban%FOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばん番号[' || iNewKanbanCD || ']のデータがすでに存在していますので更新出来ません。');
			END IF;
			CLOSE curKanban;
		END IF;


		/* かんばん削除*/
		UPDATE MA_KANBAN
		SET
			KANBAN_CD		= iNewKanbanCD		,	/* かんばんコード*/
			KANBAN_TX		= iKanbanTX			,	/* かんばん名*/
			TYPEKANBAN_CD	= iTypeKanbanCD		,	/* かんばんタイプ*/
			COLOR_TX		= iColorTX			,	/* 色*/
			AREA_TX			= iAreaTX			,	/* エリア*/
			LINEUNIT_CD		= iLineUnitCD		,	/* ラインユニット(コード) */
			LINEUNIT_NR		= iLineUnitNR		,	/* ラインユニット(数値) */
			/**/
			UPD_DT			= SYSDATE			,	/* 更新日時*/
			UPDUSER_CD		= iUserCD		 	,	/* 更新ユーザコード*/
			UPDUSER_TX		= rowUser.USER_TX	,	/* 更新ユーザ名*/
			UPDPROGRAM_CD	= iProgramCD		,	/* 更新プログラムコード*/
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1		/* 更新カウンター*/
		WHERE
			KANBAN_CD		= iOldKanbanCD;
		COMMIT;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curKanban%ISOPEN THEN CLOSE curKanban; END IF;
			RAISE;
	END UpdKanban;
	/************************************************************************************/
	/*	かんぱんマスタ削除																*/
	/************************************************************************************/
	PROCEDURE DelKanban(
		iKanbanCD			IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DelKanban';

		CURSOR curKanban(KanbanCD MA_KANBAN.KANBAN_CD%TYPE) IS
			SELECT
				KANBAN_CD
			FROM
				MA_KANBAN
			WHERE
				KANBAN_CD = TRIM(KanbanCD);
		rowKanban		curKanban%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');

		IF iKanbanCD IS Null THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばん番号が指定されていません。[' || iKanbanCD || ']');
		END IF;

		/**/
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'USER_CD=[ ' || iUserCD || ' ]' );
		/* マスタ取得*/
		PS_MASTER.GetUser(iUserCD,rowUser);

		/* マスタチェック*/
		OPEN curKanban(iKanbanCD);
		FETCH curKanban INTO rowKanban;
		IF curKanban%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばん番号[' || iKanbanCD || ']のデータが存在しません。');
		END IF;
		CLOSE curKanban;

		/* かんばん削除*/
		DELETE MA_KANBAN
		WHERE
			KANBAN_CD		= iKanbanCD;
		COMMIT;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curKanban%ISOPEN THEN CLOSE curKanban; END IF;
			RAISE;
	END DelKanban;
	/************************************************************************************/
	/*	荷姿登録																		*/
	/************************************************************************************/
	PROCEDURE AddTypePackaging(
		/*	どういう内容なのか不明。確認要。*/
		/*	SKUマスタの「出荷荷姿」(typepackaging_cd)を更新する処理と推測*/
		/**/
		iSkuCD				IN VARCHAR2	,
		iTypePackagingCD	IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'AddTypePackaging';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END AddTypePackaging;
END PW_MASTER;
/
SHOW ERRORS
