-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PI_TRANSPORTER AS
	/************************************************************/
	/* ファイル単位チェック                                     */
	/************************************************************/
	PROCEDURE CheckByFile(
		iTrnID						IN TMP_SKU.TRN_ID%TYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'CheckByFile';
		-- アップロードされたデータの重複分
		CURSOR curFrom(TrnID	TMP_TRANSPORTER.TRN_ID%TYPE) IS
			SELECT
				SUCD AS TRANSPORTER_CD
			FROM
				TMP_TRANSPORTER
			WHERE
				TRN_ID = TrnID
			GROUP BY
				SUCD
			HAVING
				COUNT(*) > 1;
		rowFrom curFrom%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 重複チェック
		OPEN curFrom(iTrnID);
		-- 対象行を取得
		FETCH curFrom INTO rowFrom;
		-- 重複行が存在した場合はエラー
		IF curFrom%FOUND THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EXCEPTION_SYS,'入荷予定明細の同一ファイル内でキーが重複しています。');
		END IF;
		CLOSE curFrom;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curFrom%ISOPEN THEN
				CLOSE curFrom;
			END IF;
			RAISE;
	END CheckByFile;
	/*----------------------------------------------------------*/
	/* Public                                                   */
	/*----------------------------------------------------------*/
	/************************************************************/
	/* アップロード完了（配送業者）                             */
	/************************************************************/
	PROCEDURE EndUploadTransPorter(
		iTrnID						IN TMP_TRANSPORTER.TRN_ID%TYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'EndUploadTransPorter';
		-- TMP
		CURSOR curTmp(TrnID	TMP_TRANSPORTER.TRN_ID%TYPE) IS
			SELECT
				SUCD AS TRANSPORTER_CD			,
				SUCD_NM AS TRANSPORTER_TX		,
				SUCD_S AS TRANSPORTERGROUP_CD	,
				SUCD_S_NM AS TRANSPORTERGROUP_TX,
				--CASE
				--	WHEN SUCD_S = '040500' THEN 0
				--	ELSE 1 
				--END AS TOTALPIC_FL				,	-- todo
				1 AS TOTALPIC_FL				,
				SORT AS PICKORDER_NR			,
				STIME AS BERTHTIME_TX
			FROM
				TMP_TRANSPORTER
			WHERE
				TRN_ID = TrnID
			ORDER BY
				SUCD;
		rowTmp curTmp%ROWTYPE;
		numCount			NUMBER;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- ファイル単位チェック
		CheckByFile(iTrnID);
		-- テーブル単位チェックはWMS側で実施
		OPEN curTmp(iTrnID);
		LOOP
			FETCH curTmp INTO rowTmp;
			EXIT WHEN curTmp%NOTFOUND;
			-- TRANSPORTER
			SELECT
				COUNT(TRANSPORTER_CD)
			INTO
				numCount
			FROM
				MA_TRANSPORTER
			WHERE
				TRANSPORTER_CD = rowTmp.TRANSPORTER_CD;
			IF numCount = 0 THEN
				--登録
				INSERT INTO MA_TRANSPORTER (
					TRANSPORTER_CD		,
					TRANSPORTER_TX		,
					TRANSPORTERGROUP_CD	,
					TRANSPORTERGROUP_TX	,
					TOTALPIC_FL			,
					PICKORDER_NR		,
					BERTHTIME_TX		,
					--
					UPD_DT				,
					UPDUSER_CD			,
					UPDUSER_TX			,
					ADD_DT				,
					ADDUSER_CD			,
					ADDUSER_TX			,
					UPDPROGRAM_CD		,
					UPDCOUNTER_NR		,
					STRECORD_ID			
				) VALUES (
					rowTmp.TRANSPORTER_CD		,
					rowTmp.TRANSPORTER_TX		,
					rowTmp.TRANSPORTERGROUP_CD	,
					rowTmp.TRANSPORTERGROUP_TX	,
					rowTmp.TOTALPIC_FL			,
					rowTmp.PICKORDER_NR			,
					rowTmp.BERTHTIME_TX			,
					--
					SYSDATE						,
					'BATCH'						,
					'BATCH'						,
					SYSDATE						,
					'BATCH'						,
					'BATCH'						,
					'BATCH'						,
					0							,
					0							
				);
			ELSE
				--更新
				UPDATE
					MA_TRANSPORTER
				SET
					TRANSPORTER_TX		= rowTmp.TRANSPORTER_TX		,
					TRANSPORTERGROUP_CD	= rowTmp.TRANSPORTERGROUP_CD,
					TRANSPORTERGROUP_TX	= rowTmp.TRANSPORTERGROUP_TX,
					--TOTALPIC_FL			= rowTmp.TOTALPIC_FL		,
					PICKORDER_NR		= rowTmp.PICKORDER_NR		,
					BERTHTIME_TX		= rowTmp.BERTHTIME_TX		,
					-- 管理項目
					UPD_DT				= SYSDATE					,
					UPDUSER_CD			= 'BATCH'					,
					UPDUSER_TX			= 'BATCH'					,
					UPDPROGRAM_CD		= 'BATCH'					,
					UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
				WHERE
					TRANSPORTER_CD = rowTmp.TRANSPORTER_CD;
			END IF;
		END LOOP;
		CLOSE curTmp;
		COMMIT;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END EndUploadTransPorter;
END PI_TRANSPORTER;
/
SHOW ERRORS
