-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PI_SUPPLIER AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PI_SUPPLIER';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************/
	/* アップロード完了（仕入先）                               */
	/************************************************************/
	PROCEDURE EndUploadSupplier(
		iTrnID						IN TMP_SUPPLIER.TRN_ID%TYPE
	);
	
END PI_SUPPLIER;
/
SHOW ERRORS
