-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_STOCK AS 

	--
	--		テーブルレベルの基本操作
	--
	
	/************************************************************************************/
	/*	在庫受払																		*/
	/************************************************************************************/
	PROCEDURE InsStockInOut(
		iWHCD				IN VARCHAR2	,	--自倉庫コード
		iTargetWHCD			IN VARCHAR2	,	--相手先倉庫コード
		iLocaCD				IN VARCHAR2	,
		iSkuCD				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,	--かんばん番号
		iCasePerPalletNR	IN NUMBER	,	--パレット積み付け数
		iPalletLayerNR		IN NUMBER	,	--パレット段数
		iPalletCapacityNR	IN NUMBER	,	-- パレット積載数
		iStackingNumberNR	IN NUMBER	,	--保管段数
		iTypePalletCD		IN VARCHAR2	,	--パレットタイプ
		iUseLimitDT			IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,	--在庫区分
		iTypeLocaCD			IN VARCHAR2	,	--ロケーション種別コード
		iTypeOperationCD	IN VARCHAR2	,	--業務区分
		iTypeRefCD			IN VARCHAR2	,	--システム参照先区分
		iRefNoID			IN NUMBER	,	--システム参照ID
		iMemoTX				IN VARCHAR2	,
		iNeedSendFL			IN NUMBER	,	--要送信フラグ
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsStockInOut';
		
		numStockInoutID	TA_STOCKINOUT.STOCKINOUT_ID%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_STOCKINOUTID.NEXTVAL INTO numStockInoutID
		FROM
			DUAL;
		INSERT INTO TA_STOCKINOUT (
			STOCKINOUT_ID		,
			WH_CD				,
			TARGETWH_CD			,
			LOCA_CD				,
			SKU_CD				,
			LOT_TX				,
			KANBAN_CD			,
			CASEPERPALLET_NR	,
			PALLETLAYER_NR		,
			PALLETCAPACITY_NR	,
			STACKINGNUMBER_NR	,
			TYPEPLALLET_CD		,
			USELIMIT_DT			,
			BATCHNO_CD			,
			STOCK_DT			,
			AMOUNT_NR			,
			PALLET_NR			,
			CASE_NR				,
			BARA_NR				,
			PCS_NR				,
			TYPESTOCK_CD		,
			TYPELOCA_CD			,
			TYPEOPERATION_CD	,
			TYPEREF_CD			,
			REFNO_ID			,
			MEMO_TX				,
			NEEDSEND_FL			,
			SENDEND_FL			,
			-- 管理項目
			UPD_DT				,
			UPDUSER_CD			,
			UPDUSER_TX			,
			ADD_DT				,
			ADDUSER_CD			,
			ADDUSER_TX			,
			UPDPROGRAM_CD		,
			UPDCOUNTER_NR		,
			STRECORD_ID
		) VALUES (
			numStockInoutID		,
			iWHCD				,
			iTargetWHCD			,
			iLocaCD				,
			iSkuCD				,
			iLotTX				,
			iKanbanCD			,	--かんばん番号
			iCasePerPalletNR	,	--パレット積み付け数
			iPalletLayerNR		,	--パレット段数
			iPalletCapacityNR	,
			iStackingNumberNR	,	--保管段数
			iTypePalletCD		,	--パレットタイプ
			iUseLimitDT			,
			iBatchnoCd			,
			iStockDT			,
			iAmountNR			,
			iPalletNR			,
			iCaseNR				,
			iBaraNR				,
			iPcsNR				,
			iTypeStockCD		,
			iTypeLocaCD			,
			iTypeOperationCD	,
			iTypeRefCD			,
			iRefNoID  			,
			iMemoTx				,
			iNeedSendFL			,
			0					,
			--
			SYSDATE				,
			iUserCD				,
			iUserTX				,
			SYSDATE				,
			iUserCD				,
			iUserTX				,
			iProgramCD			,
			0					,
			PS_DEFINE.ST_RECORD0_NORMAL
		);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsStockInOut;
	/************************************************************************************/
	/*	引当可能数・更新																*/
	/************************************************************************************/
	PROCEDURE UpdateP(
		iRowID	 			IN UROWID	,
		iAmountNR			IN NUMBER	,
		iPalletCapacityNr	IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdateP';
		numCount	NUMBER := 0;
		numRetPcs	TA_LOCASTOCKP.PCS_NR%TYPE	:= 0;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 更新
		UPDATE
			TA_LOCASTOCKP
		SET
			AMOUNT_NR		= iAmountNR						,
			PALLET_NR		= ( 
								(((PCS_NR + iPcsNR) - MOD(PCS_NR + iPcsNR,iAmountNR)) / iAmountNR) - 
								MOD(((PCS_NR + iPcsNR) - MOD(PCS_NR + iPcsNR,iAmountNR)) / iAmountNR,iPalletCapacityNr) 
								) / iPalletCapacityNr		,	-- パレット数
			CASE_NR 		= MOD(((PCS_NR + iPcsNR)-MOD(PCS_NR + iPcsNR,iAmountNR))/iAmountNR,iPalletCapacityNr),	-- ケース数
			BARA_NR 		= MOD(PCS_NR + iPcsNR,iAmountNR),
			PCS_NR			= PCS_NR + iPcsNR				,
			--
			UPD_DT			= SYSDATE						,
			UPDUSER_CD		= iUserCD						,
			UPDUSER_TX		= iUserTX						,
			UPDPROGRAM_CD	= iProgramCD					,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			ROWID = iRowID
		RETURN 
			PCS_NR INTO numRetPcs;
		numCount := SQL%ROWCOUNT;
		IF numCount <> 1 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：更新異常が発生しました。' ||
				'iRowID = [' || iRowID || '], numCount = [' || numCount || ']'
			);
		END IF;
		IF numRetPcs = 0 THEN
			DELETE
				TA_LOCASTOCKP
			WHERE
				ROWID = iRowID AND
				PCS_NR = 0;
		END IF;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END UpdateP;
	/************************************************************************************/
	/*	引当可能数・登録																*/
	/************************************************************************************/
	PROCEDURE InsertP(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX 				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iCasePerPalletNR	IN NUMBER	,
		iPalletLayerNR		IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,	-- パレット積載数
		iStackingNumberNR	IN NUMBER	,
		iTypePalletCD		IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchNoCD			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsertP';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		INSERT INTO TA_LOCASTOCKP (
			LOCA_CD			,
			SKU_CD			,
			LOT_TX			,
			KANBAN_CD		,
			CASEPERPALLET_NR,
			PALLETLAYER_NR	,
			PALLETCAPACITY_NR,
			STACKINGNUMBER_NR,
			TYPEPLALLET_CD	,
			USELIMIT_DT		,
			BATCHNO_CD		,
			STOCK_DT		,
			AMOUNT_NR		,
			PALLET_NR		,
			CASE_NR			,
			BARA_NR			,
			PCS_NR			,
			TYPESTOCK_CD	,
			-- 管理項目
			UPD_DT			,
			UPDUSER_CD		,
			UPDUSER_TX		,
			ADD_DT			,
			ADDUSER_CD		,
			ADDUSER_TX		,
			UPDPROGRAM_CD	,
			UPDCOUNTER_NR	,
			STRECORD_ID
		) VALUES (
			iLocaCD 			,
			iSkuCD 				,
			iLotTX 				,
			iKanbanCD			,
			iCasePerPalletNR	,
			iPalletLayerNR		,
			iPalletCapacityNR	,
			iStackingNumberNR	,
			iTypePalletCD		,
			iUseLimitDT 		,
			iBatchNoCD			,
			iStockDT			,
			iAmountNR			,
			iPalletNR			,
			iCaseNR				,
			iBaraNR				,
			iPcsNR				,
			iTypeStockCD		,
			--
			SYSDATE				,
			iUserCD				,
			iUserTX				,
			SYSDATE				,
			iUserCD				,
			iUserTX				,
			iProgramCD			,
			0					,
			PS_DEFINE.ST_RECORD0_NORMAL
		);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsertP;
	/************************************************************************************/
	/*	引当済数・登録																	*/
	/************************************************************************************/
	PROCEDURE InsertC(
		iTrnID				IN NUMBER	,
		iStStockID			IN NUMBER	,
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iCasePerPalletNR	IN NUMBER	,
		iPalletLayerNR		IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,	-- パレット積載数
		iStackingNumberNR	IN NUMBER	,
		iTypePalletCD		IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchNoCD			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2	,
		oLocaStockCID		OUT NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsertC';
		
		numLocaStockCID	TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_LOCASTOCKCID.NEXTVAL INTO numLocaStockCID
		FROM DUAL;
		INSERT INTO TA_LOCASTOCKC (
			LOCASTOCKC_ID		,
			TRN_ID				,
			STSTOCK_ID			,
			LOCA_CD				,
			SKU_CD				,
			LOT_TX				,
			KANBAN_CD			,
			CASEPERPALLET_NR	,
			PALLETLAYER_NR		,
			PALLETCAPACITY_NR	,
			STACKINGNUMBER_NR	,
			TYPEPLALLET_CD		,
			USELIMIT_DT			,
			BATCHNO_CD			,
			STOCK_DT			,
			AMOUNT_NR			,
			PALLET_NR			,
			CASE_NR				,
			BARA_NR				,
			PCS_NR				,
			TYPESTOCK_CD		,
			-- 管理項目
			UPD_DT				,
			UPDUSER_CD			,
			UPDUSER_TX			,
			ADD_DT				,
			ADDUSER_CD			,
			ADDUSER_TX			,
			UPDPROGRAM_CD		,
			UPDCOUNTER_NR		,
			STRECORD_ID
		) VALUES (
			numLocaStockCID		,
			iTrnID				,
			iStStockID			,
			iLocaCD 			,
			iSkuCD 				,
			iLotTX				,
			iKanbanCD			,
			iCasePerPalletNR	,
			iPalletLayerNR		,
			iPalletCapacityNR	,
			iStackingNumberNR	,
			iTypePalletCD		,
			iUseLimitDT 		,
			iBatchNoCD			,
			iStockDT			,
			iAmountNR			,
			iPalletNR			,
			iCaseNR				,
			iBaraNR				,
			iPcsNR				,
			iTypeStockCD		,
			--
			SYSDATE				,
			iUserCD				,
			iUserTX				,
			SYSDATE				,
			iUserCD				,
			iUserTX				,
			iProgramCD			,
			0					,
			PS_DEFINE.ST_RECORD0_NORMAL
		);
		oLocaStockCID := numLocaStockCID;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsertC;
	/************************************************************************************/
	/*	引当済・削除																	*/
	/************************************************************************************/
	PROCEDURE DelC(
		iLocaStockCID		IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DelC';
		numCount	NUMBER := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		DELETE
			TA_LOCASTOCKC
		WHERE
			LOCASTOCKC_ID = iLocaStockCID;
		numCount := SQL%ROWCOUNT;
		IF numCount <> 1 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：更新異常が発生しました。' ||
				'iLocaStockCID = [' || iLocaStockCID || '], numCount = [' || numCount || ']'
			);
		END IF;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END DelC;
	/************************************************************************************/
	/*	引当済数・コピー																*/
	/************************************************************************************/
	PROCEDURE CopyC(
		iLocaStockCID		IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBAraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2	,
		oLocaStockCID		OUT NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CopyC';
		numLocaStockCID	TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_LOCASTOCKCID.NEXTVAL INTO numLocaStockCID
		FROM DUAL;
		INSERT INTO TA_LOCASTOCKC (
			LOCASTOCKC_ID		,
			TRN_ID				,
			STSTOCK_ID			,
			LOCA_CD				,
			SKU_CD				,
			LOT_TX				,
			KANBAN_CD			,
			CASEPERPALLET_NR	,
			PALLETLAYER_NR		,
			PALLETCAPACITY_NR	,
			STACKINGNUMBER_NR	,
			TYPEPLALLET_CD		,
			USELIMIT_DT			,
			BATCHNO_CD			,
			STOCK_DT			,
			AMOUNT_NR			,
			PALLET_NR			,
			CASE_NR				,
			BARA_NR				,
			PCS_NR				,
			TYPESTOCK_CD		,
			-- 管理項目
			UPD_DT				,
			UPDUSER_CD			,
			UPDUSER_TX			,
			ADD_DT				,
			ADDUSER_CD			,
			ADDUSER_TX			,
			UPDPROGRAM_CD		,
			UPDCOUNTER_NR		,
			STRECORD_ID
		) SELECT
			numLocaStockCID		,
			TRN_ID				,
			STSTOCK_ID			,
			LOCA_CD				,
			SKU_CD				,
			LOT_TX				,
			KANBAN_CD			,
			CASEPERPALLET_NR	,
			PALLETLAYER_NR		,
			PALLETCAPACITY_NR	,
			STACKINGNUMBER_NR	,
			TYPEPLALLET_CD		,
			USELIMIT_DT			,
			BATCHNO_CD			,
			STOCK_DT			,
			AMOUNT_NR			,
			iPalletNR			,
			iCaseNR				,
			iBaraNR				,
			iPcsNR				,
			TYPESTOCK_CD		,
			-- 管理項目
			SYSDATE				,
			iUserCD				,
			iUserTX				,
			SYSDATE				,
			iUserCD				,
			iUserTX				,
			iProgramCD			,
			0					,
			PS_DEFINE.ST_RECORD0_NORMAL
		FROM
			TA_LOCASTOCKC
		WHERE
			LOCASTOCKC_ID	= iLocaStockCID;
		oLocaStockCID := numLocaStockCID;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END CopyC;
	/************************************************************************************/
	/*	数量取得(単位別)																*/
	/************************************************************************************/
	PROCEDURE GetUnitPcsNr(
		iSkuCD				IN VARCHAR2	,
		iPcsNR				IN NUMBER	,	--最少単位へ変換した数量（例:1ケース10入りの場合、10）
		iPalletCapacityNR	IN NUMBER	,	--パレット積載数
		oPalletNR			OUT NUMBER	,
		oCaseNR				OUT NUMBER	,
		oBaraNR				OUT NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetUnitPcsNr';
		rowSku			MA_SKU%ROWTYPE;
		numPalletNr		NUMBER;
		numCaseNR		NUMBER;
		numBaraNR		NUMBER;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 初期化
		numPalletNr := 0;
		numCaseNR := 0;
		numBaraNR := 0;
		-- データ取得
		PS_MASTER.GetSku(iSkuCD,rowSku);
		--
		numBaraNR := MOD(iPcsNr, rowSku.PCSPERCASE_NR);
		numCaseNR := MOD((iPcsNr - numBaraNR) / rowSku.PCSPERCASE_NR, iPalletCapacityNR);
		numPalletNr := (((iPcsNr - numBaraNR) / rowSku.PCSPERCASE_NR) - numCaseNR ) / iPalletCapacityNR;
		--
		oPalletNR := numPalletNr;
		oCaseNR := numCaseNR;
		oBaraNR := numBaraNR;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetUnitPcsNr;
	/************************************************************************************/
	/*	数量取得(単位別)																*/
	/************************************************************************************/
	PROCEDURE GetKanban(
		iSkuCD		IN VARCHAR2	,
		iLotTX		IN VARCHAR2	,
		oLocaCD		OUT VARCHAR2,
		oKanbanCD	OUT VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetKanban';
		CURSOR curTarget(SkuCD TA_LOCASTOCKP.SKU_CD%TYPE,LotTX TA_LOCASTOCKP.LOT_TX%TYPE) IS
			SELECT
				LOCA_CD		,
				KANBAN_CD
			FROM
				(
					SELECT
						LOCA_CD	,
						KANBAN_CD
					FROM
						TA_LOCASTOCKP
					WHERE
						SKU_CD		= SkuCD		AND
						LOT_TX		= LotTX
					UNION 
					SELECT
						LOCA_CD	,
						KANBAN_CD
					FROM
						TA_LOCASTOCKC
					WHERE
						SKU_CD		= SkuCD		AND
						LOT_TX		= LotTX		AND
						TYPESTOCK_CD = PS_DEFINE.ST_STOCK1_INAPPLY	-- todo
				)
			ORDER BY
				LOCA_CD;
		rowTarget		curTarget%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		--
		OPEN curTarget(iSkuCD,iLotTX);
		FETCH curTarget INTO rowTarget;
		IF curTarget%FOUND THEN
			oLocaCD := rowTarget.LOCA_CD;
			oKanbanCD := rowTarget.KANBAN_CD;
		ELSE
			oLocaCD := ' ';
			oKanbanCD := ' ';
		END IF;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetKanban;
	/************************************************************************************/
	/*	ロケ間口取得																	*/
	/************************************************************************************/
	PROCEDURE GetLocaNR(
		iLocaCD				IN VARCHAR2	,
		iSkuCD				IN VARCHAR2	,
		oLineUnitNR			OUT NUMBER	,	-- ラインユニット
		oStackingNumberNR	OUT NUMBER	,	-- 保管段数
		oTotalLocaNR		OUT NUMBER	,	-- 総間口
		oUseLocaNR			OUT NUMBER	,	-- 使用間口
		oEmptyLocaNR		OUT NUMBER	,	-- 空き間口
		oBaraFL				OUT NUMBER		-- バラフラグ　1:端数あり、0:完パレのみ
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLocaNR';
		CURSOR curTarget(LocaCD TA_LOCASTOCKP.LOCA_CD%TYPE,SkuCD TA_LOCASTOCKP.SKU_CD%TYPE) IS
			SELECT
				NVL(SUM(LS.PALLET_NR),0) AS PALLET_NR	,
				NVL(SUM(LS.CASE_NR),0) AS CASE_NR		,
				NVL(SUM(LS.BARA_NR),0) AS BARA_NR		,
				NVL(SUM(LS.PCS_NR),0) AS PCS_NR
			FROM
				(
					SELECT
						LP.PALLET_NR	,
						LP.CASE_NR		,
						LP.BARA_NR		,
						LP.PCS_NR
					FROM
						TA_LOCASTOCKP LP
					WHERE
						LP.LOCA_CD		= LocaCD	AND
						LP.SKU_CD		= SkuCD
					UNION 
					SELECT
						SUM(LC.PALLET_NR) AS PALLET_NR	,
						SUM(LC.CASE_NR) AS CASE_NR		,
						SUM(LC.BARA_NR) AS BARA_NR		,
						SUM(LC.PCS_NR) AS PCS_NR
					FROM
						TA_LOCASTOCKC LC
					WHERE
						LC.LOCA_CD		= LocaCD	AND
						LC.SKU_CD		= SkuCD		AND
						LC.TYPESTOCK_CD = PS_DEFINE.ST_STOCK1_INAPPLY	-- todo
				) LS;
		rowTarget		curTarget%ROWTYPE;
		rowLoca			MA_LOCA%ROWTYPE;
		rowSku			MA_SKU%ROWTYPE;
		rowSkuPallet	MA_SKUPALLET%ROWTYPE;
		numLineUnitNR		NUMBER := 0;
		numStackingNumberNR	NUMBER := 0;
		numTotalLocaNR		NUMBER := 0;
		numUseLocaNR		NUMBER := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		--
		PS_MASTER.GetLoca(iLocaCD,rowLoca);
		PS_MASTER.GetSku(iSkuCD,rowSku);
		PS_MASTER.GetSkuPallet(iSkuCD,rowSkuPallet);
		--
		numLineUnitNR := rowLoca.LINEUNIT_NR;
		numStackingNumberNR := rowSkuPallet.STACKINGNUMBER_NR;
		
		OPEN curTarget(iLocaCD,iSkuCD);
		FETCH curTarget INTO rowTarget;
		IF curTarget%FOUND THEN
			-- 総間口
			numTotalLocaNR := numLineUnitNR * numStackingNumberNR;
			IF rowTarget.CASE_NR + rowTarget.BARA_NR >= 1 THEN
				numUseLocaNR := rowTarget.PALLET_NR + 1;
				oBaraFL := 1;
			ELSE
				numUseLocaNR := rowTarget.PALLET_NR;
				oBaraFL := 0;
			END IF;
		ELSE
			numUseLocaNR := 0;
			oBaraFL := 0;
		END IF;
		
		oLineUnitNR := numLineUnitNR;
		oStackingNumberNR := numStackingNumberNR;
		oTotalLocaNR := numTotalLocaNR;
		oUseLocaNR := numUseLocaNR;
		oEmptyLocaNR := numTotalLocaNR - numUseLocaNR;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetLocaNR;
	/************************************************************************************/
	/*	在庫の使用期限（一番長い）														*/
	/************************************************************************************/
	FUNCTION GetUseLimitFar(
		iSkuCD				IN VARCHAR2
	) RETURN DATE AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetUseLimitFar';
		numLocaStockCID	TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE;
		CURSOR curTarget(SkuCD TA_LOCASTOCKP.SKU_CD%TYPE) IS
			SELECT
				USELIMIT_DT
			FROM
				(
					SELECT
						USELIMIT_DT
					FROM
						TA_LOCASTOCKP
					WHERE
						SKU_CD		= SkuCD		AND
						USELIMIT_DT	<> PS_DEFINE.USELIMITDT_DEFAULT
					UNION 
					SELECT
						USELIMIT_DT
					FROM
						TA_LOCASTOCKC
					WHERE
						SKU_CD		= SkuCD		AND
						USELIMIT_DT	<> PS_DEFINE.USELIMITDT_DEFAULT
				)
			ORDER BY
				USELIMIT_DT DESC;
		rowTarget		curTarget%ROWTYPE;
		oResult			DATE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START, ' ||
			'iSkuCD=[' || iSkuCD || ']'
		);
		OPEN curTarget(iSkuCD);
		FETCH curTarget INTO rowTarget;
		IF curTarget%FOUND THEN
			oResult := rowTarget.USELIMIT_DT;
		ELSE
			oResult := PS_DEFINE.USELIMITDT_DEFAULT;
		END IF;
		CLOSE curTarget;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END, ' ||
			'oResult=[' || oResult || ']'
		);
		RETURN oResult;
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTarget%ISOPEN THEN
				CLOSE curTarget;
			END IF;
			RAISE;
	END GetUseLimitFar;
	
	
	
	
	/****************************************************/
	/*													*/
	/*		入庫										*/
	/*													*/
	/****************************************************/
	/************************************************************************************/
	/*	入庫チェック																	*/
	/************************************************************************************/
	PROCEDURE ChkInStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2
	) AS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'ChkInStock';
		
		rowLoca				MA_LOCA%ROWTYPE;
		rowTypeLoca			MB_TYPELOCA%ROWTYPE;
		rowTypeLocaBlock	MB_TYPELOCABLOCK%ROWTYPE;
		rowSku				MA_SKU%ROWTYPE;
		rowLocaStockP		TA_LOCASTOCKP%ROWTYPE;
		rowLocaStockC		TA_LOCASTOCKC%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-----------------------------------------------------------------------
		--マスター取得
		-----------------------------------------------------------------------
		-- ロケーションロック
		PS_LOCA.GetLocaLock(iLocaCD);
		-- ロケーション取得
		PS_MASTER.GetLoca(iLocaCD, rowLoca);
		-- ロケーション関連データの取得
		PS_MASTER.GetTypeLoca(rowLoca.TYPELOCA_CD, rowTypeLoca);
		PS_MASTER.GetTypeLocaBlock(rowLoca.TYPELOCABLOCK_CD, rowTypeLocaBlock);
		-- ＳＫＵ取得
		PS_MASTER.GetSku(iSkuCD, rowSku);
		-- 引当可能数削除	--TODO ここでやる必要あるか
		DELETE TA_LOCASTOCKP
		WHERE
			LOCA_CD = iLocaCD AND
			PCS_NR	= 0;
		-- ロケーション引当可能数取得（一行に特定される。存在しない場合は0データ。複数行存在する場合はエラー。）
		PS_LOCA.GetLocaStockP(iLocaCD, rowLocaStockP);
		-- ロケーション引当済み数取得（複数行の一行のみ取得される。存在しない場合は0データ。）
		PS_LOCA.GetLocaStockC(iLocaCD, rowLocaStockC);
		--------------------------------------------------------------
		-- ＜既存引当可能在庫との整合性チェック＞
		-- ＳＫＵ・在庫状態・使用期限
		--------------------------------------------------------------
		IF rowLocaStockP.PCS_NR > 0 THEN
			-----------------------
			-- ＳＫＵチェック
			-----------------------
			IF rowLocaStockP.SKU_CD != iSkuCD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1W_NL_NO, 'ロケーション[' || iLocaCD || ']は引当可能在庫のＳＫＵ[' || rowLocaStockP.SKU_CD || ']' ||
																	'と入庫ＳＫＵ[' || iSkuCD || ']がちがいます。');
			END IF;
			-----------------------
			-- 既存ロケ在庫状態チェック
			-----------------------
			IF rowLoca.TYPELOCA_CD != rowLocaStockP.TYPESTOCK_CD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1V_NL_NO, 'ロケーション[' || iLocaCD || ']はロケーション在庫状態[' || rowLoca.TYPELOCA_CD || ']' ||
																		'と引当可能在庫状態[' || rowLocaStockP.TYPESTOCK_CD || ']がちがいます。');
			END IF;
		END IF;
		--------------------------------------------------------------
		-- ＜既存引当済み在庫との整合性チェック＞
		-- ＳＫＵ・使用期限
		--------------------------------------------------------------
		IF rowLocaStockC.PCS_NR > 0 THEN
			-----------------------
			-- ＳＫＵチェック
			-----------------------
			IF rowLocaStockC.SKU_CD != iSkuCD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1W_NL_NO, 'ロケーション[' || iLocaCD || ']は引当済み在庫のＳＫＵ[' || rowLocaStockC.SKU_CD || ']' ||
																	'と入庫ＳＫＵ[' || iSkuCD || ']がちがいます。');
			END IF;
			-----------------------
			-- 既存ロケ在庫状態チェック
			-----------------------
			IF rowLoca.TYPELOCA_CD != rowLocaStockC.TYPESTOCK_CD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1V_NL_NO, 'ロケーション[' || iLocaCD || ']はロケーション在庫状態[' || rowLoca.TYPELOCA_CD || ']' ||
																	'と引当可能在庫状態[' || rowLocaStockC.TYPESTOCK_CD || ']がちがいます。');
			END IF;
		END IF;
		------------------------------------
		-- ＜ＰＣＳ０以上チェック＞
		------------------------------------
		IF iPcsNR <= 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1V_NL_NO, '入庫ＰＣＳが０以下です。ロケーション[' || iLocaCD || ']');
		END IF;
		------------------------------------
		-- ＜在庫状態チェック＞
		------------------------------------
		IF rowLoca.TYPELOCA_CD != iTypeStockCD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1V_NL_NO, 'ロケーション[' || iLocaCD || ']はロケーション在庫状態[' || rowLoca.TYPELOCA_CD || ']' ||
																	'と入庫在庫状態[' || iTypeStockCD || ']がちがいます。');
		END IF;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ChkInStock;
	/************************************************************************************/
	/*	入庫							在庫登録										*/
	/************************************************************************************/
	PROCEDURE InStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iCASEPERPALLET_NR	IN NUMBER	,
		iPALLETLAYER_NR		IN NUMBER	,
		iPALLETCAPACITY_NR	IN NUMBER	,
		iSTACKINGNUMBER_NR	IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iTypeLocaCD			IN VARCHAR2	,
		iTypeOperationCD	IN VARCHAR2	,
		iTypeRefCD			IN VARCHAR2 ,
		iRefNoID			IN NUMBER	,
		iMemoTx				IN VARCHAR2	,
		iNeedSendFL			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InStock';
		
		CURSOR curLocaStockP(LocaCd TA_LOCASTOCKP.LOCA_CD%TYPE,
								SkuCd TA_LOCASTOCKP.SKU_CD%TYPE,
								LotTXx TA_LOCASTOCKP.LOT_TX%TYPE,
								UseLimitDt TA_LOCASTOCKP.USELIMIT_DT%TYPE,
								StockDt TA_LOCASTOCKP.STOCK_DT%TYPE) IS
			SELECT
				ROWID
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD 		= LocaCd		AND
				SKU_CD			= SkuCd			AND
				LOT_TX			= LotTXx		AND
				USELIMIT_DT		= UseLimitDt	AND
				STOCK_DT		= StockDt
			FOR UPDATE;
		rowLocaStockP	curLocaStockP%ROWTYPE;
		--
		rowLoca			MA_LOCA%ROWTYPE;
		rowTypeLoca		MB_TYPELOCA%ROWTYPE;
		rowSku			MA_SKU%ROWTYPE;
		-- rowSkuPallet	MA_SKUPALLET%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- ロケーションロック
		PS_LOCA.GetLocaLock(iLocaCD);
		--
		PS_MASTER.GetSku(iSkuCD,rowSku);
		--
		OPEN curLocaStockP(	iLocaCD		,
							iSkuCD		,
							iLotTX		,
							iUseLimitDT	,
							iStockDT
							);
		FETCH curLocaStockP INTO rowLocaStockP;
		IF curLocaStockP%FOUND THEN
			-- 引当可能数・更新
			PS_STOCK.UpdateP(	rowLocaStockP.ROWID			,
								iAmountNR					,
								iPALLETCAPACITY_NR			,
								iPalletNR					,
								iCaseNR 					,
								iBaraNR						,
								iPcsNR						,
								iUserCD						,
								iUserTX						,
								iProgramCD);
		ELSE
			-- 引当可能数・登録
			PS_STOCK.InsertP(	iLocaCD 					,
								iSkuCD						,
								iLotTX						,
								iKanbanCD					,
								iCASEPERPALLET_NR			,
								iPALLETLAYER_NR				,
								iPALLETCAPACITY_NR			,
								iSTACKINGNUMBER_NR			,
								'01'						,	-- todo iTypePalletCD
								iUseLimitDT					,
								' '		 					,
								iStockDT					,
								iAmountNR					,
								iPalletNR					,
								iCaseNR 					,
								iBaraNR						,
								iPcsNR						,
								iTypeStockCD				,
								iUserCD						,
								iUserTX						,
								iProgramCD);
		END IF;
		CLOSE curLocaStockP;
		-- 在庫受払
		PS_STOCK.InsStockInOut(	' '							,
								' '							,
								iLocaCD						,
								iSkuCD						,
								iLotTX						,
								iKanbanCD					,
								iCASEPERPALLET_NR			,
								iPALLETLAYER_NR				,
								iPALLETCAPACITY_NR			,
								iSTACKINGNUMBER_NR			,
								' '							,	-- iTypePalletCD
								iUseLimitDT					,
								iBatchnoCd 					,
								iStockDT					,
								iAmountNR					,
								iPalletNR					,
								iCaseNR 					,
								iBaraNR						,
								iPcsNR						,
								iTypeStockCD				,
								iTypeLocaCD					,
								iTypeOperationCD			,
								iTypeRefCD					,
								iRefNoID  					,
								iMemoTx						,
								iNeedSendFL					,
								iUserCD						,
								iUserTX						,
								iProgramCD
							);
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InStock;
	/************************************************************************************/
	/*	入庫（ダイレクト入庫）															*/
	/************************************************************************************/
	PROCEDURE InStockDirectInStock(
		iSrcLocaStockCID	IN NUMBER	,
		iInStockID			IN VARCHAR2 ,	--iRefNoID
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2	
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InStockDirectInStock';
		CURSOR curLocaStockP(
			LocaCd TA_LOCASTOCKP.LOCA_CD%TYPE,
			SkuCd TA_LOCASTOCKP.SKU_CD%TYPE
		) IS
			SELECT
				ROWID
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD 		= LocaCd		AND
				SKU_CD			= SkuCd			
			FOR UPDATE;
		rowLocaStockP 	curLocaStockP%ROWTYPE;
		--
		rowLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		rowLoca			MA_LOCA%ROWTYPE;
		rowTypeLoca		MB_TYPELOCA%ROWTYPE;
		newPcsNR		TA_LOCASTOCKC.PCS_NR%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 引当済み在庫
		PS_LOCA.GetLockLocaStockC(iSrcLocaStockCID, rowLocaStockC);
		-- ロケーションロック
		PS_LOCA.GetLocaLock(rowLocaStockC.LOCA_CD);
		-- ロケーション取得
		PS_MASTER.GetLoca(rowLocaStockC.LOCA_CD, rowLoca);
		-- 引当済み在庫更新
		DelC(iSrcLocaStockCID, iUserCD, iUserTX, iProgramCD);
		-- 引当可能数更新
		OPEN curLocaStockP(
			rowLocaStockC.LOCA_CD,
			rowLocaStockC.SKU_CD
		);
		FETCH curLocaStockP INTO rowLocaStockP;
		IF curLocaStockP%FOUND THEN
			-- 引当可能数・更新
			PS_STOCK.UpdateP(
				rowLocaStockP.ROWID			,
				rowLocaStockC.AMOUNT_NR		,
				rowLocaStockC.PALLETCAPACITY_NR,
				rowLocaStockC.PALLET_NR		,
				rowLocaStockC.CASE_NR		,
				rowLocaStockC.BARA_NR		,
				rowLocaStockC.PCS_NR		,
				iUserCD						,
				iUserTX						,
				iProgramCD
			);
		ELSE
			-- 引当可能数・登録
			PS_STOCK.InsertP(
				rowLocaStockC.LOCA_CD		,
				rowLocaStockC.SKU_CD		,
				rowLocaStockC.LOT_TX		,
				rowLocaStockC.KANBAN_CD		,
				rowLocaStockC.CASEPERPALLET_NR,
				rowLocaStockC.PALLETLAYER_NR,
				rowLocaStockC.PALLETCAPACITY_NR,
				rowLocaStockC.STACKINGNUMBER_NR,
				rowLocaStockC.TYPEPLALLET_CD,
				rowLocaStockC.USELIMIT_DT	,
				rowLocaStockC.BATCHNO_CD	,
				rowLocaStockC.STOCK_DT		,
				rowLocaStockC.AMOUNT_NR		,
				rowLocaStockC.PALLET_NR		,
				rowLocaStockC.CASE_NR		,
				rowLocaStockC.BARA_NR		,
				rowLocaStockC.PCS_NR		,
				rowLocaStockC.TYPESTOCK_CD	,
				iUserCD						,
				iUserTX						,
				iProgramCD
			);
		END IF;
		CLOSE curLocaStockP;
		-- 受払は検品時に作成
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InStockDirectInStock;
	
	
	
	
	/****************************************************/
	/*													*/
	/*		出庫										*/
	/*													*/
	/****************************************************/
	/************************************************************************************/
	/*	在庫出庫（引当済数から一時引当済み）											*/
	/************************************************************************************/
	PROCEDURE OutStockTemp(
		iTrnID			IN NUMBER	,
		iTempLocaCD		IN VARCHAR2	,
		iTempKanbanCD	IN VARCHAR2	,
		iLocaStockCID	IN NUMBER	,
		iAmountNR		IN NUMBER	,
		iPalletNR		IN NUMBER	,
		iCaseNR			IN NUMBER	,
		iBaraNR			IN NUMBER	,
		iPcsNR			IN NUMBER	,
		iStStockID		IN NUMBER	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		orowLocaStockC	OUT TA_LOCASTOCKC%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'OutStockTemp';
		
		rowLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		rowLoca			MA_LOCA%ROWTYPE;
		rowTypeLoca		MB_TYPELOCA%ROWTYPE;
		numLocaStockCID	TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 引当済取得
		PS_LOCA.GetLockLocaStockC(iLocaStockCID, rowLocaStockC);
		-- ロケーション取得
		PS_MASTER.GetLoca(rowLocaStockC.LOCA_CD, rowLoca);
		-- ロケーションタイプ取得
		PS_MASTER.GetTypeLoca(rowLoca.TYPELOCA_CD, rowTypeLoca);
		-- 引当済み在庫引き落とし
		PS_STOCK.DelC(iLocaStockCID		,
						iUserCD			,
						iUserTX			,
						FUNCTION_NAME);
		-- 引当済数追加
		PS_STOCK.InsertC(iTrnID						,
						iStStockID					,
						iTempLocaCD					,
						rowLocaStockC.SKU_CD		,
						rowLocaStockC.LOT_TX		,
						iTempKanbanCD				,
						rowLocaStockC.CASEPERPALLET_NR,
						rowLocaStockC.PALLETLAYER_NR,
						rowLocaStockC.PALLETCAPACITY_NR,
						rowLocaStockC.STACKINGNUMBER_NR,
						rowLocaStockC.TYPEPLALLET_CD,
						rowLocaStockC.USELIMIT_DT	,
						rowLocaStockC.BATCHNO_CD	,
						rowLocaStockC.STOCK_DT		,
						iAmountNR					,
						iPalletNR					,
						iCaseNR 					,
						iBaraNR						,
						iPcsNR						,
						rowTypeLoca.TYPELOCA_CD		,
						iUserCD						,
						iUserTX						,
						FUNCTION_NAME				,
						numLocaStockCID);
		PLOG.INFO(logCtx, 'numLocaStockCID = ' || numLocaStockCID);
		-- 引当済取得
		PS_LOCA.GetLockLocaStockC(numLocaStockCID,orowLocaStockC);
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END OutStockTemp;
	/************************************************************************************/
	/*	在庫出庫（一時引当済みから引当済数）											*/
	/************************************************************************************/
	PROCEDURE InStockTempOut(
		iTrnID			IN NUMBER	,
		iTempLocaCD		IN VARCHAR2	,
		iTempKanbanCD	IN VARCHAR2	,
		iLocaStockCID	IN NUMBER	,
		iAmountNR		IN NUMBER	,
		iPalletNR		IN NUMBER	,
		iCaseNR			IN NUMBER	,
		iBaraNR			IN NUMBER	,
		iPcsNR			IN NUMBER	,
		iStStockID		IN NUMBER	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		orowLocaStockC	OUT TA_LOCASTOCKC%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InStockTempOut';
		
		rowLocaStockC		TA_LOCASTOCKC%ROWTYPE;
		rowLoca				MA_LOCA%ROWTYPE;
		rowTypeLoca			MB_TYPELOCA%ROWTYPE;
		numLocaStockCID		TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 一時引当済取得（ロケなし）
		PS_LOCA.GetLockLocaStockC(iLocaStockCID, rowLocaStockC);
		-- 引当済み在庫引き落とし
		PS_STOCK.DelC(iLocaStockCID		,
						iUserCD			,
						iUserTX			,
						FUNCTION_NAME);
		--ロケーション取得（新ロケ）
		PS_MASTER.GetLoca(iTempLocaCD, rowLoca);
		--ロケーションタイプ取得
		PS_MASTER.GetTypeLoca(rowLoca.TYPELOCA_CD, rowTypeLoca);
		--引当済数追加
		PS_STOCK.InsertC(iTrnID						,
						iStStockID					,
						iTempLocaCD					,
						rowLocaStockC.SKU_CD		,
						rowLocaStockC.LOT_TX		,
						iTempKanbanCD				,
						rowLocaStockC.CASEPERPALLET_NR,
						rowLocaStockC.PALLETLAYER_NR	,
						rowLocaStockC.PALLETCAPACITY_NR,
						rowLocaStockC.STACKINGNUMBER_NR,
						rowLocaStockC.TYPEPLALLET_CD,
						rowLocaStockC.USELIMIT_DT	,
						rowLocaStockC.BATCHNO_CD	,
						rowLocaStockC.STOCK_DT		,
						iAmountNR					,
						iPalletNR					,
						iCaseNR 					,
						iBaraNR						,
						iPcsNR						,
						rowTypeLoca.TYPELOCA_CD		,
						iUserCD						,
						iUserTX						,
						FUNCTION_NAME				,
						numLocaStockCID);
		PLOG.INFO(logCtx, 'numLocaStockCID = '||numLocaStockCID);					
		--引当済取得
		PS_LOCA.GetLockLocaStockC(numLocaStockCID,orowLocaStockC);
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InStockTempOut;
	/************************************************************************************/
	/*	入庫（受払なし）			過剰補充格納用										*/
	/************************************************************************************/
	PROCEDURE InStockNoResult(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iCaseperPalletNR	IN NUMBER	,
		iPalletLayerNR		IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,
		iStackingNumberNR	IN NUMBER	,
		iTypePalletCD		IN VARCHAR2	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InStockNoResult';
		
		CURSOR curLocaStockP(LocaCd TA_LOCASTOCKP.LOCA_CD%TYPE,
								SkuCd TA_LOCASTOCKP.SKU_CD%TYPE,   --表「TA_LOCASTOCKP」の列「SKU_CD」の型で、変数”SkuCd”を定義する。
								LotTXx TA_LOCASTOCKP.LOT_TX%TYPE,
								UseLimitDt TA_LOCASTOCKP.USELIMIT_DT%TYPE,
								StockDt TA_LOCASTOCKP.STOCK_DT%TYPE) IS
			SELECT
				ROWID
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD 		= LocaCd		AND
				SKU_CD			= SkuCd			AND
				LOT_TX			= LotTXx		AND
				USELIMIT_DT		= UseLimitDt	AND
				STOCK_DT		= StockDt
			FOR UPDATE;
		rowLocaStockP	curLocaStockP%ROWTYPE;
		--
		rowLoca			MA_LOCA%ROWTYPE;
		rowTypeLoca		MB_TYPELOCA%ROWTYPE;
		rowSku			MA_SKU%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- ロケーションロック
		PS_LOCA.GetLocaLock(iLocaCD);
		--
		PS_MASTER.GetSku(iSkuCD,rowSku);
		--
		OPEN curLocaStockP(	iLocaCD		,
							iSkuCD		,
							iLotTX		,
							iUseLimitDT	,
							iStockDT
							);
		FETCH curLocaStockP INTO rowLocaStockP;
		IF curLocaStockP%FOUND THEN
			-- 引当可能数・更新
			PS_STOCK.UpdateP(	rowLocaStockP.ROWID			,
								iAmountNR					,
								iPalletCapacityNR			,
								iPalletNR					,
								iCaseNR 					,
								iBaraNR						,
								iPcsNR						,
								iUserCD						,
								iUserTX						,
								iProgramCD);
		ELSE
			-- 引当可能数・登録
			PS_STOCK.InsertP(	iLocaCD 					,
								iSkuCD						,
								iLotTX						,
								iKanbanCD					,
								iCaseperPalletNR			,
								iPalletLayerNR				,
								iPalletCapacityNR			,
								iStackingNumberNR			,
								iTypePalletCD				,	-- todo iTypePalletCD
								iUseLimitDT					,
								' '		 					,
								iStockDT					,
								iAmountNR					,
								iPalletNR					,
								iCaseNR 					,
								iBaraNR						,
								iPcsNR						,
								iTypeStockCD				,
								iUserCD						,
								iUserTX						,
								iProgramCD);
		END IF;
		CLOSE curLocaStockP;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InStockNoResult;
	
	
	/****************************************************/
	/*													*/
	/*		在庫調整									*/
	/*													*/
	/****************************************************/
	/************************************************************************************/
	/*	在庫変更チェック																*/
	/************************************************************************************/
	PROCEDURE ChkUpdStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2
	) AS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'ChkUpdStock';
		rowLoca				MA_LOCA%ROWTYPE;
		rowTypeLoca			MB_TYPELOCA%ROWTYPE;
		rowTypeLocaBlock	MB_TYPELOCABLOCK%ROWTYPE;
		rowSku				MA_SKU%ROWTYPE;
		rowLocaStockP		TA_LOCASTOCKP%ROWTYPE;
		rowLocaStockC		TA_LOCASTOCKC%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-----------------------------------------------------------------------
		--マスター取得
		-----------------------------------------------------------------------
		-- ロケーションロック
		PS_LOCA.GetLocaLock(iLocaCD);
		-- ロケーション取得
		PS_MASTER.GetLoca(iLocaCD,rowLoca);
		-- ロケーション関連データの取得
		PS_MASTER.GetTypeLoca(rowLoca.TYPELOCA_CD,rowTypeLoca);
		PS_MASTER.GetTypeLocaBlock(rowLoca.TYPELOCABLOCK_CD,rowTypeLocaBlock);
		-- ＳＫＵ取得
		PS_MASTER.GetSku(iSkuCD,rowSku);
		-- 引当可能数削除	--TODO ここでやる必要あるか
		DELETE TA_LOCASTOCKP
		WHERE
			LOCA_CD = iLocaCD AND
			PCS_NR	= 0;
		-- ロケーション引当可能数取得（一行に特定される。存在しない場合は0データ。複数行存在する場合はエラー。）
		PS_LOCA.GetLocaStockP(iLocaCD,rowLocaStockP);
		-- ロケーション引当済み数取得（複数行の一行のみ取得される。存在しない場合は0データ。）
		PS_LOCA.GetLocaStockC(iLocaCD,rowLocaStockC);
		--------------------------------------------------------------
		-- ＜既存引当可能在庫との整合性チェック＞
		-- ＳＫＵ・在庫状態・使用期限
		--------------------------------------------------------------
		IF rowLocaStockP.PCS_NR > 0 THEN
			-----------------------
			-- ＳＫＵチェック
			-----------------------
			IF rowLocaStockP.SKU_CD != iSkuCD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1W_NL_NO, 'ロケーション[' || iLocaCD || ']は引当可能在庫のＳＫＵ[' || rowLocaStockP.SKU_CD || ']' ||
																	'と入庫ＳＫＵ[' || iSkuCD || ']がちがいます。');
			END IF;
			-----------------------
			-- 既存ロケ在庫状態チェック
			-----------------------
			IF rowLoca.TYPELOCA_CD != rowLocaStockP.TYPESTOCK_CD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1V_NL_NO, 'ロケーション[' || iLocaCD || ']はロケーション在庫状態[' || rowLoca.TYPELOCA_CD || ']' ||
																	'と引当可能在庫状態[' || rowLocaStockP.TYPESTOCK_CD || ']がちがいます。');
			END IF;
		END IF;
		--------------------------------------------------------------
		-- ＜既存引当済み在庫との整合性チェック＞
		-- ＳＫＵ・使用期限
		--------------------------------------------------------------
		IF rowLocaStockC.PCS_NR > 0 THEN
			-----------------------
			-- ＳＫＵチェック
			-----------------------
			IF rowLocaStockC.SKU_CD != iSkuCD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1W_NL_NO, 'ロケーション[' || iLocaCD || ']は引当済み在庫のＳＫＵ[' || rowLocaStockC.SKU_CD || ']' ||
																	'と入庫ＳＫＵ[' || iSkuCD || ']がちがいます。');
			END IF;
			-----------------------
			-- 既存ロケ在庫状態チェック
			-----------------------
			IF rowLoca.TYPELOCA_CD != rowLocaStockC.TYPESTOCK_CD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1V_NL_NO, 'ロケーション[' || iLocaCD || ']はロケーション在庫状態[' || rowLoca.TYPELOCA_CD || ']' ||
																	'と引当可能在庫状態[' || rowLocaStockC.TYPESTOCK_CD || ']がちがいます。');
			END IF;
		END IF;
		------------------------------------
		-- ＜変更後ＰＣＳ０以上チェック＞
		------------------------------------
		IF iPcsNR <= 0 THEN
			IF rowLocaStockP.PCS_NR + iPcsNR < 0 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1h_NS_NO, 'ロケーション[' || iLocaCD || ']は引当可能数が足りません。' ||
																	'引当可能数[' || rowLocaStockP.PCS_NR || '] 変更ＰＣＳ数[' || iPcsNR || ']');
			END IF;
		END IF;
		------------------------------------
		--＜在庫状態チェック＞
		------------------------------------
		IF rowLoca.TYPELOCA_CD != iTypeStockCD THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT1f_NS_NO, 'ロケーション[' || iLocaCD || ']はロケーション在庫状態[' || rowLoca.TYPELOCA_CD || ']' ||
																	'と入庫在庫状態[' || iTypeStockCD || ']がちがいます。在庫修正では在庫状態の変更ができません。');
		END IF;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ChkUpdStock;
	/************************************************************************************/
	/*	在庫変更																		*/
	/************************************************************************************/
	PROCEDURE UpdStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX 				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR 			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iTypeLocaCD			IN VARCHAR2	,
		iTypeOperationCD	IN VARCHAR2	,
		iTypeRefCD			IN VARCHAR2 ,
		iRefNoID			IN NUMBER	,
		iMemoTx				IN VARCHAR2	,
		iNeedSendFL			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdStock';
		
		CURSOR curLocaStockP(LocaCd TA_LOCASTOCKP.LOCA_CD%TYPE,
								SkuCd TA_LOCASTOCKP.SKU_CD%TYPE,
								LotTx TA_LOCASTOCKP.LOT_TX%TYPE,
								UseLimitDt TA_LOCASTOCKP.USELIMIT_DT%TYPE,
								BatchnoCd TA_LOCASTOCKP.BATCHNO_CD%TYPE,
								StockDt TA_LOCASTOCKP.STOCK_DT%TYPE) IS
			SELECT
				ROWID				,
				CASEPERPALLET_NR	,
				PALLETLAYER_NR		,
				PALLETCAPACITY_NR	,
				STACKINGNUMBER_NR	,
				TYPEPLALLET_CD		,
				PCS_NR
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD 		= LocaCd		AND
				SKU_CD			= SkuCd			AND
				LOT_TX			= LotTx			AND
				USELIMIT_DT		= UseLimitDt 	AND
				BATCHNO_CD		= BatchnoCd		AND
				STOCK_DT		= StockDt
			FOR UPDATE;
		rowLocaStockP 	curLocaStockP%ROWTYPE;
		
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME ||
					' iLocaCD=[' || iLocaCD || '], ' ||
					'iSkuCD=[' || iSkuCD || '], ' ||
					'iLotTX=[' || iLotTX || '], ' ||
					'iKanbanCD=[' || iKanbanCD || '], ' ||
					'iUseLimitDT=[' || iUseLimitDT || '], ' ||
					'iBatchnoCd=[' || iBatchnoCd || '], ' ||
					'iStockDT=[' || iStockDT || '], ' ||
					'iAmountNR=[' || iAmountNR || '], ' ||
					'iPalletNR=[' || iPalletNR || '], ' ||
					'iCaseNR=[' || iCaseNR || '], ' ||
					'iBaraNR=[' || iBaraNR || '], ' ||
					'iPcsNR=[' || iPcsNR || '], ' ||
					'iTypeStockCD=[' || iTypeStockCD || '], ' ||
					'iTypeLocaCD=[' || iTypeLocaCD || '], ' ||
					'iTypeRefCD=[' || iTypeRefCD || '], ' ||
					'iRefNoID=[' || iRefNoID || '], ' ||
					'iMemoTX=[' || iMemoTX || '] '
					);
		-- ロケーションロック
		PS_LOCA.GetLocaLock(iLocaCD);
		--
		OPEN curLocaStockP(iLocaCD		,
							iSkuCD		,
							iLotTX		,
							iUseLimitDT	,
							iBatchnoCd	,
							iStockDT);
		FETCH curLocaStockP INTO rowLocaStockP;
		IF curLocaStockP%FOUND THEN
			IF rowLocaStockP.PCS_NR + iPcsNR >= 0 THEN
				-- 引当可能数更新
				PS_STOCK.UpdateP(rowLocaStockP.ROWID,
								iAmountNR			,
								rowLocaStockP.PALLETCAPACITY_NR,
								iPalletNR			,
								iCaseNR 			,
								iBaraNR				,
								iPcsNR				,
								iUserCD				,
								iUserTX				,
								iProgramCD			);
			ELSE
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '修正在庫数量が足りません。 ロケーション = ' || iLocaCD || ' ＳＫＵ = ' || iSkuCD);
			END IF;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '在庫がありません。 ロケーション = ' || iLocaCD || ' ＳＫＵ = ' || iSkuCD);
		END IF;
		CLOSE curLocaStockP;
		-- 在庫受払
		PS_STOCK.InsStockInOut(	' '							,
								' '							,
								iLocaCD						,
								iSkuCD						,
								iLotTX						,
								iKanbanCD					,
								rowLocaStockP.CASEPERPALLET_NR	,
								rowLocaStockP.PALLETLAYER_NR	,
								rowLocaStockP.PALLETCAPACITY_NR	,
								rowLocaStockP.STACKINGNUMBER_NR	,
								rowLocaStockP.TYPEPLALLET_CD,
								iUseLimitDT					,
								iBatchnoCd 					,
								iStockDT					,
								iAmountNR					,
								iPalletNR					,
								iCaseNR 					,
								iBaraNR						,
								iPcsNR						,
								iTypeStockCD				,
								iTypeLocaCD					,
								iTypeOperationCD			,
								iTypeRefCD					,
								iRefNoID  					,
								iMemoTx						,
								iNeedSendFL					,
								iUserCD						,
								iUserTX						,
								iProgramCD);
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN
				CLOSE curLocaStockP;
			END IF;
			RAISE;
	END UpdStock;
	
	
	
	
	/****************************************************/
	/*													*/
	/*		在庫引当									*/
	/*													*/
	/****************************************************/
	/************************************************************************************/
	/*	在庫引当																		*/
	/************************************************************************************/
	PROCEDURE ApplyStock(
		iTrnID				IN NUMBER	,
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iStStockID			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		orowLocaStockC		OUT TA_LOCASTOCKC%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyStock';
		
		CURSOR curLocaStockP(LocaCd		TA_LOCASTOCKP.LOCA_CD%TYPE,
							SkuCd		TA_LOCASTOCKP.SKU_CD%TYPE,
							LotTX		TA_LOCASTOCKP.LOT_TX%TYPE,
							UseLimitDt	TA_LOCASTOCKP.USELIMIT_DT%TYPE,
							BatchnoCd		TA_LOCASTOCKP.BATCHNO_CD%TYPE,
							TypeStockCD	TA_LOCASTOCKP.TYPESTOCK_CD%TYPE) IS
			SELECT
				ROWID			,
				LOCA_CD			,
				SKU_CD			,
				LOT_TX			,
				KANBAN_CD		,
				CASEPERPALLET_NR,
				PALLETLAYER_NR	,
				PALLETCAPACITY_NR,
				STACKINGNUMBER_NR,
				TYPEPLALLET_CD	,
				USELIMIT_DT		,
				BATCHNO_CD		,
				STOCK_DT		,
				AMOUNT_NR		,
				PALLET_NR		,
				CASE_NR			,
				BARA_NR			,
				PCS_NR			,
				TYPESTOCK_CD
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD 		= LocaCd		AND
				SKU_CD			= SkuCd			AND
				LOT_TX			= LotTX			AND
				USELIMIT_DT		= UseLimitDt 	AND
				TYPESTOCK_CD	= TypeStockCd	AND
				STRECORD_ID		= PS_DEFINE.ST_RECORD0_NORMAL
			FOR UPDATE;
		rowLocaStockP 	curLocaStockP%ROWTYPE;
		--
		chrBatchNoCD	TA_LOCASTOCKC.BATCHNO_CD%TYPE	:= ' ';
		numPalletNR		TA_LOCASTOCKC.PALLET_NR%TYPE	:= 0;
		numCaseNR		TA_LOCASTOCKC.CASE_NR%TYPE		:= 0;
		numBaraNR		TA_LOCASTOCKC.BARA_NR%TYPE		:= 0;
		numPcsNR		TA_LOCASTOCKC.PCS_NR%TYPE 		:= 0;
		numLocaStockCID	TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curLocaStockP(iLocaCD, iSkuCD, iLotTX, iUseLimitDT, iBatchnoCd, iTypeStockCD);
		FETCH curLocaStockP INTO rowLocaStockP;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME ||  'iLocaCD=[' || iLocaCD || '],' ||
																	'iSkuCD=[' || iSkuCD || '], ' ||
																	'iLotTX=[' || iLotTX || '], ' ||
																	'iUseLimitDT=[' || iUseLimitDT || '], ' ||
																	'iBatchnoCd=[' || iBatchnoCd || '], ' ||
																	'iTypeStockCD=[' || iTypeStockCD || ']'
																);
		IF curLocaStockP%FOUND THEN
			IF rowLocaStockP.PCS_NR >= iPcsNR THEN
				--引当可能数減算
				PS_STOCK.UpdateP(
					rowLocaStockP.ROWID,
					iAmountNR			,
					iPalletCapacityNR	,
					(iPalletNR * -1)	,
					(iCaseNR * -1)		,
					(iBaraNR * -1)		,
					(iPcsNR * -1)		,
					iUserCD				,
					iUserTX				,
					FUNCTION_NAME
				);
				IF TRIM(iBatchnoCd) IS NULL THEN
					chrBatchNoCD := rowLocaStockP.BATCHNO_CD;
				ELSE
					chrBatchNoCD := iBatchnoCd;
				END IF;
				--引当済数追加
				PS_STOCK.InsertC(
						iTrnID						,
						iStStockID					,
						rowLocaStockP.LOCA_CD		,
						rowLocaStockP.SKU_CD		,
						rowLocaStockP.LOT_TX		,
						rowLocaStockP.KANBAN_CD		,
						rowLocaStockP.CASEPERPALLET_NR,
						rowLocaStockP.PALLETLAYER_NR,
						rowLocaStockP.PALLETCAPACITY_NR,
						rowLocaStockP.STACKINGNUMBER_NR,
						rowLocaStockP.TYPEPLALLET_CD,
						rowLocaStockP.USELIMIT_DT	,
						chrBatchNoCD				,
						rowLocaStockP.STOCK_DT		,
						iAmountNR					,
						iPalletNR					,
						iCaseNR 					,
						iBaraNR						,
						iPcsNR						,
						rowLocaStockP.TYPESTOCK_CD	,
						iUserCD						,
						iUserTX						,
						FUNCTION_NAME				,
						numLocaStockCID
					);
				--引当済取得
				PS_LOCA.GetLockLocaStockC(numLocaStockCID,orowLocaStockC);
			ELSE
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '引当可能数が足りません。 ロケーション = ' || iLocaCD || ' ＳＫＵ = ' || iSkuCD ||
																	 ' 引当数 = ' || iPcsNR || ' 引当可能数 = ' || rowLocaStockP.PCS_NR);
			END IF;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '引当ロケーションがありません。 ロケーション = ' || iLocaCD || ' ＳＫＵ = ' || iSkuCD);
		END IF;
		CLOSE curLocaStockP;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyStock;
	/************************************************************************************/
	/*	在庫戻し																		*/
	/************************************************************************************/
	PROCEDURE ReApplyStock(
		iLocaStockCID	IN NUMBER		,
		iAmountNR		IN NUMBER		,	--最新の入り数
		iUserCD			IN VARCHAR2		,
		iUserTX			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ReApplyStock';
		
		CURSOR curLocaStockC(LocaStockCId	TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKC
			WHERE
				LOCASTOCKC_ID 	= LocaStockCID
			FOR UPDATE;
		rowLocaStockC	curLocaStockC%ROWTYPE;
		--
		CURSOR curLocaStockP(LocaCd		TA_LOCASTOCKP.LOCA_CD%TYPE,
							SkuCd		TA_LOCASTOCKP.SKU_CD%TYPE,
							LotTX		TA_LOCASTOCKP.LOT_TX%TYPE,
							UseLimitDt	TA_LOCASTOCKP.USELIMIT_DT%TYPE,
							BatchnoCd		TA_LOCASTOCKP.BATCHNO_CD%TYPE,
							TypeStockCD	TA_LOCASTOCKP.TYPESTOCK_CD%TYPE) IS
			SELECT
				ROWID			,
				LOCA_CD			,
				SKU_CD			,
				LOT_TX			,
				USELIMIT_DT		,
				BATCHNO_CD		,
				STOCK_DT		,
				AMOUNT_NR		,
				PALLETCAPACITY_NR,
				PALLET_NR		,
				CASE_NR			,
				BARA_NR			,
				PCS_NR			,
				TYPESTOCK_CD
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD 		= LocaCd		AND
				SKU_CD			= SkuCd			AND
				LOT_TX			= LotTX			AND
				USELIMIT_DT		= UseLimitDt 	AND
				BATCHNO_CD		= ' '			AND		--LOCASTOCKPのBATCHNO_CDは空欄固定
				TYPESTOCK_CD	= TypeStockCd	AND
				STRECORD_ID		= PS_DEFINE.ST_RECORD0_NORMAL
			FOR UPDATE;
		rowLocaStockP 	curLocaStockP%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curLocaStockC(iLocaStockCID);
		FETCH curLocaStockC INTO rowLocaStockC;
		IF curLocaStockC%FOUND THEN
			-- 引当済減算
			DelC(iLocaStockCID,iUserCD,iUserTX,FUNCTION_NAME);
			IF rowLocaStockC.STSTOCK_ID = 0 THEN
				-- 在庫IDが在庫有の場合可能数に戻し
				OPEN curLocaStockP(	rowLocaStockC.LOCA_CD		,
									rowLocaStockC.SKU_CD		,
									rowLocaStockC.LOT_TX		,
									rowLocaStockC.USELIMIT_DT	,
									rowLocaStockC.BATCHNO_CD	,
									rowLocaStockC.TYPESTOCK_CD);
				FETCH curLocaStockP INTO rowLocaStockP;
				IF curLocaStockP%FOUND THEN
					-- 可能数加算（在庫有）
					UpdateP(
						rowLocaStockP.ROWID			,
						iAmountNR					,
						rowLocaStockC.PALLETCAPACITY_NR	,
						rowLocaStockC.PALLET_NR		,
						rowLocaStockC.CASE_NR		,
						rowLocaStockC.BARA_NR		,
						rowLocaStockC.PCS_NR		,
						iUserCD						,
						iUserTX						,
						FUNCTION_NAME
					);
				ELSE
					--可能数加算（在庫無）
					InsertP(
						rowLocaStockC.LOCA_CD		,
						rowLocaStockC.SKU_CD		,
						rowLocaStockC.LOT_TX		,
						rowLocaStockC.KANBAN_CD		,
						rowLocaStockC.CASEPERPALLET_NR,
						rowLocaStockC.PALLETLAYER_NR,
						rowLocaStockC.PALLETCAPACITY_NR,
						rowLocaStockC.STACKINGNUMBER_NR,
						rowLocaStockC.TYPEPLALLET_CD,
						rowLocaStockC.USELIMIT_DT	,
						' '							,	--LOCASTOCKPのBATCHNO_CDは空欄固定
						rowLocaStockC.STOCK_DT		,
						iAmountNR					,
						rowLocaStockC.PALLET_NR		,
						rowLocaStockC.CASE_NR		,
						rowLocaStockC.BARA_NR		,
						rowLocaStockC.PCS_NR		,
						rowLocaStockC.TYPESTOCK_CD	,
						iUserCD						,
						iUserTX						,
						FUNCTION_NAME				
					);
				END IF;
			END IF;
		ELSE
			RAISE_APPLICATION_ERROR(-20000,'');
		END IF;
		CLOSE curLocaStockC;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockC%ISOPEN THEN
				CLOSE curLocaStockC;
			END IF;
			RAISE;
	END ReApplyStock;
	/************************************************************************************/
	/*	入庫引当																		*/
	/************************************************************************************/
	PROCEDURE ApplyInStock(
		iTrnID				IN NUMBER	,
		iStStockID			IN NUMBER	,
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iCasePerPalletNR	IN NUMBER	,
		iPalletLayerNR		IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,	-- パレット積載数
		iStackingNumberNR	IN NUMBER	,
		iTypePalletCD		IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchNoCD			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		orowLocaStockC	OUT TA_LOCASTOCKC%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyInStock';
		numLocaStockCID	TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE;
	BEGIN
		-- 引当済数追加
		PS_STOCK.InsertC(
			iTrnID				,
			iStStockID			,
			iLocaCD 			,
			iSkuCD 				,
			iLotTX				,
			iKanbanCD			,
			iCasePerPalletNR	,
			iPalletLayerNR		,
			iPalletCapacityNR	,
			iStackingNumberNR	,
			iTypePalletCD		,
			iUseLimitDT 		,
			iBatchNoCD			,
			iStockDT			,
			iAmountNR			,
			iPalletNR			,
			iCaseNR				,
			iBaraNR				,
			iPcsNR				,
			iTypeStockCD		,
			iUserCD				,
			iUserTX				,
			FUNCTION_NAME		,
			numLocaStockCID
		);
		-- 引当済取得
		PS_LOCA.GetLockLocaStockC(numLocaStockCID,orowLocaStockC);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyInStock;
	/************************************************************************************/
	/*	かんばん引き落とし																*/
	/*	かんばんコードをキーにしてロケを検索しする										*/
	/*	結果としてかんばんに紐付く在庫が存在しない場合は								*/
	/*	かんばんを再度利用可にする														*/
	/************************************************************************************/
	PROCEDURE DelKanban(
		iKanbanCD 			IN VARCHAR2	
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DelKanban';
		LocaPflg		VARCHAR2(1);		--引当可能在庫に同一カンバンがいるかのフラグ
		LocaCflg		VARCHAR2(1);		--引当済在庫に同一カンバンがいるかのフラグ
		KanbanPflg		VARCHAR2(1);		--TA_KANBANPの削除をおこなうかのフラグ 引当可能、済在庫が居ない場合は'1'をセット
		--引当可能在庫 カンバン単位で確認
		CURSOR curLocaPKanban(pKanbanCD TA_LOCASTOCKP.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKP
			WHERE
				KANBAN_CD = pKanbanCD;
			
			rowLocaPKanban	curLocaPKanban%ROWTYPE;
		
		--引当済在庫 カンバン単位で確認
		CURSOR curLocaCKanban(cKanbanCD TA_LOCASTOCKC.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKC
			WHERE
				KANBAN_CD = cKanbanCD;
			
			rowLocaCKanban	curLocaCKanban%ROWTYPE;
	BEGIN
		--TA_KANBANPの削除可否チェック
		--変数初期化
		LocaPflg := '0';
		LocaCflg := '0';
		KanbanPflg := '0';
		--引当可能在庫のカンバンチェック 同じカンバンの引当可能在庫が居るのかチェック
		OPEN curLocaPKanban(iKanbanCD);
		FETCH curLocaPKanban INTO rowLocaPKanban;
		IF curLocaPKanban%NOTFOUND THEN
			LocaPflg := '1';
		END IF;
		CLOSE curLocaPKanban;
		
		--引当済在庫のカンバンチェック 同じカンバンの引当済在庫が居るのかチェック
		OPEN curLocaCKanban(iKanbanCD);
		FETCH curLocaCKanban INTO rowLocaCKanban;
		IF curLocaCKanban%NOTFOUND THEN
			LocaCflg := '1';
		END IF;
		CLOSE curLocaCKanban;
		
		--引当可能も済も他にカンバンが居ない場合はフラグセット
		IF LocaPflg = '1' AND LocaCflg = '1' THEN
			KanbanPflg := '1';
		END IF;
		
		IF KanbanPflg = '1' THEN
			DELETE
				TA_KANBANP
			WHERE
				KANBAN_CD = iKanbanCD;
		END IF;
	
	END DelKanban;
END PS_STOCK;
/
SHOW ERRORS
