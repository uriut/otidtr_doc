spool C:\SQL\OTID\SQLWMS\11_WMS\50_P\CREATE.log

----------------------------------------------------
-- 優先
----------------------------------------------------
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_DEFINEH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_DEFINED

----------------------------------------------------
-- PS
----------------------------------------------------
-- ヘッダー部
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_APPLYARRIVH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_APPLYINSTOCKH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_APPLYPICKH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_APPLYSHIPH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_APPLYSUPPH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_ARRIVH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_INSTOCKH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_LOCAH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_LOCASEARCHH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_MASTERH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_NOH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_PICKH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_SHIPH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_STATUSH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_STOCKH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_SUPPH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_MOVEH
-- BODY部
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_APPLYARRIVD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_APPLYINSTOCKD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_APPLYPICKD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_APPLYSHIPD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_APPLYSUPPD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_ARRIVD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_INSTOCKD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_LOCAD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_LOCASEARCHD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_MASTERD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_NOD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_PICKD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_SHIPD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_STATUSD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_STOCKD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_SUPPD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PS_MOVED



----------------------------------------------------
-- PW
----------------------------------------------------
-- ヘッダー部
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_ARRIVH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_INSTOCKH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_MASTERH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_PICKH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_PRINTH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_SELECTH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_SHIPH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_STOCKH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_SUPPH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_MOVEH
-- BODY部
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_ARRIVD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_INSTOCKD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_MASTERD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_PICKD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_PRINTD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_SELECTD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_SHIPD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_STOCKD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_SUPPD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PW_MOVED

----------------------------------------------------
-- PI
----------------------------------------------------
-- ヘッダー部
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_ARRIVH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_FILERECVH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_FILESENDH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_SHIPH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_SKUH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_STOCKRESULTH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_SUPPLIERH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_TRANSPORTERH

-- BODY部
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_ARRIVD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_FILERECVD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_FILESENDD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_SHIPD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_SKUD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_STOCKRESULTD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_SUPPLIERD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\PI_TRANSPORTERD

----------------------------------------------------
-- PH
----------------------------------------------------
-- ヘッダー部
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\00_PH_COMM\PH_COMMH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\05_PH_HT\PH_HTH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\10_PH_HTINSTOCK\PH_HTINSTOCKH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\30_PH_HTSHIP\PH_HTSHIPH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\31_PH_PICK\PH_PICKH
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\32_PH_PICK2\PH_PICK2H
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\50_PH_HTOTHER\PH_HTOTHERH
-- BODY部
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\00_PH_COMM\PH_COMMD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\05_PH_HT\PH_HTD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\10_PH_HTINSTOCK\PH_HTINSTOCKD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\30_PH_HTSHIP\PH_HTSHIPD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\31_PH_PICK\PH_PICKD
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\32_PH_PICK2\PH_PICK2D
@C:\SQL\OTID\SQLWMS\11_WMS\50_P\HT\50_PH_HTOTHER\PH_HTOTHERD

spool off;
