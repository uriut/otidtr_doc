CREATE OR REPLACE PACKAGE PH_COMM IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PH_COMM';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PLOG.LDEBUG);
	
	/************************************************************/
	/*															*/
	/*							共通							*/
	/*															*/
	/************************************************************/
	/************************************************************/
	/*	ORACLEバージョン確認									*/
	/************************************************************/
	PROCEDURE GetOraVersion(oOraver OUT VARCHAR2);
	/************************************************************/
	/*	文字→数値変換											*/
	/************************************************************/
	FUNCTION CtoN(
		iChar	IN VARCHAR2
	) RETURN NUMBER;
	/************************************************************/
	/*	文字→日付変換											*/
	/************************************************************/
	FUNCTION CtoD(
		iChar	IN VARCHAR2
	) RETURN DATE;
	/************************************************************/
	/*	数値→文字変換											*/
	/************************************************************/
	FUNCTION NtoC(
		iNumber	IN NUMBER,
		iFormat IN VARCHAR2
	) RETURN VARCHAR2;
	/************************************************************/
	/*	日付→文字変換											*/
	/************************************************************/
	FUNCTION DtoC(
		iDate	IN DATE
	) RETURN VARCHAR2;
	/************************************************************/
	/*	CSVデータから指定された位置のデータを取得				*/
	/************************************************************/
	PROCEDURE GetData(
		iTarget		IN	VARCHAR2,
		iPos		IN	NUMBER,
		oData		OUT	VARCHAR2
	);
	/************************************************************/
	/*	HTユーザ名取得											*/
	/************************************************************/
	PROCEDURE GetUser(
		iUserCD		IN 	VARCHAR2,
		oUserTX		OUT VARCHAR2
	);
	/************************************************************/
	/*	連続トリム												*/
	/************************************************************/
	FUNCTION SeqTrim(
		iData		IN VARCHAR2		,
		ioPos		IN OUT NUMBER	,
		iLength		IN NUMBER
	) RETURN VARCHAR2;
	/************************************************************/
	/*	ＨＴロギング											*/
	/************************************************************/
	PROCEDURE LogHtWrite(
		iAppCD			IN TA_LOGHT.APP_CD%TYPE,
		iHtID			IN TA_LOGHT.HT_ID%TYPE,
		iHtUserCD		IN TA_LOGHT.HTUSER_CD%TYPE,
		iTypeHtCD		IN TA_LOGHT.TYPEHT_CD%TYPE,
		iTypeHtOpeCD	IN TA_LOGHT.TYPEHTOPE_CD%TYPE,
		iTypeHtCmdCD	IN TA_LOGHT.TYPEHTCMD_CD%TYPE,
		iWrCD			IN TA_LOGHT.WR_CD%TYPE,
		iDataTX			IN TA_LOGHT.DATA_TX%TYPE
	);
	/************************************************************/
	/*	ＨＴユーザ登録											*/
	/************************************************************/
	PROCEDURE SetHtUser(
		iHtID		IN ST_HT.HT_ID%TYPE,
		iUserCD		IN ST_HT.USER_CD%TYPE
	);
	/************************************************************/
	/*	ＨＴユーザ取得											*/
	/************************************************************/
	PROCEDURE GetHtUser(
		iHtID		IN ST_HT.HT_ID%TYPE,
		oUserCD		OUT ST_HT.USER_CD%TYPE,
		oUserTX		OUT VARCHAR2
	);
	/************************************************************/
	/*	ＨＴユーザ取得											*/
	/************************************************************/
	PROCEDURE GetHtUserForInitial(
		iUserCd		IN MA_USER.USER_CD%TYPE,
		oUserCD		OUT MA_USER.USER_CD%TYPE,
		oUserTX		OUT MA_USER.USER_TX%TYPE	--,
	);
	/************************************************************/
	/*	ＨＴ更新確認											*/
	/************************************************************/
	PROCEDURE GetHtUpdate(
		iHtID		IN  ST_HT.HT_ID%TYPE		,
		iDhcpFL		IN	ST_HT.DHCP_FL%TYPE		, --1:DHCP
		iIPAddress	IN	ST_HT.IPADDRESS_CD%TYPE	, --IPアドレス
		iDeviceCD	IN  ST_HT.DEVICE_CD%TYPE	, --端末名
		oAppupdFL	OUT ST_HT.APPUPD_FL%TYPE
	);
	/************************************************************/
	/*	ＨＴ更新完了											*/
	/************************************************************/
	PROCEDURE EndHtUpdate(
		iIPAddress	IN	ST_HT.IPADDRESS_CD%TYPE --IPアドレス
	);
	/************************************************************/
	/*	ＨＴＰＣＳ→ＰＣＳ変換									*/
	/************************************************************/
	PROCEDURE HtPcsToPcs(
		iHtPcs		IN  VARCHAR2	,
		oPcs		OUT NUMBER
	);
	/************************************************************/
	/*	ＰＣＳ数→ＨＴＰＣＳ変換								*/
	/************************************************************/
	PROCEDURE PcsToHtPcs(
		iPcs		IN  NUMBER	,
		oHtPcs		OUT VARCHAR2
	);
	/************************************************************/
	/*	ＨＴ使用期限→使用期限変換								*/
	/************************************************************/
	PROCEDURE HtUseLimitToUseLimit(
		iHtUseLimit		IN VARCHAR2	,
		oUseLimit		OUT DATE
	);
	/************************************************************/
	/*	使用期限→ＨＴ使用期限変換								*/
	/************************************************************/
	PROCEDURE UseLimitToHtUseLimit(
		iUseLimit		IN DATE	,
		oHtUseLimit		OUT VARCHAR2
	);
	/************************************************************/
	/*	指定された文字列を20バイトで区切り、1行目を返す			*/
	/************************************************************/
	PROCEDURE GetSplitOne(
		iTarget		IN 	VARCHAR2	,
		iMaxByte	IN	NUMBER		,
		oOut1Tx		OUT	VARCHAR2	
	);
	/************************************************************/
	/*	指定された文字列を20バイトで区切り、1行目と2行目を返す	*/
	/************************************************************/
	PROCEDURE GetSplitTwo(
		iTarget		IN  VARCHAR2	,
		iMaxByte	IN	NUMBER		,
		oOut1Tx		OUT VARCHAR2	,
		oOut2Tx		OUT VARCHAR2	
	);
	/************************************************************/
	/*	指定された文字列を20バイトで区切り、1,2,3行目を返す		*/
	/************************************************************/
	PROCEDURE GetSplitThree(
		iTarget		IN  VARCHAR2	,
		iMaxByte	IN	NUMBER		,
		oOut1Tx		OUT VARCHAR2	,
		oOut2Tx		OUT VARCHAR2	,
		oOut3Tx		OUT VARCHAR2	
	);
END PH_COMM;
/
SHOW ERRORS
