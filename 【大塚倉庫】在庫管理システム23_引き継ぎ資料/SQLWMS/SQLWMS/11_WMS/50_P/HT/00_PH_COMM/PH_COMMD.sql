CREATE OR REPLACE PACKAGE BODY PH_COMM AS
	/************************************************************/
	/*	ORACLEバージョン確認									*/
	/************************************************************/
	PROCEDURE GetOraVersion(oOraver OUT VARCHAR2) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.GetOraVersion';
		numRowPoint 	NUMBER := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		SELECT
			BANNER INTO oOraver
		FROM
			V$VERSION
		WHERE
			ROWNUM = 1;
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PROGRAM_ID || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PROGRAM_ID || SQLERRM);
			END IF;
			RAISE;
	END GetOraVersion;
	/************************************************************/
	/*	文字→数値変換											*/
	/************************************************************/
	FUNCTION CtoN(
		iChar	IN VARCHAR2
	) RETURN NUMBER IS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.CtoN';
		numRowPoint 	NUMBER := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		RETURN TO_NUMBER(iChar);
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PROGRAM_ID || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PROGRAM_ID || SQLERRM);
			END IF;
			RAISE;
	END CtoN;
	/************************************************************/
	/*	文字→日付変換											*/
	/************************************************************/
	FUNCTION CtoD(
		iChar	IN VARCHAR2
	) RETURN DATE IS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.CtoD';
		numRowPoint 	NUMBER := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		RETURN TO_DATE(iChar,'YYYYMMDDHH24MISS');
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PROGRAM_ID || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PROGRAM_ID || SQLERRM);
			END IF;
			RAISE;
	END CtoD;
	/************************************************************/
	/*	数値→文字変換											*/
	/************************************************************/
	FUNCTION NtoC(
		iNumber	IN NUMBER,
		iFormat IN VARCHAR2
	) RETURN VARCHAR2 IS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.NtoC';
		numRowPoint 	NUMBER := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		RETURN SUBSTRB(TO_CHAR(iNumber,iFormat),2,LENGTHB(iFormat));
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PROGRAM_ID || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PROGRAM_ID || SQLERRM);
			END IF;
			RAISE;
	END NtoC;
	/************************************************************/
	/*	日付→文字変換											*/
	/************************************************************/
	FUNCTION DtoC(
		iDate	IN DATE
	) RETURN VARCHAR2 IS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.DtoC';
		numRowPoint 	NUMBER := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		RETURN TO_CHAR(iDate,'YYYYMMDDHH24MISS');
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PROGRAM_ID || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PROGRAM_ID || SQLERRM);
			END IF;
			RAISE;
	END DtoC;
	/************************************************************/
	/*	CSVデータから指定された位置のデータを取得				*/
	/************************************************************/
	PROCEDURE GetData(
		iTarget		IN	VARCHAR2,
		iPos		IN	NUMBER,
		oData		OUT	VARCHAR2
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.GetData';
		numRowPoint 	NUMBER := 0;
		--
		numStart	NUMBER := 0;
		numEnd		NUMBER := 0;
		numLength	NUMBER := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || 
			'Start, ' || 
			'iTarget=[' || iTarget || '], ' ||
			'iPos=[' || iPos || ']'
		);
		oData := '';
		-- 該当データの開始位置（1始まりの文字数）
		IF iPos = 1 THEN
			numStart := 1;
		ELSE
			numStart := INSTR(iTarget, ',', 1, iPos - 1) + 1;
		END IF;
		IF iPos = 1 OR numStart > 1 THEN
			-- 該当データの終了位置（1始まりの文字数）
			numEnd := INSTR(iTarget, ',', 1, iPos);
			IF numEnd = 0 THEN
				-- 最後のデータであるので、最後の文字を終了位置とする
				numEnd := LENGTH(iTarget);
			ELSE
				-- 次のカンマの位置を指しているので、直前の文字を終了位置とする
				numEnd := numEnd - 1;
			END IF;
			-- 該当データの長さ
			numLength := numEnd - numStart + 1;
			-- データ取得
			oData := SUBSTR(iTarget, numStart, numLength);
		END IF;
		PLOG.DEBUG(logCtx, PROGRAM_ID || 
			'End, ' ||
			'iTarget=[' || iTarget || '], ' ||
			'iPos=[' || iPos || '], ' ||
			'oData=[' || oData || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PROGRAM_ID || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PROGRAM_ID || SQLERRM);
			END IF;
			RAISE;
	END GetData;
	/************************************************************/
	/*	社員名取得												*/
	/************************************************************/
	PROCEDURE GetUser(
		iUserCD		IN 	VARCHAR2,
		oUserTX		OUT VARCHAR2
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.GetUser';
		numRowPoint 	NUMBER := 0;
		CURSOR curUser(UserCd VA_USER.USER_CD%TYPE) IS
			SELECT
				USER_TX
			FROM
				VA_USER
			WHERE
				USER_CD = UserCd;
		rowUser curUser%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		OPEN curUser(iUserCD);
		FETCH curUser INTO rowUser;
		IF curUser%NOTFOUND THEN
			--ERROR;
			ROLLBACK;
		ELSE
			oUserTX := rowUser.USER_TX;
		END IF;
		CLOSE curUser;
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PROGRAM_ID || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PROGRAM_ID || SQLERRM);
			END IF;
			IF curUser%ISOPEN THEN
				CLOSE curUser;
			END IF;
			RAISE;
	END GetUser;
	/************************************************************/
	/*	連続トリム												*/
	/************************************************************/
	FUNCTION SeqTrim(
		iData		IN VARCHAR2		,
		ioPos		IN OUT NUMBER	,
		iLength		IN NUMBER
	) RETURN VARCHAR2 IS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.SeqTrim';
		numRowPoint 	NUMBER := 0;
		chrData			VARCHAR2(2000) := ' ';
		chrDummy		VARCHAR2(2000) := ' ';
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		chrData	:= SUBSTRB(iData,ioPos,iLength);
		ioPos := ioPos + iLength;
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
		RETURN SUBSTRB(chrData,1,iLength);
	EXCEPTION
		WHEN OTHERS THEN
			RETURN SUBSTRB(chrDummy,1,iLength);
			DBMS_OUTPUT.PUT_LINE('');
	END SeqTrim;
	/************************************************************/
	/*	ＨＴロギング											*/
	/************************************************************/
	PROCEDURE LogHtWrite(
		iAppCD			IN TA_LOGHT.APP_CD%TYPE,
		iHtID			IN TA_LOGHT.HT_ID%TYPE,
		iHtUserCD		IN TA_LOGHT.HTUSER_CD%TYPE,
		iTypeHtCD		IN TA_LOGHT.TYPEHT_CD%TYPE,
		iTypeHtOpeCD	IN TA_LOGHT.TYPEHTOPE_CD%TYPE,
		iTypeHtCmdCD	IN TA_LOGHT.TYPEHTCMD_CD%TYPE,
		iWrCD			IN TA_LOGHT.WR_CD%TYPE,
		iDataTX			IN TA_LOGHT.DATA_TX%TYPE
	) AS
		PRAGMA AUTONOMOUS_TRANSACTION;
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.LogHtWrite';
		numRowPoint 	NUMBER := 0;
		numLogHtID		TA_LOGHT.LOGHT_ID%TYPE;
		htUserCd		VARCHAR2(8);
		htUserTx		VARCHAR2(50);
	BEGIN
		IF iHtUserCD IS NULL THEN
			htUserCd := 'UNKNOWN';
			htUserTx := 'UNKNOWN';
		ELSE
			htUserCd := iHtUserCD;
			PH_COMM.GetUser(htUserCd,htUserTx);
			IF htUserTx IS NULL THEN
				htUserTx := 'UNKNOWN';
			END IF;
		END IF;
		SELECT
			SEQ_LOGHTID.NEXTVAL INTO numLogHtID
		FROM
			DUAL;
		INSERT INTO TA_LOGHT
		(
			LOGHT_ID		,
			APP_CD			,
			HT_ID			,
			HTUSER_CD		,
			TYPEHT_CD		,
			TYPEHTOPE_CD	,
			TYPEHTCMD_CD	,
			WR_CD			,
			DATA_TX			,
			UPD_DT			,
			UPDUSER_CD		,
			UPDUSER_TX		,
			ADD_DT			,
			ADDUSER_CD		,
			ADDUSER_TX		,
			UPDPROGRAM_CD	,
			UPDCOUNTER_NR	,
			STRECORD_ID		
		)
		VALUES
		(
			numLogHtID		,
			iAppCD			,
			iHtID			,
			htUserCd		,
			iTypeHtCD		,
			iTypeHtOpeCD	,
			iTypeHtCmdCD	,
			iWrCD			,
			iDataTX			,
			SYSDATE			,
			htUserCd		,
			htUserTx		,
			SYSDATE			,
			htUserCd		,
			htUserTx		,
			PROGRAM_ID	,
			0				,
			0				
		);
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PROGRAM_ID || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PROGRAM_ID || SQLERRM);
			END IF;
	END LogHtWrite;
	/************************************************************/
	/*	ＨＴユーザ登録											*/
	/************************************************************/
	PROCEDURE SetHtUser(
		iHtID		IN ST_HT.HT_ID%TYPE,
		iUserCD		IN ST_HT.USER_CD%TYPE
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.SetHtUser';
		numRowPoint 	NUMBER := 0;
		CURSOR curUser(UserCd VA_USER.USER_CD%TYPE) IS
			SELECT
				USER_TX
			FROM
				VA_USER
			WHERE
				USER_CD = UserCd;
		rowUser curUser%ROWTYPE;
		CURSOR curHT(HtId ST_HT.HT_ID%TYPE) IS
			SELECT
				USER_CD
			FROM
				ST_HT
			WHERE
				HT_ID = HtId;
		rowHt curHt%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		OPEN curUser(iUserCD);
		FETCH curUser INTO rowUser;
		IF curUser%NOTFOUND THEN
			DELETE ST_HT
				WHERE
					HT_ID = iHtID;
			COMMIT;
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01,'ユーザが存在しません');
		END IF;
		CLOSE curUser;
		OPEN curHt(iHtID);
		FETCH curHt INTO rowHt;
		IF curHt%FOUND THEN
			UPDATE ST_HT
				SET
					USER_CD = iUserCD	,
					ONLINE_FL = 1	,
					UPD_DT = SYSDATE
				WHERE
					HT_ID = iHtID;
		ELSE
			INSERT INTO ST_HT
			(
				HT_ID,
				USER_CD,
				ONLINE_FL,
				UPD_DT
			)
			VALUES
			(
				iHTID,
				iUserCD,
				1,
				SYSDATE
			);
		END IF;
		CLOSE curHt;
		COMMIT;
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			RAISE;
	END SetHtUser;
	/************************************************************/
	/*	ＨＴユーザ取得											*/
	/************************************************************/
	PROCEDURE GetHtUser(
		iHtID		IN ST_HT.HT_ID%TYPE,
		oUserCD		OUT ST_HT.USER_CD%TYPE,
		oUserTX		OUT VARCHAR2
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.GetHtUser';
		numRowPoint 	NUMBER := 0;
		CURSOR curHT(HtId ST_HT.HT_ID%TYPE) IS
			SELECT
				USER_CD
			FROM
				ST_HT
			WHERE
				HT_ID = HtId;
		rowHt curHt%ROWTYPE;

		CURSOR curUser(UserCd VA_USER.USER_CD%TYPE) IS
			SELECT
				USER_TX
			FROM
				VA_USER
			WHERE
				USER_CD = UserCd;
		rowUser curUser%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		OPEN curHt(iHTID);
		FETCH curHt INTO rowHt;
		IF curHt%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'ログインしていません');
		END IF;
		CLOSE curHt;

		OPEN curUser(rowHt.USER_CD);
		FETCH curUser INTO rowUser;
		IF curUser%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'ユーザが存在しません');
		END IF;
		CLOSE curUser;
		oUserCD := rowHt.USER_CD;
		oUserTX := rowUser.USER_TX;
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
			DBMS_OUTPUT.PUT_LINE('');
	END GetHtUser;
	/************************************************************/
	/*	ＨＴユーザ取得											*/
	/************************************************************/
	PROCEDURE GetHtUserForInitial(
		iUserCd		IN MA_USER.USER_CD%TYPE,
		oUserCD		OUT MA_USER.USER_CD%TYPE,
		oUserTX		OUT MA_USER.USER_TX%TYPE
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.GetHtUserForInitial';
		numRowPoint 	NUMBER := 0;
		CURSOR curUser(UserCd ST_HT.USER_CD%TYPE) IS
			SELECT
				USER_TX
			FROM
				VA_USER
			WHERE
				USER_CD = UserCd;
		rowUser curUser%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		OPEN curUser(iUserCd);
		FETCH curUser INTO rowUser;
		IF curUser%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'ユーザが不適切です');
		END IF;
		CLOSE curUser;
		oUserCD := iUserCd;
		oUserTX := rowUser.USER_TX;
		
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
			DBMS_OUTPUT.PUT_LINE('');
	END GetHtUserForInitial;
	
	/************************************************************/
	/*	ＨＴ更新確認											*/
	/************************************************************/
	PROCEDURE GetHtUpdate(
		iHtID		IN	ST_HT.HT_ID%TYPE		,
		iDhcpFL		IN	ST_HT.DHCP_FL%TYPE		, --1:DHCP
		iIPAddress	IN	ST_HT.IPADDRESS_CD%TYPE	, --IPアドレス
		iDeviceCD	IN	ST_HT.DEVICE_CD%TYPE	, --端末名
		oAppupdFL	OUT	ST_HT.APPUPD_FL%TYPE
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.GetHtUpdate';
		CURSOR curDevice(DeviceCD ST_HT.DEVICE_CD%TYPE) IS
			SELECT
				APPUPD_FL
			FROM
				ST_HT
			WHERE
				DEVICE_CD = DeviceCD;
		rowDevice curDevice%ROWTYPE;
		CURSOR curHT(HtID ST_HT.HT_ID%TYPE) IS
			SELECT
				APPUPD_FL
			FROM
				ST_HT
			WHERE
				HT_ID = HtID;
		rowHT curHT%ROWTYPE;
		numFoundFl		NUMBER := 0;
		chrIPAddress	VARCHAR2(15) := '';
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' iIPAddress = ' || iIPAddress);
		oAppupdFL := 0; --ST_HTに未登録の場合は更新対象外
		--IPアドレス変換
		chrIPAddress := TO_NUMBER(SUBSTR(iIPAddress,1,3)) || '.' ||
						TO_NUMBER(SUBSTR(iIPAddress,5,3)) || '.' || 
						TO_NUMBER(SUBSTR(iIPAddress,9,3)) || '.' || 
						TO_NUMBER(SUBSTR(iIPAddress,13,3));
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' chrIPAddress = ' || chrIPAddress);
		IF iDhcpFL = '1' THEN
			--DHCPの場合は、端末名から更新フラグを取得
			OPEN curDevice(iDeviceCD);
			FETCH curDevice INTO rowDevice;
			IF curDevice%FOUND THEN
				oAppupdFL := rowDevice.APPUPD_FL;
				numFoundFl := 1; --該当あり
				UPDATE
					ST_HT
				SET
					HT_ID		 = iHtID		 ,
					USER_CD		 = ' '			 ,
					IPADDRESS_CD = chrIPAddress	 ,
					DHCP_FL		 = NVL(iDhcpFL,0),
					ONLINE_FL	 = 0		 	 ,
					UPD_DT       = SYSDATE
				WHERE
					DEVICE_CD	 = iDeviceCD;
			END IF;
			CLOSE curDevice;
		ELSE
			--固定IPの場合は、IPアドレスから更新フラグを取得
			OPEN curHT(iHtID);
			FETCH curHT INTO rowHT;
			IF curHT%FOUND THEN
				oAppupdFL := rowHT.APPUPD_FL;
				numFoundFl := 1; --該当あり
				UPDATE
					ST_HT
				SET
					USER_CD			= ' '		 		,
					IPADDRESS_CD	= chrIPAddress		,
					DEVICE_CD		= NVL(iDeviceCD,' '),
					DHCP_FL			= NVL(iDhcpFL,0)	,
					ONLINE_FL		= 0			 		,
					UPD_DT			= SYSDATE
				WHERE
					HT_ID			= iHtID;
			END IF;
			CLOSE curHT;
		END IF;
		IF numFoundFl = 0 THEN
			--未登録の場合は端末名など取得
			INSERT INTO ST_HT (
				HT_ID		,
				USER_CD		,
				IPADDRESS_CD,
				DEVICE_CD	,
				DHCP_FL		,
				ONLINE_FL	,
				APPUPD_FL	,
				UPD_DT
			) VALUES (
				iHTID			  ,
				' '				  , --USER_CD
				chrIPAddress	  ,
				NVL(iDeviceCD,' '),
				NVL(iDhcpFL,0)	  ,
				0				  , --ONLINE_FL
				0				  , --APPUPD_FL
				SYSDATE
			);
		END IF;
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
			DBMS_OUTPUT.PUT_LINE('');
	END GetHtUpdate;
	/************************************************************/
	/*	ＨＴ更新完了											*/
	/************************************************************/
	PROCEDURE EndHtUpdate(
		iIPAddress	IN	ST_HT.IPADDRESS_CD%TYPE
	) AS
		PROGRAM_ID 		CONSTANT VARCHAR2(50)	:= 'PH_COMM.EndHtUpdate';
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START ');
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' iIPAddress = ' || iIPAddress);
		UPDATE
			ST_HT
		SET
			APPUPD_FL		= 0			, --更新完了
			UPD_DT			= SYSDATE
		WHERE
			IPADDRESS_CD	= iIPAddress;
		IF SQL%ROWCOUNT = 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：自動更新が完了できませんでした。');
		END IF;
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
			DBMS_OUTPUT.PUT_LINE('');
	END EndHtUpdate;
	/************************************************************/
	/*	ＨＴＰＣＳ→ＰＣＳ変換									*/
	/************************************************************/
	PROCEDURE HtPcsToPcs(
		iHtPcs		IN  VARCHAR2	,
		oPcs		OUT NUMBER
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.HtPcsToPcs';
		numRowPoint 	NUMBER := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		oPcs := CtoN(iHtPcs);
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
			DBMS_OUTPUT.PUT_LINE('');
	END HtPcsToPcs;
	/************************************************************/
	/*	ＰＣＳ数→ＨＴＰＣＳ変換								*/
	/************************************************************/
	PROCEDURE PcsToHtPcs(
		iPcs		IN  NUMBER		,
		oHtPcs		OUT VARCHAR2
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.PcsToHtPcs';
		numRowPoint 	NUMBER := 0;
		numData			NUMBER(9,0);
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		IF iPcs > 99999999 THEN
			numData := 99999999;
		ELSE
			numData := iPcs;
		END IF;
		oHtPcs := NtoC(numData,'99999999');
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
			DBMS_OUTPUT.PUT_LINE('');
	END PcsToHtPcs;
	/************************************************************/
	/*	ＨＴ使用期限→使用期限変換								*/
	/************************************************************/
	PROCEDURE HtUseLimitToUseLimit(
		iHtUseLimit		IN VARCHAR2	,
		oUseLimit		OUT DATE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HtUseLimitToUseLimit';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		IF NOT TRIM(iHtUseLimit) = '/' THEN
			oUseLimit := TO_DATE(iHtUseLimit || '/01000000','YYYY/MM/DDHH24MISS');
		ELSE
			oUseLimit := PS_DEFINE.USELIMITDT_DEFAULT;
		END IF;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT37_NN_NO,'使用期限が不正です。');
			RAISE;
			DBMS_OUTPUT.PUT_LINE('');
	END HtUseLimitToUseLimit;
	/************************************************************/
	/*	使用期限→ＨＴ使用期限変換								*/
	/************************************************************/
	PROCEDURE UseLimitToHtUseLimit(
		iUseLimit		IN  DATE	,
		oHtUseLimit		OUT VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UseLimitToHtUseLimit';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		IF NOT iUseLimit = PS_DEFINE.USELIMITDT_DEFAULT THEN
			oHtUseLimit	:= TO_CHAR(iUseLimit,'YYYY/MM');
		ELSE
			oHtUseLimit	:= ' ';
		END IF;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
			DBMS_OUTPUT.PUT_LINE('');
	END UseLimitToHtUseLimit;
	/************************************************************/
	/*	指定文字列を指定バイトで区切り、1行目を返す				*/
	/************************************************************/
	PROCEDURE GetSplitOne(
		iTarget		IN 	VARCHAR2	,
		iMaxByte	IN	NUMBER		,
		oOut1Tx		OUT	VARCHAR2	
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.GetSplitOne';
		numPos		NUMBER(3);
		resultString	VARCHAR2(200);
		targetCharacter	VARCHAR2(10);
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		resultString := '';
		targetCharacter := '';
		IF LENGTH(iTarget) > 0 THEN
			FOR numPos IN 1..LENGTH(iTarget) LOOP
				--1文字ずつ取得
				targetCharacter := SUBSTR(iTarget, numPos, 1);
				--バイト数チェック
				IF LENGTHB(resultString) + LENGTHB(targetCharacter) > iMaxByte THEN
					EXIT;
				END IF;
				resultString := resultString || targetCharacter;
			END LOOP;
			oOut1Tx := resultString;
		ELSE
			oOut1Tx := '';
		END IF;
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
			DBMS_OUTPUT.PUT_LINE('');
	END GetSplitOne;
	/************************************************************/
	/*	指定された文字列を20バイトで区切り、1行目と2行目を返す	*/
	/************************************************************/
	PROCEDURE GetSplitTwo(
		iTarget		IN  VARCHAR2	,
		iMaxByte	IN	NUMBER		,
		oOut1Tx		OUT VARCHAR2	,
		oOut2Tx		OUT VARCHAR2	
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.GetSplitTwo';
		leftString	VARCHAR2(2000) := '';
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		GetSplitOne(
			iTarget		,
			iMaxByte	,
			oOut1Tx		
		);
		leftString := SUBSTR(iTarget, LENGTH(oOut1Tx) + 1, LENGTH(iTarget) - LENGTH(oOut1Tx));
		GetSplitOne(
			leftString	,
			iMaxByte	,
			oOut2Tx		
		);
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
			DBMS_OUTPUT.PUT_LINE('');
	END GetSplitTwo;
	/************************************************************/
	/*	指定された文字列を20バイトで区切り、1,2,3行目を返す		*/
	/************************************************************/
	PROCEDURE GetSplitThree(
		iTarget		IN  VARCHAR2	,
		iMaxByte	IN	NUMBER		,
		oOut1Tx		OUT VARCHAR2	,
		oOut2Tx		OUT VARCHAR2	,
		oOut3Tx		OUT VARCHAR2	
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_COMM.GetSplitThree';
		leftString	VARCHAR2(2000) := '';
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' START');
		GetSplitOne(
			iTarget		,
			iMaxByte	,
			oOut1Tx		
		);
		leftString := SUBSTR(iTarget, LENGTH(oOut1Tx) + 1, LENGTH(iTarget) - LENGTH(oOut1Tx));
		GetSplitOne(
			leftString	,
			iMaxByte	,
			oOut2Tx		
		);
		leftString := SUBSTR(leftString, LENGTH(oOut2Tx) + 1, LENGTH(leftString) - LENGTH(oOut2Tx));
		GetSplitOne(
			leftString	,
			iMaxByte	,
			oOut3Tx		
		);
		PLOG.DEBUG(logCtx, PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
			DBMS_OUTPUT.PUT_LINE('');
	END GetSplitThree;
END PH_COMM;
/
SHOW ERRORS
