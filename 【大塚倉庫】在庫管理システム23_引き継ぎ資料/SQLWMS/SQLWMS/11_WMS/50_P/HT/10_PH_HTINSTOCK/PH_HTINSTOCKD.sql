--Copyright (C) Seaos Corporation 2012 All rights reserved
CREATE OR REPLACE PACKAGE BODY PH_HTINSTOCK AS
	/************************************************************/
	/*	伝票番号チェック										*/
	/************************************************************/
	PROCEDURE InStockCMD_0101(
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2,
		iAppCd			IN VARCHAR2,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTINSTOCK.InStockCMD_0101';
		msg					VARCHAR2(1000);
		chrSlipNo		    VARCHAR2(60);	--伝票番号
		ret					VARCHAR2(2);
		--追加変数
		numArrivId			NUMBER(10);		--入荷予定ID
		CURSOR curTarget(
			SlipNo TA_ARRIV.SLIPNO_TX%TYPE
		) IS
			SELECT 
				*
			FROM
				TA_ARRIV TA	
			WHERE
				RTRIM(TA.SLIPNO_TX) = SlipNo AND 					--初期テスト時カラムに空白が有った為トリム追加
				( 
				TA.STARRIV_ID = PS_DEFINE.ST_ARRIV3_ARRIV_E			-- 3:入荷受付完了
				OR
					(
					TA.STARRIV_ID = PS_DEFINE.ST_ARRIV4_ARRIV_CHK 	-- 4:入庫検品中
					AND
					CHECKHT_ID = iHtUserCD 
					)
				)
			ORDER BY
				TA.STARRIV_ID DESC	,  --自HTで1回以上呼び出しているデータから処理する
				TA.ARRIVPLANPCS_NR DESC;
		rowTarget 		curTarget%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ':' || ' START');
		ret := '';
		msg := '';
	
		--伝票番号取得
		PH_COMM.GetData(iRxCommand,7,chrSlipNo); 	--送信データから伝票番号取得
	
		--入荷予定取得
		OPEN curTarget(chrSlipNo);
		FETCH curTarget INTO rowTarget;
		IF curTarget%NOTFOUND THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'入荷予定に存在しません。');
		END IF;
		CLOSE curTarget;
		
		ret := '00'; --TODO_分岐時は設定する
		oRet := ret;
		PLOG.DEBUG(logCtx, PROGRAM_ID || ':' || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
	END InStockCMD_0101;
	/************************************************************/
	/*	ITF送信													*/
	/************************************************************/
	PROCEDURE InStockCMD_0102(
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2,
		iAppCd			IN VARCHAR2,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTINSTOCK.InStockCMD_0102';
		msg					VARCHAR2(1000);
		chrSkuBarcodeTx		VARCHAR2(20);
		chrSkuCd			VARCHAR2(20);
		chrSku1Tx			VARCHAR2(60);
		chrSku2Tx			VARCHAR2(20);
		chrSku3Tx			VARCHAR2(20);
		ret					VARCHAR2(2);
		--追加変数
		numArrivId			NUMBER(10);		--入荷予定ID
		chrItfBarcodeTx		VARCHAR2(14);	--ITFバーコード
		chrSlipNo		    VARCHAR2(60);	--伝票番号
		chrExteriorCd		VARCHAR2(40);	--外装コード
		chrSizeTx		    VARCHAR2(60);	--規格
		chrLotTx	    	VARCHAR2(60);	--ロット
		numPalletLayer		NUMBER(9);		--パレット段数
		numCasePerlayer		NUMBER(9);		--パレット回し数
		numCaseperPallet	NUMBER(9);		--パレット積み付け数、パレット積載数とも言う
		numArrivPlanCase	NUMBER(5);	
		
		--残りの入荷数チェック
		CURSOR curNextPlan(SlipNO TA_ARRIV.SLIPNO_TX%TYPE,
						   SkuCd TA_ARRIV.SKU_CD%TYPE
		) IS
			SELECT
				SLIPNO_TX	,
				SKU_CD		,
				SUM(ARRIVPLANPCS_NR) - SUM(GOODPCS_NR) AS NextPlanPcs
			FROM
				TA_ARRIV
			WHERE
				SLIPNO_TX = SlipNO AND
				SKU_CD = SkuCd
			GROUP BY
				SLIPNO_TX	,
				SKU_CD;
		rowNextPlan		curNextPlan%ROWTYPE;
		
		--取得するデータの条件
		--取得元、入荷予定テーブル
		--伝票番号とスキャンしたITFコードから取得したSKUにて絞り込む
		
		--STARRIV_ID = 3:入荷受付完了 もしくは４:入庫検品中で更新HTが自HTと同一のレコードを取得する
		--その場合はロット昇順で１行のみ取得とする
		
		--STARRIV_IDが「３:入荷受付完了」のみ取得
		CURSOR curTarget(
			SkuCD TA_ARRIV.SKU_CD%TYPE	,
			SlipNo TA_ARRIV.SLIPNO_TX%TYPE
		) IS
			SELECT 
				*
				--TA.SLIPNO_TX	,
			FROM
				TA_ARRIV TA	
			WHERE
				TA.SKU_CD = SkuCD AND
				RTRIM(TA.SLIPNO_TX) = SlipNo AND --初期テスト時カラムに空白が有った為トリム追加
				( 
				TA.STARRIV_ID = PS_DEFINE.ST_ARRIV3_ARRIV_E -- 3:入荷受付完了
				OR
					(
					TA.STARRIV_ID = PS_DEFINE.ST_ARRIV4_ARRIV_CHK -- 4:入庫検品中
					AND
					CHECKUSER_CD = iHtUserCD --ログイン者
					)
				)
			ORDER BY
				TA.STARRIV_ID DESC	,		--自HTで1回以上呼び出しているデータから処理する
				TA.ARRIVPLANPCS_NR DESC	,	--入荷予定数の多い順
				TA.LOT_TX
			FOR UPDATE;
			
		rowTarget 		curTarget%ROWTYPE;
		rowSku			  MA_SKU%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ':' || ' START');
		ret := '';
		msg := '';
		--JAN
		PH_COMM.GetData(iRxCommand,7,chrItfBarcodeTx);
		IF chrItfBarcodeTx IS NULL THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'予期せぬエラー(ITFがNULL)');
		END IF;
		
		--伝票番号取得
		PH_COMM.GetData(iRxCommand,8,chrSlipNo); 	--送信データから伝票番号取得
		
		--ITFコード取得
		PS_MASTER.GetSkuFromBarcodeITF(chrItfBarcodeTx, rowSku); --ITFコードで検索する
		chrSkuCd := rowSku.SKU_CD;
		
		--入荷予定残数の確認
		OPEN curNextPlan(chrSlipNo, chrSkuCd);
		FETCH curNextPlan INTO rowNextPlan;
		IF curNextPlan%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'入荷予定がありません。');
		END IF;
		CLOSE curNextPlan;
		IF rowNextPlan.NextPlanPcs <= 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'計上可能な入荷予定残が有りません。');
		END IF;
		
		--商品名セット
		--PH_COMM.GetSplitThree(rowSku.SKU_TX, 20, chrSku1Tx, chrSku2Tx, chrSku3Tx);
		PH_COMM.GetSplitOne(rowSku.SKU2_TX, 60, chrSku1Tx);
		--商品名２と３は空白としておく
		chrSku2Tx := '';
		chrSku3Tx := '';
		--外装コードセット
		PH_COMM.GetSplitOne(rowSku.EXTERIOR_CD, 20, chrExteriorCd);
		--規格
		PH_COMM.GetSplitOne(rowSku.SIZE_TX, 60, chrSizeTx);
		--入荷予定取得
		OPEN curTarget(chrSkuCd,chrSlipNo);
		FETCH curTarget INTO rowTarget;
		IF curTarget%NOTFOUND THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'入荷予定に存在しません。');
		END IF;
		
		
		--入荷予定IDセット
		numArrivId := rowTarget.ARRIV_ID;
		--ロットセット
		chrLotTx := RTRIM(rowTarget.LOT_TX); --空白の可能性があるのでトリムする
		--パレット段数セット
		numPalletLayer := rowTarget.PALLETLAYER_NR;
		--パレット回し数セット
		numCasePerlayer := rowTarget.CASEPERLAYER_NR;
		--パレット積み付け数セット
		numCaseperPallet := rowTarget.PALLETCAPACITY_NR;
		--入荷予定のPcs数 / 入数 で計上予定ケース数を返す
		numArrivPlanCase := rowTarget.ARRIVPLANPCS_NR / rowSku.PCSPERCASE_NR;
		
		--入荷予定を取得出来たら、該当入荷予定を更新する
		--更新時は送信HTのIPをIDとして登録、以後該当入荷予定はそのHTでしか処理出来なくなる
		IF numArrivId > 0 THEN
			UPDATE
				TA_ARRIV
			SET
				STARRIV_ID			= PS_DEFINE.ST_ARRIV4_ARRIV_CHK,	-- 4:入庫検品中に更新する
				CHECKHT_ID			= iHtID			,		--自HTで処理している状態に
				CHECKUSER_CD		= iHtUserCD		,		--検品者コード
				CHECKUSER_TX		= iHtUserTX		,		--検品者名
				CHECK_DT			= SYSDATE		,		--検品日
				--
				UPD_DT				= SYSDATE		,		--更新日時
				UPDUSER_CD			= iHtUserCD		,		--
				UPDUSER_TX			= iHtUserTX		,
				UPDPROGRAM_CD		= PROGRAM_ID	,
				UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
			WHERE
				ARRIV_ID 		= numArrivId; 			--入荷予定IDで更新
		END IF;
		
		CLOSE curTarget;
		
		ret := '00'; --TODO_分岐時は設定する
	--カンマ数	3		 	4		 5		  6	   	   7	 	   8	 9		 10			   11			   12				   13				 14
	------ ret, 入荷予定ID, 商品名1, 商品名2, 商品名3, 外装コード, 規格, ロット, パレット段数, パレット回し数, パレット積み付け数, 入荷予定ケース数, SKUコード
		oRet := ret || ',' || numArrivId || ',' || chrSku1Tx || ',' || chrSku2Tx || ',' || chrSku3Tx || ',' || 
				chrExteriorCd || ',' || chrSizeTx || ',' || chrLotTx || ',' || numPalletLayer || ',' ||
				 numCasePerlayer || ',' || numCaseperPallet || ',' || numArrivPlanCase || ',' || chrSkuCd || ',' ||
				 msg;     
				 
		PLOG.DEBUG(logCtx, PROGRAM_ID || ':' || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
	END InStockCMD_0102;
	/************************************************************************************/
	/*	入庫登録																		*/
	/************************************************************************************/
	PROCEDURE InStockCMD_0103(
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2,
		iAppCd			IN VARCHAR2,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	) AS
		iArrivID				NUMBER(10);
		iSkuCD					VARCHAR2(40);
		iLotTX					VARCHAR2(60);
		iSameDayShipFL			NUMBER(1);
		iPalletNR				NUMBER(9);
		iCaseNR					NUMBER(9);
		iBaraNR					NUMBER(9);
		iPcsNR					NUMBER(10);
		iUseLimitDT				DATE;
		iCheckArrivUseLimit		NUMBER(1);			--0:入荷可能期限をチェックしない、1：チェックする
		iUserCD					VARCHAR2(60);
		iProgramCD				VARCHAR2(60) := 'InStockCMD_0103';
		oInStockNoCD		 	TA_INSTOCK.INSTOCKNO_CD%TYPE;
		oKanbanCD			 	TA_INSTOCK.KANBAN_CD%TYPE	;
		ret						VARCHAR2(2);
		FUNCTION_NAME			CONSTANT VARCHAR2(40)	:= 'RegInstock';
		numCount				NUMBER(9,0) := 0;
		rowUser					VA_USER%ROWTYPE;
		numTrnID				NUMBER(10,0);
		rowSku					MA_SKU%ROWTYPE;
		rowSkuPallet			MA_SKUPALLET%ROWTYPE;
		iManuFacturerCD			VARCHAR2(60);
		rowArea					MB_AREA%ROWTYPE;
		chrAREA_CD				VARCHAR2(10);
		chrRCOLOR_TX			VARCHAR2(10);
		chrGCOLOR_TX			VARCHAR2(10);
		chrBCOLOR_TX			VARCHAR2(10);
		rowLoca					MA_LOCA%ROWTYPE;
		chrLineCD				MA_LOCA.LINEUNIT_CD%TYPE;
		iSendCaseNR				NUMBER(9);
		sSlipNo					TA_ARRIV.SLIPNO_TX%TYPE;			--
		sLotTX					TA_ARRIV.LOT_TX%TYPE;				--
		chrInStockNoCD			TA_INSTOCK.INSTOCKNO_CD%TYPE;
		chrKanbanCD				TA_INSTOCK.KANBAN_CD%TYPE;
		hArrivID				TA_ARRIV.ARRIV_ID%TYPE;				-- ARRIV_ID 保存用
		changeLotFlg			VARCHAR2(1) := '0';
		
		--データ取得用カーソル
		CURSOR curTarget(ArrivID TA_ARRIV.ARRIV_ID%TYPE) IS
			SELECT
				*
			FROM
				TA_ARRIV
			WHERE
				ARRIV_ID = ArrivID
			FOR UPDATE;
		rowTarget	curTarget%ROWTYPE;
		
		--チェック用カーソル
		CURSOR curArriv(ArrivID TA_ARRIV.ARRIV_ID%TYPE) IS
			SELECT
				*
			FROM
				TA_ARRIV
			WHERE
				ARRIV_ID = ArrivID
			FOR UPDATE;
		rowArriv	curArriv%ROWTYPE;
		
		--残りの入荷数チェック
		CURSOR curNextPlan(SlipNO TA_ARRIV.SLIPNO_TX%TYPE,
						   SkuCd TA_ARRIV.SKU_CD%TYPE
		) IS
			SELECT
				SLIPNO_TX	,
				SKU_CD		,
				SUM(ARRIVPLANPCS_NR) - SUM(GOODPCS_NR) AS NextPlanPcs
			FROM
				TA_ARRIV
			WHERE
				SLIPNO_TX = SlipNO AND
				SKU_CD = SkuCd
			GROUP BY
				SLIPNO_TX	,
				SKU_CD;
		rowNextPlan		curNextPlan%ROWTYPE;
		
		CURSOR curInStock(InStockNoCD TA_INSTOCK.INSTOCKNO_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_INSTOCK
			WHERE
				INSTOCKNO_CD = InStockNoCD;
		rowInStock	curInStock%ROWTYPE;
		
		-- ロット変更用カーソル
		CURSOR curLot(SlipNo TA_ARRIV.SLIPNO_TX%TYPE,
					  SkuCD TA_ARRIV.SKU_CD%TYPE,
					  Lot TA_ARRIV.LOT_TX%TYPE
		) IS
			SELECT
				*
			FROM
				TA_ARRIV
			WHERE
				SLIPNO_TX = SlipNo AND
				SKU_CD = SkuCD AND
				LOT_TX = Lot AND
				STARRIV_ID = PS_DEFINE.ST_ARRIV3_ARRIV_E  --入荷受付完了の物のみ
			ORDER BY
				ARRIVPLANPCS_NR DESC;
				
		rowLot		curLot%ROWTYPE;
		
		--ロック用カーソル
		CURSOR curLock(SkuCD MA_SKU.SKU_CD%TYPE) IS
			SELECT
				SKU_CD
			FROM
				MA_SKU
			WHERE
				SKU_CD = SkuCD
			FOR UPDATE;
		rowLock 	curLock%ROWTYPE;
		
	BEGIN
		--GetDataでHTからのデータを変数に代入
		PH_COMM.GetData(iRxCommand,6,iUserCD);
		PH_COMM.GetData(iRxCommand,7,iArrivID);		--入荷予定ID
		PH_COMM.GetData(iRxCommand,8,iSkuCD);		--SKUコード
		PH_COMM.GetData(iRxCommand,9,iLotTX);		--ロット
		PH_COMM.GetData(iRxCommand,10,iCaseNR);		--計上ケース数
		iSendCaseNR := PH_COMM.CtoN(iCaseNR);
		
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		PS_MASTER.GetSku(iSkuCD, rowSku);
		
		OPEN curTarget(iArrivID);
		FETCH curTarget INTO rowTarget;
		CLOSE curTarget;
		--変数セット,ここから
		iSameDayShipFL	:= rowTarget.SAMEDAYSHIP_FL;
		--Pcs数			 		     入数	   ×  ケース数
		iPcsNR			:= rowSku.PCSPERCASE_NR * iCaseNR;
		
		IF iCaseNR = rowTarget.PALLETCAPACITY_NR THEN
			iPalletNR		:= 1;
			iCaseNR			:= 0;
		Else
			iPalletNR		:= 0;
		End If;
		iBaraNR			:= rowTarget.ARRIVPLANBARA_NR;
		
		iUseLimitDT		:= PS_DEFINE.USELIMITDT_DEFAULT; --デフォルト値をセット
		iCheckArrivUseLimit := 0;
		
		iManuFacturerCD := rowTarget.MANUFACTURER_CD;
		
		PS_MASTER.GetSkuPallet(iSkuCD, iManuFacturerCD, rowSkuPallet);
		
		sLotTX := rowTarget.LOT_TX;
		sSlipNo := rowTarget.SLIPNO_TX;
		--ここまで
		
		--入荷計上ロット変更の場合
		IF iLotTX <> sLotTX THEN 							-- HTから送信されたロットとTA_ARRIVのロット比較
			PLOG.DEBUG(logCtx, 'ロット変更開始 ' || sSlipNo || ',' || iSkuCD || ',' || iLotTX || ',' );
			OPEN curLot(sSlipNo, iSkuCD, iLotTX);
			FETCH curLot INTO rowLot;
			IF curLot%FOUND THEN   							-- 同一伝票番号で別ロットが居た場合は
				PLOG.DEBUG(logCtx, 'ロット変更途中 ' || sSlipNo || ',' || iSkuCD || ',' || iLotTX || ',' );
				IF iPcsNR <= rowLot.ARRIVPLANPCS_NR THEN	-- 入荷予定数以内ならば
					PLOG.DEBUG(logCtx, 'ロット変更セット = ' || rowLot.ARRIV_ID || ',' );
					hArrivID := iArrivID;					-- 旧入荷予定IDの退避
					iArrivID := rowLot.ARRIV_ID;			-- 入荷予定ID変更
					changeLotFlg := '1';
				END IF;
			END IF;
			CLOSE curLot;
		END IF;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START, ' ||
			'iArrivID=[' || iArrivID || '], ' ||
			'iSkuCD=[' || iSkuCD || '], ' ||
			'iPalletNR=[' || iPalletNR || '],' ||
			'iCaseNR=[' || iCaseNR || '],' ||
			'iBaraNR=[' || iBaraNR || '],' ||
			'iPcsNR=[' || iPcsNR || '], ' ||
			'iUseLimitDT=[' || iUseLimitDT || '], ' ||
			'iCheckArrivUseLimit=[' || iCheckArrivUseLimit || ']'
		);
		
		-- 入荷データ取得
		OPEN curArriv(iArrivID);
		FETCH curArriv INTO rowArriv;
		IF curArriv%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'入荷予定がありません。');
		END IF;
		CLOSE curArriv;
		IF rowArriv.SKU_CD <> iSkuCD THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：商品コードが不適切です。');
		END IF;
		-- ステータスチェック
		IF rowArriv.STARRIV_ID < PS_DEFINE.ST_ARRIV3_ARRIV_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入荷受付が完了していません。');
		END IF;
		IF rowArriv.STARRIV_ID < PS_DEFINE.ST_ARRIVM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入荷予定がキャンセルされています。');
		END IF;
		--PLOG.DEBUG(logCtx, 'START_1');
		IF rowArriv.ARRIVPLANPCS_NR - ( rowTarget.GOODPCS_NR + rowTarget.NGPCS_NR + rowTarget.SAMPLEPCS_NR ) <= 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入荷予定がありません（完了済み）。');
		END IF;
		--入荷予定数以上の計上は不可
		PLOG.DEBUG(logCtx, 'iSendCaseNR = ' || iSendCaseNR || ',' || 'PALLETCAPACITY_NR = ' || rowTarget.PALLETCAPACITY_NR);
		IF iSendCaseNR > rowArriv.PALLETCAPACITY_NR THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '1ﾊﾟﾚｯﾄを超えるｹｰｽ数は送信出来ません。' || 'ｹｰｽ数[' || rowArriv.PALLETCAPACITY_NR || ']まで' );
		END IF;
		--入荷予定残数の確認
		OPEN curNextPlan(sSlipNo, iSkuCD);
		FETCH curNextPlan INTO rowNextPlan;
		IF curNextPlan%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'入荷予定残がありません。');
		END IF;
		CLOSE curNextPlan;
		IF rowNextPlan.NextPlanPcs < iPcsNR THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'残りの入荷予定数を超える数量は送信出来ません。');
		END IF;
		
		--ロック用
		OPEN curLock(rowSku.SKU_CD);
		FETCH curLock INTO rowLock;
		-- TRNID
		PS_NO.GetTrnId(numTrnID);
		-- 入庫引当、実績作成
		PS_APPLYINSTOCK.ApplyInstock(
			numTrnID			,
			rowSku				,
			rowArriv			,
			--rowSkuPallet		,
			iSameDayShipFL		,
			iPalletNR			,
			iCaseNR				,
			iBaraNR				,
			iPcsNR				,
			iUseLimitDT			,
			CASE WHEN iCheckArrivUseLimit = 1 THEN 0 ELSE 1 END,	--管理者棚指定フラグ
			'10'				,	-- TYPEINSTOCK_CD
			iArrivID			,
			iSkuCD				,
			iLotTX				,	-- 入力されたロット
			rowArriv.TYPEPLALLET_CD,
			iUserCD				,
			rowUser.USER_TX		,
			iProgramCD			,
			chrInStockNoCD		,
			chrKanbanCD			,
			rowArea
		);
		CLOSE curLock;
		
		-- 入庫指示書印刷可
		PS_INSTOCK.InStockOK(
			numTrnID		,
			iUserCD			,
			rowUser.USER_TX	,
			iProgramCD
		);
		
		IF changeLotFlg = '1' THEN
			--元の入荷予定の更新、ステータス等を元に戻す
			UPDATE
				TA_ARRIV
			SET
				STARRIV_ID			= PS_DEFINE.ST_ARRIV3_ARRIV_E		,
				CHECKHT_ID			= 0									,		--自HTで処理している状態に
				CHECKUSER_CD		= ' '								,		--検品者コード
				CHECKUSER_TX		= ' '								,		--検品者名
				--
				UPD_DT				= SYSDATE							,
				UPDUSER_CD			= iUserCD							,
				UPDUSER_TX			= rowUser.USER_TX					,
				UPDPROGRAM_CD		= iProgramCD						,
				UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
			WHERE
				ARRIV_ID			= hArrivID;
		END IF;
		
		--入荷予定更新
		UPDATE
			TA_ARRIV
		SET
			STARRIV_ID			= PS_DEFINE.ST_ARRIV4_INSTOCKCHK_E	,
			GOODPCS_NR			= GOODPCS_NR + iPcsNR				,
			--
			UPD_DT				= SYSDATE							,
			UPDUSER_CD			= iUserCD							,
			UPDUSER_TX			= rowUser.USER_TX					,
			UPDPROGRAM_CD		= iProgramCD						,
			UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
		WHERE
			ARRIV_ID			= iArrivID;
		
		--入荷予定更新（全完了）
		UPDATE
			TA_ARRIV
		SET
			STARRIV_ID			= PS_DEFINE.ST_ARRIV4_INSTOCKCHK_E	,
			--
			UPD_DT				= SYSDATE							,
			UPDUSER_CD			= iUserCD							,
			UPDUSER_TX			= rowUser.USER_TX					,
			UPDPROGRAM_CD		= iProgramCD						,
			UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
		WHERE
			ARRIV_ID			= iArrivID						AND
			STARRIV_ID			= PS_DEFINE.ST_ARRIV3_ARRIV_E	AND
			ARRIVPLANPCS_NR		<= GOODPCS_NR + NGPCS_NR + SAMPLEPCS_NR;
		
		PLOG.DEBUG(logCtx,
			'oInStockNoCD=[' || oInStockNoCD || '], ' ||
			'chrInStockNoCD=[' || chrInStockNoCD || ']'
		);
		oInStockNoCD := chrInStockNoCD;
		oKanbanCD := chrKanbanCD;
		
		
		--カンバンの色変更用のデータが返ってくるのでHTに返す
		chrAREA_CD			:= rowArea.COLOR_CD;		--カラーコード
		chrRCOLOR_TX		:= rowArea.RCOLOR_TX;		--R
		chrGCOLOR_TX		:= rowArea.GCOLOR_TX;		--G
		chrBCOLOR_TX		:= rowArea.BCOLOR_TX;		--B
		
		--20140430_Azumi_カンバンの仲間がいない場合は上記の色（カラーコード）と
		--LU（ラインユニットコード）も表示するように変更
		--HT見た目は、
		--色[A] ｶﾝﾊﾞﾝ [ A　　　　] A:はラインユニット、　はスキャン、もしくは手打ち
		--LUの取得はInStockNoCDから格納指示を検索、LocaCD取得のPS_MASTER.GetLoca(iLocaCd,orowLoca)でROWTYPEで取得
		--バラしてHTに返す
		OPEN curInStock(oInStockNoCD);
		FETCH curInStock INTO rowInStock;
		IF curInStock%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'格納指示が作成されていません。');
		END IF;
    	CLOSE curInStock;
    	
		PS_MASTER.GetLoca(rowInStock.DESTLOCA_CD,rowLoca);		--格納指示の「先ロケーションコード」からロケーションゲット
		chrLineCD := rowLoca.LINEUNIT_CD;						--ロケからMA_LOCA.LINE_CDセット
		
		ret := '00' ;
		COMMIT;
		--		2			  3						 4					 5					  6						 7						8					   9				   10
		oRet := ret || ',' || oInStockNoCD || ',' || oKanbanCD || ',' || chrAREA_CD || ',' || chrRCOLOR_TX || ',' || chrGCOLOR_TX || ',' || chrBCOLOR_TX || ',' || chrLineCD || ',' || iArrivID;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END, ' ||
			'oInStockNoCD=[' || oInStockNoCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTarget%ISOPEN THEN
				CLOSE curTarget;
			END IF;
			RAISE;
	END InStockCMD_0103;
	/************************************************************************************/
	/*	かんばん登録																	*/
	/************************************************************************************/
	PROCEDURE InStockCMD_0104(
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2,
		iAppCd			IN VARCHAR2,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	) AS
		iTrnID			NUMBER(10,0);
		iInStockNoCD	VARCHAR2(20);
		iKanbanCD		VARCHAR2(20);
		iUserCD			VARCHAR2(20);
		iUserTX			VARCHAR2(50);
		iProgramCD		VARCHAR2(50)			:= 'InStockCMD_0104';
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RegKanban';
		--追加引数
		iArrivID		TA_INSTOCK.ARRIV_ID%TYPE; 	--NUMBER(10);
		rowUser			VA_USER%ROWTYPE;
		ret				VARCHAR2(2);
		chrSlipNo		VARCHAR2(60);				-- 伝票番号
		NextFlg			VARCHAR2(1);				-- 次予定判定フラグ
		setKanbanFlg	VARCHAR2(1) := '0';			-- TA_KANBANPテーブルINSERT判定フラグ
		
		CkanbanCD		TA_LOCASTOCKC.KANBAN_CD%TYPE;
		PkanbanCD		TA_LOCASTOCKP.KANBAN_CD%TYPE;
		
		
		-- マスタ
		CURSOR curKanban(KanbanCD MA_KANBAN.KANBAN_CD%TYPE) IS
			SELECT
				KANBAN_CD
			FROM
				MA_KANBAN
			WHERE
				KANBAN_CD = KanbanCD;
		rowKanban curKanban%ROWTYPE;
		-- 使用済みかんばん
		CURSOR curUseKanban(KanbanCD TA_KANBANP.KANBAN_CD%TYPE) IS
			SELECT
				KANBAN_CD
			FROM
				TA_KANBANP
			WHERE
				KANBAN_CD = KanbanCD;
		rowUseKanban curUseKanban%ROWTYPE;
		-- 格納テーブル
		CURSOR curInStock(ARRIV_ID TA_INSTOCK.ARRIV_ID%TYPE) IS
			SELECT
				TI.SKU_CD		,
				TI.LOT_TX		,
				TI.CASEPERPALLET_NR	,
				TI.PALLETLAYER_NR	,
				TI.STACKINGNUMBER_NR,
				TI.TYPEPLALLET_CD	,
				TI.DESTLOCA_CD	,
				ML.WH_CD		,
				ML.FLOOR_CD		,
				ML.AREA_CD		,
				ML.LINE_CD		,
				TI.DESTLOCASTOCKC_ID AS LOCASTOCKC_ID
			FROM
				TA_INSTOCK TI,
				MA_LOCA ML
			WHERE
				TI.DESTLOCA_CD	= ML.LOCA_CD	AND
				TI.ARRIV_ID		= iArrivID		AND
				TI.STINSTOCK_ID = 0;
		rowInStock curInStock%ROWTYPE;
		numKanbanPID NUMBER(10,0);
		
		CURSOR curSetKanban(iArriv_ID TA_INSTOCK.ARRIV_ID%TYPE) IS
			SELECT
				TK.LOCA_CD	,
				TK.KANBAN_CD
			FROM
				TA_LOCASTOCKC TK
					INNER JOIN TA_INSTOCK TS
						ON TK.LOCASTOCKC_ID = TS.DESTLOCASTOCKC_ID
			WHERE
				TS.ARRIV_ID = iArriv_ID AND	
				TS.STINSTOCK_ID = PS_DEFINE.ST_INSTOCK0_NOTINSTOCK;  --未格納のみ
				
		rowRKanban curSetKanban%ROWTYPE;
		tKanbanCD TA_LOCASTOCKC.KANBAN_CD%TYPE;
		
		-- 次入予定確認カーソル
		CURSOR curTarget(SlipNo TA_ARRIV.SLIPNO_TX%TYPE,
						 UserCD TA_ARRIV.CHECKUSER_CD%TYPE
		) IS
			SELECT 
				*
			FROM
				TA_ARRIV TA	
			WHERE
				RTRIM(TA.SLIPNO_TX) = SlipNo AND 					--初期テスト時カラムに空白が有った為トリム追加
				( 
				TA.STARRIV_ID = PS_DEFINE.ST_ARRIV3_ARRIV_E			-- 3:入荷受付完了
				OR
					(
					TA.STARRIV_ID = PS_DEFINE.ST_ARRIV4_ARRIV_CHK 	-- 4:入庫検品中
					AND
					CHECKUSER_CD = iHtUserCD						-- 作業者単位でチェック
					)
				)
			ORDER BY
				TA.STARRIV_ID DESC	,  --自HTで1回以上呼び出しているデータから処理する
				TA.ARRIVPLANPCS_NR DESC;
		rowTarget 		curTarget%ROWTYPE;
		
		--ロック用カーソル
		CURSOR curLock(LocaCD TA_LOCALOCK.LOCA_CD%TYPE) IS
			SELECT
				LOCA_CD
			FROM
				TA_LOCALOCK
			WHERE
				LOCA_CD = LocaCD 
			FOR UPDATE;
		rowLock		curLock%ROWTYPE;
		
		--ロック用カーソル カンバン
		CURSOR curLockKanban(KanbanCD MA_KANBAN.KANBAN_CD%TYPE) IS
			SELECT
				KANBAN_CD
			FROM
				MA_KANBAN
			WHERE
				KANBAN_CD = KanbanCD
			FOR UPDATE;
		rowLockKanban		curLockKanban%ROWTYPE;
		
		-- 引当済在庫
		CURSOR curLocaC(LocaCD TA_LOCASTOCKC.LOCA_CD%TYPE,
						CStockID TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE
		) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKC
			WHERE
				LOCA_CD = LocaCD AND
				LOCASTOCKC_ID <> CStockID;
		rowLocaC	curLocaC%ROWTYPE;
		
		
		-- 引当可能在庫
		CURSOR curLocaP(LocaCD TA_LOCASTOCKP.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD = LocaCD;
		rowLocaP	curLocaP%ROWTYPE;
		
		
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		--HTからの送信データ
		
		PH_COMM.GetData(iRxCommand,6,iUserCD);		--担当者
		PS_MASTER.GetUser(iUserCD, rowUser);
		PH_COMM.GetData(iRxCommand,7,iArrivID);		--入荷予定ID
		PH_COMM.GetData(iRxCommand,8,iKanbanCD);	--カンバンコード
		PH_COMM.GetData(iRxCommand,9,chrSlipNo);	--伝票番号
		
		OPEN curInStock(iArrivID);
		FETCH curInStock INTO rowInStock;
		CLOSE curInStock;
	
		--ロック用カーソル
		OPEN curLock(rowInStock.DESTLOCA_CD);
		FETCH curLock INTO rowLock;
		IF curLock%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーションロックテーブルが有りません。ロケ=[' || rowInStock.DESTLOCA_CD || ']');
		END IF;
		
		--ロック用カーソル
		OPEN curLockKanban(iKanbanCD);
		FETCH curLockKanban INTO rowLockKanban;
		IF curLockKanban%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんがマスタに登録されていません。KANBAN_CD=[' || iKanbanCD || ']');
		END IF;
		
		--20140517_チェック追加
		--引当済にも可能にもカンバンの登録が無い、ロケ在庫が無い場合はスルー
		OPEN curLocaC(rowInStock.DESTLOCA_CD, rowInStock.LOCASTOCKC_ID);
		FETCH curLocaC INTO rowLocaC;
		IF curLocaC%FOUND THEN
			CkanbanCD := rowLocaC.KANBAN_CD;
			PLOG.DEBUG(logCtx, 'CkanbanCD(true) = ' || CkanbanCD );
		ELSE
			CkanbanCD := ' ';
			PLOG.DEBUG(logCtx, 'CkanbanCD(false) = ' || CkanbanCD );
		END IF;
		CLOSE curLocaC;
		
		OPEN curLocaP(rowInStock.DESTLOCA_CD);
		FETCH curLocaP INTO rowLocaP;
		IF curLocaP%FOUND THEN
			PkanbanCD := rowLocaP.KANBAN_CD;
			PLOG.DEBUG(logCtx, 'PkanbanCD(true) = ' || PkanbanCD );
		ELSE
			PkanbanCD := ' ';
			PLOG.DEBUG(logCtx, 'PkanbanCD(false) = ' || PkanbanCD );
		END IF;
		CLOSE curLocaP;
		
		IF PkanbanCD = ' ' AND CkanbanCD = ' ' THEN
			--ここに入る場合はOK
			NULL;
		ELSE
			IF iKanbanCD <> PkanbanCD AND PkanbanCD <> ' ' THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指定のカンバンで再度入力して下さい。ｶﾝﾊﾞﾝ=[' || PkanbanCD || ']');
			END IF;
			IF iKanbanCD <> CkanbanCD AND CkanbanCD <> ' ' THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指定のカンバンで再度入力して下さい。ｶﾝﾊﾞﾝ=[' || CkanbanCD || ']');
			END IF;
		END IF;
		
		
		OPEN curKanban(iKanbanCD);
		FETCH curKanban INTO rowKanban;
		IF curKanban%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんがマスタに登録されていません。KANBAN_CD=[' || iKanbanCD || ']');
		END IF;
		CLOSE curKanban;
		
		--
		OPEN curSetKanban(iArrivID);
		FETCH curSetKanban INTO rowRKanban;
		IF curSetKanban%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '引当済在庫が有りません。KANBAN_CD=[' || iKanbanCD || ']');
		END IF;
		CLOSE curSetKanban;
		--格納引当時にTA_LOCASTOCKCに登録されたカンバンコードを取得
  		tKanbanCD := rowRKanban.KANBAN_CD; 
		
		OPEN curUseKanban(iKanbanCD);
		FETCH curUseKanban INTO rowUseKanban;
		PLOG.DEBUG(logCtx, 'tKanbanCD = ' || tKanbanCD );
		IF tKanbanCD = ' ' THEN  
			IF PkanbanCD = ' ' AND CkanbanCD = ' ' THEN
				IF curUseKanban%FOUND THEN
					RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんが使用されています。' || CHR(13) || CHR(10) || 'ｶﾝﾊﾞﾝ=[' || iKanbanCD || ']');
				END IF;
			ELSE
				IF curUseKanban%FOUND THEN
					setKanbanFlg := '1';
				END IF;
			END IF;
		ELSE
			IF curUseKanban%FOUND THEN --一致している場合は該当のカンバンがTA_KANBANPに登録されている事を確認
				setKanbanFlg := '1';
			END IF;
		END IF;
		CLOSE curUseKanban;
		
		
		-- 更新
		-- TA_INSTOCK
		UPDATE
			TA_INSTOCK
		SET
			KANBAN_CD		= iKanbanCD			,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			ARRIV_ID		= iArrivID	AND
			STINSTOCK_ID 	= 0;
			
		-- TA_LOCASTOCKC
		UPDATE
			TA_LOCASTOCKC
		SET
			KANBAN_CD		= iKanbanCD			,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			LOCASTOCKC_ID	= rowInStock.LOCASTOCKC_ID;
			
		--TA_ARRIVにカンバンをUPDATE
		UPDATE
			TA_ARRIV
		SET
			KANBAN_CD		= iKanbanCD			,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			ARRIV_ID		= iArrivID;
			
		--TA_KANBANPに登録の無い時のみINSERTする
		IF setKanbanFlg = '0' THEN
			-- ID取得
			SELECT
				NVL(MAX(KANBANP_ID),0)
			INTO
				numKanbanPID
			FROM
				TA_KANBANP;
			numKanbanPID := numKanbanPID + 1;
			-- 引当済かんばん登録
			INSERT INTO TA_KANBANP (
				KANBANP_ID			,
				STKANBAN_CD			,
				TYPEPKANBAN_CD		,
				KANBAN_CD			,
				WH_CD				,
				FLOOR_CD			,
				AREA_CD				,
				LINE_CD				,
				SKU_CD				,
				LOT_TX				,
				CASEPERPALLET_NR	,
				PALLETLAYER_NR		,
				STACKINGNUMBER_NR	,
				TYPEPLALLET_CD		,
				-- 管理項目
				UPD_DT				,
				UPDUSER_CD			,
				UPDUSER_TX			,
				ADD_DT				,
				ADDUSER_CD			,
				ADDUSER_TX			,
				UPDPROGRAM_CD		,
				UPDCOUNTER_NR		,
				STRECORD_ID			
			) VALUES (
				numKanbanPID				,
				'00'						,
				'00'						,
				iKanbanCD					,
				rowInstock.WH_CD			,
				rowInstock.FLOOR_CD			,
				rowInstock.AREA_CD			,
				rowInstock.LINE_CD			,
				rowInstock.SKU_CD			,
				rowInstock.LOT_TX			,
				rowInstock.CASEPERPALLET_NR	,
				rowInstock.PALLETLAYER_NR	,
				rowInstock.STACKINGNUMBER_NR,
				rowInstock.TYPEPLALLET_CD	,
				--
				SYSDATE						,
				iUserCD						,
				rowUser.USER_TX				,
				SYSDATE						,
				iUserCD						,
				rowUser.USER_TX				,
				FUNCTION_NAME				,
				0							,
				0
			);
		END IF;
		
		CLOSE curLockKanban;
		CLOSE curLock;
		
		NextFlg := '0';		-- 0の場合は次レコード無しとする
		--次の入荷予定の確認、有るか無いかだけ
		OPEN curTarget(chrSlipNo, iUserCD);
		FETCH curTarget INTO rowTarget;
		IF curTarget%FOUND THEN
			NextFlg := '1';
		END IF;
		CLOSE curTarget;
		
		ret := '00' ;
		oRet := ret || ',' || NextFlg; -- || ',' || oKanbanCD;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curKanban%ISOPEN THEN
				CLOSE curKanban;
			END IF;
			IF curUseKanban%ISOPEN THEN
				CLOSE curUseKanban;
			END IF;
			IF curInStock%ISOPEN THEN
				CLOSE curInStock;
			END IF;
			RAISE;
	END InStockCMD_0104;
	/************************************************************/
	/*	コマンド別メニュー										*/
	/************************************************************/
	PROCEDURE HTCommandMenu(
		iTypeHtOpeCD	IN VARCHAR2,
		iTypeHtCmdCD	IN VARCHAR2,
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2,
		iAppCd			IN VARCHAR2,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	) AS
		PROGRAM_ID 			CONSTANT VARCHAR2(50)	:= 'PH_HTINSTOCK.HTCommandMenu';
		CMD_0101			CONSTANT VARCHAR2(4)  := '0101';
		CMD_0102			CONSTANT VARCHAR2(4)  := '0102';
		CMD_0103			CONSTANT VARCHAR2(4)  := '0103';
		CMD_0104			CONSTANT VARCHAR2(4)  := '0104';
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ':' || ' START');
		CASE iTypeHtOpeCD || iTypeHtCmdCD
			WHEN CMD_0101 THEN		-- ケース一覧取得
				InStockCMD_0101(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0102 THEN		-- JAN送信
				InStockCMD_0102(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0103 THEN		-- 入庫登録
				InStockCMD_0103(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0104 THEN		-- かんばん登録
				InStockCMD_0104(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			ELSE
				RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'予期せぬエラー');
		END CASE;
		PLOG.DEBUG(logCtx, PROGRAM_ID || ':' || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
	END HTCommandMenu;

END PH_HTINSTOCK;
/
SHOW ERRORS
