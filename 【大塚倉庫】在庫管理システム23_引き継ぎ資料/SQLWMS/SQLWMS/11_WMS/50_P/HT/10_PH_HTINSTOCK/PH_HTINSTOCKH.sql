--Copyright (C) Seaos Corporation 2012 All rights reserved
CREATE OR REPLACE PACKAGE PH_HTINSTOCK AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PH_HTINSTOCK';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.HTLOG_LEVEL);
	/************************************************************/
	/*	伝票番号チェック										*/
	/************************************************************/
	PROCEDURE InStockCMD_0101(
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2,
		iAppCd			IN VARCHAR2,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	);
	/************************************************************/
	/*	JAN送信													*/
	/************************************************************/
	PROCEDURE InStockCMD_0102(
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2,
		iAppCd			IN VARCHAR2,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	);
	/************************************************************/
	/*	入庫登録												*/
	/************************************************************/
	PROCEDURE InStockCMD_0103(
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2,
		iAppCd			IN VARCHAR2,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	);
	/************************************************************/
	/*	かんばん登録											*/
	/************************************************************/
	PROCEDURE InStockCMD_0104(
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2,
		iAppCd			IN VARCHAR2,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	);
	/************************************************************/
	/*	コマンド別メニュー										*/
	/************************************************************/
	PROCEDURE HTCommandMenu(
		iTypeHtOpeCD	IN VARCHAR2,
		iTypeHtCmdCD	IN VARCHAR2,
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2,
		iAppCd			IN VARCHAR2,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	);
END PH_HTINSTOCK;
/
SHOW ERRORS
