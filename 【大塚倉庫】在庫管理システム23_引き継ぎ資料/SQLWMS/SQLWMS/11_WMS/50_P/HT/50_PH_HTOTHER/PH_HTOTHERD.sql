-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PH_HTOTHER AS 
	/************************************************************/
	/*	カンバンチェック										*/
	/************************************************************/
	PROCEDURE OtherCMD_0101(
		iHtUserCD		IN VARCHAR2	,
		iHtUserTX		IN VARCHAR2	,
		iHtID			IN NUMBER,
		iAppCd			IN VARCHAR2	,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTOTHER.OtherCMD_0101';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信データ
		chriKanbanCD		VARCHAR2(20)	:= ' ';
		-- 送信データ
		chrFoundFlg			VARCHAR2(1)						:= '0'; -- データの有る無しをHTで判断するフラグ
		chrSkuCD			MA_SKU.SKU_CD%TYPE				:= ' ';
		chrSkuTX			MA_SKU.SKU_TX%TYPE				:= ' ';
		chrSkuTx2			MA_SKU.SKU2_TX%TYPE				:= ' ';
		chrExteriorCD		MA_SKU.EXTERIOR_CD%TYPE			:= ' ';
		chrSizeTx			MA_SKU.SIZE_TX%TYPE				:= ' ';
		chrLot				VH_STOCK.LOT_TX%TYPE			:= ' ';
		--総在庫
		numTPallet			VH_STOCK.TOTALPALLET_NR%TYPE	:= 0;
		numTCase			VH_STOCK.TOTALCASE_NR%TYPE		:= 0;
		numTBara			VH_STOCK.TOTALBARA_NR%TYPE		:= 0;
		numTPcs				VH_STOCK.TOTALPCS_NR%TYPE		:= 0;
		--引当可能
		numPPallet			VH_STOCK.PPALLET_NR%TYPE		:= 0;
		numPCase			VH_STOCK.PCASE_NR%TYPE			:= 0;
		numPBara			VH_STOCK.PBARA_NR%TYPE			:= 0;
		numPPcs				VH_STOCK.PPCS_NR%TYPE			:= 0;
		--引当済
		numCPallet			VH_STOCK.CPALLET_NR%TYPE		:= 0;
		numCCase			VH_STOCK.CCASE_NR%TYPE			:= 0;
		numCBara			VH_STOCK.CBARA_NR%TYPE			:= 0;
		numCPcs				VH_STOCK.CPCS_NR%TYPE			:= 0;
		--その他
		numAmount			VH_STOCK.AMOUNT_NR%TYPE			:= 0;
		numLineUnit			VH_STOCK.LINEUNIT_NR%TYPE		:= 0;
		numPalletCapacity	VH_STOCK.PALLETCAPACITY_NR%TYPE	:= 0;
		numPalletLayer		VH_STOCK.PALLETLAYER_NR%TYPE	:= 0;
		chrSkuBarcode		VH_STOCK.SKUBARCODE_TX%TYPE		:= ' ';
		chrItfCode			VH_STOCK.ITFCODE_TX%TYPE		:= ' ';
		
		
		-- 在庫取得カーソル
		CURSOR curKanban(KanbanCD VH_STOCK.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				VH_STOCK			-- カンバンチェック用VIEW
			WHERE
				KANBAN_CD = KanbanCD;
			
		rowKanban 		curKanban%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		ret := '';
		msg := '';
		-- ロケ
		PH_COMM.GetData(iRxCommand,7,chriKanbanCD);
		OPEN curKanban(chriKanbanCD);
		FETCH curKanban INTO rowKanban;
		-- カンバン存在チェック
		IF curKanban%FOUND THEN
			chrFoundFlg := '1';
		END IF;
		CLOSE curKanban;
		
		IF chrFoundFlg = '1' THEN
			chrSkuCD := rowKanban.SKU_CD;
			PH_COMM.GetSplitOne(rowKanban.SKU_TX, 60, chrSkuTX);
			PH_COMM.GetSplitOne(rowKanban.SKU2_TX, 60, chrSkuTx2);
			chrExteriorCD := rowKanban.EXTERIOR_CD;
			PH_COMM.GetSplitOne(rowKanban.SIZE_TX, 60, chrSizeTx);
			chrLot := rowKanban.LOT_TX;
			--総在庫
			numTPallet := rowKanban.TOTALPALLET_NR;
			numTCase := rowKanban.TOTALCASE_NR;
			numTBara := rowKanban.TOTALBARA_NR;
			numTPcs := rowKanban.TOTALPCS_NR;
			--引当可能
			numPPallet := rowKanban.PPALLET_NR;
			numPCase := rowKanban.PCASE_NR;
			numPBara := rowKanban.PBARA_NR;
			numPPcs := rowKanban.PPCS_NR;
			--引当済
			numCPallet := rowKanban.CPALLET_NR;
			numCCase := rowKanban.CCASE_NR;
			numCBara := rowKanban.CBARA_NR;
			numCPcs := rowKanban.CPCS_NR;
			--その他
			numAmount := rowKanban.AMOUNT_NR;
			numLineUnit := rowKanban.LINEUNIT_NR;
			numPalletCapacity := rowKanban.PALLETCAPACITY_NR;
			numPalletLayer := rowKanban.PALLETLAYER_NR;
			chrSkuBarcode := rowKanban.SKUBARCODE_TX;
			chrItfCode := rowKanban.ITFCODE_TX;
			
		END IF;
		ret := '00';
		oRet := ret || ',' || chrFoundFlg || ',' || chrSkuCD || ',' || chrSkuTX || ',' || chrSkuTx2 || ',' || chrExteriorCD || ',' || chrSizeTx || ',' || chrLot || ',' || 
				numTPallet || ',' || numTCase || ',' || numTBara || ',' || numTPcs || ',' || 
				numPPallet || ',' || numPCase || ',' || numPBara || ',' || numPPcs || ',' ||
				numCPallet || ',' || numCCase || ',' || numCBara || ',' || numCPcs || ',' ||
				numAmount || ',' || numLineUnit || ',' || numPalletCapacity || ',' || numPalletLayer || ',' || 
				chrSkuBarcode || ',' || chrItfCode || ',' || msg;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriKanbanCD);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriKanbanCD);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END OtherCMD_0101;
	/************************************************************/
	/*	SKUコード送信											*/
	/************************************************************/
	PROCEDURE OtherCMD_0201(
		iHtUserCD		IN VARCHAR2	,
		iHtUserTX		IN VARCHAR2	,
		iHtID 			IN NUMBER,
		iAppCd			IN VARCHAR2	,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTOTHER.OtherCMD_0201';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信データ
		chrSkuBarcodeTx		VARCHAR2(20);
		chriSkuCD			VARCHAR2(20)	:= ' ';
		chriRow				VARCHAR2(2)		:= ' ';
		-- 送信データ
		chroStatus			VARCHAR2(2)		:= ' ';
		chroLocaCD1			VARCHAR2(20)	:= ' ';
		chroUseLimitDate1	VARCHAR2(7)		:= ' ';
		chroStockPcs1		VARCHAR2(9)		:= ' ';
		chroCase1			VARCHAR2(5)		:= ' ';
		chroBara1			VARCHAR2(5)		:= ' ';
		chroLocaCD2			VARCHAR2(20)	:= ' ';
		chroUseLimitDate2	VARCHAR2(7)		:= ' ';
		chroStockPcs2		VARCHAR2(9)		:= ' ';
		chroCase2			VARCHAR2(5)		:= ' ';
		chroBara2			VARCHAR2(5)		:= ' ';
		chroLocaCD3			VARCHAR2(20)	:= ' ';
		chroUseLimitDate3	VARCHAR2(7)		:= ' ';
		chroStockPcs3		VARCHAR2(9)		:= ' ';
		chroCase3			VARCHAR2(5)		:= ' ';
		chroBara3			VARCHAR2(5)		:= ' ';
		--
		numPos				NUMBER := 1;
		numRow				NUMBER := 0;
		numStID				NUMBER := 0;
		chrSkuCD			MA_SKU.SKU_CD%TYPE	:= ' ';
		marrTypItemLocaData	PH_OTHER.ArrTypSkuLocaData;
		rowSku			MA_SKU%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		ret := '';
		msg := '';
		-- 受信データ取得
		PH_COMM.GetData(iRxCommand,7,chrSkuBarcodeTx);
		PS_MASTER.GetSkuFromBarcode(chrSkuBarcodeTx, rowSku);
		chriSkuCD := rowSku.SKU_CD;
		chrSkuCD := chriSkuCD;
		PH_COMM.GetData(iRxCommand,8,chriRow);
		numRow := PH_COMM.CToN(chriRow);
		-- ＨＴロケーション在庫
		PH_OTHER.HTGetSkuLocaPcs(iHtID,iHtUserCD,iHtUserTX,chrSkuCD,numRow,marrTypItemLocaData,numStID);
		-- 送データ作成
		chroStatus := SUBSTRB(TO_CHAR(numStID, '09'), 2, 2);
		IF NOT RTRIM(marrTypItemLocaData(1).LOCA_CD) IS NULL THEN
			chroLocaCD1 := marrTypItemLocaData(1).LOCA_CD;
			PH_COMM.UseLimitToHtUseLimit(marrTypItemLocaData(1).USELIMIT_DT, chroUseLimitDate1);
			chroStockPcs1 := marrTypItemLocaData(1).PCS_NR;
			chroCase1 := marrTypItemLocaData(1).CASE_NR;
			chroBara1 := marrTypItemLocaData(1).BARA_NR;
		ELSE
			chroLocaCD1			:= '';
			chroUseLimitDate1	:= '';
			chroStockPcs1		:= '';
			chroCase1			:= '';
			chroBara1			:= '';
		END IF;
		IF NOT RTRIM(marrTypItemLocaData(2).LOCA_CD) IS NULL THEN
			chroLocaCD2 := marrTypItemLocaData(2).LOCA_CD;
			PH_COMM.UseLimitToHtUseLimit(marrTypItemLocaData(2).USELIMIT_DT, chroUseLimitDate2);
			chroStockPcs2 := marrTypItemLocaData(2).PCS_NR;
			chroCase2 := marrTypItemLocaData(2).CASE_NR;
			chroBara2 := marrTypItemLocaData(2).BARA_NR;
		ELSE
			chroLocaCD2			:= '';
			chroUseLimitDate2	:= '';
			chroStockPcs2		:= '';
			chroCase2			:= '';
			chroBara2			:= '';
		END IF;
		IF NOT RTRIM(marrTypItemLocaData(3).LOCA_CD) IS NULL THEN
			chroLocaCD3 := marrTypItemLocaData(3).LOCA_CD;
			PH_COMM.UseLimitToHtUseLimit(marrTypItemLocaData(3).USELIMIT_DT, chroUseLimitDate3);
			chroStockPcs3 := marrTypItemLocaData(3).PCS_NR;
			chroCase3 := marrTypItemLocaData(3).CASE_NR;
			chroBara3 := marrTypItemLocaData(3).BARA_NR;
		ELSE
			chroLocaCD3			:= '';
			chroUseLimitDate3	:= '';
			chroStockPcs3		:= '';
			chroCase3			:= '';
			chroBara3			:= '';
		END IF;
		oRet := '00' || ',' || chroStatus || ',' || chroLocaCD1 || ',' || chroUseLimitDate1 || ',' || chroStockPcs1 || ',' || chroCase1 || ',' || chroBara1
								|| ',' || chroLocaCD2 || ',' || chroUseLimitDate2 || ',' || chroStockPcs2 || ',' || chroCase2 || ',' || chroBara2
								|| ',' || chroLocaCD3 || ',' || chroUseLimitDate3 || ',' || chroStockPcs3 || ',' || chroCase3 || ',' || chroBara3;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' ||  PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriSkuCD || ',' || chriRow);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' ||  PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriSkuCD || ',' || chriRow);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END OtherCMD_0201;
	/************************************************************/
	/*	ロケーション送信（複数SKU版）							*/
	/************************************************************/
	PROCEDURE OtherCMD_0301(
		iHtUserCD		IN VARCHAR2	,
		iHtUserTX		IN VARCHAR2	,
		iHtID			IN NUMBER	,
		iAppCd			IN VARCHAR2	,
		iRxCommand		IN VARCHAR2	,
		oRet			OUT VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTOTHER.OtherCMD_0301';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信データ
		chriLocaCd			VARCHAR2(20);
		chriRow				VARCHAR2(2)		:= ' ';
		-- 送信データ
		chroStatus			VARCHAR2(2)		:= ' ';
		chroSkuCD1			VARCHAR2(20)	:= ' ';
		chroUseLimitDate1	VARCHAR2(7)		:= ' ';
		chroStockPcs1		VARCHAR2(9)		:= ' ';
		chroCase1			VARCHAR2(5)		:= ' ';
		chroBara1			VARCHAR2(5)		:= ' ';
		chroSkuCD2			VARCHAR2(20)	:= ' ';
		chroUseLimitDate2	VARCHAR2(7)		:= ' ';
		chroStockPcs2		VARCHAR2(9)		:= ' ';
		chroCase2			VARCHAR2(5)		:= ' ';
		chroBara2			VARCHAR2(5)		:= ' ';
		chroSkuCD3			VARCHAR2(20)	:= ' ';
		chroUseLimitDate3	VARCHAR2(7)		:= ' ';
		chroStockPcs3		VARCHAR2(9)		:= ' ';
		chroCase3			VARCHAR2(5)		:= ' ';
		chroBara3			VARCHAR2(5)		:= ' ';
		--
		numRow				NUMBER := 0;
		numStID				NUMBER := 0;
		marrTypItemLocaData	PH_OTHER.ArrTypSkuData;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		ret := '';
		msg := '';
		-- 受信データ取得
		PH_COMM.GetData(iRxCommand,7,chriLocaCd);
		PH_COMM.GetData(iRxCommand,8,chriRow);
		numRow := PH_COMM.CToN(chriRow);
		-- ＨＴロケーション在庫
		PH_OTHER.HTGetSkuPcs(iHtID,iHtUserCD,iHtUserTX,chriLocaCd,numRow,marrTypItemLocaData,numStID);
		-- 送データ作成
		chroStatus := SUBSTRB(TO_CHAR(numStID, '09'), 2, 2);
		IF NOT RTRIM(marrTypItemLocaData(1).SKU_CD) IS NULL THEN
			chroSkuCD1 := marrTypItemLocaData(1).SKU_CD;
			PH_COMM.UseLimitToHtUseLimit(marrTypItemLocaData(1).USELIMIT_DT, chroUseLimitDate1);
			chroStockPcs1 := marrTypItemLocaData(1).PCS_NR;
			chroCase1 := marrTypItemLocaData(1).CASE_NR;
			chroBara1 := marrTypItemLocaData(1).BARA_NR;
		ELSE
			chroSkuCD1			:= '';
			chroUseLimitDate1	:= '';
			chroStockPcs1		:= '';
			chroCase1			:= '';
			chroBara1			:= '';
		END IF;
		IF NOT RTRIM(marrTypItemLocaData(2).SKU_CD) IS NULL THEN
			chroSkuCD2 := marrTypItemLocaData(2).SKU_CD;
			PH_COMM.UseLimitToHtUseLimit(marrTypItemLocaData(2).USELIMIT_DT, chroUseLimitDate2);
			chroStockPcs2 := marrTypItemLocaData(2).PCS_NR;
			chroCase2 := marrTypItemLocaData(2).CASE_NR;
			chroBara2 := marrTypItemLocaData(2).BARA_NR;
		ELSE
			chroSkuCD2			:= '';
			chroUseLimitDate2	:= '';
			chroStockPcs2		:= '';
			chroCase2			:= '';
			chroBara2			:= '';
		END IF;
		IF NOT RTRIM(marrTypItemLocaData(3).SKU_CD) IS NULL THEN
			chroSkuCD3 := marrTypItemLocaData(3).SKU_CD;
			PH_COMM.UseLimitToHtUseLimit(marrTypItemLocaData(3).USELIMIT_DT, chroUseLimitDate3);
			chroStockPcs3 := marrTypItemLocaData(3).PCS_NR;
			chroCase3 := marrTypItemLocaData(3).CASE_NR;
			chroBara3 := marrTypItemLocaData(3).BARA_NR;
		ELSE
			chroSkuCD3			:= '';
			chroUseLimitDate3	:= '';
			chroStockPcs3		:= '';
			chroCase3			:= '';
			chroBara3			:= '';
		END IF;
		oRet := '00' || ',' || chroStatus || ',' || chroSkuCD1 || ',' || chroUseLimitDate1 || ',' || chroStockPcs1 || ',' || chroCase1 || ',' || chroBara1
								|| ',' || chroSkuCD2 || ',' || chroUseLimitDate2 || ',' || chroStockPcs2 || ',' || chroCase2 || ',' || chroBara2
								|| ',' || chroSkuCD3 || ',' || chroUseLimitDate3 || ',' || chroStockPcs3 || ',' || chroCase3 || ',' || chroBara3;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' ||  PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriLocaCD || ',' || chriRow);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' ||  PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriLocaCD || ',' || chriRow);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END OtherCMD_0301;
	/************************************************************/
	/*	コマンド別メニュー										*/
	/************************************************************/
	PROCEDURE HTCommandMenu(
		iTypeHtOpeCD	IN VARCHAR2,
		iTypeHtCmdCD	IN VARCHAR2,
		iHtUserCD		IN VARCHAR2	,
		iHtUserTX		IN VARCHAR2	,
		iHtID			IN NUMBER,
		iAppCd			IN VARCHAR2	,
		iRxCommand		IN VARCHAR2,
		oRet			OUT VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HTCommandMenu';
		CMD_0101			CONSTANT VARCHAR2(4)  := '0101';
		CMD_0201			CONSTANT VARCHAR2(4)  := '0201';
		CMD_0301			CONSTANT VARCHAR2(4)  := '0301';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		CASE iTypeHtOpeCD || iTypeHtCmdCD
			WHEN CMD_0101 THEN		-- カンバンチェック
				OtherCMD_0101(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0201 THEN		-- SKUコード送信
				OtherCMD_0201(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0301 THEN		-- ロケーション送信（複数SKU版）
				OtherCMD_0301(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			ELSE
				RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'予期せぬエラー');
		END CASE;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
	END HTCommandMenu;
END PH_HTOTHER;
/
SHOW ERRORS
