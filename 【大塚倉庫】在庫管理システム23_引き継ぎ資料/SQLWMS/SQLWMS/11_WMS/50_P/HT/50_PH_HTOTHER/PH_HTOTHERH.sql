-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PH_HTOTHER IS 
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PH_HTOTHER';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.HTLOG_LEVEL);
	/************************************************************/
	/*	ロケーション送信										*/
	/************************************************************/
	PROCEDURE OtherCMD_0101(
		iHtUserCD		IN VARCHAR2	,
		iHtUserTX		IN VARCHAR2	,
		iHtID			IN NUMBER	,
		iAppCd			IN VARCHAR2	,
		iRxCommand		IN VARCHAR2	,
		oRet			OUT VARCHAR2
	);
	/************************************************************/
	/*	SKUコード送信											*/
	/************************************************************/
	PROCEDURE OtherCMD_0201(
		iHtUserCD		IN VARCHAR2	,
		iHtUserTX		IN VARCHAR2	,
		iHtID 			IN NUMBER	,
		iAppCd			IN VARCHAR2	,
		iRxCommand		IN VARCHAR2	,
		oRet			OUT VARCHAR2
	);
	/************************************************************/
	/*	ロケーション送信（複数SKU版）							*/
	/************************************************************/
	PROCEDURE OtherCMD_0301(
		iHtUserCD		IN VARCHAR2	,
		iHtUserTX		IN VARCHAR2	,
		iHtID			IN NUMBER	,
		iAppCd			IN VARCHAR2	,
		iRxCommand		IN VARCHAR2	,
		oRet			OUT VARCHAR2
	);
	/************************************************************/
	/*	コマンド別メニュー										*/
	/************************************************************/
	PROCEDURE HTCommandMenu(
		iTypeHtOpeCD	IN	VARCHAR2,
		iTypeHtCmdCD	IN	VARCHAR2,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
END PH_HTOTHER;
/
SHOW ERRORS
