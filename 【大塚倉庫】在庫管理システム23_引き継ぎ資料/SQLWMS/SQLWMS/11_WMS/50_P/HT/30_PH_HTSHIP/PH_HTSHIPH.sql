-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PH_HTSHIP AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PH_HTSHIP';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.HTLOG_LEVEL);
	/************************************************************/
	/*	指示書番号送信											*/
	/************************************************************/
	PROCEDURE ShipCMD_0501(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
	/************************************************************/
	/*	カンバン送信											*/
	/************************************************************/
	PROCEDURE ShipCMD_0502(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
	/************************************************************/
	/*	SKU送信													*/
	/************************************************************/
	PROCEDURE ShipCMD_0503(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
	/************************************************************/
	/*	保留送信												*/
	/************************************************************/
	PROCEDURE ShipCMD_0504(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
	/************************************************************/
	/*	全送信													*/
	/************************************************************/
	PROCEDURE ShipCMD_0598(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
	/************************************************************/
	/*	指示書番号送信											*/
	/************************************************************/
	PROCEDURE ShipCMD_0601(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
	/************************************************************/
	/*	SKU送信													*/
	/************************************************************/
	PROCEDURE ShipCMD_0602(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
	/************************************************************/
	/*	PCS送信													*/
	/************************************************************/
	PROCEDURE ShipCMD_0603(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
	/************************************************************/
	/*	保留送信												*/
	/************************************************************/
	PROCEDURE ShipCMD_0604(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
	/************************************************************/
	/*	全送信													*/
	/************************************************************/
	PROCEDURE ShipCMD_0698(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
	/************************************************************/
	/*	コマンド別メニュー										*/
	/************************************************************/
	PROCEDURE HTCommandMenu(
		iTypeHtOpeCD	IN	VARCHAR2,
		iTypeHtCmdCD	IN	VARCHAR2,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	);
END PH_HTSHIP;
/
SHOW ERRORS
