-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PH_HTSHIP AS

	/************************************************************/
	/*	指示書番号送信											*/
	/************************************************************/
	PROCEDURE ShipCMD_0501(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTSHIP.ShipCMD_0501';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信ﾃﾞｰﾀ
		chriPickNoCD		VARCHAR2(10)	:= '';
		
		chrSku1Tx			MA_SKU.SKU2_TX%TYPE;
		chrSizeTx		    MA_SKU.SIZE_TX%TYPE;		--規格
		chrExteriorCd		MA_SKU.EXTERIOR_CD%TYPE;	--外装コード
		chrLotTx	    	TA_PICK.LOT_TX%TYPE;		--ロット
		chrLocaTx			VARCHAR2(20) := '';			--元ロケ
		rowArea				MB_AREA%ROWTYPE;			--エリア
		chrRCOLOR_TX		VARCHAR2(10);
		chrGCOLOR_TX		VARCHAR2(10);
		chrBCOLOR_TX		VARCHAR2(10);
		numCase				NUMBER(5);					--出荷数
		
		--パレット明細から指示取得
		CURSOR curTarget(PickNoCD TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				--TA.PICKNO_CD			,
				TA.PALLETDETNO_CD		,
				TA.SKU_CD				,	--商品コード
				MA.SKU2_TX 				,	--商品名
				MA.SIZE_TX				,	--規格
				MA.EXTERIOR_CD			,	--外装コード
				TA.LOT_TX				,	--ロット
				TA.AMOUNT_NR			,	--入数
				TA.SRCLOCA_CD			,	--元ロケ
				SUM(TA.PCS_NR) AS PCS_NR	--PCS数
			FROM
				TA_PICK TA 
					LEFT OUTER JOIN MA_SKU MA 
						ON TA.SKU_CD = MA.SKU_CD
			WHERE
				TA.PALLETDETNO_CD = PickNoCD AND				-- パレット明細単位
				TA.STPICK_ID = PS_DEFINE.ST_PICK3_PICK_S		-- ピック中 更新されているはず
			GROUP BY
				TA.PALLETDETNO_CD		,
				TA.SKU_CD				,	--商品コード
				MA.SKU2_TX 				,	--商品名
				MA.SIZE_TX				,	--規格
				MA.EXTERIOR_CD			,	--外装コード
				TA.LOT_TX				,	--ロット
				TA.AMOUNT_NR			,	--入数
				TA.SRCLOCA_CD			;	--元ロケ

		rowTarget	curTarget%ROWTYPE;	
		
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		-- 指示書番号
		PH_COMM.GetData(iRxCommand,7,chriPickNoCD);
		-- ＨＴ出荷検品開始
		PH_PICK.HTStartPick(
			iHtID		,
			iHtUserCD	,
			iHtUserTX	,
			chriPickNoCD
		);
		
		OPEN curTarget(chriPickNoCD);
		FETCH curTarget INTO rowTarget;
		IF curTarget%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'ピック指示エラー。');
		END IF;
		CLOSE curTarget;
		
		--商品名
		PH_COMM.GetSplitOne(rowTarget.SKU2_TX, 60, chrSku1Tx);
		--規格
		PH_COMM.GetSplitOne(rowTarget.SIZE_TX, 60, chrSizeTx);
		--外装コード
		PH_COMM.GetSplitOne(rowTarget.EXTERIOR_CD, 20, chrExteriorCd);
		--ロット
		chrLotTx := rowTarget.LOT_TX;
		--元ロケ
		chrLocaTx := SUBSTR(rowTarget.SRCLOCA_CD, 5, 1) || '-' ||
					 SUBSTR(rowTarget.SRCLOCA_CD, 6, 2) || '-' ||
					 SUBSTR(rowTarget.SRCLOCA_CD, 8, 3) || '-' ||
					 SUBSTR(rowTarget.SRCLOCA_CD,11, 2) || '-' ||
					 SUBSTR(rowTarget.SRCLOCA_CD,13, 2);

		PS_MASTER.GetArea(SUBSTR(rowTarget.SRCLOCA_CD, 5, 1), rowArea);
		chrRCOLOR_TX := rowArea.RCOLOR_TX; --R
		chrGCOLOR_TX := rowArea.GCOLOR_TX; --G
		chrBCOLOR_TX := rowArea.BCOLOR_TX; --B

		--',' || chrRCOLOR_TX || ',' || chrGCOLOR_TX || ',' || chrBCOLOR_TX ||

		-- 出荷指示数の算出    Pcs数  / 入数 （標準ケース・メーカー箱の入数）
		numCase	  := rowTarget.PCS_NR / rowTarget.AMOUNT_NR;
		
		ret := '00';
		--					3,商品名			4,規格				5,外装コード			6,ロット		   7,ロケーション		8,R						9,G						10,B				11,出荷指示ケース数
		oRet := ret || ',' || chrSku1Tx || ',' || chrSizeTx || ',' || chrExteriorCd || ',' || chrLotTx || ',' || chrLocaTx || ',' || chrRCOLOR_TX || ',' || chrGCOLOR_TX || ',' || chrBCOLOR_TX || ',' || numCase || ',' || msg;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriPickNoCD);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ','|| chriPickNoCD);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END ShipCMD_0501;
	/************************************************************/
	/*	カンバン送信											*/
	/************************************************************/
	PROCEDURE ShipCMD_0502(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTSHIP.ShipCMD_0502';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信ﾃﾞｰﾀ
		chriPickNoCD		VARCHAR2(10)	:= ' ';
		chriKanbanCd		VARCHAR2(20)	:= ' ';

	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		-- 指示書番号
		PH_COMM.GetData(iRxCommand,7,chriPickNoCD);
		-- カンバン
		PH_COMM.GetData(iRxCommand,8,chriKanbanCd);
		
		-- ＨＴ出荷検品カンバンチェック
		PH_PICK.HTChkKanban(
			iHtID		,
			iHtUserCD	,
			iHtUserTX	,
			chriPickNoCD,
			chriKanbanCd
		);
		
		--oRet := '00';
		ret := '00';
		oRet := ret;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'00000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriPickNoCD);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'00000000') || ',' || iHtUserCD || ',' || iHtUserTX || ','|| chriPickNoCD);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END ShipCMD_0502;
	/************************************************************/
	/*	SKU送信													*/
	/************************************************************/
	PROCEDURE ShipCMD_0503(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTSHIP.ShipCMD_0503';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信ﾃﾞｰﾀ
		chriPickNoCD		VARCHAR2(10)	:= ' ';
		chriKanbanCd		VARCHAR2(20)	:= ' ';
		chrItfBarcodeTx		VARCHAR2(20);
		rowSku				MA_SKU%ROWTYPE;
		
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		-- 指示書番号
		PH_COMM.GetData(iRxCommand,7,chriPickNoCD);
		-- カンバンコード
		PH_COMM.GetData(iRxCommand,8,chriKanbanCd);
		-- ITF
		PH_COMM.GetData(iRxCommand,9,chrItfBarcodeTx);
		
		-- ＨＴ出荷検品SKUチェック
		PH_PICK.HTChkSku(
			iHtID		,
			iHtUserCD	,
			iHtUserTX	,
			chriPickNoCD,
			chriKanbanCd,
			chrItfBarcodeTx
		);
		
		oRet := '00';
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriPickNoCD);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ','|| chriPickNoCD);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END ShipCMD_0503;
	/************************************************************/
	/*	保留送信												*/
	/************************************************************/
	PROCEDURE ShipCMD_0504(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTSHIP.ShipCMD_0504';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信ﾃﾞｰﾀ
		chriPickNoCD		VARCHAR2(10)	:= ' ';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		-- 指示書番号
		PH_COMM.GetData(iRxCommand,7,chriPickNoCD);
		-- ＨＴ保留
		PH_PICK.HTDeferPick(
			iHtID		,
			iHtUserCD	,
			iHtUserTX	,
			chriPickNoCD,
			oRet		--
		);
		oRet := '00';
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriPickNoCD);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ','|| chriPickNoCD);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END ShipCMD_0504;
	/************************************************************/
	/*	全送信													*/
	/************************************************************/
	PROCEDURE ShipCMD_0598(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTSHIP.ShipCMD_0598';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信ﾃﾞｰﾀ
		chriPickNoCD		VARCHAR2(10)	:= '';
		chriShipCase		VARCHAR2(10)	:= '';
		numShipCase			NUMBER(5)		:= 1;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		-- 指示書番号
		PH_COMM.GetData(iRxCommand,7,chriPickNoCD);
		-- 出荷ケース数
		PH_COMM.GetData(iRxCommand,8,chriShipCase);
		numShipCase := PH_COMM.CtoN(chriShipCase);
		-- ＨＴ出荷検品完了
		PH_PICK.HTEndPick(
			iHtID		,
			iHtUserCD	,
			iHtUserTX	,
			chriPickNoCD,
			numShipCase			--配送伝票印刷枚数
		);
		oRet := '00';
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriPickNoCD);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ','|| chriPickNoCD);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END ShipCMD_0598;
	/************************************************************/
	/*	指示書番号送信											*/
	/************************************************************/
	PROCEDURE ShipCMD_0601(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTSHIP.ShipCMD_0601';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		chrSku1Tx			MA_SKU.SKU2_TX%TYPE;
		chrSizeTx		    MA_SKU.SIZE_TX%TYPE;		--規格
		chrExteriorCd		MA_SKU.EXTERIOR_CD%TYPE;	--外装コード
		chrLotTx	    	TA_PICK.LOT_TX%TYPE;		--ロット
		numCase				NUMBER(5);					--出荷数
		-- 受信ﾃﾞｰﾀ
		chriPickNoCD		VARCHAR2(10)	:= ' ';
		--戻りデータ
		chrSkuCD			TA_PICK.SKU_CD%TYPE;
		chrLot				TA_PICK.LOT_TX%TYPE;
		chrLoca				TA_PICK.SRCLOCA_CD%TYPE;
		chrLocaTx			VARCHAR2(20) := '';			--元ロケ
		rowArea				MB_AREA%ROWTYPE;			--エリア
		chrRCOLOR_TX		VARCHAR2(10);
		chrGCOLOR_TX		VARCHAR2(10);
		chrBCOLOR_TX		VARCHAR2(10);
		
		--指示書NOから指示取得
		CURSOR curTarget(PalletDetNo VH_PICK.PALLETDETNO_CD%TYPE,
						 SkuCD VH_PICK.SKU_CD%TYPE,
						 Lot VH_PICK.LOT_TX%TYPE,
						 LocaCD VH_PICK.SRCLOCA_CD%TYPE
		) IS
			SELECT
				*
			FROM
				VH_PICK
			WHERE
				PALLETDETNO_CD = PalletDetNo AND
				SKU_CD = SkuCD AND
				LOT_TX = Lot AND
				SRCLOCA_CD = LocaCD;
				
			rowTarget curTarget%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		-- 指示書番号
		PH_COMM.GetData(iRxCommand,7,chriPickNoCD);
		-- ＨＴ出荷検品開始
		PH_PICK2.HTStartPick(
			iHtID		,
			iHtUserCD	,
			iHtUserTX	,
			chriPickNoCD,
			chrSkuCD	,
			chrLot		,
			chrLoca
		);
		
		OPEN curTarget(chriPickNoCD, chrSkuCD, chrLot, chrLoca);
		FETCH curTarget INTO rowTarget;
		IF curTarget%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'ピック指示エラー。');
		END IF;
		CLOSE curTarget;
		
		--商品名
		PH_COMM.GetSplitOne(rowTarget.SKU2_TX, 60, chrSku1Tx);
		--規格
		PH_COMM.GetSplitOne(rowTarget.SIZE_TX, 60, chrSizeTx);
		--外装コード
		PH_COMM.GetSplitOne(rowTarget.EXTERIOR_CD, 20, chrExteriorCd);
		--ロット
		chrLotTx := rowTarget.LOT_TX;
		--元ロケ
		chrLocaTx := SUBSTR(rowTarget.SRCLOCA_CD, 5, 1) || '-' ||
					 SUBSTR(rowTarget.SRCLOCA_CD, 6, 2) || '-' ||
					 SUBSTR(rowTarget.SRCLOCA_CD, 8, 3) || '-' ||
					 SUBSTR(rowTarget.SRCLOCA_CD,11, 2) || '-' ||
					 SUBSTR(rowTarget.SRCLOCA_CD,13, 2);

		PS_MASTER.GetArea(SUBSTR(rowTarget.SRCLOCA_CD, 5, 1), rowArea);
		chrRCOLOR_TX := rowArea.RCOLOR_TX; --R
		chrGCOLOR_TX := rowArea.GCOLOR_TX; --G
		chrBCOLOR_TX := rowArea.BCOLOR_TX; --B

		-- 出荷指示数の算出    Pcs数  / 入数 （標準ケース・メーカー箱の入数）
		numCase	  := rowTarget.PCS_NR / rowTarget.AMOUNT_NR;
    
		ret := '00';
		--SKUとLOCAをくっつけてHTに帰しておく
		--					3,SKU + Loca	   			  4,商品名			  5,規格			  6,外装コード			  7,ロット			 8.ロケーション		9,R						10,G					11,B					12,出荷指示ケース数
		oRet := ret || ',' || chrSkuCD || chrLoca || ',' || chrSku1Tx || ',' || chrSizeTx || ',' || chrExteriorCd || ',' || chrLotTx || ',' || chrLocaTx || ',' || chrRCOLOR_TX || ',' || chrGCOLOR_TX || ',' || chrBCOLOR_TX || ',' || numCase || ',' || msg;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriPickNoCD);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ','|| chriPickNoCD);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END ShipCMD_0601;
	/************************************************************/
	/*	ｶﾝﾊﾞﾝ送信												*/
	/************************************************************/
	PROCEDURE ShipCMD_0602(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTSHIP.ShipCMD_0602';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信ﾃﾞｰﾀ
		chrPalletDetNo		VARCHAR2(10);
		chriSkuCD			VARCHAR2(60); --SKU + SRCLOCA_CD --MA_SKU.SKU_CD%TYPE;
		chriLot				TA_PICK.LOT_TX%TYPE;
		chrKanbanCD			TA_PICK.KANBAN_CD%TYPE;
		
		CURSOR curView(PalletDetNo VH_PICK.PALLETDETNO_CD%TYPE,
					   SkuCD VARCHAR2 ,
					   Lot VH_PICK.LOT_TX%TYPE,
					   KanbanCD VH_PICK.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				VH_PICK
			WHERE
				PALLETDETNO_CD = PalletDetNo AND
				SKU_CD || SRCLOCA_CD = SkuCD AND
				LOT_TX = Lot AND
				KANBAN_CD = KanbanCD;
				
		rowView 		curView%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		-- パレット明細番号
		PH_COMM.GetData(iRxCommand,7,chrPalletDetNo);
		-- SKU + SRCLOCA_CD
		PH_COMM.GetData(iRxCommand,8,chriSkuCD);
		-- ロット
		PH_COMM.GetData(iRxCommand,9,chriLot);
		-- カンバン
		PH_COMM.GetData(iRxCommand,10,chrKanbanCD);
		OPEN curView(chrPalletDetNo, chriSkuCD, chriLot, chrKanbanCD);
		FETCH curView INTO rowView;
		-- 指示書存在チェック
		IF curView%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'カンバンが違います。');
		END IF;
		CLOSE curView;
		
		oRet := '00';
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chrPalletDetNo || ',' || chrKanbanCD);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ','|| chrPalletDetNo || ',' || chrKanbanCD);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END ShipCMD_0602;
	/************************************************************/
	/*	ITF送信													*/
	/************************************************************/
	PROCEDURE ShipCMD_0603(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTSHIP.ShipCMD_0602';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信ﾃﾞｰﾀ
		chrPalletDetNo		VARCHAR2(10);		--パレット明細番号
		chriSkuCD			VARCHAR2(60); 		--SKU + SRCLOCA_CD  -- MA_SKU.SKU_CD%TYPE;
		chriLot				TA_PICK.LOT_TX%TYPE;
		chrSkuBarcodeTx		VARCHAR2(20);		--ITF

		CURSOR curView(PalletDetNo VH_PICK.PALLETDETNO_CD%TYPE,
					   SkuCD VARCHAR2,
					   Lot TA_PICK.LOT_TX%TYPE,
					   ItfCD MA_SKU.ITFCODE_TX%TYPE) IS
			SELECT
				VR.ROW_NR	,
				VR.SKU_CD	,
				MA.ITFCODE_TX
			FROM
				VH_PICK VR
					LEFT OUTER JOIN MA_SKU MA
						ON VR.SKU_CD = MA.SKU_CD
			WHERE
				VR.PALLETDETNO_CD = PalletDetNo AND
				VR.SKU_CD || SRCLOCA_CD = SkuCD AND
				VR.LOT_TX = Lot AND
				MA.ITFCODE_TX = ItfCD;
		rowView 		curView%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		-- 指示書番号
		PH_COMM.GetData(iRxCommand, 7, chrPalletDetNo);
		-- SKU
		PH_COMM.GetData(iRxCommand, 8, chriSkuCD);
		-- ロット
		PH_COMM.GetData(iRxCommand,9,chriLot);
		-- ITFコード
		PH_COMM.GetData(iRxCommand, 10, chrSkuBarcodeTx);
		
		OPEN curView(chrPalletDetNo, chriSkuCD, chriLot, chrSkuBarcodeTx);
		FETCH curView INTO rowView;
		-- 指示書存在チェック
		IF curView%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '商品が違います。');
		END IF;
		CLOSE curView;
		
		oRet := '00';
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chrPalletDetNo || ',' || chrSkuBarcodeTx);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ','|| chrPalletDetNo || ',' || chrSkuBarcodeTx);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END ShipCMD_0603;
	/************************************************************/
	/*	保留送信												*/
	/************************************************************/
	PROCEDURE ShipCMD_0604(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTSHIP.ShipCMD_0604';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信ﾃﾞｰﾀ
		chriPickNoCD		VARCHAR2(10)	:= ' ';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		-- 指示書番号
		PH_COMM.GetData(iRxCommand,7,chriPickNoCD);
		-- ＨＴ保留
		PH_PICK2.HTDeferPick(
			iHtID		,
			iHtUserCD	,
			iHtUserTX	,
			chriPickNoCD,
			oRet		--
		);
		oRet := '00';
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriPickNoCD);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ','|| chriPickNoCD);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END ShipCMD_0604;
	/************************************************************/
	/*	全送信													*/
	/************************************************************/
	PROCEDURE ShipCMD_0698(
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	) AS
		PROGRAM_ID			CONSTANT VARCHAR2(50)	:= 'PH_HTSHIP.ShipCMD_0698';
		ret					VARCHAR2(2);
		msg					VARCHAR2(1000);
		-- 受信ﾃﾞｰﾀ
		chriPalletDetNo		VARCHAR2(10)	:= '';
		chriSkuCD			VARCHAR2(60);	--SKU + SRCLOCA_CD
		chriLot				TA_PICK.LOT_TX%TYPE;
		chriShipCase		VARCHAR2(10)	:= ' ';
		numShipCase			NUMBER(5)		:= 1;
    	numPickID   		TA_PICK.PICK_ID%TYPE;
		-- 戻しデータ 予め初期化しておく
		oLocaStockFL		VARCHAR2(1);
		chroSkuCD			VARCHAR2(60)				:= ' ';		--SKUCD + SRCLOCA_CD 
		chroLot	    		TA_PICK.LOT_TX%TYPE			:= ' ';		--ロット
		chroLoca			TA_PICK.SRCLOCA_CD%TYPE		:= ' ';		--ロケーション
		chrLocaTx			VARCHAR2(20) 				:= ' ';		--元ロケ
		chrSkuTx			MA_SKU.SKU2_TX%TYPE			:= ' ';		--商品名
		chrSizeTx			MA_SKU.SIZE_TX%TYPE			:= ' ';		--規格
		chrExteriorCd		MA_SKU.EXTERIOR_CD%TYPE		:= ' ';		--外装コード
		numCase				NUMBER(5)					:= 0;		--出荷数
		rowArea				MB_AREA%ROWTYPE;			--エリア
		chrRCOLOR_TX		VARCHAR2(10) := ' ';
		chrGCOLOR_TX		VARCHAR2(10) := ' ';
		chrBCOLOR_TX		VARCHAR2(10) := ' ';
		
		--TA_PICKカーソル このカーソルでループ
		CURSOR curPick(PalletDetNo TA_PICK.PALLETDETNO_CD%TYPE,
					   SkuCD VARCHAR2 ,
					   Lot TA_PICK.LOT_TX%TYPE
		) IS
			SELECT
				*
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = PalletDetNo AND
				SKU_CD || SRCLOCA_CD = SkuCD AND
				LOT_TX = Lot;
			
		rowPick 		curPick%ROWTYPE;
	
		--指示書NOから次指示取得
		CURSOR curTarget(PalletDetNo VH_PICK.PALLETDETNO_CD%TYPE,
						 SkuCD VH_PICK.SKU_CD%TYPE,
						 Lot VH_PICK.LOT_TX%TYPE,
						 LocaCD VH_PICK.SRCLOCA_CD%TYPE
		) IS
			SELECT
				*
			FROM
				VH_PICK
			WHERE
				PALLETDETNO_CD = PalletDetNo AND
				SKU_CD = SkuCD AND
				LOT_TX = Lot AND
				SRCLOCA_CD = LocaCD;
				
		rowTarget curTarget%ROWTYPE;
		
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		--指示書番号
		PH_COMM.GetData(iRxCommand, 7, chriPalletDetNo);
		-- SKU
		PH_COMM.GetData(iRxCommand, 8, chriSkuCD);
		-- ロット
		PH_COMM.GetData(iRxCommand, 9, chriLot);
		-- 出荷ケース数
		PH_COMM.GetData(iRxCommand,10,chriShipCase);
		numShipCase := PH_COMM.CtoN(chriShipCase);
		
		OPEN curPick(chriPalletDetNo, chriSkuCD, chriLot);
		LOOP
			FETCH curPick INTO rowPick;
			EXIT WHEN curPick%NOTFOUND;
		
			numPickID := rowPick.PICK_ID;
		
			-- ＨＴ出荷検品完了
			PH_PICK2.HTEndPick(
				iHtID		,
				iHtUserCD	,
				iHtUserTX	,
				numPickID	,
				oLocaStockFL		--次指示有りフラグ 0:次指示無し  1:次指示有り
			);
		END LOOP;
		CLOSE curPick;
		
		IF oLocaStockFL = '1' THEN --次レコード有りの場合 次のレコードの情報を取得して返す
			
			-- ＨＴ出荷検品開始
			PH_PICK2.HTStartPick(
				iHtID			,
				iHtUserCD		,
				iHtUserTX		,
				chriPalletDetNo	,
				chroSkuCD		,	--OUT
				chroLot			,	--OUT
				chroLoca			--OUT
			);
		
			OPEN curTarget(chriPalletDetNo, chroSkuCD, chroLot, chroLoca);
			FETCH curTarget INTO rowTarget;
			IF curTarget%NOTFOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'ピック指示エラー。');
			END IF;
			CLOSE curTarget;
			-- 商品コード
			chroSkuCD := rowTarget.SKU_CD;
			--商品名
			PH_COMM.GetSplitOne(rowTarget.SKU2_TX, 60, chrSkuTx);
			--規格
			PH_COMM.GetSplitOne(rowTarget.SIZE_TX, 60, chrSizeTx);
			--外装コード
			PH_COMM.GetSplitOne(rowTarget.EXTERIOR_CD, 20, chrExteriorCd);
			--元ロケ
			chrLocaTx := SUBSTR(rowTarget.SRCLOCA_CD, 5, 1) || '-' ||
						 SUBSTR(rowTarget.SRCLOCA_CD, 6, 2) || '-' ||
						 SUBSTR(rowTarget.SRCLOCA_CD, 8, 3) || '-' ||
						 SUBSTR(rowTarget.SRCLOCA_CD,11, 2) || '-' ||
						 SUBSTR(rowTarget.SRCLOCA_CD,13, 2);

			PS_MASTER.GetArea(SUBSTR(rowTarget.SRCLOCA_CD, 5, 1), rowArea);
			chrRCOLOR_TX := rowArea.RCOLOR_TX; --R
			chrGCOLOR_TX := rowArea.GCOLOR_TX; --G
			chrBCOLOR_TX := rowArea.BCOLOR_TX; --B

			-- 出荷指示数の算出    Pcs数  / 入数 （標準ケース・メーカー箱の入数）
			numCase	  := rowTarget.PCS_NR / rowTarget.AMOUNT_NR;
		END IF;
		
		ret := '00';
		--					3,次レコードフラグ	   4,SKU + Loca		 	   		   5,商品名			  6,規格			  7,外装コード			  8,ロット		    9.ロケーション		10,R					11,G					12,B				13,出荷指示ケース数
		oRet := ret || ',' || oLocaStockFL || ',' || chroSkuCD || chroLoca || ',' || chrSkuTx || ',' || chrSizeTx || ',' || chrExteriorCd || ',' || chroLot || ',' || chrLocaTx || ',' || chrRCOLOR_TX || ',' || chrGCOLOR_TX || ',' || chrBCOLOR_TX || ',' || numCase || ',' || msg;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ',' || chriPalletDetNo);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ':' || iAppCd || ',' || PH_COMM.NtoC(iHtID,'000000') || ',' || iHtUserCD || ',' || iHtUserTX || ','|| chriPalletDetNo);
			END IF;
			DBMS_OUTPUT.PUT_LINE(SQLERRM);
			RAISE;
	END ShipCMD_0698;
	/************************************************************/
	/*	コマンド別メニュー										*/
	/************************************************************/
	PROCEDURE HTCommandMenu(
		iTypeHtOpeCD	IN	VARCHAR2,
		iTypeHtCmdCD	IN	VARCHAR2,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iHtID 			IN	NUMBER	,
		iAppCd			IN	VARCHAR2,
		iRxCommand		IN	VARCHAR2,
		oRet			OUT	VARCHAR2
	) AS
		PROGRAM_ID 			CONSTANT VARCHAR2(50)	:= 'PH_HTSHIP.HTCommandMenu';
		CMD_0501			CONSTANT VARCHAR2(4)  := '0501';
		CMD_0502			CONSTANT VARCHAR2(4)  := '0502';
		CMD_0503			CONSTANT VARCHAR2(4)  := '0503';
		CMD_0504			CONSTANT VARCHAR2(4)  := '0504';
		CMD_0598			CONSTANT VARCHAR2(4)  := '0598';
		CMD_0601			CONSTANT VARCHAR2(4)  := '0601';
		CMD_0602			CONSTANT VARCHAR2(4)  := '0602';
		CMD_0603			CONSTANT VARCHAR2(4)  := '0603';
		CMD_0604			CONSTANT VARCHAR2(4)  := '0604';
		CMD_0698			CONSTANT VARCHAR2(4)  := '0698';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' START');
		CASE iTypeHtOpeCD || iTypeHtCmdCD
			WHEN CMD_0501 THEN		-- 指示書番号送信
				ShipCMD_0501(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0502 THEN		-- カンバン送信
				ShipCMD_0502(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0503 THEN		-- SKU送信
				ShipCMD_0503(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0504 THEN		-- 保留
				ShipCMD_0504(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0598 THEN		-- 全送信
				ShipCMD_0598(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0601 THEN		-- 指示書番号送信
				ShipCMD_0601(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0602 THEN		-- SKU送信
				ShipCMD_0602(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0603 THEN		-- PCS送信
				ShipCMD_0603(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0604 THEN		-- 保留
				ShipCMD_0604(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			WHEN CMD_0698 THEN		-- 全送信
				ShipCMD_0698(iHtUserCD,iHtUserTX,iHtID,iAppCd,iRxCommand,oRet);
			ELSE
				RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'予期せぬエラー');
		END CASE;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || PROGRAM_ID || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
	END HTCommandMenu;
END PH_HTSHIP;
/
SHOW ERRORS
