-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PH_PICK2 AS
	/************************************************************************************/
	/*																					*/
	/*									Private											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************************************/
	/*	ＨＴ情報更新																	*/
	/************************************************************************************/
	PROCEDURE StartPick(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickID			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'StartPick';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 初回のみ更新
		UPDATE
			TA_PICK
		SET
			START_DT		= SYSDATE					,
			SRCSTART_DT		= SYSDATE					,	--元開始時刻
			SRCHT_ID		= iHtID						,
			SRCHTUSER_CD	= iHtUserCD					,	--作業者コード
			SRCHTUSER_TX	= iHtUserTX					,	--作業者名
			STPICK_ID		= PS_DEFINE.ST_PICK3_PICK_S	,
			--
			UPD_DT			= SYSDATE					,
			UPDUSER_CD		= iHtUserCD					,
			UPDUSER_TX		= iHtUserTX					,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PICK_ID			= iPickID AND		--ピックIDで指定
			START_DT		IS NULL		;
		-- 検品情報更新
		UPDATE
			TA_PICK
		SET
			CHECKCOUNT_NR	= CHECKCOUNT_NR + 1,
			CHECKSTART_DT	= SYSDATE	,
			CHECKEND_DT		= NULL		,
			CHECKHT_ID		= iHtID		,
			CHECKHTUSER_CD	= iHtUserCD	,
			CHECKHTUSER_TX	= iHtUserTX	,
			--
			UPD_DT			= SYSDATE					,
			UPDUSER_CD		= iHtUserCD					,
			UPDUSER_TX		= iHtUserTX					,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PICK_ID			= iPickID;		--ピックIDで指定
		-- 出荷指示更新
		UPDATE
			TA_SHIPH
		SET
			STSHIP_ID		= PS_DEFINE.ST_SHIP11_PICK_S,
			--
			UPD_DT			= SYSDATE					,
			UPDUSER_CD		= iHtUserCD					,
			UPDUSER_TX		= iHtUserTX					,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			(
				STSHIP_ID	= PS_DEFINE.ST_SHIP9_PAPPLY_E		OR
				STSHIP_ID	= PS_DEFINE.ST_SHIP10_PICKPRINT_E
			) AND
			SHIPH_ID IN (
				SELECT DISTINCT
					SHIPH_ID
				FROM
					TA_PICK
				WHERE
					PICK_ID = iPickID		--ピックIDで指定
			);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			RAISE;
	END StartPick;
	/************************************************************************************/
	/*	ピッキング指示更新																*/
	/************************************************************************************/
	PROCEDURE EndPick(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickID			IN	NUMBER	,
		irowPick		IN	TA_PICK%ROWTYPE,
		iRowLocaStock	IN	TA_LOCASTOCKC%ROWTYPE,
		irowLoca		IN	MA_LOCA%ROWTYPE,
		oLocaStockFL	OUT VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'EndPick';
		LocaStockFL		VARCHAR2(1) := '1';
		
		CURSOR curPick(PickNO TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_PICK TA
			WHERE
				TA.PALLETDETNO_CD = PickNO AND
				TA.STPICK_ID <= PS_DEFINE.ST_PICK3_PICK_S AND
				TA.STPICK_ID <> PS_DEFINE.ST_PICKM2_CANCEL;			--キャンセルは除く
		rowPick 		curPick%ROWTYPE;
		
		CURSOR curPick2(PickNO TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				TA.PICKNO_CD  , 
				TA.STPICK_ID  , 
				TA.SRCLOCASTOCKC_ID	,
				LC.LOCA_CD    , 
				LC.SKU_CD     , 
				LC.LOT_TX      , 
				LC.KANBAN_CD, 
				LC.CASEPERPALLET_NR, 
				LC.PALLETLAYER_NR, 
				LC.PALLETCAPACITY_NR, 
				LC.STACKINGNUMBER_NR, 
				LC.TYPEPLALLET_CD, 
				LC.USELIMIT_DT, 
				LC.BATCHNO_CD, 
				LC.STOCK_DT, 
				LC.AMOUNT_NR, 
				LC.PALLET_NR, 
				LC.CASE_NR, 
				LC.BARA_NR, 
				LC.PCS_NR, 
				LC.TYPESTOCK_CD, 
				LO.TYPELOCA_CD 
			FROM
				TA_PICK TA 
				LEFT OUTER JOIN TA_LOCASTOCKC LC
					ON TA.SRCLOCASTOCKC_ID = LC.LOCASTOCKC_ID
				LEFT OUTER JOIN MA_LOCA LO
					ON LC.LOCA_CD = LO.LOCA_CD
			WHERE
				TA.PALLETDETNO_CD = PickNO AND
				TA.STPICK_ID = PS_DEFINE.ST_PICK4_PICKOK_E;
				
		rowPick2 		curPick2%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 初回のみ更新
		UPDATE
			TA_PICK
		SET
			END_DT			= SYSDATE		,
			STPICK_ID		= PS_DEFINE.ST_PICK4_PICKOK_E,
			--
			UPD_DT			= SYSDATE		,
			UPDUSER_CD		= iHtUserCD		,
			UPDUSER_TX		= iHtUserTX		,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PICK_ID			= iPickID AND
			END_DT			IS NULL;
		
		-- 検品情報更新
		UPDATE
			TA_PICK
		SET
			CHECKEND_DT		= SYSDATE		,
			CHECKHT_ID		= iHtID			,
			CHECKHTUSER_CD	= iHtUserCD		,
			CHECKHTUSER_TX	= iHtUserTX		,
			--
			UPD_DT			= SYSDATE		,
			UPDUSER_CD		= iHtUserCD		,
			UPDUSER_TX		= iHtUserTX		,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PICK_ID			= iPickID;
			
		-- 出荷指示更新
		UPDATE
			TA_SHIPD
		SET
			STSHIP_ID		= PS_DEFINE.ST_SHIP12_PICKOK_E,
			UPD_DT			= SYSDATE					,
			UPDUSER_CD		= iHtUserCD					,
			UPDUSER_TX		= iHtUserTX					,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			STSHIP_ID		= PS_DEFINE.ST_SHIP11_PICK_S	AND
			SHIPH_ID IN (
				SELECT DISTINCT
					SHIPH_ID
				FROM
					TA_PICK
				WHERE
					PICK_ID = iPickID
			);
			
		UPDATE
			TA_SHIPH
		SET
			STSHIP_ID		= PS_DEFINE.ST_SHIP12_PICKOK_E,
			UPD_DT			= SYSDATE					,
			UPDUSER_CD		= iHtUserCD					,
			UPDUSER_TX		= iHtUserTX					,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			STSHIP_ID		= PS_DEFINE.ST_SHIP11_PICK_S	AND
			SHIPH_ID IN (
				SELECT DISTINCT
					SHIPH_ID
				FROM
					TA_PICK
				WHERE
					PICK_ID = iPickID
			);
			
		oLocaStockFL := '1';
		-- 導入初期はHTでのピック完了時に引当済在庫の引き落とし処理を実施する
		-- 引当済在庫の引き落とし、受払いの登録、引当済カンバンの解放（他に引当可能、済の在庫が居ない場合）
		OPEN curPick(irowPick.PALLETDETNO_CD);
		FETCH curPick INTO rowPick;
		-- ピック番号単位で全部完了したら引当可能在庫を落とす
		IF curPick%NOTFOUND THEN
			LocaStockFL := '0';
			oLocaStockFL := '0';
		END IF;
		CLOSE curPick;
		
		IF LocaStockFL = '0' THEN
			OPEN curPick2(irowPick.PALLETDETNO_CD);
			LOOP
				FETCH curPick2 INTO rowPick2;
				EXIT WHEN curPick2%NOTFOUND;
				PLOG.DEBUG(logCtx, 'PICKNO_CD = ' || irowPick.PICKNO_CD || ',' || 'LOCASTOCKC_ID = ' || rowPick2.SRCLOCASTOCKC_ID);
				--引当済在庫の削除
				PS_STOCK.DelC(
					rowPick2.SRCLOCASTOCKC_ID	,
					iHtUserCD					,
					iHtUserTX					,
					FUNCTION_NAME
				);
				
				-- 受払いの登録
				PS_STOCK.InsStockInOut(	' '							,
										' '							,
										rowPick2.LOCA_CD			,
										rowPick2.SKU_CD				,
										rowPick2.LOT_TX				,
										rowPick2.KANBAN_CD			,
										rowPick2.CASEPERPALLET_NR	,
										rowPick2.PALLETLAYER_NR		,
										rowPick2.PALLETCAPACITY_NR	,
										rowPick2.STACKINGNUMBER_NR	,
										rowPick2.TYPEPLALLET_CD		,
										rowPick2.USELIMIT_DT		,
										rowPick2.BATCHNO_CD			,
										rowPick2.STOCK_DT			,
										rowPick2.AMOUNT_NR			,
										rowPick2.PALLET_NR * -1		,
										rowPick2.CASE_NR * -1		,
										rowPick2.BARA_NR * -1		,
										rowPick2.PCS_NR * -1		,
										rowPick2.TYPESTOCK_CD		,
										rowPick2.TYPELOCA_CD		,
										'200'						,		--業務区分 200:出荷
										'PICK'						,		--参照タイプ（システム上の参照先）
										0							,		--RefNoID システム参照ID
										''							,		--備考
										0							,		--要送信フラグ
										iHtUserCD					,
										iHtUserTX					,
										FUNCTION_NAME);
										
			END LOOP;
			CLOSE curPick2;
		END IF;
			
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			RAISE;
	END EndPick;
	/************************************************************************************/
	/*																					*/
	/*									Public											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************/
	/*		ＨＴ出荷検品開始									*/
	/************************************************************/
	PROCEDURE HTStartPick(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickNoCD 		IN	VARCHAR2,
		oSkuCD			OUT VARCHAR2,
		oLot			OUT VARCHAR2,
		oLoca			OUT VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HTStartPick';
		
		--ピッキング終了確認
		CURSOR curPickEnd(PalletDetNo TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				MIN(STPICK_ID) AS STPICK_ID	,
				MAX(DEFER_FL) AS DEFER_FL
			FROM
				VH_PICK
			WHERE
				PALLETDETNO_CD = PalletDetNo
			GROUP BY
				PALLETDETNO_CD;
				
		rowPickEnd 		curPickEnd%ROWTYPE;
		--ピック指示確認
		CURSOR curView(PalletDetNo TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				*
			FROM
				VH_PICK
			WHERE
				PALLETDETNO_CD = PalletDetNo AND
				(
					STPICK_ID = PS_DEFINE.ST_PICK2_PRINT_E OR		-- 2:印刷完了
					STPICK_ID = PS_DEFINE.ST_PICK3_PICK_S			-- 3:ピック中
				)
			ORDER BY
				STPICK_ID DESC		,
				SORTGROUP_ID 		,
				SORTWEIGHT_ID DESC	, --ケース数が6以上のもの:従来通り個数 * ケース当たり重量の降順
				SRCLOCA_CD			; --ケース数が5以下のもの:ロケーションコードの昇順
				
		rowView 		curView%ROWTYPE;
		
		CURSOR curPick(PalletDetNo TA_PICK.PALLETDETNO_CD%TYPE,
					   SkuCD TA_PICK.SKU_CD%TYPE,
					   Lot TA_PICK.LOT_TX%TYPE  ,
					   LocaCD TA_PICK.SRCLOCA_CD%TYPE
		) IS
			SELECT
				*
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = PalletDetNo AND
				SKU_CD = SkuCD AND
				LOT_TX = Lot AND
				SRCLOCA_CD = LocaCD;
			
		rowPick 		curPick%ROWTYPE;
		chrSkuCD		TA_PICK.SKU_CD%TYPE;
		chrLot			TA_PICK.LOT_TX%TYPE;
		chrLoca			TA_PICK.SRCLOCA_CD%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 指示書完了チェック
		OPEN curPickEnd(iPickNoCD);	--パレット明細番号
		FETCH curPickEnd INTO rowPickEnd;
		IF curPickEnd%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号が存在しません。');
		END IF;
		CLOSE curPickEnd;
		IF rowPickEnd.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN			--ピック完了
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '完了済指示書です。');
		END IF;
		IF rowPickEnd.DEFER_FL = 1 THEN			--保留
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示が保留されています。');
		END IF;
		
		--指示書ステータス確認
		OPEN curView(iPickNoCD);	--パレット明細番号
		FETCH curView INTO rowView;
		-- 指示書存在チェック
		IF curView%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号が存在しません。');
		END IF;
		CLOSE curView;
		-- 指示書種類チェック
		IF rowView.TYPEPICK_CD != PS_DEFINE.TYPE_PICK_CASE THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'ケース出荷指示書ではありません。');
		END IF;
		-- 作業者チェック  3:ピック中の作業者が違う場合はエラー
		IF rowView.STPICK_ID = PS_DEFINE.ST_PICK3_PICK_S AND rowView.SRCHTUSER_CD <> iHtUserCD THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '他作業者が作業中です。作業者コード = ' || rowView.SRCHTUSER_CD );
		END IF;
		--変数セット
		chrSkuCD := rowView.SKU_CD;		--SKU
		chrLot := rowView.LOT_TX;		--ロット
		chrLoca := rowView.SRCLOCA_CD;	--ロケ
		
		
		oSkuCD := chrSkuCD;
		oLot := chrLot;
		oLoca := chrLoca;
		
		OPEN curPick(iPickNoCD, chrSkuCD, chrLot, chrLoca);	--パレット明細番号、SKU、LOT、ロケで絞込み
		LOOP
			FETCH curPick INTO rowPick;
			EXIT WHEN curPick%NOTFOUND;
			
			-- 初期化
			PH_PICK2.StartPick(
				iHtID 		,
				iHtUserCD	,
				iHtUserTX	,
				rowPick.PICK_ID --IDを渡すように変更
			);
		END LOOP;
		CLOSE curPick;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END HTStartPick;
	/************************************************************/
	/*	ＨＴ出荷検品SKUチェック									*/
	/************************************************************/
	PROCEDURE HTChkKanban(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickID 		IN	NUMBER,
		iKanbanCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HTChkKanban';
		-- ピッキングデータ
		CURSOR curPick(PickID TA_PICK.PICK_ID%TYPE) IS
			SELECT
				TYPEPICK_CD	,
				START_DT	,
				STPICK_ID	
			FROM
				TA_PICK
			WHERE
				PICK_ID = PickID;
		rowPick 		curPick%ROWTYPE;
		CURSOR curTarget(PickID TA_PICK.PICK_ID%TYPE, KanbanCD TA_PICK.KANBAN_CD%TYPE) IS
			SELECT
				PICK_ID		,
				KANBAN_CD	
			FROM
				TA_PICK
			WHERE
				PICK_ID   = PickID	AND
				KANBAN_CD = KanbanCD;
		rowTarget 		curTarget%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curPick(iPickID);
		FETCH curPick INTO rowPick;
		-- 指示書存在チェック
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号が存在しません。');
		END IF;
		CLOSE curPick;
		-- 指示書種類チェック
		IF rowPick.TYPEPICK_CD != PS_DEFINE.TYPE_PICK_CASE THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'ケース出荷指示書ではありません。');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号がキャンセルされています。');
		END IF;
		-- 印刷完了チェック
		IF rowPick.STPICK_ID < PS_DEFINE.ST_PICK2_PRINT_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が印刷されていません。');
		END IF;
		-- 指示書完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が完了しています。');
		END IF;
		--カンバン確認
		OPEN curTarget(iPickID, iKanbanCD);
		FETCH curTarget INTO rowTarget;
		IF curTarget%NOTFOUND THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'カンバンが違います。');
		END IF;
		CLOSE curTarget;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END HTChkKanban;
	/************************************************************/
	/*	ＨＴ出荷検品SKUチェック									*/
	/************************************************************/
	PROCEDURE HTChkSku(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickCD 		IN	NUMBER  ,
		iITFCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HTChkSku';
		-- TA_PICkを検索
		CURSOR curPick(PickID TA_PICK.PICK_ID%TYPE,
					   ITFCode MA_SKU.ITFCODE_TX%TYPE
		) IS
			SELECT
				TA.PICK_ID, 
				TA.TYPEPICK_CD, 
				TA.STPICK_ID, 
				TA.START_DT, 
				MA.ITFCODE_TX 
			FROM
				TA_PICK TA 
				INNER JOIN MA_SKU MA 
					ON TA.SKU_CD = MA.SKU_CD 
			WHERE
				TA.PICK_ID = PickID AND
				MA.ITFCODE_TX = ITFCode;
		rowPick 		curPick%ROWTYPE;
		
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curPick(iPickCD, iITFCD);
		FETCH curPick INTO rowPick;
		-- 指示書存在チェック
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '商品が違います。');
		END IF;
		CLOSE curPick;
		-- 指示書種類チェック
		IF rowPick.TYPEPICK_CD != PS_DEFINE.TYPE_PICK_CASE THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '該当指示書ではありません。');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号がキャンセルされています。');
		END IF;
		-- 印刷完了チェック
		IF rowPick.STPICK_ID < PS_DEFINE.ST_PICK2_PRINT_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が印刷されていません。');
		END IF;
		-- 指示書完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が完了しています。');
		END IF;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END HTChkSku;
	/************************************************************/
	/*		ＨＴ出荷検品完了									*/
	/************************************************************/
	PROCEDURE HTEndPick(
		iHtID 			IN	NUMBER		,
		iHtUserCD		IN	VARCHAR2	,
		iHtUserTX		IN	VARCHAR2	,
		iPickID 		IN	NUMBER		,
		oLocaStockFL	OUT VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HTEndPick';
		-- ピッキングデータ
		CURSOR curPick(PickID TA_PICK.PICK_ID%TYPE) IS
			SELECT
				*
			FROM
				TA_PICK
			WHERE
				PICK_ID	= PickID;
		rowPick 		curPick%ROWTYPE;
		locaStockID		TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE;		--引当済在庫ID
		--カンバン引き落とし用カーソル
		CURSOR curPick2(PalletCD TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				KANBAN_CD
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD	= PalletCD
			GROUP BY
				KANBAN_CD
			ORDER BY
				KANBAN_CD;
		rowPick2 		curPick2%ROWTYPE;
		--引当済在庫カーソル
		CURSOR curLocaStockC(LocaStockcID TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKC
			WHERE
				LOCASTOCKC_ID = LocaStockcID;
		rowLocaC	curLocaStockC%ROWTYPE;
		--ロケマスタ
		CURSOR curLoca(LocaCD MA_LOCA.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_LOCA
			WHERE
				LOCA_CD = LocaCD;
		rowLoca		curLoca%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curPick(iPickID);
		FETCH curPick INTO rowPick;
		-- 指示書存在チェック
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号が存在しません。');
		END IF;
		CLOSE curPick;
		-- 指示書種類チェック
		IF rowPick.TYPEPICK_CD != PS_DEFINE.TYPE_PICK_CASE THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '該当指示書ではありません。');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号がキャンセルされています。');
		END IF;
		-- 印刷完了チェック
		IF rowPick.STPICK_ID < PS_DEFINE.ST_PICK2_PRINT_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が印刷されていません。');
		END IF;
		-- 指示書完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が完了しています。');
		END IF;
		
		--導入初期はHTでのピック完了時に引当済在庫の引き落とし処理を実施する
		--引当済在庫の引き落とし、受払いの登録、引当済カンバンの解放（他に引当可能、済の在庫が居ない場合）
		locaStockID := rowPick.SRCLOCASTOCKC_ID;
		OPEN curLocaStockC(locaStockID);
		FETCH curLocaStockC INTO rowLocaC;
		-- 指示書存在チェック
		IF curLocaStockC%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '引当済在庫が存在しません。');
		END IF;
		CLOSE curLocaStockC;
		
		OPEN curLoca(rowLocaC.LOCA_CD);
		FETCH curLoca INTO rowLoca;
		IF curLoca%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'ロケーションが有りません。');
		END IF;
		CLOSE curLoca;

		-- ピッキング指示更新
		PH_PICK2.EndPick(
			iHtID 		,
			iHtUserCD	,
			iHtUserTX	,
			iPickID		,
			rowPick		,
			rowLocaC	,
			rowLoca		,
			oLocaStockFL
		);
		
		IF oLocaStockFL = '0' THEN
			OPEN curPick2(rowPick.PALLETDETNO_CD);
			LOOP
				FETCH curPick2 INTO rowPick2;
				EXIT WHEN curPick2%NOTFOUND;
				PLOG.DEBUG(logCtx, 'Delete_kanbanCD=[' || rowPick2.KANBAN_CD || '] ');
				-- かんばん削除
				PS_STOCK.DelKanban(rowPick2.KANBAN_CD);
			END LOOP;
			CLOSE curPick2;
		END IF;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END HTEndPick;
	/************************************************************/
	/*		ＨＴ出荷検品保留									*/
	/************************************************************/
	PROCEDURE HTDeferPick(
		iHtID 			IN	NUMBER		,
		iHtUserCD		IN	VARCHAR2	,
		iHtUserTX		IN	VARCHAR2	,
		iPalletDetNoCD	IN	VARCHAR2	,
		oLot			OUT VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HTDeferPick';
		-- ピッキングデータ
		CURSOR curPick(PalletDetNoCD TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				TYPEPICK_CD	,
				MIN(STPICK_ID) AS STPICK_ID,
				MAX(DEFER_FL) AS DEFER_FL
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD	= PalletDetNoCD
			GROUP BY
				TYPEPICK_CD;
		rowPick 		curPick%ROWTYPE;
		--ロケマスタ
		CURSOR curLoca(LocaCD MA_LOCA.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_LOCA
			WHERE
				LOCA_CD = LocaCD;
		rowLoca		curLoca%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curPick(iPalletDetNoCD);
		FETCH curPick INTO rowPick;
		-- 指示書存在チェック
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号が存在しません。');
		END IF;
		CLOSE curPick;
		-- 指示書種類チェック
		IF rowPick.TYPEPICK_CD != PS_DEFINE.TYPE_PICK_CASE THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '該当指示書ではありません。');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号がキャンセルされています。');
		END IF;
		-- 印刷完了チェック
		IF rowPick.STPICK_ID < PS_DEFINE.ST_PICK2_PRINT_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が印刷されていません。');
		END IF;
		-- 指示書完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が完了しています。');
		END IF;
		IF rowPick.DEFER_FL = 1 THEN			--保留
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示が保留されています。');
		END IF;
		--
		UPDATE TA_PICK
		SET
			STPICK_ID		= PS_DEFINE.ST_PICK2_PRINT_E	,
			DEFER_FL		= 1								,
			UPD_DT			= SYSDATE						,
			UPDUSER_CD		= iHtUserCD						,
			UPDUSER_TX		= iHtUserTX						,
			UPDPROGRAM_CD	= FUNCTION_NAME					,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD	= iPalletDetNoCD;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END HTDeferPick;
END PH_PICK2;
/
SHOW ERRORS
