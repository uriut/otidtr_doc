-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PH_PICK AS
	/************************************************************************************/
	/*																					*/
	/*									Private											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************************************/
	/*	ＨＴ情報更新																	*/
	/************************************************************************************/
	PROCEDURE StartPick(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickNoCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'StartPick';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 初回のみ更新
		UPDATE
			TA_PICK
		SET
			START_DT		= SYSDATE					,
			SRCSTART_DT		= SYSDATE					,	--元開始時刻
			STPICK_ID		= PS_DEFINE.ST_PICK3_PICK_S	,	--ピック状態を3:ピック中に
			SRCHT_ID		= iHtID						,	--元HT
			SRCHTUSER_CD	= iHtUserCD					,	--元HT社員コード
			SRCHTUSER_TX	= iHtUserTX					,	--元HT社員名
			--
			UPD_DT			= SYSDATE					,
			UPDUSER_CD		= iHtUserCD					,
			UPDUSER_TX		= iHtUserTX					,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD	= iPickNoCD AND					--パレット明細番号単位で更新
			SRCSTART_DT		IS NULL;
		-- 検品情報更新
		UPDATE
			TA_PICK
		SET
			CHECKSTART_DT	= SYSDATE	,
			CHECKEND_DT		= NULL		,
			CHECKHT_ID		= iHtID		,
			CHECKHTUSER_CD	= iHtUserCD	,
			CHECKHTUSER_TX	= iHtUserTX	,
			--
			UPD_DT			= SYSDATE					,
			UPDUSER_CD		= iHtUserCD					,
			UPDUSER_TX		= iHtUserTX					,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD	= iPickNoCD;					--パレット明細番号単位で更新
			
		-- 出荷指示更新
		UPDATE
			TA_SHIPH
		SET
			STSHIP_ID		= PS_DEFINE.ST_SHIP11_PICK_S,
			--
			UPD_DT			= SYSDATE					,
			UPDUSER_CD		= iHtUserCD					,
			UPDUSER_TX		= iHtUserTX					,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			(
				STSHIP_ID	= PS_DEFINE.ST_SHIP9_PAPPLY_E		OR
				STSHIP_ID	= PS_DEFINE.ST_SHIP10_PICKPRINT_E
			) AND
			SHIPH_ID IN (
				SELECT DISTINCT
					SHIPH_ID
				FROM
					TA_PICK
				WHERE
					PALLETDETNO_CD	= iPickNoCD
					--PICKNO_CD = iPickNoCD
			);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			RAISE;
	END StartPick;
	/************************************************************************************/
	/*	ピッキング指示更新																*/
	/************************************************************************************/
	PROCEDURE EndPick(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickNoCD		IN	VARCHAR2,
		iPrintNum		IN	NUMBER	,
		irowPick		IN	TA_PICK%ROWTYPE,
		irowLocaStock	IN	TA_LOCASTOCKC%ROWTYPE,
		irowLoca		IN	MA_LOCA%ROWTYPE,
		KanbanPflg		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'EndPick';
		oCashFL			NUMBER(1);
		
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 初回のみ更新
		UPDATE
			TA_PICK
		SET
			END_DT			= SYSDATE		,
			STPICK_ID		= PS_DEFINE.ST_PICK4_PICKOK_E,
			--
			UPD_DT			= SYSDATE		,
			UPDUSER_CD		= iHtUserCD		,
			UPDUSER_TX		= iHtUserTX		,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PICK_ID			= irowPick.PICK_ID AND
			END_DT			IS NULL		;
			
		-- 検品情報更新
		UPDATE
			TA_PICK
		SET
			CHECKEND_DT		= SYSDATE		,
			CHECKHT_ID		= iHtID			,
			CHECKHTUSER_CD	= iHtUserCD		,
			CHECKHTUSER_TX	= iHtUserTX		,
			--
			UPD_DT			= SYSDATE		,
			UPDUSER_CD		= iHtUserCD		,
			UPDUSER_TX		= iHtUserTX		,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PICK_ID			= irowPick.PICK_ID;
			
		-- 出荷指示更新（明細）
		UPDATE
			TA_SHIPD
		SET
			STSHIP_ID		= PS_DEFINE.ST_SHIP12_PICKOK_E,
			UPD_DT			= SYSDATE					,
			UPDUSER_CD		= iHtUserCD					,
			UPDUSER_TX		= iHtUserTX					,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			STSHIP_ID		= PS_DEFINE.ST_SHIP11_PICK_S	AND
			SHIPH_ID IN (
				SELECT DISTINCT
					SHIPH_ID
				FROM
					TA_PICK
				WHERE
					PICK_ID		= irowPick.PICK_ID
			);
		-- 出荷指示更新（ヘッダー）
		UPDATE
			TA_SHIPH
		SET
			STSHIP_ID		= PS_DEFINE.ST_SHIP12_PICKOK_E,
			UPD_DT			= SYSDATE					,
			UPDUSER_CD		= iHtUserCD					,
			UPDUSER_TX		= iHtUserTX					,
			UPDPROGRAM_CD	= PACKAGE_NAME || '.' || FUNCTION_NAME,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			STSHIP_ID		= PS_DEFINE.ST_SHIP11_PICK_S	AND
			SHIPH_ID IN (
				SELECT DISTINCT
					SHIPH_ID
				FROM
					TA_PICK
				WHERE
					PICK_ID = irowPick.PICK_ID
			);
			
		-- 導入初期はHTでのピック完了時に引当済在庫の引き落とし処理を実施する
		-- 引当済在庫の引き落とし、受払いの登録、引当済カンバンの解放（他に引当可能、済の在庫が居ない場合）
		--引当済在庫の削除
		PS_STOCK.DelC(
			irowLocaStock.LOCASTOCKC_ID	,
			iHtUserCD					,
			iHtUserTX					,
			FUNCTION_NAME
		);
		
		-- 受払いの登録
		PS_STOCK.InsStockInOut(	' '								,
								' '								,
								irowLocaStock.LOCA_CD			,
								irowLocaStock.SKU_CD			,
								irowLocaStock.LOT_TX			,
								irowLocaStock.KANBAN_CD			,
								irowLocaStock.CASEPERPALLET_NR	,
								irowLocaStock.PALLETLAYER_NR	,
								irowLocaStock.PALLETCAPACITY_NR	,
								irowLocaStock.STACKINGNUMBER_NR	,
								irowLocaStock.TYPEPLALLET_CD	,
								irowLocaStock.USELIMIT_DT		,
								irowLocaStock.BATCHNO_CD		,
								irowLocaStock.STOCK_DT			,
								irowLocaStock.AMOUNT_NR			,
								irowLocaStock.PALLET_NR * -1	,
								irowLocaStock.CASE_NR * -1		,
								irowLocaStock.BARA_NR * -1		,
								irowLocaStock.PCS_NR * -1		,
								irowLocaStock.TYPESTOCK_CD		,
								irowLoca.TYPELOCA_CD			,
								'200'							,		--業務区分 200:出荷
								'PICK'							,		--参照タイプ（システム上の参照先）
								0								,		--RefNoID システム参照ID
								''								,		--備考
								0								,		--要送信フラグ
								iHtUserCD						,
								iHtUserTX						,
								FUNCTION_NAME);
		
		--TA_KANBANPの削除
		IF KanbanPflg = '1' THEN
			DELETE
				TA_KANBANP
			WHERE
				KANBAN_CD = irowLocaStock.KANBAN_CD;
		END IF;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			RAISE;
	END EndPick;
	/************************************************************************************/
	/*																					*/
	/*									Public											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************/
	/*		ＨＴ出荷検品開始									*/
	/************************************************************/
	PROCEDURE HTStartPick(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickNoCD 		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HTStartPick';
		--ピッキングデータ
		CURSOR curPick(PickNoCD TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				TP.TYPEPICK_CD	,
				TP.STPICK_ID	,
				TP.SRCHTUSER_CD	,
				MAX(TP.DEFER_FL) AS DEFER_FL
			FROM
				TA_PICK TP
			WHERE
				TP.PALLETDETNO_CD = PickNoCD
			GROUP BY
				TP.TYPEPICK_CD	,
				TP.STPICK_ID	,
				TP.SRCHTUSER_CD;
		rowPick 	curPick%ROWTYPE;
		
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curPick(iPickNoCD);
		FETCH curPick INTO rowPick;
		-- 指示書存在チェック
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号が存在しません。');
		END IF;
		CLOSE curPick;
		-- 指示書種類チェック
		-- パレットピック（通常）と パレットピック（集約）のどちらかならOK
		IF rowPick.TYPEPICK_CD != PS_DEFINE.TYPE_PICK_PALLET_N AND rowPick.TYPEPICK_CD != PS_DEFINE.TYPE_PICK_PALLET_S THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'パレット出荷指示書ではありません。');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号がキャンセルされています。');
		END IF;
		-- 印刷完了チェック
		IF rowPick.STPICK_ID < PS_DEFINE.ST_PICK2_PRINT_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が印刷されていません。');
		END IF;
		-- 指示書完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が完了しています。');
		END IF;
		-- 保留チェック
		IF rowPick.DEFER_FL = 1 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が保留されています。');
		END IF;
		-- 作業者チェック  3:ピック中の作業者が違う場合はエラー
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICK3_PICK_S AND rowPick.SRCHTUSER_CD <> iHtUserCD THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '他作業者が作業中です。作業者コード = ' || rowPick.SRCHTUSER_CD );
		END IF;
		
		-- 初期化
		PH_PICK.StartPick(
			iHtID 		,
			iHtUserCD	,
			iHtUserTX	,
			iPickNoCD
		);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END HTStartPick;
	/************************************************************/
	/*	ＨＴ出荷検品カンバンチェック							*/
	/************************************************************/
	PROCEDURE HTChkKanban(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickNoCD 		IN	VARCHAR2,
		iKanbanCd		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HTChkKanban';
		-- ピッキングデータ
		CURSOR curPick(PickNoCD TA_PICK.PALLETDETNO_CD%TYPE,
					   KanbanCd TA_PICK.KANBAN_CD%TYPE
		) IS
			SELECT
				STPICK_ID	
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD 	= PickNoCD AND		--パレット明細番号単位で確認
				KANBAN_CD		= KanbanCd;			--カンバンコード
		rowPick 		curPick%ROWTYPE;
		
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curPick(iPickNoCD, iKanbanCd);
		FETCH curPick INTO rowPick;
		-- カンバン存在チェック
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示に含まれないカンバンです。');
		END IF;
		CLOSE curPick;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号がキャンセルされています。');
		END IF;
		-- 印刷完了チェック
		IF rowPick.STPICK_ID < PS_DEFINE.ST_PICK2_PRINT_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が印刷されていません。');
		END IF;
		-- 指示書完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が完了しています。');
		END IF;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END HTChkKanban;
	/************************************************************/
	/*	ＨＴ出荷検品SKUチェック									*/
	/************************************************************/
	PROCEDURE HTChkSku(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickNoCD 		IN	VARCHAR2,
		iKanbanCd		IN	VARCHAR2,
		iItfBarcodeTx	IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HTChkSku';
		-- TA_PICkを検索
		CURSOR curPick(PickID TA_PICK.PALLETDETNO_CD%TYPE,
					   ITFCode MA_SKU.ITFCODE_TX%TYPE
		) IS
			SELECT
				TA.PICK_ID, 
				TA.TYPEPICK_CD, 
				TA.STPICK_ID, 
				TA.START_DT, 
				MA.ITFCODE_TX 
			FROM
				TA_PICK TA 
				INNER JOIN MA_SKU MA 
					ON TA.SKU_CD = MA.SKU_CD 
			WHERE
				TA.PALLETDETNO_CD = PickID AND
				MA.ITFCODE_TX = ITFCode;
		rowPick		curPick%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curPick(iPickNoCD, iItfBarcodeTx);
		FETCH curPick INTO rowPick;
		-- 指示商品ITFチェック
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '商品が違います。');
		END IF;
		CLOSE curPick;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END HTChkSku;
	/************************************************************/
	/*		ＨＴ出荷検品完了									*/
	/************************************************************/
	PROCEDURE HTEndPick(
		iHtID 			IN	NUMBER		,
		iHtUserCD		IN	VARCHAR2	,
		iHtUserTX		IN	VARCHAR2	,
		iPickNoCD 		IN	VARCHAR2	,
		iPickNum		IN	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HTEndPick';
		LocaPflg		VARCHAR2(1);		--引当可能在庫に同一カンバンがいるかのフラグ
		LocaCflg		VARCHAR2(1);		--引当済在庫に同一カンバンがいるかのフラグ
		
		KanbanPflg		VARCHAR2(1);		--TA_KANBANPの削除をおこなうかのフラグ 引当可能、済在庫が居ない場合は'1'をセット
		
		-- ピッキングデータ
		CURSOR curPick(iPickNoCD TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = iPickNoCD;
				
			rowPick 		curPick%ROWTYPE;
			locaStockID		TA_PICK.SRCLOCASTOCKC_ID%TYPE;		--引当済在庫ID
			
		--引当済在庫カーソル
		CURSOR curLocaStockC(LocaStockcID TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKC
			WHERE
				LOCASTOCKC_ID = LocaStockcID;
			
			rowLocaC	curLocaStockC%ROWTYPE;
			
		--ロケマスタ
		CURSOR curLoca(LocaCD MA_LOCA.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_LOCA
			WHERE
				LOCA_CD = LocaCD;
			
			rowLoca		curLoca%ROWTYPE;
		
		--引当可能在庫 カンバン単位で確認
		CURSOR curLocaPKanban(pKanbanCD TA_LOCASTOCKP.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKP
			WHERE
				KANBAN_CD = pKanbanCD;
			
			rowLocaPKanban	curLocaPKanban%ROWTYPE;
		
		--引当済在庫 カンバン単位で確認
		CURSOR curLocaCKanban(LocaStockID TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE,
							  cKanbanCD TA_LOCASTOCKC.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKC
			WHERE
				LOCASTOCKC_ID <> LocaStockID AND	--これから引き落とす在庫は除く
				KANBAN_CD = cKanbanCD;
			
			rowLocaCKanban	curLocaCKanban%ROWTYPE;
		
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 集約パレットピックに対応する為にPALLETDETNO_CD単位でTA_PICKの指示のチェックを実施
		-- TA_PICKの指示、引当済在庫、ロケのチェック
		OPEN curPick(iPickNoCD);
		LOOP
			FETCH curPick INTO rowPick;
			EXIT WHEN curPick%NOTFOUND;
			-- 指示書存在チェック
			IF curPick%NOTFOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号が存在しません。');
			END IF;
			-- 指示書種類チェック
			-- パレットピック（通常）と パレットピック（集約）のどちらかならOK
			IF rowPick.TYPEPICK_CD != PS_DEFINE.TYPE_PICK_PALLET_N AND rowPick.TYPEPICK_CD != PS_DEFINE.TYPE_PICK_PALLET_S THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '該当指示書ではありません。');
			END IF;
			-- キャンセルチェック
			IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号がキャンセルされています。');
			END IF;
			-- 印刷完了チェック
			IF rowPick.STPICK_ID < PS_DEFINE.ST_PICK2_PRINT_E THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が印刷されていません。');
			END IF;
			-- 指示書完了チェック
			IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が完了しています。');
			END IF;
		
			--導入初期はHTでのピック完了時に引当済在庫の引き落とし処理を実施する
			--引当済在庫の引き落とし、受払いの登録、引当済カンバンの解放（他に引当可能、済の在庫が居ない場合）
			locaStockID := rowPick.SRCLOCASTOCKC_ID;
			
			OPEN curLocaStockC(locaStockID);
			FETCH curLocaStockC INTO rowLocaC;
			-- 引当済在庫チェック
			IF curLocaStockC%NOTFOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '引当済在庫が存在しません。');
			END IF;
			CLOSE curLocaStockC;
			-- TODO 引当済在庫のステータスチェック
			
			OPEN curLoca(rowLocaC.LOCA_CD);
			FETCH curLoca INTO rowLoca;
			IF curLoca%NOTFOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, 'ロケーションが有りません。');
			END IF;
			CLOSE curLoca;
		
			--TA_KANBANPの削除可否チェック
			--変数初期化
			LocaPflg := '0';
			LocaCflg := '0';
			KanbanPflg := '0';
			--引当可能在庫のカンバンチェック 同じカンバンの引当可能在庫が居るのかチェック
			OPEN curLocaPKanban(rowLocaC.KANBAN_CD);
			FETCH curLocaPKanban INTO rowLocaPKanban;
			IF curLocaPKanban%NOTFOUND THEN
				LocaPflg := '1';
			END IF;
			CLOSE curLocaPKanban;
			
			--引当済在庫のカンバンチェック 同じカンバンの引当済在庫が居るのかチェック
			OPEN curLocaCKanban(locaStockID, rowLocaC.KANBAN_CD);
			FETCH curLocaCKanban INTO rowLocaCKanban;
			IF curLocaCKanban%NOTFOUND THEN
				LocaCflg := '1';
			END IF;
			CLOSE curLocaCKanban;
			
			--引当可能も済も他にカンバンが居ない場合はフラグセット
			IF LocaPflg = '1' AND LocaCflg = '1' THEN
				KanbanPflg := '1';
			END IF;
			
			--デバッグ用
			PLOG.DEBUG(logCtx,'EndPick前 PICK_ID =' || rowPick.PICK_ID || ',' || 'LOCASTOCKC_ID =' || rowLocaC.LOCASTOCKC_ID || ',' ||
										'Loca =' || rowLoca.LOCA_CD || ',' || 'KanbanPflg =' || KanbanPflg);
			-- ピッキング指示更新
			PH_PICK.EndPick(
				iHtID 		,
				iHtUserCD	,
				iHtUserTX	,
				iPickNoCD	,
				iPickNum	,
				rowPick		,
				rowLocaC	,
				rowLoca		,
				KanbanPflg
			);
		END LOOP;
		CLOSE curPick;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END HTEndPick;
	/************************************************************/
	/*		ＨＴ出荷検品保留									*/
	/************************************************************/
	PROCEDURE HTDeferPick(
		iHtID 			IN	NUMBER		,
		iHtUserCD		IN	VARCHAR2	,
		iHtUserTX		IN	VARCHAR2	,
		iPalletDetNoCD	IN	VARCHAR2	,
		oLot			OUT VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'HTDeferPick';
		-- ピッキングデータ
		CURSOR curPick(PalletDetNoCD TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				TYPEPICK_CD	,
				MIN(STPICK_ID) AS STPICK_ID,
				MAX(DEFER_FL) AS DEFER_FL
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD	= PalletDetNoCD
			GROUP BY
				TYPEPICK_CD;
		rowPick 		curPick%ROWTYPE;
		--ロケマスタ
		CURSOR curLoca(LocaCD MA_LOCA.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_LOCA
			WHERE
				LOCA_CD = LocaCD;
		rowLoca		curLoca%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curPick(iPalletDetNoCD);
		FETCH curPick INTO rowPick;
		-- 指示書存在チェック
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号が存在しません。');
		END IF;
		CLOSE curPick;
		-- 指示書種類チェック
		-- パレットピック（通常）と パレットピック（集約）のどちらかならOK
		IF rowPick.TYPEPICK_CD != PS_DEFINE.TYPE_PICK_PALLET_N AND rowPick.TYPEPICK_CD != PS_DEFINE.TYPE_PICK_PALLET_S THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '該当指示書ではありません。');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書番号がキャンセルされています。');
		END IF;
		-- 印刷完了チェック
		IF rowPick.STPICK_ID < PS_DEFINE.ST_PICK2_PRINT_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が印刷されていません。');
		END IF;
		-- 指示書完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示書が完了しています。');
		END IF;
		IF rowPick.DEFER_FL = 1 THEN			--保留
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01, '指示が保留されています。');
		END IF;
		--
		UPDATE TA_PICK
		SET
			STPICK_ID		= PS_DEFINE.ST_PICK2_PRINT_E	,
			DEFER_FL		= 1								,
			UPD_DT			= SYSDATE						,
			UPDUSER_CD		= iHtUserCD						,
			UPDUSER_TX		= iHtUserTX						,
			UPDPROGRAM_CD	= FUNCTION_NAME					,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD	= iPalletDetNoCD;
		oLot := '00';
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END HTDeferPick;
END PH_PICK;
/
SHOW ERRORS
