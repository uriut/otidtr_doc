-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PH_PICK AS
	/************************************************************/
	/*	¤Ê													*/
	/************************************************************/
	--pbP[W¼
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PH_PICK';
	-- Ox
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.HTLOG_LEVEL);
	/************************************************************/
	/*	gsîñXV											*/
	/************************************************************/
	PROCEDURE StartPick(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickNoCD 		IN	VARCHAR2
	);
	/************************************************************/
	/*	gso×iJn										*/
	/************************************************************/
	PROCEDURE HTStartPick(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickNoCD 		IN	VARCHAR2
	);
	/************************************************************/
	/*	gso×iJo`FbN							*/
	/************************************************************/
	PROCEDURE HTChkKanban(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickNoCD 		IN	VARCHAR2,
		iKanbanCd		IN	VARCHAR2
	);
	/************************************************************/
	/*	gso×iSKU`FbN									*/
	/************************************************************/
	PROCEDURE HTChkSku(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickNoCD 		IN	VARCHAR2,
		iKanbanCd		IN	VARCHAR2,
		iItfBarcodeTx	IN	VARCHAR2
	);
	/************************************************************/
	/*	gso×i®¹										*/
	/************************************************************/
	PROCEDURE HTEndPick(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPickNoCD 		IN	VARCHAR2,
		iPickNum		IN	NUMBER
	);
	/************************************************************/
	/*	gso×iÛ¯										*/
	/************************************************************/
	PROCEDURE HTDeferPick(
		iHtID 			IN	NUMBER	,
		iHtUserCD		IN	VARCHAR2,
		iHtUserTX		IN	VARCHAR2,
		iPalletDetNoCD	IN	VARCHAR2,
    	oLot			OUT VARCHAR2
	);
END PH_PICK;
/
SHOW ERRORS
