CREATE OR REPLACE PACKAGE BODY PH_HT AS 
	/************************************************************************************/
	/*																					*/
	/*									Private											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************************************/
	/*																					*/
	/*									Public											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************/
	/*	ログイン・全送信										*/
	/************************************************************/
	PROCEDURE InitialCMD_0100(
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_HT.InitialCMD_0000';
		numRowPoint 	NUMBER := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ':' || ' START');
		PH_COMM.SetHtUser(iHtID,iHtUserCD);
		PLOG.DEBUG(logCtx, PROGRAM_ID || ':' || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			RAISE;
	END InitialCMD_0100;
	/************************************************************/
	/*	無線取り合い関数										*/
	/************************************************************/
	PROCEDURE HTCommand(
		iRxCommand IN VARCHAR2,
		oSxCommand OUT VARCHAR2
	) AS
		PROGRAM_ID 	CONSTANT VARCHAR2(50)	:= 'PH_HT.HTCommand';
		varMsg 			VARCHAR2(512)			:= ' ';
		numRowPoint 	NUMBER := 0;
		--
		MENU_TYPE_INITIAL	CONSTANT VARCHAR2(2)  := '00';	-- ログイン
		MENU_TYPE_INSTOCK	CONSTANT VARCHAR2(2)  := '01';	-- 入荷
		MENU_TYPE_MOVE		CONSTANT VARCHAR2(2)  := '02';	-- 移動
		MENU_TYPE_SHIP		CONSTANT VARCHAR2(2)  := '03';	-- 出荷
		MENU_TYPE_OTHER		CONSTANT VARCHAR2(2)  := '0A';	-- その他
		--
		chrAppCd		VARCHAR2(2)								;
		chrHtID			VARCHAR2(6)								;
		chrUserCd		VARCHAR2(10)							;
		chrTypeHt		VARCHAR2(20) := ' '						;
		chrTypeHtOpe	VARCHAR2(20) := ' '						;
		chrTypeHtCmd	VARCHAR2(20) := ' '						;
		chrDhcpFl		VARCHAR2(1)	 := ' '						;
		chrIPAddress	VARCHAR2(15) := ' '						;
		chrDeviceCd		VARCHAR2(20) := ' '						;
		var2Ret			VARCHAR2(1500)							;
		var2SxCommand	VARCHAR2(1500)							;
		chrUserTx		VA_USER.USER_TX%TYPE;
		numHTAppupdFl	ST_HT.APPUPD_FL%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PROGRAM_ID || ':' || ' START');
		PLOG.DEBUG(logCtx, PROGRAM_ID || ':' || iRxCommand);
		--アプリコード
		PH_COMM.GetData(iRxCommand,1,chrAppCd);
		IF NOT chrHtID = 'OT' THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'予期せぬエラー');
		END IF;
		--HTID
		PH_COMM.GetData(iRxCommand,2,chrHtID);
		IF chrHtID IS NULL THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'予期せぬエラー');
		END IF;
		--HT分類
		PH_COMM.GetData(iRxCommand,3,chrTypeHt);
		IF chrTypeHt IS NULL THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'予期せぬエラー');
		END IF;
		--HT業務区分
		PH_COMM.GetData(iRxCommand,4,chrTypeHtOpe);
		IF chrTypeHtOpe IS NULL THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'予期せぬエラー');
		END IF;
		--HTコマンド区分
		PH_COMM.GetData(iRxCommand,5,chrTypeHtCmd);
		IF chrTypeHtCmd IS NULL THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'予期せぬエラー');
		END IF;
		-- HT受信ログ書込
		PH_COMM.LogHtWrite(chrAppCd,chrHtID,chrUserCd,chrTypeHt,chrTypeHtOpe,chrTypeHtCmd,'R',iRxCommand);
		--ログインチェック
		IF chrTypeHt = MENU_TYPE_INITIAL THEN
			IF chrTypeHtCmd = '00' THEN
				--DHCP有無
				PH_COMM.GetData(iRxCommand,6,chrDhcpFl);
				--IPアドレス
				PH_COMM.GetData(iRxCommand,7,chrIPAddress);
				--端末名
				PH_COMM.GetData(iRxCommand,8,chrDeviceCd);
				--自動更新判定
				PH_COMM.GetHtUpdate(chrHtID,chrDhcpFl,chrIPAddress,chrDeviceCd,numHTAppupdFl);
			ELSE
				--ユーザコード
				PH_COMM.GetData(iRxCommand,6,chrUserCd);
				--ユーザー名称
				PH_COMM.GetHtUserForInitial(chrUserCd,chrUserCd,chrUserTx);
			END IF;
		ELSE
			--ユーザコード
			PH_COMM.GetData(iRxCommand,6,chrUserCd);
			--ユーザー名称
			PH_COMM.GetUser(chrUserCd,chrUserTx);
		END IF;
		PLOG.DEBUG(logCtx, chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd);
		CASE chrTypeHt
			WHEN MENU_TYPE_INITIAL THEN
				--ログイン
				IF chrTypeHtOpe = '00' AND chrTypeHtCmd = '01' THEN
					var2Ret := '00';
					var2SxCommand := chrAppCd || ',' || chrHTID || ',' || chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd || ',' || chrUserCd || ',' || var2Ret || ',' || chrUserTx;
				ELSIF chrTypeHtOpe = '00' AND chrTypeHtCmd = '98' THEN
					InitialCMD_0100(chrUserCd,chrUserTx,chrHtID);
					var2Ret := '00';
					var2SxCommand := chrAppCd || ',' || chrHTID || ',' || chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd || ',' || chrUserCd || ',' || var2Ret;
				ELSIF chrTypeHtOpe = '00' AND chrTypeHtCmd = '00' THEN
					var2Ret  := '00';
					var2SxCommand := chrAppCd || ',' || chrHTID || ',' || chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd || ',' || var2Ret || ',' ||  numHTAppupdFl;
				ELSE
					RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'予期せぬエラー');
				END IF;
			WHEN MENU_TYPE_INSTOCK THEN
				--入荷
				PH_HTINSTOCK.HTCommandMenu(chrTypeHtOpe,chrTypeHtCmd,chrUserCd,chrUserTx,chrHtID,chrAppCd,iRxCommand,var2Ret);
				var2SxCommand := chrAppCd || ',' || chrHTID || ',' || chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd || ',' || chrUserCd || ',' || var2Ret;
			WHEN MENU_TYPE_MOVE THEN
				--移動
				PH_HTMOVE.HTCommandMenu(chrTypeHtOpe,chrTypeHtCmd,chrUserCd,chrUserTx,chrHtID,chrAppCd,iRxCommand,var2Ret);
				var2SxCommand := chrAppCd || ',' || chrHTID || ',' || chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd || ',' || chrUserCd || ',' || var2Ret;
			WHEN MENU_TYPE_SHIP THEN
				--出荷
				PH_HTSHIP.HTCommandMenu(chrTypeHtOpe,chrTypeHtCmd,chrUserCd,chrUserTx,chrHtID,chrAppCd,iRxCommand,var2Ret);
				var2SxCommand := chrAppCd || ',' || chrHTID || ',' || chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd || ',' || chrUserCd || ',' || var2Ret;
			WHEN MENU_TYPE_OTHER THEN
				-- 棚卸し／その他
				PH_HTOTHER.HTCommandMenu(chrTypeHtOpe,chrTypeHtCmd,chrUserCd,chrUserTx,chrHtID,chrAppCd,iRxCommand,var2Ret);
				var2SxCommand := chrAppCd || ',' || chrHTID || ',' || chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd || ',' || chrUserCd || ',' || var2Ret;
			ELSE
				RAISE_APPLICATION_ERROR (PS_DEFINE.EX_HT01,'予期せぬエラー');
		END CASE;
		oSxCommand := var2SxCommand;
		--ＨＴ送信ログ書込
		PH_COMM.LogHtWrite(chrAppCd,chrHtID,chrUserCd,chrTypeHt,chrTypeHtOpe,chrTypeHtCmd,'W',oSxCommand);
		COMMIT;
		PLOG.DEBUG(logCtx, PROGRAM_ID || ':' || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PROGRAM_ID || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PROGRAM_ID || ':' || SQLERRM);
			END IF;
			IF INSTR(SQLERRM, 'ORA-20101: ') > 0 THEN
				varMsg := REPLACE(SQLERRM, 'ORA-20101: ', '');
				var2SxCommand := chrAppCd || ',' || chrHTID || ',' || chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd || ',' || chrUserCd || ',' || '01' || ',' || varMsg;
			ELSIF INSTR(SQLERRM, 'ORA-20102: ') > 0 THEN
				varMsg := REPLACE(SQLERRM, 'ORA-20102: ', '');
				var2SxCommand := chrAppCd || ',' || chrHTID || ',' || chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd || ',' || chrUserCd || ',' || '02' || ',' || varMsg;
			ELSIF INSTR(SQLERRM, 'ORA-20001: ') > 0 THEN
				varMsg := REPLACE(SQLERRM, 'ORA-20001: ', '');
				var2SxCommand := chrAppCd || ',' || chrHTID || ',' || chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd || ',' || chrUserCd || ',' || '01' || ',' || varMsg;
			ELSE
				varMsg := SQLERRM;
				var2SxCommand := chrAppCd || ',' || chrHTID || ',' || chrTypeHt || ',' || chrTypeHtOpe || ',' || chrTypeHtCmd || ',' || chrUserCd || ',' || '01' || ',' || varMsg;
			END IF;
			oSxCommand := var2SxCommand;
			--ＨＴ送信ログ書込
			PH_COMM.LogHtWrite(chrAppCd,chrHtID,chrUserCd,chrTypeHt,chrTypeHtOpe,chrTypeHtCmd,'W',oSxCommand);
	END HTCommand;
END PH_HT;
/
SHOW ERRORS
