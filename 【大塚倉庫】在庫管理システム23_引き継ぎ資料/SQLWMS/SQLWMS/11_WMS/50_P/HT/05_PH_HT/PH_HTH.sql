CREATE OR REPLACE PACKAGE PH_HT IS 
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PH_HT';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.HTLOG_LEVEL);
	
	/************************************************************/
	/*	ログイン・全送信										*/
	/************************************************************/
	PROCEDURE InitialCMD_0100(
		iHtUserCD		IN VARCHAR2,
		iHtUserTX		IN VARCHAR2,
		iHtID 			IN VARCHAR2
	);
	/************************************************************/
	/*	無線取り合い関数										*/
	/************************************************************/
	PROCEDURE HTCommand(
		iRxCommand IN VARCHAR2,
		oSxCommand OUT VARCHAR2
	);
	
END PH_HT;
/
SHOW ERRORS
