-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PI_SKU AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PI_SKU';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************/
	/* アップロード完了（SKU）                                  */
	/************************************************************/
	PROCEDURE EndUploadSku(
		iTrnID						IN TMP_SKU.TRN_ID%TYPE
	);
	/************************************************************/
	/* アップロード（MA_SKUPALLET）                             */
	/************************************************************/
	PROCEDURE UploadSkuPallet(
		iTrnID						IN TMP_SKU.TRN_ID%TYPE
	);
END PI_SKU;
/
SHOW ERRORS
