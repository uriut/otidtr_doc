-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PW_MOVE AS
	/************************************************************************************/
	/*																					*/
	/*									Private											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************************************/
	/*	移動指示作成																	*/
	/************************************************************************************/
	PROCEDURE RegistMove(
		iSkuCD			IN	VARCHAR2,
		iKanbanCD		IN	VARCHAR2,
		iSrcLocaCD		IN	VARCHAR2, --元ロケ
		iDestLocaCD		IN	VARCHAR2, --先ロケ
		iPalletNR		IN	NUMBER	, --移動パレット数
		iCaseNR			IN	NUMBER	, --移動ケース数
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RegistMove';

		--ロック用カーソル
		CURSOR curLock(LocaCD TA_LOCALOCK.LOCA_CD%TYPE) IS
			SELECT
				LOCA_CD
			FROM
				TA_LOCALOCK
			WHERE
				LOCA_CD = LocaCD 
			FOR UPDATE;
		rowLock		curLock%ROWTYPE;

		CURSOR curLocaS(LocaCD MA_LOCA.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_LOCA
			WHERE
				LOCA_CD = LocaCD;
		rowLocaS	curLocaS%ROWTYPE;

		CURSOR curLocaD(LocaCD MA_LOCA.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_LOCA
			WHERE
				LOCA_CD = LocaCD;
		rowLocaD	curLocaD%ROWTYPE;

		-- 引当可能在庫
		CURSOR curLocaP(LocaCD TA_LOCASTOCKP.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD = LocaCD;
		rowLocaP	curLocaP%ROWTYPE;

		-- 引当済在庫
		CURSOR curLocaC(LocaCD TA_LOCASTOCKC.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKC
			WHERE
				LOCA_CD = LocaCD;
		rowLocaC	curLocaC%ROWTYPE;
	
		rowUser				VA_USER%ROWTYPE;
		rowSLocaStockC		TA_LOCASTOCKC%ROWTYPE;
		rowDLocaStockC		TA_LOCASTOCKC%ROWTYPE;
		chrMoveNoCD			TA_MOVE.MOVENO_CD%TYPE;
		numTrnID			NUMBER(10,0);
		numPalletNR			NUMBER;
		numCaseNR			NUMBER;
		numBaraNR			NUMBER;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');

		--排他ロック
		OPEN curLock(iSrcLocaCD);
		FETCH curLock INTO rowLock;
		IF curLock%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーションロックテーブルが有りません。ロケ=[' || iDestLocaCD || ']');
		END IF;

		--ロケーションロック取得
		PS_LOCA.GetLocaLock(iDestLocaCD);

		PS_MASTER.GetUser(iUserCD, rowUser);

		PS_NO.GetTrnId(numTrnID);

		--先ロケは空きロケのみ
		OPEN curLocaP(iDestLocaCD);
		FETCH curLocaP INTO rowLocaP;
		IF curLocaP%FOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '移動先が空きロケではありません。ロケ=[' || iDestLocaCD || ']');
		END IF;
		CLOSE curLocaP;
	
		OPEN curLocaC(iDestLocaCD);
		FETCH curLocaC INTO rowLocaC;
		IF curLocaC%FOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '移動先が空きロケではありません。ロケ=[' || iDestLocaCD || ']');
		END IF;
		CLOSE curLocaC;

		--数量チェック
		IF iPcsNR <= 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '移動数を入力して下さい。');
		END IF;

		OPEN curLocaS(iSrcLocaCD);
		FETCH curLocaS INTO rowLocaS;
		IF curLocaS%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーションマスタが有りません。ロケ=[' || iSrcLocaCD || ']');
		END IF;
		CLOSE curLocaS;

		--在庫チェック
		OPEN curLocaP(iSrcLocaCD);
		FETCH curLocaP INTO rowLocaP;
		IF curLocaP%FOUND THEN

			--エリアチェック
			OPEN curLocaD(iDestLocaCD);
			FETCH curLocaD INTO rowLocaD;
			IF curLocaD%FOUND THEN

				-- 出荷荷姿コード  ケース成り出荷とパレット出荷のエリアを識別するコード　
				-- 01:ケース成り、02:パレット成り、03:ネステナ

				--ケース成りネスエリアは、完パレ以外も移動可
				IF rowLocaD.SHIPVOLUME_CD IN ( '01', '03' ) THEN

					--パレットエリア→ネスエリアへの移動
					IF rowLocaS.SHIPVOLUME_CD = '02' THEN
						IF iPalletNR = 0 OR iCaseNR > 0 OR numBaraNR > 0  THEN
							RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレット成りエリアはパレットのみ移動できます。ロケ=[' || iDestLocaCD || ']');
						END IF;
					--ケースエリア→ケースエリアへの移動
					ELSE
						IF iPalletNR = 0 AND iCaseNR = 0 THEN
							RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレット数、またはケース数を入力して下さい。');
						END IF;
					END IF;

				--パレット成りエリアは、パレットのみ
				ELSIF rowLocaD.SHIPVOLUME_CD = '02' THEN

					--ケースエリア→パレットエリアへの移動           
					IF rowLocaS.SHIPVOLUME_CD IN ( '01', '03' ) THEN 
						PS_STOCK.GetUnitPcsNr(
							iSkuCD			,
							iPcsNR			,
							rowLocaP.PALLETCAPACITY_NR,
							numPalletNR		,
							numCaseNR		,
							numBaraNR
						);
						IF numPalletNR = 0 OR numCaseNR > 0 OR numBaraNR > 0 THEN
							RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレット成りエリアはパレットのみ移動できます。ロケ=[' || iDestLocaCD || ']');
						END IF;
					ELSE
						IF iPalletNR = 0 THEN
							RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレット成りエリアはパレットのみ移動できます。ロケ=[' || iDestLocaCD || ']');
						END IF;
					END IF;
				END IF;
			ELSE
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーションマスタが有りません。ロケ=[' || iDestLocaCD || ']');
			END IF;
			CLOSE curLocaD;

			IF rowLocaP.PCS_NR > 0 THEN

				--在庫数を超えていないか
				IF iPcsNR > rowLocaP.PCS_NR THEN
					RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '移動元の引当在庫数を超えて移動できません。ロケ=[' || iSrcLocaCD || ']');
				END IF;

				PS_STOCK.GetUnitPcsNr(
					rowLocaP.SKU_CD	,
					iPcsNR			,
					rowLocaP.PALLETCAPACITY_NR,
					numPalletNR		,
					numCaseNR		,
					numBaraNR
				);

				-- LINEUNIT_NR * STACKINGNUMBER_NR < iPalletNR (ケース数があれば、iPalletNR+1)
				-- ネスは1パレのみ
				IF rowLocaD.SHIPVOLUME_CD = '03' THEN
					IF numCaseNR = 0 THEN
						IF rowLocaD.LINEUNIT_NR < numPalletNR THEN
							RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ラインユニットを超えて移動できません。ロケ=[' || iDestLocaCD || ']');
						END IF;
					ELSE
						IF rowLocaD.LINEUNIT_NR < numPalletNR + 1 THEN
							RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ラインユニットを超えて移動できません。ロケ=[' || iDestLocaCD || ']');
						END IF;
					END IF;
				ELSE
					IF numCaseNR = 0 THEN
						IF rowLocaD.LINEUNIT_NR * rowLocaP.STACKINGNUMBER_NR < numPalletNR THEN
							RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ラインユニットを超えて移動できません。ロケ=[' || iDestLocaCD || ']');
						END IF;
					ELSE
						IF rowLocaD.LINEUNIT_NR * rowLocaP.STACKINGNUMBER_NR < numPalletNR + 1 THEN
							RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ラインユニットを超えて移動できません。ロケ=[' || iDestLocaCD || ']');
						END IF;
					END IF;
				END IF;

				IF numBaraNR > 0 THEN
					RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：バラ数が発生しました。ロケ=[' || iDestLocaCD || ']');
				END IF;

				--引当済み作成
				PS_STOCK.ApplyStock(
					numTrnID					,
					rowLocaP.LOCA_CD			,
					rowLocaP.SKU_CD				,
					rowLocaP.LOT_TX				,
					rowLocaP.USELIMIT_DT		,
					' '							,	--BATCHNO_CD
					rowLocaP.STOCK_DT			,
					rowLocaP.AMOUNT_NR			,
					rowLocaP.PALLETCAPACITY_NR	,
					numPalletNR					,
					numCaseNR					,
					numBaraNR					,
					iPcsNR						,
					rowLocaP.TYPESTOCK_CD		,
					PS_DEFINE.ST_STOCK0_NOR		,
					iUserCD						,
					rowUser.USER_TX				,
					rowSLocaStockC
				);

				PS_STOCK.ApplyInStock(
					numTrnID					,
					PS_DEFINE.ST_STOCK32_MOVEA	,	-- 移動引当済
					iDestLocaCD					,	-- iLocaCD
					rowLocaP.SKU_CD				,
					rowLocaP.LOT_TX				,
					' '							,	-- iKanbanCD
					rowLocaP.CASEPERPALLET_NR	,
					rowLocaP.PALLETLAYER_NR		,
					rowLocaP.PALLETCAPACITY_NR	,
					rowLocaP.STACKINGNUMBER_NR	,
					rowLocaP.TYPEPLALLET_CD		,
					rowLocaP.USELIMIT_DT		,
					' '							,	-- BATCHNO_CD
					rowLocaP.STOCK_DT			,
					rowLocaP.AMOUNT_NR			,
					numPalletNR					,
					numCaseNR					,
					numBaraNR   				,
					iPcsNR						,
					rowLocaP.TYPESTOCK_CD		,
					iUserCD						,
					rowUser.USER_TX				,
					rowDLocaStockC
				);

				-- 指示番号
				PS_NO.GetMoveNo(chrMoveNoCD);

				--移動指示登録
				PS_MOVE.InsMove(
					numTrnID				,
					chrMoveNoCD				,
					PS_DEFINE.TYPE_MOVE_NOR	,
					rowSLocaStockC			,
					rowDLocaStockC			,
					iUserCD					,
					rowUser.USER_TX			,
					iProgramCD
				);
	
			ELSE
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '移動元の引当可能在庫がありません。ロケ=[' || iSrcLocaCD || ']');
			END IF;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '移動先が空きロケではありません。ロケ=[' || iDestLocaCD || ']');
		END IF;
		CLOSE curLocaP;

		CLOSE curLock;

		--指示書の有効化
		PS_MOVE.MoveDataOK(numTrnID);

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curLock%ISOPEN  THEN CLOSE curLock; END IF;
			IF curLocaS%ISOPEN THEN CLOSE curLocaS; END IF;
			IF curLocaD%ISOPEN THEN CLOSE curLocaD; END IF;
			IF curLocaP%ISOPEN THEN CLOSE curLocaP; END IF;
			IF curLocaC%ISOPEN THEN CLOSE curLocaC; END IF;
			RAISE;
	END RegistMove;
	/************************************************************************************/
	/*	移動指示取消																	*/
	/************************************************************************************/
	PROCEDURE CanMove(
		iMoveNoCD		IN	VARCHAR2,
		iUserCD			IN  VARCHAR2,
		iProgramCD		IN  VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CanMove';

		CURSOR curMove(MoveNoCD TA_MOVE.MOVENO_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_MOVE
			WHERE
				MOVENO_CD	= MoveNoCD	AND
				STMOVE_ID	!= PS_DEFINE.ST_MOVEM2_CANCEL;
		rowMove		curMove%ROWTYPE;
		rowSku		MA_SKU%ROWTYPE;
		rowUser		VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');

		PS_MASTER.GetUser(iUserCD, rowUser);

		OPEN curMove(iMoveNoCD);
		FETCH curMove INTO rowMove;
		IF curMove%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,' 指示書番号が存在しません。');
		END IF;
		CLOSE curMove;
		-- キャンセルチェック
		IF rowMove.STMOVE_ID = PS_DEFINE.ST_MOVEM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書番号がキャンセルされています。');
		END IF;
		-- 指示書種類チェック
		IF rowMove.TYPEMOVE_CD != PS_DEFINE.TYPE_MOVE_NOR THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '該当指示書ではありません。');
		END IF;
		-- 指示書完了チェック
		IF rowMove.END_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。');
		END IF;
		-- ＳＫＵ取得
		PS_MASTER.GetSku(rowMove.SKU_CD, rowSku);
		-- ロケーションロック取得
		PS_LOCA.GetLocaLock(rowMove.SRCLOCA_CD);
		-- 引当戻し（元ロケ）
		PS_STOCK.ReApplyStock(
			rowMove.SRCLOCASTOCKC_ID	,
			rowSku.PCSPERCASE_NR		,
			iUserCD						,
			rowUser.USER_TX
		);
		-- 引当戻し（先ロケ）
		DELETE TA_LOCASTOCKC
		WHERE
			LOCASTOCKC_ID	= rowMove.DESTLOCASTOCKC_ID;
		-- 指示書キャンセル
		UPDATE TA_MOVE
		SET
			STMOVE_ID		= PS_DEFINE.ST_MOVEM2_CANCEL	,
			UPD_DT			= SYSDATE						,
			UPDUSER_CD		= iUserCD						,
			UPDUSER_TX		= rowUser.USER_TX				,
			UPDPROGRAM_CD	= iProgramCD					,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			MOVE_ID			= rowMove.MOVE_ID;
		-- 受払削除
		DELETE TA_STOCKINOUT
		WHERE
			TYPEREF_CD = rowMove.MOVENO_CD;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curMove%ISOPEN THEN CLOSE curMove; END IF;
			RAISE;
	END CanMove;
	/************************************************************************************/
	/*	かんばん更新																	*/
	/************************************************************************************/
	PROCEDURE UpdKanban(
		iNewKanbanCD		IN	VARCHAR2,
		iOldKanbanCD		IN	VARCHAR2,
		iDestLocaCD			IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iCasePerPalletNR	IN	NUMBER	,
		iPalletLayerNR		IN	NUMBER	,
		iStackingNumberNR	IN	NUMBER	,
		iTypePalletCD		IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdKanban';
		
		CURSOR curKanban(KanbanCD MA_KANBAN.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_KANBAN
			WHERE
				KANBAN_CD = KanbanCD;
		rowKanban 		curKanban%ROWTYPE;
		CURSOR curUseKanban(KanbanCD TA_KANBANP.KANBAN_CD%TYPE,
							SkuCD TA_KANBANP.SKU_CD%TYPE,
							LotTX TA_KANBANP.LOT_TX%TYPE) IS
			SELECT
				*
			FROM
				TA_KANBANP
			WHERE
				KANBAN_CD	= KanbanCD	AND
				(
				SKU_CD <> SkuCD OR
				LOT_TX <> LotTX
				);
		rowUseKanban 		curUseKanban%ROWTYPE;
		rowLoca		 		MA_LOCA%ROWTYPE;
		numKanbanPID		NUMBER := 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		--
		PS_MASTER.GetLoca(iDestLocaCD, rowLoca);
		-- かんばんマスタチェック
		OPEN curKanban(iNewKanbanCD);
		FETCH curKanban INTO rowKanban;
		IF curKanban%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんがマスタに登録されていません。KANBAN_CD=[' || iNewKanbanCD || ']');
		END IF;
		CLOSE curKanban;
		--使用済みかんばん
		OPEN curUseKanban(iNewKanbanCD,iSkuCD,iLotTX);
		FETCH curUseKanban INTO rowUseKanban;
		IF curUseKanban%FOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんが使用されています。' ||
																'KANBAN_CD=[' || iNewKanbanCD || '], ' ||
																'SKU_CD=[' || iSkuCD || '], ' ||
																'LOT_TX=[' || iLotTX || ']'
																);
		END IF;
		CLOSE curUseKanban;
		-- 旧かんばん削除
		PS_STOCK.DelKanban(iOldKanbanCD);
		--DELETE
		--	TA_KANBANP
		--WHERE
		--	KANBAN_CD = iOldKanbanCD;
		
		-- 旧、新が違う場合、かんばん作成
		IF iOldKanbanCD <> iNewKanbanCD THEN
			-- ID取得
			SELECT
				NVL(MAX(KANBANP_ID),0)
			INTO
				numKanbanPID
			FROM
				TA_KANBANP;
			numKanbanPID := numKanbanPID + 1;
			-- 引当済かんばん登録
			INSERT INTO TA_KANBANP (
				KANBANP_ID			,
				STKANBAN_CD			,
				TYPEPKANBAN_CD		,
				KANBAN_CD			,
				WH_CD				,
				FLOOR_CD			,
				AREA_CD				,
				LINE_CD				,
				SKU_CD				,
				LOT_TX				,
				CASEPERPALLET_NR	,
				PALLETLAYER_NR		,
				STACKINGNUMBER_NR	,
				TYPEPLALLET_CD		,
				-- 管理項目
				UPD_DT				,
				UPDUSER_CD			,
				UPDUSER_TX			,
				ADD_DT				,
				ADDUSER_CD			,
				ADDUSER_TX			,
				UPDPROGRAM_CD		,
				UPDCOUNTER_NR		,
				STRECORD_ID			
			) VALUES (
				numKanbanPID				,
				'00'						,
				'00'						,
				iNewKanbanCD				,
				rowLoca.WH_CD				,
				rowLoca.FLOOR_CD			,
				rowLoca.AREA_CD				,
				rowLoca.LINE_CD				,
				iSkuCD						,
				iLotTX						,
				iCasePerPalletNR			,
				iPalletLayerNR				,
				iStackingNumberNR			,
				iTypePalletCD				,
				--
				SYSDATE						,
				iUserCD						,
				iUserTX						,
				SYSDATE						,
				iUserCD						,
				iUserTX						,
				FUNCTION_NAME				,
				0							,
				0
			);
		END IF;
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curKanban%ISOPEN THEN CLOSE curKanban; END IF;
			IF curUseKanban%ISOPEN THEN CLOSE curUseKanban; END IF;
			RAISE;
	END UpdKanban;
	/************************************************************************************/
	/*	移動元登録更新																	*/
	/************************************************************************************/
	PROCEDURE UpdSrcMove(
		iMoveNoCD			IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iKanbanCD			IN	VARCHAR2,
		iAmountNR			IN	NUMBER	,
		iPalletCapacityNR	IN	NUMBER	,
		iPalletNR			IN	NUMBER	,
		iCaseNR				IN	NUMBER	,
		iBaraNR				IN	NUMBER	,
		iPcsNR				IN	NUMBER	,
		iSrcLocaCD			IN	VARCHAR2,
		iDestLocaCD			IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdSrcMove';
		
		CURSOR curMove(
			MoveNoCD	TA_MOVE.MOVENO_CD%TYPE	,
			SkuCD		TA_MOVE.SKU_CD%TYPE		,
			SrcLocaCD	TA_MOVE.SRCLOCA_CD%TYPE	,
			DestLocaCD	TA_MOVE.DESTLOCA_CD%TYPE
		) IS
			SELECT
				ROWID				,
				TRN_ID				,
				SRCLOCA_CD			,
				SRCLOCASTOCKC_ID	,
				DESTLOCA_CD			,
				DESTTYPESTOCK_CD	,
				DESTLOCASTOCKC_ID	,
				AMOUNT_NR			,
				PALLET_NR			,
				CASE_NR				,
				BARA_NR				,
				PCS_NR				,
				CASEPERPALLET_NR	,
				PALLETLAYER_NR		,
				PALLETCAPACITY_NR	,
				STACKINGNUMBER_NR	,
				TYPEPLALLET_CD
			FROM
				TA_MOVE
			WHERE
				MOVENO_CD	= MoveNoCD		AND
				SKU_CD		= SkuCD			AND
				SRCLOCA_CD	= SrcLocaCD 	AND
				DESTLOCA_CD	= DestLocaCD
			ORDER BY
				SRCLOCA_CD;
		rowMove 	curMove%ROWTYPE;
		
		rowLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		rowDLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		numCase			TA_MOVE.CASE_NR%TYPE	:= 0;
		numBara			TA_MOVE.BARA_NR%TYPE	:= 0;
		numPcs			TA_MOVE.PCS_NR%TYPE		:= 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- ピッキング指示更新
		OPEN curMove(iMoveNoCD, iSkuCd, iSrcLocaCD, iDestLocaCD);
		LOOP
			FETCH curMove INTO rowMove;
			EXIT WHEN curMove%NOTFOUND;
			
			-- 予約引当削除（先ロケ）
			PS_LOCA.GetLockLocaStockC(rowMove.DESTLOCASTOCKC_ID, rowLocaStockC);
			-- 引当済み在庫引き落とし
			PS_STOCK.DelC(rowMove.DESTLOCASTOCKC_ID	,
							iUserCD					,
							iUserTX					,
							FUNCTION_NAME);
			-- 在庫一時引き落とし
			PS_LOCA.GetLockLocaStockC(rowMove.SRCLOCASTOCKC_ID, rowLocaStockC);
			PS_STOCK.OutStockTemp(
				rowMove.TRN_ID				,	-- iTrnID
				rowMove.DESTLOCA_CD			,
				iKanbanCD					,
				rowMove.SRCLOCASTOCKC_ID	,
				iAmountNR					,
				rowMove.PALLET_NR			,
				rowMove.CASE_NR				,
				rowMove.BARA_NR				,
				rowMove.PCS_NR				,
				PS_DEFINE.ST_STOCK12_MOVE	,	--移動中
				iUserCD						,
				iUserTX						,
				rowDLocaStockC
			);
			-- 更新
			UPDATE TA_MOVE
			SET
				DESTLOCASTOCKC_ID	= rowDLocaStockC.LOCASTOCKC_ID
			WHERE
				ROWID = rowMove.ROWID;

		END LOOP;
		CLOSE curMove;
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curMove%ISOPEN THEN CLOSE curMove; END IF;
			RAISE;
	END UpdSrcMove;
	/************************************************************************************/
	/*	移動先登録更新																	*/
	/************************************************************************************/
	PROCEDURE UpdDestMove(
		iMoveNoCD			IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iKanbanCD			IN	VARCHAR2,
		iAmountNR			IN	NUMBER	,
		iPalletCapacityNR	IN	NUMBER	,
		iPalletNR			IN	NUMBER	,
		iCaseNR				IN	NUMBER	,
		iBaraNR				IN	NUMBER	,
		iPcsNR				IN	NUMBER	,
		iSrcLocaCD			IN	VARCHAR2,
		iDestLocaCD			IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdDestMove';
		
		CURSOR curMove(
			MoveNoCD	TA_MOVE.MOVENO_CD%TYPE	,
			SkuCD		TA_MOVE.SKU_CD%TYPE		,
			SrcLocaCD	TA_MOVE.SRCLOCA_CD%TYPE	,
			DestLocaCD	TA_MOVE.DESTLOCA_CD%TYPE
		) IS
			SELECT
				ROWID				,
				TRN_ID				,
				SRCLOCA_CD			,
				SRCLOCASTOCKC_ID	,
				DESTLOCA_CD			,
				DESTTYPESTOCK_CD	,
				DESTLOCASTOCKC_ID	,
				AMOUNT_NR			,
				PALLET_NR			,
				CASE_NR				,
				BARA_NR				,
				PCS_NR				,
				CASEPERPALLET_NR	,
				PALLETLAYER_NR		,
				PALLETCAPACITY_NR	,
				STACKINGNUMBER_NR	,
				TYPEPLALLET_CD
			FROM
				TA_MOVE
			WHERE
				MOVENO_CD	= MoveNoCD		AND
				SKU_CD		= SkuCD			AND
				SRCLOCA_CD	= SrcLocaCD 	AND
				DESTLOCA_CD	= DestLocaCD
			ORDER BY
				SRCLOCA_CD;
		rowMove 	curMove%ROWTYPE;
		
		rowLocaStockC	TA_LOCASTOCKC%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- ピッキング指示更新
		OPEN curMove(iMoveNoCD, iSkuCd, iSrcLocaCD, iDestLocaCD);
		LOOP
			FETCH curMove INTO rowMove;
			EXIT WHEN curMove%NOTFOUND;

			-- 引当済取得
			PS_LOCA.GetLockLocaStockC(rowMove.DESTLOCASTOCKC_ID, rowLocaStockC); 
			-- 引当済み在庫引き落とし
--			PS_STOCK.DelC(rowMove.DESTLOCASTOCKC_ID,
--							iUserCD		  ,
--							iUserTX		  ,
--							FUNCTION_NAME);
			-- 在庫入庫
			PS_STOCK.InStockDirectInStock(
				rowMove.DESTLOCASTOCKC_ID,
				0						 , --iInStockID
				iUserCD					 ,
				iUserTX					 ,
				FUNCTION_NAME
			);

		END LOOP;
		CLOSE curMove;
		
		-- ステータス更新
		UPDATE
			TA_MOVE
		SET
			STMOVE_ID = PS_DEFINE.ST_MOVE6_MOVEINOK_E
		WHERE
			MOVENO_CD = iMoveNoCD;
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curMove%ISOPEN THEN CLOSE curMove; END IF;
			RAISE;
	END UpdDestMove;
	/************************************************************************************/
	/*																					*/
	/*									Public											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************/
	/*	移動元開始												*/
	/************************************************************/
	PROCEDURE StartSrcMove(
		iMoveNoCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'StartSrcMove';
		
		CURSOR curMove(MoveNoCD TA_MOVE.MOVENO_CD%TYPE) IS
			SELECT
				TYPEMOVE_CD						,
				MAX(STMOVE_ID) AS STMOVE_ID		,
				MAX(SRCEND_DT) AS SRCEND_DT		,
				MAX(DESTEND_DT) AS DESTEND_DT	,
				MAX(START_DT) AS START_DT		,
				MAX(END_DT) AS END_DT
			FROM
				TA_MOVE
			WHERE
				MOVENO_CD = MoveNoCD
			GROUP BY
				TYPEMOVE_CD;
		rowMove 		curMove%ROWTYPE;
		
		rowUser			VA_USER%ROWTYPE;
		rowMoveInfo		TA_MOVE%ROWTYPE;
		rowLoca			MA_LOCA%ROWTYPE;
		numAmount	 	MA_SKU.PCSPERCASE_NR%TYPE	:= 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		
		OPEN curMove(iMoveNoCD);
		FETCH curMove INTO rowMove;
		-- 指示書存在チェック
		IF curMove%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,' 指示書番号が存在しません。');
		END IF;
		-- キャンセルチェック
		IF rowMove.STMOVE_ID = PS_DEFINE.ST_MOVEM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書番号がキャンセルされています。');
		END IF;
		-- 指示書種類チェック
		IF rowMove.TYPEMOVE_CD != PS_DEFINE.TYPE_MOVE_NOR THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '該当指示書ではありません。');
		END IF;
		-- 指示書完了チェック
		IF rowMove.DESTEND_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '全移動が完了しています。');
		END IF;

		-- 指示書完了チェック
		IF rowMove.END_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。');
		END IF;
		
		--
		UPDATE
			TA_MOVE
		SET
			STMOVE_ID = PS_DEFINE.ST_MOVE2_MOVEOUT_S,
			--
			SRCSTART_DT		= SYSDATE			,
			SRCHTUSER_CD	= iUserCD			,
			SRCHTUSER_TX	= rowUser.USER_TX	,
			START_DT		= SYSDATE			,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			MOVENO_CD		= iMoveNoCD			AND
			STMOVE_ID		= PS_DEFINE.ST_MOVE0_NOTMOVE;
		CLOSE curMove;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curMove%ISOPEN THEN CLOSE curMove; END IF;
			RAISE;
	END StartSrcMove;
	/************************************************************/
	/*	移動元完了												*/
	/************************************************************/
	PROCEDURE EndSrcMove(
		iMoveNoCD		IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		iPalletNR		IN	NUMBER	,
		iCaseNR			IN	NUMBER	,
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iSrcLocaCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'EndSrcMove';
		
		CURSOR curMove(MoveNoCD TA_MOVE.MOVENO_CD%TYPE) IS
			SELECT
				MOVE_ID							,
				SKU_CD							,
				SRCKANBAN_CD					,
				DESTKANBAN_CD					,
				LOT_TX							,
				AMOUNT_NR						,
				USELIMIT_DT						,
				STOCK_DT						,
				TYPEMOVE_CD						,
				SRCLOCA_CD						,
				DESTLOCA_CD						,
				SRCTYPESTOCK_CD					,
				DESTTYPESTOCK_CD				,
				CASEPERPALLET_NR				,
				PALLETLAYER_NR					,
				PALLETCAPACITY_NR				,
				STACKINGNUMBER_NR				,
				TYPEPLALLET_CD					,
				STMOVE_ID						,
				SRCSTART_DT						,
				SRCEND_DT						,
				DESTEND_DT						,
				START_DT						,
				END_DT							,
				PALLET_NR						,
				CASE_NR							,
				BARA_NR							,
				PCS_NR							,
				SRCPALLET_NR					,
				SRCCASE_NR						,
				SRCBARA_NR						,
				SRCPCS_NR
			FROM
				TA_MOVE
			WHERE
				MOVENO_CD = MoveNoCD
			ORDER BY
				SRCLOCA_CD;
		rowMove 		curMove%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
		rowSku			MA_SKU%ROWTYPE;
		rowLoca		 	MA_LOCA%ROWTYPE;
		numAmount	 	MA_SKU.PCSPERCASE_NR%TYPE	:= 0;
		
		numPalletNR		TA_MOVE.PALLET_NR%TYPE;
		numCaseNR		TA_MOVE.CASE_NR%TYPE;
		numBaraNR		TA_MOVE.BARA_NR%TYPE;
		numPcsNR		TA_SUPP.PCS_NR%TYPE;
		numImpPalletNR	TA_MOVE.PALLET_NR%TYPE;
		numImpCaseNR	TA_MOVE.CASE_NR%TYPE;
		numImpBaraNR	TA_MOVE.BARA_NR%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		PS_MASTER.GetSku(iSkuCD, rowSku);
		PS_MASTER.GetLoca(iSrcLocaCD, rowLoca);
		
		-- 入力されたピース数を別の変数としてセット
		numPcsNR := iPcsNR;
	
		OPEN curMove(iMoveNoCD);
		Loop
			FETCH curMove INTO rowMove;
			EXIT WHEN curMove%NOTFOUND;
			EXIT WHEN numPcsNR = 0;
			-- 指示書存在チェック
			IF curMove%NOTFOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,' 指示書番号が存在しません。');
			END IF;
			-- キャンセルチェック
			IF rowMove.STMOVE_ID = PS_DEFINE.ST_MOVEM2_CANCEL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書番号がキャンセルされています。');
			END IF;
			-- 指示書種類チェック
			IF rowMove.TYPEMOVE_CD != PS_DEFINE.TYPE_MOVE_NOR THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '該当指示書ではありません。');
			END IF;
			-- 指示書完了チェック
			IF rowMove.DESTEND_DT IS NOT NULL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '全移動が完了しています。');
			END IF;

			-- 指示書完了チェック
			IF rowMove.END_DT IS NOT NULL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。');
			END IF;
			-- ロケチェック
			IF iSrcLocaCD != rowMove.SRCLOCA_CD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '元ロケーションが指示と異なります。');
			END IF;

			numAmount := rowSku.PCSPERCASE_NR;
			-- 移動数量取得
			PS_STOCK.GetUnitPcsNr(
				iSkuCD			,
				rowMove.PCS_NR	,
				rowMove.PALLETCAPACITY_NR	,
				numPalletNR		,
				numCaseNR		,
				numBaraNR		
				);
			-- 入力数量取得
			PS_STOCK.GetUnitPcsNr(
				iSkuCD			,
				numPcsNR		,
				rowMove.PALLETCAPACITY_NR	,
				numImpPalletNR	,
				numImpCaseNR	,
				numImpBaraNR	
				);
	
			IF numPcsNR = (rowMove.PCS_NR - rowMove.SRCPCS_NR) THEN
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN A');
				-- 残りのピース数(バラ単位)が入力されたピース数と等しい場合(=移動元完了)
				UPDATE
					TA_MOVE
				SET
					SRCPALLET_NR	= SRCPALLET_NR + numImpPalletNR	,
					SRCCASE_NR		= SRCCASE_NR + numImpCaseNR		,
					SRCBARA_NR		= SRCBARA_NR + numImpBaraNR		,
					SRCPCS_NR		= SRCPCS_NR + numPcsNR			,
					SRCEND_DT		= SYSDATE						,
					SRCHTUSER_CD	= iUserCD						,
					SRCHTUSER_TX	= rowUser.USER_TX				
				WHERE
					MOVE_ID			= rowMove.MOVE_ID;
				
				UPDATE
					TA_MOVE
				SET
					--
					UPD_DT			= SYSDATE					,
					UPDUSER_CD		= iUserCD					,
					UPDUSER_TX		= rowUser.USER_TX			,
					UPDPROGRAM_CD	= iProgramCD				,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
					--
				WHERE
					MOVENO_CD		= iMoveNoCD;
				numPcsNR := 0;
			ELSIF numPcsNR < (rowMove.PCS_NR - rowMove.SRCPCS_NR) THEN
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN B');
				-- 残りのピース数(バラ単位)が入力されたピース数より多い場合(移動指示に対して入力数が少ない場合)
				UPDATE
					TA_MOVE
				SET
					SRCPALLET_NR	= SRCPALLET_NR + numImpPalletNR	,
					SRCCASE_NR		= SRCCASE_NR + numImpCaseNR		,
					SRCBARA_NR		= SRCBARA_NR + numImpBaraNR		,
					SRCPCS_NR		= SRCPCS_NR + numPcsNR			,
					--
					UPD_DT			= SYSDATE						,
					UPDUSER_CD		= iUserCD						,
					UPDUSER_TX		= rowUser.USER_TX				,
					UPDPROGRAM_CD	= iProgramCD					,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1				
				WHERE
					MOVE_ID		= rowMove.MOVE_ID;
					
				numPcsNR := 0;
			ELSE
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN C');

				-- 残りのピース数(バラ単位)が入力されたピース数より多い場合(移動指示に対して入力が多い場合 = レコードを完了して次のレコードを検索)
				UPDATE
					TA_MOVE
				SET
					SRCPALLET_NR	= rowMove.PALLET_NR				,
					SRCCASE_NR		= rowMove.CASE_NR				,
					SRCBARA_NR		= rowMove.BARA_NR				,
					SRCPCS_NR		= rowMove.PCS_NR				,
					--
					UPD_DT			= SYSDATE						,
					UPDUSER_CD		= iUserCD						,
					UPDUSER_TX		= rowUser.USER_TX				,
					UPDPROGRAM_CD	= iProgramCD					,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
				WHERE
					MOVE_ID		= rowMove.MOVE_ID;
					
				numPcsNR := numPcsNR - (rowMove.PCS_NR - rowMove.SRCPCS_NR);
			END IF;
		END LOOP;
		CLOSE curMove;
		IF numPcsNR <> 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '移動完了のピース数が不正です。');
		END IF;
		-- 受払作成
		-- 出庫
		PS_STOCK.InsStockInOut(	
			' '							,
			' '							,
			rowMove.SRCLOCA_CD			,
			rowMove.SKU_CD				,
			rowMove.LOT_TX				,
			rowMove.SRCKANBAN_CD		,
			rowMove.CASEPERPALLET_NR	,
			rowMove.PALLETLAYER_NR		,
			rowMove.PALLETCAPACITY_NR	,
			rowMove.STACKINGNUMBER_NR	,
			rowMove.TYPEPLALLET_CD		,	-- iTypePalletCD
			rowMove.USELIMIT_DT			,
			' '							,	-- BATCHNO_CD
			rowMove.STOCK_DT			,
			rowMove.AMOUNT_NR			,
			iPalletNR* (-1) 			,
			iCaseNR	* (-1) 				,
			iBaraNR	* (-1) 				,
			iPcsNR * (-1) 				,
			rowMove.SRCTYPESTOCK_CD		,
			rowMove.SRCTYPESTOCK_CD		,
			'300'						,	-- 移動出庫
			iMoveNoCD					,
			0			  				,
			'MOVE'						,
			0							,	-- iNeedSendFL
			iUserCD						,
			rowUser.USER_TX				,
			iProgramCD
		);
		-- ステータス更新
		UPDATE
			TA_MOVE
		SET
			STMOVE_ID = PS_DEFINE.ST_MOVE3_MOVEOUTOK_E
		WHERE
			MOVENO_CD = iMoveNoCD;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curMove%ISOPEN THEN CLOSE curMove; END IF;
			RAISE;
	END EndSrcMove;
	/************************************************************/
	/*	移動先開始												*/
	/************************************************************/
	PROCEDURE StartDestMove(
		iMoveNoCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'StartDestMove';
		
		CURSOR curMove(MoveNoCD TA_MOVE.MOVENO_CD%TYPE) IS
			SELECT
				TYPEMOVE_CD						,
				MAX(STMOVE_ID) AS STMOVE_ID		,
				MAX(SRCEND_DT) AS SRCEND_DT		,
				MAX(DESTEND_DT) AS DESTEND_DT	,
				MAX(START_DT) AS START_DT		,
				MAX(END_DT) AS END_DT
			FROM
				TA_MOVE
			WHERE
				MOVENO_CD = MoveNoCD
			GROUP BY
				TYPEMOVE_CD;
		rowMove 		curMove%ROWTYPE;
		
		rowUser			VA_USER%ROWTYPE;
		rowMoveInfo		TA_MOVE%ROWTYPE;
		rowLoca			MA_LOCA%ROWTYPE;
		numAmount	 	MA_SKU.PCSPERCASE_NR%TYPE	:= 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		
		OPEN curMove(iMoveNoCD);
		FETCH curMove INTO rowMove;
		-- 指示書存在チェック
		IF curMove%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,' 指示書番号が存在しません。');
		END IF;
		-- キャンセルチェック
		IF rowMove.STMOVE_ID = PS_DEFINE.ST_MOVEM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書番号がキャンセルされています。');
		END IF;
		-- 指示書種類チェック
		IF rowMove.TYPEMOVE_CD != PS_DEFINE.TYPE_MOVE_NOR THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '該当指示書ではありません。');
		END IF;
		-- 指示書完了チェック
		IF rowMove.DESTEND_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '全移動が完了しています。');
		END IF;

		-- 指示書完了チェック
		IF rowMove.END_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。');
		END IF;
		
		--
		UPDATE
			TA_MOVE
		SET
			STMOVE_ID = PS_DEFINE.ST_MOVE5_MOVEIN_S,
			--
			DESTSTART_DT	= SYSDATE			,
			DESTHTUSER_CD	= iUserCD			,
			DESTHTUSER_TX	= rowUser.USER_TX	,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			MOVENO_CD		= iMoveNoCD			;
		CLOSE curMove;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curMove%ISOPEN THEN CLOSE curMove; END IF;
			RAISE;
	END StartDestMove;
	/************************************************************/
	/*	移動先完了												*/
	/************************************************************/
	PROCEDURE EndDestMove(
		iMoveNoCD		IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		iKanbanCD		IN	VARCHAR2,
		iPalletNR		IN	NUMBER	,
		iCaseNR			IN	NUMBER	,
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iDestLocaCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'EndDestMove';
		
		CURSOR curMove(MoveNoCD TA_MOVE.MOVENO_CD%TYPE) IS
			SELECT
				MOVE_ID							,
				SKU_CD							,
				SRCKANBAN_CD					,
				DESTKANBAN_CD					,
				LOT_TX							,
				AMOUNT_NR						,
				USELIMIT_DT						,
				STOCK_DT						,
				TYPEMOVE_CD						,
				SRCLOCA_CD						,
				DESTLOCA_CD						,
				SRCTYPESTOCK_CD					,
				DESTTYPESTOCK_CD				,
				CASEPERPALLET_NR				,
				PALLETLAYER_NR					,
				PALLETCAPACITY_NR				,
				STACKINGNUMBER_NR				,
				TYPEPLALLET_CD					,
				STMOVE_ID						,
				SRCSTART_DT						,
				SRCEND_DT						,
				DESTEND_DT						,
				START_DT						,
				END_DT							,
				PALLET_NR						,
				CASE_NR							,
				BARA_NR							,
				PCS_NR							,
				DESTPALLET_NR					,
				DESTCASE_NR						,
				DESTBARA_NR						,
				DESTPCS_NR
			FROM
				TA_MOVE
			WHERE
				MOVENO_CD = MoveNoCD
			ORDER BY
				END_DT		,
				STMOVE_ID;
		rowMove 		curMove%ROWTYPE;
		
		CURSOR curKanban(KanbanCD MA_KANBAN.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_KANBAN
			WHERE
				KANBAN_CD = KanbanCD;
		rowKanban 		curKanban%ROWTYPE;
		CURSOR curUseKanban(KanbanCD TA_KANBANP.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_KANBANP
			WHERE
				KANBAN_CD = KanbanCD;
		rowUseKanban 		curUseKanban%ROWTYPE;

		rowUser			VA_USER%ROWTYPE;
		rowSku			MA_SKU%ROWTYPE;
		rowLoca		 	MA_LOCA%ROWTYPE;
		chrDestLocaCD	TA_MOVE.DESTLOCA_CD%TYPE;
		numAmount	 	MA_SKU.PCSPERCASE_NR%TYPE	:= 0;
		
		numPalletNR		TA_MOVE.PALLET_NR%TYPE;
		numCaseNR		TA_MOVE.CASE_NR%TYPE;
		numBaraNR		TA_MOVE.BARA_NR%TYPE;
		numPcsNR		TA_SUPP.PCS_NR%TYPE;
		numImpPalletNR	TA_MOVE.PALLET_NR%TYPE;
		numImpCaseNR	TA_MOVE.CASE_NR%TYPE;
		numImpBaraNR	TA_MOVE.BARA_NR%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		PS_MASTER.GetSku(iSkuCD, rowSku);
		PS_MASTER.GetLoca(iDestLocaCD, rowLoca);

		-- 入力されたピース数を別の変数としてセット
		numPcsNR := iPcsNR;
		
		OPEN curMove(iMoveNoCD);
		Loop
			FETCH curMove INTO rowMove;
			EXIT WHEN curMove%NOTFOUND;
			EXIT WHEN numPcsNR = 0;
			-- 指示書存在チェック
			IF curMove%NOTFOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,' 指示書番号が存在しません。');
			END IF;
			-- キャンセルチェック
			IF rowMove.STMOVE_ID = PS_DEFINE.ST_MOVEM2_CANCEL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書番号がキャンセルされています。');
			END IF;
			-- 指示書種類チェック
			IF rowMove.TYPEMOVE_CD != PS_DEFINE.TYPE_MOVE_NOR THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '該当指示書ではありません。');
			END IF;
			-- 指示書完了チェック
			IF rowMove.DESTEND_DT IS NOT NULL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '全移動が完了しています。');
			END IF;
			-- 指示書完了チェック
			IF rowMove.END_DT IS NOT NULL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。');
			END IF;
			-- ロケチェック
			IF iDestLocaCD != rowMove.DESTLOCA_CD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '先ロケーションが指示と異なります。');
			END IF;
			-- かんばんチェック
			IF RTRIM(iKanbanCD) = '' THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんが入力されていません。');
			END IF;
			IF rowMove.DESTKANBAN_CD <> iKanbanCD AND rowMove.DESTKANBAN_CD IS NOT NULL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '前回入力されたかんばんと異なります。');
			END IF;
			-- かんばんマスタチェック
			OPEN curKanban(iKanbanCD);
			FETCH curKanban INTO rowKanban;
			IF curKanban%NOTFOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんがマスタに登録されていません。KANBAN_CD=[' || iKanbanCD || ']');
			END IF;
			CLOSE curKanban;

			numAmount := rowSku.PCSPERCASE_NR;
			-- 移動数量取得
			PS_STOCK.GetUnitPcsNr(
				iSkuCD			,
				rowMove.PCS_NR	,
				rowMove.PALLETCAPACITY_NR	,
				numPalletNR		,
				numCaseNR		,
				numBaraNR		
				);
			-- 入力数量取得
			PS_STOCK.GetUnitPcsNr(
				iSkuCD			,
				numPcsNR		,
				rowMove.PALLETCAPACITY_NR	,
				numImpPalletNR	,
				numImpCaseNR	,
				numImpBaraNR	
				);
			-- 処理
			-- 残りのピース数(バラ単位)が入力されたピース数より多い場合(移動指示に対して入力が多い場合 = レコードを完了して次のレコードを検索)
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'STARTING PATTERN numPcsNR='||numPcsNR);
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'STARTING PATTERN rowMove.PCS_NR - rowMove.DESTPCS_NR='||(rowMove.PCS_NR - rowMove.DESTPCS_NR));

			IF numPcsNR > (rowMove.PCS_NR - rowMove.DESTPCS_NR) THEN
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN A numPcsNR='||numPcsNR);
				-- 更新のみ
				UPDATE
					TA_MOVE
				SET
					DESTKANBAN_CD	= iKanbanCD						,
					DESTPALLET_NR	= rowMove.PALLET_NR				,
					DESTCASE_NR		= rowMove.CASE_NR				,
					DESTBARA_NR		= rowMove.BARA_NR				,
					DESTPCS_NR		= rowMove.PCS_NR				,
					--
					UPD_DT			= SYSDATE					,
					UPDUSER_CD		= iUserCD					,
					UPDUSER_TX		= rowUser.USER_TX			,
					UPDPROGRAM_CD	= iProgramCD				,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
					--
				WHERE
					MOVE_ID		= rowMove.MOVE_ID;
				numPcsNR := numPcsNR - (rowMove.PCS_NR - rowMove.DESTPCS_NR);
				
			-- 残りのピース数(バラ単位)が入力されたピース数と等しい場合(=移動元完了)
			ELSIF  numPcsNR = (rowMove.PCS_NR - rowMove.DESTPCS_NR) THEN
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN B numPcsNR='||numPcsNR);

				--入庫チェック
				PS_STOCK.ChkInStock(
					iDestLocaCD 	,
					iSkuCD 			,
					PS_DEFINE.USELIMITDT_DEFAULT,	-- todo iUseLimitDT
					' '				,
					TO_DATE(TO_CHAR(SYSDATE,'YYYYMM') || '01','YYYYMMDD'),
					rowMove.AMOUNT_NR,
					rowMove.CASE_NR,
					rowMove.BARA_NR,
					rowMove.PCS_NR	,
					rowMove.DESTTYPESTOCK_CD,	-- todo iTypeStockCD
					iUserCD			,
					rowUser.USER_TX
				);

				--在庫増加
				--移動情報更新
				PW_MOVE.UpdSrcMove(
					iMoveNoCD					,
					iSkuCD						,
					rowMove.LOT_TX				,
					rowMove.SRCKANBAN_CD		,
					rowSku.PCSPERCASE_NR		,
					rowMove.PALLETCAPACITY_NR	,
					rowMove.PALLET_NR			,
					rowMove.CASE_NR				,
					rowMove.BARA_NR				,
					rowMove.PCS_NR				,
					rowMove.SRCLOCA_CD			,
					rowMove.DESTLOCA_CD			,
					iUserCD						,
					rowUser.USER_TX				,
					iProgramCD
				);

				--移動情報更新
				--完了になったときのみ引当済数を更新する
				PW_MOVE.UpdDestMove(
					iMoveNoCD					,
					iSkuCD						,
					rowMove.LOT_TX				,
					iKanbanCD					,
					rowSku.PCSPERCASE_NR		,
					rowMove.PALLETCAPACITY_NR	,
					numPalletNR					,
					numCaseNR					,
					numBaraNR					,
					rowMove.PCS_NR				,
					rowMove.SRCLOCA_CD			,
					iDestLocaCD					,
					iUserCD						,
					rowUser.USER_TX				,
					iProgramCD
				);

				-- 更新のみ
				UPDATE
					TA_MOVE
				SET
					STMOVE_ID		= PS_DEFINE.ST_MOVE6_MOVEINOK_E,	--移動完了
					DESTKANBAN_CD	= iKanbanCD						,
					DESTPALLET_NR	= DESTPALLET_NR + numImpPalletNR	,
					DESTCASE_NR		= DESTCASE_NR + numImpCaseNR		,
					DESTBARA_NR		= DESTBARA_NR + numImpBaraNR		,
					DESTPCS_NR		= DESTPCS_NR + numPcsNR		,
					--
					END_DT			= SYSDATE					,
					DESTEND_DT		= SYSDATE					,
					DESTHTUSER_CD	= iUserCD					,
					DESTHTUSER_TX	= rowUser.USER_TX			,
					--
					UPD_DT			= SYSDATE					,
					UPDUSER_CD		= iUserCD					,
					UPDUSER_TX		= rowUser.USER_TX			,
					UPDPROGRAM_CD	= iProgramCD				,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
					--
				WHERE
					MOVE_ID		= rowMove.MOVE_ID;
				numPcsNR :=0;

				-- かんばん更新
				PW_MOVE.UpdKanban(
					iKanbanCD					,	-- new
					rowMove.SRCKANBAN_CD		,	-- old
					rowMove.DESTLOCA_CD			,
					rowMove.SKU_CD				,
					rowMove.LOT_TX				,
					rowMove.CASEPERPALLET_NR	,
					rowMove.PALLETLAYER_NR		,
					rowMove.STACKINGNUMBER_NR	,
					rowMove.TYPEPLALLET_CD		,
					iUserCD						,
					rowUser.USER_TX
				);

			ELSE 
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN C numPcsNR='||numPcsNR);
			-- 残りのピース数(バラ単位)が入力されたピース数より多い場合(移動指示に対して入力数が少ない場合)
				UPDATE
					TA_MOVE
				SET
					STMOVE_ID		= PS_DEFINE.ST_MOVE0_NOTMOVE		,	--一部移動済みの場合は未移動に戻す
					DESTPALLET_NR	= DESTPALLET_NR + numImpPalletNR	,
					DESTCASE_NR		= DESTCASE_NR + numImpCaseNR		,
					DESTBARA_NR		= DESTBARA_NR + numImpBaraNR		,
					DESTPCS_NR		= DESTPCS_NR + numPcsNR				,
					--
					UPD_DT			= SYSDATE						,
					UPDUSER_CD		= iUserCD						,
					UPDUSER_TX		= rowUser.USER_TX				,
					UPDPROGRAM_CD	= iProgramCD					,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1				
				WHERE
					MOVE_ID		= rowMove.MOVE_ID;
				numPcsNR := 0;
			END IF;
		END LOOP;
		
		IF numPcsNR <> 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '移動完了のピース数が不正です。残ピース数:'||numPcsNR);
		END IF;
		-- 受払作成
		-- 入庫
		PS_STOCK.InsStockInOut(	
			' '							,
			' '							,
			rowMove.DESTLOCA_CD			,
			rowMove.SKU_CD				,
			rowMove.LOT_TX				,
			iKanbanCD					,
			rowMove.CASEPERPALLET_NR	,
			rowMove.PALLETLAYER_NR		,
			rowMove.PALLETCAPACITY_NR	,
			rowMove.STACKINGNUMBER_NR	,
			rowMove.TYPEPLALLET_CD		,	-- iTypePalletCD
			rowMove.USELIMIT_DT			,
			' '							,	-- BATCHNO_CD
			rowMove.STOCK_DT			,
			rowMove.AMOUNT_NR			,
			iPalletNR					,
			iCaseNR						,
			iBaraNR						,
			iPcsNR						,
			rowMove.DESTTYPESTOCK_CD	,
			rowMove.DESTTYPESTOCK_CD	,
			'350'						,	-- 移動入庫
			iMoveNoCD					,
			0			  				,
			'MOVE'						,
			0							,	-- iNeedSendFL
			iUserCD						,
			rowUser.USER_TX				,
			iProgramCD
		);
		CLOSE curMove;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curMove%ISOPEN THEN CLOSE curMove; END IF;
			IF curKanban%ISOPEN THEN CLOSE curKanban; END IF;
			RAISE;
	END EndDestMove;
	/************************************************************************************/
	/*	移動紐付解除																	*/
	/************************************************************************************/
	PROCEDURE RstiPad(
		iMoveNoCD		IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstiPad';
		-- 移動指示
		CURSOR curMove(MoveNoCD	TA_MOVE.MOVENO_CD%TYPE) IS
			SELECT
				MOVENO_CD			,
				STMOVE_ID
			FROM
				TA_MOVE
			WHERE
				MOVENO_CD = MoveNoCD
			;
		rowMove			curMove%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		OPEN curMove(iMoveNoCD);
		FETCH curMove INTO rowMove;
		IF curMove%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iMoveNoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowMove.STMOVE_ID = PS_DEFINE.ST_MOVEM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iMoveNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowMove.STMOVE_ID >= PS_DEFINE.ST_MOVE6_MOVEINOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了済みです。' ||
																'指示書番号 = [' || iMoveNoCD || ']');
		END IF;
		CLOSE curMove;
		UPDATE TA_MOVE
		SET
			STMOVE_ID		= PS_DEFINE.ST_MOVE0_NOTMOVE,
			START_DT		= NULL				,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			MOVENO_CD		= iMoveNoCD;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curMove%ISOPEN THEN CLOSE curMove; END IF;
			RAISE;
	END RstiPad;
END PW_MOVE;
/
SHOW ERRORS
