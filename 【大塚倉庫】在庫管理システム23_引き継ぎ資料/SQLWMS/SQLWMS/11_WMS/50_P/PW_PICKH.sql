-- Copyright (C) Seaos Corporation 2014 All rights reserved
CREATE OR REPLACE PACKAGE PW_PICK AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PW_PICK';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);

	/************************************************************/
	/*	進捗率確認(パレット成りピック用)						*/
	/************************************************************/
	PROCEDURE ChkProgressRatio(
		iTransporterCD	IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2,
		oGroupCompNO	OUT NUMBER,
		oGroupTotalNO	OUT NUMBER,
		oAllCompNO		OUT NUMBER,
		oAllTotalNO		OUT NUMBER
	);
	/************************************************************/
	/*	２ＰＬピック確認(パレット成りピック用)					*/
	/************************************************************/
	PROCEDURE ChkPalletPick(
		iPalletdetnoCD	IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2,
		oPalletNum		OUT	NUMBER, --2:2PLピック可, 1:1PLピックのみ, 0:ピック済み
		oPalletdetnoCD	OUT	VARCHAR2
	);
	/************************************************************/
	/*	補充元ピック完了(パレット成りピック用)					*/
	/************************************************************/
	PROCEDURE SetPrintOK(
		iPalletdetnoCD	IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	補充元ピック完了(パレット成りピック用)２パレ用			*/
	/************************************************************/
	PROCEDURE SetPrintOK_2PL(
		iPalletdetnoCD	IN	VARCHAR2,
		iPalletdetnoCD2	IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	補充元ピック戻し(パレット成りピック用)エラー時			*/
	/************************************************************/
--	PROCEDURE RstPrintOK(
--		iPalletdetnoCD	IN	VARCHAR2,
--		iUserCD			IN	VARCHAR2,
--		iProgramCD		IN	VARCHAR2
--	);
	/************************************************************/
	/*	補充元ピック開始(パレット成りピック用-ロケ単位)			*/
	/************************************************************/
	PROCEDURE StartPalletPick(
		iPalletdetnoCD	IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	補充保留												*/
	/************************************************************/
	PROCEDURE DeferPick(
		iPalletdetnoCD	IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	補充保留 ２パレ用										*/
	/************************************************************/
	PROCEDURE DeferPick_2PL(
		iPalletdetnoCD	IN	VARCHAR2,
		iPalletdetnoCD2	IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	補充保留解除											*/
	/************************************************************/
	PROCEDURE CancelDeferPick(
		iPalletdetnoCD	IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	指示書再印刷											*/
	/************************************************************/
	PROCEDURE RePrint(
		iTrnID			IN NUMBER	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
	/************************************************************/
	/*	iPad紐づき解除											*/
	/************************************************************/
	PROCEDURE RstiPad(
		iPalletDetNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
	/************************************************************/
	/*	ハンディ紐づき解除										*/
	/************************************************************/
	PROCEDURE RstHt(
		iPalletDetNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
END PW_PICK;
/
SHOW ERRORS
