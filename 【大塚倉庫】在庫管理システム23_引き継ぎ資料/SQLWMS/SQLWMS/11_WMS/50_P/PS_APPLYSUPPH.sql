-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_APPLYSUPP IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME	CONSTANT VARCHAR2(40)	:= 'PS_APPLYSUPP';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************************************/
	/*	緊急補充引当																	*/
	/************************************************************************************/
	PROCEDURE ApplyESupp(
		iTrnID			IN	NUMBER	,
		iUserCD			IN	VARCHAR2,
		iUserTX			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************************************/
	/*	補充先ロケ検索（空きロケ）														*/
	/************************************************************************************/
	PROCEDURE ApplyStockEmpty(
		iTrnID				IN NUMBER			,
		irowSku				IN MA_SKU%ROWTYPE	,
		iLotTX				IN VARCHAR2			,
		iPalletCapacityNR	IN NUMBER			,
		iStackingNumberNR	IN NUMBER			,
		iPalletNR			IN NUMBER			,
		iTypeLocaCD			IN VARCHAR2			,
		iTypeLocaBlockCD	IN VARCHAR2			,
		iShipVolumeCD		IN VARCHAR2			,
		oEndFL				OUT NUMBER			,
		oLocaCD				OUT VARCHAR2
	);
END PS_APPLYSUPP;
/
SHOW ERRORS
