-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PW_ARRIV IS
	
	/************************************************************************************/
	/*	入荷受付																		*/
	/************************************************************************************/
	PROCEDURE EndArriv(
		iArrivOrderNoCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'EndArriv';
		-- 入荷予定
		CURSOR curTmp(ArrivOrderNoCD	TA_ARRIV.ARRIVORDERNO_CD%TYPE) IS
			SELECT
				TAR.ARRIVORDERNO_CD	,
				MAX(TAR.STARRIV_ID) AS STARRIV_ID
			FROM
				TA_ARRIV TAR
			WHERE
				TAR.ARRIVORDERNO_CD = ArrivOrderNoCD
			GROUP BY
				TAR.ARRIVORDERNO_CD;
		rowTmp 	curTmp%ROWTYPE;
		
		rowUser	VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		-- 入荷予定取得
		OPEN curTmp(iArrivOrderNoCD);
		FETCH curTmp INTO rowTmp;
		-- ステータスチェック
		IF rowTmp.STARRIV_ID = PS_DEFINE.ST_ARRIVM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'キャンセルされています。入荷予定番号 = [' || iArrivOrderNoCD || ']');
		END IF;
		IF rowTmp.STARRIV_ID < PS_DEFINE.ST_ARRIV2_NOTARRIV THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '車両情報が更新されてされていません。入荷予定番号 = [' || iArrivOrderNoCD || ']');
		END IF;
		IF rowTmp.STARRIV_ID = PS_DEFINE.ST_ARRIV3_ARRIV_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入荷受付が完了しています。入荷予定番号 = [' || iArrivOrderNoCD || ']');
		END IF;
		IF rowTmp.STARRIV_ID >= PS_DEFINE.ST_ARRIV4_INSTOCKCHK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '検品が完了しています。入荷予定番号 = [' || iArrivOrderNoCD || ']');
		END IF;
		CLOSE curTmp;
		-- 入荷予定更新
		UPDATE TA_ARRIV
		SET
			STARRIV_ID		= PS_DEFINE.ST_ARRIV3_ARRIV_E,
			ARRIVEND_DT		= SYSDATE			,
			ARRIVENDUSER_CD	= iUserCD			,
			ARRIVENDUSER_TX	= rowUser.USER_TX	,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			ARRIVORDERNO_CD	= iArrivOrderNoCD;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTmp%ISOPEN THEN CLOSE curTmp; END IF;
			RAISE;
	END EndArriv;
	/************************************************************************************/
	/*	入荷受付キャンセル																*/
	/************************************************************************************/
	PROCEDURE CanArriv(
		iArrivOrderNoCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CanArriv';
		-- 入荷予定
		CURSOR curTmp(ArrivOrderNoCD	TA_ARRIV.ARRIVORDERNO_CD%TYPE) IS
			SELECT
				TAR.ARRIVORDERNO_CD	,
				MAX(TAR.STARRIV_ID) AS STARRIV_ID
			FROM
				TA_ARRIV TAR
			WHERE
				TAR.ARRIVORDERNO_CD = ArrivOrderNoCD
			GROUP BY
				TAR.ARRIVORDERNO_CD;
		rowTmp	curTmp%ROWTYPE;
		
		rowUser	VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		-- 入荷予定取得
		OPEN curTmp(iArrivOrderNoCD);
		FETCH curTmp INTO rowTmp;
		-- ステータスチェック
		IF rowTmp.STARRIV_ID = PS_DEFINE.ST_ARRIV2_NOTARRIV THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入荷受付されていません。入荷予定番号 = [' || iArrivOrderNoCD || ']');
		END IF;
		IF rowTmp.STARRIV_ID >= PS_DEFINE.ST_ARRIV4_INSTOCKCHK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '既に検品完了しています。入荷予定番号 = [' || iArrivOrderNoCD || ']');
		END IF;
		CLOSE curTmp;
		-- 入荷予定更新
		UPDATE TA_ARRIV
		SET
			STARRIV_ID		= PS_DEFINE.ST_ARRIV2_NOTARRIV,
			ARRIVEND_DT		= NULL				,
			ARRIVENDUSER_CD	= ' '				,
			ARRIVENDUSER_TX	= ' '				,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			ARRIVORDERNO_CD	= iArrivOrderNoCD;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTmp%ISOPEN THEN CLOSE curTmp; END IF;
			RAISE;
	END CanArriv;
	/************************************************************************************/
	/*	入荷検品中キャンセル																*/
	/************************************************************************************/
	PROCEDURE CancelCheck(
		iArrivID			IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CancelCheck';
		-- 入荷予定
		CURSOR curTmp(ArrivID	TA_ARRIV.ARRIV_ID%TYPE) IS
			SELECT
				TAR.ARRIV_ID	,
				TAR.STARRIV_ID	
			FROM
				TA_ARRIV TAR
			WHERE
				TAR.ARRIV_ID = iArrivID
			;
		CURSOR curInStock(ArrivID	TA_ARRIV.ARRIV_ID%TYPE) IS
			SELECT
				TAS.INSTOCKNO_CD,
				TAS.ARRIV_ID	
			FROM
				TA_INSTOCK TAS
					LEFT OUTER JOIN TA_ARRIV TAR
						ON TAS.ARRIV_ID = TAR.ARRIV_ID
			WHERE
				TAR.ARRIV_ID = iArrivID
			;
		rowTmp		curTmp%ROWTYPE;
		rowInStock	curInStock%ROWTYPE;
		rowUser		VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		
		
		-- 入荷予定取得
		OPEN curTmp(iArrivID);
		FETCH curTmp INTO rowTmp;
		-- ステータスチェック
		IF rowTmp.STARRIV_ID <= PS_DEFINE.ST_ARRIV3_ARRIV_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入荷受付されていません。入荷予定ID = [' || iArrivID || ']');
		END IF;
		IF rowTmp.STARRIV_ID >= PS_DEFINE.ST_ARRIV4_INSTOCKCHK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '既に検品完了しています。入荷予定ID = [' || iArrivID || ']');
		END IF;
		CLOSE curTmp;
		-- 入荷予定更新
		UPDATE TA_ARRIV
		SET
			STARRIV_ID		= PS_DEFINE.ST_ARRIV3_ARRIV_E,
			CHECK_DT		= NULL				,
			CHECK_TX		= NULL				,
			CHECKUSER_CD	= NULL				,
			CHECKUSER_TX	= NULL				,
			CHECKHT_ID		= 0					,
			CHECKHTPCS_NR	= 0					,
			KANBAN_CD		= NULL				,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			ARRIV_ID	= iArrivID;

		-- 格納指示が作成されている場合はキャンセルをする
		OPEN curInStock(iArrivID);
		LOOP
			FETCH curInStock INTO rowInStock;
			EXIT WHEN curInStock%NOTFOUND;
			
			--格納指示のキャンセルを実行
			PW_INSTOCK.CanInStock(rowInStock.INSTOCKNO_CD, iUserCD, iProgramCD);
		END LOOP;

		
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTmp%ISOPEN THEN CLOSE curTmp; END IF;
			RAISE;
	END CancelCheck;
	/************************************************************************************/
	/*	入荷受付キャンセル																*/
	/************************************************************************************/
	PROCEDURE CanArrivPlan(
		iArrivOrderNoCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CanArrivPlan';
		-- 入荷予定
		CURSOR curTmp(ArrivOrderNoCD	TA_ARRIV.ARRIVORDERNO_CD%TYPE) IS
			SELECT
				TAR.ARRIVORDERNO_CD	,
				MAX(TAR.STARRIV_ID) AS STARRIV_ID
			FROM
				TA_ARRIV TAR
			WHERE
				TAR.ARRIVORDERNO_CD = ArrivOrderNoCD
			GROUP BY
				TAR.ARRIVORDERNO_CD;
		rowTmp	curTmp%ROWTYPE;
		
		rowUser	VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		-- 入荷予定取得
		OPEN curTmp(iArrivOrderNoCD);
		FETCH curTmp INTO rowTmp;
		-- ステータスチェック
		IF rowTmp.STARRIV_ID = PS_DEFINE.ST_ARRIVM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '既にキャンセルされています。入荷予定番号 = [' || iArrivOrderNoCD || ']');
		END IF;
		IF rowTmp.STARRIV_ID >= PS_DEFINE.ST_ARRIV3_ARRIV_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '受付が完了しています。入荷予定番号 = [' || iArrivOrderNoCD || ']');
		END IF;
		CLOSE curTmp;
		-- 入荷予定更新
		UPDATE TA_ARRIV
		SET
			STARRIV_ID		= PS_DEFINE.ST_ARRIVM2_CANCEL,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			ARRIVORDERNO_CD	= iArrivOrderNoCD;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTmp%ISOPEN THEN CLOSE curTmp; END IF;
			RAISE;
	END CanArrivPlan;
END PW_ARRIV;
/
SHOW ERRORS
