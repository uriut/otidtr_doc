-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PI_FILESEND AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PI_FILESEND';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************/
	/*	トランザクションID新規取得								*/
	/************************************************************/
	FUNCTION GetNewTrnId RETURN NUMBER;
	/************************************************************/
	/* 入荷予定ダウンロード完了                                 */
	/************************************************************/
	PROCEDURE EndDownLoadArriv;
END PI_FILESEND;
/
SHOW ERRORS
