-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PI_STOCKRESULT AS
	/*----------------------------------------------------------*/
	/* Public                                                   */
	/*----------------------------------------------------------*/
	/************************************************************/
	/* �A�b�v���[�h�����i�݌Ɏ��сj                             */
	/************************************************************/
	PROCEDURE EndUploadStockResult(
		iTrnID						IN TMP_STOCKRESULT.TRN_ID%TYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'EndUploadStockResult';
		-- TMP
		CURSOR curTmp(TrnID	TMP_STOCKRESULT.TRN_ID%TYPE) IS
			SELECT
				SUB2.SKU_CD		,
				NVL(MS.EXTERIOR_CD, ' ') AS EXTERIOR_CD	,
				NVL(MS.PCSPERCASE_NR,0) AS AMOUNT_NR	,
				SUB2.LOT_TX		,
				SUB2.WMSPCS_NR	,
				SUB2.HOSTPCS_NR
			FROM
				(
				SELECT
					SUB.SKU_CD		,
					SUB.LOT_TX		,
					SUM(SUB.WMSPCS_NR) AS WMSPCS_NR	,
					SUM(SUB.HOSTPCS_NR) AS HOSTPCS_NR
				FROM
					(
					SELECT
						SKU_CD						,
						LOT_TX						,
						SUM(PCS_NR) AS WMSPCS_NR	,
						0 AS HOSTPCS_NR
					FROM
						TA_LOCASTOCKP
					GROUP BY
						SKU_CD						,
						LOT_TX
					UNION ALL
					SELECT
						SKU_CD						,
						LOT_TX						,
						SUM(PCS_NR) AS WMSPCS_NR	,
						0 AS HOSTPCS_NR
					FROM
						TA_LOCASTOCKC
					WHERE
						STSTOCK_ID = 0	-- ��[�A�ړ�������
					GROUP BY
						SKU_CD						,
						LOT_TX
					UNION ALL
					SELECT
						NINUSHI || SYOCD AS SKU_CD	,
						LOT AS LOT_TX				,
						0 AS WMSPCS_NR				,
						SUM(KZANK) * KAN1 + SUM(KZANH) AS HOSTPCS_NR
					FROM
						TMP_STOCKRESULT
					WHERE
						TRN_ID = TrnID
					GROUP BY
						NINUSHI		,
						SYOCD		,
						LOT			,
						KAN1
					) SUB
				GROUP BY
					SUB.SKU_CD		,
					SUB.LOT_TX
				) SUB2
					LEFT OUTER JOIN MA_SKU MS
						ON SUB2.SKU_CD = MS.SKU_CD
			WHERE
				SUB2.WMSPCS_NR <> SUB2.HOSTPCS_NR
			ORDER BY
				SUB2.SKU_CD;
		rowTmp curTmp%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		--
		DELETE Z_CHECKSTOCK;
		
		OPEN curTmp(iTrnID);
		LOOP
			FETCH curTmp INTO rowTmp;
			EXIT WHEN curTmp%NOTFOUND;
				-- �o�^
				INSERT INTO Z_CHECKSTOCK (
					SKU_CD		,
					EXTERIOR_CD	,
					AMOUNT_NR	,
					LOT_TX		,
					WMSPCS_NR	,
					HOSTPCS_NR	,
					ADD_DT
				) VALUES (
					rowTmp.SKU_CD		,
					rowTmp.EXTERIOR_CD	,
					rowTmp.AMOUNT_NR	,
					rowTmp.LOT_TX		,
					rowTmp.WMSPCS_NR	,
					rowTmp.HOSTPCS_NR	,
					SYSDATE
				);
		END LOOP;
		CLOSE curTmp;
		
		COMMIT;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTmp%ISOPEN THEN CLOSE curTmp; END IF;
			RAISE;
	END EndUploadStockResult;
END PI_STOCKRESULT;
/
SHOW ERRORS
