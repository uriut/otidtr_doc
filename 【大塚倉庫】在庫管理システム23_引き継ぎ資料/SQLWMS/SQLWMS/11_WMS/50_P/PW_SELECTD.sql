-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PW_SELECT IS
	/************************************************************************************/
	/*	TRNID�擾																		*/
	/************************************************************************************/
	PROCEDURE GetTrnID(
		oTrnID			OUT NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetTrnID';
		numTrnID		NUMBER;
	BEGIN
		PS_NO.GetTrnId(numTrnID);
		oTrnID := numTrnID;
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetTrnID;
	/************************************************************************************/
	/*	�I��ID�ǉ�																		*/
	/************************************************************************************/
	PROCEDURE InsSelectID(
		iTrnID			IN NUMBER,
		iSelectID		IN NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsSelectID';
		numCount		NUMBER;
	BEGIN
		SELECT
			COUNT(*)
		INTO
			numCount
		FROM
			TMP_SELECTID
		WHERE
			TRN_ID		= iTrnID AND
			SELECT_ID	= iSelectID;
		IF numCount = 0 THEN
			INSERT INTO TMP_SELECTID (
				TRN_ID		,
				SELECT_ID	,
				ADD_DT
			) VALUES (
				iTrnID		,
				iSelectID	,
				SYSDATE
			);
		END IF;
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsSelectID;
	/************************************************************************************/
	/*	�I��ID�폜																		*/
	/************************************************************************************/
	PROCEDURE DelSelectID(
		iTrnID			IN NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DelSelectID';
	BEGIN
		DELETE TMP_SELECTID
		WHERE
			TRN_ID = iTrnID;
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END DelSelectID;
	/************************************************************************************/
	/*	�I��CD�ǉ�																		*/
	/************************************************************************************/
	PROCEDURE InsSelectCD(
		iTrnID			IN NUMBER,
		iSelectCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsSelectCD';
		numCount		NUMBER;
	BEGIN
		SELECT
			COUNT(*)
		INTO
			numCount
		FROM
			TMP_SELECTCD
		WHERE
			TRN_ID		= iTrnID AND
			SELECT_CD	= iSelectCD;
		IF numCount = 0 THEN
			INSERT INTO TMP_SELECTCD (
				TRN_ID		,
				SELECT_CD	,
				ADD_DT
			) VALUES (
				iTrnID		,
				iSelectCD	,
				SYSDATE
			);
		END IF;
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsSelectCD;
	/************************************************************************************/
	/*	�I��CD�폜																		*/
	/************************************************************************************/
	PROCEDURE DelSelectCD(
		iTrnID			IN NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DelSelectCD';
	BEGIN
		DELETE TMP_SELECTCD
		WHERE
			TRN_ID = iTrnID;
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END DelSelectCD;
END PW_SELECT;
/
SHOW ERRORS
