-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_APPLYSHIP AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_APPLYSHIP';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************************************/
	/*	ピッキング引当																	*/
	/************************************************************************************/
	PROCEDURE ApplyShipMain(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyShipCheck(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	); 

	PROCEDURE ApplyShipOnePallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyShipSumPallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyShipSumPalletOrder(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyShipSumPalletStock(
		iTrnID				IN	NUMBER	,
		iSkuCD	   			IN	VARCHAR2,
		iLotTX	   			IN	VARCHAR2,
		iTypeStockCD		IN	VARCHAR2,
		iPalletCapacityNR	IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyShipCase(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE GetShipData(
		iShipDID			IN	NUMBER			,
		orowShipH			OUT TA_SHIPH%ROWTYPE,
		orowShipD			OUT TA_SHIPD%ROWTYPE
	);

	PROCEDURE ApplyStockOnePallet(
		iTrnID				IN	NUMBER	,
		iShipDID			IN	NUMBER	,
		iPalletDetNoCD		IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTypeStockCD		IN	VARCHAR2,
		iPallerCapacityNR	IN  NUMBER  ,
		iPcsNR				IN	NUMBER	,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	--トータルピック
	PROCEDURE ApplyStockSumPalletTotal(
		iTrnID				IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTypeStockCD		IN	VARCHAR2,
		iPallerCapacityNR	IN  NUMBER  ,
		iSumPcsNR			IN	NUMBER	,
		iTransporter2CD		IN	VARCHAR2, --配送業者
		iPalletDetNoCD		IN	NUMBER	, --パレット明細番号を仮付番
		irowLocaStockP		IN  TA_LOCASTOCKP%ROWTYPE,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iTypeCarryCD		IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	--軒先ピック
	PROCEDURE ApplyStockSumPalletOrder(
		iTrnID				IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTypeStockCD		IN	VARCHAR2,
		iPallerCapacityNR	IN  NUMBER  ,
		iSumPcsNR			IN	NUMBER	,
		iTransporter2CD		IN	VARCHAR2, --配送業者
		iDtNameTX			IN	VARCHAR2,    
		iDtAddress1TX		IN	VARCHAR2,
		iDtAddress2TX		IN	VARCHAR2,
		iDtAddress3TX		IN	VARCHAR2,
		iDtAddress4TX		IN	VARCHAR2,
		iDirectDeliveryCD 	IN	VARCHAR2, --直送先コード
		iDeliDateTX			IN	VARCHAR2, --着荷指定日
		iPalletDetNoCD		IN	NUMBER	, --パレット明細番号を仮付番
		irowLocaStockP		IN  TA_LOCASTOCKP%ROWTYPE,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iTypeCarryCD		IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ApplyStockCase(
		iTrnID				IN	NUMBER	,
		iShipDID			IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTypeStockCD		IN	VARCHAR2,
		iPallerCapacityNR	IN  NUMBER  ,
		iPcsNR				IN	NUMBER	,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ReApplyMain(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2,
		oApplyFL			OUT NUMBER --引当フラグ
	);

	PROCEDURE ReApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

	PROCEDURE ReApplyPick(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2,
		oApplyFL			OUT NUMBER --再引当フラグ
	);

	PROCEDURE ReApplyStock(
		iTrnID				IN	NUMBER	,
		iLocaStockCID		IN 	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);

END PS_APPLYSHIP;
/
SHOW ERRORS

