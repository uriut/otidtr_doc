-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PW_PRINT IS
	/************************************************************************************/
	/*	ピッキング指示書印刷完了（個別）												*/
	/************************************************************************************/
	PROCEDURE EndPrintPickBara(
		iPickNoCD			IN	VARCHAR2,
		iPalletDetNoCD		IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'EndPrintPickBara';
		-- ピッキング
		CURSOR curPick(PickNoCD TA_PICK.PICKNO_CD%TYPE,PalletDetNoCD TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				PICKNO_CD		,
				PALLETDETNO_CD	,
				MIN(STPICK_ID) STPICK_ID,
				MAX(PRINT_FL) PRINT_FL
			FROM
				TA_PICK
			WHERE
				PICKNO_CD		= PickNoCD AND
				PALLETDETNO_CD	= PalletDetNoCD
			GROUP BY
				PICKNO_CD		,
				PALLETDETNO_CD
			HAVING
				MIN(STPICK_ID) > PS_DEFINE.ST_PICK0_PRINTNG
			ORDER BY
				PICKNO_CD		,
				PALLETDETNO_CD	;
		rowPick		curPick%ROWTYPE;
		--
		-- rowUser		VA_USER%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iPickNoCD = [' || iPickNoCD || '], iUserCD = [' || iUserCD || ']'
		);
		-- マスタ取得
		-- PS_MASTER.GetUser(iUserCD, rowUser);
		-- ピッキング指示
		OPEN curPick(iPickNoCD,iPalletDetNoCD);
		FETCH curPick INTO rowPick;
		IF  curPick%FOUND THEN
			-- ステータス更新
			IF rowPick.STPICK_ID = PS_DEFINE.ST_PICK1_PRINTOK THEN
				-- ピッキング更新
				UPDATE TA_PICK
				SET
					STPICK_ID		= PS_DEFINE.ST_PICK2_PRINT_E,
					PRINT_FL		= 0					,	-- todo
					--
					UPD_DT			= SYSDATE			,
					UPDUSER_CD		= iUserCD			,
					UPDUSER_TX		= iUserTX			,
					UPDPROGRAM_CD	= iProgramCD		,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
				WHERE
					PICKNO_CD		= rowPick.PICKNO_CD	AND
					PALLETDETNO_CD	= rowPick.PALLETDETNO_CD;
				-- 出荷指示
				UPDATE TA_SHIPH
				SET
					STSHIP_ID		= PS_DEFINE.ST_SHIP10_PICKPRINT_E,
					--
					UPD_DT			= SYSDATE			,
					UPDUSER_CD		= iUserCD			,
					UPDUSER_TX		= iUserTX			,
					UPDPROGRAM_CD	= iProgramCD		,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
				WHERE
					STSHIP_ID	= PS_DEFINE.ST_SHIP9_PAPPLY_E	AND
					SHIPH_ID IN (
						SELECT
							SHIPH_ID
						FROM
							TA_PICK
						WHERE
							PICKNO_CD		= rowPick.PICKNO_CD			AND
							PALLETDETNO_CD	= rowPick.PALLETDETNO_CD
					);
			END IF;
			-- 再印刷
			IF rowPick.PRINT_FL = 1 THEN
				-- ピッキング更新
				UPDATE TA_PICK
				SET
					PRINT_FL		= 0					,
					--
					UPD_DT			= SYSDATE			,
					UPDUSER_CD		= iUserCD			,
					UPDUSER_TX		= iUserTX			,
					UPDPROGRAM_CD	= iProgramCD		,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
				WHERE
					PICKNO_CD		= rowPick.PICKNO_CD	AND
					PALLETDETNO_CD	= rowPick.PALLETDETNO_CD;
			END IF;
			
			-- 印刷情報更新
			UPDATE TA_PICK
			SET
				PRTCOUNT_NR		= PRTCOUNT_NR + 1	,
				PRT_DT			= SYSDATE			,
				PRTUSER_CD		= iUserCD			,
				PRTUSER_TX		= iUserTX
			WHERE
				PICKNO_CD		= rowPick.PICKNO_CD		AND
				PALLETDETNO_CD	= rowPick.PALLETDETNO_CD;
		END IF;
		CLOSE curPick;
		COMMIT;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END EndPrintPickBara;
END PW_PRINT;
/
SHOW ERRORS
