-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PI_SUPPLIER AS
	/************************************************************/
	/* ファイル単位チェック                                     */
	/************************************************************/
	PROCEDURE CheckByFile(
		iTrnID						IN TMP_SKU.TRN_ID%TYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'CheckByFile';
		-- アップロードされたデータの重複分
		CURSOR curFrom(TrnID	TMP_SKU.TRN_ID%TYPE) IS
			SELECT
				SKCHKCD AS SUPPLIER_CD
			FROM
				TMP_SUPPLIER
			WHERE
				TRN_ID = TrnID
			GROUP BY
				SKCHKCD
			HAVING
				COUNT(*) > 1;
		rowFrom curFrom%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 重複チェック
		OPEN curFrom(iTrnID);
		-- 対象行を取得
		FETCH curFrom INTO rowFrom;
		-- 重複行が存在した場合はエラー
		IF curFrom%FOUND THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EXCEPTION_SYS,'入荷予定明細の同一ファイル内でキーが重複しています。');
		END IF;
		CLOSE curFrom;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curFrom%ISOPEN THEN
				CLOSE curFrom;
			END IF;
			RAISE;
	END CheckByFile;
	/*----------------------------------------------------------*/
	/* Public                                                   */
	/*----------------------------------------------------------*/
	/************************************************************/
	/* アップロード完了（仕入先）                               */
	/************************************************************/
	PROCEDURE EndUploadSupplier(
		iTrnID						IN TMP_SUPPLIER.TRN_ID%TYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'EndUploadSupplier';
		-- TMP
		CURSOR curTmp(TrnID	TMP_SUPPLIER.TRN_ID%TYPE) IS
			SELECT
				SKCHKCD AS SUPPLIER_CD		,
				'01' AS TYPESUPPLIER_CD		,
				NAME AS SUPPLIER_TX			,
				YUBIN AS SUPPLIERPOST_TX	,
				JYUSYO AS SUPPLIERADDRESS_TX	-- todo
			FROM
				TMP_SUPPLIER
			WHERE
				TRN_ID = TrnID
			ORDER BY
				SKCHKCD;
		rowTmp curTmp%ROWTYPE;
		numCount			NUMBER;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- ファイル単位チェック
		CheckByFile(iTrnID);
		-- テーブル単位チェックはWMS側で実施
		OPEN curTmp(iTrnID);
		LOOP
			FETCH curTmp INTO rowTmp;
			EXIT WHEN curTmp%NOTFOUND;
			-- SUPPLIER
			SELECT
				COUNT(SUPPLIER_CD)
			INTO
				numCount
			FROM
				MA_SUPPLIER
			WHERE
				SUPPLIER_CD = rowTmp.SUPPLIER_CD;
			IF numCount = 0 THEN
				--登録
				INSERT INTO MA_SUPPLIER (
					SUPPLIER_CD			,
					TYPESUPPLIER_CD		,
					SUPPLIER_TX			,
					SUPPLIERPOST_TX		,
					SUPPLIERADDRESS_TX	,
					SUPPLIERMEMO_TX		,
					--
					UPD_DT				,
					UPDUSER_CD			,
					UPDUSER_TX			,
					ADD_DT				,
					ADDUSER_CD			,
					ADDUSER_TX			,
					UPDPROGRAM_CD		,
					UPDCOUNTER_NR		,
					STRECORD_ID			
				) VALUES (
					rowTmp.SUPPLIER_CD			,
					rowTmp.TYPESUPPLIER_CD		,
					rowTmp.SUPPLIER_TX			,
					rowTmp.SUPPLIERPOST_TX		,
					rowTmp.SUPPLIERADDRESS_TX	,
					' '							,
					--
					SYSDATE						,
					'BATCH'						,
					'BATCH'						,
					SYSDATE						,
					'BATCH'						,
					'BATCH'						,
					'BATCH'						,
					0							,
					0							
				);
			ELSE
				--更新
				UPDATE
					MA_SUPPLIER
				SET
					SUPPLIER_TX			= rowTmp.SUPPLIER_TX		,
					SUPPLIERPOST_TX		= rowTmp.SUPPLIERPOST_TX	,
					SUPPLIERADDRESS_TX	= rowTmp.SUPPLIERADDRESS_TX	,
					-- 管理項目
					UPD_DT				= SYSDATE					,
					UPDUSER_CD			= 'BATCH'					,
					UPDUSER_TX			= 'BATCH'					,
					UPDPROGRAM_CD		= 'BATCH'					,
					UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
				WHERE
					SUPPLIER_CD = rowTmp.SUPPLIER_CD;
			END IF;
		END LOOP;
		CLOSE curTmp;
		COMMIT;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END EndUploadSupplier;
END PI_SUPPLIER;
/
SHOW ERRORS
