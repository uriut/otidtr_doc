-- Copyright (C) Seaos Corporation 2014 All rights reserved
CREATE OR REPLACE PACKAGE PW_INSTOCK IS 
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PW_INSTOCK';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************************************/
	/*	格納指示書取消																	*/
	/************************************************************************************/
	PROCEDURE CanInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
	/************************************************************************************/
	/*	入庫登録																		*/
	/************************************************************************************/
	PROCEDURE RegInstock(
		iArrivID		IN NUMBER	,
		iSkuCD			IN VARCHAR2	,
		iLotTX			IN VARCHAR2	,
		iSameDayShipFL	IN NUMBER	,
		iPalletNR		IN NUMBER	,
		iCaseNR			IN NUMBER	,
		iBaraNR			IN NUMBER	,
		iPcsNR			IN NUMBER	,
		iUseLimitDT		IN DATE		,
		iCheckArrivUseLimit	IN NUMBER,	--0:入荷可能期限をチェックしない、1：チェックする
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2	,
		oInStockNoCD	OUT TA_INSTOCK.INSTOCKNO_CD%TYPE,
		oKanbanCD		OUT TA_INSTOCK.KANBAN_CD%TYPE
	);
	/************************************************************/
	/*	入庫保留												*/
	/************************************************************/
	PROCEDURE DeferInStock(
		iInStockNoCD	IN VARCHAR2,
		iUserCD			IN VARCHAR2,
		iProgramCD		IN VARCHAR2
	);
	/************************************************************/
	/*	入庫保留キャンセル										*/
	/************************************************************/
	PROCEDURE CanDeferInStock(
		iInStockNoCD	IN VARCHAR2,
		iUserCD			IN VARCHAR2,
		iProgramCD		IN VARCHAR2
	);
	/************************************************************/
	/*	入庫登録完了											*/
	/************************************************************/
	PROCEDURE EndInStock(
		iInStockNoCD	IN	VARCHAR2,
		iLocaCD			IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		iLotTX			IN	VARCHAR2,
		iAmountNR		IN	NUMBER	,
		iPalletNR		IN	NUMBER	,
		iCaseNR			IN	NUMBER	,
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
	/************************************************************/
	/*	移動紐付解除											*/
	/************************************************************/
	PROCEDURE RstiPad(
		iInStockNoCD	IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************************************/
	/*	格納開始(iPad)																	*/
	/************************************************************************************/
	PROCEDURE StartInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
END PW_INSTOCK;
/
SHOW ERRORS
