-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_LOCASEARCH IS
	/************************************************************/
	/*	����													*/
	/************************************************************/
	--�p�b�P�[�W��
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_LOCASEARCH';
	-- ���O���x��
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************************************/
	/*	�o�b�r�����i�p���b�g�j															*/
	/************************************************************************************/
	PROCEDURE PalletPcs(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iPalletCapacityNR	IN	NUMBER					, --Add
		iPalletNR			IN	NUMBER					,
		iCaseNR				IN	NUMBER					,
		iBaraNR				IN	NUMBER					,
		iPcsNR				IN	NUMBER					,
		irowSku				IN	MA_SKU%ROWTYPE			,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP 		OUT	TA_LOCASTOCKP%ROWTYPE	,
		oTypeCarryCD		OUT	VARCHAR2				,
		oSerchOK			OUT	NUMBER
	);
	/************************************************************************************/
	/*	�o�b�r�����i�P�[�X�j															*/
	/************************************************************************************/
	PROCEDURE CasePcs(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iPalletNR			IN	NUMBER					,
		iCaseNR				IN	NUMBER					,
		iBaraNR				IN	NUMBER					,
		iPcsNR				IN	NUMBER					,
		irowSku				IN	MA_SKU%ROWTYPE			,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP 		OUT	TA_LOCASTOCKP%ROWTYPE	,
		oTypeCarryCD		OUT	VARCHAR2				,
		oSerchOK			OUT	NUMBER
	);
	/************************************************************************************/
	/*	�o�b�r�����E���p���i�p���b�g�ύڐ�����j										*/
	/************************************************************************************/
	PROCEDURE PcsPalletCapa(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iPalletCapacityNR	IN	NUMBER					, --Add
		iLocaBlockCD		IN	VARCHAR2				,
		iShipVolumeCD		IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP		OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	);
	/************************************************************************************/
	/*	�o�b�r�����E���p���łȂ��i�p���b�g�ύڐ�����j									*/
	/************************************************************************************/
	PROCEDURE PcsNotPalletCapa(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iPalletCapacityNR	IN	NUMBER					, --Add
		iLocaBlockCD		IN	VARCHAR2				,
		iShipVolumeCD		IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP	 	OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	);
	/************************************************************************************/
	/*	�o�b�r�����E���p��																*/
	/************************************************************************************/
	PROCEDURE PcsPallet(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iLocaBlockCD		IN	VARCHAR2				,
		iShipVolumeCD		IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP		OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	);
	/************************************************************************************/
	/*	�o�b�r�����E���p���łȂ�														*/
	/************************************************************************************/
	PROCEDURE PcsNotPallet(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iLocaBlockCD		IN	VARCHAR2				,
		iShipVolumeCD		IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP	 	OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	);
	/************************************************************************************/
	/*	�o�b�r�����E��[																*/
	/************************************************************************************/
	PROCEDURE PcsSupp(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iLocaBlockCD		IN	VARCHAR2				,
		iShipVolumeCD		IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP	 	OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	);
	/************************************************************************************/
	/*	���P�[�V�����f�[�^�擾															*/
	/************************************************************************************/
	PROCEDURE GetLocaData(
		iRowID				IN	UROWID					,
		iLocaCD				IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP	 	OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	);

END PS_LOCASEARCH;
/
SHOW ERRORS
