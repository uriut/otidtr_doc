-- Copyright (C) Seaos Corporation 2014 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_STATUS AS
	/************************************************************/
	/*	 引当チェック（ロック取得）								*/
	/************************************************************/
	PROCEDURE ChkApply AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ChkApply';
		
		-- ステータスカーソル
		CURSOR curStApply IS
			SELECT
				SHIPAPPLY_FL	,
				MOVEAPPLY_FL	,
				AMOVEAPPLY_FL	,
				INVENTA_FL		,
				MASTER_FL
			FROM
				ST_APPLY
			FOR UPDATE;
		rowStApply curStApply%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curStApply;
		FETCH curStApply INTO rowStApply;
		IF curStApply%FOUND THEN
			IF rowStApply.SHIPAPPLY_FL = 1 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '出荷引当中です・');
			END IF;
			IF rowStApply.MOVEAPPLY_FL = 1 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '移動引当中です。');
			END IF;
			IF rowStApply.AMOVEAPPLY_FL = 1 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '積み増し引当中です。');
			END IF;
			IF rowStApply.INVENTA_FL = 1 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '棚卸中です。');
			END IF;
			IF rowStApply.MASTER_FL = 1 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'マスター関連を修正中です。');
			END IF;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ステータステーブルがありません。');
		END IF;
		CLOSE curStApply;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curStApply%ISOPEN THEN CLOSE curStApply; END IF;
			RAISE;
	END ChkApply;
	/************************************************************/
	/*	 出荷引当ステータス更新									*/
	/************************************************************/
	PROCEDURE SetShipApply AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SetShipApply';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE ST_APPLY
		SET
			SHIPAPPLY_FL = 1;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END SetShipApply;
	/************************************************************/
	/*	出荷引当ステータスリセット更新							*/
	/************************************************************/
	PROCEDURE RstShipApply AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstShipApply';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE ST_APPLY
		SET
			SHIPAPPLY_FL = 0;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END RstShipApply;
	/************************************************************/
	/*	移動引当ステータス更新									*/
	/************************************************************/
	PROCEDURE SetMoveApply AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SetMoveApply';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE ST_APPLY
		SET
			MOVEAPPLY_FL = 1;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END SetMoveApply;
	/************************************************************/
	/*	移動引当ステータスリセット更新							*/
	/************************************************************/
	PROCEDURE RstMoveApply AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstMoveApply';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE ST_APPLY
		SET
			MOVEAPPLY_FL = 0;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END RstMoveApply;
	/************************************************************/
	/*	積み増し引当ステータス更新								*/
	/************************************************************/
	PROCEDURE SetAmoveApply AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SetAmoveApply';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE ST_APPLY
		SET
			AMOVEAPPLY_FL = 1;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END SetAmoveApply;
	/************************************************************/
	/*	積み増し引当ステータスリセット更新						*/
	/************************************************************/
	PROCEDURE RstAmoveApply AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstAmoveApply';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE ST_APPLY
		SET
			AMOVEAPPLY_FL = 0;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END RstAmoveApply;
	/************************************************************/
	/*	棚卸中ステータス更新									*/
	/************************************************************/
	PROCEDURE SetInvent AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SetInvent';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE ST_APPLY
		SET
			INVENTA_FL = 1;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END SetInvent;
	/************************************************************/
	/*	棚卸中ステータスリセット更新							*/
	/************************************************************/
	PROCEDURE RstInvent AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstInvent';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE ST_APPLY
		SET
			INVENTA_FL = 0;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END RstInvent;	
	/************************************************************/
	/*	マスタ修正ステータス更新								*/
	/************************************************************/
	PROCEDURE SetMaster AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SetMaster';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE ST_APPLY
		SET
			MASTER_FL = 1;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END SetMaster;
	/************************************************************/
	/*	マスタ修正ステータスリセット更新						*/
	/************************************************************/
	PROCEDURE RstMaster AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstMaster';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE ST_APPLY
		SET
			MASTER_FL = 0;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END RstMaster;
END PS_STATUS;
/
SHOW ERRORS
