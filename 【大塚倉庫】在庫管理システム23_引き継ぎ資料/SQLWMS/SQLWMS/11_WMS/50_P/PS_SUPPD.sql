-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_SUPP AS
	/************************************************************************************/
	/*	補充データ作成																	*/
	/************************************************************************************/
	PROCEDURE InsSupp(
		iTrnID			IN	NUMBER	,
		iSuppNoCD		IN	VARCHAR2,
		iTypeSuppCD		IN	VARCHAR2,
		irowPick		IN	TA_PICK%ROWTYPE,
		irowDLocaStockC	IN	TA_LOCASTOCKC%ROWTYPE,
		iUserCD			IN	VARCHAR2,
		iUserTX			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsSupp';
		
		numSuppID		TA_SUPP.SUPP_ID%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_SUPPID.NEXtVAL INTO numSuppID
		FROM
			DUAL;
		INSERT INTO TA_SUPP (
			SUPP_ID				,	-- 補充ID
			TRN_ID				,	-- トランザクションID
			SUPPNO_CD			,	-- 補充指示NO
			TYPESUPP_CD			,	-- 補充タイプコード
			STSUPP_ID			,	-- 補充ステータス
			-- 出荷指示
			SHIPH_ID			,	-- 出荷指示ヘッダID
			SHIPD_ID			,	-- 出荷指示明細ID
			-- ピック
			PICK_ID				,	-- ピックID
			-- SKU
			SKU_CD				,	-- SKUコード
			LOT_TX				,	-- ロット
			SRCKANBAN_CD		,	-- かんばん番号
			CASEPERPALLET_NR	,	-- 
			PALLETLAYER_NR		,	-- 
			PALLETCAPACITY_NR	,	-- 
			STACKINGNUMBER_NR	,	-- 
			TYPEPLALLET_CD		,	-- 
			USELIMIT_DT			,	-- 使用期限
			STOCK_DT			,	-- 入庫日
			AMOUNT_NR			,	-- 入数 標準ケース・メーカー箱の入数
			PALLET_NR			,	-- パレット数
			CASE_NR				,	-- ケース数 
			BARA_NR				,	-- バラ数
			PCS_NR				,	-- ＰＣＳ
			-- 元ロケ
			SRCTYPESTOCK_CD		,	-- 在庫タイプコード
			SRCLOCA_CD			,	-- 元ロケーションコード
			SRCSTART_DT			,	-- 元開始時刻
			SRCEND_DT			,	-- 元終了時刻
			SRCHT_ID			,	-- 元HT
			SRCHTUSER_CD		,	-- 元HT社員コード
			SRCHTUSER_TX		,	-- 元HT社員名
			SRCLOCASTOCKC_ID	,	-- 元引当済み在庫ID
			-- 先ロケ
			DESTTYPESTOCK_CD	,	-- 在庫タイプコード
			DESTLOCA_CD			,	-- 先ロケーションコード
			DESTSTART_DT		,	-- 先開始時刻
			DESTEND_DT			,	-- 先終了時刻
			DESTHT_ID			,	-- 先HT
			DESTHTUSER_CD		,	-- 先HT社員コード
			DESTHTUSER_TX		,	-- 先HT社員名
			DESTLOCASTOCKC_ID	,	-- 先引当済み在庫ID
			-- 印刷
			PRTCOUNT_NR			,	-- 印刷回数
			PRT_DT				,	-- 印刷日
			PRTUSER_CD			,	-- 印刷社員コード
			PRTUSER_TX			,	-- 印刷社員名
			-- 
			BATCHNO_CD			,	-- バッチNO
			TYPEAPPLY_CD		,	-- 引当タイプコード
			TYPEBLOCK_CD		,	-- ブロックタイプ
			TYPECARRY_CD		,	-- 運搬タイプコード
			-- 管理項目
			UPD_DT				,	-- 更新日時
			UPDUSER_CD			,	-- 更新社員コード
			UPDUSER_TX			,	-- 更新社員名
			ADD_DT				,	-- 登録日時
			ADDUSER_CD			,	-- 登録社員コード
			ADDUSER_TX			,	-- 登録社員名
			UPDPROGRAM_CD		,	-- 更新プログラムコード
			UPDCOUNTER_NR		,	-- 更新カウンター
			STRECORD_ID				-- レコード状態（-2:削除、-1:作成中、0:通常）
		) VALUES (
			numSuppID						,	-- SUPP_ID
			iTrnID							,	-- TRN_ID
			iSuppNoCD						,	-- SUPPNO_CD
			iTypeSuppCD						,	-- TYPESUPP_CD
			PS_DEFINE.ST_SUPPM1_CREATE		,	-- STSUPP_ID
			-- 出荷指示
			irowPick.SHIPH_ID				,	-- SHIPH_ID
			irowPick.SHIPD_ID				,	-- SHIPD_ID
			-- ピック
			irowPick.PICK_ID				,	-- PICK_ID
			-- SKU
			irowPick.SKU_CD					,	-- SKU_CD
			irowPick.LOT_TX					,	-- ロット
			irowPick.KANBAN_CD				,	-- かんばん番号
			irowPick.CASEPERPALLET_NR		,	-- 
			irowPick.PALLETLAYER_NR			,	-- 
			irowPick.PALLETCAPACITY_NR		,	-- 
			irowPick.STACKINGNUMBER_NR		,	-- 
			irowPick.TYPEPLALLET_CD			,	-- 
			irowPick.USELIMIT_DT			,	-- USELIMIT_DT
			irowPick.STOCK_DT				,	-- STOCK_DT
			irowPick.AMOUNT_NR				,	-- AMOUNT_NR
			irowPick.PALLET_NR				,	-- PALLET_NR
			irowPick.CASE_NR				,	-- CASE_NR
			irowPick.BARA_NR				,	-- BARA_NR
			irowPick.PCS_NR					,	-- PCS_NR
			-- 元ロケ
			irowPick.SRCTYPESTOCK_CD		,	-- SRCTYPESTOCK_CD
			irowPick.SRCLOCA_CD				,	-- SRCLOCA_CD
			NULL							,	-- SRCSTART_DT
			NULL							,	-- SRCEND_DT
			0								,	-- SRCHT_ID
			' '								,	-- SRCHTUSER_CD
			' '								,	-- SRCHTUSER_TX
			irowPick.SRCLOCASTOCKC_ID		,	-- SRCLOCASTOCKC_ID
			-- 先ロケ
			irowDLocaStockC.TYPESTOCK_CD	,	-- DESTTYPESTOCK_CD
			irowDLocaStockC.LOCA_CD			,	-- DESTLOCA_CD
			NULL							,	-- DESTSTART_DT
			NULL							,	-- DESTEND_DT
			0								,	-- DESTHT_ID
			' '								,	-- DESTHTUSER_CD
			' '								,	-- DESTHTUSER_TX
			irowDLocaStockC.LOCASTOCKC_ID	,	-- DESTLOCASTOCKC_ID
			-- 印刷
			0								,	-- PRTCOUNT_NR
			NULL							,	-- PRT_DT
			' '								,	-- PRTEMP_CD
			' '								,	-- PRTEMP_TX
			--
			irowPick.BATCHNO_CD				,	-- BATCHNO_CD
			irowPick.TYPEAPPLY_CD			,	-- TYPEAPPLY_CD
			' '								,	-- TYPEBLOCK_CD
			irowPick.TYPECARRY_CD			,	-- TYPECARRY_CD
			-- 管理項目
			NULL							,	-- UPD_DT
			' '								,	-- UPDUSER_CD
			' '								,	-- UPDUSER_TX
			SYSDATE							,	-- ADD_DT
			iUserCD							,	-- ADDUSER_CD
			iUserTX							,	-- ADDUSER_TX
			iProgramCD						,	-- UPDPROGRAM_CD
			0								,	-- UPDCOUNTER_NR
			PS_DEFINE.ST_RECORD0_NORMAL			-- STRECORD_ID
		);
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsSupp;
	/************************************************************************************/
	/*	補充データ作成（引当ロケの端数）												*/
	/************************************************************************************/
	PROCEDURE InsSupp2(
		iTrnID			IN	NUMBER	,
		iSuppNoCD		IN	VARCHAR2,
		iTypeSuppCD		IN	VARCHAR2,
		irowPick		IN	TA_PICK%ROWTYPE,
		irowSLocaStockC	IN	TA_LOCASTOCKC%ROWTYPE,
		irowDLocaStockC	IN	TA_LOCASTOCKC%ROWTYPE,
		iUserCD			IN	VARCHAR2,
		iUserTX			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsSupp2';
		
		numSuppID		TA_SUPP.SUPP_ID%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_SUPPID.NEXtVAL INTO numSuppID
		FROM
			DUAL;
		INSERT INTO TA_SUPP (
			SUPP_ID				,	-- 補充ID
			TRN_ID				,	-- トランザクションID
			SUPPNO_CD			,	-- 補充指示NO
			TYPESUPP_CD			,	-- 補充タイプコード
			STSUPP_ID			,	-- 補充ステータス
			-- 出荷指示
			SHIPH_ID			,	-- 出荷指示ヘッダID
			SHIPD_ID			,	-- 出荷指示明細ID
			-- ピック
			PICK_ID				,	-- ピックID
			-- SKU
			SKU_CD				,	-- SKUコード
			LOT_TX				,	-- ロット
			SRCKANBAN_CD		,	-- かんばん番号
			CASEPERPALLET_NR	,	-- 
			PALLETLAYER_NR		,	-- 
			PALLETCAPACITY_NR	,	-- 
			STACKINGNUMBER_NR	,	-- 
			TYPEPLALLET_CD		,	-- 
			USELIMIT_DT			,	-- 使用期限
			STOCK_DT			,	-- 入庫日
			AMOUNT_NR			,	-- 入数 標準ケース・メーカー箱の入数
			PALLET_NR			,	-- パレット数
			CASE_NR				,	-- ケース数 
			BARA_NR				,	-- バラ数
			PCS_NR				,	-- ＰＣＳ
			-- 元ロケ
			SRCTYPESTOCK_CD		,	-- 在庫タイプコード
			SRCLOCA_CD			,	-- 元ロケーションコード
			SRCSTART_DT			,	-- 元開始時刻
			SRCEND_DT			,	-- 元終了時刻
			SRCHT_ID			,	-- 元HT
			SRCHTUSER_CD		,	-- 元HT社員コード
			SRCHTUSER_TX		,	-- 元HT社員名
			SRCLOCASTOCKC_ID	,	-- 元引当済み在庫ID
			-- 先ロケ
			DESTTYPESTOCK_CD	,	-- 在庫タイプコード
			DESTLOCA_CD			,	-- 先ロケーションコード
			DESTSTART_DT		,	-- 先開始時刻
			DESTEND_DT			,	-- 先終了時刻
			DESTHT_ID			,	-- 先HT
			DESTHTUSER_CD		,	-- 先HT社員コード
			DESTHTUSER_TX		,	-- 先HT社員名
			DESTLOCASTOCKC_ID	,	-- 先引当済み在庫ID
			-- 印刷
			PRTCOUNT_NR			,	-- 印刷回数
			PRT_DT				,	-- 印刷日
			PRTUSER_CD			,	-- 印刷社員コード
			PRTUSER_TX			,	-- 印刷社員名
			-- 
			BATCHNO_CD			,	-- バッチNO
			TYPEAPPLY_CD		,	-- 引当タイプコード
			TYPEBLOCK_CD		,	-- ブロックタイプ
			TYPECARRY_CD		,	-- 運搬タイプコード
			-- 管理項目
			UPD_DT				,	-- 更新日時
			UPDUSER_CD			,	-- 更新社員コード
			UPDUSER_TX			,	-- 更新社員名
			ADD_DT				,	-- 登録日時
			ADDUSER_CD			,	-- 登録社員コード
			ADDUSER_TX			,	-- 登録社員名
			UPDPROGRAM_CD		,	-- 更新プログラムコード
			UPDCOUNTER_NR		,	-- 更新カウンター
			STRECORD_ID				-- レコード状態（-2:削除、-1:作成中、0:通常）
		) VALUES (
			numSuppID						,	-- SUPP_ID
			iTrnID							,	-- TRN_ID
			iSuppNoCD						,	-- SUPPNO_CD
			iTypeSuppCD						,	-- TYPESUPP_CD
			PS_DEFINE.ST_SUPPM1_CREATE		,	-- STSUPP_ID
			-- 出荷指示
			0								,	-- SHIPH_ID
			0								,	-- SHIPD_ID
			-- ピック
			irowPick.PICK_ID				,	-- PICK_ID
			-- SKU
			irowPick.SKU_CD					,	-- SKU_CD
			irowPick.LOT_TX					,	-- ロット
			irowPick.KANBAN_CD				,	-- かんばん番号
			irowPick.CASEPERPALLET_NR		,	-- 
			irowPick.PALLETLAYER_NR			,	-- 
			irowPick.PALLETCAPACITY_NR		,	-- 
			irowPick.STACKINGNUMBER_NR		,	-- 
			irowPick.TYPEPLALLET_CD			,	-- 
			irowPick.USELIMIT_DT			,	-- USELIMIT_DT
			irowPick.STOCK_DT				,	-- STOCK_DT
			irowPick.AMOUNT_NR				,	-- AMOUNT_NR
			irowSLocaStockC.PALLET_NR		,	-- PALLET_NR
			irowSLocaStockC.CASE_NR			,	-- CASE_NR
			irowSLocaStockC.BARA_NR			,	-- BARA_NR
			irowSLocaStockC.PCS_NR			,	-- PCS_NR
			-- 元ロケ
			irowSLocaStockC.TYPESTOCK_CD	,	-- SRCTYPESTOCK_CD
			irowSLocaStockC.LOCA_CD			,	-- SRCLOCA_CD
			NULL							,	-- SRCSTART_DT
			NULL							,	-- SRCEND_DT
			0								,	-- SRCHT_ID
			' '								,	-- SRCHTUSER_CD
			' '								,	-- SRCHTUSER_TX
			irowSLocaStockC.LOCASTOCKC_ID	,	-- SRCLOCASTOCKC_ID
			-- 先ロケ
			irowDLocaStockC.TYPESTOCK_CD	,	-- DESTTYPESTOCK_CD
			irowDLocaStockC.LOCA_CD			,	-- DESTLOCA_CD
			NULL							,	-- DESTSTART_DT
			NULL							,	-- DESTEND_DT
			0								,	-- DESTHT_ID
			' '								,	-- DESTHTUSER_CD
			' '								,	-- DESTHTUSER_TX
			irowDLocaStockC.LOCASTOCKC_ID	,	-- DESTLOCASTOCKC_ID
			-- 印刷
			0								,	-- PRTCOUNT_NR
			NULL							,	-- PRT_DT
			' '								,	-- PRTEMP_CD
			' '								,	-- PRTEMP_TX
			--
			irowPick.BATCHNO_CD				,	-- BATCHNO_CD
			irowPick.TYPEAPPLY_CD			,	-- TYPEAPPLY_CD
			' '								,	-- TYPEBLOCK_CD
			irowPick.TYPECARRY_CD			,	-- TYPECARRY_CD
			-- 管理項目
			NULL							,	-- UPD_DT
			' '								,	-- UPDUSER_CD
			' '								,	-- UPDUSER_TX
			SYSDATE							,	-- ADD_DT
			iUserCD							,	-- ADDUSER_CD
			iUserTX							,	-- ADDUSER_TX
			iProgramCD						,	-- UPDPROGRAM_CD
			0								,	-- UPDCOUNTER_NR
			PS_DEFINE.ST_RECORD0_NORMAL			-- STRECORD_ID
		);
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsSupp2;
	/************************************************************************************/
	/*	補充指示有効																	*/
	/************************************************************************************/
	PROCEDURE SuppDataOK(
		iTrnID			IN	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SuppDataOK';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE TA_SUPP
		SET
			STSUPP_ID	= PS_DEFINE.ST_SUPP0_NOTSUPP
		WHERE
			TRN_ID		= iTrnID						AND
			STSUPP_ID	= PS_DEFINE.ST_SUPPM1_CREATE	;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END SuppDataOK;
	/************************************************************************************/
	/*	補充ＰＣＳ取得																	*/
	/************************************************************************************/
	PROCEDURE GetSuppPcs(
		iTypeSuppCD		IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		oPcsNR			OUT	NUMBER
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'GetSuppPcs';
		numRowPoint 	NUMBER := 0;
		CURSOR curSupp(TypeSuppCD	TA_SUPP.TYPESUPP_CD%TYPE,
							SkuCD 	TA_SUPP.SKU_CD%TYPE) IS
			SELECT
				SKU_CD,
				PCS_NR
			FROM
				TA_SUPP
			WHERE
				SKU_CD		= SkuCD							AND
				TYPESUPP_CD	= TypeSuppCD					AND
				STSUPP_ID	<> PS_DEFINE.ST_SUPPM2_CANCEL	AND
				STSUPP_ID	<> PS_DEFINE.ST_SUPPM1_CREATE	AND
				DESTEND_DT		IS NULL	;
		rowSupp	curSupp%ROWTYPE;

		numPcs	TA_SUPP.PCS_NR%TYPE := 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curSupp(iTypeSuppCD,iSkuCD);
		numPcs := 0;
		LOOP
			FETCH curSupp INTO rowSupp;
			EXIT WHEN curSupp%NOTFOUND;
			numPcs	:= numPcs + rowSupp.PCS_NR;
		END LOOP;
		CLOSE curSupp;
		oPcsNR := numPcs;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetSuppPcs;	
	
	
END PS_SUPP;
/
SHOW ERRORS
