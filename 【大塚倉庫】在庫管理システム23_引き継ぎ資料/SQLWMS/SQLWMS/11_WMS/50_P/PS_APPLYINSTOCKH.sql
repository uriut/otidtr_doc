-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_APPLYINSTOCK IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_APPLYINSTOCK';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************************************/
	/*	入庫キャンセル																	*/
	/************************************************************************************/
	PROCEDURE CanInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
	/************************************************************************************/
	/*	入庫保留																		*/
	/************************************************************************************/
	PROCEDURE DeferInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
	/************************************************************************************/
	/*	入庫保留キャンセル																*/
	/************************************************************************************/
	PROCEDURE CanDeferInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
	/************************************************************************************/
	/*	良品入庫引当																	*/
	/************************************************************************************/
	PROCEDURE ApplyInstock(
		iTrnID				IN NUMBER				,
		irowSku				IN MA_SKU%ROWTYPE		,
		irowArriv			IN TA_ARRIV%ROWTYPE		,
		iSameDaYShipFL		IN NUMBER				,
		iPalletNR			IN NUMBER				,
		iCaseNR				IN NUMBER				,
		iBaraNR				IN NUMBER				,
		iPcsNR				IN NUMBER				,
		iUseLimitDT			IN DATE					,
		iAdminLocaFL		IN NUMBER				,
		iTypeInstockCD		IN VARCHAR2				,
		iArrivID			IN NUMBER				,
		iSkuCD				IN VARCHAR2				,
		iLotTX				IN VARCHAR2				,
		iTypePalletCD		IN VARCHAR2				,
		iUserCD				IN VARCHAR2				,
		iUserTX				IN VARCHAR2				,
		iProgramCD			IN VARCHAR2				,
		oInStockNoCD		OUT TA_INSTOCK.INSTOCKNO_CD%TYPE,
		oKanbanCD			OUT TA_INSTOCK.KANBAN_CD%TYPE,
		orowArea			OUT MB_AREA%ROWTYPE
	);
	
	/************************************************************************************/
	/*	入庫ロケ検索（空きロケ）														*/
	/************************************************************************************/
	PROCEDURE ApplyStockEmpty(
		iTrnID				IN NUMBER			,
		irowSku				IN MA_SKU%ROWTYPE	,
		iLotTX				IN VARCHAR2			,
		iPalletCapacityNR	IN NUMBER			,
		iTypeLocaCD			IN VARCHAR2			,
		iTypeLocaBlockCD	IN VARCHAR2			,
		iShipVolumeCD		IN VARCHAR2			,
		iAbcID				IN NUMBER			,
		oEndFL				OUT NUMBER			,
		oLocaCD				OUT VARCHAR2		,
		oKanbanCD			OUT VARCHAR2		,
		oAreaCD				OUT VARCHAR2
	);
	/************************************************************************************/
	/*	入庫ロケ検索（該当商品の在庫あり）												*/
	/************************************************************************************/
	PROCEDURE ApplyStock(
		iTrnID				IN NUMBER			,
		irowSku				IN MA_SKU%ROWTYPE	,
		iLotTX				IN VARCHAR2			,
		iPalletCapacityNR	IN NUMBER			,
		iTypeLocaCD			IN VARCHAR2			,
		iTypeLocaBlockCD	IN VARCHAR2			,
		iShipVolumeCD		IN VARCHAR2			,
		iAbcID				IN NUMBER			,
		oEndFL				OUT NUMBER			,
		oLocaCD				OUT VARCHAR2		,
		oKanbanCD			OUT VARCHAR2		,
		oAreaCD				OUT VARCHAR2
	);
END PS_APPLYINSTOCK;
/
SHOW ERRORS
