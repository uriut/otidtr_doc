-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PW_SUPP AS
	/************************************************************/
	/*	¤Ê													*/
	/************************************************************/
	--pbP[W¼
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PW_SUPP';
	-- Ox
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************/
	/*	â[³sbNJn										*/
	/************************************************************/
	PROCEDURE StartSrcSupp(
		iSuppNoCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	â[³sbN®¹										*/
	/************************************************************/
	PROCEDURE EndSrcSupp(
		iSuppNoCD		IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		iPalletNR		IN	NUMBER	,
		iCaseNR			IN	NUMBER	,
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iSrcLocaCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	â[æüÉJn											*/
	/************************************************************/
	PROCEDURE StartDestSupp(
		iSuppNoCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	â[æüÉ®¹											*/
	/************************************************************/
	PROCEDURE EndDestSupp(
		iSuppNoCD		IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		iKanbanCD		IN	VARCHAR2,
		iPalletNR		IN	NUMBER	,
		iCaseNR			IN	NUMBER	,
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iDestLocaCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	iPadRÃ«ð											*/
	/************************************************************/
	PROCEDURE RstiPad(
		iSuppNoCD		IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
END PW_SUPP;
/
SHOW ERRORS
