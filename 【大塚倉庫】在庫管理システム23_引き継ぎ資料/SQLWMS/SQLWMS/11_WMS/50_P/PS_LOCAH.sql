-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_LOCA IS 
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_LOCA';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.HTLOG_LEVEL);
	/************************************************************/
	/*	ロケーションロック登録									*/
	/************************************************************/
	PROCEDURE InsLocaLock;
	/************************************************************/
	/*	ロケーションロック取得									*/
	/************************************************************/
	PROCEDURE GetLocaLock(
		iLocaCD 		IN	VARCHAR2
	);
	/************************************************************/
	/*	引当済み在庫取得										*/
	/************************************************************/
	PROCEDURE GetLockLocaStockC(
		iLocaStockCID	IN	NUMBER				,
		orowLocaStockC	OUT	TA_LOCASTOCKC%ROWTYPE
	);
	/************************************************************/
	/*	引当可能在庫取得										*/
	/************************************************************/
	PROCEDURE GetLockLocaStockProwid(
		iRowID			IN	UROWID				,
		orowLocaStockP	OUT	TA_LOCASTOCKP%ROWTYPE
	);
	/************************************************************/
	/*	引当可能在庫取得										*/
	/************************************************************/
	PROCEDURE GetLockLocaStockP(
		iLocaCD			IN	VARCHAR2			,
		iSkuCD			IN	VARCHAR2			,
		orowLocaStockP	OUT	TA_LOCASTOCKP%ROWTYPE
	);
	/************************************************************/
	/*	引当可能在庫取得										*/
	/************************************************************/
	PROCEDURE GetLockLocaStockP(
		iLocaCD 		IN VARCHAR2		,
		orowLocaStockP	OUT TA_LOCASTOCKP%ROWTYPE
	);
	/************************************************************/
	/*	引当可能在庫取得										*/
	/************************************************************/
	PROCEDURE GetLocaStockP(
		iLocaCD			IN	VARCHAR2			,
		orowLocaStockP	OUT TA_LOCASTOCKP%ROWTYPE
	);
	/************************************************************/
	/*	引当可能在庫取得										*/
	/************************************************************/
	PROCEDURE GetLocaStockP(
		iLocaCD			IN	VARCHAR2			,
		iSkuCD			IN	VARCHAR2			,
		orowLocaStockP	OUT TA_LOCASTOCKP%ROWTYPE
	);
	/************************************************************/
	/*	引当済み在庫取得										*/
	/************************************************************/
	PROCEDURE GetLocaStockC(
		iLocaCD			IN	VARCHAR2			,
		orowLocaStockC	OUT	TA_LOCASTOCKC%ROWTYPE
	);
	/************************************************************/
	/*	引当済み在庫取得										*/
	/************************************************************/
	PROCEDURE GetLocaStockC(
		iLocaCD			IN	VARCHAR2			,
		iSkuCD			IN	VARCHAR2			,
		orowLocaStockC	OUT	TA_LOCASTOCKC%ROWTYPE
	);
	/************************************************************/
	/*	在庫取得												*/
	/************************************************************/
	PROCEDURE GetLocaStock(
		iLocaCD			IN	VARCHAR2			,
		orowLocaStockP	OUT	TA_LOCASTOCKP%ROWTYPE
	);
	/************************************************************/
	/*	在庫取得(ロケーション全在庫)							*/
	/************************************************************/
	PROCEDURE GetLocaStockAll (
		iLocaCD			IN	VARCHAR2			,
		orowLocaStockP	OUT	TA_LOCASTOCKP%ROWTYPE
	);
	/************************************************************/
	/*	在庫取得（ＳＫＵ指定）									*/
	/************************************************************/
	PROCEDURE GetLocaStock(
		iLocaCD 		IN	VARCHAR2			,
		iSkuCD			IN	VARCHAR2			,
		orowLocaStockP	OUT	TA_LOCASTOCKP%ROWTYPE
	);
	/************************************************************/
	/*	在庫比率取得											*/
	/************************************************************/
	PROCEDURE GetLocaStockRate(
		iSkuCD			IN	VARCHAR2			,
		iLotTX			IN	VARCHAR2			,
		oNestanerRateNR	OUT MA_SKU.NESTAINERINSTOCKRATE_NR%TYPE	,
		oCaseRateNR		OUT MA_SKU.CASESTOCKRATE_NR%TYPE		,
		oPalletRateNR	OUT MA_SKU.NESTAINERINSTOCKRATE_NR%TYPE	,
		oPcsNR			OUT NUMBER				
	);
END PS_LOCA;
/
SHOW ERRORS
