-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_APPLYPICK AS


	PROCEDURE ApplyPickMain(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickMain';

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		--複数パレット分割
		PS_APPLYPICK.ApplyPickDevPallet(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD
		);

		--１パレット	
		PS_APPLYPICK.ApplyPickOnePallet(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD
		);

		--集約１パレット
		PS_APPLYPICK.ApplyPickSumPallet(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD
		);

		--１ロケ商品で３５ケース以上
		PS_APPLYPICK.ApplyPickCaseLoca(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD
		);

		--最大積載率
		PS_APPLYPICK.ApplyPickVolume(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD
		);

		--商品で３５ケース以上
		PS_APPLYPICK.ApplyPickCase(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD
		);

		--混載積みつけ
		PS_APPLYPICK.ApplyPickMixSku(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD
		);

		--ピックタイプ変換
		PS_APPLYPICK.ConvCaseToPallet(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD
		);

		--PICKNO付番
		PS_APPLYPICK.SetPickNo(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD
		);

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickMain;

	PROCEDURE ApplyPickDevPallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickDevPallet';

		CURSOR curPallet(
			TrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				PICK_ID
			FROM
				TA_PICK
			WHERE
				TRN_ID		   = TrnID					 		AND
				TYPEPICK_CD	   = PS_DEFINE.TYPE_PICK_PALLET_N 	AND --通常
				STPICK_ID	   = PS_DEFINE.ST_PICKM1_CREATE 	AND
				PALLETDETNO_CD = ' ' 							AND
				PALLET_NR	   > 1
			ORDER BY
				PICK_ID;
				
		rowPallet		curPallet%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curPallet(iTrnID);
		LOOP
			FETCH curPallet INTO rowPallet;
			EXIT WHEN curPallet%NOTFOUND;

			PS_PICK.DevPickPallet(
				iTrnID			  ,
				rowPallet.PICK_ID ,
				iUserCD			  ,
				iUserTX			  ,
				iProgramCD
			);

		END LOOP;
		CLOSE curPallet;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickDevPallet;

	--ピックのパレット数１のもの
	PROCEDURE ApplyPickOnePallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickOnePallet';

		CURSOR curPallet(
			TrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				TPK.PICK_ID,
				TPK.PALLET_NR
			FROM
				TA_PICK 		TPK,
				MA_TRANSPORTER	MTP
			WHERE
				TPK.TRN_ID		   = TrnID					 		AND
				TPK.TRANSPORTER_CD = MTP.TRANSPORTER_CD(+) 			AND
				TPK.TYPEPICK_CD	   = PS_DEFINE.TYPE_PICK_PALLET_N 	AND --通常パレット
				TPK.STPICK_ID	   = PS_DEFINE.ST_PICKM1_CREATE		AND
				TPK.PALLETDETNO_CD = ' '
			ORDER BY
				NVL(MTP.PICKORDER_NR, 0),
				TPK.TRANSPORTER_CD,
				TPK.SRCLOCA_CD	  , --ロケ順
				TPK.PICK_ID		  ;
				
		rowPallet			curPallet%ROWTYPE;
		chrPalletDetNoCD	TA_PICK.PALLETDETNO_CD%TYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curPallet(iTrnID);
		LOOP
			FETCH curPallet INTO rowPallet;
			EXIT WHEN curPallet%NOTFOUND;

			IF rowPallet.PALLET_NR = 1 THEN
				--付番
				PS_NO.GetPalletDetNo(chrPalletDetNoCD);

				UPDATE
					TA_PICK
				SET
					PALLETDETNO_CD  = chrPalletDetNoCD,
					TYPEAPPLY_CD	= PS_DEFINE.TYPE_APPLY_PALLET_N,
					--
					UPD_DT			= SYSDATE		,
					UPDUSER_CD		= iUserCD		,
					UPDUSER_TX		= iUserTX		,
					UPDPROGRAM_CD	= iProgramCD	,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
				WHERE
					PICK_ID			= rowPallet.PICK_ID;

			ELSE
				--異常
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：パレット数が１ではありません。' ||
																 'PICK_ID = [' || rowPallet.PICK_ID || ']');
			END IF;

		END LOOP;
		CLOSE curPallet;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickOnePallet;

	PROCEDURE ApplyPickSumPallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickSumPallet';

		CURSOR curPallet(
			TrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				TPK.SKU_CD,
				TPK.SRCLOCA_CD,
				TPK.PALLETDETNO_CD,
				TPK.PALLETCAPACITY_NR,
				SUM(TPK.PCS_NR) AS PCS_NR
			FROM
				TA_PICK 		TPK,
				MA_TRANSPORTER	MTP
			WHERE
				TPK.TRN_ID		   = TrnID						  AND
				TPK.TRANSPORTER_CD = MTP.TRANSPORTER_CD(+) 		  AND
				TPK.TYPEPICK_CD	   = PS_DEFINE.TYPE_PICK_PALLET_S AND --集約パレット
				TPK.STPICK_ID	   = PS_DEFINE.ST_PICKM1_CREATE
			GROUP BY
				TPK.SKU_CD,
				TPK.SRCLOCA_CD,
				TPK.PALLETDETNO_CD,	--軒先ピックを考慮して付番済み
				TPK.PALLETCAPACITY_NR,
				MTP.PICKORDER_NR,
				TPK.TRANSPORTER_CD --データはTRANSPORTER2_CD
			ORDER BY
				NVL(MTP.PICKORDER_NR, 0),
				TPK.TRANSPORTER_CD,
				TPK.SRCLOCA_CD	  , --ロケ順
				TPK.PALLETDETNO_CD;
				
		rowPallet			curPallet%ROWTYPE;
		rowSku				MA_SKU%ROWTYPE;
		chrPalletDetNoCD	TA_PICK.PALLETDETNO_CD%TYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curPallet(iTrnID);
		LOOP
			FETCH curPallet INTO rowPallet;
			EXIT WHEN curPallet%NOTFOUND;

			IF rowPallet.PALLETDETNO_CD <> ' '  THEN

				-- データ取得
				PS_MASTER.GetSku(rowPallet.SKU_CD, rowSku);

				IF rowPallet.PCS_NR / rowSku.PCSPERCASE_NR / rowPallet.PALLETCAPACITY_NR = 1 THEN

					--付番
					PS_NO.GetPalletDetNo(chrPalletDetNoCD);

					UPDATE
						TA_PICK
					SET
						PALLETDETNO_CD  = chrPalletDetNoCD, --正規番号
						TYPEAPPLY_CD	= PS_DEFINE.TYPE_APPLY_PALLET_S,
						--
						UPD_DT			= SYSDATE		,
						UPDUSER_CD		= iUserCD		,
						UPDUSER_TX		= iUserTX		,
						UPDPROGRAM_CD	= iProgramCD	,
						UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
					WHERE
						TRN_ID			= iTrnID AND
						PALLETDETNO_CD	= rowPallet.PALLETDETNO_CD; --仮番号

				ELSE
					--異常
					RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：集約パレット数が不正です。' ||
																	 'SKU_CD = [' || rowPallet.SKU_CD || '], ' ||
																	 'PALLETCAPACITY_NR = [' || rowPallet.PALLETCAPACITY_NR || ']');
				END IF;

			ELSE
				--異常
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：パレット明細仮番号が不正です。');
			END IF;

		END LOOP;
		CLOSE curPallet;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickSumPallet;

	--１ロケ商品で３５ケース以上のもの
	PROCEDURE ApplyPickCaseLoca(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickCaseLoca';

		CURSOR curCase(
			TrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				TPK.SKU_CD,
				TPK.LOT_TX,
				TPK.SRCLOCA_CD,
				TPK.TRANSPORTER_CD,
				TPK.TOTALPIC_FL
			FROM
				TA_PICK			TPK,
				MA_SKU			MSK,
				MA_TRANSPORTER	MTP
			WHERE
				TPK.TRN_ID		   = TrnID						 AND
				TPK.SKU_CD		   = MSK.SKU_CD					 AND
				TPK.TRANSPORTER_CD = MTP.TRANSPORTER_CD(+) 		 AND
				TPK.TYPEPICK_CD	   = PS_DEFINE.TYPE_PICK_CASE	 AND --ケース
				TPK.STPICK_ID	   = PS_DEFINE.ST_PICKM1_CREATE  AND
				TPK.PALLETDETNO_CD = ' '						 AND
				TPK.TOTALPIC_FL    = 1								 --トータルピック対象
			GROUP BY
				TPK.SKU_CD,
				TPK.LOT_TX, --ロット別にする
				TPK.SRCLOCA_CD,
				MTP.PICKORDER_NR,
				TPK.TRANSPORTER_CD, --データはTRANSPORTER2_CD
				TPK.TOTALPIC_FL,
				MSK.PCSPERCASE_NR
			HAVING
				SUM(TPK.PCS_NR) / MSK.PCSPERCASE_NR > 35
			ORDER BY
				NVL(MTP.PICKORDER_NR, 0),
				TPK.TRANSPORTER_CD,
				TPK.SKU_CD		  ,
				TPK.LOT_TX		  ,
				TPK.SRCLOCA_CD	  ;
				
		rowCase				curCase%ROWTYPE;
		chrPalletDetNoCD	TA_PICK.PALLETDETNO_CD%TYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curCase(iTrnID);
		LOOP
			FETCH curCase INTO rowCase;
			EXIT WHEN curCase%NOTFOUND;

			--付番
			PS_NO.GetPalletDetNo(chrPalletDetNoCD);

			--トータルピック
			UPDATE
				TA_PICK
			SET
				PALLETDETNO_CD = chrPalletDetNoCD,
				TYPEAPPLY_CD   = PS_DEFINE.TYPE_APPLY_CASE_LOCA,
				--
				UPD_DT		   = SYSDATE		,
				UPDUSER_CD	   = iUserCD		,
				UPDUSER_TX	   = iUserTX		,
				UPDPROGRAM_CD  = iProgramCD		,
				UPDCOUNTER_NR  = UPDCOUNTER_NR + 1
			WHERE
				TRN_ID		   = iTrnID						AND
				SKU_CD		   = rowCase.SKU_CD				AND
				LOT_TX		   = rowCase.LOT_TX				AND --ロット別にする
				SRCLOCA_CD	   = rowCase.SRCLOCA_CD			AND --１ロケでピック完結
				TRANSPORTER_CD = rowCase.TRANSPORTER_CD		AND --データはTRANSPORTER2_CD
				TOTALPIC_FL	   = rowCase.TOTALPIC_FL		AND
				TYPEPICK_CD	   = PS_DEFINE.TYPE_PICK_CASE	AND --ケース 
				STPICK_ID	   = PS_DEFINE.ST_PICKM1_CREATE AND          
				PALLETDETNO_CD = ' ';

		END LOOP;
		CLOSE curCase;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickCaseLoca;

	--総体積がパレットの最大積載率以上
	PROCEDURE ApplyPickVolume(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickVolume';

		CURSOR curVolume(
			TrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				TPK.SKU_CD			 ,
				TPK.LOT_TX			 ,
				TPK.TRANSPORTER_CD	 , --データはTRANSPORTER2_CD
				' ' AS DTNAME_TX,
				' ' AS DTADDRESS1_TX,
				' ' AS DTADDRESS2_TX,
				' ' AS DTADDRESS3_TX,
				' ' AS DTADDRESS4_TX,
				' ' AS DIRECTDELIVERY_CD,
				' ' AS DELIDATE_TX	 ,      
				TPK.PALLETCAPACITY_NR,
				TPK.TOTALPIC_FL		 ,
				NVL(MTR.PICKORDER_NR, 0) AS PICKORDER_NR
			FROM
				TA_PICK			TPK,
				MA_SKU			MSK,
				MB_TYPEPALLET	MTP,
				MA_TRANSPORTER	MTR
			WHERE
				TPK.TRN_ID			= TrnID					 	 AND
				TPK.SKU_CD			= MSK.SKU_CD				 AND
				TPK.TYPEPLALLET_CD 	= MTP.TYPEPALLET_CD(+)		 AND --(+)は現在上位に不具合あるため暫定
				TPK.TRANSPORTER_CD  = MTR.TRANSPORTER_CD(+) 	 AND
				TPK.TYPEPICK_CD		= PS_DEFINE.TYPE_PICK_CASE	 AND --ケース
				TPK.STPICK_ID		= PS_DEFINE.ST_PICKM1_CREATE AND
				TPK.PALLETDETNO_CD  = ' '						 AND
				TPK.TOTALPIC_FL 	= 1								 --トータルピック対象
			GROUP BY
				TPK.SKU_CD			 ,
				TPK.LOT_TX			 , --ロット別にする
				MTR.PICKORDER_NR	 ,
				TPK.TRANSPORTER_CD	 ,
				MSK.PCSPERCASE_NR	 ,
				TPK.PALLETCAPACITY_NR,
				MTP.MAXLOADFACTOR_NR ,
				TPK.TOTALPIC_FL
			HAVING
				--ケース数 / パレット積載数 が 最大積載率 0.6 〜 0.8 以上
				SUM(TPK.PCS_NR / MSK.PCSPERCASE_NR / TPK.PALLETCAPACITY_NR) >= MTP.MAXLOADFACTOR_NR
			UNION ALL
			SELECT
				TPK.SKU_CD			 ,
				TPK.LOT_TX			 ,
				TPK.TRANSPORTER_CD	 , --データはTRANSPORTER2_CD
				TPK.DTNAME_TX		 ,     
				TPK.DTADDRESS1_TX	 , 
				TPK.DTADDRESS2_TX	 , 
				TPK.DTADDRESS3_TX	 , 
				TPK.DTADDRESS4_TX	 , 
				TPK.DIRECTDELIVERY_CD, --直送先コード 
				TPK.DELIDATE_TX		 , --着荷指定日   
				TPK.PALLETCAPACITY_NR,
				TPK.TOTALPIC_FL		 ,
				NVL(MTR.PICKORDER_NR, 0) AS PICKORDER_NR
			FROM
				TA_PICK			TPK,
				MA_SKU			MSK,
				MB_TYPEPALLET	MTP,
				MA_TRANSPORTER	MTR
			WHERE
				TPK.TRN_ID			= TrnID					 	 AND
				TPK.SKU_CD			= MSK.SKU_CD				 AND
				TPK.TYPEPLALLET_CD 	= MTP.TYPEPALLET_CD(+)		 AND --(+)は現在上位に不具合あるため暫定
				TPK.TRANSPORTER_CD  = MTR.TRANSPORTER_CD(+) 	 AND
				TPK.TYPEPICK_CD		= PS_DEFINE.TYPE_PICK_CASE	 AND --ケース
				TPK.STPICK_ID		= PS_DEFINE.ST_PICKM1_CREATE AND
				TPK.PALLETDETNO_CD  = ' '						 AND
				TPK.TOTALPIC_FL		= 0								 --軒先ピック対象
			GROUP BY
				TPK.SKU_CD			 ,
				TPK.LOT_TX			 , --ロット別にする
				MTR.PICKORDER_NR	 ,
				TPK.TRANSPORTER_CD	 ,
				MSK.PCSPERCASE_NR	 ,
				TPK.PALLETCAPACITY_NR,
				MTP.MAXLOADFACTOR_NR ,
				TPK.DTNAME_TX		 ,     
				TPK.DTADDRESS1_TX	 , 
				TPK.DTADDRESS2_TX	 , 
				TPK.DTADDRESS3_TX	 , 
				TPK.DTADDRESS4_TX	 , 
				TPK.DIRECTDELIVERY_CD,
				TPK.DELIDATE_TX		 ,
				TPK.TOTALPIC_FL
			HAVING
				--ケース数 / パレット積載数 が 最大積載率 0.6 〜 0.8 以上
				SUM(TPK.PCS_NR / MSK.PCSPERCASE_NR / TPK.PALLETCAPACITY_NR) >= MTP.MAXLOADFACTOR_NR
			ORDER BY
				PICKORDER_NR   ,
				TRANSPORTER_CD ,
				SKU_CD		   ,
				LOT_TX		   ,
				PALLETCAPACITY_NR;
				
		rowVolume			curVolume%ROWTYPE;
		chrPalletDetNoCD	TA_PICK.PALLETDETNO_CD%TYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curVolume(iTrnID);
		LOOP
			FETCH curVolume INTO rowVolume;
			EXIT WHEN curVolume%NOTFOUND;

			IF rowVolume.TOTALPIC_FL = 1 THEN
				--トータルピック
				ApplyPickVolumeSearchTotal(
					iTrnID						,
					rowVolume.SKU_CD			,
					rowVolume.LOT_TX			,
					rowVolume.TRANSPORTER_CD	,
					rowVolume.PALLETCAPACITY_NR	,
					rowVolume.TOTALPIC_FL		,
					iUserCD						,
					iUserTX						,
					iProgramCD
				);
			ELSE
				--軒先ピック
				ApplyPickVolumeSearchOrder(
					iTrnID						,
					rowVolume.SKU_CD			,
					rowVolume.LOT_TX			,
					rowVolume.TRANSPORTER_CD	,
					rowVolume.DTNAME_TX	 		,
					rowVolume.DTADDRESS1_TX		,
					rowVolume.DTADDRESS2_TX		,
					rowVolume.DTADDRESS3_TX		,
					rowVolume.DTADDRESS4_TX		,
					rowVolume.DIRECTDELIVERY_CD , --直送先コード
					rowVolume.DELIDATE_TX	   	, --着荷指定日
					rowVolume.PALLETCAPACITY_NR	,
					rowVolume.TOTALPIC_FL		,
					iUserCD						,
					iUserTX						,
					iProgramCD
				);
			END IF;

		END LOOP;
		CLOSE curVolume;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickVolume;

	PROCEDURE ApplyPickVolumeSearchTotal(
		iTrnID				IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTransporterCD		IN	VARCHAR2,
		iCapacityNR			IN	NUMBER	,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickVolumeSearchTotal';

		CURSOR curVolume(
			TrnID			TA_PICK.TRN_ID%TYPE,
			SkuCD			TA_PICK.SKU_CD%TYPE,
			LotTX			TA_PICK.LOT_TX%TYPE,
			TransporterCD	TA_PICK.TRANSPORTER_CD%TYPE,
			CapacityNR		TA_PICK.PALLETCAPACITY_NR%TYPE,
			TotalPicFL		TA_PICK.TOTALPIC_FL%TYPE
		) IS
			SELECT
				TPK.PICK_ID			 ,
				TPK.SKU_CD			 ,
				TPK.LOT_TX			 ,
				TPK.TRANSPORTER_CD	 , --データはTRANSPORTER2_CD
				MTP.MAXLOADFACTOR_NR ,
				MSK.PCSPERCASE_NR	 ,
				TPK.PALLETCAPACITY_NR,
				TPK.SRCLOCASTOCKC_ID ,
				TPK.PCS_NR / MSK.PCSPERCASE_NR AS CASE_NR,
				TPK.PCS_NR / MSK.PCSPERCASE_NR / TPK.PALLETCAPACITY_NR AS CAPACITY_NR --積載率
			FROM
				TA_PICK			TPK,
				MA_SKU			MSK,
				MB_TYPEPALLET	MTP
			WHERE
				TPK.TRN_ID			= TrnID					 	 AND
				TPK.SKU_CD			= SkuCD		 				 AND
				TPK.LOT_TX			= LotTX		 				 AND
				TPK.TRANSPORTER_CD	= TransporterCD				 AND
				TPK.PALLETCAPACITY_NR = CapacityNR				 AND
				TPK.TOTALPIC_FL		= TotalPicFL				 AND
				TPK.SKU_CD			= MSK.SKU_CD				 AND
				TPK.TYPEPLALLET_CD 	= MTP.TYPEPALLET_CD(+)		 AND --(+)は現在上位に不具合あるため暫定
				TPK.TYPEPICK_CD		= PS_DEFINE.TYPE_PICK_CASE	 AND --ケース
				TPK.STPICK_ID		= PS_DEFINE.ST_PICKM1_CREATE AND
				TPK.PALLETDETNO_CD  = ' '
			ORDER BY
				TPK.TRANSPORTER_CD	 ,
				TPK.SKU_CD			 ,
				TPK.LOT_TX			 ,
				TPK.SRCLOCA_CD		 ; --ロケ順
				
		rowVolume			curVolume%ROWTYPE;

		chrPalletDetNoCD	TA_PICK.PALLETDETNO_CD%TYPE;
		numNextSearch		NUMBER := 1;

		numSumCaseNR		NUMBER(5,0) := 0;
		numModCaseNR		NUMBER(5,0) := 0;
		numReqCaseNR		NUMBER(5,0) := 0;
		numModPcsNR			NUMBER(9,0) := 0;
		numReqPcsNR			NUMBER(9,0) := 0;
		numPalletNR			NUMBER(5,0) := 0;
		numCaseNR			NUMBER(5,0) := 0;
		numBaraNR			NUMBER(9,0) := 0;
		numPickID			NUMBER;
		numLocaStockCID		NUMBER;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		WHILE ( numNextSearch = 1 ) LOOP

			numNextSearch := 0;
			numSumCaseNR := 0;

			--付番
			PS_NO.GetPalletDetNo(chrPalletDetNoCD);

			OPEN curVolume(iTrnID, iSkuCD, iLotTX, iTransporterCD, iCapacityNR, iTotalPicFL);
			LOOP
				FETCH curVolume INTO rowVolume;
				EXIT WHEN curVolume%NOTFOUND;

				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
					'iTrnID = [' || iTrnID || '], ' ||
					'iSkuCD = [' || iSkuCD || '], ' ||
					'iLotTX = [' || iLotTX || '], ' ||
					'iTransporterCD = [' || iTransporterCD || '], ' ||
					'iTotalPicFL = [' || iTotalPicFL || '], ' ||
					'chrPalletDetNoCD = [' || chrPalletDetNoCD || '], ' ||
					'numSumCaseNR = [' || numSumCaseNR || '], ' ||
					'rowVolume.CASE_NR = [' || rowVolume.CASE_NR || '], ' ||
					'rowVolume.PALLETCAPACITY_NR = [' || rowVolume.PALLETCAPACITY_NR || ']'); 

				IF numSumCaseNR + rowVolume.CASE_NR <= rowVolume.PALLETCAPACITY_NR THEN

					--付番する
					UPDATE
						TA_PICK
					SET
						PALLETDETNO_CD  = chrPalletDetNoCD,
						TYPEAPPLY_CD    = PS_DEFINE.TYPE_APPLY_CASE_VOL,
						--
						UPD_DT			= SYSDATE		,
						UPDUSER_CD		= iUserCD		,
						UPDUSER_TX		= iUserTX		,
						UPDPROGRAM_CD	= iProgramCD	,
						UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
					WHERE
						TRN_ID	   		= iTrnID					  AND
						PICK_ID			= rowVolume.PICK_ID			  AND
						TYPEPICK_CD		= PS_DEFINE.TYPE_PICK_CASE	  AND --ケース  
						STPICK_ID		= PS_DEFINE.ST_PICKM1_CREATE  AND           
						PALLETDETNO_CD	= ' ';

					numSumCaseNR := numSumCaseNR + rowVolume.CASE_NR;
					IF numSumCaseNR = rowVolume.PALLETCAPACITY_NR THEN
						numNextSearch := 1;
						EXIT;
					END IF;
				ELSE
					--超える
					numModCaseNR := numSumCaseNR + rowVolume.CASE_NR - rowVolume.PALLETCAPACITY_NR;
					numReqCaseNR := rowVolume.CASE_NR - numModCaseNR;
					numModPcsNR  := numModCaseNR * rowVolume.PCSPERCASE_NR;
					numReqPcsNR  := numReqCaseNR * rowVolume.PCSPERCASE_NR;

					PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME ||
						'iTrnID = [' || iTrnID || '], ' ||
						'iSkuCD = [' || iSkuCD || '], ' ||
						'iLotTX = [' || iLotTX || '], ' ||
						'iTransporterCD = [' || iTransporterCD || '], ' ||
						'iTotalPicFL = [' || iTotalPicFL || '], ' ||
						'chrPalletDetNoCD = [' || chrPalletDetNoCD || '], ' ||
						'numModCaseNR = [' || numModCaseNR || '], ' ||
						'numReqCaseNR = [' || numReqCaseNR || '], ' ||
						'numModPcsNR = [' || numModPcsNR || '], ' ||
						'numReqPcsNR = [' || numReqPcsNR || ']'
					);

					PS_PICK.DevPickCase(
						iTrnID			  ,
						rowVolume.PICK_ID , --元
						iUserCD			  ,
						iUserTX			  ,
						iProgramCD		  ,
						numPickID			--残
					);

					PS_STOCK.GetUnitPcsNr(
						rowVolume.SKU_CD			,
						numReqPcsNR					, --完パレになるために必要な数量
						rowVolume.PALLETCAPACITY_NR ,
						numPalletNR					,
						numCaseNR					,
						numBaraNR
					);

					PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME ||
						'iTrnID = [' || iTrnID || '], ' ||
						'iSkuCD = [' || iSkuCD || '], ' ||
						'iLotTX = [' || iLotTX || '], ' ||
						'iTransporterCD = [' || iTransporterCD || '], ' ||
						'iTotalPicFL = [' || iTotalPicFL || '], ' ||
						'chrPalletDetNoCD = [' || chrPalletDetNoCD || '], ' ||
						'numPickID = [' || numPickID || '], ' ||
						'numPalletNR = [' || numPalletNR || '], ' ||
						'numCaseNR = [' || numCaseNR || '], ' ||
						'numBaraNR = [' || numBaraNR || ']'
					);

					--元
					UPDATE
						TA_PICK
					SET
						PALLETDETNO_CD  = chrPalletDetNoCD,
						TYPEAPPLY_CD    = PS_DEFINE.TYPE_APPLY_CASE_VOL,
						PALLET_NR		= numPalletNR	,
						CASE_NR	 		= numCaseNR	 	,
						BARA_NR	 		= numBaraNR    	,
						PCS_NR	 		= numReqPcsNR	,
						--
						UPD_DT			= SYSDATE		,
						UPDUSER_CD		= iUserCD		,
						UPDUSER_TX		= iUserTX		,
						UPDPROGRAM_CD	= iProgramCD	,
						UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
					WHERE
						TRN_ID	   		= iTrnID					  AND
						PICK_ID			= rowVolume.PICK_ID			  AND
						TYPEPICK_CD		= PS_DEFINE.TYPE_PICK_CASE	  AND --ケース  
						STPICK_ID		= PS_DEFINE.ST_PICKM1_CREATE  AND           
						PALLETDETNO_CD	= ' ';

					--元
					UPDATE
						TA_LOCASTOCKC
					SET
						PALLET_NR		= numPalletNR	,
						CASE_NR	 		= numCaseNR	 	,
						BARA_NR	 		= numBaraNR    	,
						PCS_NR	 		= numReqPcsNR	
					WHERE
						LOCASTOCKC_ID = rowVolume.SRCLOCASTOCKC_ID;

					PS_STOCK.GetUnitPcsNr(
						rowVolume.SKU_CD			,
						numModPcsNR					, --余り
						rowVolume.PALLETCAPACITY_NR ,
						numPalletNR					,
						numCaseNR					,
						numBaraNR
					);

					PS_STOCK.CopyC(
						rowVolume.SRCLOCASTOCKC_ID, --元
						numPalletNR		  ,
						numCaseNR  		  ,
						numBaraNR  		  ,
						numModPcsNR		  , --余り
						iUserCD			  ,
						iUserTX			  ,
						iProgramCD		  ,
						numLocaStockCID		--残
					);

					PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME ||
						'iTrnID = [' || iTrnID || '], ' ||
						'iSkuCD = [' || iSkuCD || '], ' ||
						'iLotTX = [' || iLotTX || '], ' ||
						'iTransporterCD = [' || iTransporterCD || '], ' ||
						'iTotalPicFL = [' || iTotalPicFL || '], ' ||
						'chrPalletDetNoCD = [' || chrPalletDetNoCD || '], ' ||
						'LOCASTOCKC_ID = [' || rowVolume.SRCLOCASTOCKC_ID || '], ' ||
						'numLocaStockCID = [' || numLocaStockCID || '], ' ||
						'numPalletNR = [' || numPalletNR || '], ' ||
						'numCaseNR = [' || numCaseNR || '], ' ||
						'numBaraNR = [' || numBaraNR || ']'
					);

					--残
					UPDATE
						TA_PICK
					SET
						PALLET_NR		 = numPalletNR	   ,
						CASE_NR			 = numCaseNR	   ,
						BARA_NR			 = numBaraNR       ,
						PCS_NR			 = numModPcsNR	   ,
						SRCLOCASTOCKC_ID = numLocaStockCID , --残
						--
						UPD_DT			 = SYSDATE		   ,
						UPDUSER_CD		 = iUserCD		   ,
						UPDUSER_TX		 = iUserTX		   ,
						UPDPROGRAM_CD	 = iProgramCD	   ,
						UPDCOUNTER_NR	 = UPDCOUNTER_NR + 1
					WHERE
						TRN_ID			 = iTrnID					  AND
						PICK_ID			 = numPickID				  AND --残
						TYPEPICK_CD		 = PS_DEFINE.TYPE_PICK_CASE	  AND --ケース  
						STPICK_ID		 = PS_DEFINE.ST_PICKM1_CREATE AND           
						PALLETDETNO_CD	 = ' ';

					--残
					UPDATE
						TA_LOCASTOCKC
					SET
						PALLET_NR		= numPalletNR	,
						CASE_NR	 		= numCaseNR	 	,
						BARA_NR	 		= numBaraNR    	,
						PCS_NR	 		= numModPcsNR	
					WHERE
						LOCASTOCKC_ID = numLocaStockCID;

					numNextSearch := 1;
					EXIT;

				END IF;

			END LOOP;
			CLOSE curVolume;

		END LOOP;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickVolumeSearchTotal;

	PROCEDURE ApplyPickVolumeSearchOrder(
		iTrnID				IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTransporterCD		IN	VARCHAR2,
		iDtNameTX			IN	VARCHAR2,    
		iDtAddress1TX		IN	VARCHAR2,
		iDtAddress2TX		IN	VARCHAR2,
		iDtAddress3TX		IN	VARCHAR2,
		iDtAddress4TX		IN	VARCHAR2,
		iDirectDeliveryCD 	IN	VARCHAR2, --直送先コード
		iDeliDateTX			IN	VARCHAR2, --着荷指定日
		iCapacityNR			IN	NUMBER	,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickVolumeSearchOrder';

		CURSOR curVolume(
			TrnID				TA_PICK.TRN_ID%TYPE,
			SkuCD				TA_PICK.SKU_CD%TYPE,
			LotTX				TA_PICK.LOT_TX%TYPE,
			TransporterCD		TA_PICK.TRANSPORTER_CD%TYPE,
			DtNameTX	 		TA_PICK.DTNAME_TX%TYPE,	 
			DtAddress1TX		TA_PICK.DTADDRESS1_TX%TYPE,
			DtAddress2TX		TA_PICK.DTADDRESS2_TX%TYPE,
			DtAddress3TX		TA_PICK.DTADDRESS3_TX%TYPE,
			DtAddress4TX		TA_PICK.DTADDRESS4_TX%TYPE,
			DirectDeliveryCD	TA_PICK.DIRECTDELIVERY_CD%TYPE,
			DeliDateTX		 	TA_PICK.DELIDATE_TX%TYPE,
			CapacityNR			TA_PICK.PALLETCAPACITY_NR%TYPE,
			TotalPicFL			TA_PICK.TOTALPIC_FL%TYPE
		) IS
			SELECT
				TPK.PICK_ID			 ,
				TPK.SKU_CD			 ,
				TPK.LOT_TX			 ,
				TPK.TRANSPORTER_CD	 , --データはTRANSPORTER2_CD
				MTP.MAXLOADFACTOR_NR ,
				MSK.PCSPERCASE_NR	 ,
				TPK.PALLETCAPACITY_NR,
				TPK.SRCLOCASTOCKC_ID ,
				TPK.PCS_NR / MSK.PCSPERCASE_NR AS CASE_NR,
				TPK.PCS_NR / MSK.PCSPERCASE_NR / TPK.PALLETCAPACITY_NR AS CAPACITY_NR --積載率
			FROM
				TA_PICK			TPK,
				MA_SKU			MSK,
				MB_TYPEPALLET	MTP
			WHERE
				TPK.TRN_ID			  = TrnID					   AND
				TPK.SKU_CD			  = SkuCD					   AND
				TPK.LOT_TX			  = LotTX					   AND
				TPK.TRANSPORTER_CD	  = TransporterCD			   AND
				TPK.DTNAME_TX	 	  = DtNameTX				   AND
				TPK.DTADDRESS1_TX	  = DtAddress1TX			   AND
				TPK.DTADDRESS2_TX	  = DtAddress2TX			   AND
				TPK.DTADDRESS3_TX	  = DtAddress3TX			   AND
				TPK.DTADDRESS4_TX	  = DtAddress4TX			   AND
				TPK.DIRECTDELIVERY_CD = DirectDeliveryCD		   AND
				TPK.DELIDATE_TX	  	  = DeliDateTX				   AND
				TPK.PALLETCAPACITY_NR = CapacityNR				   AND
				TPK.TOTALPIC_FL		  = TotalPicFL				   AND
				TPK.SKU_CD			  = MSK.SKU_CD				   AND
				TPK.TYPEPLALLET_CD	  = MTP.TYPEPALLET_CD(+)	   AND --(+)は現在上位に不具合あるため暫定
				TPK.TYPEPICK_CD		  = PS_DEFINE.TYPE_PICK_CASE   AND --ケース
				TPK.STPICK_ID		  = PS_DEFINE.ST_PICKM1_CREATE AND
				TPK.PALLETDETNO_CD	  = ' '
			ORDER BY
				TPK.TRANSPORTER_CD	 ,
				TPK.SKU_CD			 ,
				TPK.LOT_TX			 ,
				TPK.SRCLOCA_CD		 ; --ロケ順
				
		rowVolume			curVolume%ROWTYPE;

		chrPalletDetNoCD	TA_PICK.PALLETDETNO_CD%TYPE;
		numNextSearch		NUMBER := 1;

		numSumCaseNR		NUMBER(5,0) := 0;
		numModCaseNR		NUMBER(5,0) := 0;
		numReqCaseNR		NUMBER(5,0) := 0;
		numModPcsNR			NUMBER(9,0) := 0;
		numReqPcsNR			NUMBER(9,0) := 0;
		numPalletNR			NUMBER(5,0) := 0;
		numCaseNR			NUMBER(5,0) := 0;
		numBaraNR			NUMBER(9,0) := 0;
		numPickID			NUMBER;
		numLocaStockCID		NUMBER;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		WHILE ( numNextSearch = 1 ) LOOP

			numNextSearch := 0;
			numSumCaseNR := 0;

			--付番
			PS_NO.GetPalletDetNo(chrPalletDetNoCD);
		 
			OPEN curVolume(iTrnID, iSkuCD, iLotTX, iTransporterCD, iDtNameTX, iDtAddress1TX, iDtAddress2TX,
							iDtAddress3TX, iDtAddress4TX, iDirectDeliveryCD, iDeliDateTX, iCapacityNR, iTotalPicFL);
			LOOP
				FETCH curVolume INTO rowVolume;
				EXIT WHEN curVolume%NOTFOUND;

				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
					'iTrnID = [' || iTrnID || '], ' ||
					'iSkuCD = [' || iSkuCD || '], ' ||
					'iLotTX = [' || iLotTX || '], ' ||
					'iTransporterCD = [' || iTransporterCD || '], ' ||
					'iDtNameTX = [' || iDtNameTX || '], ' ||	 
					'iDtAddress1TX = [' || iDtAddress1TX || '], ' ||
					'iDtAddress2TX = [' || iDtAddress2TX || '], ' ||
					'iDtAddress3TX = [' || iDtAddress3TX || '], ' ||
					'iDtAddress4TX = [' || iDtAddress4TX || '], ' ||
					'iDirectDeliveryCD = [' || iDirectDeliveryCD || '], ' ||
					'iDeliDateTX = [' || iDeliDateTX || '], ' ||
					'iTotalPicFL = [' || iTotalPicFL || '], ' ||
					'chrPalletDetNoCD = [' || chrPalletDetNoCD || '], ' ||
					'numSumCaseNR = [' || numSumCaseNR || '], ' ||
					'rowVolume.CASE_NR = [' || rowVolume.CASE_NR || '], ' ||
					'rowVolume.PALLETCAPACITY_NR = [' || rowVolume.PALLETCAPACITY_NR || ']'
				);

				IF numSumCaseNR + rowVolume.CASE_NR <= rowVolume.PALLETCAPACITY_NR THEN

					--付番する
					UPDATE
						TA_PICK
					SET
						PALLETDETNO_CD  = chrPalletDetNoCD,
						TYPEAPPLY_CD    = PS_DEFINE.TYPE_APPLY_CASE_VOL,
						--
						UPD_DT			= SYSDATE		,
						UPDUSER_CD		= iUserCD		,
						UPDUSER_TX		= iUserTX		,
						UPDPROGRAM_CD	= iProgramCD	,
						UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
					WHERE
						TRN_ID	   		= iTrnID					  AND
						PICK_ID			= rowVolume.PICK_ID			  AND
						TYPEPICK_CD		= PS_DEFINE.TYPE_PICK_CASE	  AND --ケース  
						STPICK_ID		= PS_DEFINE.ST_PICKM1_CREATE  AND           
						PALLETDETNO_CD	= ' ';

					numSumCaseNR := numSumCaseNR + rowVolume.CASE_NR;
					IF numSumCaseNR = rowVolume.PALLETCAPACITY_NR THEN
						numNextSearch := 1;
						EXIT;
					END IF;
				ELSE
					--超える
					numModCaseNR := numSumCaseNR + rowVolume.CASE_NR - rowVolume.PALLETCAPACITY_NR;
					numReqCaseNR := rowVolume.CASE_NR - numModCaseNR;
					numModPcsNR  := numModCaseNR * rowVolume.PCSPERCASE_NR;
					numReqPcsNR  := numReqCaseNR * rowVolume.PCSPERCASE_NR;

					PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME ||
						'iTrnID = [' || iTrnID || '], ' ||
						'iSkuCD = [' || iSkuCD || '], ' ||
						'iLotTX = [' || iLotTX || '], ' ||
						'iTransporterCD = [' || iTransporterCD || '], ' ||
						'iDtNameTX = [' || iDtNameTX || '], ' ||	 
						'iDtAddress1TX = [' || iDtAddress1TX || '], ' ||
						'iDtAddress2TX = [' || iDtAddress2TX || '], ' ||
						'iDtAddress3TX = [' || iDtAddress3TX || '], ' ||
						'iDtAddress4TX = [' || iDtAddress4TX || '], ' ||
						'iDirectDeliveryCD = [' || iDirectDeliveryCD || '], ' ||
						'iDeliDateTX = [' || iDeliDateTX || '], ' ||
						'iTotalPicFL = [' || iTotalPicFL || '], ' ||
						'chrPalletDetNoCD = [' || chrPalletDetNoCD || '], ' ||
						'numModCaseNR = [' || numModCaseNR || '], ' ||
						'numReqCaseNR = [' || numReqCaseNR || '], ' ||
						'numModPcsNR = [' || numModPcsNR || '], ' ||
						'numReqPcsNR = [' || numReqPcsNR || ']'
					);

					PS_PICK.DevPickCase(
						iTrnID			  ,
						rowVolume.PICK_ID , --元
						iUserCD			  ,
						iUserTX			  ,
						iProgramCD		  ,
						numPickID			--残
					);

					PS_STOCK.GetUnitPcsNr(
						rowVolume.SKU_CD			,
						numReqPcsNR					, --完パレになるために必要な数量
						rowVolume.PALLETCAPACITY_NR ,
						numPalletNR					,
						numCaseNR					,
						numBaraNR
					);

					PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME ||
						'iTrnID = [' || iTrnID || '], ' ||
						'iSkuCD = [' || iSkuCD || '], ' ||
						'iLotTX = [' || iLotTX || '], ' ||
						'iTransporterCD = [' || iTransporterCD || '], ' ||
						'iDtNameTX = [' || iDtNameTX || '], ' ||	 
						'iDtAddress1TX = [' || iDtAddress1TX || '], ' ||
						'iDtAddress2TX = [' || iDtAddress2TX || '], ' ||
						'iDtAddress3TX = [' || iDtAddress3TX || '], ' ||
						'iDtAddress4TX = [' || iDtAddress4TX || '], ' ||
						'iDirectDeliveryCD = [' || iDirectDeliveryCD || '], ' ||
						'iDeliDateTX = [' || iDeliDateTX || '], ' ||
						'iTotalPicFL = [' || iTotalPicFL || '], ' ||
						'chrPalletDetNoCD = [' || chrPalletDetNoCD || '], ' ||
						'numPickID = [' || numPickID || '], ' ||
						'numPalletNR = [' || numPalletNR || '], ' ||
						'numCaseNR = [' || numCaseNR || '], ' ||
						'numBaraNR = [' || numBaraNR || ']'
					);

					--元
					UPDATE
						TA_PICK
					SET
						PALLETDETNO_CD  = chrPalletDetNoCD,
						TYPEAPPLY_CD    = PS_DEFINE.TYPE_APPLY_CASE_VOL,
						PALLET_NR		= numPalletNR	,
						CASE_NR	 		= numCaseNR	 	,
						BARA_NR	 		= numBaraNR    	,
						PCS_NR	 		= numReqPcsNR	,
						--
						UPD_DT			= SYSDATE		,
						UPDUSER_CD		= iUserCD		,
						UPDUSER_TX		= iUserTX		,
						UPDPROGRAM_CD	= iProgramCD	,
						UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
					WHERE
						TRN_ID	   		= iTrnID					  AND
						PICK_ID			= rowVolume.PICK_ID			  AND
						TYPEPICK_CD		= PS_DEFINE.TYPE_PICK_CASE	  AND --ケース  
						STPICK_ID		= PS_DEFINE.ST_PICKM1_CREATE  AND           
						PALLETDETNO_CD	= ' ';

					--元
					UPDATE
						TA_LOCASTOCKC
					SET
						PALLET_NR		= numPalletNR	,
						CASE_NR	 		= numCaseNR	 	,
						BARA_NR	 		= numBaraNR    	,
						PCS_NR	 		= numReqPcsNR	
					WHERE
						LOCASTOCKC_ID = rowVolume.SRCLOCASTOCKC_ID;

					PS_STOCK.GetUnitPcsNr(
						rowVolume.SKU_CD			,
						numModPcsNR					, --余り
						rowVolume.PALLETCAPACITY_NR ,
						numPalletNR					,
						numCaseNR					,
						numBaraNR
					);

					PS_STOCK.CopyC(
						rowVolume.SRCLOCASTOCKC_ID, --元
						numPalletNR		  ,
						numCaseNR  		  ,
						numBaraNR  		  ,
						numModPcsNR		  , --余り
						iUserCD			  ,
						iUserTX			  ,
						iProgramCD		  ,
						numLocaStockCID		--残
					);

					PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME ||
						'iTrnID = [' || iTrnID || '], ' ||
						'iSkuCD = [' || iSkuCD || '], ' ||
						'iLotTX = [' || iLotTX || '], ' ||
						'iTransporterCD = [' || iTransporterCD || '], ' ||
						'iDtNameTX = [' || iDtNameTX || '], ' ||	 
						'iDtAddress1TX = [' || iDtAddress1TX || '], ' ||
						'iDtAddress2TX = [' || iDtAddress2TX || '], ' ||
						'iDtAddress3TX = [' || iDtAddress3TX || '], ' ||
						'iDtAddress4TX = [' || iDtAddress4TX || '], ' ||
						'iDirectDeliveryCD = [' || iDirectDeliveryCD || '], ' ||
						'iDeliDateTX = [' || iDeliDateTX || '], ' ||
						'iTotalPicFL = [' || iTotalPicFL || '], ' ||
						'chrPalletDetNoCD = [' || chrPalletDetNoCD || '], ' ||
						'LOCASTOCKC_ID = [' || rowVolume.SRCLOCASTOCKC_ID || '], ' ||
						'numLocaStockCID = [' || numLocaStockCID || '], ' ||
						'numPalletNR = [' || numPalletNR || '], ' ||
						'numCaseNR = [' || numCaseNR || '], ' ||
						'numBaraNR = [' || numBaraNR || ']'
					);

					--残
					UPDATE
						TA_PICK
					SET
						PALLET_NR		 = numPalletNR	   ,
						CASE_NR			 = numCaseNR	   ,
						BARA_NR			 = numBaraNR       ,
						PCS_NR			 = numModPcsNR	   ,
						SRCLOCASTOCKC_ID = numLocaStockCID , --残
						--
						UPD_DT			 = SYSDATE		   ,
						UPDUSER_CD		 = iUserCD		   ,
						UPDUSER_TX		 = iUserTX		   ,
						UPDPROGRAM_CD	 = iProgramCD	   ,
						UPDCOUNTER_NR	 = UPDCOUNTER_NR + 1
					WHERE
						TRN_ID			 = iTrnID					  AND
						PICK_ID			 = numPickID				  AND --残
						TYPEPICK_CD		 = PS_DEFINE.TYPE_PICK_CASE	  AND --ケース  
						STPICK_ID		 = PS_DEFINE.ST_PICKM1_CREATE AND           
						PALLETDETNO_CD	 = ' ';

					--残
					UPDATE
						TA_LOCASTOCKC
					SET
						PALLET_NR		= numPalletNR	,
						CASE_NR	 		= numCaseNR	 	,
						BARA_NR	 		= numBaraNR    	,
						PCS_NR	 		= numModPcsNR	
					WHERE
						LOCASTOCKC_ID = numLocaStockCID;

					numNextSearch := 1;
					EXIT;

				END IF;

			END LOOP;
			CLOSE curVolume;

		END LOOP;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickVolumeSearchOrder;

	--商品で３５ケース以上のもの
	PROCEDURE ApplyPickCase(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickCase';

		CURSOR curCase(
			TrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				TPK.SKU_CD,
				TPK.LOT_TX,
				TPK.TRANSPORTER_CD,
				TPK.TOTALPIC_FL
			FROM
				TA_PICK			TPK,
				MA_SKU			MSK,
				MA_TRANSPORTER	MTP
			WHERE
				TPK.TRN_ID		   = TrnID						 AND
				TPK.SKU_CD		   = MSK.SKU_CD					 AND
				TPK.TRANSPORTER_CD = MTP.TRANSPORTER_CD(+) 		 AND
				TPK.TYPEPICK_CD	   = PS_DEFINE.TYPE_PICK_CASE	 AND --ケース
				TPK.STPICK_ID	   = PS_DEFINE.ST_PICKM1_CREATE  AND
				TPK.PALLETDETNO_CD = ' '						 AND
				TPK.TOTALPIC_FL    = 1								 --トータルピック対象
			GROUP BY
				TPK.SKU_CD,
				TPK.LOT_TX, --ロット別にする
				MTP.PICKORDER_NR,
				TPK.TRANSPORTER_CD, --データはTRANSPORTER2_CD
				TPK.TOTALPIC_FL,
				MSK.PCSPERCASE_NR
			HAVING
				SUM(TPK.PCS_NR) / MSK.PCSPERCASE_NR > 35
			ORDER BY
				NVL(MTP.PICKORDER_NR, 0),
				TPK.TRANSPORTER_CD,
				TPK.SKU_CD		  ,
				TPK.LOT_TX		  ;
				
		rowCase				curCase%ROWTYPE;
		chrPalletDetNoCD	TA_PICK.PALLETDETNO_CD%TYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curCase(iTrnID);
		LOOP
			FETCH curCase INTO rowCase;
			EXIT WHEN curCase%NOTFOUND;

			--付番
			PS_NO.GetPalletDetNo(chrPalletDetNoCD);

			--トータルピック
			UPDATE
				TA_PICK
			SET
				PALLETDETNO_CD = chrPalletDetNoCD,
				TYPEAPPLY_CD   = PS_DEFINE.TYPE_APPLY_CASE_NUM,
				--
				UPD_DT		   = SYSDATE		,
				UPDUSER_CD	   = iUserCD		,
				UPDUSER_TX	   = iUserTX		,
				UPDPROGRAM_CD  = iProgramCD		,
				UPDCOUNTER_NR  = UPDCOUNTER_NR + 1
			WHERE
				TRN_ID		   = iTrnID						AND
				SKU_CD		   = rowCase.SKU_CD				AND
				LOT_TX		   = rowCase.LOT_TX				AND --ロット別にする
				TRANSPORTER_CD = rowCase.TRANSPORTER_CD		AND --データはTRANSPORTER2_CD
				TOTALPIC_FL	   = rowCase.TOTALPIC_FL		AND
				TYPEPICK_CD	   = PS_DEFINE.TYPE_PICK_CASE	AND --ケース 
				STPICK_ID	   = PS_DEFINE.ST_PICKM1_CREATE AND          
				PALLETDETNO_CD = ' ';

		END LOOP;
		CLOSE curCase;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickCase;

	--混載
	PROCEDURE ApplyPickMixSku(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickMixSku';

		CURSOR curMixSku(
			TrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				TAP.SKU_CD			 ,
				TAP.TRANSPORTER_CD	 ,
				TAP.DTNAME_TX	 	 ,
				TAP.DTADDRESS1_TX	 ,
				TAP.DTADDRESS2_TX	 ,
				TAP.DTADDRESS3_TX	 ,
				TAP.DTADDRESS4_TX	 ,
				TAP.DIRECTDELIVERY_CD, 
				TAP.DELIDATE_TX		 ,       
				TAP.TOTALPIC_FL		 ,
				TAP.MAXLOADFACTOR_NR ,
				TAP.ORDER_ID		 ,
				TAP.CASE_NR			 , --ケース数
				TAP.WEIGHT_NR		 , --総重量  
				TAP.VOLUME_NR		 , --総体積  
				TAP.CAPACITY_NR		   --積載率  
			FROM
				TMP_APPLYPICK 	TAP,
				MA_TRANSPORTER	MTP
			WHERE
				TAP.TRN_ID 		   = TrnID AND
				TAP.TRANSPORTER_CD = MTP.TRANSPORTER_CD(+)
			ORDER BY
				NVL(MTP.PICKORDER_NR, 0),
				TAP.TRANSPORTER_CD		, --配送業者別
				TAP.ORDER_ID			, --重量グループが重い順
				TAP.WEIGHT_NR DESC		, --総重量が重い順
				TAP.SKU_CD				;
				
		rowMixSku			curMixSku%ROWTYPE;

		chrPalletDetNoCD	TA_PICK.PALLETDETNO_CD%TYPE;
		numNextSearch		NUMBER := 1;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		WHILE ( numNextSearch = 1 ) LOOP

			ApplyPickMixInit(iTrnID);
			numNextSearch := 0;

			OPEN curMixSku(iTrnID);
			LOOP
				FETCH curMixSku INTO rowMixSku;
				EXIT WHEN curMixSku%NOTFOUND;

				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME ||
					'SKU_CD	= [' || rowMixSku.SKU_CD || '], ' ||
					'TRANSPORTER_CD = [' || rowMixSku.TRANSPORTER_CD || '], ' ||
					'DTNAME_TX = [' || rowMixSku.DTNAME_TX || '], ' ||
					'DTADDRESS1_TX = [' || rowMixSku.DTADDRESS1_TX || '], ' ||
					'DTADDRESS2_TX = [' || rowMixSku.DTADDRESS2_TX || '], ' ||
					'DTADDRESS3_TX = [' || rowMixSku.DTADDRESS3_TX || '], ' ||
					'DTADDRESS4_TX = [' || rowMixSku.DTADDRESS4_TX || '], ' ||
					'DIRECTDELIVERY_CD = [' || rowMixSku.DIRECTDELIVERY_CD || '], ' ||
					'DELIDATE_TX = [' || rowMixSku.DELIDATE_TX || '], ' ||
					'TOTALPIC_FL = [' || rowMixSku.TOTALPIC_FL || '], ' ||
					'MAXLOADFACTOR_NR = [' || rowMixSku.MAXLOADFACTOR_NR  || '], ' ||
					'ORDER_ID = [' || rowMixSku.ORDER_ID || '], ' ||
					'CASE_NR = [' || rowMixSku.CASE_NR || '], ' ||
					'WEIGHT_NR = [' || rowMixSku.WEIGHT_NR || '], ' ||
					'VOLUME_NR = [' || rowMixSku.VOLUME_NR || '], ' ||
					'CAPACITY_NR = [' || rowMixSku.CAPACITY_NR || ']');

				numNextSearch := 1;

				PS_NO.GetPalletDetNo(chrPalletDetNoCD);

				IF rowMixSku.TOTALPIC_FL = 1 THEN

					--トータルピック

					UPDATE
						TA_PICK
					SET
						PALLETDETNO_CD = chrPalletDetNoCD,
						TYPEAPPLY_CD   = PS_DEFINE.TYPE_APPLY_SKU_MIX,
						--
						UPD_DT		   = SYSDATE		,
						UPDUSER_CD	   = iUserCD		,
						UPDUSER_TX	   = iUserTX		,
						UPDPROGRAM_CD  = iProgramCD		,
						UPDCOUNTER_NR  = UPDCOUNTER_NR + 1
					WHERE
						TRN_ID			   = iTrnID						 AND
						SKU_CD			   = rowMixSku.SKU_CD			 AND
						TRANSPORTER_CD	   = rowMixSku.TRANSPORTER_CD	 AND --データはTRANSPORTER2_CD
						TOTALPIC_FL		   = rowMixSku.TOTALPIC_FL		 AND
						TYPEPICK_CD		   = PS_DEFINE.TYPE_PICK_CASE	 AND --ケース 
						STPICK_ID		   = PS_DEFINE.ST_PICKM1_CREATE  AND          
						PALLETDETNO_CD	   = ' ';

					IF rowMixSku.CAPACITY_NR <= rowMixSku.MAXLOADFACTOR_NR THEN

						DELETE
							TMP_APPLYPICKSKU
						WHERE
							TRN_ID = iTrnID;

						ApplyPickSkuAdd(iTrnID, rowMixSku.SKU_CD, rowMixSku.CASE_NR);

						ApplyPickMixSearchTotal(
							iTrnID					,
							chrPalletDetNoCD		,
							rowMixSku.SKU_CD		,
							rowMixSku.TRANSPORTER_CD,
							rowMixSku.TOTALPIC_FL	,
							rowMixSku.ORDER_ID		,
							rowMixSku.CAPACITY_NR	,
							iUserCD					,
							iUserTX					,
							iProgramCD
						);

						EXIT;

					END IF;

				ELSE

					--軒先ピック

					UPDATE
						TA_PICK
					SET
						PALLETDETNO_CD = chrPalletDetNoCD,
						TYPEAPPLY_CD   = PS_DEFINE.TYPE_APPLY_SKU_MIX,
						--
						UPD_DT		   = SYSDATE		,
						UPDUSER_CD	   = iUserCD		,
						UPDUSER_TX	   = iUserTX		,
						UPDPROGRAM_CD  = iProgramCD		,
						UPDCOUNTER_NR  = UPDCOUNTER_NR + 1
					WHERE
						TRN_ID			   = iTrnID						 AND
						SKU_CD			   = rowMixSku.SKU_CD			 AND
						TRANSPORTER_CD	   = rowMixSku.TRANSPORTER_CD	 AND --データはTRANSPORTER2_CD
						DTNAME_TX	 	   = rowMixSku.DTNAME_TX	 	 AND
						DTADDRESS1_TX	   = rowMixSku.DTADDRESS1_TX	 AND
						DTADDRESS2_TX	   = rowMixSku.DTADDRESS2_TX	 AND
						DTADDRESS3_TX	   = rowMixSku.DTADDRESS3_TX	 AND
						DTADDRESS4_TX	   = rowMixSku.DTADDRESS4_TX	 AND
						DIRECTDELIVERY_CD  = rowMixSku.DIRECTDELIVERY_CD AND
						DELIDATE_TX		   = rowMixSku.DELIDATE_TX		 AND
						TOTALPIC_FL		   = rowMixSku.TOTALPIC_FL		 AND
						TYPEPICK_CD		   = PS_DEFINE.TYPE_PICK_CASE	 AND --ケース 
						STPICK_ID		   = PS_DEFINE.ST_PICKM1_CREATE  AND          
						PALLETDETNO_CD	   = ' ';

					IF rowMixSku.CAPACITY_NR <= rowMixSku.MAXLOADFACTOR_NR THEN

--						DELETE
--							TMP_APPLYPICKSKU
--						WHERE
--							TRN_ID = iTrnID;

--						ApplyPickSkuAdd(iTrnID, rowMixSku.SKU_CD, rowMixSku.CASE_NR);

						ApplyPickMixSearchOrder(
							iTrnID						,
							chrPalletDetNoCD			,
							rowMixSku.SKU_CD			,
							rowMixSku.TRANSPORTER_CD	,
							rowMixSku.DTNAME_TX			,
							rowMixSku.DTADDRESS1_TX		,
							rowMixSku.DTADDRESS2_TX		,
							rowMixSku.DTADDRESS3_TX		,
							rowMixSku.DTADDRESS4_TX		,
							rowMixSku.DIRECTDELIVERY_CD	, --直送先コー 
							rowMixSku.DELIDATE_TX		, --着荷指定日  
							rowMixSku.TOTALPIC_FL		,
							rowMixSku.ORDER_ID			,
							rowMixSku.CAPACITY_NR		,
							iUserCD						,
							iUserTX						,
							iProgramCD
						);

						EXIT;

					END IF;

				END IF;

			END LOOP;
			CLOSE curMixSku;

		END LOOP;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickMixSku;

	PROCEDURE ApplyPickMixInit(
		iTrnID			IN	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickMixInit';

		CURSOR curMixSku(
			TrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				TMP.SKU_CD						 ,
				TMP.TRANSPORTER_CD	 			 ,
				' ' AS DTNAME_TX	   			 ,
				' ' AS DTADDRESS1_TX			 ,
				' ' AS DTADDRESS2_TX			 ,
				' ' AS DTADDRESS3_TX			 ,
				' ' AS DTADDRESS4_TX			 ,
				' ' AS DIRECTDELIVERY_CD		 ,
				' ' AS DELIDATE_TX				 ,      
				TMP.TOTALPIC_FL					 ,
				TMP.MAXLOADFACTOR_NR			 ,  --最大積載率    
				TMP.ORDER_ID					 ,  --重量グループ
				SUM(TMP.CASE_NR) 	 AS CASE_NR	 ,	--ケース数
				SUM(TMP.WEIGHT_NR)   AS WEIGHT_NR,	--総重量
				SUM(TMP.VOLUME_NR) 	 AS VOLUME_NR,	--総体積
				SUM(TMP.CAPACITY_NR) AS CAPACITY_NR --積載率
			FROM
				(SELECT
					TPK.SKU_CD			 ,
					TPK.TRANSPORTER_CD	 ,
					TPK.TOTALPIC_FL		 ,
					TPK.TYPEPLALLET_CD	 ,
					TPK.PALLETCAPACITY_NR,
					MTP.MAXLOADFACTOR_NR ,
					NVL((SELECT MGW.ORDER_ID FROM MB_GROUPWEIGHT MGW 
						WHERE MSK.WEIGHT_NR >= MGW.MINWEIGHT_NR AND MSK.WEIGHT_NR < MGW.MAXWEIGHT_NR AND MGW.TOTALPIC_FL = 1), 0) AS ORDER_ID, --単位はｇ
					TPK.PCS_NR / MSK.PCSPERCASE_NR AS CASE_NR, --ケース数
					TPK.PCS_NR / MSK.PCSPERCASE_NR * MSK.WEIGHT_NR AS WEIGHT_NR, --総重量
					TPK.PCS_NR / MSK.PCSPERCASE_NR * MSK.LENGTH_NR * MSK.WIDTH_NR * MSK.HEIGHT_NR AS VOLUME_NR, --総体積
					TPK.PCS_NR / MSK.PCSPERCASE_NR / TPK.PALLETCAPACITY_NR AS CAPACITY_NR --積載率
				FROM
					TA_PICK			TPK,
					MA_SKU			MSK,
					MB_TYPEPALLET	MTP
				WHERE
					TPK.TRN_ID			= TrnID					 	 AND
					TPK.SKU_CD			= MSK.SKU_CD				 AND
					TPK.TYPEPLALLET_CD 	= MTP.TYPEPALLET_CD(+)		 AND --(+)は現在上位に不具合あるため暫定
					TPK.TYPEPICK_CD		= PS_DEFINE.TYPE_PICK_CASE	 AND --ケース
					TPK.STPICK_ID		= PS_DEFINE.ST_PICKM1_CREATE AND
					TPK.PALLETDETNO_CD  = ' '						 AND
					TPK.TOTALPIC_FL 	= 1								 --トータルピック対象
				) TMP
			GROUP BY
				TMP.SKU_CD			 ,
				TMP.TRANSPORTER_CD	 ,
				TMP.TOTALPIC_FL		 ,
				TMP.MAXLOADFACTOR_NR ,
				TMP.ORDER_ID
			UNION ALL
			SELECT
				TMP.SKU_CD						 ,
				TMP.TRANSPORTER_CD	 			 ,
				TMP.DTNAME_TX					 ,
				TMP.DTADDRESS1_TX				 ,
				TMP.DTADDRESS2_TX				 ,
				TMP.DTADDRESS3_TX				 ,
				TMP.DTADDRESS4_TX				 ,
				TMP.DIRECTDELIVERY_CD			 ,	--直送先コード  
				TMP.DELIDATE_TX					 ,	--着荷指定日    
				TMP.TOTALPIC_FL		 			 ,
				TMP.MAXLOADFACTOR_NR			 ,  --最大積載率    
				TMP.ORDER_ID					 ,  --重量グループ
				SUM(TMP.CASE_NR) 	 AS CASE_NR	 ,	--ケース数
				SUM(TMP.WEIGHT_NR)   AS WEIGHT_NR,	--総重量
				SUM(TMP.VOLUME_NR) 	 AS VOLUME_NR,	--総体積
				SUM(TMP.CAPACITY_NR) AS CAPACITY_NR --積載率
			FROM
				(SELECT
					TPK.SKU_CD			 ,
					TPK.TRANSPORTER_CD	 ,
					TPK.DTNAME_TX	 	 ,
					TPK.DTADDRESS1_TX	 ,
					TPK.DTADDRESS2_TX	 ,
					TPK.DTADDRESS3_TX	 ,
					TPK.DTADDRESS4_TX	 ,
					TPK.DIRECTDELIVERY_CD,
					TPK.DELIDATE_TX		 ,
					TPK.TOTALPIC_FL		 ,
					TPK.TYPEPLALLET_CD	 ,
					TPK.PALLETCAPACITY_NR,
					MTP.MAXLOADFACTOR_NR ,
					NVL((SELECT MGW.ORDER_ID FROM MB_GROUPWEIGHT MGW 
						WHERE MSK.WEIGHT_NR >= MGW.MINWEIGHT_NR AND MSK.WEIGHT_NR < MGW.MAXWEIGHT_NR AND MGW.TOTALPIC_FL = 1), 0) AS ORDER_ID, --単位はｇ
					TPK.PCS_NR / MSK.PCSPERCASE_NR AS CASE_NR, --ケース数
					TPK.PCS_NR / MSK.PCSPERCASE_NR * MSK.WEIGHT_NR AS WEIGHT_NR, --総重量
					TPK.PCS_NR / MSK.PCSPERCASE_NR * MSK.LENGTH_NR * MSK.WIDTH_NR * MSK.HEIGHT_NR AS VOLUME_NR, --総体積
					TPK.PCS_NR / MSK.PCSPERCASE_NR / TPK.PALLETCAPACITY_NR AS CAPACITY_NR --積載率
				FROM
					TA_PICK			TPK,
					MA_SKU			MSK,
					MB_TYPEPALLET	MTP
				WHERE
					TPK.TRN_ID			= TrnID					 	 AND
					TPK.SKU_CD			= MSK.SKU_CD				 AND
					TPK.TYPEPLALLET_CD 	= MTP.TYPEPALLET_CD(+)		 AND --(+)は現在上位に不具合あるため暫定
					TPK.TYPEPICK_CD		= PS_DEFINE.TYPE_PICK_CASE	 AND --ケース
					TPK.STPICK_ID		= PS_DEFINE.ST_PICKM1_CREATE AND
					TPK.PALLETDETNO_CD  = ' '						 AND
					TPK.TOTALPIC_FL		= 0								 --軒先ピック対象
				) TMP
			GROUP BY
				TMP.SKU_CD			 ,
				TMP.TRANSPORTER_CD	 ,
				TMP.DTNAME_TX		 ,
				TMP.DTADDRESS1_TX	 ,
				TMP.DTADDRESS2_TX	 ,
				TMP.DTADDRESS3_TX	 ,
				TMP.DTADDRESS4_TX	 ,
				TMP.DIRECTDELIVERY_CD,
				TMP.DELIDATE_TX		 ,
				TMP.TOTALPIC_FL		 ,
				TMP.MAXLOADFACTOR_NR ,
				TMP.ORDER_ID
			ORDER BY
				TRANSPORTER_CD		 , --配送業者別
				ORDER_ID			 , --重量グループが重い順
				WEIGHT_NR DESC	 	 , --総重量が重い順
				SKU_CD				 ;
				
		rowMixSku			curMixSku%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		DELETE
			TMP_APPLYPICK
		WHERE
			TRN_ID = iTrnID;

		OPEN curMixSku(iTrnID);
		LOOP
			FETCH curMixSku INTO rowMixSku;
			EXIT WHEN curMixSku%NOTFOUND;

			INSERT INTO TMP_APPLYPICK
			(
				TRN_ID			 ,
				ADD_DT			 ,
				SKU_CD			 ,
				TRANSPORTER_CD	 ,
				DTNAME_TX	 	 ,
				DTADDRESS1_TX	 ,
				DTADDRESS2_TX	 ,
				DTADDRESS3_TX	 ,
				DTADDRESS4_TX	 ,
				DIRECTDELIVERY_CD,
				DELIDATE_TX      ,
				TOTALPIC_FL		 ,
				MAXLOADFACTOR_NR ,
				ORDER_ID		 ,
				CASE_NR			 ,
				WEIGHT_NR		 ,
				VOLUME_NR		 ,
				CAPACITY_NR		
			)
			VALUES
			(
				iTrnID						,
				SYSDATE						,
				rowMixSku.SKU_CD			, 
				rowMixSku.TRANSPORTER_CD	, 
				rowMixSku.DTNAME_TX			,
				rowMixSku.DTADDRESS1_TX		,
				rowMixSku.DTADDRESS2_TX		,
				rowMixSku.DTADDRESS3_TX		,
				rowMixSku.DTADDRESS4_TX		,
				rowMixSku.DIRECTDELIVERY_CD	, --直送先コード  
				rowMixSku.DELIDATE_TX      	, --着荷指定日    
				rowMixSku.TOTALPIC_FL		, --トータルピックフラグ
				rowMixSku.MAXLOADFACTOR_NR	,
				rowMixSku.ORDER_ID			, 
				rowMixSku.CASE_NR	  		, 
				rowMixSku.WEIGHT_NR 		, 
				rowMixSku.VOLUME_NR 		, 
				rowMixSku.CAPACITY_NR		  
			);

		END LOOP;
		CLOSE curMixSku;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickMixInit;

	PROCEDURE ApplyPickMixSearchTotal(
		iTrnID				IN	NUMBER	,
		iPalletDetNoCD		IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iTransporterCD		IN	VARCHAR2,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iOrderID			IN	NUMBER	, --重量グループ別に付番
		iCapacityNR			IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickMixSearchTotal';

		CURSOR curMixSku(
			TrnID			TA_PICK.TRN_ID%TYPE,
			SkuCD			TA_PICK.SKU_CD%TYPE,
			TransporterCD	TA_PICK.TRANSPORTER_CD%TYPE,
			TotalPicFL		TA_PICK.TOTALPIC_FL%TYPE,
			OrderID			TMP_APPLYPICK.ORDER_ID%TYPE
		) IS
			SELECT
				SKU_CD			,
				TRANSPORTER_CD	,
				TOTALPIC_FL		,
				MAXLOADFACTOR_NR,
				ORDER_ID		,
				CASE_NR			, --ケース数
				WEIGHT_NR		, --総重量  
				VOLUME_NR		, --総体積  
				CAPACITY_NR 	  --積載率  
			FROM
				TMP_APPLYPICK	
			WHERE
				TRN_ID 		   = TrnID		   AND
				SKU_CD		   <> SkuCD		   AND
				TRANSPORTER_CD = TransporterCD AND
				TOTALPIC_FL	   = TotalPicFL	   AND
				ORDER_ID	   = OrderID
			ORDER BY
				TRANSPORTER_CD  , --配送業者別
				ORDER_ID  	   	, --重量グループが重い順
				WEIGHT_NR 		DESC, --総重量が重い順
				SKU_CD		    ;

		rowMixSku			curMixSku%ROWTYPE;

		numCapacityNR		NUMBER(15, 5); --積載率合計
		numSkuNR			NUMBER;		   --ＳＫＵ数合計
		numFoundFL			NUMBER;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		numCapacityNR := iCapacityNR;
		numSkuNR	  := 0;

		OPEN curMixSku(iTrnID, iSkuCD, iTransporterCD, iTotalPicFL, iOrderID);
		LOOP
			FETCH curMixSku INTO rowMixSku;
			EXIT WHEN curMixSku%NOTFOUND;

			ApplyPickSkuCnt(iTrnID, rowMixSku.SKU_CD, numSkuNR);

			PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
				'iTrnID = [' || iTrnID || '], ' ||
				'SKU_CD = [' || rowMixSku.SKU_CD || '], ' ||
				'numSkuNR = [' || numSkuNR || '], ' ||
				'ORDER_ID = [' || rowMixSku.ORDER_ID || '], ' ||
				'CASE_NR = [' || rowMixSku.CASE_NR || '], ' ||
				'numCapacityNR = [' || numCapacityNR || '], ' ||
				'CAPACITY_NR = [' || rowMixSku.CAPACITY_NR || '], ' ||
				'MAXLOADFACTOR_NR = [' || rowMixSku.MAXLOADFACTOR_NR || '], ' ||
				'iPalletDetNoCD = [' || iPalletDetNoCD || '], ' ||
				'iTransporterCD = [' || iTransporterCD || '], ' ||
				'iTotalPicFL = [' || iTotalPicFL || ']');

			IF rowMixSku.CASE_NR >= 5 THEN
				IF numCapacityNR + rowMixSku.CAPACITY_NR <= rowMixSku.MAXLOADFACTOR_NR AND numSkuNR + 1 <= 3 THEN --３以下に変更
					numFoundFL := 1;
				ELSE
					numFoundFL := 0;
				END IF;
			ELSE
				IF numCapacityNR + rowMixSku.CAPACITY_NR <= rowMixSku.MAXLOADFACTOR_NR AND numSkuNR <= 3 THEN --３以下に変更
					numFoundFL := 1;
				ELSE
					numFoundFL := 0;
				END IF;
			END IF;

			IF numFoundFL = 1 THEN

				UPDATE
					TA_PICK
				SET
					PALLETDETNO_CD	   = iPalletDetNoCD,
					TYPEAPPLY_CD	   = PS_DEFINE.TYPE_APPLY_SKU_MIX,
					--
					UPD_DT			   = SYSDATE		,
					UPDUSER_CD		   = iUserCD		,
					UPDUSER_TX		   = iUserTX		,
					UPDPROGRAM_CD	   = iProgramCD		,
					UPDCOUNTER_NR	   = UPDCOUNTER_NR + 1
				WHERE
					TRN_ID			   = iTrnID						 AND
					SKU_CD			   = rowMixSku.SKU_CD			 AND
					TRANSPORTER_CD	   = rowMixSku.TRANSPORTER_CD	 AND --データはTRANSPORTER2_CD
					TOTALPIC_FL		   = rowMixSku.TOTALPIC_FL		 AND
					TYPEPICK_CD		   = PS_DEFINE.TYPE_PICK_CASE	 AND --ケース 
					STPICK_ID		   = PS_DEFINE.ST_PICKM1_CREATE  AND          
					PALLETDETNO_CD	   = ' ';

				ApplyPickSkuAdd(iTrnID, rowMixSku.SKU_CD, rowMixSku.CASE_NR);
				numCapacityNR := numCapacityNR + rowMixSku.CAPACITY_NR;

			END IF;

		END LOOP;
		CLOSE curMixSku;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickMixSearchTotal;

	PROCEDURE ApplyPickMixSearchOrder(
		iTrnID				IN	NUMBER	,
		iPalletDetNoCD		IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iTransporterCD		IN	VARCHAR2,
		iDtNameTX			IN	VARCHAR2,    
		iDtAddress1TX		IN	VARCHAR2,
		iDtAddress2TX		IN	VARCHAR2,
		iDtAddress3TX		IN	VARCHAR2,
		iDtAddress4TX		IN	VARCHAR2,
		iDirectDeliveryCD 	IN	VARCHAR2, --直送先コード
		iDeliDateTX			IN	VARCHAR2, --着荷指定日
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iOrderID			IN	NUMBER	, --重量グループ別に付番
		iCapacityNR			IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickMixSearchOrder';

		CURSOR curMixSku(
			TrnID				TA_PICK.TRN_ID%TYPE,
			SkuCD				TA_PICK.SKU_CD%TYPE,
			TransporterCD		TA_PICK.TRANSPORTER_CD%TYPE,
			DtNameTX	 		TA_PICK.DTNAME_TX%TYPE,	 
			DtAddress1TX		TA_PICK.DTADDRESS1_TX%TYPE,
			DtAddress2TX		TA_PICK.DTADDRESS2_TX%TYPE,
			DtAddress3TX		TA_PICK.DTADDRESS3_TX%TYPE,
			DtAddress4TX		TA_PICK.DTADDRESS4_TX%TYPE,
			DirectDeliveryCD	TA_PICK.DIRECTDELIVERY_CD%TYPE,
			DeliDateTX		 	TA_PICK.DELIDATE_TX%TYPE,
			TotalPicFL			TA_PICK.TOTALPIC_FL%TYPE,
			OrderID				TMP_APPLYPICK.ORDER_ID%TYPE
		) IS
			SELECT
				SKU_CD			 ,
				TRANSPORTER_CD	 ,
				DTNAME_TX	 	 ,
				DTADDRESS1_TX	 ,
				DTADDRESS2_TX	 ,
				DTADDRESS3_TX	 ,
				DTADDRESS4_TX	 ,
				DIRECTDELIVERY_CD,
				DELIDATE_TX	  	 ,
				TOTALPIC_FL		 ,
				MAXLOADFACTOR_NR ,
				ORDER_ID		 ,
				CASE_NR			 , --ケース数
				WEIGHT_NR		 , --総重量  
				VOLUME_NR		 , --総体積  
				CAPACITY_NR		   --積載率  
			FROM
				TMP_APPLYPICK	
			WHERE
				TRN_ID			  = TrnID			 AND
				SKU_CD			  <> SkuCD			 AND
				TRANSPORTER_CD	  = TransporterCD 	 AND
				DTNAME_TX	 	  = DtNameTX		 AND
				DTADDRESS1_TX	  = DtAddress1TX	 AND
				DTADDRESS2_TX	  = DtAddress2TX	 AND
				DTADDRESS3_TX	  = DtAddress3TX	 AND
				DTADDRESS4_TX	  = DtAddress4TX	 AND
				DIRECTDELIVERY_CD = DirectDeliveryCD AND
				DELIDATE_TX	  	  = DeliDateTX		 AND
				TOTALPIC_FL	   	  = TotalPicFL	   	 AND
				ORDER_ID	   	  = OrderID
			ORDER BY
				TRANSPORTER_CD  , --配送業者別
				ORDER_ID  	   	, --重量グループが重い順
				WEIGHT_NR 		DESC, --総重量が重い順
				SKU_CD		    ;

		rowMixSku			curMixSku%ROWTYPE;

		numCapacityNR		NUMBER(15, 5); --積載率合計
		--numSkuNR			NUMBER;		   --ＳＫＵ数合計
		numFoundFL			NUMBER;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		numCapacityNR := iCapacityNR;
		--numSkuNR	  := 0;

		OPEN curMixSku(iTrnID, iSkuCD, iTransporterCD, iDtNameTX, iDtAddress1TX, iDtAddress2TX, 
						iDtAddress3TX, iDtAddress4TX, iDirectDeliveryCD, iDeliDateTX, iTotalPicFL, iOrderID);
		LOOP
			FETCH curMixSku INTO rowMixSku;
			EXIT WHEN curMixSku%NOTFOUND;

			--ApplyPickSkuCnt(iTrnID, rowMixSku.SKU_CD, numSkuNR);

			PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
				'iTrnID = [' || iTrnID || '], ' ||
				'SKU_CD = [' || rowMixSku.SKU_CD || '], ' ||
				--'numSkuNR = [' || numSkuNR || '], ' ||
				'ORDER_ID = [' || rowMixSku.ORDER_ID || '], ' ||
				'CASE_NR = [' || rowMixSku.CASE_NR || '], ' ||
				'numCapacityNR = [' || numCapacityNR || '], ' ||
				'CAPACITY_NR = [' || rowMixSku.CAPACITY_NR || '], ' ||
				'MAXLOADFACTOR_NR = [' || rowMixSku.MAXLOADFACTOR_NR || '], ' ||
				'iPalletDetNoCD = [' || iPalletDetNoCD || '], ' ||
				'iTransporterCD = [' || iTransporterCD || '], ' ||
				'iDtNameTX = [' || iDtNameTX || '], ' ||	 
				'iDtAddress1TX = [' || iDtAddress1TX || '], ' ||
				'iDtAddress2TX = [' || iDtAddress2TX || '], ' ||
				'iDtAddress3TX = [' || iDtAddress3TX || '], ' ||
				'iDtAddress4TX = [' || iDtAddress4TX || '], ' ||
				'iDirectDeliveryCD = [' || iDirectDeliveryCD || '], ' ||
				'iDeliDateTX = [' || iDeliDateTX || '], ' ||		 
				'iTotalPicFL = [' || iTotalPicFL || ']');

			IF numCapacityNR + rowMixSku.CAPACITY_NR <= rowMixSku.MAXLOADFACTOR_NR THEN
				numFoundFL := 1;
			ELSE
				numFoundFL := 0;
			END IF;

--			IF rowMixSku.CASE_NR >= 5 THEN
--				IF numCapacityNR + rowMixSku.CAPACITY_NR <= rowMixSku.MAXLOADFACTOR_NR AND numSkuNR + 1 <= 3 THEN --３以下に変更
--					numFoundFL := 1;
--				ELSE
--					numFoundFL := 0;
--				END IF;
--			ELSE
--				IF numCapacityNR + rowMixSku.CAPACITY_NR <= rowMixSku.MAXLOADFACTOR_NR AND numSkuNR <= 3 THEN --３以下に変更
--					numFoundFL := 1;
--				ELSE
--					numFoundFL := 0;
--				END IF;
--			END IF;

			IF numFoundFL = 1 THEN

				UPDATE
					TA_PICK
				SET
					PALLETDETNO_CD	   = iPalletDetNoCD,
					TYPEAPPLY_CD	   = PS_DEFINE.TYPE_APPLY_SKU_MIX,
					--
					UPD_DT			   = SYSDATE		,
					UPDUSER_CD		   = iUserCD		,
					UPDUSER_TX		   = iUserTX		,
					UPDPROGRAM_CD	   = iProgramCD		,
					UPDCOUNTER_NR	   = UPDCOUNTER_NR + 1
				WHERE
					TRN_ID			   = iTrnID						 AND
					SKU_CD			   = rowMixSku.SKU_CD			 AND
					TRANSPORTER_CD	   = rowMixSku.TRANSPORTER_CD	 AND --データはTRANSPORTER2_CD
					DTNAME_TX	 	   = rowMixSku.DTNAME_TX	 	 AND
					DTADDRESS1_TX	   = rowMixSku.DTADDRESS1_TX	 AND
					DTADDRESS2_TX	   = rowMixSku.DTADDRESS2_TX	 AND
					DTADDRESS3_TX	   = rowMixSku.DTADDRESS3_TX	 AND
					DTADDRESS4_TX	   = rowMixSku.DTADDRESS4_TX	 AND
					DIRECTDELIVERY_CD  = rowMixSku.DIRECTDELIVERY_CD AND
					DELIDATE_TX		   = rowMixSku.DELIDATE_TX		 AND
					TOTALPIC_FL		   = rowMixSku.TOTALPIC_FL		 AND
					TYPEPICK_CD		   = PS_DEFINE.TYPE_PICK_CASE	 AND --ケース 
					STPICK_ID		   = PS_DEFINE.ST_PICKM1_CREATE  AND          
					PALLETDETNO_CD	   = ' ';

				--ApplyPickSkuAdd(iTrnID, rowMixSku.SKU_CD, rowMixSku.CASE_NR);
				numCapacityNR := numCapacityNR + rowMixSku.CAPACITY_NR;

			END IF;

		END LOOP;
		CLOSE curMixSku;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickMixSearchOrder;

	PROCEDURE ApplyPickSkuAdd(
		iTrnID				IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iCaseNR				IN	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickSkuAdd';

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		INSERT INTO TMP_APPLYPICKSKU
		(
			TRN_ID,
			ADD_DT,
			SKU_CD,
			CASE_NR
		)
		VALUES
		(
			iTrnID,
			SYSDATE,
			iSkuCD,
			iCaseNR
		);

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickSkuAdd;

	PROCEDURE ApplyPickSkuCnt(
		iTrnID			IN	NUMBER,
		iSkuCD			IN	VARCHAR2,
		oSkuCnt			OUT	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyPickSkuCnt';

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'iSkuCD = [' || iSkuCD || ']');

		--20140515 サブクエリで集計後、カウントするよう修正
		SELECT
			NVL(COUNT(DISTINCT TMP.SKU_CD), 0) INTO oSkuCnt
		FROM
			(SELECT
				SKU_CD
			FROM
				TMP_APPLYPICKSKU
			WHERE
				TRN_ID = iTrnID AND
				SKU_CD <> iSkuCD
			GROUP BY
				SKU_CD
			HAVING
				SUM(CASE_NR) >= 5) TMP;
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			oSkuCnt := 0;
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyPickSkuCnt;

	PROCEDURE ConvCaseToPallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ConvCaseToPallet';

		CURSOR curPalletN(
			TrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				PALLETDETNO_CD
			FROM
				TA_PICK
			WHERE
				TRN_ID 		= TrnID					 	 AND
				TYPEPICK_CD = PS_DEFINE.TYPE_PICK_CASE	 AND --ケース  
				STPICK_ID	= PS_DEFINE.ST_PICKM1_CREATE AND
				PALLET_NR 	= 1								 --完パレ
			ORDER BY
				PALLETDETNO_CD;

		CURSOR curPalletS(
			TrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				PALLETDETNO_CD
			FROM
				TA_PICK
			WHERE
				TRN_ID 		= TrnID					 	 AND
				TYPEPICK_CD = PS_DEFINE.TYPE_PICK_CASE	 AND --ケース  
				STPICK_ID	= PS_DEFINE.ST_PICKM1_CREATE AND
				TOTALPIC_FL	= 1
			GROUP BY 
				SKU_CD			 , --同一SKU
				LOT_TX			 , --同一ロット
				SRCLOCA_CD		 , --同一ロケ
				PALLETDETNO_CD	 , 
				PALLETCAPACITY_NR,
				TRANSPORTER_CD	   --同一配送業者
			HAVING
				PALLETCAPACITY_NR = SUM(CASE_NR)			 --集約完パレ
			UNION ALL
			SELECT
				PALLETDETNO_CD
			FROM
				TA_PICK
			WHERE
				TRN_ID 		= TrnID					 	 AND
				TYPEPICK_CD = PS_DEFINE.TYPE_PICK_CASE	 AND --ケース  
				STPICK_ID	= PS_DEFINE.ST_PICKM1_CREATE AND
				TOTALPIC_FL	= 0
			GROUP BY 
				SKU_CD			 , --同一SKU
				LOT_TX			 , --同一ロット
				SRCLOCA_CD		 , --同一ロケ
				PALLETDETNO_CD	 , 
				PALLETCAPACITY_NR,
				TRANSPORTER_CD	 , --同一配送業者
				DTNAME_TX		 ,     
				DTADDRESS1_TX	 , 
				DTADDRESS2_TX	 , 
				DTADDRESS3_TX	 , 
				DTADDRESS4_TX	 , 
				DIRECTDELIVERY_CD,
				DELIDATE_TX		 
			HAVING
				PALLETCAPACITY_NR = SUM(CASE_NR)			 --集約完パレ
			ORDER BY
				PALLETDETNO_CD;

		rowPalletN		curPalletN%ROWTYPE;
		rowPalletS		curPalletS%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curPalletN(iTrnID);
		LOOP
			FETCH curPalletN INTO rowPalletN;
			EXIT WHEN curPalletN%NOTFOUND;

			PLOG.DEBUG(logCtx, 'curPalletN ' || 'PALLETDETNO_CD = [' || rowPalletN.PALLETDETNO_CD || ']');

			UPDATE
				TA_PICK
			SET
				TYPEPICK_CD    = PS_DEFINE.TYPE_PICK_PALLET_N,
				--
				UPD_DT		   = SYSDATE		,
				UPDUSER_CD	   = iUserCD		,
				UPDUSER_TX	   = iUserTX		,
				UPDPROGRAM_CD  = iProgramCD		,
				UPDCOUNTER_NR  = UPDCOUNTER_NR + 1
			WHERE
				TRN_ID		   = iTrnID						AND
				TYPEPICK_CD    = PS_DEFINE.TYPE_PICK_CASE	AND --ケース  
				STPICK_ID	   = PS_DEFINE.ST_PICKM1_CREATE AND          
				PALLETDETNO_CD = rowPalletN.PALLETDETNO_CD;
				
		END LOOP;
		CLOSE curPalletN;

		OPEN curPalletS(iTrnID);
		LOOP
			FETCH curPalletS INTO rowPalletS;
			EXIT WHEN curPalletS%NOTFOUND;

			PLOG.DEBUG(logCtx, 'curPalletS ' || 'PALLETDETNO_CD = [' || rowPalletS.PALLETDETNO_CD || ']');

			UPDATE
				TA_PICK
			SET
				TYPEPICK_CD    = PS_DEFINE.TYPE_PICK_PALLET_S,
				--
				UPD_DT		   = SYSDATE		,
				UPDUSER_CD	   = iUserCD		,
				UPDUSER_TX	   = iUserTX		,
				UPDPROGRAM_CD  = iProgramCD		,
				UPDCOUNTER_NR  = UPDCOUNTER_NR + 1
			WHERE
				TRN_ID		   = iTrnID						AND
				TYPEPICK_CD    = PS_DEFINE.TYPE_PICK_CASE	AND --ケース  
				STPICK_ID	   = PS_DEFINE.ST_PICKM1_CREATE AND          
				PALLETDETNO_CD = rowPalletS.PALLETDETNO_CD;
				
		END LOOP;
		CLOSE curPalletS;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ConvCaseToPallet;

	PROCEDURE SetPickNo(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SetPickNo';

		CURSOR curPick(
			iTrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				TPK.TRANSPORTER_CD,
				NVL(MTP.BERTHTIME_TX, ' ') BERTHTIME_TX,
				NVL(MTP.SHIPBERTH_CD, ' ') SHIPBERTH_CD
			FROM
				TA_PICK 	   TPK,
				MA_TRANSPORTER MTP
			WHERE
				TPK.TRN_ID = iTrnID					 	   AND
				TPK.TRANSPORTER_CD = MTP.TRANSPORTER_CD(+) AND
				TPK.STPICK_ID = PS_DEFINE.ST_PICKM1_CREATE AND
				TPK.PICKNO_CD = ' '
			GROUP BY
				TPK.TRANSPORTER_CD,
				MTP.BERTHTIME_TX  , 
				MTP.SHIPBERTH_CD  ,
				MTP.PICKORDER_NR
			ORDER BY
				NVL(MTP.PICKORDER_NR, 0);
				
		rowPick			curPick%ROWTYPE;
		chrPickNoCD		TA_PICK.PICKNO_CD%TYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curPick(iTrnID);
		LOOP
			FETCH curPick INTO rowPick;
			EXIT WHEN curPick%NOTFOUND;

			PS_NO.GetPickNo(chrPickNoCD);

			UPDATE
				TA_PICK
			SET
				PICKNO_CD 	   = chrPickNoCD		 ,
				BERTHTIME_TX   = rowPick.BERTHTIME_TX, 
				SHIPBERTH_CD   = rowPick.SHIPBERTH_CD,
				--
				UPD_DT		   = SYSDATE		,
				UPDUSER_CD	   = iUserCD		,
				UPDUSER_TX	   = iUserTX		,
				UPDPROGRAM_CD  = iProgramCD		,
				UPDCOUNTER_NR  = UPDCOUNTER_NR + 1
			WHERE
				TRN_ID		   = iTrnID						AND
				TRANSPORTER_CD = rowPick.TRANSPORTER_CD		AND
				STPICK_ID	   = PS_DEFINE.ST_PICKM1_CREATE AND          
				PICKNO_CD	   = ' ';

		END LOOP;
		CLOSE curPick;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END SetPickNo;

END PS_APPLYPICK;
/
SHOW ERRORS
