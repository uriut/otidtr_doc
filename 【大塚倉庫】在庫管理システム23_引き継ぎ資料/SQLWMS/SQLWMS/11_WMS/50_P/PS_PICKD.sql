-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_PICK AS
	/************************************************************************************/
	/*	ピック指示書追加(通常)															*/
	/************************************************************************************/
	PROCEDURE InsPick(
		iTrnID				IN	NUMBER	,
		iPickNoCD			IN	VARCHAR2,
		iPalletDetNoCD		IN	VARCHAR2, -- パレット明細番号
		iTypePickCD			IN	VARCHAR2,
		irowSLocaStockC		IN	TA_LOCASTOCKC%ROWTYPE,
		irowShipH			IN	TA_SHIPH%ROWTYPE,
		irowShipD			IN	TA_SHIPD%ROWTYPE,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iTypeCarryCD		IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsPick';
		numPickID		TA_PICK.PICK_ID%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_PICKID.NEXtVAL INTO numPickID
		FROM
			DUAL;
		INSERT INTO TA_PICK (
			PICK_ID						,
			TRN_ID						,
			PICKNO_CD					,
			TYPEPICK_CD					,
			STPICK_ID					,
			--
			SHIPH_ID					,
			SHIPD_ID					,
			ORDERNO_CD					,
			ORDERNOSUB_CD				,
			ORDERDETNO_CD				,
			-- SKU
			SKU_CD						,
			LOT_TX						,
			KANBAN_CD					,
			CASEPERPALLET_NR			,
			PALLETLAYER_NR				,
			PALLETCAPACITY_NR			,
			STACKINGNUMBER_NR			,
			TYPEPLALLET_CD				,
			PALLETDETNO_CD				,
			TRANSPORTER_CD				,
			DELIVERYDESTINATION_CD		,
			-- 軒先ピック対応
			DTNAME_TX	 				,
			DTADDRESS1_TX				,
			DTADDRESS2_TX				,
			DTADDRESS3_TX				,
			DTADDRESS4_TX				,
			DIRECTDELIVERY_CD			,
			DELIDATE_TX					,
			TOTALPIC_FL					,
			--
			SHIPBERTH_CD				,
			SHIPPLAN_DT					,
			SLIPNO_TX					,
			--
			USELIMIT_DT					,
			STOCK_DT					,
			AMOUNT_NR					,
			PALLET_NR					,
			CASE_NR						,
			BARA_NR						,
			PCS_NR						,
			-- 元ロケ
			SRCTYPESTOCK_CD				,
			SRCLOCA_CD					,
			SRCSTART_DT					,
			SRCEND_DT					,
			SRCHT_ID					,
			SRCHTUSER_CD				,
			SRCHTUSER_TX				,
			SRCLOCASTOCKC_ID			,
			-- 納品書
			PRTPACKLIST_FL				,
			-- 
			BATCHNO_CD					,
			TYPEAPPLY_CD				,
			TYPEBLOCK_CD				,
			TYPECARRY_CD				,
			-- 管理項目
			UPD_DT						,
			UPDUSER_CD					,
			UPDUSER_TX					,
			ADD_DT						,
			ADDUSER_CD					,
			ADDUSER_TX					,
			UPDPROGRAM_CD				,
			UPDCOUNTER_NR				,
			STRECORD_ID					
		) VALUES (
			numPickID						,	-- PICK_ID
			iTrnID							,	-- TRN_ID
			iPickNoCD						,	-- PICKNO_CD
			iTypePickCD						,	-- TYPEPICK_CD
			PS_DEFINE.ST_PICKM1_CREATE		,	-- STPICK_ID
			--
			irowShipH.SHIPH_ID				,
			irowShipD.SHIPD_ID				,
			irowShipH.ORDERNO_CD			,
			irowShipH.ORDERNOSUB_CD			,
			irowShipD.ORDERDETNO_CD			,
			-- SKU
			irowSLocaStockC.SKU_CD			,	-- SKU_CD
			irowSLocaStockC.LOT_TX			,	-- LOT_TX
			irowSLocaStockC.KANBAN_CD		,	-- KANBAN_CD
			irowSLocaStockC.CASEPERPALLET_NR,	-- CASEPERPALLET_NR
			irowSLocaStockC.PALLETLAYER_NR	,	-- PALLETLAYER_NR
			irowSLocaStockC.PALLETCAPACITY_NR,	-- PALLETCAPACITY_NR
			irowSLocaStockC.STACKINGNUMBER_NR,	-- STACKINGNUMBER_NR
			irowShipH.TYPEPLALLET_CD		,
			iPalletDetNoCD					,	-- 
			irowShipH.TRANSPORTER2_CD		,
			irowShipH.DELIVERYDESTINATION_CD,
			-- 軒先ピック対応
			irowShipH.DTNAME_TX	 			,
			irowShipH.DTADDRESS1_TX			,
			irowShipH.DTADDRESS2_TX			,
			irowShipH.DTADDRESS3_TX			,
			irowShipH.DTADDRESS4_TX			,
			irowShipH.DIRECTDELIVERY_CD		,
			irowShipH.DELIDATE_TX			,
			iTotalPicFL						,
			--
			irowShipH.SHIPBERTH_CD			,
			irowShipH.SHIPPLAN_DT			,
			irowShipH.SLIPNO_TX				,
			irowSLocaStockC.USELIMIT_DT		,	-- USELIMIT_DT
			irowSLocaStockC.STOCK_DT		,	-- STOCK_DT
			irowSLocaStockC.AMOUNT_NR		,	-- AMOUNT_NR
			irowSLocaStockC.PALLET_NR		,	-- PALLET_NR
			irowSLocaStockC.CASE_NR			,	-- CASE_NR
			irowSLocaStockC.BARA_NR			,	-- BARA_NR
			irowSLocaStockC.PCS_NR			,	-- PCS_NR
			-- 元ロケ
			irowSLocaStockC.TYPESTOCK_CD	,	-- SRCTYPESTOCK_CD
			irowSLocaStockC.LOCA_CD			,	-- SRCLOCA_CD
			NULL							,	-- SRCSTART_DT
			NULL							,	-- SRCEND_DT
			0								,	-- SRCHT_ID
			' '								,	-- SRCHTUSER_CD
			' '								,	-- SRCHTUSER_TX
			irowSLocaStockC.LOCASTOCKC_ID	,	-- SRCLOCASTOCKC_ID
			-- 納品書
			irowShipH.INVOICE_FL			,
			--
			irowShipD.BATCHNO_CD			,	-- BATCHNO_CD
			irowShipD.TYPEAPPLY_CD			,	-- TYPEAPPLY_CD
			' '								,	-- TYPEBLOCK_CD
			iTypeCarryCD					,	-- TYPECARRY_CD
			-- 管理項目
			NULL							,	-- UPD_DT
			' '								,	-- UPDUSER_CD
			' '								,	-- UPDUSER_TX
			SYSDATE							,	-- ADD_DT
			iUserCD							,	-- ADDUSER_CD
			iUserTX							,	-- ADDUSER_TX
			iProgramCD						,	-- UPDPROGRAM_CD
			0								,	-- UPDCOUNTER_NR
			PS_DEFINE.ST_RECORD0_NORMAL			-- STRECORD_ID
		);
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsPick;
	/************************************************************************************/
	/*	ピック指示書追加(パレット分割)															*/
	/************************************************************************************/
	PROCEDURE DevPickPallet(
		iTrnID				IN	NUMBER	,
		iPickID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DevPickPallet';

		CURSOR curPick(
			iPickID	TA_PICK.PICK_ID%TYPE
		) IS
			SELECT
				*
			FROM
				TA_PICK
			WHERE
				PICK_ID	= iPickID;

		rowPick			curPick%ROWTYPE;
		numPickID		TA_PICK.PICK_ID%TYPE;
		rowSku			MA_SKU%ROWTYPE;
		numPalletNR		NUMBER;
		numCaseNR		NUMBER;
		numBaraNR		NUMBER;
		numPcsNR		NUMBER;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || '], iPickID = [' || iPickID || ']'
		);

		OPEN curPick(iPickID);
		FETCH curPick INTO rowPick;
		IF curPick%FOUND THEN

			--パレット数を１ずつばらすため以下をチェック

			IF rowPick.CASE_NR <> 0 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：ケース数が０ではありません。' ||
																 'PICK_ID = [' || iPickID || ']');
			END IF;

			IF rowPick.BARA_NR <> 0 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：バラ数が０ではありません。' ||
																 'PICK_ID = [' || iPickID || ']');
			END IF;

			-- データ取得
			PS_MASTER.GetSku(rowPick.SKU_CD, rowSku);

			FOR i IN 1..rowPick.PALLET_NR LOOP

				numPalletNR := 1;
				numCaseNR	:= 0;
				numBaraNR	:= 0;
				numPcsNR 	:= rowSku.PCSPERCASE_NR * rowPick.PALLETCAPACITY_NR;

				SELECT
					SEQ_PICKID.NEXtVAL INTO numPickID
				FROM
					DUAL;

				INSERT INTO TA_PICK (
					PICK_ID						,
					TRN_ID						,
					PICKNO_CD					,
					TYPEPICK_CD					,
					STPICK_ID					,
					--
					SHIPH_ID					,
					SHIPD_ID					,
					ORDERNO_CD					,
					ORDERNOSUB_CD				,
					ORDERDETNO_CD				,
					-- SKU
					SKU_CD						,
					LOT_TX						,
					KANBAN_CD					,
					CASEPERPALLET_NR			,
					PALLETLAYER_NR				,
					PALLETCAPACITY_NR			,
					STACKINGNUMBER_NR			,
					TYPEPLALLET_CD				,
					PALLETDETNO_CD				,
					TRANSPORTER_CD				,
					DELIVERYDESTINATION_CD		,
					-- 軒先ピック対応
					DTNAME_TX	 				,
					DTADDRESS1_TX				,
					DTADDRESS2_TX				,
					DTADDRESS3_TX				,
					DTADDRESS4_TX				,
					DIRECTDELIVERY_CD			,
					DELIDATE_TX					,
					TOTALPIC_FL					,
					--
					SHIPBERTH_CD				,
					SHIPPLAN_DT					,
					SLIPNO_TX					,
					--
					USELIMIT_DT					,
					STOCK_DT					,
					AMOUNT_NR					,
					PALLET_NR					,
					CASE_NR						,
					BARA_NR						,
					PCS_NR						,
					-- 元ロケ
					SRCTYPESTOCK_CD				,
					SRCLOCA_CD					,
					SRCSTART_DT					,
					SRCEND_DT					,
					SRCHT_ID					,
					SRCHTUSER_CD				,
					SRCHTUSER_TX				,
					SRCLOCASTOCKC_ID			,
					-- 納品書
					PRTPACKLIST_FL				,
					-- 
					BATCHNO_CD					,
					TYPEAPPLY_CD				,
					TYPEBLOCK_CD				,
					TYPECARRY_CD				,
					-- 管理項目
					UPD_DT						,
					UPDUSER_CD					,
					UPDUSER_TX					,
					ADD_DT						,
					ADDUSER_CD					,
					ADDUSER_TX					,
					UPDPROGRAM_CD				,
					UPDCOUNTER_NR				,
					STRECORD_ID					
				) VALUES (
					numPickID							,
					rowPick.TRN_ID						,
					rowPick.PICKNO_CD					,
					rowPick.TYPEPICK_CD					,
					rowPick.STPICK_ID					,
					--
					rowPick.SHIPH_ID					,
					rowPick.SHIPD_ID					,
					rowPick.ORDERNO_CD					,
					rowPick.ORDERNOSUB_CD				,
					rowPick.ORDERDETNO_CD				,
					-- SKU
					rowPick.SKU_CD						,
					rowPick.LOT_TX						,
					rowPick.KANBAN_CD					,
					rowPick.CASEPERPALLET_NR			,
					rowPick.PALLETLAYER_NR				,
					rowPick.PALLETCAPACITY_NR			,
					rowPick.STACKINGNUMBER_NR			,
					rowPick.TYPEPLALLET_CD				,
					rowPick.PALLETDETNO_CD				,
					rowPick.TRANSPORTER_CD				,
					rowPick.DELIVERYDESTINATION_CD		,
					rowPick.DTNAME_TX	 	   			,
					rowPick.DTADDRESS1_TX	   			,
					rowPick.DTADDRESS2_TX	   			,
					rowPick.DTADDRESS3_TX	   			,
					rowPick.DTADDRESS4_TX	   			,
					rowPick.DIRECTDELIVERY_CD			,
					rowPick.DELIDATE_TX	   				,
					rowPick.TOTALPIC_FL					,
					rowPick.SHIPBERTH_CD				,
					rowPick.SHIPPLAN_DT					,
					rowPick.SLIPNO_TX					,
					--
					rowPick.USELIMIT_DT					,
					rowPick.STOCK_DT					,
					rowPick.AMOUNT_NR					,
					numPalletNR							, --PALLET_NR
					numCaseNR							, --CASE_NR	 
					numBaraNR							, --BARA_NR	 
					numPcsNR							, --PCS_NR	 
					-- 元ロケ
					rowPick.SRCTYPESTOCK_CD				,
					rowPick.SRCLOCA_CD					,
					rowPick.SRCSTART_DT					,
					rowPick.SRCEND_DT					,
					rowPick.SRCHT_ID					,
					rowPick.SRCHTUSER_CD				,
					rowPick.SRCHTUSER_TX				,
					rowPick.SRCLOCASTOCKC_ID			,
					-- 納品書
					rowPick.PRTPACKLIST_FL				,
					-- 
					rowPick.BATCHNO_CD					,
					rowPick.TYPEAPPLY_CD				,
					rowPick.TYPEBLOCK_CD				,
					rowPick.TYPECARRY_CD				,
					-- 管理項目
					rowPick.UPD_DT						,
					rowPick.UPDUSER_CD					,
					rowPick.UPDUSER_TX					,
					rowPick.ADD_DT						,
					rowPick.ADDUSER_CD					,
					rowPick.ADDUSER_TX					,
					rowPick.UPDPROGRAM_CD				,
					rowPick.UPDCOUNTER_NR				,
					rowPick.STRECORD_ID					
				);	

			END LOOP;

			--分割元データは削除
			DELETE
				TA_PICK
			WHERE
				PICK_ID	= iPickID;

		END IF;
		CLOSE curPick;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || '], iPickID = [' || iPickID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END DevPickPallet;
	/************************************************************************************/
	/*	ピック指示書追加(ケース分割)															*/
	/************************************************************************************/
	PROCEDURE DevPickCase(
		iTrnID				IN	NUMBER	,
		iPickID				IN	NUMBER	, --元ピックデータ
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2,
		oPickID				OUT	NUMBER	  --新ピックデータ
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DevPickCase';

		CURSOR curPick(
			iPickID	TA_PICK.PICK_ID%TYPE
		) IS
			SELECT
				*
			FROM
				TA_PICK
			WHERE
				PICK_ID	= iPickID;

		rowPick			curPick%ROWTYPE;
		numPickID		TA_PICK.PICK_ID%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || '], iPickID = [' || iPickID || ']'
		);

		OPEN curPick(iPickID);
		FETCH curPick INTO rowPick;
		IF curPick%FOUND THEN

--			IF rowPick.PALLET_NR <> 0 THEN
--				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：パレット数が０ではありません。' ||
--																 'PICK_ID = [' || iPickID || ']');
--			END IF;

			IF rowPick.BARA_NR <> 0 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：バラ数が０ではありません。' ||
																 'PICK_ID = [' || iPickID || ']');
			END IF;

			SELECT
				SEQ_PICKID.NEXtVAL INTO oPickID
			FROM
				DUAL;

			INSERT INTO TA_PICK (
				PICK_ID						,
				TRN_ID						,
				PICKNO_CD					,
				TYPEPICK_CD					,
				STPICK_ID					,
				--
				SHIPH_ID					,
				SHIPD_ID					,
				ORDERNO_CD					,
				ORDERNOSUB_CD				,
				ORDERDETNO_CD				,
				-- SKU
				SKU_CD						,
				LOT_TX						,
				KANBAN_CD					,
				CASEPERPALLET_NR			,
				PALLETLAYER_NR				,
				PALLETCAPACITY_NR			,
				STACKINGNUMBER_NR			,
				TYPEPLALLET_CD				,
				PALLETDETNO_CD				,
				TRANSPORTER_CD				,
				DELIVERYDESTINATION_CD		,
				-- 軒先ピック対応
				DTNAME_TX	 				,
				DTADDRESS1_TX				,
				DTADDRESS2_TX				,
				DTADDRESS3_TX				,
				DTADDRESS4_TX				,
				DIRECTDELIVERY_CD			,
				DELIDATE_TX					,
				TOTALPIC_FL					,
				--
				SHIPBERTH_CD				,
				SHIPPLAN_DT					,
				SLIPNO_TX					,
				--
				USELIMIT_DT					,
				STOCK_DT					,
				AMOUNT_NR					,
				PALLET_NR					,
				CASE_NR						,
				BARA_NR						,
				PCS_NR						,
				-- 元ロケ
				SRCTYPESTOCK_CD				,
				SRCLOCA_CD					,
				SRCSTART_DT					,
				SRCEND_DT					,
				SRCHT_ID					,
				SRCHTUSER_CD				,
				SRCHTUSER_TX				,
				SRCLOCASTOCKC_ID			,
				-- 納品書
				PRTPACKLIST_FL				,
				-- 
				BATCHNO_CD					,
				TYPEAPPLY_CD				,
				TYPEBLOCK_CD				,
				TYPECARRY_CD				,
				-- 管理項目
				UPD_DT						,
				UPDUSER_CD					,
				UPDUSER_TX					,
				ADD_DT						,
				ADDUSER_CD					,
				ADDUSER_TX					,
				UPDPROGRAM_CD				,
				UPDCOUNTER_NR				,
				STRECORD_ID					
			) VALUES (
				oPickID								,
				rowPick.TRN_ID						,
				rowPick.PICKNO_CD					,
				rowPick.TYPEPICK_CD					,
				rowPick.STPICK_ID					,
				--
				rowPick.SHIPH_ID					,
				rowPick.SHIPD_ID					,
				rowPick.ORDERNO_CD					,
				rowPick.ORDERNOSUB_CD				,
				rowPick.ORDERDETNO_CD				,
				-- SKU
				rowPick.SKU_CD						,
				rowPick.LOT_TX						,
				rowPick.KANBAN_CD					,
				rowPick.CASEPERPALLET_NR			,
				rowPick.PALLETLAYER_NR				,
				rowPick.PALLETCAPACITY_NR			,
				rowPick.STACKINGNUMBER_NR			,
				rowPick.TYPEPLALLET_CD				,
				rowPick.PALLETDETNO_CD				,
				rowPick.TRANSPORTER_CD				,
				rowPick.DELIVERYDESTINATION_CD		,
				rowPick.DTNAME_TX	 	   			,
				rowPick.DTADDRESS1_TX	   			,
				rowPick.DTADDRESS2_TX	   			,
				rowPick.DTADDRESS3_TX	   			,
				rowPick.DTADDRESS4_TX	   			,
				rowPick.DIRECTDELIVERY_CD			,
				rowPick.DELIDATE_TX	   				,
				rowPick.TOTALPIC_FL					,
				rowPick.SHIPBERTH_CD				,
				rowPick.SHIPPLAN_DT					,
				rowPick.SLIPNO_TX					,
				--
				rowPick.USELIMIT_DT					,
				rowPick.STOCK_DT					,
				rowPick.AMOUNT_NR					,
				rowPick.PALLET_NR					,
				rowPick.CASE_NR	  					,
				rowPick.BARA_NR	  					,
				rowPick.PCS_NR	  					,
				-- 元ロケ
				rowPick.SRCTYPESTOCK_CD				,
				rowPick.SRCLOCA_CD					,
				rowPick.SRCSTART_DT					,
				rowPick.SRCEND_DT					,
				rowPick.SRCHT_ID					,
				rowPick.SRCHTUSER_CD				,
				rowPick.SRCHTUSER_TX				,
				rowPick.SRCLOCASTOCKC_ID			,
				-- 納品書
				rowPick.PRTPACKLIST_FL				,
				-- 
				rowPick.BATCHNO_CD					,
				rowPick.TYPEAPPLY_CD				,
				rowPick.TYPEBLOCK_CD				,
				rowPick.TYPECARRY_CD				,
				-- 管理項目
				rowPick.UPD_DT						,
				rowPick.UPDUSER_CD					,
				rowPick.UPDUSER_TX					,
				rowPick.ADD_DT						,
				rowPick.ADDUSER_CD					,
				rowPick.ADDUSER_TX					,
				rowPick.UPDPROGRAM_CD				,
				rowPick.UPDCOUNTER_NR				,
				rowPick.STRECORD_ID					
			);	

		END IF;
		CLOSE curPick;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || '], iPickID = [' || iPickID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END DevPickCase;
	/************************************************************************************/
	/*	ピッキングデータ有効															*/
	/************************************************************************************/
	PROCEDURE PickDataOK(
		iTrnID				IN	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'PickDataOK';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- iPad作業が先のためパレット成りは未印刷にする
		UPDATE TA_PICK
		SET
			STPICK_ID	= PS_DEFINE.ST_PICK0_PRINTNG
		WHERE
			TRN_ID		= iTrnID						AND
			STPICK_ID	= PS_DEFINE.ST_PICKM1_CREATE	AND
			TYPEPICK_CD IN ( PS_DEFINE.TYPE_PICK_PALLET_N, PS_DEFINE.TYPE_PICK_PALLET_S ); --パレット成り
		-- 未補充
		UPDATE TA_PICK
		SET
			STPICK_ID	= PS_DEFINE.ST_PICK0_PRINTNG
		WHERE
			TRN_ID		= iTrnID						AND
			STPICK_ID	= PS_DEFINE.ST_PICKM1_CREATE	AND
			PICK_ID IN (
				SELECT
					PICK_ID
				FROM
					TA_SUPP
				WHERE
					TRN_ID = iTrnID );
		-- 印刷可
		UPDATE TA_PICK
		SET
			STPICK_ID	= PS_DEFINE.ST_PICK1_PRINTOK
		WHERE
			TRN_ID		= iTrnID						AND
			STPICK_ID	= PS_DEFINE.ST_PICKM1_CREATE;	--AND
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END PickDataOK;
	/************************************************************************************/
	/*	作成中ピッキングデータ削除														*/
	/************************************************************************************/
	PROCEDURE DelPick(
		iTrnID				IN	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DelPick';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		DELETE TA_PICK
		WHERE
			TRN_ID	 	= iTrnID	AND
			STPICK_ID	= PS_DEFINE.ST_PICKM1_CREATE;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END DelPick;
	/************************************************************************************/
	/*	作成中ピッキングデータ削除（指示書指定）										*/
	/************************************************************************************/
	PROCEDURE DelPick(
		iTrnID				IN	NUMBER,
		iPickNoCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DelPick';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		DELETE TA_PICK
		WHERE
			TRN_ID	 	= iTrnID	AND
			PICKNO_CD	= iPickNoCD	AND
			STPICK_ID	= PS_DEFINE.ST_PICKM1_CREATE;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END DelPick;
	/************************************************************************************/
	/*	作成中ピッキングデータ削除（明細指定）											*/
	/************************************************************************************/
	PROCEDURE DelPick(
		iTrnID				IN	NUMBER,
		iShipHID			IN	NUMBER,
		iShipDID			IN	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DelPick';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		DELETE TA_PICK
		WHERE
			TRN_ID	 	= iTrnID	AND
			SHIPH_ID	= iShipHID	AND
			SHIPD_ID	= iShipDID	AND
			STPICK_ID	= PS_DEFINE.ST_PICKM1_CREATE;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END DelPick;
	/************************************************************************************/
	/*	ピック指示書完了																*/
	/************************************************************************************/
	PROCEDURE EndPick(
		iPickNoCD			IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'EndPick';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		--ピック更新
		UPDATE TA_PICK
		SET
			STPICK_ID		= PS_DEFINE.ST_PICK8_SHIPOK_E	,
			SHIPEND_FL		= 1								,
			SHIPENDCOUNT_NR	= SHIPENDCOUNT_NR + 1			,
			SHIPENDSTART_DT	= SYSDATE						,
			SHIPENDEND_DT	= SYSDATE						,
			--
			UPD_DT			= SYSDATE						,
			UPDUSER_CD		= iUserCd						,
			UPDUSER_TX		= iUserTx						,
			UPDPROGRAM_CD	= iProgramCD					,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PICKNO_CD		= iPickNoCD;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END EndPick;
	/************************************************************************************/
	/*	ピッキングデータ・補充元消し込み												*/
	/************************************************************************************/
	PROCEDURE UpdPickSrcSupp(
		iPickID				IN	NUMBER	,
		iOldLocaStockCID	IN	NUMBER	,
		iNewLocaStockCID	IN	NUMBER	,
		iLocaCD				IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdPickSrcSupp';
		
		CURSOR curPick(
			PickID			TA_PICK.PICK_ID%TYPE,
			LocaStockCID	TA_PICK.SRCLOCASTOCKC_ID%TYPE
		) IS
			SELECT
				ROWID
			FROM
				TA_PICK
			WHERE
				PICK_ID				= PickID	AND
				SRCLOCASTOCKC_ID	= LocaStockCID;
		rowPick 	curPick%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curPick(iPickID, iOldLocaStockCID);
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ピッキング指示書の補充元を消し込めません。' ||
					'PICK_ID = [' || iPickID || '], SRCLOCASTOCKC_ID = [' || iOldLocaStockCID || ']'
			);
		END IF;
		UPDATE TA_PICK
		SET
			SRCLOCA_CD			= iLocaCD,
			SRCLOCASTOCKC_ID	= iNewLocaStockCID
		WHERE
			ROWID				= rowPick.ROWID;
		CLOSE curPick;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END UpdPickSrcSupp;
	/************************************************************************************/
	/*	ピッキングデータ・補充先消し込み												*/
	/************************************************************************************/
	PROCEDURE UpdPickDestSupp(
		iPickID				IN	NUMBER	,
		iOldLocaStockCID	IN	NUMBER	,
		iNewLocaStockCID	IN	NUMBER	,
		iLocaCD				IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdPickDestSupp';
		
		CURSOR curPick(
			PickID			TA_PICK.PICK_ID%TYPE,
			LocaStockCID	TA_PICK.SRCLOCASTOCKC_ID%TYPE
		) IS
			SELECT
				ROWID
			FROM
				TA_PICK
			WHERE
				PICK_ID				= PickID	AND
				SRCLOCASTOCKC_ID	= LocaStockCID;
		rowPick 	curPick%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curPick(iPickID, iOldLocaStockCID);
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ピッキング指示書の補充先を消し込めません。' ||
					'PICK_ID = [' || iPickID || '], SRCLOCASTOCKC_ID = [' || iOldLocaStockCID || ']'
			);
		END IF;
		UPDATE TA_PICK
		SET
			SRCLOCA_CD			= iLocaCD,
			SRCLOCASTOCKC_ID	= iNewLocaStockCID
		WHERE
			ROWID				= rowPick.ROWID;
		CLOSE curPick;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END UpdPickDestSupp;
	/************************************************************************************/
	/*	ピッキングデータ・補充消し込み													*/
	/************************************************************************************/
	PROCEDURE UpdPickEndSupp(
		iSuppNoCD			IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdPickEndSupp';
		-- 補充指示
		CURSOR curSupp(
			SuppNoCD		TA_SUPP.SUPPNO_CD%TYPE
		) IS
			SELECT
				SHIPH_ID
			FROM
				TA_SUPP
			WHERE
				SUPPNO_CD	= SuppNoCD
			GROUP BY
				SHIPH_ID;
		rowSupp		curSupp%ROWTYPE;
		-- 引当全完了、かつ、補充全完了のオーダー
		CURSOR curComp(
			ShipHID		TA_SHIPH.SHIPH_ID%TYPE
		) IS
			SELECT
				TSH.SHIPH_ID
			FROM
				TA_SHIPH	TSH,
				TA_SUPP		TSP
			WHERE
				TSH.SHIPH_ID	= TSP.SHIPH_ID	AND
				TSH.SHIPH_ID	= ShipHID
			GROUP BY
				TSH.SHIPH_ID
			HAVING
				MIN(TSH.STSHIP_ID) >= PS_DEFINE.ST_SHIP9_PAPPLY_E	AND
				MIN(TSP.STSUPP_ID) = PS_DEFINE.ST_SUPP8_SUPPINOK_E	;
		rowComp		curComp%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curSupp(iSuppNoCD);
		LOOP
			FETCH curSupp INTO rowSupp;
			EXIT WHEN curSupp%NOTFOUND;
			--
			OPEN curComp(rowSupp.SHIPH_ID);
			FETCH curComp INTO rowComp;
			IF curComp%FOUND THEN
				UPDATE TA_PICK
				SET
					STPICK_ID	= PS_DEFINE.ST_PICK1_PRINTOK
				WHERE
					SHIPH_ID	= rowSupp.SHIPH_ID AND
					TYPEPICK_CD	NOT IN (PS_DEFINE.TYPE_PICK_PALLET_N,PS_DEFINE.TYPE_PICK_PALLET_S) AND	-- パレット成り以外
					STPICK_ID	= PS_DEFINE.ST_PICK0_PRINTNG;
			END IF;
			CLOSE curComp;
		END LOOP;
		CLOSE curSupp;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curSupp%ISOPEN THEN CLOSE curSupp; END IF;
			IF curComp%ISOPEN THEN CLOSE curComp; END IF;
			RAISE;
	END UpdPickEndSupp;
END PS_PICK;
/
SHOW ERRORS
