-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PW_TAKING AS 
	--
	--
	--
	/************************************************************************************/
	/*	荷動き棚卸																		*/
	/************************************************************************************/

	PROCEDURE Stocktaking(
		--	iPalletDetNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2			
--		iProgramCD		IN VARCHAR2
	 ) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'Stocktaking';		
		CURSOR curTmp1(A	VARCHAR2) IS
			SELECT
				MAX(tsst.MANAGER_NUM) AS MANAGER_NUM_max
			FROM
				ta_savestocktaking tsst
			WHERE
				SUBSTR(tsst.MANAGER_NUM,0,8) = A;
			rowTmp1 	curTmp1%ROWTYPE;		

			ikk				 number := 0;
		CURSOR curTmp IS	
		SELECT
		STOCKTAKING_ID,
		LOCA_CD,
		KANBAN_CD,
		EXTERIOR_CD,
		SKU_TX,
		SIZE_TX,
		LOT_TX,
		PALLETCAPACITY_NR,
		SUB.PALLET_NR + TRUNC(SUB.CASE_NR / SUB.PALLETCAPACITY_NR) AS PALLET_NR,
		MOD(SUB.CASE_NR,SUB. PALLETCAPACITY_NR) + TRUNC(SUB.BARA_NR / SUB.AMOUNT_NR) AS CASE_NR,
		MOD(SUB.BARA_NR,SUB.AMOUNT_NR) AS BARA_NR
		FROM
		(
		SELECT
			ROW_NUMBER()
			OVER (ORDER BY SIO.LOCA_CD) AS STOCKTAKING_ID				,		
--			SUBSTR(SYSDATE,0,8) || (SUBSTR(MANAGER_NUM_max,8,10) + 1) AS MANAGER_NUM					,
			SIO.LOCA_CD	AS LOCA_CD										,
			LP.KANBAN_CD AS KANBAN_CD									,
			MS.EXTERIOR_CD AS EXTERIOR_CD      							,
			MS.SKU2_TX AS SKU_TX										,
			MS.SIZE_TX AS SIZE_TX										,
			LP.LOT_TX AS LOT_TX											,
			LP.PALLETCAPACITY_NR AS PALLETCAPACITY_NR     ,
			LP.AMOUNT_NR AS AMOUNT_NR                     ,
			SUM(LP.PALLET_NR) AS PALLET_NR								,
			SUM(LP.CASE_NR) AS CASE_NR									,
			SUM(LP.BARA_NR) AS BARA_NR									,
			SUM(LP.PCS_NR) AS PCS_NR											
			FROM
				(
				SELECT
					LOCA_CD
				FROM
					TA_STOCKINOUT
--				WHERE
--					ADD_DT > TRUNC(SYSDATE)			--ON当日荷動きロケを抽出; OFF当日全だなロケを抽出
				GROUP BY
					LOCA_CD
				) SIO
					LEFT OUTER JOIN (
							SELECT
								LOCA_CD				,
								KANBAN_CD			,
								SKU_CD				,
								LOT_TX				,
								PALLETCAPACITY_NR	,
								AMOUNT_NR		    ,
								PALLET_NR			,
								CASE_NR				,
								BARA_NR				,
								PCS_NR
							FROM
								TA_LOCASTOCKP						--引当可能しかない
							UNION ALL
							SELECT
								LOCA_CD				,
								KANBAN_CD			,
								SKU_CD				,
								LOT_TX				,
								PALLETCAPACITY_NR	,
								AMOUNT_NR     		,
								SUM(PALLET_NR) AS PALLET_NR	,
								SUM(CASE_NR) AS CASE_NR		,
								SUM(BARA_NR) AS BARA_NR		,
								SUM(PCS_NR) AS PCS_NR
							FROM
								TA_LOCASTOCKC						--引当済みしかない
							WHERE
								STSTOCK_ID = 0
							GROUP BY
								LOCA_CD				,
								KANBAN_CD			,
								SKU_CD				,
								LOT_TX				,
								PALLETCAPACITY_NR,
                AMOUNT_NR     
					) LP
						ON SIO.LOCA_CD = LP.LOCA_CD
					LEFT OUTER JOIN MA_LOCA ML
						ON SIO.LOCA_CD = ML.LOCA_CD
					LEFT OUTER JOIN MA_SKU MS
						ON LP.SKU_CD = MS.SKU_CD
			GROUP BY
--			SUBSTR(SYSDATE,0,8) || '-01'						,
			SIO.LOCA_CD											,
			LP.KANBAN_CD										,
			MS.EXTERIOR_CD     									,
			MS.SKU2_TX											,
			MS.SIZE_TX											,
			LP.LOT_TX							        		,
			LP.PALLETCAPACITY_NR       						    ,
			LP.AMOUNT_NR
		) SUB
			ORDER BY
				STOCKTAKING_ID ASC;
				rowTmp curTmp%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');

		OPEN curTmp1(SUBSTR(SYSDATE,0,8));
		FETCH curTmp1 INTO rowTmp1;
		IF curTmp1%NOTFOUND THEN
		ikk :=  1;		--棚卸保存テーブルに値がない場合、当日一回目の棚卸
		else
		ikk := SUBSTR(rowTmp1.MANAGER_NUM_max,9) + 1;
		END IF;
		CLOSE curTmp1;
		
		DELETE TA_STOCKTAKING;		
		OPEN curTmp;
		LOOP
		
			FETCH curTmp INTO rowTmp;
			EXIT WHEN curTmp%NOTFOUND;
				INSERT INTO TA_STOCKTAKING (
					STOCKTAKING_ID		,
					MANAGER_NUM			,
					LOCA_CD				,
					KANBAN_CD			,
					EXTERIOR_CD			,
					SKU_TX				,
					SIZE_TX				,	
					LOT_TX				,
					PALLETCAPACITY_NR	,
					PALLET_NR			,
					CASE_NR				,
					BARA_NR				
--					PCS_NR				,
--					MEMO_TX				,			
--					NEEDSEND_FL			,
--					SENDEND_FL			,
					-- 管理項目
--					UPD_DT				,
--					UPDUSER_CD			,
--					UPDUSER_TX			,
--					ADD_DT				,
--					ADDUSER_CD			,
--					ADDUSER_TX			,
--					UPDPROGRAM_CD		,
--					UPDCOUNTER_NR		,
--					STRECORD_ID
				) VALUES (
					rowTmp.STOCKTAKING_ID		,
					SUBSTR(SYSDATE,0,8) || '-' || ikk			,
					rowTmp.LOCA_CD				,
					rowTmp.KANBAN_CD			,
					rowTmp.EXTERIOR_CD			,
					rowTmp.SKU_TX				,
					rowTmp.SIZE_TX				,
					rowTmp.LOT_TX				,
					rowTmp.PALLETCAPACITY_NR	,					
					rowTmp.PALLET_NR			,
					rowTmp.CASE_NR				,
					rowTmp.BARA_NR				
--					rowTmp.PCS_NR				
					-- 管理項目
--					rowTmp.UPD_DT				,
--					rowTmp.UPDUSER_CD			,
--					rowTmp.UPDUSER_TX			,
--					rowTmp.ADD_DT				,
--					rowTmp.ADDUSER_CD			,
--					rowTmp.ADDUSER_TX			,
--					rowTmp.UPDPROGRAM_CD		,
--					rowTmp.UPDCOUNTER_NR		,
--					rowTmp.STRECORD_ID
				);
		END LOOP;
		CLOSE curTmp;		
		COMMIT;
				PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
			EXCEPTION
				WHEN OTHERS THEN
					IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
						PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					ELSE
						PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					END IF;
					RAISE;
			END Stocktaking;
	/************************************************************************************/
	/*	全棚棚卸																		*/
	/************************************************************************************/
	PROCEDURE StocktakingAll(
		--	iPalletDetNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	
--		iProgramCD		IN VARCHAR2
	 ) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'StocktakingAll';		
		CURSOR curTmp IS
	SELECT
		STOCKTAKING_ID	,
		MANAGER_NUM		,
		LOCA_CD			,
		KANBAN_CD		,
		EXTERIOR_CD		,
		SKU_TX			,
		SIZE_TX			,
		LOT_TX			,
		PALLETCAPACITY_NR	,
		PALLET_NR + TRUNC(CASE_NR / PALLETCAPACITY_NR) 						AS PALLET_NR	,
		MOD(CASE_NR, PALLETCAPACITY_NR) + TRUNC(BARA_NR / AMOUNT_NR) 		AS CASE_NR		,
		MOD(BARA_NR,AMOUNT_NR) 												AS BARA_NR					
	FROM
	(
		SELECT
			ROW_NUMBER()
			OVER (ORDER BY LOCA_CD) AS STOCKTAKING_ID				,		
			SUBSTR(SYSDATE,0,8) || '-01' AS MANAGER_NUM				,
					LOCA_CD				,
					KANBAN_CD			,
					EXTERIOR_CD			,
					SKU_TX				,
					SIZE_TX				,	
					LOT_TX				,
					PALLETCAPACITY_NR	,
					AMOUNT_NR			,
--					PALLET_NR + TRUNC(CASE_NR / PALLETCAPACITY_NR) 						AS PALLET_NR	,
--					MOD(CASE_NR, PALLETCAPACITY_NR) + TRUNC(BARA_NR / AMOUNT_NR) 		AS CASE_NR		,
--					MOD(BARA_NR,AMOUNT_NR) 												AS BARA_NR					
					(PPALLET_NR + CPALLET_NR) AS PALLET_NR			,
					(PCASE_NR + CCASE_NR) AS CASE_NR				,
					(PBARA_NR + CBARA_NR) AS BARA_NR				
--					(PPCS_NR + CPCS_NR) AS PCS_NR					
--					MEMO_TX				,			
--					NEEDSEND_FL			,
--					SENDEND_FL			,
					-- 管理項目
--					UPD_DT				,
--					UPDUSER_CD			
--					UPDUSER_TX			,
--					ADD_DT				,
--					ADDUSER_CD			,
--					ADDUSER_TX			,
--					UPDPROGRAM_CD		,
--					UPDCOUNTER_NR		,
--					STRECORD_ID
		FROM
		(
			SELECT
			MLC.LOCA_CD 										,
			MLC.LOCA_TX 										,
			MLC.WH_CD 											,
			MLC.FLOOR_CD 										,
			MLC.AREA_CD 										,
			MLC.LINE_CD 										,
			MLC.SHELF_CD 										,
			MLC.LAYER_CD 										,
			MLC.ORDER_CD 										,
			MLC.TYPELOCABLOCK_CD 								,
			MTB.TYPELOCABLOCK_TX 								,
			MLC.HEQUIP_CD 										,
			MLC.ARRIVPRIORITY_NR 								,
			MLC.SHIPPRIORITY_NR 								,
			MLC.INVENT_FL 										,
			DECODE(MLC.NOTUSE_FL,0, '入荷可', 1, '入荷不可')	,
			DECODE(MLC.NOTSHIP_FL,0, '出荷可', 1, '出荷不可')	,
			LS2.SKU_CD 											,
			LS2.KANBAN_CD 										,
			LS2.LOT_TX 											,
			LS2.PALLETCAPACITY_NR								,
			LS2.EXTERIOR_CD 									,
			LS2.SIZE_TX 										,
			LS2.TOTALPALLET_NR 									,
			LS2.TOTALCASE_NR 									,
			LS2.TOTALBARA_NR 									,
			LS2.TOTALPCS_NR 									,
			LS2.PPALLET_NR 										,
			LS2.PCASE_NR 										,
			LS2.PBARA_NR 										,
			LS2.PPCS_NR 										,
			LS2.CPALLET_NR 										,
			LS2.CCASE_NR 										,
			LS2.CBARA_NR 										,
			LS2.CPCS_NR 										,
			LS2.TYPESTOCK_CD 									,
			LS2.TYPESTOCK_TX 									,
			MLC.TYPELOCA_CD 									,
			MTL.TYPELOCA_TX 									,
			MLC.SHIPVOLUME_CD 									,
			MBSH.SHIPVOLUME_TX 									,
			LS2.STSTOCK_ID										,
			LS2.STSTOCK_TX 										,
			REPLACE(REPLACE(LS2.SKU_TX, Chr(13), ''),Chr(10), '') AS SKU_TX,
			LS2.SKUBARCODE_TX 									,
			LS2.ITFCODE_TX 										,
			LS2.AMOUNT_NR 										,
			MLC.LINEUNIT_NR 									,
			LS2.CASEPERPALLET_NR 								,
			LS2.PALLETLAYER_NR									,
			CASE
				WHEN LS2.SKU_CD IS NULL AND TMP.LOCA_CD IS NULL THEN 1 --空きロケ
				ELSE 0												--使用ロケ
			END AS LOCAEMPTY_FL									,
			MLC.ABC_ID											,	-- ロケーションABCID
			MAABC.ABC_CD										,	-- ロケーションABC
			MLC.ABCSUB_ID										,	-- 重量グループID
			MASUB.ABCSUB_TX											-- 重量グループ
		FROM
			(
			SELECT
				TLS.LOCA_CD 							,
				TLS.SKU_CD 								,
				TLS.KANBAN_CD 							,
				TLS.LOT_TX 								,
				TLS.PALLETCAPACITY_NR					,
				SUM(TLS.TOTALPALLET_NR) AS TOTALPALLET_NR,
				SUM(TLS.TOTALCASE_NR)   AS TOTALCASE_NR	,
				SUM(TLS.TOTALBARA_NR)   AS TOTALBARA_NR	,
				SUM(TLS.TOTALPCS_NR)    AS TOTALPCS_NR	,
				SUM(TLS.PPALLET_NR)     AS PPALLET_NR	,
				SUM(TLS.PCASE_NR)       AS PCASE_NR		,
				SUM(TLS.PBARA_NR)       AS PBARA_NR		,
				SUM(TLS.PPCS_NR)        AS PPCS_NR		,
				SUM(TLS.CPALLET_NR)     AS CPALLET_NR	,
				SUM(TLS.CCASE_NR)       AS CCASE_NR		,
				SUM(TLS.CBARA_NR)       AS CBARA_NR		,
				SUM(TLS.CPCS_NR)        AS CPCS_NR		,
				TLS.TYPESTOCK_CD 						,
				MTS.TYPESTOCK_TX 						,
				TLS.STSTOCK_ID 							,
				MSS.STSTOCK_TX 							,
				TLS.SKU_TX 								,
				TLS.SKUBARCODE_TX 						,
				TLS.ITFCODE_TX 							,
				TLS.AMOUNT_NR 							,
				TLS.CASEPERPALLET_NR 					,
				TLS.PALLETLAYER_NR 						,
				TLS.EXTERIOR_CD 						,
				TLS.SIZE_TX
			FROM
				(
				SELECT
					TLP.LOCA_CD 					,
					TLP.SKU_CD 						,
					TLP.KANBAN_CD 					,
					TLP.LOT_TX 						,
					TLP.PALLETCAPACITY_NR			,
					TLP.PALLET_NR AS TOTALPALLET_NR	,
					TLP.CASE_NR   AS TOTALCASE_NR	,
					TLP.BARA_NR   AS TOTALBARA_NR	,
					TLP.PCS_NR    AS TOTALPCS_NR	,
					TLP.PALLET_NR AS PPALLET_NR		,
					TLP.CASE_NR   AS PCASE_NR		,
					TLP.BARA_NR   AS PBARA_NR		,
					TLP.PCS_NR    AS PPCS_NR		,
					0             AS CPALLET_NR		,
					0             AS CCASE_NR		,
					0             AS CBARA_NR		,
					0             AS CPCS_NR		,
					TLP.TYPESTOCK_CD 				,
					0 AS STSTOCK_ID					,
					MSK.SKU_TX 						,
					MSK.SKUBARCODE_TX 				,
					MSK.ITFCODE_TX 					,
					MSK.PCSPERCASE_NR AS AMOUNT_NR	,
					MSK.EXTERIOR_CD 				,
					MSK.SIZE_TX 					,
					TLP.CASEPERPALLET_NR			,
					TLP.PALLETLAYER_NR
				FROM
					TA_LOCASTOCKP TLP,
					MA_SKU MSK
				WHERE
					TLP.SKU_CD = MSK.SKU_CD
			UNION ALL
			SELECT
				TLC.LOCA_CD							,
				TLC.SKU_CD							,
				TLC.KANBAN_CD 						,
				TLC.LOT_TX 							,
				TLC.PALLETCAPACITY_NR				,
				SUM(TLC.PALLET_NR) AS TOTALPALLET_NR,
				SUM(TLC.CASE_NR)   AS TOTALCASE_NR	,
				SUM(TLC.BARA_NR)   AS TOTALBARA_NR	,
				SUM(TLC.PCS_NR)    AS TOTALPCS_NR	,
				0                  AS PPALLET_NR	,
				0                  AS PCASE_NR		,
				0                  AS PBARA_NR		,
				0                  AS PPCS_NR		,
				SUM(TLC.PALLET_NR) AS CPALLET_NR	,
				SUM(TLC.CASE_NR)   AS CCASE_NR		,
				SUM(TLC.BARA_NR)   AS CBARA_NR		,
				SUM(TLC.PCS_NR)    AS CPCS_NR		,
				TLC.TYPESTOCK_CD 					,
				TLC.STSTOCK_ID 						,
				MSK.SKU_TX 							,
				MSK.SKUBARCODE_TX 					,
				MSK.ITFCODE_TX 						,
				MSK.PCSPERCASE_NR AS AMOUNT_NR 		,
				MSK.EXTERIOR_CD 					,
				MSK.SIZE_TX 						,
				TLC.CASEPERPALLET_NR 				,
				TLC.PALLETLAYER_NR
			FROM TA_LOCASTOCKC TLC,
				MA_SKU MSK
			WHERE
				TLC.SKU_CD		= MSK.SKU_CD					AND
				TLC.STSTOCK_ID	<= PS_DEFINE.ST_STOCK19_PICKE
			GROUP BY
				TLC.LOCA_CD 			,
				TLC.SKU_CD 				,
				TLC.KANBAN_CD 			,
				TLC.LOT_TX 				,
				TLC.PALLETCAPACITY_NR	,
				TLC.TYPESTOCK_CD 		,
				TLC.STSTOCK_ID 			,
				MSK.SKU_TX 				,
				MSK.SKUBARCODE_TX 		,
				MSK.ITFCODE_TX 			,
				MSK.PCSPERCASE_NR 		,
				MSK.EXTERIOR_CD 		,
				MSK.SIZE_TX 			,
				TLC.CASEPERPALLET_NR	,
				TLC.PALLETLAYER_NR 		,
				MSK.EXTERIOR_CD 		,
				MSK.SIZE_TX
			) TLS				,
			MB_TYPESTOCK MTS	,
			MB_STSTOCK MSS
		WHERE
			TLS.TYPESTOCK_CD	= MTS.TYPESTOCK_CD(+)	AND
			TLS.STSTOCK_ID		= MSS.STSTOCK_ID(+)
		GROUP BY
			TLS.LOCA_CD 		,
			TLS.SKU_CD 			,
			TLS.KANBAN_CD 		,
			TLS.LOT_TX 			,
			TLS.PALLETCAPACITY_NR	,
			TLS.TYPESTOCK_CD 	,
			MTS.TYPESTOCK_TX 	,
			TLS.STSTOCK_ID 		,
			MSS.STSTOCK_TX 		,
			TLS.SKU_TX 			,
			TLS.SKUBARCODE_TX 	,
			TLS.ITFCODE_TX 		,
			TLS.AMOUNT_NR 		,
			TLS.CASEPERPALLET_NR,
			TLS.PALLETLAYER_NR 	,
			TLS.EXTERIOR_CD 	,
			TLS.SIZE_TX
			) LS2
		FULL OUTER JOIN MA_LOCA MLC
			ON LS2.LOCA_CD = MLC.LOCA_CD
		LEFT OUTER JOIN MB_TYPELOCA MTL
			ON MLC.TYPELOCA_CD = MTL.TYPELOCA_CD
		LEFT OUTER JOIN MB_TYPELOCABLOCK MTB
			ON MLC.TYPELOCABLOCK_CD = MTB.TYPELOCABLOCK_CD
		LEFT OUTER JOIN MB_SHIPVOLUME MBSH
			ON MLC.SHIPVOLUME_CD = MBSH.SHIPVOLUME_CD
		LEFT OUTER JOIN
			(SELECT
					LOCA_CD
				FROM
					TA_LOCASTOCKC
				WHERE
					STSTOCK_ID > PS_DEFINE.ST_STOCK19_PICKE --移動先ロケを探す
				GROUP BY
					LOCA_CD
			) TMP
			ON MLC.LOCA_CD = TMP.LOCA_CD
		LEFT OUTER JOIN MA_ABC MAABC
			ON MLC.ABC_ID = MAABC.ABC_ID
		LEFT OUTER JOIN MA_ABCSUB MASUB
			ON MLC.ABCSUB_ID = MASUB.ABCSUB_ID
		ORDER BY
			MLC.LOCA_CD				
		)
	) ;
		rowTmp curTmp%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		DELETE TA_STOCKTAKING;		
		OPEN curTmp;
		LOOP
			FETCH curTmp INTO rowTmp;
			EXIT WHEN curTmp%NOTFOUND;
				INSERT INTO TA_STOCKTAKING (
					STOCKTAKING_ID		,
					MANAGER_NUM			,
					LOCA_CD				,
					KANBAN_CD			,
					EXTERIOR_CD			,
					SKU_TX				,
					SIZE_TX				,	
					LOT_TX				,
					PALLETCAPACITY_NR	,
					PALLET_NR			,
					CASE_NR				,
					BARA_NR				
--					PCS_NR				
--					MEMO_TX				,			
--					NEEDSEND_FL			,
--					SENDEND_FL			,
					-- 管理項目
--					UPD_DT				,
--					UPDUSER_CD			,
--					UPDUSER_TX			,
--					ADD_DT				,
--					ADDUSER_CD			,
--					ADDUSER_TX			,
--					UPDPROGRAM_CD		,
--					UPDCOUNTER_NR		,
--					STRECORD_ID
				) VALUES (
					rowTmp.STOCKTAKING_ID		,
					rowTmp.MANAGER_NUM			,
					rowTmp.LOCA_CD				,
					rowTmp.KANBAN_CD			,
					rowTmp.EXTERIOR_CD			,
					rowTmp.SKU_TX				,
					rowTmp.SIZE_TX				,
					rowTmp.LOT_TX				,
					rowTmp.PALLETCAPACITY_NR	,
					rowTmp.PALLET_NR			,
					rowTmp.CASE_NR				,
					rowTmp.BARA_NR				
--					rowTmp.PCS_NR				
					-- 管理項目
--					rowTmp.UPD_DT				,
--					rowTmp.UPDUSER_CD			,
--					rowTmp.UPDUSER_TX			,
--					rowTmp.ADD_DT				,
--					rowTmp.ADDUSER_CD			,
--					rowTmp.ADDUSER_TX			,
--					rowTmp.UPDPROGRAM_CD		,
--					rowTmp.UPDCOUNTER_NR		,
--					rowTmp.STRECORD_ID
				);
		END LOOP;
		CLOSE curTmp;		
		COMMIT;
				PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
			EXCEPTION
				WHEN OTHERS THEN
					IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
						PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					ELSE
						PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					END IF;
					RAISE;
			END StocktakingAll;

	/************************************************************************************/
	/*	ステータス更新																	*/
	/************************************************************************************/
	PROCEDURE UpdateStatus(
		--	iPalletDetNoCD	IN VARCHAR2	,
		iLoca_CD			IN VARCHAR2,	
		iOperator_TX		IN VARCHAR2,
		iProgramCD			IN VARCHAR2,
		iResult_TX			IN VARCHAR2,
		iStatus_TX			IN VARCHAR2
--		iSend_DT			IN Date
		--		iProgramCD		IN VARCHAR2
	 ) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdateStatus';		
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
	-- マスタ取得
	--	PS_MASTER.GetUser(iUserCD, rowUser);
	
				UPDATE TA_STOCKTAKING
				SET
					STATUS_TX		= iStatus_TX,
					OPERATOR_TX		= iOperator_TX,
					RESULT_TX		= iResult_TX,
					SEND_DT			= SYSDATE
				WHERE
					LOCA_CD  = iLoca_CD;
		COMMIT;
				PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
			EXCEPTION
				WHEN OTHERS THEN
					IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
						PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					ELSE
						PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					END IF;
					RAISE;
			END UpdateStatus;

			
	/************************************************************************************/
	/*	ステータス更新																	*/
	/************************************************************************************/
	PROCEDURE UpdateStatusAuthorizer(
--		iLoca_CD			IN VARCHAR2,	
		iAuthorizer_TX		IN VARCHAR2,	--承認者
		iProgramCD			IN VARCHAR2,
		iStatus_TX			IN VARCHAR2
--		iSend_DT			IN VARCHAR2		--承認日時
	 ) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdateStatusAuthorizer';		
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
	-- マスタ取得
	--	PS_MASTER.GetUser(iUserCD, rowUser);				
				UPDATE TA_STOCKTAKING
				SET
					STATUS_TX			= iStatus_TX,
					AUTHORIZER_TX		= iAuthorizer_TX,
					SEND_DT				= SYSDATE;
--				WHERE
--					LOCA_CD  = iLoca_CD;
		COMMIT;
				PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
			EXCEPTION
				WHEN OTHERS THEN
					IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
						PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					ELSE
						PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					END IF;
					RAISE;
			END UpdateStatusAuthorizer;			
			
	/************************************************************************************/
	/*	棚卸データを棚卸保存テーブルに移動																	*/
	/************************************************************************************/
	PROCEDURE MoveStocktaking(
--		iLoca_CD			IN VARCHAR2,	
		iAuthorizer_TX		IN VARCHAR2,	--承認者
		iProgramCD			IN VARCHAR2,
		iStatus_TX			IN VARCHAR2
--		iSend_DT			IN VARCHAR2		--承認日時
	 ) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'MoveStocktaking';		
		BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
			UPDATE TA_STOCKTAKING
			SET
				STATUS_TX			= iStatus_TX,
				AUTHORIZER_TX		= iAuthorizer_TX,
				SEND_DT				= SYSDATE;
			INSERT INTO TA_SAVESTOCKTAKING (
				STOCKTAKING_ID				,		
				MANAGER_NUM					,
				LOCA_CD						,
				KANBAN_CD					,
				EXTERIOR_CD     			,
				SKU_TX						,
				SIZE_TX						,
				LOT_TX						,
				PALLET_NR					,
				CASE_NR						,
				BARA_NR						,
				PCS_NR						
			)
			SELECT
				TST.STOCKTAKING_ID				,		
				TST.MANAGER_NUM					,
				TST.LOCA_CD						,
				TST.KANBAN_CD					,
				TST.EXTERIOR_CD     			,
				TST.SKU_TX						,
				TST.SIZE_TX						,
				TST.LOT_TX						,
				TST.PALLET_NR					,
				TST.CASE_NR						,
				TST.BARA_NR						,
				TST.PCS_NR
				FROM
				TA_STOCKTAKING TST
				ORDER BY
				TST.STOCKTAKING_ID;		
		DELETE TA_STOCKTAKING;						
		COMMIT;
				PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
			EXCEPTION
				WHEN OTHERS THEN
					IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
						PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					ELSE
						PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					END IF;
					RAISE;
			END MoveStocktaking;			
END PW_TAKING;
/
SHOW ERRORS
