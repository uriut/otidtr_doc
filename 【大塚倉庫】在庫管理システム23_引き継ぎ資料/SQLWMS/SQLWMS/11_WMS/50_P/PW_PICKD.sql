-- Copyright (C) Seaos Corporation 2014 All rights reserved
CREATE OR REPLACE PACKAGE BODY PW_PICK AS
	/************************************************************************************/
	/*	進捗率算出(パレット成りピック用)												*/
	/************************************************************************************/
	PROCEDURE ChkProgressRatio(
		iTransporterCD	IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2,
		oGroupCompNO	OUT NUMBER,
		oGroupTotalNO	OUT NUMBER,
		oAllCompNO		OUT NUMBER,
		oAllTotalNO		OUT NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ChkProgressRatio';

		CURSOR curSpecifiedGroupUnstarted(TransporterCD TA_PICK.TRANSPORTER_CD%TYPE) IS
			SELECT
				SUM(pallet_nos.pallet_no)
			FROM
				(SELECT
					SUM(PALLET_NR) + TRUNC(SUM(CASE_NR)/PALLETCAPACITY_NR) AS pallet_no
				FROM
					TA_PICK
				WHERE
					STPICK_ID = 0					AND
					--ADD_DT 	 >= TRUNC(SYSDATE) 		AND
					ADD_DT >= NVL((SELECT
									TRUNC(MIN(ADD_DT))
								FROM
									TA_PICK
								WHERE
									STPICK_ID = 0					AND
									TRANSPORTER_CD = TransporterCD	AND
									TYPEPICK_CD IN ( PS_DEFINE.TYPE_PICK_PALLET_N, PS_DEFINE.TYPE_PICK_PALLET_S )),TRUNC(SYSDATE)) AND
					TRANSPORTER_CD = TransporterCD	AND
					TYPEPICK_CD IN ( PS_DEFINE.TYPE_PICK_PALLET_N, PS_DEFINE.TYPE_PICK_PALLET_S )
				GROUP BY
					PALLETCAPACITY_NR, PALLETDETNO_CD) pallet_nos;

		numSpecifiedGroupUnstarted	NUMBER;

		CURSOR curSpecifiedGroupTotal(TransporterCD TA_PICK.TRANSPORTER_CD%TYPE) IS
			SELECT
				SUM(pallet_nos.pallet_no)
			FROM
				(SELECT
					SUM(PALLET_NR) + TRUNC(SUM(CASE_NR)/PALLETCAPACITY_NR) AS pallet_no
				FROM
					TA_PICK
				WHERE
					--ADD_DT 	 >= TRUNC(SYSDATE) 		AND
					ADD_DT >= NVL((SELECT
									TRUNC(MIN(ADD_DT))
								FROM
									TA_PICK
								WHERE
									STPICK_ID = 0					AND
									TRANSPORTER_CD = TransporterCD	AND
									TYPEPICK_CD IN ( PS_DEFINE.TYPE_PICK_PALLET_N, PS_DEFINE.TYPE_PICK_PALLET_S )),TRUNC(SYSDATE)) AND
					TRANSPORTER_CD = TransporterCD	AND
					TYPEPICK_CD IN ( PS_DEFINE.TYPE_PICK_PALLET_N, PS_DEFINE.TYPE_PICK_PALLET_S )
				GROUP BY
					PALLETCAPACITY_NR, PALLETDETNO_CD) pallet_nos;

		numSpecifiedGroupTotal		NUMBER;

		CURSOR curAllGroupsUnstarted IS
			SELECT
				SUM(pallet_nos.pallet_no)
			FROM
				(SELECT
					SUM(PALLET_NR) + TRUNC(SUM(CASE_NR)/PALLETCAPACITY_NR) AS pallet_no
				FROM
					TA_PICK
				WHERE
					STPICK_ID = 0					AND
					--ADD_DT 	 >= TRUNC(SYSDATE) 		AND
					ADD_DT >= NVL((SELECT
									TRUNC(MIN(ADD_DT))
								FROM
									TA_PICK
								WHERE
									STPICK_ID = 0					AND
									TYPEPICK_CD IN ( PS_DEFINE.TYPE_PICK_PALLET_N, PS_DEFINE.TYPE_PICK_PALLET_S )),TRUNC(SYSDATE)) AND
					TYPEPICK_CD IN ( PS_DEFINE.TYPE_PICK_PALLET_N, PS_DEFINE.TYPE_PICK_PALLET_S )
				GROUP BY
					PALLETCAPACITY_NR, PALLETDETNO_CD) pallet_nos;

		numAllGroupsUnstarted		NUMBER;

		CURSOR curAllGroupsTotal IS
			SELECT
				SUM(pallet_nos.pallet_no)
			FROM
				(SELECT
					SUM(PALLET_NR) + TRUNC(SUM(CASE_NR)/PALLETCAPACITY_NR) AS pallet_no
				FROM
					TA_PICK
				WHERE
					--ADD_DT 	 >= TRUNC(SYSDATE) 		AND
					ADD_DT >= NVL((SELECT
									TRUNC(MIN(ADD_DT))
								FROM
									TA_PICK
								WHERE
									STPICK_ID = 0					AND
									TYPEPICK_CD IN ( PS_DEFINE.TYPE_PICK_PALLET_N, PS_DEFINE.TYPE_PICK_PALLET_S )),TRUNC(SYSDATE)) AND
					TYPEPICK_CD IN ( PS_DEFINE.TYPE_PICK_PALLET_N, PS_DEFINE.TYPE_PICK_PALLET_S )
				GROUP BY
					PALLETCAPACITY_NR, PALLETDETNO_CD) pallet_nos;

		numAllGroupsTotal			NUMBER;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');

		-- 該当グループ毎進捗率算出
		OPEN curSpecifiedGroupUnstarted(iTransporterCD);
		FETCH curSpecifiedGroupUnstarted INTO numSpecifiedGroupUnstarted;
		IF numSpecifiedGroupUnstarted IS NULL THEN
			numSpecifiedGroupUnstarted := 0;
		END IF;
		CLOSE curSpecifiedGroupUnstarted;

		OPEN curSpecifiedGroupTotal(iTransporterCD);
		FETCH curSpecifiedGroupTotal INTO numSpecifiedGroupTotal;
		IF numSpecifiedGroupTotal IS NULL THEN
			numSpecifiedGroupTotal := 0;
		END IF;
		CLOSE curSpecifiedGroupTotal;

		oGroupCompNO  := numSpecifiedGroupTotal - numSpecifiedGroupUnstarted;
		oGroupTotalNO := numSpecifiedGroupTotal;

		-- 全体進捗率算出
		OPEN curAllGroupsUnstarted();
		FETCH curAllGroupsUnstarted INTO numAllGroupsUnstarted;
		IF numAllGroupsUnstarted IS NULL THEN
			numAllGroupsUnstarted := 0;
		END IF;
		CLOSE curAllGroupsUnstarted;

		OPEN curAllGroupsTotal();
		FETCH curAllGroupsTotal INTO numAllGroupsTotal;
		IF numAllGroupsTotal IS NULL THEN
			numAllGroupsTotal := 0;
		END IF;
		CLOSE curAllGroupsTotal;

		oAllCompNO  := numAllGroupsTotal - numAllGroupsUnstarted;
		oAllTotalNO := numAllGroupsTotal;

		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curSpecifiedGroupUnstarted%ISOPEN THEN CLOSE curSpecifiedGroupUnstarted; END IF;
			IF curSpecifiedGroupTotal%ISOPEN THEN CLOSE curSpecifiedGroupTotal; END IF;
			IF curAllGroupsUnstarted%ISOPEN THEN CLOSE curAllGroupsUnstarted; END IF;
			IF curAllGroupsTotal%ISOPEN THEN CLOSE curAllGroupsTotal; END IF;
			RAISE;
	END ChkProgressRatio;
	/************************************************************************************/
	/*	２ＰＬピック確認(パレット成りピック用)											*/
	/************************************************************************************/
	PROCEDURE ChkPalletPick(
		iPalletdetnoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2 ,
		oPalletNum		OUT	NUMBER  , --2:2PLピック可, 1:1PLピックのみ, 0:ピック済み
		oPalletdetnoCD	OUT	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ChkPalletPick';

		CURSOR curPick(PalletdetnoCD TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				STPICK_ID,
				KANBAN_CD,
				TRANSPORTER_CD,
				SRCLOCA_CD
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = PalletdetnoCD;

		rowPick			curPick%ROWTYPE;

		CURSOR curPallet(PalletdetnoCD TA_PICK.PALLETDETNO_CD%TYPE,
						 KanbanCD	   TA_PICK.KANBAN_CD%TYPE,
						 TransporterCD TA_PICK.TRANSPORTER_CD%TYPE,
						 SrcLocaCD	   TA_PICK.SRCLOCA_CD%TYPE) IS
			SELECT             
				PALLETDETNO_CD 
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD <> PalletdetnoCD				AND
				KANBAN_CD	   = KanbanCD					AND
				TRANSPORTER_CD = TransporterCD				AND
				SRCLOCA_CD	   = SrcLocaCD					AND
				START_DT	   IS NULL						AND --ロックされていないもの
				STPICK_ID	   = PS_DEFINE.ST_PICK0_PRINTNG AND
				TYPEPICK_CD IN ( PS_DEFINE.TYPE_PICK_PALLET_N, PS_DEFINE.TYPE_PICK_PALLET_S )
			ORDER BY
				PALLETDETNO_CD;

		rowPallet		curPallet%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');

		oPalletNum := -1; --初期値

		OPEN curPick(iPalletdetnoCD);
		FETCH curPick INTO rowPick;
		IF curPick%FOUND THEN
			--ステータスチェック
			IF rowPick.STPICK_ID = PS_DEFINE.ST_PICK0_PRINTNG THEN
				--もうひとつの指示を取得
				OPEN curPallet(iPalletdetnoCD, rowPick.KANBAN_CD, rowPick.TRANSPORTER_CD, rowPick.SRCLOCA_CD);
				FETCH curPallet INTO rowPallet;
				IF curPallet%FOUND THEN
					--２パレ可能
					oPalletNum := 2;
					oPalletdetnoCD := rowPallet.PALLETDETNO_CD;
				ELSE
					--１パレのみ
					oPalletNum := 1;
					oPalletdetnoCD := '';
				END IF;
				CLOSE curPallet;
			ELSE
				--完了済み
				oPalletNum 	   := 0;
				oPalletdetnoCD := '';
			END IF;
		END IF;
		CLOSE curPick;

		--指示書存在チェック
		IF oPalletNum < 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
															 '指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;

		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			IF curPallet%ISOPEN THEN CLOSE curPallet; END IF;
			RAISE;
	END ChkPalletPick;
	/************************************************************************************/
	/*	補充元ピック完了(パレット成りピック用)											*/
	/************************************************************************************/
	PROCEDURE SetPrintOK(
		iPalletdetnoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SetPrintOK';
		-- 完パレピック指示
		CURSOR curPick(PalletdetnoCD	TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				PALLETDETNO_CD		,
				TYPEPICK_CD			,
				STPICK_ID			,
				SRCLOCA_CD			
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = PalletdetnoCD
			;
		rowPick			curPick%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		OPEN curPick(iPalletdetnoCD);
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK1_PRINTOK THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が印刷済みです。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完パレピックチェック
		IF (rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_N AND rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_S )THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレットピックの指示書ではありません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		CLOSE curPick;
		-- ピック指示更新
		UPDATE TA_PICK
		SET
			STPICK_ID		= PS_DEFINE.ST_PICK1_PRINTOK,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD = iPalletdetnoCD;
		
		-- パレット指示番号のロケーションに紐付く他の完パレ指示書の開始を解除する
		UPDATE TA_PICK
		SET
			START_DT		= Null
		WHERE
			SRCLOCA_CD = rowPick.SRCLOCA_CD		AND
			PALLETDETNO_CD <> iPalletdetnoCD	AND
			STPICK_ID	= PS_DEFINE.ST_PICK0_PRINTNG			AND
			(TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_N OR TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_S)
		;
		
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END SetPrintOK;
	/************************************************************************************/
	/*	補充元ピック完了(パレット成りピック用)２パレ用									*/
	/************************************************************************************/
	PROCEDURE SetPrintOK_2PL(
		iPalletdetnoCD	IN VARCHAR2	,
		iPalletdetnoCD2	IN VARCHAR2 ,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SetPrintOK_2PL';
		-- 完パレピック指示
		CURSOR curPick(PalletdetnoCD	TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				PALLETDETNO_CD		,
				TYPEPICK_CD			,
				STPICK_ID			,
				SRCLOCA_CD			
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = PalletdetnoCD
			;
		rowPick			curPick%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		OPEN curPick(iPalletdetnoCD);
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK1_PRINTOK THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が印刷済みです。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完パレピックチェック
		IF (rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_N AND rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_S )THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレットピックの指示書ではありません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		CLOSE curPick;
		-- ピック指示更新
		UPDATE TA_PICK
		SET
			STPICK_ID		= PS_DEFINE.ST_PICK1_PRINTOK,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD = iPalletdetnoCD;

		OPEN curPick(iPalletdetnoCD2); --２パレ目
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK1_PRINTOK THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が印刷済みです。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完パレピックチェック
		IF (rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_N AND rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_S )THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレットピックの指示書ではありません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		CLOSE curPick;
		-- ピック指示更新
		UPDATE TA_PICK
		SET
			STPICK_ID		= PS_DEFINE.ST_PICK1_PRINTOK,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD = iPalletdetnoCD2; --２パレ目
		
		-- パレット指示番号のロケーションに紐付く他の完パレ指示書の開始を解除する
		UPDATE TA_PICK
		SET
			START_DT		= Null
		WHERE
			SRCLOCA_CD = rowPick.SRCLOCA_CD		AND
			PALLETDETNO_CD NOT IN ( iPalletdetnoCD, iPalletdetnoCD2 ) AND --１パレ２パレ以外
			--PALLETDETNO_CD <> iPalletdetnoCD	AND
			STPICK_ID	= PS_DEFINE.ST_PICK0_PRINTNG			AND
			(TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_N OR TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_S)
		;
		
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END SetPrintOK_2PL;

	/************************************************************************************/
	/*	補充元ピック戻し(パレット成りピック用)エラー時									*/
	/************************************************************************************/
--	PROCEDURE RstPrintOK(
--		iPalletdetnoCD	IN VARCHAR2	,
--		iUserCD			IN VARCHAR2	,
--		iProgramCD		IN VARCHAR2
--	) AS
--		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstPrintOK';
--		rowUser			VA_USER%ROWTYPE;
--	BEGIN
--		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
--
--		-- マスタ取得
--		PS_MASTER.GetUser(iUserCD, rowUser);
--
--		-- ピック指示更新
--		UPDATE TA_PICK
--		SET
--			STPICK_ID		= PS_DEFINE.ST_PICK0_PRINTNG, --戻す
--			UPD_DT			= SYSDATE			,
--			UPDUSER_CD		= iUserCD			,
--			UPDUSER_TX		= rowUser.USER_TX	,
--			UPDPROGRAM_CD	= iProgramCD		,
--			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
--		WHERE
--			PALLETDETNO_CD = iPalletdetnoCD;
--
--		COMMIT;
--		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
--	EXCEPTION
--		WHEN OTHERS THEN
--			ROLLBACK;
--			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
--				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
--			ELSE
--				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
--			END IF;
--			RAISE;
--	END RstPrintOK;
	/************************************************************************************/
	/*	補充元ピック開始(パレット成りピック用-ロケ単位)									*/
	/************************************************************************************/
	PROCEDURE StartPalletPick(
		iPalletdetnoCD	IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'StartPalletPick';
		-- 完パレピック指示
		CURSOR curPick(PalletdetnoCD	TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				PALLETDETNO_CD		,
				TYPEPICK_CD			,
				STPICK_ID			,
				SRCLOCA_CD			
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = PalletdetnoCD
			;
		rowPick			curPick%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		OPEN curPick(iPalletdetnoCD);
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK1_PRINTOK THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が印刷済みです。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完パレピックチェック
		IF (rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_N AND rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_S )THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレットピックの指示書ではありません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		CLOSE curPick;
		
		-- パレット明細番号に紐付くロケーションに関するピック指示を開始する
		-- ピックステータス=未補充, ピックタイプ=パレット単位を条件とする
		-- ピック指示の開始時間を更新
		UPDATE TA_PICK
		SET
			START_DT		= SYSDATE			,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			SRCLOCA_CD 	= rowPick.SRCLOCA_CD					AND
			STPICK_ID	= PS_DEFINE.ST_PICK0_PRINTNG			AND
			(TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_N OR TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_S)
			;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END StartPalletPick;
	/************************************************************************************/
	/*	補充保留																		*/
	/************************************************************************************/
	PROCEDURE DeferPick(
		iPalletdetnoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SetPrintOK';
		-- 完パレピック指示
		CURSOR curPick(PalletdetnoCD	TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				PALLETDETNO_CD		,
				TYPEPICK_CD			,
				STPICK_ID			
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = PalletdetnoCD
			;
		rowPick			curPick%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		OPEN curPick(iPalletdetnoCD);
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了済みです。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完パレピックチェック
		IF (rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_N AND rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_S )THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレットピックの指示書ではありません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		CLOSE curPick;
		UPDATE TA_PICK
		SET
			DEFER_FL		= 1					,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD = iPalletdetnoCD;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END DeferPick;
	/************************************************************************************/
	/*	補充保留 ２パレ用																*/
	/************************************************************************************/
	PROCEDURE DeferPick_2PL(
		iPalletdetnoCD	IN VARCHAR2	,
		iPalletdetnoCD2	IN VARCHAR2 ,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SetPrintOK_2PL';
		-- 完パレピック指示
		CURSOR curPick(PalletdetnoCD	TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				PALLETDETNO_CD		,
				TYPEPICK_CD			,
				STPICK_ID			
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = PalletdetnoCD
			;
		rowPick			curPick%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		OPEN curPick(iPalletdetnoCD);
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了済みです。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完パレピックチェック
		IF (rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_N AND rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_S )THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレットピックの指示書ではありません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		CLOSE curPick;
		UPDATE TA_PICK
		SET
			DEFER_FL		= 1					,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD = iPalletdetnoCD;

		OPEN curPick(iPalletdetnoCD2); --２パレ目
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了済みです。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完パレピックチェック
		IF (rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_N AND rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_S )THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレットピックの指示書ではありません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		CLOSE curPick;
		UPDATE TA_PICK
		SET
			DEFER_FL		= 1					,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD = iPalletdetnoCD2; --２パレ目

		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END DeferPick_2PL;
	/************************************************************************************/
	/*	補充保留解除																	*/
	/************************************************************************************/
	PROCEDURE CancelDeferPick(
		iPalletdetnoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'SetPrintOK';
		-- 完パレピック指示
		CURSOR curPick(PalletdetnoCD	TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				PALLETDETNO_CD		,
				TYPEPICK_CD			,
				MIN(STPICK_ID) AS STPICK_ID			
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = PalletdetnoCD
			GROUP BY
				PALLETDETNO_CD		,
				TYPEPICK_CD			;			
		
		rowPick			curPick%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		OPEN curPick(iPalletdetnoCD);
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.STPICK_ID >= PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了済みです。' ||
																'指示書番号 = [' || iPalletdetnoCD || ']');
		END IF;
		-- 完パレピックチェック
		--IF (rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_N AND rowPick.TYPEPICK_CD <> PS_DEFINE.TYPE_PICK_PALLET_S )THEN
		--	RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレットピックの指示書ではありません。' ||
		--														'指示書番号 = [' || iPalletdetnoCD || ']');
		--END IF;
		CLOSE curPick;
		UPDATE TA_PICK
		SET
			DEFER_FL		= 0					,
			START_DT		= NULL				,
			END_DT			= NULL				, --不具合のためリセット
			SRCSTART_DT		= NULL				,
			SRCHT_ID		= 0					,
			SRCHTUSER_CD	= ' '				,
			SRCHTUSER_TX	= ' '				,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD = iPalletdetnoCD;

		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END CancelDeferPick;
	/************************************************************************************/
	/*	指示書再印刷																	*/
	/************************************************************************************/
	PROCEDURE RePrint(
		iTrnID			IN NUMBER	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RePrint';
		-- 
		CURSOR curPick(TrnID	TMP_SELECTCD.TRN_ID%TYPE) IS
			SELECT
				TP.PALLETDETNO_CD		,
				MIN(TP.STPICK_ID) AS STPICK_ID
			FROM
				TA_PICK TP
					INNER JOIN TMP_SELECTCD TMP
						ON TP.PALLETDETNO_CD = TMP.SELECT_CD
			WHERE
				TMP.TRN_ID	= TrnID
			GROUP BY
				TP.PALLETDETNO_CD
			ORDER BY
				TP.PALLETDETNO_CD
			;
		rowPick			curPick%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		OPEN curPick(iTrnID);
		LOOP
			FETCH curPick INTO rowPick;
			EXIT WHEN curPick%NOTFOUND;
			-- キャンセルチェック
			IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																	'指示書番号 = [' || rowPick.PALLETDETNO_CD || ']');
			END IF;
			-- ステータスチェック
			IF rowPick.STPICK_ID < PS_DEFINE.ST_PICK2_PRINT_E THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が印刷されていません。' ||
																	'指示書番号 = [' || rowPick.PALLETDETNO_CD || ']');
			END IF;
			-- 印刷フラグ更新
			UPDATE TA_PICK
			SET
				PRINT_FL		= 1					,
				UPD_DT			= SYSDATE			,
				UPDUSER_CD		= iUserCD			,
				UPDUSER_TX		= rowUser.USER_TX	,
				UPDPROGRAM_CD	= iProgramCD		,
				UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
			WHERE
				PALLETDETNO_CD = rowPick.PALLETDETNO_CD;
		END LOOP;
		CLOSE curPick;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END RePrint;
	/************************************************************************************/
	/*	iPad紐づき解除																	*/
	/************************************************************************************/
	PROCEDURE RstiPad(
		iPalletDetNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstiPad';
		-- ピック指示
		CURSOR curPick(PalletDetNoCD	TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				MIN(STPICK_ID) AS STPICK_ID		,
				MAX(SRCSTART_DT) AS SRCSTART_DT	,
				MAX(SRCEND_DT) AS SRCEND_DT		,
				TYPEPICK_CD
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = PalletDetNoCD
			GROUP BY
				TYPEPICK_CD;
		rowPick			curPick%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
		
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		-- ピック指示チェック
		OPEN curPick(iPalletDetNoCD);
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iPalletDetNoCD || ']');
		END IF;
		CLOSE curPick;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iPalletDetNoCD || ']');
		END IF;
		-- 印刷完了
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICK2_PRINT_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が印刷されています。' ||
																'指示書番号 = [' || iPalletDetNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。' ||
																'指示書番号 = [' || iPalletDetNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.SRCEND_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。' ||
																'指示書番号 = [' || iPalletDetNoCD || ']');
		END IF;
		IF rowPick.TYPEPICK_CD NOT IN (PS_DEFINE.TYPE_PICK_PALLET_N, PS_DEFINE.TYPE_PICK_PALLET_S) THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'パレット成りの指示書ではありません。' ||
																'指示書番号 = [' || iPalletDetNoCD || ']');
		END IF;

		-- 紐づき解除
		UPDATE
			TA_PICK
		SET
			STPICK_ID		= PS_DEFINE.ST_PICK0_PRINTNG,
			START_DT		= NULL				,
			END_DT			= NULL				,
			SRCSTART_DT		= NULL				,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			(STPICK_ID = PS_DEFINE.ST_PICK0_PRINTNG OR STPICK_ID = PS_DEFINE.ST_PICK1_PRINTOK) AND
			(TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_N OR TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_S) AND
			SRCLOCA_CD IN (
				SELECT
					SRCLOCA_CD
				FROM
					TA_PICK
				WHERE
					PALLETDETNO_CD	= iPalletDetNoCD);

/*
		UPDATE
			TA_PICK
		SET
			STPICK_ID		= PS_DEFINE.ST_PICK0_PRINTNG,
			START_DT		= NULL				,
			SRCSTART_DT		= NULL				,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD	= iPalletDetNoCD;
*/
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END RstiPad;
	/************************************************************************************/
	/*	ハンディ紐づき解除																*/
	/************************************************************************************/
	PROCEDURE RstHt(
		iPalletDetNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstHt';
		-- ピック指示
		CURSOR curPick(PalletDetNoCD	TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				MIN(STPICK_ID) AS STPICK_ID		,
				MAX(SRCSTART_DT) AS SRCSTART_DT	,
				MAX(SRCEND_DT) AS SRCEND_DT		,
				TYPEPICK_CD
			FROM
				TA_PICK
			WHERE
				PALLETDETNO_CD = PalletDetNoCD
			GROUP BY
				TYPEPICK_CD;
		rowPick			curPick%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
		
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		-- ピック指示チェック
		OPEN curPick(iPalletDetNoCD);
		FETCH curPick INTO rowPick;
		IF curPick%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iPalletDetNoCD || ']');
		END IF;
		CLOSE curPick;
		-- キャンセルチェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iPalletDetNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.STPICK_ID = PS_DEFINE.ST_PICK4_PICKOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。' ||
																'指示書番号 = [' || iPalletDetNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowPick.SRCEND_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。' ||
																'指示書番号 = [' || iPalletDetNoCD || ']');
		END IF;
		-- 紐づき解除
		UPDATE
			TA_PICK
		SET
			STPICK_ID		= PS_DEFINE.ST_PICK2_PRINT_E,
			START_DT		= NULL				,
			END_DT			= NULL				,
			SRCSTART_DT		= NULL				,
			SRCHT_ID		= 0					,
			SRCHTUSER_CD	= ' '				,
			SRCHTUSER_TX	= ' '				,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			PALLETDETNO_CD	= iPalletDetNoCD;
		COMMIT;
		
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			RAISE;
	END RstHt;
END PW_PICK;
/
SHOW ERRORS
