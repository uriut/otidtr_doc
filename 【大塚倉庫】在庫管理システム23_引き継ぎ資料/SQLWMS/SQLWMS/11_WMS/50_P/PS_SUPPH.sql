-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_SUPP AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_SUPP';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************************************/
	/*	補充データ作成																	*/
	/************************************************************************************/
	PROCEDURE InsSupp(
		iTrnID			IN	NUMBER	,
		iSuppNoCD		IN	VARCHAR2,
		iTypeSuppCD		IN	VARCHAR2,
		irowPick		IN	TA_PICK%ROWTYPE,
		irowDLocaStockC	IN	TA_LOCASTOCKC%ROWTYPE,
		iUserCD			IN	VARCHAR2,
		iUserTX			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************************************/
	/*	補充データ作成(引当ロケの端数)													*/
	/************************************************************************************/
	PROCEDURE InsSupp2(
		iTrnID			IN	NUMBER	,
		iSuppNoCD		IN	VARCHAR2,
		iTypeSuppCD		IN	VARCHAR2,
		irowPick		IN	TA_PICK%ROWTYPE,
		irowSLocaStockC	IN	TA_LOCASTOCKC%ROWTYPE,
		irowDLocaStockC	IN	TA_LOCASTOCKC%ROWTYPE,
		iUserCD			IN	VARCHAR2,
		iUserTX			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************************************/
	/*	補充指示有効																	*/
	/************************************************************************************/
	PROCEDURE SuppDataOK(
		iTrnID			IN	NUMBER
	);
	/************************************************************************************/
	/*	補充ＰＣＳ取得																	*/
	/************************************************************************************/
	PROCEDURE GetSuppPcs(
		iTypeSuppCD		IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		oPcsNR			OUT	NUMBER
	);
END PS_SUPP;
/
SHOW ERRORS
