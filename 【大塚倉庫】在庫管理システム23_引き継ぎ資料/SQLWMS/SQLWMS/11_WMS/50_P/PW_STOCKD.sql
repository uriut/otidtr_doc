-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PW_STOCK AS
	
	/************************************************************************************/
	/*	在庫追加																		*/
	/************************************************************************************/
	--空きロケへの在庫追加
	PROCEDURE InsStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iCASEPERPALLET_NR	IN NUMBER	,
		iPALLETLAYER_NR		IN NUMBER	,
		iPALLETCAPACITY_NR	IN NUMBER	,
		iSTACKINGNUMBER_NR	IN NUMBER	,
		iAMOUNT_NR			IN NUMBER	,
		iTypeOperationCD	IN VARCHAR2	,
		iMemoTX				IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsStock';
		
		CURSOR curMaLoca(LocaCD	MA_LOCA.LOCA_CD%TYPE) IS
			SELECT
				LOCA_CD		,
				WH_CD		,
				FLOOR_CD	,
				AREA_CD		,
				LINE_CD		,
				SHELF_CD	,
				LAYER_CD	,
				ORDER_CD	,
				TYPELOCA_CD	
			FROM
				MA_LOCA
			WHERE
				LOCA_CD = LocaCD
			;
		rowMaLoca curMaLoca%ROWTYPE;
		rowUser				VA_USER%ROWTYPE;
		rowLoca				MA_LOCA%ROWTYPE;
		rowSku				MA_SKU%ROWTYPE;
		rowTypeStock		MB_TYPESTOCK%ROWTYPE;
		rowTypeOperation	MB_TYPEOPERATION%ROWTYPE;
		numPalletNR			TA_LOCASTOCKP.PALLET_NR%TYPE;
		numCaseNR			TA_LOCASTOCKP.CASE_NR%TYPE;
		numBaraNR			TA_LOCASTOCKP.BARA_NR%TYPE;
		numKanbanPID		TA_KANBANP.KANBANP_ID%TYPE;
		--
		numLineUnitNR		NUMBER := 0;
		numStackingNumberNR	NUMBER := 0;
		numTotalLocaNR		NUMBER := 0;
		numUseLocaNR		NUMBER := 0;
		numEmptyLocaNR		NUMBER := 0;
		numBaraFL			NUMBER := 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		--マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		PS_MASTER.GetLoca(iLocaCD, rowLoca);
		PS_MASTER.GetSku(iSkuCD, rowSku);
		PS_MASTER.GetTypeOperation(iTypeOperationCD, rowTypeOperation);
		-- 業務区分チェック
		-- 在庫追加で選択可能な区分であること
		IF rowTypeOperation.STOCKADD_FL <> 1 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '業務区分が不適切です。');
		END IF;
		-- 符号チェック
		-- 業務区分で指定された符号であること
		IF iPcsNR > 0 THEN
			IF rowTypeOperation.PLUS_FL <> 1 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '数量の符号が不適切です。');
			END IF;
		END IF;
		IF iPcsNR = 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '数量0は指定できません。');
		END IF;
		IF iPcsNR < 0 THEN
			IF rowTypeOperation.MINUS_FL <> 1 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '数量の符号が不適切です。');
			END IF;
		END IF;
		-- TODO 使用期限チェック
		-- 間口取得
		PS_STOCK.GetLocaNR(
			iLocaCD				,
			iSkuCD				,
			numLineUnitNR		,	-- ラインユニット数
			numStackingNumberNR	,	-- 保管段数
			numTotalLocaNR		,	-- 総間口数
			numUseLocaNR		,	-- 使用間口数
			numEmptyLocaNR		,	-- 空き間口数
			numBaraFL				-- バラフラグ(端数フラグ)
			);
		numStackingNumberNR := iSTACKINGNUMBER_NR;
		numTotalLocaNR := numStackingNumberNR * numLineUnitNR;
		numUseLocaNR := 0;
		numEmptyLocaNR := numTotalLocaNR;
		
		-- 間口確認
		IF iPalletNR > numEmptyLocaNR THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーションの間口が足りません。総間口=[ ' || numTotalLocaNR || ' ]、空き間口=[ ' || numEmptyLocaNR || ' ]');
		END IF;
		
		--入庫可能チェック
		PS_STOCK.ChkInStock(
			iLocaCD 		,
			iSkuCD 			,
			PS_DEFINE.USELIMITDT_DEFAULT,	-- todo iUseLimitDT
			' '				,
			TO_DATE(TO_CHAR(SYSDATE,'YYYYMM') || '01','YYYYMMDD'),
			rowSku.PCSPERCASE_NR,
			iCaseNR			,
			iBaraNR			,
			iPcsNR			,
			rowLoca.TYPELOCA_CD,	-- todo iTypeStockCD
			iUserCD			,
			rowUser.USER_TX
		);
		--入庫
		PS_STOCK.InStock(
			iLocaCD 		,
			iSkuCD 			,
			iLotTX			,
			iKanbanCD		,
			PS_DEFINE.USELIMITDT_DEFAULT,	-- todo iUseLimitDT
			' '				,	-- iBatchNoCD
			TO_DATE(TO_CHAR(SYSDATE,'YYYYMM') || '01','YYYYMMDD'),
			iAMOUNT_NR				,
			iPalletNR				,
			iCaseNR					,
			iBaraNR					,
			iPcsNR					,
			iCASEPERPALLET_NR		,
			iPALLETLAYER_NR			,
			iPALLETCAPACITY_NR		,
			iSTACKINGNUMBER_NR		,
			rowLoca.TYPELOCA_CD,	-- todo iTypeStockCD
			rowLoca.TYPELOCA_CD,
			iTypeOperationCD,
			'INOUT'			,
			0				,
			iMemoTx			,
			1				,	--iNeedSendFL
			iUserCD			,
			rowUser.USER_TX	,
			iProgramCD
		);
		
		-- かんばん登録
		-- ID取得
		SELECT
			NVL(MAX(KANBANP_ID),0)
		INTO
			numKanbanPID
		FROM
			TA_KANBANP;
		numKanbanPID := numKanbanPID + 1;
		
		OPEN curMaLoca(iLocaCD);
		FETCH curMaLoca INTO rowMaLoca;
		
		-- 引当済かんばん登録
		INSERT INTO TA_KANBANP (
			KANBANP_ID			,
			STKANBAN_CD			,
			TYPEPKANBAN_CD		,
			KANBAN_CD			,
			WH_CD				,
			FLOOR_CD			,
			AREA_CD				,
			LINE_CD				,
			SKU_CD				,
			LOT_TX				,
			CASEPERPALLET_NR	,
			PALLETLAYER_NR		,
			STACKINGNUMBER_NR	,
			TYPEPLALLET_CD		,
			-- 管理項目
			UPD_DT				,
			UPDUSER_CD			,
			UPDUSER_TX			,
			ADD_DT				,
			ADDUSER_CD			,
			ADDUSER_TX			,
			UPDPROGRAM_CD		,
			UPDCOUNTER_NR		,
			STRECORD_ID			
		) VALUES (
			numKanbanPID				,
			'01'						,
			'00'						,
			iKanbanCD					,
			rowMaLoca.WH_CD				,
			rowMaLoca.FLOOR_CD			,
			rowMaLoca.AREA_CD			,
			rowMaLoca.LINE_CD			,
			iSKUCD						,
			iLotTX						,
			iCASEPERPALLET_NR			,
			iPALLETLAYER_NR				,
			iSTACKINGNUMBER_NR			,
			'01'						,
			--
			SYSDATE						,
			iUserCD						,
			rowUser.USER_TX				,
			SYSDATE						,
			iUserCD						,
			rowUser.USER_TX				,
			FUNCTION_NAME				,
			0							,
			0
		);
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsStock;
	/************************************************************************************/
	/*	在庫修正																		*/
	/************************************************************************************/
	--既存ロケへの在庫修正
	PROCEDURE UpdStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeOperationCD	IN VARCHAR2	,
		iMemoTX				IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2	
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdStock';
		
		rowUser				VA_USER%ROWTYPE;
		rowLoca				MA_LOCA%ROWTYPE;
		rowSku				MA_SKU%ROWTYPE;
		rowTypeOperation	MB_TYPEOPERATION%ROWTYPE;
		rowLocaStockP		TA_LOCASTOCKP%ROWTYPE;
		numPalletNR			TA_LOCASTOCKP.PALLET_NR%TYPE;
		numCaseNR			TA_LOCASTOCKP.CASE_NR%TYPE;
		numBaraNR			TA_LOCASTOCKP.BARA_NR%TYPE;
		numPcsNR			TA_LOCASTOCKP.PCS_NR%TYPE;
		
		numLineUnitNR		NUMBER := 0;
		numStackingNumberNR	NUMBER := 0;
		numTotalLocaNR		NUMBER := 0;
		numUseLocaNR		NUMBER := 0;
		numEmptyLocaNR		NUMBER := 0;
		numBaraFL			NUMBER := 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		PLOG.DEBUG(logCtx, 
					'iLocaCD=[' || iLocaCD || '], ' ||
					'iSkuCD=[' || iSkuCD || '], ' ||
					'iLotTX=[' || iLotTX || '], ' ||
					'iKanbanCD=[' || iKanbanCD || '], ' ||
					'iPalletNR=[' || iPalletNR || '], ' ||
					'iCaseNR=[' || iCaseNR || '], ' ||
					'iBaraNR=[' || iBaraNR || '], ' ||
					'iPcsNR=[' || iPcsNR || '], ' ||
					'iTypeOperationCD=[' || iTypeOperationCD || '], ' ||
					'iMemoTX=[' || iMemoTX || '] '
					);
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD,rowUser);
		PS_MASTER.GetLoca(iLocaCD,rowLoca);
		PS_MASTER.GetSku(iSkuCD,rowSku);
		PS_MASTER.GetTypeOperation(iTypeOperationCD,rowTypeOperation);
		-- 業務区分チェック
		-- 在庫追加で選択可能な区分であること
		IF rowTypeOperation.STOCKUPD_FL <> 1 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '業務区分が不適切です。');
		END IF;
		-- 符号チェック
		-- 業務区分で指定された符号であること
		IF iPcsNR > 0 THEN
			IF rowTypeOperation.PLUS_FL <> 1 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '数量の符号が不適切です。');
			END IF;
		END IF;
		IF iPcsNR = 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '数量0は指定できません。');
		END IF;
		IF iPcsNR < 0 THEN
			IF rowTypeOperation.MINUS_FL <> 1 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '数量の符号が不適切です。');
			END IF;
		END IF;
		-- TODO 使用期限チェック
		PS_LOCA.GetLocaStockP(iLocaCD,iSkuCD,rowLocaStockP);
		-- 数量チェック
		numPcsNR := (iPalletNR * rowLocaStockP.PALLETCAPACITY_NR * rowLocaStockP.AMOUNT_NR) + (iCaseNR * rowLocaStockP.AMOUNT_NR) + iBaraNR;
		IF numPcsNR <> iPcsNR THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'PCS数が不適切です。');
		END IF;
		PS_STOCK.GetUnitPcsNr(
			iSkuCD		,
			iPcsNR		,
			rowLocaStockP.PALLETCAPACITY_NR,
			numPalletNR	,
			numCaseNR	,
			numBaraNR	
			);
		-- 間口取得
		PS_STOCK.GetLocaNR(
			iLocaCD				,
			iSkuCD				,
			numLineUnitNR		,	-- ラインユニット数
			numStackingNumberNR	,	-- 保管段数
			numTotalLocaNR		,	-- 総間口数
			numUseLocaNR		,	-- 使用間口数
			numEmptyLocaNR		,	-- 空き間口数
			numBaraFL				-- バラフラグ(端数フラグ)
			);
		IF numPalletNR > numEmptyLocaNR THEN
			IF rowTypeOperation.MINUS_FL <> 1 THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーションの間口が足りません。総間口=[ ' || numTotalLocaNR || ' ]、空き間口=[ ' || numEmptyLocaNR || ' ]');
			END IF;
		END IF;
		-- 入庫更新チェック
		PS_STOCK.ChkUpdStock(
			iLocaCD 		,
			iSkuCD 			,
			PS_DEFINE.USELIMITDT_DEFAULT,	-- todo iUseLimitDT
			rowLocaStockP.BATCHNO_CD,
			rowLocaStockP.STOCK_DT,
			rowSku.PCSPERCASE_NR,
			numCaseNR		,
			numBaraNR		,
			iPcsNR			,
			rowLoca.TYPELOCA_CD,	-- todo iTypeStockCD
			iUserCD			,
			rowUser.USER_TX
		);
		--入庫
		PS_STOCK.UpdStock(
			iLocaCD 		,
			iSkuCD 			,
			iLotTX			,
			iKanbanCD		,
			PS_DEFINE.USELIMITDT_DEFAULT,	-- todo iUseLimitDT
			rowLocaStockP.BATCHNO_CD,
			rowLocaStockP.STOCK_DT,
			rowSku.PCSPERCASE_NR,
			numPalletNR		,
			numCaseNR		,
			numBaraNR		,
			iPcsNR			,
			rowLoca.TYPELOCA_CD,	-- todo iTypeStockCD
			rowLoca.TYPELOCA_CD,
			iTypeOperationCD,
			'INOUT'			,
			0				,
			iMemoTx			,
			1				,	--iNeedSendFL
			iUserCD			,
			rowUser.USER_TX	,
			iProgramCD
		);
		
		-- ロケが空く場合はかんばんの引き落とし
		PS_STOCK.DelKanban(iKanbanCD);
		
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END UpdStock;

END PW_STOCK;
/
SHOW ERRORS
