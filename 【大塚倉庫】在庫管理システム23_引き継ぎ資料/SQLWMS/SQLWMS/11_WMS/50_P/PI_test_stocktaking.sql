-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_TAKING AS 

	--
	--		
	--
	
	/************************************************************************************/
	/*	棚卸																		*/
	/************************************************************************************/


	PROCEDURE Stocktaking(
		iLocaCD				IN VARCHAR2	,
		iWHCD				IN VARCHAR2	,	--自倉庫コード
		iSkuCD				IN VARCHAR2	,
		iSkuTX				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iExteriorCD			IN NUMBER	,   --外装	
		iKanbanCD			IN VARCHAR2	,	--かんばん番号
--		iCasePerPalletNR	IN NUMBER	,	--パレット積み付け数
--		iPalletLayerNR		IN NUMBER	,	--パレット段数
		iPalletCapacityNR	IN NUMBER	,	-- パレット積載数
		iImputpalletTX		IN NUMBER	,		
		iImputcaseTX		IN NUMBER	,
		iSumpalletNR		IN NUMBER	,
		iSumcaseNR			IN NUMBER	,
		iAnswercaseNR		IN NUMBER	,
		iNesFL				IN NUMBER	,	
		
--		iStackingNumberNR	IN NUMBER	,	--保管段数
--		iAmountNR			IN NUMBER	,
--		iPalletNR			IN NUMBER	,
--		iCaseNR				IN NUMBER	,
--		iBaraNR				IN NUMBER	,
--		iPcsNR				IN NUMBER	,
--		iStatusTX			IN VARCHAR2	,
--		iMemoTX				IN VARCHAR2	,
--		iNeedSendFL			IN NUMBER	,	--要送信フラグ
--		iSendEndFL			IN NUMBER	,	
--		iSendDT				IN DATE		,	
--		iUserCD				IN VARCHAR2	,
--		iUserTX				IN VARCHAR2	,
--		iProgramCD			IN VARCHAR2
		) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'Stocktaking';
		
--		numStocktakingID TA_STOCKTAKING.STOCKTAKING_ID%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			ROW_NUMBER()
			OVER (ORDER BY SIO.LOCA_CD) AS STOCKTAKING_ID		,
			SIO.LOCA_CD										,
			SUBSTR(ML.LOCA_TX,7,15) AS WH_CD				,
			LP.SKU_CD										,
			MS.SKU2_TX										,
			LP.LOT_TX										,
			MS.EXTERIOR_CD									,
			LP.KANBAN_CD									,
			LP.PALLETCAPACITY_NR							,
			'' AS IMPUTPALLET_TX							,	-- 入力用
			'' AS IMPUTCASE_TX								,	-- 入力用
			SUM(LP.PALLET_NR)								,	-- 総パレット数
			SUM(LP.CASE_NR)									,	-- 総ケース数
			CASE
				WHEN SUM(LP.PALLET_NR) = 0 THEN SUM(LP.CASE_NR)
				ELSE (SUM(LP.PALLET_NR) * LP.PALLETCAPACITY_NR) + SUM(LP.CASE_NR)
			END ANSWERCASE_NR								,	-- 答え
			CASE
				WHEN ML.SHIPVOLUME_CD <> '03' THEN 0
				ELSE 1
			END NES_FL										
			FROM
				(
				SELECT
					LOCA_CD
				FROM
					TA_STOCKINOUT
--				WHERE
--					ADD_DT > TRUNC(SYSDATE)			--当日荷動きロケを抽出
				GROUP BY
					LOCA_CD
				) SIO
					LEFT OUTER JOIN (
							SELECT
								LOCA_CD				,
								KANBAN_CD			,
								SKU_CD				,
								LOT_TX				,
								PALLETCAPACITY_NR	,
								PALLET_NR			,
								CASE_NR				,
								BARA_NR				,
								PCS_NR
							FROM
								TA_LOCASTOCKP
							UNION ALL
							SELECT
								LOCA_CD				,
								KANBAN_CD			,
								SKU_CD				,
								LOT_TX				,
								PALLETCAPACITY_NR	,
								SUM(PALLET_NR) AS PALLET_NR	,
								SUM(CASE_NR) AS CASE_NR		,
								SUM(BARA_NR) AS BARA_NR		,
								SUM(PCS_NR) AS PCS_NR
							FROM
								TA_LOCASTOCKC
							WHERE
								STSTOCK_ID = 0
							GROUP BY
								LOCA_CD				,
								KANBAN_CD			,
								SKU_CD				,
								LOT_TX				,
								PALLETCAPACITY_NR
					) LP
						ON SIO.LOCA_CD = LP.LOCA_CD
					LEFT OUTER JOIN MA_LOCA ML
						ON SIO.LOCA_CD = ML.LOCA_CD
					LEFT OUTER JOIN MA_SKU MS
						ON LP.SKU_CD = MS.SKU_CD
			GROUP BY
			SIO.LOCA_CD										,
			SUBSTR(ML.LOCA_TX,7,15)				,
			LP.SKU_CD										,
			MS.SKU2_TX										,
			LP.LOT_TX										,
			MS.EXTERIOR_CD									,
			LP.KANBAN_CD									,
			LP.PALLETCAPACITY_NR							,
--			CASE
--				WHEN SUM(LP.PALLET_NR) = 0 THEN SUM(LP.CASE_NR)
--				ELSE (SUM(LP.PALLET_NR) * LP.PALLETCAPACITY_NR) + SUM(LP.CASE_NR)
--			END												,
			CASE
				WHEN ML.SHIPVOLUME_CD <> '03' THEN 0
				ELSE 1
			END
			ORDER BY
				SIO.LOCA_CD;
				
				INSERT INTO TA_STOCKTAKING (
					STOCKTAKING_ID		,
					LOCA_CD				,
					WH_CD				,
					SKU_CD				,
					SKU_TX				,
					LOT_TX				,
					EXTERIOR_CD			,
					KANBAN_CD			,
--					CASEPERPALLET_NR	,
--					PALLETLAYER_NR		,
					PALLETCAPACITY_NR	,
--					STACKINGNUMBER_NR	,
					IMPUTPALLET_TX      ,
					IMPUTCASE_TX      	,
					SUMPALLET_NR		,
					SUMCASE_NR			,
					ANSWERCASE_NR		,
					NES_FL				,
--					AMOUNT_NR			,
--					PALLET_NR			,
--					CASE_NR				,
--					BARA_NR				,
--					PCS_NR				,
--					STATUS_TX			,
--					MEMO_TX				,			
--					NEEDSEND_FL			,
--					SENDEND_FL			,
					-- 管理項目
--					UPD_DT				,
--					UPDUSER_CD			,
--					UPDUSER_TX			,
--					ADD_DT				,
--					ADDUSER_CD			,
--					ADDUSER_TX			,
--					UPDPROGRAM_CD		,
--					UPDCOUNTER_NR		,
--					STRECORD_ID
				) VALUES (
					numStocktakingID		,
					iLocaCD					,
					iWHCD					,	--自倉庫コード
					iSkuCD					,
					iSkuTX					,
					iLotTX					,
					iExteriorCD				,   --外装	
					iKanbanCD				,	--かんばん番号
			--		iCasePerPalletNR		,	--パレット積み付け数
			--		iPalletLayerNR			,	--パレット段数
					iPalletCapacityNR		,	-- パレット積載数
					iImputpalletTX			,		
					iImputcaseTX			,
					iSumpalletNR			,
					iSumcaseNR				,
					iAnswercaseNR			,
					iNesFL					,	
				);
				PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
			EXCEPTION
				WHEN OTHERS THEN
					IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
						PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					ELSE
						PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
					END IF;
					RAISE;
			END Stocktaking;
END PS_TAKING;
/
SHOW ERRORS
