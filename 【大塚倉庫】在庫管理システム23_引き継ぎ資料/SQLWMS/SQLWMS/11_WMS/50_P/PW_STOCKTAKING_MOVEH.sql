-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PW_TAKING IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PW_TAKING';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************************************/
	/*	荷動き棚卸																		*/
	/************************************************************************************/
	
	PROCEDURE Stocktaking(
		--	iPalletDetNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	
--		iProgramCD		IN VARCHAR2
	);
	
	/************************************************************************************/
	/*	全棚棚卸																			*/
	/************************************************************************************/
	
	PROCEDURE StocktakingAll(
		--	iPalletDetNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	
--		iProgramCD		IN VARCHAR2
	);

	/************************************************************************************/
	/*	作業員用画面よりデータ更新(WA9511)																			*/
	/************************************************************************************/
	
	PROCEDURE UpdateStatus(
		--	iPalletDetNoCD	IN VARCHAR2	,
		iLoca_CD			IN VARCHAR2,	
		iOperator_TX		IN VARCHAR2,	--作業員
		iProgramCD			IN VARCHAR2,
		iResult_TX			IN VARCHAR2,
		iStatus_TX			IN VARCHAR2
--		iSend_DT			IN VARCHAR2		--作業日時
--		iProgramCD		IN VARCHAR2
	);
	
	/************************************************************************************/
	/*	管理者用画面よりスデータ更新(WA901及びWA902)																			*/
	/************************************************************************************/
	
	PROCEDURE UpdateStatusAuthorizer(
		--	iPalletDetNoCD	IN VARCHAR2	,
--		iLoca_CD			IN VARCHAR2,	
		iAuthorizer_TX		IN VARCHAR2,	--承認者
		iProgramCD			IN VARCHAR2,
		iStatus_TX			IN VARCHAR2
--		iSend_DT			IN VARCHAR2		--承認日時
--		iProgramCD		IN VARCHAR2
	);

	/************************************************************************************/
	/*	棚卸データを棚卸保存テーブルに移動																	*/
	/************************************************************************************/
	PROCEDURE MoveStocktaking(
--		iLoca_CD			IN VARCHAR2,	
		iAuthorizer_TX		IN VARCHAR2,	--承認者
		iProgramCD			IN VARCHAR2,
		iStatus_TX			IN VARCHAR2
--		iSend_DT			IN VARCHAR2		--承認日時
	 );
	
END	PW_TAKING;
/
SHOW ERRORS
