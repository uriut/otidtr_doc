-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PI_FILESEND AS
	/*----------------------------------------------------------*/
	/* Public                                                   */
	/*----------------------------------------------------------*/
	/************************************************************/
	/*	トランザクションID新規取得								*/
	/************************************************************/
	FUNCTION GetNewTrnId
	RETURN NUMBER IS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetNewTrnId';
		numTrnId NUMBER(10,0) := -1;
	BEGIN
		-- 主処理
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_TRNID.NEXTVAL INTO numTrnId
		FROM
			DUAL;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
		RETURN numTrnId;
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			COMMIT;
			RAISE;
	END GetNewTrnId;
	/************************************************************/
	/* 入荷予定ダウンロード完了                                 */
	/************************************************************/
	PROCEDURE EndDownLoadArriv
	IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'EndDownLoadArriv';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 入荷予定更新
		UPDATE
			TA_ARRIV
		SET
			STARRIV_ID		= PS_DEFINE.ST_ARRIV1_SEND	,
			--
			UPD_DT			= SYSDATE					,
			UPDUSER_CD		= 'BATCH'					,
			UPDUSER_TX		= 'BATCH'					,
			UPDPROGRAM_CD	= FUNCTION_NAME				,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			STARRIV_ID = PS_DEFINE.ST_ARRIV0_NOTSEND;		-- 0:未送信
		COMMIT;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END EndDownLoadArriv;
END PI_FILESEND;
/
SHOW ERRORS
