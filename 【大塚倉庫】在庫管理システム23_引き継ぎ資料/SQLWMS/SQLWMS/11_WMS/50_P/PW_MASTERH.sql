CREATE OR REPLACE
PACKAGE PW_MASTER IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	/*パッケージ名*/
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PW_MASTER';
	/* ログレベル*/
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************************************/
	/*	SKU更新																			*/
	/************************************************************************************/
	PROCEDURE UpdSku(
		iSkuCD					IN VARCHAR2	,
		iNestainerInStockRateNR	IN NUMBER	,
		iCaseStockRateNR		IN NUMBER	,
		iAbcID					IN NUMBER	,
		iAbcSubID				IN NUMBER	,
		iUserCD					IN VARCHAR2	,
		iProgramCD				IN VARCHAR2
	);
	/************************************************************************************/
	/*	ロケーションマスタ登録															*/
	/************************************************************************************/
	PROCEDURE AddLoca(
		iWhCD				IN VARCHAR2	,
		iFloorCD			IN VARCHAR2	,
		iAreaCD				IN VARCHAR2	,
		iLineCD				IN VARCHAR2	,
		iShelfCD			IN VARCHAR2	,
		iLayerCD			IN VARCHAR2	,
		iOrderCD			IN VARCHAR2	,
		iTypeLocaBlockCD	IN VARCHAR2	,
		iLineUnitCD			IN VARCHAR2	,
		iLineUnitNR			IN NUMBER	,
		iTypeLocaCD			IN VARCHAR2	,
		iHEquipCD			IN VARCHAR2	,
		iCaseCD				IN VARCHAR2	,
		iPalletDivNR		IN NUMBER	,
		iShipVolumeCD		IN VARCHAR2	,
		iAbcID				IN NUMBER	,
		iAbcSubID			IN NUMBER	,
		iArrivPriorityNR	IN NUMBER	,
		iShipPriorityNR		IN NUMBER	,
		iNotUseFL			IN NUMBER	,
		iNotShipFL			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/************************************************************************************/
	/*	ロケーションマスタ更新				棚単位										*/
	/************************************************************************************/
	PROCEDURE UpdLoca(
		iLocaShelfCD		IN VARCHAR2	,
		iTypeLocaBlockCD	IN VARCHAR2	,
		iLineUnitCD			IN VARCHAR2	,
		iLineUnitNR			IN NUMBER	,
		iTypeLocaCD			IN VARCHAR2	,
		iCaseCD				IN VARCHAR2	,
		iPalletDivNR		IN NUMBER	,
		iShipVolumeCD		IN VARCHAR2	,
		iAbcID				IN NUMBER	,
		iAbcSubID			IN NUMBER	,
		iArrivPriorityNR	IN NUMBER	,
		iShipPriorityNR		IN NUMBER	,
		iNotUseFL			IN NUMBER	,
		iNotShipFL			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/************************************************************************************/
	/*	ロケーションマスタ更新				ロケ別										*/
	/************************************************************************************/
	PROCEDURE UpdLocation(
		iLocaCD				IN VARCHAR2	,
		iTypeLocaBlockCD	IN VARCHAR2	,
		iLineUnitCD			IN VARCHAR2	,
		iLineUnitNR			IN NUMBER	,
		iTypeLocaCD			IN VARCHAR2	,
		iCaseCD				IN VARCHAR2	,
		iPalletDivNR		IN NUMBER	,
		iShipVolumeCD		IN VARCHAR2	,
		iAbcID				IN NUMBER	,
		iAbcSubID			IN NUMBER	,
		iArrivPriorityNR	IN NUMBER	,
		iShipPriorityNR		IN NUMBER	,
		iNotUseFL			IN NUMBER	,
		iNotShipFL			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/************************************************************************************/
	/*	ロケーション削除																*/
	/************************************************************************************/
	PROCEDURE DelLoca(
		iLocaShelfCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/************************************************************************************/
	/*	かんぱんマスタ登録																*/
	/************************************************************************************/
	PROCEDURE AddKanban(
		iKanbanCD			IN VARCHAR2	,
		iKanbanTX			IN VARCHAR2	,
		iTypeKanbanCD		IN VARCHAR2	,
		iColorTX			IN VARCHAR2	,
		iAreaTX				IN VARCHAR2	,
		iLineUnitCD			IN VARCHAR2	,
		iLineUnitNR			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/************************************************************************************/
	/*	かんぱんマスタ更新																*/
	/************************************************************************************/
	PROCEDURE UpdKanban(
		iOldKanbanCD		IN VARCHAR2	,
		iNewKanbanCD		IN VARCHAR2	,
		iKanbanTX			IN VARCHAR2	,
		iTypeKanbanCD		IN VARCHAR2	,
		iColorTX			IN VARCHAR2	,
		iAreaTX				IN VARCHAR2	,
		iLineUnitCD			IN VARCHAR2	,
		iLineUnitNR			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/************************************************************************************/
	/*	かんぱんマスタ削除																*/
	/************************************************************************************/
	PROCEDURE DelKanban(
		iKanbanCD			IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/************************************************************************************/
	/*	荷姿登録																		*/
	/************************************************************************************/
	PROCEDURE AddTypePackaging(
		/*	どういう内容なのか不明。確認要。*/
		/*	SKUマスタの「出荷荷姿」(typepackaging_cd)を更新する処理と推測*/
		/**/
		iSkuCD				IN VARCHAR2	,
		iTypePackagingCD	IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
END PW_MASTER;
/
SHOW ERRORS
