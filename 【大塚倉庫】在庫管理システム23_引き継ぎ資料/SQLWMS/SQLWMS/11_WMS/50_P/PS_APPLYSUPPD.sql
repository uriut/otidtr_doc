-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_APPLYSUPP AS
	/************************************************************************************/
	/*	緊急補充引当																	*/
	/************************************************************************************/
	PROCEDURE ApplyESupp(
		iTrnID			IN	NUMBER	,
		iUserCD			IN	VARCHAR2,
		iUserTX			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'ApplyESupp';
		-- 補充対象
		CURSOR curSupp(TrnID TA_PICK.TRN_ID%TYPE) IS
			SELECT
				SRCLOCA_CD			,
				SKU_CD				,
				LOT_TX				,
				PALLETCAPACITY_NR	,
				STACKINGNUMBER_NR	,
				AMOUNT_NR			,
				SUM(PCS_NR) AS PCS_NR
			FROM
				TA_PICK
			WHERE
				TRN_ID			= TrnID							AND
				STPICK_ID		= PS_DEFINE.ST_PICKM1_CREATE	AND
				TYPECARRY_CD	= '10'
			GROUP BY
				SRCLOCA_CD			,
				SKU_CD				,
				LOT_TX				,
				PALLETCAPACITY_NR	,
				STACKINGNUMBER_NR	,
				AMOUNT_NR
			ORDER BY
				SKU_CD		,
				LOT_TX		,
				SRCLOCA_CD	;
		rowSupp		curSupp%ROWTYPE;
		-- ピッキング指示
		CURSOR curPick(	TrnID TA_PICK.TRN_ID%TYPE,
						SkuCD TA_PICK.SKU_CD%TYPE,
						LotTX TA_PICK.LOT_TX%TYPE,
						SrcLocaCD TA_PICK.SRCLOCA_CD%TYPE
						) IS
			SELECT
				*
			FROM
				TA_PICK
			WHERE
				TRN_ID			= TrnID		AND
				SKU_CD			= SkuCD		AND
				LOT_TX			= LotTX		AND
				SRCLOCA_CD		= SrcLocaCD	AND
				STPICK_ID		= PS_DEFINE.ST_PICKM1_CREATE	AND
				TYPECARRY_CD	= '10'
			ORDER BY
				SKU_CD		,
				LOT_TX		,
				SRCLOCA_CD	,
				PICK_ID		;
		rowPick		curPick%ROWTYPE;
		-- 引当可能在庫取得
		CURSOR curStockP(
						LocaCD TA_LOCASTOCKP.LOCA_CD%TYPE,
						SkuCD TA_LOCASTOCKP.SKU_CD%TYPE,
						LotTX TA_LOCASTOCKP.LOT_TX%TYPE
						) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD			= LocaCD	AND
				SKU_CD			= SkuCD		AND
				LOT_TX			= LotTX;
		rowStockP		curStockP%ROWTYPE;
		
		chrSkuCD			TA_PICK.SKU_CD%TYPE;
		chrLocaCD			TA_PICK.SRCLOCA_CD%TYPE;
		chrSuppNoCD			TA_SUPP.SUPPNO_CD%TYPE;
		--
		rowDLocaStockC		TA_LOCASTOCKC%ROWTYPE;
		rowDLocaStockC2		TA_LOCASTOCKC%ROWTYPE;
		--
		rowSLocaStockC		TA_LOCASTOCKC%ROWTYPE;
		numSuppPcsNR		NUMBER;
		numStockPcsNR		NUMBER;
		numTotalPcsNR		NUMBER;
		numBaraPcsNR		NUMBER;
		numReqPcsNR 		NUMBER;
		numPalletNR			NUMBER;
		numCaseNR			NUMBER;
		numBaraNR			NUMBER;

		-- ロケ検索結果
		rowSku				MA_SKU%ROWTYPE;
		oEndFL				NUMBER(1);
		oLocaCD				MA_LOCA.LOCA_CD%TYPE;
		oChangeFL			NUMBER(1);
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 初期化
		chrLocaCD	:= ' ';
		chrSkuCD	:= ' ';
		chrSuppNoCD	:= ' ';
		rowDLocaStockC.TYPESTOCK_CD		:= ' ';
		rowDLocaStockC.LOCA_CD			:= ' ';
		rowDLocaStockC.LOCASTOCKC_ID	:= 0;
		-- 補充対象
		OPEN curSupp(iTrnID);
		LOOP
			FETCH curSupp INTO rowSupp;
			EXIT WHEN curSupp%NOTFOUND;
			PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'ﾛｹ= [' || rowSupp.SRCLOCA_CD || '], '||
																	   'SKUｺｰﾄﾞ=[' || rowSupp.SKU_CD || '], '|| 
																	   'ﾛｯﾄ=[' || rowSupp.LOT_TX || ']');
			
			-- SKUマスタ取得
			PS_MASTER.GetSku(rowSupp.SKU_CD, rowSku);
			-- 初期化
			oEndFL := 0;
			oChangeFL := 0;
			oLocaCD := ' ';
			numTotalPcsNR := 0;
			numSuppPcsNR := 0;
			numStockPcsNR := 0;
			
			-- 元ロケーション単位で指示書を作成(todo　1ロケ、複数SKUではない為、ロケ単位で問題ない？)
			IF chrLocaCD != rowSupp.SRCLOCA_CD THEN

				-- パレット単位で指示書を作成
				
				-- 補充総数取得
				numSuppPcsNR := rowSupp.PCS_NR;
				PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' numSuppPcsNR=[' || numSuppPcsNR || ']');
				-- 指示番号
				PS_NO.GetSuppNo(chrSuppNoCD);
				/**********************************************************************************/
				/* 大塚IDは、緊急補充の対象ロケ在庫は全て補充する必要がある
				/* 対象ロケ引当残が存在する場合、引当
				/* 指示は1パレ単位
				/* 課題：例えば在庫ロケに2パレある場合、2パレ分補充を作成するが、
				/*       その内の1パレが出荷対象の場合、
				/*       もう1パレは指示が紐づいていない補充となる為、
				/*       1パレのみ補充しても指示書印刷が出来ない場合がある。
				/**********************************************************************************/
				-- 引当可能在庫
				PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' ' || rowSupp.SRCLOCA_CD || ' ' || rowSupp.SKU_CD || ' ' || rowSupp.LOT_TX);
				OPEN curStockP(rowSupp.SRCLOCA_CD, rowSupp.SKU_CD, rowSupp.LOT_TX);
				FETCH curStockP INTO rowStockP;
				IF curStockP%FOUND THEN
					IF rowStockP.PCS_NR > 0 THEN
						-- 在庫数取得
						numStockPcsNR := rowStockP.PCS_NR;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' numStockPcsNR=[' || numStockPcsNR || ']');
					END IF;
				ELSE
					PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' LOCASTOCKP NOT FOUND!');
				END IF;
				CLOSE curStockP;

				PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' ' || 
								'rowSupp.PCS_NR = [' || rowSupp.PCS_NR || '], ' ||
								'rowSupp.AMOUNT_NR = [' || rowSupp.AMOUNT_NR || '], ' ||
								'rowSupp.PALLETCAPACITY_NR = [' || rowSupp.PALLETCAPACITY_NR || ']'
				);

				--移動総数取得
				IF MOD(TRUNC(rowSupp.PCS_NR / rowSupp.AMOUNT_NR), rowSupp.PALLETCAPACITY_NR) = 0 THEN
					IF TRUNC(rowSupp.PCS_NR / rowSupp.AMOUNT_NR / rowSupp.PALLETCAPACITY_NR) > 0 THEN
						numTotalPcsNR := rowSupp.PCS_NR;
						numStockPcsNR := 0;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || '完パレ numTotalPcsNR=[' || numTotalPcsNR || '], numStockPcsNR=[' || numStockPcsNR || ']');
					ELSE
						RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'予期せぬエラー：補充指示数が不正です。' || 
																		'PCS_NR=[' || rowSupp.PCS_NR || '], AMOUNT_NR=[' || rowSupp.AMOUNT_NR || '], PALLETCAPACITY_NR=[' || rowSupp.PALLETCAPACITY_NR || ']');
					END IF;
				ELSE
					numBaraPcsNR := MOD(TRUNC(rowSupp.PCS_NR / rowSupp.AMOUNT_NR), rowSupp.PALLETCAPACITY_NR);
					numReqPcsNR  := rowSupp.PALLETCAPACITY_NR * rowSupp.AMOUNT_NR - numBaraPcsNR * rowSupp.AMOUNT_NR;
					PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'numBaraPcsNR=[' || numBaraPcsNR || '], numReqPcsNR=[' || numReqPcsNR || ']');

					IF numStockPcsNR >= numReqPcsNR THEN
						numTotalPcsNR := rowSupp.PCS_NR + numReqPcsNR;
						numStockPcsNR := numReqPcsNR;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'バラあり numTotalPcsNR=[' || numTotalPcsNR || '], numStockPcsNR=[' || numStockPcsNR || ']');
					ELSE
						RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'予期せぬエラー：補充在庫数が不正です。' || 
																		'numStockPcsNR=[' || numStockPcsNR || '], AMOUNT_NR=[' || rowSupp.AMOUNT_NR || '], PALLETCAPACITY_NR=[' || rowSupp.PALLETCAPACITY_NR || '], numBaraPcsNR=[' || numBaraPcsNR || ']');
					END IF;
				END IF;

				-- 先ロケ検索
				IF oEndFL = 0 THEN
					PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' LocaSearchSupp_1:skuCD=[' || rowPick.SKU_CD || ']'
					);
					PS_STOCK.GetUnitPcsNr(
						rowSupp.SKU_CD	,
						numTotalPcsNR	,
						rowSupp.PALLETCAPACITY_NR,
						numPalletNR		,
						numCaseNR		,
						numBaraNR
						);
					IF numCaseNR <> 0 OR numBaraNR <> 0 THEN
						RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'完パレ補充ではありません。' || 
																		'ﾊﾟﾚｯﾄ数=[' || numPalletNR || '], ｹｰｽ数=[' || numCaseNR || '], ﾊﾞﾗ数=[' || numBaraNR || ']');
					END IF;
					PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'ﾛｹ= [' || rowSupp.SRCLOCA_CD || '], ' ||
																		 	   'SKUｺｰﾄﾞ=[' || rowSupp.SKU_CD || '], ' || 
																		  	   'ﾛｯﾄ=[' || rowSupp.LOT_TX || ']');
					-- 空きロケ検索
					ApplyStockEmpty(
						iTrnID								,	
						rowSku								,	
						rowSupp.LOT_TX						,	
						rowSupp.PALLETCAPACITY_NR			,	
						rowSupp.STACKINGNUMBER_NR			,	
						numPalletNR							,	
						PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
						PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
						PS_DEFINE.TYPE_SHIPVOLUME1_CASE		,	-- SHIPVOLUME_CD
						oEndFL								,	
						oLocaCD
						);
				END IF;
				PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'SKU_CD=[' || rowSupp.SKU_CD || '], ' ||
																		   'LOT_TX=[' || rowSupp.LOT_TX || '], ' ||
																		   'oLocaCD=[' || oLocaCD || '], ' || 
																		   'oEndFL=[' || oEndFL || '] '
																		   );
				IF oEndFL = 0 THEN
					RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,
						'補充可能ロケがありません。SKUｺｰﾄﾞ=[' || rowSupp.SKU_CD || '], ' ||
													'ﾛｯﾄ=[' || rowSupp.LOT_TX || '], ' ||
													'保管段数=[' || rowSupp.STACKINGNUMBER_NR || '], ' ||
													'補充ﾊﾟﾚｯﾄ数=[' || numPalletNR || ']'
													);
				END IF;

				IF numStockPcsNR > 0 THEN

					PS_STOCK.GetUnitPcsNr(
						rowSupp.SKU_CD	,
						numStockPcsNR	,
						rowSupp.PALLETCAPACITY_NR,
						numPalletNR		,
						numCaseNR		,
						numBaraNR
						);

					-- 補充元引当
					PS_STOCK.ApplyStock(
						iTrnID						,
						rowStockP.LOCA_CD			,
						rowStockP.SKU_CD			,
						rowStockP.LOT_TX			,
						rowStockP.USELIMIT_DT		,
						' '							,	--BATCHNO_CD
						rowStockP.STOCK_DT			,
						rowStockP.AMOUNT_NR			,
						rowStockP.PALLETCAPACITY_NR	,
						numPalletNR					,
						numCaseNR					,
						numBaraNR   				,
						numStockPcsNR				,
						rowStockP.TYPESTOCK_CD		,
						PS_DEFINE.ST_STOCK0_NOR		,
						iUserCD						,
						iUserTX						,
						rowSLocaStockC
					);
					-- 補充先引当
					PS_STOCK.ApplyInStock(
						iTrnID						,
						PS_DEFINE.ST_STOCK11_SUPP	,
						oLocaCD						,	-- iLocaCD
						rowStockP.SKU_CD			,
						rowStockP.LOT_TX			,
						' '							,	-- iKanbanCD
						rowStockP.CASEPERPALLET_NR	,
						rowStockP.PALLETLAYER_NR	,
						rowStockP.PALLETCAPACITY_NR	,
						rowStockP.STACKINGNUMBER_NR	,
						rowStockP.TYPEPLALLET_CD	,
						rowStockP.USELIMIT_DT		,
						' '							,	-- BATCHNO_CD
						rowStockP.STOCK_DT			,
						rowStockP.AMOUNT_NR			,
						numPalletNR					,
						numCaseNR					,
						numBaraNR   				,
						numStockPcsNR				,
						rowStockP.TYPESTOCK_CD		,
						iUserCD						,
						iUserTX						,
						rowDLocaStockC2
					);
				END IF;
			END IF;
			
			--ロケーションロック取得
			PS_LOCA.GetLocaLock(oLocaCD);
			
			-- ピッキング指示
			OPEN curPick(iTrnID, rowSupp.SKU_CD, rowSupp.LOT_TX, rowSupp.SRCLOCA_CD);
			LOOP
				FETCH curPick INTO rowPick;
				EXIT WHEN curPick%NOTFOUND;
				rowDLocaStockC.LOCA_CD := oLocaCD;
				-- 補充先引当
				PS_STOCK.ApplyInStock(
					iTrnID						,
					PS_DEFINE.ST_STOCK11_SUPP	,
					oLocaCD						,	-- iLocaCD
					rowPick.SKU_CD				,
					rowPick.LOT_TX				,
					' '							,	-- iKanbanCD
					rowPick.CASEPERPALLET_NR	,
					rowPick.PALLETLAYER_NR		,
					rowPick.PALLETCAPACITY_NR	,
					rowPick.STACKINGNUMBER_NR	,
					rowPick.TYPEPLALLET_CD		,
					rowPick.USELIMIT_DT			,
					' '							,	-- BATCHNO_CD
					rowPick.STOCK_DT			,
					rowPick.AMOUNT_NR			,
					rowPick.PALLET_NR			,
					rowPick.CASE_NR				,
					rowPick.BARA_NR				,
					rowPick.PCS_NR				,
					rowPick.SRCTYPESTOCK_CD		,
					iUserCD						,
					iUserTX						,
					rowDLocaStockC
				);
				-- 補充データ作成
				PS_SUPP.InsSupp(
					iTrnID					,
					chrSuppNoCD				,
					PS_DEFINE.TYPE_SUPP_EMG	,
					rowPick					,
					rowDLocaStockC			,
					iUserCD					,
					iUserTX					,
					iProgramCD
				);
			END LOOP;
			
			IF numStockPcsNR > 0 THEN
				-- 補充データ作成（今回引き当て分）
				PS_SUPP.InsSupp2(
					iTrnID					,
					chrSuppNoCD				,
					PS_DEFINE.TYPE_SUPP_EMG	,
					rowPick					,
					rowSLocaStockC			,
					rowDLocaStockC2			,
					iUserCD					,
					iUserTX					,
					iProgramCD
				);
			END IF;
			CLOSE curPick;
			-- 次回ループ用に保存
			chrLocaCD := rowSupp.SRCLOCA_CD;
		END LOOP;
		CLOSE curSupp;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curSupp%ISOPEN THEN CLOSE curSupp; END IF;
			IF curPick%ISOPEN THEN CLOSE curPick; END IF;
			IF curStockP%ISOPEN THEN CLOSE curStockP; END IF;
			RAISE;
	END ApplyESupp;
	/************************************************************************************/
	/*	補充先ロケ検索（空きロケ）														*/
	/************************************************************************************/
	PROCEDURE ApplyStockEmpty(
		iTrnID				IN NUMBER		,
		irowSku				IN MA_SKU%ROWTYPE,
		iLotTX				IN VARCHAR2		,
		iPalletCapacityNR	IN NUMBER		,
		iStackingNumberNR	IN NUMBER		,
		iPalletNR			IN NUMBER		,
		iTypeLocaCD			IN VARCHAR2		,
		iTypeLocaBlockCD	IN VARCHAR2		,
		iShipVolumeCD		IN VARCHAR2		,
		oEndFL				OUT NUMBER		,
		oLocaCD				OUT VARCHAR2
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'ApplyStockEmpty';
		numRowPoint 	NUMBER := 0;
		CURSOR curLocaStock(
			SkuCD				TA_LOCASTOCKP.SKU_CD%TYPE				,
			StackingNumberNR	TA_LOCASTOCKP.STACKINGNUMBER_NR%TYPE	,
			PalletNR			TA_LOCASTOCKP.PALLET_NR%TYPE			,
			TypeLocaCD			MA_LOCA.TYPELOCA_CD%TYPE				,
			TypeLocaBlockCD		MA_LOCA.TYPELOCABLOCK_CD%TYPE			,
			ShipVolumeCD		MA_LOCA.SHIPVOLUME_CD%TYPE
		) IS
			SELECT
				ML.LOCA_CD				,
				ML.ARRIVPRIORITY_NR		,
				ML.LOCA_TX				,
				ML.LINEUNIT_NR			,
				ML.AREA_CD
			FROM
				MA_LOCA ML
					LEFT OUTER JOIN TA_LOCASTOCKP LP
						ON ML.LOCA_CD = LP.LOCA_CD
					LEFT OUTER JOIN TA_LOCASTOCKC LC
						ON ML.LOCA_CD = LC.LOCA_CD
			WHERE
				ML.TYPELOCA_CD		= PS_DEFINE.TYPE_STOCK_OK	AND
				ML.SHIPVOLUME_CD	= ShipVolumeCD				AND
				--ML.NOTUSE_FL		= 0							AND
				ML.NOTSHIP_FL		= 0 						AND -- 出荷引当可
				ML.LINEUNIT_NR * StackingNumberNR >= PalletNR 	AND	-- ラインユニット * 保管段数 > 格納パレット数
				LP.LOCA_CD			IS NULL						AND
				LC.LOCA_CD			IS NULL
			ORDER BY
				ML.NOTUSE_FL DESC		,	-- 入荷不可から
				ML.LINEUNIT_NR			,
				ML.LOCA_CD				;
		rowLocaStock	curLocaStock%ROWTYPE;
		--
		rowLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		numInStockID	TA_INSTOCK.INSTOCK_ID%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START, ' ||
			'iTrnID=[' || iTrnID || '], ' ||
			'iTypeLocaCD=[' || iTypeLocaCD || '], ' ||
			'iTypeLocaBlockCD=[' || iTypeLocaBlockCD || '], ' ||
			'iShipVolumeCD=[' || iShipVolumeCD || '], ' ||
			'SKU_CD=[' || irowSku.SKU_CD || ']'
		);
		OPEN curLocaStock(
			irowSku.SKU_CD	,
			iStackingNumberNR,
			iPalletNR		,
			iTypeLocaCD		,
			iTypeLocaBlockCD,
			iShipVolumeCD
		);
		oEndFL := 0;
		LOOP
			FETCH curLocaStock INTO rowLocaStock;
			EXIT WHEN curLocaStock%NOTFOUND;
			EXIT WHEN oEndFL = 1;
			PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' FOUND, ' ||
				'iTrnID=[' || iTrnID || '], ' ||
				'SKU_CD=[' || irowSku.SKU_CD || '], ' ||
				'LOCA_CD=[' || rowLocaStock.LOCA_CD || ']'
			);
			oEndFL := 1;
			oLocaCD := rowLocaStock.LOCA_CD;
			-- 
		END LOOP;
		CLOSE curLocaStock;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END,' ||
			'oEndFL=[' || oEndFL || '], ' ||
			'oLocaCD=[' || oLocaCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStock%ISOPEN THEN
				CLOSE curLocaStock;
			END IF;
			RAISE;
	END ApplyStockEmpty;
END PS_APPLYSUPP;
/
SHOW ERRORS
