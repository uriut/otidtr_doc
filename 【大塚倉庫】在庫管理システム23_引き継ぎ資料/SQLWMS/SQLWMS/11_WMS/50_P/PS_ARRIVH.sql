-- Copyright (C) Seaos Corporation 2014 All rights reserved
CREATE OR REPLACE PACKAGE PS_ARRIV IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_ARRIVH';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************/
	/*	入荷前情報取得									*/
	/************************************************************/
	PROCEDURE GetArrivCount(
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iArrivplanDT		IN DATE		,
		iShipVolumeCD		IN VARCHAR2	,
		oNumLineUnit		OUT NUMBER	
	);
END PS_ARRIV;
/
SHOW ERRORS
