-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_STOCK AS 

	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_STOCK';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	--
	--		テーブルレベルの基本操作
	--
	
	/****************************************************/
	/*	在庫受払										*/
	/****************************************************/
	PROCEDURE InsStockInOut(
		iWHCD				IN VARCHAR2	,	--自倉庫コード
		iTargetWHCD			IN VARCHAR2	,	--相手先倉庫コード
		iLocaCD				IN VARCHAR2	,
		iSkuCD				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,	--かんばん番号
		iCasePerPalletNR	IN NUMBER	,	--パレット積み付け数
		iPalletLayerNR		IN NUMBER	,	--パレット段数
		iPalletCapacityNR	IN NUMBER	,	-- パレット積載数
		iStackingNumberNR	IN NUMBER	,	--保管段数
		iTypePalletCD		IN VARCHAR2	,	--パレットタイプ
		iUseLimitDT			IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,	--在庫区分
		iTypeLocaCD			IN VARCHAR2	,	--ロケーション種別コード
		iTypeOperationCD	IN VARCHAR2	,	--業務区分
		iTypeRefCD			IN VARCHAR2	,	--システム参照先区分
		iRefNoID			IN NUMBER	,	--システム参照ID
		iMemoTX				IN VARCHAR2	,
		iNeedSendFL			IN NUMBER	,	--要送信フラグ
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/****************************************************/
	/*	引当可能数・更新								*/
	/****************************************************/
	PROCEDURE UpdateP(
		iRowID	 			IN UROWID	,
		iAmountNR			IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,	-- パレット積載数
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/****************************************************/
	/*	引当可能数・登録								*/
	/****************************************************/
	PROCEDURE InsertP(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX 				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iCasePerPalletNR	IN NUMBER	,
		iPalletLayerNR		IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,	-- パレット積載数
		iStackingNumberNR	IN NUMBER	,
		iTypePalletCD		IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchNoCD			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/****************************************************/
	/*	引当済数・登録									*/
	/****************************************************/
	PROCEDURE InsertC(
		iTrnID				IN NUMBER	,
		iStStockID			IN NUMBER	,
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iCasePerPalletNR	IN NUMBER	,
		iPalletLayerNR		IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,	-- パレット積載数
		iStackingNumberNR	IN NUMBER	,
		iTypePalletCD		IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchNoCD			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2	,
		oLocaStockCID		OUT NUMBER
	);
	/****************************************************/
	/*	引当済数・削除									*/
	/****************************************************/
	PROCEDURE DelC(
		iLocaStockCID		IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/****************************************************/
	/*	引当済数・コピー								*/
	/****************************************************/
	PROCEDURE CopyC(
		iLocaStockCID		IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBAraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2	,
		oLocaStockCID		OUT NUMBER
	);
	
	/****************************************************/
	/*	数量取得(単位別)								*/
	/****************************************************/
	PROCEDURE GetUnitPcsNr(
		iSkuCD				IN VARCHAR2	,
		iPcsNR				IN NUMBER	,	--最少単位へ変換した数量（例:1ケース10入りの場合、10を渡す）
		--iAmontNR			IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,
		oPalletNR			OUT NUMBER	,
		oCaseNR				OUT NUMBER	,
		oBaraNR				OUT NUMBER
	);
	
	/****************************************************/
	/*	かんばん取得									*/
	/****************************************************/
	PROCEDURE GetKanban(
		iSkuCD		IN VARCHAR2	,
		iLotTX		IN VARCHAR2	,
		oLocaCD		OUT VARCHAR2,
		oKanbanCD	OUT VARCHAR2
	);
	
	/****************************************************/
	/*	ロケ間口取得									*/
	/****************************************************/
	PROCEDURE GetLocaNR(
		iLocaCD				IN VARCHAR2	,
		iSkuCD				IN VARCHAR2	,
		oLineUnitNR			OUT NUMBER	,	-- ラインユニット
		oStackingNumberNR	OUT NUMBER	,	-- 保管段数
		oTotalLocaNR		OUT NUMBER	,	-- 総間口
		oUseLocaNR			OUT NUMBER	,	-- 使用間口
		oEmptyLocaNR		OUT NUMBER	,	-- 空き間口
		oBaraFL				OUT NUMBER		-- バラフラグ　1:端数あり
	);
	
	/****************************************************/
	/*	在庫の使用期限（一番長い）						*/
	/****************************************************/
	FUNCTION GetUseLimitFar(
		iSkuCD				IN VARCHAR2
	) RETURN DATE;
	
	/****************************************************/
	/*													*/
	/*		入庫										*/
	/*													*/
	/****************************************************/
	/****************************************************/
	/*	入庫チェック									*/
	/****************************************************/
	PROCEDURE ChkInStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2
	);
	/****************************************************/
	/*	入庫（在庫調整）								*/
	/****************************************************/
	PROCEDURE InStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX 				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR 			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iCASEPERPALLET_NR	IN NUMBER	,
		iPALLETLAYER_NR		IN NUMBER	,
		iPALLETCAPACITY_NR	IN NUMBER	,
		iSTACKINGNUMBER_NR	IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iTypeLocaCD			IN VARCHAR2	,
		iTypeOperationCD	IN VARCHAR2	,
		iTypeRefCD			IN VARCHAR2 ,
		iRefNoID			IN NUMBER	,
		iMemoTx				IN VARCHAR2	,
		iNeedSendFL			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	
	/****************************************************/
	/*	入庫（ダイレクト入庫）							*/
	/****************************************************/
	PROCEDURE InStockDirectInStock(
		iSrcLocaStockCID	IN NUMBER	,
		iInStockID			IN VARCHAR2 ,	--iRefNoID
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2	
	);
	
	
	
	
	/****************************************************/
	/*													*/
	/*		出庫										*/
	/*													*/
	/****************************************************/
	/************************************************************************************/
	/*	在庫出庫（引当済数から一時引当済み）											*/
	/************************************************************************************/
	PROCEDURE OutStockTemp(
		iTrnID			IN NUMBER	,
		iTempLocaCD		IN VARCHAR2	,
		iTempKanbanCD	IN VARCHAR2	,
		iLocaStockCID	IN NUMBER	,
		iAmountNR		IN NUMBER	,
		iPalletNR		IN NUMBER	,
		iCaseNR			IN NUMBER	,
		iBaraNR			IN NUMBER	,
		iPcsNR			IN NUMBER	,
		iStStockID		IN NUMBER	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		orowLocaStockC	OUT TA_LOCASTOCKC%ROWTYPE
	);
	/************************************************************************************/
	/*	在庫出庫（一時引当済みから引当済数）											*/
	/************************************************************************************/
	PROCEDURE InStockTempOut(
		iTrnID			IN NUMBER	,
		iTempLocaCD		IN VARCHAR2	,
		iTempKanbanCD	IN VARCHAR2	,
		iLocaStockCID	IN NUMBER	,
		iAmountNR		IN NUMBER	,
		iPalletNR		IN NUMBER	,
		iCaseNR			IN NUMBER	,
		iBaraNR			IN NUMBER	,
		iPcsNR			IN NUMBER	,
		iStStockID		IN NUMBER	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		orowLocaStockC	OUT TA_LOCASTOCKC%ROWTYPE
	);
	/****************************************************/
	/*	入庫（受払なし）								*/
	/****************************************************/
	PROCEDURE InStockNoResult(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX 				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR 			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iCasePerPalletNR	IN NUMBER	,
		iPalletLayerNR		IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,
		iStackingNumberNR	IN NUMBER	,
		iTypePalletCD		IN VARCHAR2	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	
	
	
	
	/****************************************************/
	/*													*/
	/*		在庫調整									*/
	/*													*/
	/****************************************************/
	/****************************************************/
	/*	在庫変更チェック								*/
	/****************************************************/
	PROCEDURE ChkUpdStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2
	);
	/****************************************************/
	/*	在庫変更										*/
	/****************************************************/
	PROCEDURE UpdStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX 				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR 			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iTypeLocaCD			IN VARCHAR2	,
		iTypeOperationCD	IN VARCHAR2	,
		iTypeRefCD			IN VARCHAR2 ,
		iRefNoID			IN NUMBER	,
		iMemoTx				IN VARCHAR2	,
		iNeedSendFL			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	
	
	
	/****************************************************/
	/*													*/
	/*		在庫引当									*/
	/*													*/
	/****************************************************/
	/****************************************************/
	/*	在庫引当										*/
	/****************************************************/
	PROCEDURE ApplyStock(
		iTrnID				IN NUMBER	,
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchnoCd			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR 			IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iStStockID			IN NUMBER	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		orowLocaStockC		OUT TA_LOCASTOCKC%ROWTYPE
	);
	/****************************************************/
	/*	在庫戻し										*/
	/****************************************************/
	PROCEDURE ReApplyStock(
		iLocaStockCID	IN NUMBER		,
		iAmountNR		IN NUMBER		,
		iUserCD			IN VARCHAR2		,
		iUserTX			IN VARCHAR2
	);
	
	/****************************************************/
	/*	入庫引当										*/
	/****************************************************/
	PROCEDURE ApplyInStock(
		iTrnID				IN NUMBER	,
		iStStockID			IN NUMBER	,
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iCasePerPalletNR	IN NUMBER	,
		iPalletLayerNR		IN NUMBER	,
		iPalletCapacityNR	IN NUMBER	,	-- パレット積載数
		iStackingNumberNR	IN NUMBER	,
		iTypePalletCD		IN VARCHAR2	,
		iUseLimitDT 		IN DATE		,
		iBatchNoCD			IN VARCHAR2	,
		iStockDT			IN DATE		,
		iAmountNR			IN NUMBER	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeStockCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iUserTX				IN VARCHAR2	,
		orowLocaStockC		OUT TA_LOCASTOCKC%ROWTYPE
	);
	/****************************************************/
	/*													*/
	/*		かんばん									*/
	/*													*/
	/****************************************************/
	/****************************************************/
	/*	かんばん引き落とし								*/
	/*	かんばんコードをキーにしてロケを検索しする		*/
	/*	結果としてかんばんに紐付く在庫が存在しない場合は*/
	/*	かんばんを再度利用可にする						*/
	/****************************************************/
	PROCEDURE DelKanban(
		iKanbanCD 			IN VARCHAR2	
	);

END PS_STOCK;
/
SHOW ERRORS
