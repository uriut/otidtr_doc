-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_MASTER AS
	/************************************************************/
	/*	ユーザーマスタ取得										*/
	/************************************************************/
	PROCEDURE GetUser(
		iUserCD		IN	VARCHAR2		,
		orowUser	OUT	VA_USER%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40) := 'GetUser';
		
		CURSOR curUser(UserCD VA_USER.USER_CD%TYPE) IS
			SELECT
				*
			FROM
				VA_USER
			WHERE
				USER_CD = UserCD;
		rowUser curUser%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curUser(iUserCD);
		FETCH curUser INTO rowUser;
		IF curUser%FOUND THEN
			orowUser := rowUser;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ユーザマスタのデータが見つかりません。');
		END IF;
		CLOSE curUser;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curUser%ISOPEN THEN CLOSE curUser; END IF;
			RAISE;
	END GetUser;
	/************************************************************/
	/*	SKUマスタ取得											*/
	/************************************************************/
	PROCEDURE GetSku(
		iSkuCD		IN 	VARCHAR2		,
		orowSku		OUT	MA_SKU%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40) := 'GetSku';
		-- 商品情報カーソル
		CURSOR curSku(SkuCD MA_SKU.SKU_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_SKU
			WHERE
				SKU_CD	= SkuCD;
		rowSku curSku%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curSku(iSkuCD);
		FETCH curSku INTO rowSku;
		IF curSku%FOUND THEN
			orowSku := rowSku;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '商品マスタのデータが見つかりません。[' || iSkuCD || ']');
		END IF;
		CLOSE curSku;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curSku%ISOPEN THEN CLOSE curSku; END IF;
			RAISE;
	END GetSku;
	/************************************************************/
	/*	SKUマスタ取得											*/
	/************************************************************/
	--TODO 一意に定まらない場合
	PROCEDURE GetSkuFromBarcode(
		iSkuBarcodeTX	IN 	VARCHAR2		,
		orowSku			OUT	MA_SKU%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40) := 'GetSkuFromBarcode';
		-- 商品情報カーソル
		CURSOR curSku(SkuBarcodeTX MA_SKU.SKU_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_SKU
			WHERE
				SKUBARCODE_TX	= SkuBarcodeTX;
		rowSku curSku%ROWTYPE;
		numFoundFL	NUMBER(1) := 0;
		chrCodeTX	VARCHAR2(20);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curSku(iSkuBarcodeTX);
		FETCH curSku INTO rowSku;
		IF curSku%FOUND THEN
			orowSku := rowSku;
			numFoundFL := 1;
		END IF;
		CLOSE curSku;
		IF numFoundFL = 0 AND LENGTH(iSkuBarcodeTX) < 13 THEN
			chrCodeTX := LPAD(iSkuBarcodeTX, 13, '0');
			OPEN curSku(chrCodeTX);
			FETCH curSku INTO rowSku;
			IF curSku%FOUND THEN
				orowSku := rowSku;
				numFoundFL := 1;
			END IF;
			CLOSE curSku;
		END IF;
		IF numFoundFL = 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '商品マスタのデータが見つかりません。');
		END IF;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curSku%ISOPEN THEN CLOSE curSku; END IF;
			RAISE;
	END GetSkuFromBarcode;
	/************************************************************/
	/*	SKUマスタ取得（ITFバーコードver）						*/
	/************************************************************/
	--TODO 一意に定まらない場合
	PROCEDURE GetSkuFromBarcodeITF(
		iSkuBarcodeTX	IN 	VARCHAR2		,
		orowSku			OUT	MA_SKU%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40) := 'GetSkuFromBarcodeITF';
		-- 商品情報カーソル
		CURSOR curSku(SkuBarcodeTX MA_SKU.SKU_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_SKU
			WHERE
				ITFCODE_TX	= SkuBarcodeTX;
		rowSku curSku%ROWTYPE;
		numFoundFL	NUMBER(1) := 0;
		chrCodeTX	VARCHAR2(20);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curSku(iSkuBarcodeTX);
		FETCH curSku INTO rowSku;
		IF curSku%FOUND THEN
			orowSku := rowSku;
			numFoundFL := 1;
		END IF;
		CLOSE curSku;
		IF numFoundFL = 0 AND LENGTH(iSkuBarcodeTX) < 13 THEN
			chrCodeTX := LPAD(iSkuBarcodeTX, 13, '0');
			OPEN curSku(chrCodeTX);
			FETCH curSku INTO rowSku;
			IF curSku%FOUND THEN
				orowSku := rowSku;
				numFoundFL := 1;
			END IF;
			CLOSE curSku;
		END IF;
		IF numFoundFL = 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '商品マスタのデータが見つかりません。');
		END IF;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curSku%ISOPEN THEN CLOSE curSku; END IF;
			RAISE;
	END GetSkuFromBarcodeITF;
	/************************************************************/
	/*	SKUパレットマスタ取得									*/
	/************************************************************/
	PROCEDURE GetSkuPallet(
		iSkuCD			IN 	VARCHAR2		,
		iManuFacturerCD	IN 	VARCHAR2		,
		orowSkuPallet	OUT	MA_SKUPALLET%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40) := 'GetSkuPallet';
		-- 商品情報カーソル
		CURSOR curSkuPallet(SkuCD MA_SKUPALLET.SKU_CD%TYPE,ManuFacturerCD MA_SKUPALLET.MANUFACTURER_CD%TYPE) IS
			SELECT DISTINCT
				*
			FROM
				MA_SKUPALLET
			WHERE
				SKU_CD	= SkuCD AND
				MANUFACTURER_CD = ManuFacturerCD;
		rowSkuPallet curSkuPallet%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curSkuPallet(iSkuCD,iManuFacturerCD);
		FETCH curSkuPallet INTO rowSkuPallet;
		IF curSkuPallet%FOUND THEN
			orowSkuPallet := rowSkuPallet;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'SKUパレットマスタのデータが見つかりません。[' || iSkuCD || ']');
		END IF;
		CLOSE curSkuPallet;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curSkuPallet%ISOPEN THEN CLOSE curSkuPallet; END IF;
			RAISE;
	END GetSkuPallet;
	/************************************************************/
	/*	SKUパレットマスタ取得									*/
	/************************************************************/
	PROCEDURE GetSkuPallet(
		iSkuCD			IN 	VARCHAR2		,
		orowSkuPallet	OUT	MA_SKUPALLET%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40) := 'GetSkuPallet';
		-- 商品情報カーソル
		CURSOR curSkuPallet(SkuCD MA_SKUPALLET.SKU_CD%TYPE) IS
			SELECT DISTINCT
				*
			FROM
				MA_SKUPALLET
			WHERE
				SKU_CD	= SkuCD;
		rowSkuPallet curSkuPallet%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curSkuPallet(iSkuCD);
		FETCH curSkuPallet INTO rowSkuPallet;
		IF curSkuPallet%FOUND THEN
			orowSkuPallet := rowSkuPallet;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'SKUパレットマスタのデータが見つかりません。[' || iSkuCD || ']');
		END IF;
		CLOSE curSkuPallet;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curSkuPallet%ISOPEN THEN CLOSE curSkuPallet; END IF;
			RAISE;
	END GetSkuPallet;
	/************************************************************/
	/*	ロケーションマスタ取得									*/
	/************************************************************/
	PROCEDURE GetLoca (
		iLocaCd		IN	VARCHAR2		,
		orowLoca	OUT	MA_LOCA%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40) := 'GetLoca';
		
		CURSOR curLoca (LocaCd MA_LOCA.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_LOCA
			WHERE
				LOCA_CD	= LocaCd;
		rowLoca	curLoca%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curLoca(iLocaCd);
		FETCH curLoca INTO rowLoca;
		IF curLoca%FOUND THEN
			orowLoca := rowLoca;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーションマスタのデータが見つかりません。');
		END IF;
		CLOSE curLoca;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLoca%ISOPEN THEN CLOSE curLoca; END IF;
			RAISE;
	END GetLoca;
	/************************************************************/
	/*	ロケーションブロックマスタ取得							*/
	/************************************************************/
	PROCEDURE GetTypeLocaBlock(
		iTypeLocaBlockCD	IN 	VARCHAR2,
		orowTypeLocaBlock	OUT MB_TYPELOCABLOCK%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40) := 'GetTypeLocaBlock';
		
		CURSOR curTypeLocaBlock(TypeLocaBlock MB_TYPELOCABLOCK.TYPELOCABLOCK_CD%TYPE) IS
			SELECT
				*
			FROM
				MB_TYPELOCABLOCK
			WHERE
				TYPELOCABLOCK_CD = TypeLocaBlock;
		rowTypeLocaBlock curTypeLocaBlock%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curTypeLocaBlock(iTypeLocaBlockCD);
		FETCH curTypeLocaBlock INTO rowTypeLocaBlock;
		IF curTypeLocaBlock%FOUND THEN
			orowTypeLocaBlock := rowTypeLocaBlock;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーションブロックマスタのデータが見つかりません。');
		END IF;
		CLOSE curTypeLocaBlock;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTypeLocaBlock%ISOPEN THEN CLOSE curTypeLocaBlock; END IF;
			RAISE;
	END GetTypeLocaBlock;
	/************************************************************/
	/*	ロケーションタイプマスタ取得							*/
	/************************************************************/
	PROCEDURE GetTypeLoca(
		iTypeLocaCD		IN 	VARCHAR2,
		orowTypeLoca	OUT MB_TYPELOCA%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40) := 'GetTypeLoca';
		CURSOR curTypeLoca(TypeLocaCD MB_TYPELOCA.TYPELOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				MB_TYPELOCA
			WHERE
				TYPELOCA_CD = TypeLocaCD;
		rowTypeLoca curTypeLoca%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curTypeLoca(iTypeLocaCD);
		FETCH curTypeLoca INTO rowTypeLoca;
		IF curTypeLoca%FOUND THEN
			orowTypeLoca := rowTypeLoca;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'ロケーションタイプマスターのデータが見つかりません。');
		END IF;
		CLOSE curTypeLoca;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTypeLoca%ISOPEN THEN CLOSE curTypeLoca; END IF;
			RAISE;
	END GetTypeLoca;
	/************************************************************/
	/*	業務区分取得											*/
	/************************************************************/
	PROCEDURE GetTypeOperation(
		iTypeOperationCD	IN 	VARCHAR2,
		orowTypeOperation	OUT MB_TYPEOPERATION%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetTypeOperation';
		CURSOR curTypeOperation(TypeOperationCD MB_TYPEOPERATION.TYPEOPERATION_CD%TYPE) IS
			SELECT
				*
			FROM
				MB_TYPEOPERATION
			WHERE
				TYPEOPERATION_CD = TypeOperationCD;
		rowTypeOperation curTypeOperation%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curTypeOperation(iTypeOperationCD);
		FETCH curTypeOperation INTO rowTypeOperation;
		IF curTypeOperation%FOUND THEN
			orowTypeOperation := rowTypeOperation;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '業務区分マスタのデータがみつかりません。');
		END IF;
		CLOSE curTypeOperation;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTypeOperation%ISOPEN THEN
				CLOSE curTypeOperation;
			END IF;
			RAISE;
	END GetTypeOperation;
	/************************************************************/
	/*	エリアマスタ取得										*/
	/************************************************************/
	PROCEDURE GetArea(
		iAreaCD		IN 	VARCHAR2,
		orowArea	OUT MB_AREA%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetArea';
		CURSOR curArea(AreaCD MB_AREA.AREA_CD%TYPE) IS
			SELECT
				*
			FROM
				MB_AREA
			WHERE
				AREA_CD = AreaCD;
		rowArea curArea%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curArea(iAreaCD);
		FETCH curArea INTO rowArea;
		IF curArea%FOUND THEN
			orowArea := rowArea;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '業務区分マスタのデータがみつかりません。');
		END IF;
		CLOSE curArea;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curArea%ISOPEN THEN
				CLOSE curArea;
			END IF;
			RAISE;
	END GetArea;
END PS_MASTER;
/
SHOW ERRORS
