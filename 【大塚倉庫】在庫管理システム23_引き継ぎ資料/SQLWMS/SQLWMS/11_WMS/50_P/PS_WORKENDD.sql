-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_WORKEND AS
	/************************************************************************************/
	/*	日締め																			*/
	/************************************************************************************/
	PROCEDURE WorkEnd AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'WorkEnd';
		numRowPoint 	NUMBER := 0;
		dtmCommDate		DATE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		dtmCommDate	:= TRUNC(SYSDATE);

		DBMS_OUTPUT.PUT_LINE('WorkEnd Start :'|| TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS'));
		--LOG関連
		--T_LOGHT
		DELETE
			TA_LOGHT
		WHERE
			ADD_DT <= dtmCommDate - 2;
		DBMS_OUTPUT.PUT_LINE('DELETE TA_LOGHT :'||SQL%ROWCOUNT);
		COMMIT;
		
		--TLOG
		DELETE
			LOG.TLOG
		WHERE
			LDATE <= dtmCommDate - 2;
		DBMS_OUTPUT.PUT_LINE('DELETE LOG.TLOG :'||SQL%ROWCOUNT);
		COMMIT;
		
		--履歴関連
		--履歴登録日よりXXX日以前のデータを削除
		--当面削除しない
/*
		--HIS_ARRIV
		DELETE
			HIS_ARRIV
		WHERE
			REGIST_DT <= dtmCommDate -200;
		DBMS_OUTPUT.PUT_LINE('DELETE HIS_ARRIV :'||SQL%ROWCOUNT);
		COMMIT;
		--HIS_INSTOCK
		DELETE
			HIS_INSTOCK
		WHERE
			REGIST_DT <= dtmCommDate -200;
		DBMS_OUTPUT.PUT_LINE('DELETE HIS_INSTOCK :'||SQL%ROWCOUNT);
		COMMIT;
		--HIS_MOVE
		DELETE
			HIS_MOVE
		WHERE
			REGIST_DT <= dtmCommDate -200;
		DBMS_OUTPUT.PUT_LINE('DELETE HIS_MOVE :'||SQL%ROWCOUNT);
		COMMIT;
		--HIS_SUPP
		DELETE
			HIS_SUPP
		WHERE
			REGIST_DT <= dtmCommDate -200;
		DBMS_OUTPUT.PUT_LINE('DELETE HIS_SUPP :'||SQL%ROWCOUNT);
		COMMIT;
		--HIS_PICK
		DELETE
			HIS_PICK
		WHERE
			REGIST_DT <= dtmCommDate -400;
		DBMS_OUTPUT.PUT_LINE('DELETE HIS_PICK :'||SQL%ROWCOUNT);
		COMMIT;
		--HIS_SHIPD
		DELETE
			HIS_SHIPD
		WHERE
			REGIST_DT <= dtmCommDate -400;
		DBMS_OUTPUT.PUT_LINE('DELETE HIS_SHIPD :'||SQL%ROWCOUNT);
		COMMIT;
		--HIS_SHIPH
		DELETE
			HIS_SHIPH
		WHERE
			REGIST_DT <= dtmCommDate -400;
		DBMS_OUTPUT.PUT_LINE('DELETE HIS_SHIPH :'||SQL%ROWCOUNT);
		COMMIT;
*/		
		--入荷関連（更新日が1ヶ月前で完了のデータ）
		PS_WORKEND.EndArriv(dtmCommDate);
		COMMIT;
		--移動指示（更新日が1ヶ月前で完了のデータ）
		PS_WORKEND.EndMove(dtmCommDate);
		COMMIT;
		--出荷関連（更新日が1ヶ月前で完了のデータ）
		PS_WORKEND.EndShip(dtmCommDate);
		COMMIT;
		
		DBMS_OUTPUT.PUT_LINE('WorkEnd End :'|| TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS'));
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Err WorkEnd :' ||SQLERRM);
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END WorkEnd;
	/************************************************************************************/
	/*	入荷予定移動																	*/
	/************************************************************************************/
	PROCEDURE EndArriv(
		iCommDate	IN DATE
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'EndArriv';
		numRowPoint 	NUMBER := 0;
		dtDelDate		DATE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 削除日
		dtDelDate := ADD_MONTHS(iCommDate, -1 );
		
		DBMS_OUTPUT.PUT_LINE('EndArriv Start :'|| TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS'));
		-- 1ヶ月前の予定を更新
		-- 無条件で対象にしてよい？
		UPDATE 
			TA_ARRIV
		SET
			STRECORD_ID = 1
		WHERE
			UPD_DT <= dtDelDate;
		DBMS_OUTPUT.PUT_LINE('UPDATE TA_ARRIV :'||SQL%ROWCOUNT);
		--HIS_ARRIV登録
		INSERT INTO HIS_ARRIV
		(
			ARRIV_ID			,
			TRN_ID				,
			ARRIVORDERNO_CD		,
			ARRIVORDERDETNO_CD	,
			TYPEARRIV_CD		,
			STARRIV_ID			,
			-- 発店
			SUPPLIER_CD			,
			SUPPLIER_TX			,
			-- 着店
			OFFICE_CD			,
			OFFICE_TX			,
			--
			MANUFACTURER_CD		,
			TRANSPORTER_CD		,
			TRANSPORTER_TX		,
			CARNUMBER_CD		,
			SLIPNO_TX			,
			SLIPDATE_DT			,
			OWNER_CD			,
			--
			SKU_CD				,
			LOT_TX				,
			CASEPERPALLET_NR	,
			PALLETLAYER_NR		,
			CASEPERLAYER_NR		,
			PALLETCAPACITY_NR	,
			STACKINGNUMBER_NR	,
			TYPEPLALLET_CD		,
			ARRIVPLAN_DT		,
			ARRIVPLAN2_DT		,
			ARRIVPLANPALLET_NR	,
			ARRIVPLANCASE_NR	,
			ARRIVPLANBARA_NR	,
			ARRIVPLANPCS_NR		,
			DRIVERTEL_TX		,
			DRIVER_TX			,
			BERTH_TX			,
			SAMEDAYSHIP_FL		,
			MEMO_TX				,
			CHECK_DT			,
			ARRIVEND_DT			,
			ARRIVENDUSER_CD		,
			ARRIVENDUSER_TX		,
			CHECK_TX			,
			CHECKUSER_CD		,
			CHECKUSER_TX		,
			CHECKHT_ID			,
			KANBAN_CD			,
			CHECKHTPCS_NR		,
			ARRIVRSLT_DT		,
			ARRIVRSLTUSER_CD	,
			ARRIVRSLTUSER_TX	,
			ARRIVRSLTHT_ID		,
			GOODPCS_NR			,
			NGPCS_NR			,
			SAMPLEPCS_NR		,
			TYPEENTRY_CD		,
			-- 管理項目
			UPD_DT				,
			UPDUSER_CD			,
			UPDUSER_TX			,
			ADD_DT				,
			ADDUSER_CD			,
			ADDUSER_TX			,
			UPDPROGRAM_CD		,
			UPDCOUNTER_NR		,
			STRECORD_ID			,
			REGIST_DT			
		)
		SELECT
			ARRIV_ID			,
			TRN_ID				,
			ARRIVORDERNO_CD		,
			ARRIVORDERDETNO_CD	,
			TYPEARRIV_CD		,
			STARRIV_ID			,
			-- 発店
			SUPPLIER_CD			,
			SUPPLIER_TX			,
			-- 着店
			OFFICE_CD			,
			OFFICE_TX			,
			--
			MANUFACTURER_CD		,
			TRANSPORTER_CD		,
			TRANSPORTER_TX		,
			CARNUMBER_CD		,
			SLIPNO_TX			,
			SLIPDATE_DT			,
			OWNER_CD			,
			--
			SKU_CD				,
			LOT_TX				,
			CASEPERPALLET_NR	,
			PALLETLAYER_NR		,
			CASEPERLAYER_NR		,
			PALLETCAPACITY_NR	,
			STACKINGNUMBER_NR	,
			TYPEPLALLET_CD		,
			ARRIVPLAN_DT		,
			ARRIVPLAN2_DT		,
			ARRIVPLANPALLET_NR	,
			ARRIVPLANCASE_NR	,
			ARRIVPLANBARA_NR	,
			ARRIVPLANPCS_NR		,
			DRIVERTEL_TX		,
			DRIVER_TX			,
			BERTH_TX			,
			SAMEDAYSHIP_FL		,
			MEMO_TX				,
			CHECK_DT			,
			ARRIVEND_DT			,
			ARRIVENDUSER_CD		,
			ARRIVENDUSER_TX		,
			CHECK_TX			,
			CHECKUSER_CD		,
			CHECKUSER_TX		,
			CHECKHT_ID			,
			KANBAN_CD			,
			CHECKHTPCS_NR		,
			ARRIVRSLT_DT		,
			ARRIVRSLTUSER_CD	,
			ARRIVRSLTUSER_TX	,
			ARRIVRSLTHT_ID		,
			GOODPCS_NR			,
			NGPCS_NR			,
			SAMPLEPCS_NR		,
			TYPEENTRY_CD		,
			-- 管理項目
			UPD_DT				,
			UPDUSER_CD			,
			UPDUSER_TX			,
			ADD_DT				,
			ADDUSER_CD			,
			ADDUSER_TX			,
			UPDPROGRAM_CD		,
			UPDCOUNTER_NR		,
			STRECORD_ID			,
			iCommDate
		FROM
			TA_ARRIV
		WHERE
			STRECORD_ID = 1
		;
		DBMS_OUTPUT.PUT_LINE('INSERT HIS_ARRIV :'||SQL%ROWCOUNT);

		--HIS_INSTOCK登録
		INSERT INTO HIS_INSTOCK
			(
				INSTOCK_ID				,
				TRN_ID					,
				INSTOCKNO_CD			,
				TYPEINSTOCK_CD			,
				TYPESTOCK_CD			,
				START_DT				,
				END_DT					,
				STINSTOCK_ID			,
				-- 入荷予定
				ARRIV_ID				,
				-- 計上
				APPLYINSTOCKNPALLET_NR	,
				APPLYINSTOCKNCASE_NR	,
				APPLYINSTOCKNBARA_NR	,
				APPLYINSTOCKNPCS_NR		,
				APPLYINSTOCK_DT			,
				APPLYINSTOCKUSER_CD		,
				APPLYINSTOCKUSER_TX		,
				SKU_CD					,
				LOT_TX					,
				KANBAN_CD				,
				CASEPERPALLET_NR		,
				PALLETLAYER_NR			,
				PALLETCAPACITY_NR		,
				STACKINGNUMBER_NR		,
				TYPEPLALLET_CD			,
				STOCK_DT				,
				AMOUNT_NR				,
				PALLET_NR				,
				CASE_NR					,
				BARA_NR					,
				PCS_NR					,
				-- 先ロケ
				DESTTYPESTOCK_CD		,
				DESTLOCA_CD				,
				RESULTLOCA_CD			,
				SHIPVOLUME_CD			,
				DESTSTART_DT			,
				DESTEND_DT				,
				DESTHT_ID				,
				DESTHTUSER_CD			,
				DESTHTUSER_TX			,
				DESTLOCASTOCKC_ID		,
				-- 印刷
				PRTCOUNT_NR				,
				PRT_DT					,
				PRTUSER_CD				,
				PRTUSER_TX				,
				BATCHNO_CD				,
				--
				TYPEAPPLY_CD			,
				TYPEBLOCK_CD			,
				TYPECARRY_CD			,
				-- 入荷実績
				ARRIVNEEDSEND_FL		,
				ARRIVSENDEND_FL			,
				ARRIVSEND_DT			,
				--
				INSTOCKNEEDSEND_FL		,
				INSTOCKSENDEND_FL		,
				INSTOCKSEND_DT			,
				-- 管理項目
				UPD_DT					,
				UPDUSER_CD				,
				UPDUSER_TX				,
				ADD_DT					,
				ADDUSER_CD				,
				ADDUSER_TX				,
				UPDPROGRAM_CD			,
				UPDCOUNTER_NR			,
				STRECORD_ID				,
				REGIST_DT				
			)
			SELECT
				INSTOCK_ID				,
				TRN_ID					,
				INSTOCKNO_CD			,
				TYPEINSTOCK_CD			,
				TYPESTOCK_CD			,
				START_DT				,
				END_DT					,
				STINSTOCK_ID			,
				-- 入荷予定
				ARRIV_ID				,
				-- 計上
				APPLYINSTOCKNPALLET_NR	,
				APPLYINSTOCKNCASE_NR	,
				APPLYINSTOCKNBARA_NR	,
				APPLYINSTOCKNPCS_NR		,
				APPLYINSTOCK_DT			,
				APPLYINSTOCKUSER_CD		,
				APPLYINSTOCKUSER_TX		,
				SKU_CD					,
				LOT_TX					,
				KANBAN_CD				,
				CASEPERPALLET_NR		,
				PALLETLAYER_NR			,
				PALLETCAPACITY_NR		,
				STACKINGNUMBER_NR		,
				TYPEPLALLET_CD			,
				STOCK_DT				,
				AMOUNT_NR				,
				PALLET_NR				,
				CASE_NR					,
				BARA_NR					,
				PCS_NR					,
				-- 先ロケ
				DESTTYPESTOCK_CD		,
				DESTLOCA_CD				,
				RESULTLOCA_CD			,
				SHIPVOLUME_CD			,
				DESTSTART_DT			,
				DESTEND_DT				,
				DESTHT_ID				,
				DESTHTUSER_CD			,
				DESTHTUSER_TX			,
				DESTLOCASTOCKC_ID		,
				-- 印刷
				PRTCOUNT_NR				,
				PRT_DT					,
				PRTUSER_CD				,
				PRTUSER_TX				,
				BATCHNO_CD				,
				--
				TYPEAPPLY_CD			,
				TYPEBLOCK_CD			,
				TYPECARRY_CD			,
				-- 入荷実績
				ARRIVNEEDSEND_FL		,
				ARRIVSENDEND_FL			,
				ARRIVSEND_DT			,
				--
				INSTOCKNEEDSEND_FL		,
				INSTOCKSENDEND_FL		,
				INSTOCKSEND_DT			,
				-- 管理項目
				UPD_DT					,
				UPDUSER_CD				,
				UPDUSER_TX				,
				ADD_DT					,
				ADDUSER_CD				,
				ADDUSER_TX				,
				UPDPROGRAM_CD			,
				UPDCOUNTER_NR			,
				STRECORD_ID				,
				iCommDate
			FROM
				TA_INSTOCK
			WHERE
				ARRIV_ID IN (
							SELECT
								ARRIV_ID
							FROM
								TA_ARRIV
							WHERE
								STRECORD_ID = 1
							);
		DBMS_OUTPUT.PUT_LINE('INSERT HIS_INSTOCK :'||SQL%ROWCOUNT);
		--T_INSTOCK削除
		DELETE
			TA_INSTOCK
		WHERE
			ARRIV_ID IN (
						SELECT
							ARRIV_ID
						FROM
							TA_ARRIV
						WHERE
							STRECORD_ID = 1
						);
		DBMS_OUTPUT.PUT_LINE('DELETE TA_INSTOCK :'||SQL%ROWCOUNT);
		
		--T_ARRIV削除
		DELETE
			TA_ARRIV
		WHERE
			STRECORD_ID = 1
		;
		DBMS_OUTPUT.PUT_LINE('DELETE TA_ARRIV :'||SQL%ROWCOUNT);
		
		DBMS_OUTPUT.PUT_LINE('EndArriv End :'|| TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS'));
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Err EndArriv :' ||SQLERRM);
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END EndArriv;
	/************************************************************************************/
	/*	移動指示履歴																	*/
	/************************************************************************************/
	PROCEDURE EndMove(
		iCommDate	IN DATE
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'EndMove';
		numRowPoint 	NUMBER := 0;
		dtDelDate		DATE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		DBMS_OUTPUT.PUT_LINE('EndMove Start :'|| TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS'));
		-- 削除日
		dtDelDate := ADD_MONTHS(iCommDate, -1 );
		-- 1ヶ月前の指示、またはキャンセル分を更新
		UPDATE 
			TA_MOVE
		SET
			STRECORD_ID = 1
		WHERE
			( 
				END_DT <= dtDelDate OR
				STMOVE_ID = PS_DEFINE.ST_MOVEM2_CANCEL
			);
		--
		INSERT INTO HIS_MOVE
			SELECT
				MOVE_ID				,
				TRN_ID				,
				MOVENO_CD			,
				TYPEMOVE_CD			,
				START_DT			,
				END_DT				,
				STMOVE_ID			,
				-- SKU
				SKU_CD				,
				LOT_TX				,
				SRCKANBAN_CD		,
				DESTKANBAN_CD		,
				CASEPERPALLET_NR	,
				PALLETLAYER_NR		,
				PALLETCAPACITY_NR	,
				STACKINGNUMBER_NR	,
				TYPEPLALLET_CD		,
				USELIMIT_DT			,
				STOCK_DT			,
				AMOUNT_NR			,
				PALLET_NR			,
				CASE_NR				,
				BARA_NR				,
				PCS_NR				,
				-- 元ロケ
				SRCTYPESTOCK_CD		,
				SRCLOCA_CD			,
				SRCSTART_DT			,
				SRCEND_DT			,
				SRCHT_ID			,
				SRCHTUSER_CD		,
				SRCHTUSER_TX		,
				SRCLOCASTOCKC_ID	,
				SRCPALLET_NR		,
				SRCCASE_NR			,
				SRCBARA_NR			,
				SRCPCS_NR			,
				-- 先ロケ
				DESTTYPESTOCK_CD	,
				DESTLOCA_CD			,
				DESTSTART_DT		,
				DESTEND_DT			,
				DESTHT_ID			,
				DESTHTUSER_CD		,
				DESTHTUSER_TX		,
				DESTLOCASTOCKC_ID	,
				DESTPALLET_NR		,
				DESTCASE_NR			,
				DESTBARA_NR			,
				DESTPCS_NR			,
				-- 印刷
				PRTCOUNT_NR			,
				PRT_DT				,
				PRTUSER_CD			,
				PRTUSER_TX			,
				--
				BATCHNO_CD			,
				TYPEAPPLY_CD		,
				TYPEBLOCK_CD		,
				TYPECARRY_CD		,
				-- 管理項目
				UPD_DT				,
				UPDUSER_CD			,
				UPDUSER_TX			,
				ADD_DT				,
				ADDUSER_CD			,
				ADDUSER_TX			,
				UPDPROGRAM_CD		,
				UPDCOUNTER_NR		,
				STRECORD_ID			,
				iCommDate
			FROM
				TA_MOVE
			WHERE
				STRECORD_ID = 1;
		DBMS_OUTPUT.PUT_LINE('INSERT HIS_MOVE :'||SQL%ROWCOUNT);
		--TA_MOVE削除
		DELETE
			TA_MOVE
		WHERE
			STRECORD_ID = 1;
		DBMS_OUTPUT.PUT_LINE('DELETE TA_MOVE :'||SQL%ROWCOUNT);
		
		DBMS_OUTPUT.PUT_LINE('EndMove End :'|| TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS'));
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Err EndMove :' ||SQLERRM);
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END EndMove;
	/************************************************************************************/
	/*	出荷指示履歴																	*/
	/************************************************************************************/
	PROCEDURE EndShip(
		iCommDate	IN DATE
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'EndShip';
		numRowPoint 	NUMBER := 0;
		dtDelDate		DATE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 削除日
		dtDelDate := ADD_MONTHS(iCommDate, -1 );
		-- 1ヶ月前の指示、またはキャンセル分を更新
		-- TA_PICK
		UPDATE
			TA_PICK
		SET
			STRECORD_ID = 1
		WHERE
			END_DT <= dtDelDate;
		-- TA_SUPP
		UPDATE
			TA_SUPP
		SET
			STRECORD_ID = 1
		WHERE
			PICK_ID IN (
						SELECT
							PICK_ID
						FROM
							TA_PICK
						WHERE
							STRECORD_ID = 1
						);
		-- TA_SHIPH
		UPDATE 
			TA_SHIPH
		SET
			STRECORD_ID = 1
		WHERE
			(
				SHIPH_ID IN (
							SELECT
								SHIPH_ID
							FROM
								TA_PICK
							WHERE
								STRECORD_ID = 1
							GROUP BY
								SHIPH_ID
							) OR
				STSHIP_ID = PS_DEFINE.ST_SHIPM2_CANCEL
			);
		-- TA_SHIPD
		UPDATE
			TA_SHIPD
		SET
			STRECORD_ID = 1
		WHERE
			SHIPH_ID IN (
						SELECT
							SHIPH_ID
						FROM
							TA_SHIPH
						WHERE
							STRECORD_ID = 1
						);
		
		DBMS_OUTPUT.PUT_LINE('EndShip Start :'|| TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS'));
		--HIS_SHIPH登録
		INSERT INTO HIS_SHIPH
			SELECT
				SHIPH_ID				,
				TRN_ID					,
				TYPESHIP_CD				,
				TYPESHIP_TX				,
				START_DT				,
				END_DT					,
				STSHIP_ID				,
				-- 出荷指示
				ORDERNO_CD				,
				ORDERNOSUB_CD			,
				CUSTOMERORDERNO_CD		,
				CUSTOMER_CD				,
				ORDERTYPE_CD			,
				ENTRYTYPE_CD			,
				ORDERDATE_DT			,
				SALESSHOP_CD			,
				OWNER_CD				,
				CHANNELTYPE_CD			,
				TRANSPORTER1_CD			,
				TRANSPORTER2_CD			,
				DELIVERYDESTINATION_CD	,
				DTNAME_TX				,
				DTPOSTAL_CD				,
				DTADDRESS1_TX			,
				DTADDRESS2_TX			,
				DTADDRESS3_TX			,
				DTADDRESS4_TX			,
				DIRECTDELIVERY_CD		,
				TYPEPLALLET_CD			,
				SHIPBERTH_CD			,
				SHIPPLAN_DT				,
				SLIPNO_TX				,
				CARRIER_CD				,
				DELITYPE_CD				,
				DELIDATE_TX				,
				DELITIME_CD				,
				CHARGETYPE_CD			,
				GIFTTYPE_CD				,
				TYPETEMPERATURE_CD		,
				TOTALITEMPRICE_NR		,
				TOTALPRICE_NR			,
				TOTALTAX_NR				,
				DELICHARGE_NR			,
				GIFTCHARGE_NR			,
				CODCHARGE_NR			,
				DISCOUNT_NR				,
				SALEDISCOUNT_NR			,
				COUPON_CD				,
				COUPONDISCOUNT_NR		,
				USEPOINT_NR				,
				ADDPOINT_NR				,
				CUSTOMERCOMMENT_TX		,
				CSCOMMENT_TX			,
				OPERATORCOMMENT_TX		,
				DETCOUNT_NR				,
				-- 納品書
				INVOICE_FL				,
				INVOICECOMMENT_TX		,
				PRTINVOICECOUNT_NR		,
				PRTINVOICE_DT			,
				PRTINVOICEUSER_CD		,
				PRTINVOICEUSER_TX		,
				-- 出荷伝票
				DELIBOX_FL				,
				DELIITEM_TX				,
				DELICOMMENT_TX			,
				PRTDELICOUNT_NR			,
				PRTDELI_DT				,
				PRTDELIUSER_CD			,
				PRTDELIUSER_TX			,
				--
				PACKNO1_CD				,
				PACKNO2_CD				,
				PACKNO3_CD				,
				PACKNO4_CD				,
				PACKNO5_CD				,
				PACKNO6_CD				,
				PACKNO7_CD				,
				PACKNO8_CD				,
				PACKNO9_CD				,
				PACKNO10_CD				,
				-- 管理項目
				UPD_DT					,
				UPDUSER_CD				,
				UPDUSER_TX				,
				ADD_DT					,
				ADDUSER_CD				,
				ADDUSER_TX				,
				UPDPROGRAM_CD			,
				UPDCOUNTER_NR			,
				STRECORD_ID				,
				iCommDate
			FROM
				TA_SHIPH
			WHERE
				STRECORD_ID = 1;
		DBMS_OUTPUT.PUT_LINE('INSERT HIS_SHIPH :'||SQL%ROWCOUNT);
		--HIS_SHIPD登録
		INSERT INTO HIS_SHIPD
			SELECT
				SHIPD_ID				,
				SHIPH_ID				,
				TRN_ID					,
				TYPESHIP_CD				,
				TYPESHIP_TX				,
				START_DT				,
				END_DT					,
				STSHIP_ID				,
				-- 出荷指示
				ORDERNO_CD				,
				ORDERNOSUB_CD			,
				ORDERDETNO_CD			,
				SKU_CD					,
				LOT_TX					,
				KANBAN_CD				,
				CASEPERPALLET_NR		,
				PALLETLAYER_NR			,
				STACKINGNUMBER_NR		,
				SKU_TX					,
				TYPELOCA_CD				,
				STOCKTYPE_CD			,
				PALLETCAPACITY_NR		,
				ORDERPALLET_NR			,
				ORDERCASE_NR			,
				ORDERBARA_NR			,
				ORDERPCS_NR				,
				UNITPRICE_NR			,
				UNITADDPOINT_NR			,
				BONUS_FL				,
				UNITSALEDISCOUNT_NR		,
				DETCOMMENT_TX			,
				TYPESHIPAPPLY_CD		,
				TYPESHIPROUTE_CD		,
				SKUTYPE_CD				,
				SUPPLYTYPE_CD			,
				SUPPLIER_CD				,
				SETSKUGROUPSEQ_NR		,
				PRINTDETTYPE_CD			,
				PRINTPICKLISTTYPE_CD	,
				-- 引当結果
				APPLY_DT				,
				APPLYUSER_CD			,
				APPLYUSER_TX			,
				--
				BATCHNO_CD				,
				TYPEAPPLY_CD			,
				TYPEBLOCK_CD			,
				TYPECARRY_CD			,
				-- 管理項目
				UPD_DT					,
				UPDUSER_CD				,
				UPDUSER_TX				,
				ADD_DT					,
				ADDUSER_CD				,
				ADDUSER_TX				,
				UPDPROGRAM_CD			,
				UPDCOUNTER_NR			,
				STRECORD_ID				,
				iCommDate
			FROM
				TA_SHIPD
			WHERE
				STRECORD_ID = 1;
		DBMS_OUTPUT.PUT_LINE('INSERT HIS_SHIPD :'||SQL%ROWCOUNT);

		--TA_SHIPD削除
		DELETE
			TA_SHIPD
		WHERE
			STRECORD_ID = 1;
		--TA_SHIPH削除
		DELETE
			TA_SHIPH
		WHERE
			STRECORD_ID = 1;
		DBMS_OUTPUT.PUT_LINE('DELETE TA_SHIPH :'||SQL%ROWCOUNT);
		--HIS_SUPP登録
		INSERT INTO HIS_SUPP
			SELECT
				SUPP_ID				,
				TRN_ID				,
				SUPPNO_CD			,
				TYPESUPP_CD			,
				START_DT			,
				END_DT				,
				STSUPP_ID			,
				-- 出荷指示
				SHIPH_ID			,
				SHIPD_ID			,
				-- ピック
				PICK_ID				,
				--
				SKU_CD				,
				LOT_TX				,
				SRCKANBAN_CD		,
				DESTKANBAN_CD		,
				CASEPERPALLET_NR	,
				PALLETLAYER_NR		,
				PALLETCAPACITY_NR	,
				STACKINGNUMBER_NR	,
				TYPEPLALLET_CD		,
				USELIMIT_DT			,
				STOCK_DT			,
				AMOUNT_NR			,
				PALLET_NR			,
				CASE_NR				,
				BARA_NR				,
				PCS_NR				,
				-- 元ロケ
				SRCTYPESTOCK_CD		,
				SRCLOCA_CD			,
				SRCSTART_DT			,
				SRCEND_DT			,
				SRCHT_ID			,
				SRCHTUSER_CD		,
				SRCHTUSER_TX		,
				SRCLOCASTOCKC_ID	,
				SRCPALLET_NR		,
				SRCCASE_NR			,
				SRCBARA_NR			,
				SRCPCS_NR			,
				-- 先ロケ
				DESTTYPESTOCK_CD	,
				DESTLOCA_CD			,
				DESTSTART_DT		,
				DESTEND_DT			,
				DESTHT_ID			,
				DESTHTUSER_CD		,
				DESTHTUSER_TX		,
				DESTLOCASTOCKC_ID	,
				DESTPALLET_NR		,
				DESTCASE_NR			,
				DESTBARA_NR			,
				DESTPCS_NR			,
				-- 印刷
				PRTCOUNT_NR			,
				PRT_DT				,
				PRTUSER_CD			,
				PRTUSER_TX			,
				--
				BATCHNO_CD			,
				TYPEAPPLY_CD		,
				TYPEBLOCK_CD		,
				TYPECARRY_CD		,
				-- 管理項目
				UPD_DT				,
				UPDUSER_CD			,
				UPDUSER_TX			,
				ADD_DT				,
				ADDUSER_CD			,
				ADDUSER_TX			,
				UPDPROGRAM_CD		,
				UPDCOUNTER_NR		,
				STRECORD_ID			,
				iCommDate
			FROM
				TA_SUPP
			WHERE
				STRECORD_ID = 1;
		DBMS_OUTPUT.PUT_LINE('INSERT HIS_SUPP :'||SQL%ROWCOUNT);
		--TA_SUPP削除
		DELETE
			TA_SUPP
		WHERE
			STRECORD_ID = 1;
		DBMS_OUTPUT.PUT_LINE('DELETE TA_SUPP :'||SQL%ROWCOUNT);
		
		--HIS_PICK登録
		INSERT INTO HIS_PICK
			SELECT
				PICK_ID					,
				TRN_ID					,
				PICKNO_CD				,
				TYPEPICK_CD				,
				START_DT				,
				END_DT					,
				STPICK_ID				,
				-- 出荷指示
				SHIPH_ID				,
				SHIPD_ID				,
				ORDERNO_CD				,
				ORDERNOSUB_CD			,
				ORDERDETNO_CD			,
				-- SKU
				SKU_CD					,
				LOT_TX					,
				KANBAN_CD				,
				CASEPERPALLET_NR		,
				PALLETLAYER_NR			,
				PALLETCAPACITY_NR		,
				STACKINGNUMBER_NR		,
				TYPEPLALLET_CD			,
				PALLETDETNO_CD			,
				TRANSPORTER_CD			,
				DELIVERYDESTINATION_CD	,
				-- 軒先ピック対応
				DTNAME_TX				,
				DTADDRESS1_TX			,
				DTADDRESS2_TX			,
				DTADDRESS3_TX			,
				DTADDRESS4_TX			,
				DIRECTDELIVERY_CD		,
				DELIDATE_TX      		,
				TOTALPIC_FL				,
				--
				SHIPBERTH_CD			,
				SHIPPLAN_DT				,
				BERTHTIME_TX			,
				SLIPNO_TX				,
				--
				USELIMIT_DT				,
				STOCK_DT				,
				AMOUNT_NR				,
				PALLET_NR				,
				CASE_NR					,
				BARA_NR					,
				PCS_NR					,
				-- 元ロケ
				DEFER_FL				,
				SRCTYPESTOCK_CD			,
				SRCLOCA_CD				,
				SRCSTART_DT				,
				SRCEND_DT				,
				SRCHT_ID				,
				SRCHTUSER_CD			,
				SRCHTUSER_TX			,
				SRCLOCASTOCKC_ID		,
				-- 検品
				CHECK_FL				,
				CHECKCOUNT_NR			,
				CHECKSTART_DT			,
				CHECKEND_DT				,
				CHECKHT_ID				,
				CHECKHTUSER_CD			,
				CHECKHTUSER_TX			,
				-- 出荷完了
				SHIPEND_FL				,
				SHIPENDCOUNT_NR			,
				SHIPENDSTART_DT			,
				SHIPENDEND_DT			,
				SHIPENDHT_ID			,
				SHIPENDHTUSER_CD		,
				SHIPENDHTUSER_TX		,
				-- 納品書
				PRTPACKLIST_FL			,
				-- 印刷
				PRINT_FL				,
				PRTCOUNT_NR				,
				PRT_DT					,
				PRTUSER_CD				,
				PRTUSER_TX				,
				--
				WRAPGROUP_ID			,
				WRAPAPPLY_NR			,
				--
				BATCHNO_CD				,
				TYPEAPPLY_CD			,
				TYPEBLOCK_CD			,
				TYPECARRY_CD			,
				-- 管理項目
				UPD_DT					,
				UPDUSER_CD				,
				UPDUSER_TX				,
				ADD_DT					,
				ADDUSER_CD				,
				ADDUSER_TX				,
				UPDPROGRAM_CD			,
				UPDCOUNTER_NR			,
				STRECORD_ID				,
				iCommDate
			FROM
				TA_PICK
			WHERE
				STRECORD_ID = 1;
		DBMS_OUTPUT.PUT_LINE('INSERT HIS_PICK :'||SQL%ROWCOUNT);
		--TA_PICK削除
		DELETE
			TA_PICK
		WHERE
			STRECORD_ID = 1;
		DBMS_OUTPUT.PUT_LINE('DELETE TA_PICK :'||SQL%ROWCOUNT);
		
		DBMS_OUTPUT.PUT_LINE('EndShip End :'|| TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS'));
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Err EndShip :' ||SQLERRM);
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END EndShip;
	/************************************************************************************/
	/*	ログ削除																		*/
	/************************************************************************************/
	PROCEDURE LogDel AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'LogDel';
		numRowPoint 	NUMBER := 0;
		dtmCommDate		DATE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		dtmCommDate	:= TRUNC(SYSDATE);

		DBMS_OUTPUT.PUT_LINE('LogDel Start :'|| TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS'));

		--LOG関連
		--T_LOGHT
		DELETE
			TA_LOGHT
		WHERE
			ADD_DT <= dtmCommDate - 7;
		DBMS_OUTPUT.PUT_LINE('DELETE TA_LOGHT :'||SQL%ROWCOUNT);
		COMMIT;
		
		--TLOG
		DELETE
			LOG.TLOG
		WHERE
			LDATE <= dtmCommDate - 2;
		DBMS_OUTPUT.PUT_LINE('DELETE LOG.TLOG :'||SQL%ROWCOUNT);
		COMMIT;
		
		DBMS_OUTPUT.PUT_LINE('LogDel End :'|| TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS'));
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Err LogDel :' ||SQLERRM);
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END LogDel;
END PS_WORKEND;
/
SHOW ERRORS
