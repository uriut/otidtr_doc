-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_APPLYINSTOCK AS 
	/************************************************************************************/
	/*	良品入庫キャンセル																*/
	/************************************************************************************/
	PROCEDURE CanInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CanInStock';
		
		CURSOR curInStock(InStockNoCD TA_INSTOCK.INSTOCKNO_CD%TYPE) IS
			SELECT
				DISTINCT
				DESTLOCASTOCKC_ID
			FROM
				TA_INSTOCK
			WHERE
				INSTOCKNO_CD = InStockNoCD	AND
				STINSTOCK_ID != PS_DEFINE.ST_INSTOCKM2_CANCEL;
		rowInStock curInStock%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curInStock(iInStockNoCD);
		LOOP
			FETCH curInStock INTO rowInStock;
			EXIT WHEN curInStock%NOTFOUND;
			DELETE TA_LOCASTOCKC
			WHERE
				LOCASTOCKC_ID = rowInStock.DESTLOCASTOCKC_ID;
		END LOOP;
		CLOSE curInStock;
		-- 指示書キャンセル
		UPDATE TA_INSTOCK
		SET
			STINSTOCK_ID	= PS_DEFINE.ST_INSTOCKM2_CANCEL	,
			UPD_DT			= SYSDATE						,
			UPDUSER_CD		= iUserCD						,
			UPDUSER_TX		= iUserTX						,
			UPDPROGRAM_CD	= iProgramCD					,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			INSTOCKNO_CD	= iInStockNoCD;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curInStock%ISOPEN THEN CLOSE curInStock; END IF;
			RAISE;
	END CanInStock;
	/************************************************************************************/
	/*	入庫保留																		*/
	/************************************************************************************/
	PROCEDURE DeferInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DeferInStock';
		
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 指示書保留
		UPDATE TA_INSTOCK
		SET
			STINSTOCK_ID	= PS_DEFINE.ST_INSTOCK_INSTOCK_D,
			UPD_DT			= SYSDATE						,
			UPDUSER_CD		= iUserCD						,
			UPDUSER_TX		= iUserTX						,
			UPDPROGRAM_CD	= iProgramCD					,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			INSTOCKNO_CD	= iInStockNoCD;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END DeferInStock;
	/************************************************************************************/
	/*	入庫保留キャンセル																*/
	/************************************************************************************/
	PROCEDURE CanDeferInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CanDeferInStock';
		
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 指示書保留
		UPDATE TA_INSTOCK
		SET
			STINSTOCK_ID	= PS_DEFINE.ST_INSTOCK0_NOTINSTOCK	,
			UPD_DT			= SYSDATE							,
			UPDUSER_CD		= iUserCD							,
			UPDUSER_TX		= iUserTX							,
			UPDPROGRAM_CD	= iProgramCD						,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			INSTOCKNO_CD	= iInStockNoCD;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END CanDeferInStock;

	/************************************************************************************/
	/*	良品入庫引当																	*/
	/************************************************************************************/
	PROCEDURE ApplyInstock(
		iTrnID				IN NUMBER			,
		irowSku				IN MA_SKU%ROWTYPE	,
		irowArriv			IN TA_ARRIV%ROWTYPE	,
		iSameDaYShipFL		IN NUMBER			,
		iPalletNR			IN NUMBER			,
		iCaseNR				IN NUMBER			,
		iBaraNR				IN NUMBER			,
		iPcsNR				IN NUMBER			,
		iUseLimitDT			IN DATE				,
		iAdminLocaFL		IN NUMBER			,
		iTypeInstockCD		IN VARCHAR2			,
		iArrivID			IN NUMBER			,
		iSkuCD				IN VARCHAR2			,
		iLotTX				IN VARCHAR2			,
		iTypePalletCD		IN VARCHAR2			,
		iUserCD				IN VARCHAR2			,
		iUserTX				IN VARCHAR2			,
		iProgramCD			IN VARCHAR2			,
		oInStockNoCD		OUT TA_INSTOCK.INSTOCKNO_CD%TYPE,
		oKanbanCD			OUT TA_INSTOCK.KANBAN_CD%TYPE,
		orowArea			OUT MB_AREA%ROWTYPE
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'ApplyInstock';

		CURSOR curAbc(
			AbcID	MA_SKU.ABC_ID%TYPE
		) IS
			SELECT
				ABC_ID,
				ABC_CD
			FROM
				MA_ABC
			WHERE
				ABC_ID >= (
					SELECT
						ABC_ID
					FROM
						MA_ABC
					WHERE
						ABC_ID = AbcID )
			ORDER BY
				ABC_ID;
		rowAbc				curAbc%ROWTYPE;

		CURSOR curAbcSub(
			AbcSubID	MA_SKU.ABCSUB_ID%TYPE
		) IS
			SELECT
				ABCSUB_TX
			FROM
				MA_ABCSUB
			WHERE
				ABCSUB_ID = AbcSubID;
		rowAbcSub			curAbcSub%ROWTYPE;

		chrInStockNoCD		TA_INSTOCK.INSTOCKNO_CD%TYPE;
		oEndFL				NUMBER(1);
		oLocaCD				MA_LOCA.LOCA_CD%TYPE;
		oShipVolumeCD		MA_LOCA.SHIPVOLUME_CD%TYPE;
		rowLocaStockC		TA_LOCASTOCKC%ROWTYPE;
		rowArea				MB_AREA%ROWTYPE;
		numInStockID		TA_INSTOCK.INSTOCK_ID%TYPE;
		numPalletFL				NUMBER(1);
		numNestanerStockRate	MA_SKU.NESTAINERINSTOCKRATE_NR%TYPE;
		numCaseStockRate		MA_SKU.CASESTOCKRATE_NR%TYPE;
		numPalletStockRate		MA_SKU.NESTAINERINSTOCKRATE_NR%TYPE;
		numTotalPcsNR			NUMBER;
		chrAreaCD				MB_AREA.AREA_CD%TYPE;
		chrAbcCD				MA_ABC.ABC_CD%TYPE;
		numPcsNR				NUMBER;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START, ' ||
			'iTrnID=[' || iTrnID || '], ' ||
			'ARRIV_ID=[' || iArrivID || '], ' ||
			'SKU_CD=[' || irowSku.SKU_CD || '], ' ||
			'PCS_NR=[' || iPcsNR || '], ' ||
			'USELIMIT_DT=[' || iUseLimitDT || '], ' ||
			'iAdminLocaFL=[' || iAdminLocaFL || '], ' ||
			'TYPEINSTOCK_CD=[' || iTypeInstockCD || ']'
		);
		oEndFL := 0;
		-- 完パレか確認
		-- 初期化
		numPalletFL := 0;
		numNestanerStockRate := 0;
		numCaseStockRate := 0;
		numPalletStockRate := 0;
		oShipVolumeCD := '';
		chrAreaCD := '';
		chrAbcCD := '';
		-- 完パレ確認
		IF iPalletNR = 0 THEN
			numPalletFL := 0;
		ELSE
			numPalletFL := 1;
		END IF;
		-- 数量チェック
		numPcsNR := (iPalletNR * irowArriv.PALLETCAPACITY_NR * irowSku.PCSPERCASE_NR) + (iCaseNR * irowSku.PCSPERCASE_NR) + iBaraNR;
		IF numPcsNR <> iPcsNR THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'PCS数が不適切です。PcsNr=[' || numPcsNr || ']');
		END IF;
		-- 入荷消し込み
		PS_APPLYARRIV.ApplyArriv(iTrnID, irowSku, irowArriv, iPcsNR, iUserCD, iUserTX, iProgramCD);
		-- 格納指示書番号
		PS_NO.GetInStockNo(chrInStockNoCD);
		/************************************************/
		/* 引当ロジック
		/* 
		/* 1パレ単位で処理、端数パレット同士の積み増しはない
		/************************************************/
		-- 在庫比率取得（引当可能、引当済（格納待ち）の比率を確認）
		PS_LOCA.GetLocaStockRate(
			iSkuCD				,
			iLotTX				,
			numNestanerStockRate,
			numCaseStockRate	,
			numPalletStockRate	,
			numTotalPcsNR
			);
			PLOG.DEBUG(logCtx,'StockRate: SkuCd=[ ' || iSkuCD || '], ' ||
								'LotTx=[' || iLotTX || '], ' ||
								'numNestanerStockRate=[' || numNestanerStockRate || '], ' ||
								'numCaseStockRate=[' || numCaseStockRate || '], ' ||
								'numPalletStockRate=[' || numPalletStockRate || '], ' ||
								'numTotalPcsNR=[' || numTotalPcsNR || ']'
						);

		OPEN curAbc(irowSku.ABC_ID);
		FETCH curAbc INTO rowAbc; 
		chrAbcCD := rowAbc.ABC_CD;
		CLOSE curAbc;

		-- 即出荷
		IF iSameDaYShipFL = 1 THEN
			/*
			即出荷の引当順
			�@即出荷エリアの積み増し可能ロケ
			�A即出荷エリアの空きロケ
			�Bケース成りエリアの積み増しロケ
			�Cケース成りエリアの空きロケ
			�Eパレット成りエリアの空きロケ
			�Fネスエリアの空きロケ
			*/
			-- �@即出荷エリアの積み増し可能ロケ
			IF oEndFL = 0 THEN
				OPEN curAbc(irowSku.ABC_ID);
				LOOP               
					FETCH curAbc INTO rowAbc; 
					EXIT WHEN curAbc%NOTFOUND;        
					EXIT WHEN oEndFL = 1;
					PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 1_LocaSearchSameDayShip: ' ||
						'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
						'SKU_CD=[ ' || iSkuCD || '], ' ||
						'LOT_TX=[' || iLotTX || '], ' ||
						'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
						'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
					);
					ApplyStock(
						iTrnID								,	
						irowSku								,	
						iLotTX								,	
						irowArriv.PALLETCAPACITY_NR			,	
						PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
						PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
						PS_DEFINE.TYPE_SHIPVOLUME4_SAMEDAYSHIP	,	-- SHIPVOLUME_CD
						rowAbc.ABC_ID						,
						oEndFL								,	
						oLocaCD								,	
						oKanbanCD							,	
						chrAreaCD
						);
					oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME4_SAMEDAYSHIP;
				END LOOP;                                     
				CLOSE curAbc;                         
			END IF;
			-- �A即出荷エリアの空きロケ
			IF oEndFL = 0 THEN
				OPEN curAbc(irowSku.ABC_ID);
				LOOP               
					FETCH curAbc INTO rowAbc; 
					EXIT WHEN curAbc%NOTFOUND;        
					EXIT WHEN oEndFL = 1;
					PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 2_LocaSearchSameDayShipEmpty: ' ||
						'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
						'SKU_CD=[ ' || iSkuCD || '], ' ||
						'LOT_TX=[' || iLotTX || '], ' ||
						'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
						'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
					);
					ApplyStockEmpty(
						iTrnID								,	
						irowSku								,	
						iLotTX								,	
						irowArriv.PALLETCAPACITY_NR			,	
						PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
						PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
						PS_DEFINE.TYPE_SHIPVOLUME4_SAMEDAYSHIP	,	-- SHIPVOLUME_CD
						rowAbc.ABC_ID						,
						oEndFL								,	
						oLocaCD								,	
						oKanbanCD							,	
						chrAreaCD
						);
					oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME4_SAMEDAYSHIP;
				END LOOP;                                     
				CLOSE curAbc;                         
			END IF;
			-- �Bース成りエリアの積み増しロケ
			IF oEndFL = 0 THEN
				OPEN curAbc(irowSku.ABC_ID);
				LOOP               
					FETCH curAbc INTO rowAbc; 
					EXIT WHEN curAbc%NOTFOUND;        
					EXIT WHEN oEndFL = 1;
					PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 3_LocaSearchSameDayShip_Case: ' ||
						'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
						'SKU_CD=[ ' || iSkuCD || '], ' ||
						'LOT_TX=[' || iLotTX || '], ' ||
						'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
						'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
					);
					ApplyStock(
						iTrnID								,	
						irowSku								,	
						iLotTX								,	
						irowArriv.PALLETCAPACITY_NR			,	
						PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
						PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
						PS_DEFINE.TYPE_SHIPVOLUME1_CASE		,	-- SHIPVOLUME_CD
						rowAbc.ABC_ID						,
						oEndFL								,	
						oLocaCD								,	
						oKanbanCD							,	
						chrAreaCD
						);
					oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME1_CASE;
				END LOOP;                                     
				CLOSE curAbc;                         
			END IF;
			-- �Cケース成りエリアの空きロケ
			IF oEndFL = 0 THEN
				OPEN curAbc(irowSku.ABC_ID);
				LOOP               
					FETCH curAbc INTO rowAbc; 
					EXIT WHEN curAbc%NOTFOUND;        
					EXIT WHEN oEndFL = 1;
					PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 4_LocaSearchSameDayShip_CaseEmpty: ' ||
						'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
						'SKU_CD=[ ' || iSkuCD || '], ' ||
						'LOT_TX=[' || iLotTX || '], ' ||
						'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
						'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
					);
					ApplyStockEmpty(
						iTrnID								,	
						irowSku								,	
						iLotTX								,	
						irowArriv.PALLETCAPACITY_NR			,	
						PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
						PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
						PS_DEFINE.TYPE_SHIPVOLUME1_CASE		,	-- SHIPVOLUME_CD
						rowAbc.ABC_ID						,
						oEndFL								,	
						oLocaCD								,	
						oKanbanCD							,	
						chrAreaCD
						);
					oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME1_CASE;
				END LOOP;                                     
				CLOSE curAbc;                         
			END IF;
			-- �Dパレット成りエリアの積み増しロケ
			IF oEndFL = 0 THEN
				OPEN curAbc(irowSku.ABC_ID);
				LOOP               
					FETCH curAbc INTO rowAbc; 
					EXIT WHEN curAbc%NOTFOUND;        
					EXIT WHEN oEndFL = 1;
					PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 5_LocaSearchSameDayShip_Pallet: ' ||
						'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
						'SKU_CD=[ ' || iSkuCD || '], ' ||
						'LOT_TX=[' || iLotTX || '], ' ||
						'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
						'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
					);
					ApplyStock(
						iTrnID								,	
						irowSku								,	
						iLotTX								,	
						irowArriv.PALLETCAPACITY_NR			,	
						PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
						PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
						PS_DEFINE.TYPE_SHIPVOLUME2_PALLET	,	-- SHIPVOLUME_CD
						rowAbc.ABC_ID						,
						oEndFL								,	
						oLocaCD								,	
						oKanbanCD							,	
						chrAreaCD
						);
					oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME2_PALLET;
				END LOOP;                                     
				CLOSE curAbc;                         
			END IF;
			-- �Eパレット成りエリアの空きロケ
			IF oEndFL = 0 THEN
				OPEN curAbc(irowSku.ABC_ID);
				LOOP               
					FETCH curAbc INTO rowAbc; 
					EXIT WHEN curAbc%NOTFOUND;        
					EXIT WHEN oEndFL = 1;
					PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 6_LocaSearchSameDayShip_PalletEmpty: ' ||
						'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
						'SKU_CD=[ ' || iSkuCD || '], ' ||
						'LOT_TX=[' || iLotTX || '], ' ||
						'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
						'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
					);
					ApplyStockEmpty(
						iTrnID								,	
						irowSku								,	
						iLotTX								,	
						irowArriv.PALLETCAPACITY_NR			,	
						PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
						PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
						PS_DEFINE.TYPE_SHIPVOLUME2_PALLET	,	-- SHIPVOLUME_CD
						rowAbc.ABC_ID						,
						oEndFL								,	
						oLocaCD								,	
						oKanbanCD							,	
						chrAreaCD
						);
					oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME2_PALLET;
				END LOOP;                                     
				CLOSE curAbc;                         
			END IF;
			-- �Fネスエリアの空きロケ
			IF oEndFL = 0 THEN
				OPEN curAbc(irowSku.ABC_ID);
				LOOP               
					FETCH curAbc INTO rowAbc; 
					EXIT WHEN curAbc%NOTFOUND;        
					EXIT WHEN oEndFL = 1;
					PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 7_LocaSearchSameDayShip_Nestaner: ' ||
						'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
						'SKU_CD=[ ' || iSkuCD || '], ' ||
						'LOT_TX=[' || iLotTX || '], ' ||
						'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
						'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
					);
					ApplyStockEmpty(
						iTrnID								,	
						irowSku								,	
						iLotTX								,	
						irowArriv.PALLETCAPACITY_NR			,	
						PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
						PS_DEFINE.TYPE_LOCABLOCK_N1			,	-- TYPELOCABLOCK_CD
						PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER	,	-- SHIPVOLUME_CD
						rowAbc.ABC_ID						,
						oEndFL								,	
						oLocaCD								,	
						oKanbanCD							,	
						chrAreaCD
						);
					oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER;
				END LOOP;                                     
				CLOSE curAbc;                         
			END IF;
		-- 即出荷でない場合
		ELSE
			-- 完パレ以外
			IF numPalletFL = 0 THEN
				/*
				完パレ以外の引当順
				�@ネスエリアの空きロケ
				�Aケース成りエリアの積み増しロケ
				�Bケース成りエリアの空きロケ
				*/
				IF oEndFL = 0 THEN
					OPEN curAbc(irowSku.ABC_ID);
					LOOP               
						FETCH curAbc INTO rowAbc; 
						EXIT WHEN curAbc%NOTFOUND;        
						EXIT WHEN oEndFL = 1;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 1_LocaSearchBara_Nestaner: ' ||
							'oInstockNoCD=[' || chrInStockNoCD || '],' ||
							'SKU_CD=[ ' || iSkuCD || '], ' ||
							'LOT_TX=[' || iLotTX || '], ' ||
							'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
							'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
						);
						ApplyStockEmpty(
							iTrnID								,
							irowSku								,
							iLotTX								,
							irowArriv.PALLETCAPACITY_NR			,
							PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
							PS_DEFINE.TYPE_LOCABLOCK_N1			,	-- TYPELOCABLOCK_CD
							PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER	,	-- SHIPVOLUME_CD
							rowAbc.ABC_ID						,
							oEndFL								,
							oLocaCD								,
							oKanbanCD							,
							chrAreaCD
							);
						oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER;
					END LOOP;                                     
					CLOSE curAbc;                         
				END IF;
				-- �Fケース成り積み増しロケ
				IF oEndFL = 0 THEN
					OPEN curAbc(irowSku.ABC_ID);
					LOOP               
						FETCH curAbc INTO rowAbc; 
						EXIT WHEN curAbc%NOTFOUND;        
						EXIT WHEN oEndFL = 1;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 2_LocaSearchBara_Case: ' ||
							'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
							'SKU_CD=[ ' || iSkuCD || '], ' ||
							'LOT_TX=[' || iLotTX || '], ' ||
							'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
							'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
						);
						ApplyStock(
							iTrnID								,	
							irowSku								,	
							iLotTX								,	
							irowArriv.PALLETCAPACITY_NR			,	
							PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
							PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
							PS_DEFINE.TYPE_SHIPVOLUME1_CASE		,	-- SHIPVOLUME_CD
							rowAbc.ABC_ID						,
							oEndFL								,	
							oLocaCD								,	
							oKanbanCD							,	
							chrAreaCD
							);
						oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME1_CASE;
					END LOOP;                                     
					CLOSE curAbc;                         
				END IF;
				-- �Gケース成り空きロケ
				IF oEndFL = 0 THEN
					OPEN curAbc(irowSku.ABC_ID);
					LOOP               
						FETCH curAbc INTO rowAbc; 
						EXIT WHEN curAbc%NOTFOUND;        
						EXIT WHEN oEndFL = 1;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 3_LocaSearchBara_CaseEmpty: ' ||
							'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
							'SKU_CD=[ ' || iSkuCD || '], ' ||
							'LOT_TX=[' || iLotTX || '], ' ||
							'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
							'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
						);
						ApplyStockEmpty(
							iTrnID								,	
							irowSku								,	
							iLotTX								,	
							irowArriv.PALLETCAPACITY_NR			,	
							PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
							PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
							PS_DEFINE.TYPE_SHIPVOLUME1_CASE		,	-- SHIPVOLUME_CD
							rowAbc.ABC_ID						,
							oEndFL								,	
							oLocaCD								,	
							oKanbanCD							,	
							chrAreaCD
							);
						oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME1_CASE;
					END LOOP;                                     
					CLOSE curAbc;                         
				END IF;
			ELSE
				/*
				完パレの引当順
				�@積み付け段数が1の場合はネスエリアの空きロケ
				�Aケース成り積み増しロケ
				�Bパレット成り積み増しロケ
				�Cネスエリア在庫割合 <= マスタ割合、ネスエリア空きロケ
				�Dケース成りエリア在庫割合 <= マスタ割合 の場合、ケース成り空きロケ
				�Eケース成りエリア在庫割合 <= マスタ割合 の場合、パレット成り空きロケ
				�Fケース成りエリア在庫割合 <= マスタ割合 の場合、ネスエリアの空きロケ
				�Gパレット成り空きロケ
				�Hケース成り空きロケ
				�Iネスエリアの空きロケ
				*/
				
				-- �@積み付け段数が1の場合はネスエリアの空きロケ
				IF irowArriv.STACKINGNUMBER_NR = 1 THEN
					OPEN curAbc(irowSku.ABC_ID);
					LOOP               
						FETCH curAbc INTO rowAbc; 
						EXIT WHEN curAbc%NOTFOUND;        
						EXIT WHEN oEndFL = 1;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 1_LocaSearchFull_Nestaner: ' ||
							'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
							'SKU_CD=[ ' || iSkuCD || '], ' ||
							'LOT_TX=[' || iLotTX || '], ' ||
							'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
							'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
						);
						ApplyStockEmpty(
							iTrnID								,	
							irowSku								,	
							iLotTX								,	
							irowArriv.PALLETCAPACITY_NR			,	
							PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
							PS_DEFINE.TYPE_LOCABLOCK_N1			,	-- TYPELOCABLOCK_CD
							PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER	,	-- SHIPVOLUME_CD
							rowAbc.ABC_ID						,
							oEndFL								,	
							oLocaCD								,	
							oKanbanCD							,	
							chrAreaCD
							);
						oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER;
					END LOOP;                                     
					CLOSE curAbc;                         
				END IF;
				-- �Aケース成り積み増しロケ
				IF oEndFL = 0 THEN
					OPEN curAbc(irowSku.ABC_ID);
					LOOP               
						FETCH curAbc INTO rowAbc; 
						EXIT WHEN curAbc%NOTFOUND;        
						EXIT WHEN oEndFL = 1;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 2_LocaSearchFull_Case: ' ||
							'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
							'SKU_CD=[ ' || iSkuCD || '], ' ||
							'LOT_TX=[' || iLotTX || '], ' ||
							'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
							'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
						);
						ApplyStock(
							iTrnID								,	
							irowSku								,	
							iLotTX								,	
							irowArriv.PALLETCAPACITY_NR			,	
							PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
							PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
							PS_DEFINE.TYPE_SHIPVOLUME1_CASE		,	-- SHIPVOLUME_CD
							rowAbc.ABC_ID						,
							oEndFL								,	
							oLocaCD								,	
							oKanbanCD							,	
							chrAreaCD
							);
						oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME1_CASE;
					END LOOP;                                     
					CLOSE curAbc;                         
				END IF;
				-- �Bパレット成り積み増しロケ
				IF oEndFL = 0 THEN
					OPEN curAbc(irowSku.ABC_ID);
					LOOP               
						FETCH curAbc INTO rowAbc; 
						EXIT WHEN curAbc%NOTFOUND;        
						EXIT WHEN oEndFL = 1;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 3_LocaSearchFull_Pallet: ' ||
							'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
							'SKU_CD=[ ' || iSkuCD || '], ' ||
							'LOT_TX=[' || iLotTX || '], ' ||
							'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
							'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
						);
						ApplyStock(
							iTrnID								,	
							irowSku								,	
							iLotTX								,	
							irowArriv.PALLETCAPACITY_NR			,	
							PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
							PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
							PS_DEFINE.TYPE_SHIPVOLUME2_PALLET	,	-- SHIPVOLUME_CD
							rowAbc.ABC_ID						,
							oEndFL								,	
							oLocaCD								,	
							oKanbanCD							,	
							chrAreaCD
							);
						oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME2_PALLET;
					END LOOP;                                     
					CLOSE curAbc;                         
				END IF;
				-- ネスロジック
				IF irowSku.NESTAINERINSTOCKRATE_NR <> 0 THEN	-- 比率が0でない
					PLOG.DEBUG(logCtx,'SkuRate: SkuCd=[ ' || iSkuCD || '], ' ||
										'NESTAINERINSTOCKRATE_NR=[' || irowSku.NESTAINERINSTOCKRATE_NR || ']'
								);
					IF numNestanerStockRate <= irowSku.NESTAINERINSTOCKRATE_NR THEN
						-- �Cネスエリア在庫割合 <= マスタ割合、ネスエリア空きロケ
						IF oEndFL = 0 THEN
							OPEN curAbc(irowSku.ABC_ID);
							LOOP               
								FETCH curAbc INTO rowAbc; 
								EXIT WHEN curAbc%NOTFOUND;        
								EXIT WHEN oEndFL = 1;
								PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 4_LocaSearchFull_Nestaner_Rate: ' ||
									'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
									'SKU_CD=[ ' || iSkuCD || '], ' ||
									'LOT_TX=[' || iLotTX || '], ' ||
									'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
									'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
								);
								ApplyStockEmpty(
									iTrnID								,	
									irowSku								,	
									iLotTX								,	
									irowArriv.PALLETCAPACITY_NR			,	
									PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
									PS_DEFINE.TYPE_LOCABLOCK_N1			,	-- TYPELOCABLOCK_CD
									PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER	,	-- SHIPVOLUME_CD
									rowAbc.ABC_ID						,
									oEndFL								,	
									oLocaCD								,	
									oKanbanCD							,	
									chrAreaCD
									);
								oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER;
							END LOOP;                                     
							CLOSE curAbc;                         
						END IF;
					END IF;
				END IF;
				-- ケース成りロジック
				IF irowSku.CASESTOCKRATE_NR <> 0 THEN	-- 比率が0でない
					PLOG.DEBUG(logCtx,'SkuRate: SkuCd=[ ' || iSkuCD || '], ' ||
										'CASESTOCKRATE_NR=[' || irowSku.CASESTOCKRATE_NR || ']'
								);
					IF numCaseStockRate <= irowSku.CASESTOCKRATE_NR THEN
						-- �Dケース成りエリア在庫割合 <= マスタ割合 の場合、ケース成り空きロケ
						IF oEndFL = 0 THEN
							OPEN curAbc(irowSku.ABC_ID);
							LOOP               
								FETCH curAbc INTO rowAbc; 
								EXIT WHEN curAbc%NOTFOUND;        
								EXIT WHEN oEndFL = 1;
								PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 5_LocaSearchFull_Case_Rate: ' ||
									'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
									'SKU_CD=[ ' || iSkuCD || '], ' ||
									'LOT_TX=[' || iLotTX || '], ' ||
									'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
									'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
								);
								ApplyStockEmpty(
									iTrnID								,	
									irowSku								,	
									iLotTX								,	
									irowArriv.PALLETCAPACITY_NR			,	
									PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
									PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
									PS_DEFINE.TYPE_SHIPVOLUME1_CASE		,	-- SHIPVOLUME_CD
									rowAbc.ABC_ID						,
									oEndFL								,	
									oLocaCD								,	
									oKanbanCD							,	
									chrAreaCD
									);
								oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME1_CASE;
							END LOOP;                                     
							CLOSE curAbc;                         
						END IF;
						-- �Eケース成りエリア在庫割合 <= マスタ割合 の場合、パレット成り空きロケ
						IF oEndFL = 0 THEN
							OPEN curAbc(irowSku.ABC_ID);
							LOOP               
								FETCH curAbc INTO rowAbc; 
								EXIT WHEN curAbc%NOTFOUND;        
								EXIT WHEN oEndFL = 1;
								PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 6_LocaSearchFull_Pallet_Rate: ' ||
									'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
									'SKU_CD=[ ' || iSkuCD || '], ' ||
									'LOT_TX=[' || iLotTX || '], ' ||
									'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
									'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
								);
								ApplyStockEmpty(
									iTrnID								,	
									irowSku								,	
									iLotTX								,	
									irowArriv.PALLETCAPACITY_NR			,	
									PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
									PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
									PS_DEFINE.TYPE_SHIPVOLUME2_PALLET	,	-- SHIPVOLUME_CD
									rowAbc.ABC_ID						,
									oEndFL								,	
									oLocaCD								,	
									oKanbanCD							,	
									chrAreaCD
									);
								oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME2_PALLET;
							END LOOP;                                     
							CLOSE curAbc;                         
						END IF;
						-- �Fケース成りエリア在庫割合 <= マスタ割合 の場合、ネスエリアの空きロケ
						IF oEndFL = 0 THEN
							OPEN curAbc(irowSku.ABC_ID);
							LOOP               
								FETCH curAbc INTO rowAbc; 
								EXIT WHEN curAbc%NOTFOUND;        
								EXIT WHEN oEndFL = 1;
								PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 7_LocaSearchFull_Nestaner: ' ||
									'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
									'SKU_CD=[ ' || iSkuCD || '], ' ||
									'LOT_TX=[' || iLotTX || '], ' ||
									'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
									'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
								);
								ApplyStockEmpty(
									iTrnID								,	
									irowSku								,	
									iLotTX								,	
									irowArriv.PALLETCAPACITY_NR			,	
									PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
									PS_DEFINE.TYPE_LOCABLOCK_N1			,	-- TYPELOCABLOCK_CD
									PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER	,	-- SHIPVOLUME_CD
									rowAbc.ABC_ID						,
									oEndFL								,	
									oLocaCD								,	
									oKanbanCD							,	
									chrAreaCD
									);
								oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER;
							END LOOP;                                     
							CLOSE curAbc;                         
						END IF;
					END IF;
				END IF;
				-- �Gパレット成り空きロケ
				IF oEndFL = 0 THEN
					OPEN curAbc(irowSku.ABC_ID);
					LOOP               
						FETCH curAbc INTO rowAbc; 
						EXIT WHEN curAbc%NOTFOUND;        
						EXIT WHEN oEndFL = 1;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 8_LocaSearchFull_PalletEmpty: ' ||
							'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
							'SKU_CD=[ ' || iSkuCD || '], ' ||
							'LOT_TX=[' || iLotTX || '], ' ||
							'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
							'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
						);
						ApplyStockEmpty(
							iTrnID								,	
							irowSku								,	
							iLotTX								,	
							irowArriv.PALLETCAPACITY_NR			,	
							PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
							PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
							PS_DEFINE.TYPE_SHIPVOLUME2_PALLET	,	-- SHIPVOLUME_CD
							rowAbc.ABC_ID						,
							oEndFL								,	
							oLocaCD								,	
							oKanbanCD							,	
							chrAreaCD
							);
						oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME2_PALLET;
					END LOOP;                                     
					CLOSE curAbc;                         
				END IF;
				-- �Hケース成り空きロケ
				IF oEndFL = 0 THEN
					OPEN curAbc(irowSku.ABC_ID);
					LOOP               
						FETCH curAbc INTO rowAbc; 
						EXIT WHEN curAbc%NOTFOUND;        
						EXIT WHEN oEndFL = 1;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 9_LocaSearchFull_CaseEmpty: ' ||
							'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
							'SKU_CD=[ ' || iSkuCD || '], ' ||
							'LOT_TX=[' || iLotTX || '], ' ||
							'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
							'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
						);
						ApplyStockEmpty(
							iTrnID								,	
							irowSku								,	
							iLotTX								,	
							irowArriv.PALLETCAPACITY_NR			,	
							PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
							PS_DEFINE.TYPE_LOCABLOCK_P1			,	-- TYPELOCABLOCK_CD
							PS_DEFINE.TYPE_SHIPVOLUME1_CASE		,	-- SHIPVOLUME_CD
							rowAbc.ABC_ID						,
							oEndFL								,	
							oLocaCD								,	
							oKanbanCD							,	
							chrAreaCD
							);
						oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME1_CASE;
					END LOOP;                                     
					CLOSE curAbc;                         
				END IF;
				-- �Iネスエリアの空きロケ
				IF oEndFL = 0 THEN
					OPEN curAbc(irowSku.ABC_ID);
					LOOP               
						FETCH curAbc INTO rowAbc; 
						EXIT WHEN curAbc%NOTFOUND;        
						EXIT WHEN oEndFL = 1;
						PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' 10_LocaSearchFull_NestanerEmpty: ' ||
							'oInstockNoCD=[' || chrInStockNoCD || '], ' ||
							'SKU_CD=[ ' || iSkuCD || '], ' ||
							'LOT_TX=[' || iLotTX || '], ' ||
							'ABC_CD=[' || rowAbc.ABC_CD || '], ' ||
							'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
						);
						ApplyStockEmpty(
							iTrnID								,	
							irowSku								,	
							iLotTX								,	
							irowArriv.PALLETCAPACITY_NR			,	
							PS_DEFINE.TYPE_LOCA_NORMAL			,	-- TYPELOCA_CD　現在、未使用
							PS_DEFINE.TYPE_LOCABLOCK_N1			,	-- TYPELOCABLOCK_CD
							PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER	,	-- SHIPVOLUME_CD
							rowAbc.ABC_ID						,
							oEndFL								,	
							oLocaCD								,	
							oKanbanCD							,	
							chrAreaCD
							);
						oShipVolumeCD := PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER;
					END LOOP;                                     
					CLOSE curAbc;                         
				END IF;
			END IF;
		END IF;

		IF oEndFL = 0 THEN
			OPEN curAbcSub(irowSku.ABCSUB_ID);
			FETCH curAbcSub INTO rowAbcSub;
			CLOSE curAbcSub;
			PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' LocationNotFound: ' ||
						'InstockNoCD=[' || chrInStockNoCD || '], ' ||
						'SkuCD=[' || iSkuCD || '], ' ||
						'LotTX=[' || iLotTX || '], ' ||
						'Abc=[' || TO_CHAR(irowSku.ABC_ID) || ':' || chrAbcCD || '], ' ||
						'AbcSub=[' || NVL(TO_CHAR(irowSku.ABCSUB_ID), ' ') || ':' || NVL(rowAbcSub.ABCSUB_TX, ' ') || ']'
						);
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,
									'格納可能ロケなし' ||
									'ロケABC=[' || chrAbcCD || ':' || NVL(rowAbcSub.ABCSUB_TX, ' ') || ']'
									);
		END IF;
		--ロケーションロック取得
		PS_LOCA.GetLocaLock(oLocaCD);
		PS_MASTER.GetArea(chrAreaCD,rowArea);
		orowArea := rowArea;
		oEndFL := 1;
		
		
		--引当済み追加
		PS_STOCK.ApplyInStock(
			iTrnID							,
			PS_DEFINE.ST_STOCK1_INAPPLY		,
			oLocaCD 						,
			irowSku.SKU_CD					,
			iLotTX							,
			oKanbanCD						,
			irowArriv.CASEPERPALLET_NR		,
			irowArriv.PALLETLAYER_NR		,
			irowArriv.PALLETCAPACITY_NR		,
			irowArriv.STACKINGNUMBER_NR		,
			iTypePalletCD					,
			iUseLimitDT 					,
			' '								,	-- BATCHNO_CD
			TO_DATE(TO_CHAR(SYSDATE,'YYYYMM') || '01','YYYYMMDD'),	-- STOCK_DT
			irowSku.PCSPERCASE_NR			,	-- AMOUNT_NR
			iPalletNR						,
			iCaseNR							,
			iBaraNR							,
			iPcsNR							,
			'10'							,	-- TYPESTOCK_CD
			iUserCD							,
			iUserTX							,
			rowLocaStockC
		);
		--指示書の追加
		PS_INSTOCK.InsInstock(
			iTrnID			,
			chrInStockNoCD	,
			iTypeInstockCD	,	--TODO	iTypeInStockCD
			'10'			,	--TODO	iTypeApplyCD
			rowLocaStockC	,
			iArrivID		,
			iSkuCD			,
			iLotTX			,
			oKanbanCD		,
			oShipVolumeCD	,
			iUserCD			,
			iUserTX			,
			FUNCTION_NAME	,
			numInStockID
		);
		oInstockNoCD := chrInStockNoCD;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END, ' ||
			'oInstockNoCD=[' || oInstockNoCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			--IF curTypeSkuSize%ISOPEN THEN CLOSE curTypeSkuSize; END IF;
			RAISE;
	END ApplyInstock;
	/************************************************************************************/
	/*	入庫ロケ検索（空きロケ）														*/
	/************************************************************************************/
	PROCEDURE ApplyStockEmpty(
		iTrnID				IN NUMBER		,
		irowSku				IN MA_SKU%ROWTYPE,
		iLotTX				IN VARCHAR2		,
		--iCasePerPalletNR	IN NUMBER		,
		iPalletCapacityNR	IN NUMBER		,
		iTypeLocaCD			IN VARCHAR2		,
		iTypeLocaBlockCD	IN VARCHAR2		,
		iShipVolumeCD		IN VARCHAR2		,
		iAbcID				IN NUMBER		,
		oEndFL				OUT NUMBER		,
		oLocaCD				OUT VARCHAR2	,
		oKanbanCD			OUT VARCHAR2	,
		oAreaCD				OUT VARCHAR2
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'ApplyStockEmpty';
		numRowPoint 	NUMBER := 0;
		CURSOR curLocaStock1(
			SkuCD				TA_LOCASTOCKP.SKU_CD%TYPE		,
			TypeLocaCD			MA_LOCA.TYPELOCA_CD%TYPE		,
			TypeLocaBlockCD		MA_LOCA.TYPELOCABLOCK_CD%TYPE	,
			LineUnitNr			MA_LOCA.LINEUNIT_NR%TYPE		,
			ShipVolumeCD		MA_LOCA.SHIPVOLUME_CD%TYPE		,
			AbcID	 			MA_LOCA.ABC_ID%TYPE				,
			AbcSubID			MA_LOCA.ABCSUB_ID%TYPE
		) IS
			SELECT
				ML.LOCA_CD				,
				ML.ARRIVPRIORITY_NR		,
				ML.LOCA_TX				,
				ML.LINEUNIT_NR			,
				ML.AREA_CD
			FROM
				MA_LOCA ML
					LEFT OUTER JOIN TA_LOCASTOCKP LP
						ON ML.LOCA_CD = LP.LOCA_CD
					LEFT OUTER JOIN TA_LOCASTOCKC LC
						ON ML.LOCA_CD = LC.LOCA_CD
			WHERE
				ML.TYPELOCABLOCK_CD	= TypeLocaBlockCD	AND
				ML.TYPELOCA_CD		= TypeLocaCD		AND
				ML.SHIPVOLUME_CD	= ShipVolumeCD		AND
				ML.NOTUSE_FL		= 0					AND
				ML.LINEUNIT_NR		>= LineUnitNr		AND
				ML.ABC_ID			= AbcID				AND
				ML.ABCSUB_ID		= AbcSubID			AND
				LP.LOCA_CD			IS NULL				AND
				LC.LOCA_CD			IS NULL
			ORDER BY
				ML.LINEUNIT_NR ASC		,
				ML.LOCA_CD				;
		CURSOR curLocaStock2(
			SkuCD				TA_LOCASTOCKP.SKU_CD%TYPE		,
			TypeLocaCD			MA_LOCA.TYPELOCA_CD%TYPE		,
			TypeLocaBlockCD		MA_LOCA.TYPELOCABLOCK_CD%TYPE	,
			LineUnitNr			MA_LOCA.LINEUNIT_NR%TYPE		,
			ShipVolumeCD		MA_LOCA.SHIPVOLUME_CD%TYPE		,
			AbcID	 			MA_LOCA.ABC_ID%TYPE				,
			AbcSubID			MA_LOCA.ABCSUB_ID%TYPE
		) IS
			SELECT
				ML.LOCA_CD				,
				ML.ARRIVPRIORITY_NR		,
				ML.LOCA_TX				,
				ML.LINEUNIT_NR			,
				ML.AREA_CD
			FROM
				MA_LOCA ML
					LEFT OUTER JOIN TA_LOCASTOCKP LP
						ON ML.LOCA_CD = LP.LOCA_CD
					LEFT OUTER JOIN TA_LOCASTOCKC LC
						ON ML.LOCA_CD = LC.LOCA_CD
			WHERE
				ML.TYPELOCABLOCK_CD	= TypeLocaBlockCD	AND
				ML.TYPELOCA_CD		= TypeLocaCD		AND
				ML.SHIPVOLUME_CD	= ShipVolumeCD		AND
				ML.NOTUSE_FL		= 0					AND
				ML.LINEUNIT_NR		< LineUnitNr		AND
				ML.ABC_ID			= AbcID				AND
				ML.ABCSUB_ID		= AbcSubID			AND
				LP.LOCA_CD			IS NULL				AND
				LC.LOCA_CD			IS NULL
			ORDER BY
				ML.LINEUNIT_NR DESC		,
				ML.LOCA_CD				;
		rowLocaStock	curLocaStock1%ROWTYPE;
		--
		rowLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		numInStockID	TA_INSTOCK.INSTOCK_ID%TYPE;
		numLineunit		int;						--シリンダー数
		today			date := sysdate;			--当日の日付
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START, ' ||
			'iTrnID=[' || iTrnID || '], ' ||
			'iTypeLocaCD=[' || iTypeLocaCD || '], ' ||
			'iTypeLocaBlockCD=[' || iTypeLocaBlockCD || '], ' ||
			'iShipVolumeCD=[' || iShipVolumeCD || '], ' ||
			'SKU_CD=[' || irowSku.SKU_CD || '], ' ||
			'ABC_ID=[' || iAbcID || '], ' ||
			'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
		);
		
		-- 当日の入荷予定情報を取得する
		-- numLineunitに当日の入荷予定残のシリンダー数を取得する(上限を最大ラインユニット数とする)
		PS_ARRIV.GetArrivCount(
			irowSku.SKU_CD 		,
			iLotTX				,
			today				,
			iShipVolumeCD		,
			numLineunit			
		);
		
		-- �@当日のシリンダー数を下限として昇順で検索
		OPEN curLocaStock1(
			irowSku.SKU_CD	,
			iTypeLocaCD		,
			iTypeLocaBlockCD,
			numLineunit		,
			iShipVolumeCD	,
			iAbcID			,
			irowSku.ABCSUB_ID
		);
		oEndFL := 0;
		oAreaCD := '';
		LOOP
			FETCH curLocaStock1 INTO rowLocaStock;
			EXIT WHEN curLocaStock1%NOTFOUND;
			EXIT WHEN oEndFL = 1;
			PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' FOUND, ' ||
				'iTrnID=[' || iTrnID || '], ' ||
				'SKU_CD=[' || irowSku.SKU_CD || '], ' ||
				'LOCA_CD=[' || rowLocaStock.LOCA_CD || ']'
			);
			oEndFL := 1;
			oKanbanCD := ' ';
			oLocaCD := rowLocaStock.LOCA_CD;
			-- 
			oAreaCD := rowLocaStock.AREA_CD;
		END LOOP;
		CLOSE curLocaStock1;
		
		-- �A当日のシリンダー数を未満のラインユニットで降順で検索
		OPEN curLocaStock2(
			irowSku.SKU_CD	,
			iTypeLocaCD		,
			iTypeLocaBlockCD,
			numLineunit		,
			iShipVolumeCD	,
			iAbcID			,
			irowSku.ABCSUB_ID
		);

		IF  oEndFL = 0 THEN
			LOOP
				FETCH curLocaStock2 INTO rowLocaStock;
				EXIT WHEN curLocaStock2%NOTFOUND;
				EXIT WHEN oEndFL = 1;
				PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' FOUND, ' ||
					'iTrnID=[' || iTrnID || '], ' ||
					'SKU_CD=[' || irowSku.SKU_CD || '], ' ||
					'LOCA_CD=[' || rowLocaStock.LOCA_CD || ']'
				);
				oEndFL := 1;
				oKanbanCD := ' ';
				oLocaCD := rowLocaStock.LOCA_CD;
				-- 
				oAreaCD := rowLocaStock.AREA_CD;
			END LOOP;
		END IF;
		CLOSE curLocaStock2;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END,' ||
			'oEndFL=[' || oEndFL || '], ' ||
			'oKanbanCD=[' || oKanbanCD || '], ' ||
			'oLocaCD=[' || oLocaCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStock1%ISOPEN THEN
				CLOSE curLocaStock1;
			END IF;
			IF curLocaStock2%ISOPEN THEN
				CLOSE curLocaStock2;
			END IF;
			RAISE;
	END ApplyStockEmpty;
	/************************************************************************************/
	/*	入庫ロケ検索（該当商品の在庫あり）												*/
	/************************************************************************************/
	PROCEDURE ApplyStock(
		iTrnID				IN NUMBER		,
		irowSku				IN MA_SKU%ROWTYPE,
		iLotTX				IN VARCHAR2		,
		--iCasePerPalletNR	IN NUMBER		,
		iPalletCapacityNR	IN NUMBER		,
		iTypeLocaCD			IN VARCHAR2		,
		iTypeLocaBlockCD	IN VARCHAR2		,
		iShipVolumeCD		IN VARCHAR2		,
		iAbcID				IN NUMBER		,
		oEndFL				OUT NUMBER		,
		oLocaCD				OUT VARCHAR2	,
		oKanbanCD			OUT VARCHAR2	,
		oAreaCD				OUT VARCHAR2
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'ApplyStock';
		numRowPoint 	NUMBER := 0;
		CURSOR curLocaStock(
			SkuCD				TA_LOCASTOCKP.SKU_CD%TYPE		,
			LotTX				TA_LOCASTOCKP.LOT_TX%TYPE		,
			PalletCapacityNR	TA_LOCASTOCKP.PALLETCAPACITY_NR%TYPE,
			TypeLocaCD			MA_LOCA.TYPELOCA_CD%TYPE		,
			TypeLocaBlockCD		MA_LOCA.TYPELOCABLOCK_CD%TYPE	,
			ShipVolumeCD		MA_LOCA.SHIPVOLUME_CD%TYPE		,
			AbcID	 			MA_LOCA.ABC_ID%TYPE				,
			AbcSubID			MA_LOCA.ABCSUB_ID%TYPE
		) IS
			SELECT
				ML.LOCA_CD				,
				ML.ARRIVPRIORITY_NR		,
				ML.LOCA_TX				,
				LS2.KANBAN_CD			,
				LS2.PALLET_NR			,
				ML.LINEUNIT_NR			,
				ML.AREA_CD
			FROM
				MA_LOCA ML
					LEFT OUTER JOIN
						(
						SELECT
							LS.LOCA_CD				,
							LS.KANBAN_CD			,
							LS.SKU_CD				,
							LS.LOT_TX				,
							LS.PALLETCAPACITY_NR	,
							LS.STACKINGNUMBER_NR	,
							SUM(LS.PALLET_NR) AS PALLET_NR,
							SUM(LS.CASE_NR) AS CASE_NR		,
							SUM(LS.BARA_NR) AS BARA_NR
						FROM
							(
							SELECT
								LOCA_CD				,
								KANBAN_CD			,
								SKU_CD				,
								LOT_TX				,
								PALLETCAPACITY_NR	,
								STACKINGNUMBER_NR	,
								PALLET_NR			,
								CASE_NR				,
								BARA_NR				
							FROM
								TA_LOCASTOCKP
							UNION ALL
							SELECT
								LOCA_CD				,
								KANBAN_CD			,
								SKU_CD				,
								LOT_TX				,
								PALLETCAPACITY_NR	,
								STACKINGNUMBER_NR	,
								SUM(PALLET_NR) AS PALLET_NR,
								SUM(CASE_NR) AS CASE_NR		,
								SUM(BARA_NR) AS BARA_NR
							FROM
								TA_LOCASTOCKC
--							WHERE
--								STSTOCK_ID = PS_DEFINE.ST_STOCK1_INAPPLY
							GROUP BY
								LOCA_CD				,
								KANBAN_CD			,
								SKU_CD				,
								LOT_TX				,
								PALLETCAPACITY_NR	,
								STACKINGNUMBER_NR
							) LS
						GROUP BY
							LS.LOCA_CD				,
							LS.KANBAN_CD			,
							LS.SKU_CD				,
							LS.LOT_TX				,
							LS.PALLETCAPACITY_NR	,
							LS.STACKINGNUMBER_NR
						) LS2
						ON ML.LOCA_CD = LS2.LOCA_CD
			WHERE
				ML.TYPELOCABLOCK_CD	= TypeLocaBlockCD	AND
				ML.TYPELOCA_CD		= TypeLocaCD		AND
				ML.SHIPVOLUME_CD	= ShipVolumeCD		AND
				ML.ABC_ID			= AbcID				AND
				ML.ABCSUB_ID		= AbcSubID			AND
				ML.NOTUSE_FL		= 0					AND
				LS2.LOCA_CD IS NOT NULL					AND
				LS2.SKU_CD			= SkuCD				AND
				LS2.LOT_TX			= LotTX				AND
				LS2.PALLETCAPACITY_NR = PalletCapacityNR	AND
				LS2.PALLET_NR		< ML.LINEUNIT_NR * LS2.STACKINGNUMBER_NR AND
				LS2.CASE_NR			= 0					AND
				LS2.BARA_NR			= 0
			ORDER BY
				LS2.PALLET_NR DESC		,
				ML.LOCA_TX				;
		rowLocaStock	curLocaStock%ROWTYPE;
		--
		rowLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		numInStockID	TA_INSTOCK.INSTOCK_ID%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START, ' ||
			'iTrnID=[' || iTrnID || '], ' ||
			'iTypeLocaCD=[' || iTypeLocaCD || '], ' ||
			'iTypeLocaBlockCD=[' || iTypeLocaBlockCD || '], ' ||
			'iShipVolumeCD=[' || iShipVolumeCD || '], ' ||
			'SKU_CD=[' || irowSku.SKU_CD || '], ' ||
			'LOT_TX=[' || iLotTX || '], ' ||
			'ABC_ID=[' || iAbcID || '], ' ||
			'ABCSUB_ID=[' || irowSku.ABCSUB_ID || ']'
		);
		OPEN curLocaStock(
			irowSku.SKU_CD	,
			iLotTX			,
			iPalletCapacityNR,
			iTypeLocaCD		,
			iTypeLocaBlockCD,
			iShipVolumeCD	,
			iAbcID			,
			irowSku.ABCSUB_ID
		);
		oEndFL := 0;
		oAreaCD := '';
		LOOP
			FETCH curLocaStock INTO rowLocaStock;
			EXIT WHEN curLocaStock%NOTFOUND;
			EXIT WHEN oEndFL = 1;
			PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' FOUND, ' ||
				'iTrnID=[' || iTrnID || '], ' ||
				'SKU_CD=[' || irowSku.SKU_CD || '], ' ||
				'LOT_TX=[' || iLotTX || '], ' ||
				'LOCA_CD=[' || rowLocaStock.LOCA_CD || ']'
			);
			oEndFL := 1;
			oKanbanCD := rowLocaStock.KANBAN_CD;
			oLocaCD := rowLocaStock.LOCA_CD;
			-- 
			oAreaCD := rowLocaStock.AREA_CD;
			
		END LOOP;
		CLOSE curLocaStock;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END,' ||
			'oEndFL=[' || oEndFL || '], ' ||
			'oKanbanCD=[' || oKanbanCD || '], ' ||
			'oLocaCD=[' || oLocaCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStock%ISOPEN THEN
				CLOSE curLocaStock;
			END IF;
			RAISE;
	END ApplyStock;
END PS_APPLYINSTOCK;
/
SHOW ERRORS
