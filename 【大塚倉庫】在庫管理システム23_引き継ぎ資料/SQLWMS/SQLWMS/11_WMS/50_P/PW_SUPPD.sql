-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PW_SUPP AS
	/************************************************************************************/
	/*																					*/
	/*									Private											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************************************/
	/*	かんばん更新																	*/
	/************************************************************************************/
	PROCEDURE UpdKanban(
		iNewKanbanCD		IN	VARCHAR2,
		iOldKanbanCD		IN	VARCHAR2,
		iDestLocaCD			IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iCasePerPalletNR	IN	NUMBER	,
		iPalletLayerNR		IN	NUMBER	,
		iStackingNumberNR	IN	NUMBER	,
		iTypePalletCD		IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdKanban';
		
		CURSOR curKanban(KanbanCD MA_KANBAN.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_KANBAN
			WHERE
				KANBAN_CD = KanbanCD;
		rowKanban 		curKanban%ROWTYPE;
		CURSOR curUseKanban(KanbanCD TA_KANBANP.KANBAN_CD%TYPE,
							SkuCD TA_KANBANP.SKU_CD%TYPE,
							LotTX TA_KANBANP.LOT_TX%TYPE) IS
			SELECT
				*
			FROM
				TA_KANBANP
			WHERE
				KANBAN_CD	= KanbanCD	AND
				(
				SKU_CD <> SkuCD OR
				LOT_TX <> LotTX
				);
		rowUseKanban 		curUseKanban%ROWTYPE;
		rowLoca		 		MA_LOCA%ROWTYPE;
		numKanbanPID		NUMBER := 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		--
		PS_MASTER.GetLoca(iDestLocaCD, rowLoca);
		-- かんばんマスタチェック
		OPEN curKanban(iNewKanbanCD);
		FETCH curKanban INTO rowKanban;
		IF curKanban%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんがマスタに登録されていません。KANBAN_CD=[' || iNewKanbanCD || ']');
		END IF;
		CLOSE curKanban;
		--使用済みかんばん
		OPEN curUseKanban(iNewKanbanCD,iSkuCD,iLotTX);
		FETCH curUseKanban INTO rowUseKanban;
		IF curUseKanban%FOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんが使用されています。' ||
																'KANBAN_CD=[' || iNewKanbanCD || '], ' ||
																'SKU_CD=[' || iSkuCD || '], ' ||
																'LOT_TX=[' || iLotTX || ']'
																);
		END IF;
		CLOSE curUseKanban;
		-- 旧かんばん削除
		PS_STOCK.DelKanban(iOldKanbanCD);
		--DELETE
		--	TA_KANBANP
		--WHERE
		--	KANBAN_CD = iOldKanbanCD;
		
		IF iOldKanbanCD <> iNewKanbanCD THEN
			-- ID取得
			SELECT
				NVL(MAX(KANBANP_ID),0)
			INTO
				numKanbanPID
			FROM
				TA_KANBANP;
			numKanbanPID := numKanbanPID + 1;
			-- 引当済かんばん登録
			INSERT INTO TA_KANBANP (
				KANBANP_ID			,
				STKANBAN_CD			,
				TYPEPKANBAN_CD		,
				KANBAN_CD			,
				WH_CD				,
				FLOOR_CD			,
				AREA_CD				,
				LINE_CD				,
				SKU_CD				,
				LOT_TX				,
				CASEPERPALLET_NR	,
				PALLETLAYER_NR		,
				STACKINGNUMBER_NR	,
				TYPEPLALLET_CD		,
				-- 管理項目
				UPD_DT				,
				UPDUSER_CD			,
				UPDUSER_TX			,
				ADD_DT				,
				ADDUSER_CD			,
				ADDUSER_TX			,
				UPDPROGRAM_CD		,
				UPDCOUNTER_NR		,
				STRECORD_ID			
			) VALUES (
				numKanbanPID				,
				'00'						,
				'00'						,
				iNewKanbanCD				,
				rowLoca.WH_CD				,
				rowLoca.FLOOR_CD			,
				rowLoca.AREA_CD				,
				rowLoca.LINE_CD				,
				iSkuCD						,
				iLotTX						,
				iCasePerPalletNR			,
				iPalletLayerNR				,
				iStackingNumberNR			,
				iTypePalletCD				,
				--
				SYSDATE						,
				iUserCD						,
				iUserTX						,
				SYSDATE						,
				iUserCD						,
				iUserTX						,
				FUNCTION_NAME				,
				0							,
				0
			);
		END IF;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curKanban%ISOPEN THEN CLOSE curKanban; END IF;
			IF curUseKanban%ISOPEN THEN CLOSE curUseKanban; END IF;
			RAISE;
	END UpdKanban;
	/************************************************************************************/
	/*	補充元登録更新																	*/
	/************************************************************************************/
	PROCEDURE UpdSrcSupp(
		iSuppNoCD			IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iKanbanCD			IN	VARCHAR2,
		iAmountNR			IN	NUMBER	,
		iPalletCapacityNR	IN	NUMBER	,
		iPalletNR			IN	NUMBER	,
		iCaseNR				IN	NUMBER	,
		iBaraNR				IN	NUMBER	,
		iPcsNR				IN	NUMBER	,
		iSrcLocaCD			IN	VARCHAR2,
		iDestLocaCD			IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdSrcSupp';
		
		CURSOR curSupp(
			SuppNoCD	TA_SUPP.SUPPNO_CD%TYPE	,
			SkuCD		TA_SUPP.SKU_CD%TYPE		,
			SrcLocaCD	TA_SUPP.SRCLOCA_CD%TYPE	,
			DestLocaCD	TA_SUPP.DESTLOCA_CD%TYPE
		) IS
			SELECT
				TS.ROWID				,
				TS.TRN_ID				,
				TS.PICK_ID				,
				TS.SHIPD_ID				,
				TS.SRCLOCA_CD			,
				TS.SRCLOCASTOCKC_ID		,
				TS.DESTLOCA_CD			,
				TS.DESTTYPESTOCK_CD		,
				TS.DESTLOCASTOCKC_ID	,
				TS.AMOUNT_NR			,
				TS.PALLET_NR			,
				TS.CASE_NR				,
				TS.BARA_NR				,
				TS.PCS_NR				,
				TS.CASEPERPALLET_NR		,
				TS.PALLETLAYER_NR		,
				TS.PALLETCAPACITY_NR	,
				TS.STACKINGNUMBER_NR	,
				TS.TYPEPLALLET_CD
			FROM
				TA_SUPP TS,
				TA_PICK TP
			WHERE
				TS.PICK_ID		= TP.PICK_ID (+)	AND
				TS.SHIPD_ID		= TP.SHIPD_ID (+)	AND
				TS.SUPPNO_CD	= SuppNoCD		AND
				TS.SKU_CD		= SkuCD			AND
				TS.SRCLOCA_CD	= SrcLocaCD 	AND
				TS.DESTLOCA_CD	= DestLocaCD
			ORDER BY
				TS.SUPP_ID;
		rowSupp 	curSupp%ROWTYPE;
		
		rowLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		rowDLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		numCase			TA_SUPP.CASE_NR%TYPE	:= 0;
		numBara			TA_SUPP.BARA_NR%TYPE	:= 0;
		numPcs			TA_SUPP.PCS_NR%TYPE		:= 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- ピッキング指示更新
		OPEN curSupp(iSuppNoCD, iSkuCd, iSrcLocaCD, iDestLocaCD);
		LOOP
			FETCH curSupp INTO rowSupp;
			EXIT WHEN curSupp%NOTFOUND;
			
			-- 予約引当削除（先ロケ）
			PS_LOCA.GetLockLocaStockC(rowSupp.DESTLOCASTOCKC_ID, rowLocaStockC);
			-- 引当済み在庫引き落とし
			PS_STOCK.DelC(rowSupp.DESTLOCASTOCKC_ID	,
							iUserCD					,
							iUserTX					,
							FUNCTION_NAME);
			
			PS_STOCK.OutStockTemp(
				rowSupp.TRN_ID				,	-- iTrnID
				rowSupp.DESTLOCA_CD			,
				iKanbanCD					,
				rowSupp.SRCLOCASTOCKC_ID	,
				iAmountNR					,
				rowSupp.PALLET_NR			,
				rowSupp.CASE_NR				,
				rowSupp.BARA_NR				,
				rowSupp.PCS_NR				,
				PS_DEFINE.ST_STOCK11_SUPP	,
				iUserCD						,
				iUserTX						,
				rowDLocaStockC
			);
			-- 更新
			UPDATE TA_SUPP
			SET
				DESTLOCASTOCKC_ID	= rowDLocaStockC.LOCASTOCKC_ID
			WHERE
				ROWID = rowSupp.ROWID;
			-- ピック対象のみ在庫ID更新
			IF rowSupp.SHIPD_ID <> 0 THEN
				-- ピッキングデータ・補充元消し込み
				PS_PICK.UpdPickSrcSupp(
					rowSupp.PICK_ID				,
					rowSupp.SRCLOCASTOCKC_ID	,	--old
					rowDLocaStockC.LOCASTOCKC_ID,	--new
					rowDLocaStockC.LOCA_CD		,
					iUserCD,
					iUserTX
				);
			END IF;
		END LOOP;
		CLOSE curSupp;
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curSupp%ISOPEN THEN CLOSE curSupp; END IF;
			RAISE;
	END UpdSrcSupp;
	/************************************************************************************/
	/*	補充先登録更新																	*/
	/************************************************************************************/
	PROCEDURE UpdDestSupp(
		iSuppNoCD			IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iKanbanCD			IN	VARCHAR2,
		iAmountNR			IN	NUMBER	,
		iPalletCapacityNR	IN	NUMBER	,
		iPalletNR			IN	NUMBER	,
		iCaseNR				IN	NUMBER	,
		iBaraNR				IN	NUMBER	,
		iPcsNR				IN	NUMBER	,
		iSrcLocaCD			IN	VARCHAR2,
		iDestLocaCD			IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'UpdDestSupp';
		
		CURSOR curSupp(
			SuppNoCD	TA_SUPP.SUPPNO_CD%TYPE	,
			SkuCD		TA_SUPP.SKU_CD%TYPE		,
			SrcLocaCD	TA_SUPP.SRCLOCA_CD%TYPE	,
			DestLocaCD	TA_SUPP.DESTLOCA_CD%TYPE
		) IS
			SELECT
				TS.ROWID				,
				TS.TRN_ID				,
				TS.PICK_ID				,
				TS.SHIPD_ID				,
				TS.SRCLOCA_CD			,
				TS.SRCLOCASTOCKC_ID		,
				TS.DESTLOCA_CD			,
				TS.DESTTYPESTOCK_CD		,
				TS.DESTLOCASTOCKC_ID	,
				TS.AMOUNT_NR			,
				TS.PALLET_NR			,
				TS.CASE_NR				,
				TS.BARA_NR				,
				TS.PCS_NR				,
				TS.CASEPERPALLET_NR		,
				TS.PALLETLAYER_NR		,
				TS.PALLETCAPACITY_NR	,
				TS.STACKINGNUMBER_NR	,
				TS.TYPEPLALLET_CD
			FROM
				TA_SUPP TS,
				TA_PICK TP
			WHERE
				TS.PICK_ID		= TP.PICK_ID (+)	AND
				TS.SHIPD_ID		= TP.SHIPD_ID (+)	AND
				TS.SUPPNO_CD	= SuppNoCD		AND
				TS.SKU_CD		= SkuCD			AND
				TS.SRCLOCA_CD	= SrcLocaCD 	AND
				TS.DESTLOCA_CD	= DestLocaCD
			ORDER BY
				TS.SUPP_ID;
		rowSupp 	curSupp%ROWTYPE;
		
		rowLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		rowDLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		numCase			TA_SUPP.CASE_NR%TYPE	:= 0;
		numBara			TA_SUPP.BARA_NR%TYPE	:= 0;
		numPcs			TA_SUPP.PCS_NR%TYPE		:= 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- ピッキング指示更新
		OPEN curSupp(iSuppNoCD, iSkuCd, iSrcLocaCD, iDestLocaCD);
		LOOP
			FETCH curSupp INTO rowSupp;
			EXIT WHEN curSupp%NOTFOUND;
			numPcs 	:= rowSupp.PCS_NR;
			numCase	:= TRUNC(numPcs / iAmountNR);
			numBara	:= MOD(numPcs, iAmountNR);
							
			-- todo
			UPDATE
				TA_LOCASTOCKC
			SET
				LOCA_CD			= iDestLocaCD,
				KANBAN_CD		= iKanbanCD
			WHERE
				LOCASTOCKC_ID = rowSupp.SRCLOCASTOCKC_ID;
			-- 過剰補充分を格納
			IF rowSupp.SHIPD_ID = 0 THEN
				-- 予約引当削除（先ロケ）
				PS_LOCA.GetLockLocaStockC(rowSupp.DESTLOCASTOCKC_ID, rowLocaStockC);
				-- 引当済み在庫引き落とし
				PS_STOCK.DelC(rowSupp.DESTLOCASTOCKC_ID	,
								iUserCD					,
								iUserTX					,
								FUNCTION_NAME);
				--格納
				PS_STOCK.InStockNoResult(
					iDestLocaCD				,
					iSkuCD 					,
					iLotTX					,
					iKanbanCD				,
					PS_DEFINE.USELIMITDT_DEFAULT,	-- todo iUseLimitDT
					' '						,	-- iBatchNoCD
					TO_DATE(TO_CHAR(SYSDATE,'YYYYMM') || '01','YYYYMMDD'),
					rowSupp.AMOUNT_NR		,
					rowSupp.PALLET_NR		,
					rowSupp.CASE_NR			,
					rowSupp.BARA_NR			,
					rowSupp.PCS_NR			,
					rowSupp.CASEPERPALLET_NR,
					rowSupp.PALLETLAYER_NR	,
					rowSupp.PALLETCAPACITY_NR,
					rowSupp.STACKINGNUMBER_NR,
					rowSupp.TYPEPLALLET_CD	,
					rowSupp.DESTTYPESTOCK_CD,
					iUserCD					,
					iUserTX					,
					iProgramCD
				);
			ELSE
				-- 在庫出庫（一時引当済みから引当済数）
				PS_STOCK.InStockTempOut(
					rowSupp.TRN_ID				,
					iDestLocaCD					,
					iKanbanCD					,
					rowSupp.DESTLOCASTOCKC_ID	,
					iAmountNR					,
					rowSupp.PALLET_NR			,
					rowSupp.CASE_NR				,
					rowSupp.BARA_NR				,
					rowSupp.PCS_NR				,
					PS_DEFINE.ST_STOCK0_NOR		,
					iUserCD						,
					iUserTX						,
					rowDLocaStockC
				);
				
				-- ピッキングデータ・補充先消し込み
				PS_PICK.UpdPickDestSupp(
					rowSupp.PICK_ID				,
					rowSupp.DESTLOCASTOCKC_ID	,	--old
					rowDLocaStockC.LOCASTOCKC_ID,	--new
					rowDLocaStockC.LOCA_CD		,
					iUserCD						,
					iUserTX
				);
			END IF;
		END LOOP;
		CLOSE curSupp;
		
		-- ステータス更新
		UPDATE
			TA_SUPP
		SET
			STSUPP_ID = PS_DEFINE.ST_SUPP8_SUPPINOK_E
		WHERE
			SUPPNO_CD = iSuppNoCD;
		
		-- ピッキングデータ・補充消し込み
		PS_PICK.UpdPickEndSupp(
			iSuppNoCD	,
			iUserCD		,
			iUserTX
		);
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curSupp%ISOPEN THEN CLOSE curSupp; END IF;
			RAISE;
	END UpdDestSupp;
	/************************************************************************************/
	/*																					*/
	/*									Public											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************/
	/*	補充元開始												*/
	/************************************************************/
	PROCEDURE StartSrcSupp(
		iSuppNoCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'StartSrcSupp';
		
		CURSOR curSupp(SuppNoCD TA_SUPP.SUPPNO_CD%TYPE) IS
			SELECT
				TYPESUPP_CD						,
				MAX(STSUPP_ID) AS STSUPP_ID		,
				MAX(SRCEND_DT) AS SRCEND_DT		,
				MAX(DESTEND_DT) AS DESTEND_DT	,
				MAX(START_DT) AS START_DT		,
				MAX(END_DT) AS END_DT
			FROM
				TA_SUPP
			WHERE
				SUPPNO_CD = SuppNoCD
			GROUP BY
				TYPESUPP_CD;
		rowSupp 		curSupp%ROWTYPE;
		
		rowUser			VA_USER%ROWTYPE;
		rowSuppInfo		TA_SUPP%ROWTYPE;
		rowLoca			MA_LOCA%ROWTYPE;
		numAmount	 	MA_SKU.PCSPERCASE_NR%TYPE	:= 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		
		OPEN curSupp(iSuppNoCD);
		FETCH curSupp INTO rowSupp;
		-- 指示書存在チェック
		IF curSupp%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,' 指示書番号が存在しません。');
		END IF;
		-- キャンセルチェック
		IF rowSupp.STSUPP_ID = PS_DEFINE.ST_SUPPM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書番号がキャンセルされています。');
		END IF;
		-- 指示書種類チェック
		IF rowSupp.TYPESUPP_CD != PS_DEFINE.TYPE_SUPP_EMG THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '該当指示書ではありません。');
		END IF;
		-- 指示書完了チェック
		IF rowSupp.DESTEND_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '全補充が完了しています。');
		END IF;

		-- 指示書完了チェック
		IF rowSupp.END_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。');
		END IF;
		
		--
		UPDATE
			TA_SUPP
		SET
			STSUPP_ID		= PS_DEFINE.ST_SUPP2_SUPPOUT_S,
			--
			SRCSTART_DT		= SYSDATE			,
			SRCHTUSER_CD	= iUserCD			,
			SRCHTUSER_TX	= rowUser.USER_TX	,
			START_DT		= SYSDATE			,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			SUPPNO_CD		= iSuppNoCD			AND
			STSUPP_ID		= PS_DEFINE.ST_SUPP0_NOTSUPP;
		CLOSE curSupp;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curSupp%ISOPEN THEN CLOSE curSupp; END IF;
			RAISE;
	END StartSrcSupp;
	/************************************************************/
	/*	補充元完了												*/
	/*  完パレ単位での補充をすることを想定する					*/
	/************************************************************/
	PROCEDURE EndSrcSupp(
		iSuppNoCD		IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		iPalletNR		IN	NUMBER	,
		iCaseNR			IN	NUMBER	,
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iSrcLocaCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'EndSrcSupp';
		
		CURSOR curSupp(SuppNoCD TA_SUPP.SUPPNO_CD%TYPE) IS
			SELECT
				SUPP_ID							,
				SKU_CD							,
				SRCKANBAN_CD					,
				DESTKANBAN_CD					,
				LOT_TX							,
				AMOUNT_NR						,
				USELIMIT_DT						,
				STOCK_DT						,
				TYPESUPP_CD						,
				SRCLOCA_CD						,
				DESTLOCA_CD						,
				SRCTYPESTOCK_CD					,
				DESTTYPESTOCK_CD				,
				CASEPERPALLET_NR				,
				PALLETLAYER_NR					,
				PALLETCAPACITY_NR				,
				STACKINGNUMBER_NR				,
				TYPEPLALLET_CD					,
				STSUPP_ID						,
				SRCSTART_DT						,
				SRCEND_DT						,
				DESTEND_DT						,
				START_DT						,
				END_DT							,
				PALLET_NR						,
				CASE_NR							,
				BARA_NR							,
				PCS_NR							,
				SRCPALLET_NR					,
				SRCCASE_NR						,
				SRCBARA_NR						,
				SRCPCS_NR
			FROM
				TA_SUPP
			WHERE
				SUPPNO_CD = SuppNoCD
			ORDER BY
				SUPP_ID;
		rowSupp 		curSupp%ROWTYPE;
		-- 完了確認
		CURSOR curCmpSupp(SuppNoCD TA_SUPP.SUPPNO_CD%TYPE) IS
			SELECT
				SUPPNO_CD					,
				SUM(PCS_NR) AS PCS_NR		,
				SUM(SRCPCS_NR) AS SRCPCS_NR
			FROM
				TA_SUPP
			WHERE
				SUPPNO_CD = SuppNoCD
			GROUP BY
				SUPPNO_CD;
		rowCmpSupp 		curCmpSupp%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
		rowSku			MA_SKU%ROWTYPE;
		rowLoca		 	MA_LOCA%ROWTYPE;
		numAmount	 	MA_SKU.PCSPERCASE_NR%TYPE	:= 0;
		
		numPalletNR		TA_SUPP.PALLET_NR%TYPE;
		numCaseNR		TA_SUPP.CASE_NR%TYPE;
		numBaraNR		TA_SUPP.BARA_NR%TYPE;
		numPcsNR		TA_SUPP.PCS_NR%TYPE;
		numImpPalletNR	TA_SUPP.PALLET_NR%TYPE;
		numImpCaseNR	TA_SUPP.CASE_NR%TYPE;
		numImpBaraNR	TA_SUPP.BARA_NR%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		PS_MASTER.GetSku(iSkuCD, rowSku);
		PS_MASTER.GetLoca(iSrcLocaCD, rowLoca);
		
		-- 入力されたピース数を別の変数としてセット
		numPcsNR := iPcsNR;
		
		OPEN curSupp(iSuppNoCD);
		Loop
			FETCH curSupp INTO rowSupp;
			EXIT WHEN curSupp%NOTFOUND;
			EXIT WHEN numPcsNR = 0;
			-- 指示書存在チェック
			IF curSupp%NOTFOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,' 指示書番号が存在しません。');
			END IF;
			-- キャンセルチェック
			IF rowSupp.STSUPP_ID = PS_DEFINE.ST_SUPPM2_CANCEL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書番号がキャンセルされています。');
			END IF;
			-- 指示書種類チェック
			IF rowSupp.TYPESUPP_CD != PS_DEFINE.TYPE_SUPP_EMG THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '該当指示書ではありません。');
			END IF;
			-- 指示書完了チェック
			IF rowSupp.DESTEND_DT IS NOT NULL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '全補充が完了しています。');
			END IF;

			-- 指示書完了チェック
			IF rowSupp.END_DT IS NOT NULL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。');
			END IF;
			-- ロケチェック
			IF iSrcLocaCD != rowSupp.SRCLOCA_CD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '元ロケーションが指示と異なります。');
			END IF;
			
			numAmount := rowSku.PCSPERCASE_NR;
			-- 補充数量取得
			PS_STOCK.GetUnitPcsNr(
				iSkuCD			,
				rowSupp.PCS_NR	,
				rowSupp.PALLETCAPACITY_NR	,
				numPalletNR		,
				numCaseNR		,
				numBaraNR		
				);
			-- 入力数量取得
			PS_STOCK.GetUnitPcsNr(
				iSkuCD			,
				numPcsNR		,
				rowSupp.PALLETCAPACITY_NR	,
				numImpPalletNR	,
				numImpCaseNR	,
				numImpBaraNR	
				);
			-- 処理
			
			IF numPcsNR = (rowSupp.PCS_NR - rowSupp.SRCPCS_NR) THEN
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN A');
				-- 残りのピース数(バラ単位)が入力されたピース数と等しい場合(=補充元完了)
				UPDATE
					TA_SUPP
				SET
					SRCPALLET_NR	= SRCPALLET_NR + numImpPalletNR	,
					SRCCASE_NR		= SRCCASE_NR + numImpCaseNR		,
					SRCBARA_NR		= SRCBARA_NR + numImpBaraNR		,
					SRCPCS_NR		= SRCPCS_NR + numPcsNR			,
					SRCEND_DT		= SYSDATE						,
					SRCHTUSER_CD	= iUserCD						,
					SRCHTUSER_TX	= rowUser.USER_TX				
				WHERE
					SUPP_ID			= rowSupp.SUPP_ID;
				
				UPDATE
					TA_SUPP
				SET
					--
					UPD_DT			= SYSDATE					,
					UPDUSER_CD		= iUserCD					,
					UPDUSER_TX		= rowUser.USER_TX			,
					UPDPROGRAM_CD	= iProgramCD				,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
					--
				WHERE
					SUPPNO_CD		= iSuppNoCD;
				numPcsNR := 0;
			ELSIF numPcsNR < (rowSupp.PCS_NR - rowSupp.SRCPCS_NR) THEN
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN B');
				-- 残りのピース数(バラ単位)が入力されたピース数より多い場合(補充指示に対して入力数が少ない場合)
				UPDATE
					TA_SUPP
				SET
					SRCPALLET_NR	= SRCPALLET_NR + numImpPalletNR	,
					SRCCASE_NR		= SRCCASE_NR + numImpCaseNR		,
					SRCBARA_NR		= SRCBARA_NR + numImpBaraNR		,
					SRCPCS_NR		= SRCPCS_NR + numPcsNR			,
					--
					UPD_DT			= SYSDATE						,
					UPDUSER_CD		= iUserCD						,
					UPDUSER_TX		= rowUser.USER_TX				,
					UPDPROGRAM_CD	= iProgramCD					,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1				
				WHERE
					SUPP_ID		= rowSupp.SUPP_ID;
				numPcsNR := 0;
			ELSE
			PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN C');
				-- 残りのピース数(バラ単位)が入力されたピース数より多い場合(補充指示に対して入力が多い場合 = レコードを完了して次のレコードを検索)
				UPDATE
					TA_SUPP
				SET
					SRCPALLET_NR	= rowSupp.PALLET_NR				,
					SRCCASE_NR		= rowSupp.CASE_NR				,
					SRCBARA_NR		= rowSupp.BARA_NR				,
					SRCPCS_NR		= rowSupp.PCS_NR				,
					--
					UPD_DT			= SYSDATE						,
					UPDUSER_CD		= iUserCD						,
					UPDUSER_TX		= rowUser.USER_TX				,
					UPDPROGRAM_CD	= iProgramCD					,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
				WHERE
					SUPP_ID		= rowSupp.SUPP_ID;
				numPcsNR := numPcsNR - (rowSupp.PCS_NR - rowSupp.SRCPCS_NR);
			END IF;
		END LOOP;
		CLOSE curSupp;
		IF numPcsNR <> 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '補充完了のピース数が不正です。');
		END IF;
		-- 受払作成
			-- 出庫
		PS_STOCK.InsStockInOut(	
			' '							,
			' '							,
			rowSupp.SRCLOCA_CD			,
			rowSupp.SKU_CD				,
			rowSupp.LOT_TX				,
			rowSupp.SRCKANBAN_CD		,
			rowSupp.CASEPERPALLET_NR	,
			rowSupp.PALLETLAYER_NR		,
			rowSupp.PALLETCAPACITY_NR	,
			rowSupp.STACKINGNUMBER_NR	,
			rowSupp.TYPEPLALLET_CD		,	-- iTypePalletCD
			rowSupp.USELIMIT_DT			,
			' '							,	-- BATCHNO_CD
			rowSupp.STOCK_DT			,
			rowSupp.AMOUNT_NR			,
			iPalletNR* (-1) 			,
			iCaseNR	* (-1) 				,
			iBaraNR	* (-1) 				,
			iPcsNR * (-1) 				,
			rowSupp.SRCTYPESTOCK_CD		,
			rowSupp.SRCTYPESTOCK_CD		,	-- todo iTypeLocaCD
			'400'						,	-- 補充出庫
			iSuppNoCD					,
			0			  				,
			'SUPP'						,
			0							,	-- iNeedSendFL
			iUserCD						,
			rowUser.USER_TX				,
			iProgramCD
		);
		-- ステータス更新
		UPDATE
			TA_SUPP
		SET
			STSUPP_ID = PS_DEFINE.ST_SUPP4_SUPPOUTOK_E
		WHERE
			SUPPNO_CD = iSuppNoCD;
		--
		OPEN curCmpSupp(iSuppNoCD);
		FETCH curCmpSupp INTO rowCmpSupp;
		IF rowCmpSupp.PCS_NR = rowCmpSupp.SRCPCS_NR THEN
			UPDATE
				TA_SUPP
			SET
				SRCEND_DT		= SYSDATE			,
				SRCHTUSER_CD	= iUserCD			,
				SRCHTUSER_TX	= rowUser.USER_TX	
			WHERE
				SUPPNO_CD		= iSuppNoCD;
		END IF;
		CLOSE curCmpSupp;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curSupp%ISOPEN THEN CLOSE curSupp; END IF;
			IF curCmpSupp%ISOPEN THEN CLOSE curCmpSupp; END IF;
			RAISE;
	END EndSrcSupp;
	/************************************************************/
	/*	補充先開始												*/
	/************************************************************/
	PROCEDURE StartDestSupp(
		iSuppNoCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'StartDestSupp';
		
		CURSOR curSupp(SuppNoCD TA_SUPP.SUPPNO_CD%TYPE) IS
			SELECT
				TYPESUPP_CD						,
				MAX(STSUPP_ID) AS STSUPP_ID		,
				MAX(SRCEND_DT) AS SRCEND_DT		,
				MAX(DESTEND_DT) AS DESTEND_DT	,
				MAX(START_DT) AS START_DT		,
				MAX(END_DT) AS END_DT
			FROM
				TA_SUPP
			WHERE
				SUPPNO_CD = SuppNoCD
			GROUP BY
				TYPESUPP_CD;
		rowSupp 		curSupp%ROWTYPE;
		
		rowUser			VA_USER%ROWTYPE;
		rowSuppInfo		TA_SUPP%ROWTYPE;
		rowLoca			MA_LOCA%ROWTYPE;
		numAmount	 	MA_SKU.PCSPERCASE_NR%TYPE	:= 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		
		OPEN curSupp(iSuppNoCD);
		FETCH curSupp INTO rowSupp;
		-- 指示書存在チェック
		IF curSupp%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,' 指示書番号が存在しません。');
		END IF;
		-- キャンセルチェック
		IF rowSupp.STSUPP_ID = PS_DEFINE.ST_SUPPM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書番号がキャンセルされています。');
		END IF;
		-- 指示書種類チェック
		IF rowSupp.TYPESUPP_CD != PS_DEFINE.TYPE_SUPP_EMG THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '該当指示書ではありません。');
		END IF;
		-- 指示書完了チェック
		IF rowSupp.DESTEND_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '全補充が完了しています。');
		END IF;

		-- 指示書完了チェック
		IF rowSupp.END_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。');
		END IF;
		
		--
		UPDATE
			TA_SUPP
		SET
			STSUPP_ID		= PS_DEFINE.ST_SUPP6_SUPPIN_S,
			--
			DESTSTART_DT	= SYSDATE			,
			DESTHTUSER_CD	= iUserCD			,
			DESTHTUSER_TX	= rowUser.USER_TX	,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			SUPPNO_CD		= iSuppNoCD			;
		CLOSE curSupp;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curSupp%ISOPEN THEN CLOSE curSupp; END IF;
			RAISE;
	END StartDestSupp;
	/************************************************************/
	/*	補充先完了												*/
	/************************************************************/
	PROCEDURE EndDestSupp(
		iSuppNoCD		IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		iKanbanCD		IN	VARCHAR2,
		iPalletNR		IN	NUMBER	,
		iCaseNR			IN	NUMBER	,
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iDestLocaCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'EndDestSupp';
		
		CURSOR curSupp(SuppNoCD TA_SUPP.SUPPNO_CD%TYPE) IS
			SELECT
				SUPP_ID							,
				SKU_CD							,
				SRCKANBAN_CD					,
				DESTKANBAN_CD					,
				LOT_TX							,
				AMOUNT_NR						,
				USELIMIT_DT						,
				STOCK_DT						,
				TYPESUPP_CD						,
				SRCLOCA_CD						,
				DESTLOCA_CD						,
				SRCTYPESTOCK_CD					,
				DESTTYPESTOCK_CD				,
				CASEPERPALLET_NR				,
				PALLETLAYER_NR					,
				PALLETCAPACITY_NR				,
				STACKINGNUMBER_NR				,
				TYPEPLALLET_CD					,
				STSUPP_ID						,
				SRCSTART_DT						,
				SRCEND_DT						,
				DESTEND_DT						,
				START_DT						,
				END_DT							,
				PALLET_NR						,
				CASE_NR							,
				BARA_NR							,
				PCS_NR							,
				DESTPALLET_NR					,
				DESTCASE_NR						,
				DESTBARA_NR						,
				DESTPCS_NR
			FROM
				TA_SUPP
			WHERE
				SUPPNO_CD = SuppNoCD
			ORDER BY
				SUPP_ID;
		rowSupp 		curSupp%ROWTYPE;
		-- 完了確認
		CURSOR curCmpSupp(SuppNoCD TA_SUPP.SUPPNO_CD%TYPE) IS
			SELECT
				SUPPNO_CD					,
				SUM(PCS_NR) AS PCS_NR		,
				SUM(DESTPCS_NR) AS DESTPCS_NR
			FROM
				TA_SUPP
			WHERE
				SUPPNO_CD = SuppNoCD
			GROUP BY
				SUPPNO_CD;
		rowCmpSupp 		curCmpSupp%ROWTYPE;
		
		CURSOR curKanban(KanbanCD MA_KANBAN.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_KANBAN
			WHERE
				KANBAN_CD = KanbanCD;
		rowKanban 		curKanban%ROWTYPE;
		CURSOR curUseKanban(KanbanCD TA_KANBANP.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_KANBANP
			WHERE
				KANBAN_CD = KanbanCD;
		rowUseKanban 		curUseKanban%ROWTYPE;

		rowUser			VA_USER%ROWTYPE;
		rowSku			MA_SKU%ROWTYPE;
		rowLoca		 	MA_LOCA%ROWTYPE;
		chrDestLocaCD	TA_SUPP.DESTLOCA_CD%TYPE;
		numAmount	 	MA_SKU.PCSPERCASE_NR%TYPE	:= 0;
		
		numPalletNR		TA_SUPP.PALLET_NR%TYPE;
		numCaseNR		TA_SUPP.CASE_NR%TYPE;
		numBaraNR		TA_SUPP.BARA_NR%TYPE;
		numPcsNR		TA_SUPP.PCS_NR%TYPE;
		numImpPalletNR	TA_SUPP.PALLET_NR%TYPE;
		numImpCaseNR	TA_SUPP.CASE_NR%TYPE;
		numImpBaraNR	TA_SUPP.BARA_NR%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		PS_MASTER.GetSku(iSkuCD, rowSku);
		PS_MASTER.GetLoca(iDestLocaCD, rowLoca);
		
		-- 入力されたピース数を別の変数としてセット
		numPcsNR := iPcsNR;
		
		OPEN curSupp(iSuppNoCD);
		Loop
			FETCH curSupp INTO rowSupp;
			EXIT WHEN curSupp%NOTFOUND;
			EXIT WHEN numPcsNR = 0;
			-- 指示書存在チェック
			IF curSupp%NOTFOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,' 指示書番号が存在しません。');
			END IF;
			-- キャンセルチェック
			IF rowSupp.STSUPP_ID = PS_DEFINE.ST_SUPPM2_CANCEL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書番号がキャンセルされています。');
			END IF;
			-- 指示書種類チェック
			IF rowSupp.TYPESUPP_CD != PS_DEFINE.TYPE_SUPP_EMG THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '該当指示書ではありません。');
			END IF;
			-- 指示書完了チェック
			IF rowSupp.DESTEND_DT IS NOT NULL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '全補充が完了しています。');
			END IF;
			-- 指示書完了チェック
			IF rowSupp.END_DT IS NOT NULL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。');
			END IF;
			-- ロケチェック
			IF iDestLocaCD != rowSupp.DESTLOCA_CD THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '先ロケーションが指示と異なります。');
			END IF;
			-- かんばんチェック
			IF rowSupp.STSUPP_ID = PS_DEFINE.ST_SUPP7_SUPPIN_ENT THEN
				IF rowSupp.DESTKANBAN_CD <> iKanbanCD THEN
					RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '前回入力されたかんばんと異なります。');
				END IF;
			END IF;
			
			IF RTRIM(iKanbanCD) = '' THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんが入力されていません。');
			END IF;
			-- かんばんマスタチェック
			OPEN curKanban(iKanbanCD);
			FETCH curKanban INTO rowKanban;
			IF curKanban%NOTFOUND THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんがマスタに登録されていません。KANBAN_CD=[' || iKanbanCD || ']');
			END IF;
			CLOSE curKanban;
			numAmount := rowSku.PCSPERCASE_NR;
			-- 補充数量取得
			PS_STOCK.GetUnitPcsNr(
				iSkuCD			,
				rowSupp.PCS_NR	,
				rowSupp.PALLETCAPACITY_NR	,
				numPalletNR		,
				numCaseNR		,
				numBaraNR		
				);
			-- 入力数量取得
			PS_STOCK.GetUnitPcsNr(
				iSkuCD			,
				numPcsNR		,
				rowSupp.PALLETCAPACITY_NR	,
				numImpPalletNR	,
				numImpCaseNR	,
				numImpBaraNR	
				);
			-- 処理
			-- 残りのピース数(バラ単位)が入力されたピース数より多い場合(補充指示に対して入力が多い場合 = レコードを完了して次のレコードを検索)
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'STARTING PATTERN numPcsNR='||numPcsNR);
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || 'STARTING PATTERN rowSupp.PCS_NR - rowSupp.DESTPCS_NR='||(rowSupp.PCS_NR - rowSupp.DESTPCS_NR));

			IF numPcsNR > (rowSupp.PCS_NR - rowSupp.DESTPCS_NR) THEN
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN A numPcsNR='||numPcsNR);
				-- 更新のみ
				UPDATE
					TA_SUPP
				SET
					DESTKANBAN_CD	= iKanbanCD						,
					DESTPALLET_NR	= rowSupp.PALLET_NR				,
					DESTCASE_NR		= rowSupp.CASE_NR				,
					DESTBARA_NR		= rowSupp.BARA_NR				,
					DESTPCS_NR		= rowSupp.PCS_NR				,
					--
					UPD_DT			= SYSDATE					,
					UPDUSER_CD		= iUserCD					,
					UPDUSER_TX		= rowUser.USER_TX			,
					UPDPROGRAM_CD	= iProgramCD				,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
					--
				WHERE
					SUPP_ID		= rowSupp.SUPP_ID;
				numPcsNR := numPcsNR - (rowSupp.PCS_NR - rowSupp.DESTPCS_NR);
				
			-- 残りのピース数(バラ単位)が入力されたピース数と等しい場合(=補充元完了)
			ELSIF  numPcsNR = (rowSupp.PCS_NR - rowSupp.DESTPCS_NR) THEN
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN B numPcsNR='||numPcsNR);
				--入庫チェック
				PS_STOCK.ChkInStock(
					iDestLocaCD 	,
					iSkuCD 			,
					PS_DEFINE.USELIMITDT_DEFAULT,	-- todo iUseLimitDT
					' '				,
					TO_DATE(TO_CHAR(SYSDATE,'YYYYMM') || '01','YYYYMMDD'),
					rowSupp.AMOUNT_NR,
					rowSupp.CASE_NR,
					rowSupp.BARA_NR,
					rowSupp.PCS_NR	,
					rowSupp.DESTTYPESTOCK_CD,	-- todo iTypeStockCD
					iUserCD			,
					rowUser.USER_TX
				);
				--在庫増加
				--補充情報更新
				PW_SUPP.UpdSrcSupp(
					iSuppNoCD					,
					iSkuCD						,
					rowSupp.LOT_TX				,
					rowSupp.SRCKANBAN_CD		,
					numAmount					,
					rowSupp.PALLETCAPACITY_NR	,
					rowSupp.PALLET_NR			,
					rowSupp.CASE_NR				,
					rowSupp.BARA_NR				,
					rowSupp.PCS_NR				,
					rowSupp.SRCLOCA_CD			,
					rowSupp.DESTLOCA_CD			,
					iUserCD						,
					rowUser.USER_TX				,
					iProgramCD
				);
				--補充情報更新
				--完了になったときのみ引当済数を更新する
				PW_SUPP.UpdDestSupp(
					iSuppNoCD					,
					iSkuCD						,
					rowSupp.LOT_TX				,
					iKanbanCD					,
					numAmount					,
					rowSupp.PALLETCAPACITY_NR	,
					numPalletNR					,
					numCaseNR					,
					numBaraNR					,
					rowSupp.PCS_NR				,
					rowSupp.SRCLOCA_CD			,
					iDestLocaCD					,
					iUserCD						,
					rowUser.USER_TX				,
					iProgramCD
				);
				-- 更新
				UPDATE
					TA_SUPP
				SET
					STSUPP_ID		= PS_DEFINE.ST_SUPP8_SUPPINOK_E,	--補充完了
					DESTKANBAN_CD	= iKanbanCD					,
					DESTPALLET_NR	= DESTPALLET_NR + numImpPalletNR	,
					DESTCASE_NR		= DESTCASE_NR + numImpCaseNR		,
					DESTBARA_NR		= DESTBARA_NR + numImpBaraNR		,
					DESTPCS_NR		= DESTPCS_NR + numPcsNR		,
					--
					END_DT			= SYSDATE					,
					DESTEND_DT		= SYSDATE					,
					DESTHTUSER_CD	= iUserCD					,
					DESTHTUSER_TX	= rowUser.USER_TX			,
					--
					UPD_DT			= SYSDATE					,
					UPDUSER_CD		= iUserCD					,
					UPDUSER_TX		= rowUser.USER_TX			,
					UPDPROGRAM_CD	= iProgramCD				,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
				WHERE
					SUPP_ID		= rowSupp.SUPP_ID;
				numPcsNR :=0;

				UPDATE
					TA_PICK
				SET
					SRCLOCA_CD		= iDestLocaCD				,
					KANBAN_CD		= iKanbanCD					,
					--
					UPD_DT			= SYSDATE					,
					UPDUSER_CD		= iUserCD					,
					UPDUSER_TX		= rowUser.USER_TX			,
					UPDPROGRAM_CD	= iProgramCD				,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
				WHERE
					PICK_ID IN (
								SELECT
									PICK_ID
								FROM
									TA_SUPP
								WHERE
									SUPPNO_CD = iSuppNoCD AND
									SHIPD_ID <> 0
								);
				-- かんばん更新
				PW_SUPP.UpdKanban(
					iKanbanCD					,	-- new
					rowSupp.SRCKANBAN_CD		,	-- old
					rowSupp.DESTLOCA_CD			,
					rowSupp.SKU_CD				,
					rowSupp.LOT_TX				,
					rowSupp.CASEPERPALLET_NR	,
					rowSupp.PALLETLAYER_NR		,
					rowSupp.STACKINGNUMBER_NR	,
					rowSupp.TYPEPLALLET_CD		,
					iUserCD						,
					rowUser.USER_TX
					);
			ELSE 
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' PATTERN C numPcsNR='||numPcsNR);
			-- 残りのピース数(バラ単位)が入力されたピース数より多い場合(補充指示に対して入力数が少ない場合)
				UPDATE
					TA_SUPP
				SET
					STSUPP_ID		= PS_DEFINE.ST_SUPP0_NOTSUPP		,	-- 一部補充済みの場合は未補充に戻す
					DESTPALLET_NR	= DESTPALLET_NR + numImpPalletNR	,
					DESTCASE_NR		= DESTCASE_NR + numImpCaseNR		,
					DESTBARA_NR		= DESTBARA_NR + numImpBaraNR		,
					DESTPCS_NR		= DESTPCS_NR + numPcsNR				,
					--
					UPD_DT			= SYSDATE						,
					UPDUSER_CD		= iUserCD						,
					UPDUSER_TX		= rowUser.USER_TX				,
					UPDPROGRAM_CD	= iProgramCD					,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1				
				WHERE
					SUPP_ID		= rowSupp.SUPP_ID;
				numPcsNR := 0;
			END IF;
		END LOOP;
		
		IF numPcsNR <> 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '補充完了のピース数が不正です。残ピース数:'||numPcsNR);
		END IF;
		-- 受払作成
		-- 入庫
		PS_STOCK.InsStockInOut(	
			' '							,
			' '							,
			rowSupp.DESTLOCA_CD			,
			rowSupp.SKU_CD				,
			rowSupp.LOT_TX				,
			iKanbanCD					,
			rowSupp.CASEPERPALLET_NR	,
			rowSupp.PALLETLAYER_NR		,
			rowSupp.PALLETCAPACITY_NR	,
			rowSupp.STACKINGNUMBER_NR	,
			rowSupp.TYPEPLALLET_CD		,	-- iTypePalletCD
			rowSupp.USELIMIT_DT			,
			' '							,	-- BATCHNO_CD
			rowSupp.STOCK_DT			,
			rowSupp.AMOUNT_NR			,
			iPalletNR					,
			iCaseNR						,
			iBaraNR						,
			iPcsNR						,
			rowSupp.DESTTYPESTOCK_CD		,
			rowSupp.DESTTYPESTOCK_CD		,	-- todo iTypeLocaCD
			'450'						,	-- 補充入庫
			iSuppNoCD					,
			0			  				,
			'SUPP'						,
			0							,	-- iNeedSendFL
			iUserCD						,
			rowUser.USER_TX				,
			iProgramCD
		);
		CLOSE curSupp;
		-- 
		OPEN curCmpSupp(iSuppNoCD);
		FETCH curCmpSupp INTO rowCmpSupp;
		IF rowCmpSupp.PCS_NR = rowCmpSupp.DESTPCS_NR THEN
			UPDATE
				TA_SUPP
			SET
				END_DT			= SYSDATE			,
				DESTEND_DT		= SYSDATE			,
				SRCHTUSER_CD	= iUserCD			,
				SRCHTUSER_TX	= rowUser.USER_TX	
			WHERE
				SUPPNO_CD		= iSuppNoCD;
		END IF;
		CLOSE curCmpSupp;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			IF curSupp%ISOPEN THEN CLOSE curSupp; END IF;
			IF curCmpSupp%ISOPEN THEN CLOSE curCmpSupp; END IF;
			IF curKanban%ISOPEN THEN CLOSE curKanban; END IF;
			RAISE;
	END EndDestSupp;
	/************************************************************************************/
	/*	iPad紐づき解除																	*/
	/************************************************************************************/
	PROCEDURE RstiPad(
		iSuppNoCD		IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstiPad';
		-- ピック指示
		CURSOR curSupp(PalletDetNoCD	TA_PICK.PALLETDETNO_CD%TYPE) IS
			SELECT
				SUPPNO_CD					,
				MIN(STSUPP_ID) AS STSUPP_ID	,
				MAX(END_DT) AS END_DT
			FROM
				TA_SUPP
			WHERE
				SUPPNO_CD = PalletDetNoCD
			GROUP BY
				SUPPNO_CD;
		rowSupp			curSupp%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
		
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		-- ピック指示チェック
		OPEN curSupp(iSuppNoCD);
		FETCH curSupp INTO rowSupp;
		IF curSupp%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iSuppNoCD || ']');
		END IF;
		CLOSE curSupp;
		-- キャンセルチェック
		IF rowSupp.STSUPP_ID = PS_DEFINE.ST_SUPPM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iSuppNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowSupp.STSUPP_ID = PS_DEFINE.ST_SUPP8_SUPPINOK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。' ||
																'指示書番号 = [' || iSuppNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowSupp.END_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。' ||
																'指示書番号 = [' || iSuppNoCD || ']');
		END IF;
		-- 紐づき解除
		UPDATE
			TA_SUPP
		SET
			STSUPP_ID		= PS_DEFINE.ST_SUPP0_NOTSUPP,
			START_DT		= NULL				,
			--
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			SUPPNO_CD		= iSuppNoCD;
		COMMIT;
		
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curSupp%ISOPEN THEN CLOSE curSupp; END IF;
			RAISE;
	END RstiPad;
END PW_SUPP;
/
SHOW ERRORS
