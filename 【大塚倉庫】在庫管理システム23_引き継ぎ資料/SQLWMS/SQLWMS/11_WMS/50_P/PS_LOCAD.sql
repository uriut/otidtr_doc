-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_LOCA AS
	/************************************************************************************/
	/*																					*/
	/*									Private											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************************************/
	/*																					*/
	/*									Public											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************/
	/*	ロケーションロック登録									*/
	/************************************************************/
	PROCEDURE InsLocaLock
	AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsLocaLock';
		CURSOR curLocaLock IS
			SELECT
				ML.LOCA_CD
			FROM
				MA_LOCA ML,
				TA_LOCALOCK LCK
			WHERE
				ML.LOCA_CD = LCK.LOCA_CD (+) AND
				LCK.LOCA_CD IS NULL;
		rowLocaLock curLocaLock%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START ');
		OPEN curLocaLock;
		LOOP
			FETCH curLocaLock INTO rowLocaLock;
			IF curLocaLock%NOTFOUND THEN
				EXIT;
			END IF;
			INSERT INTO TA_LOCALOCK (
				LOCA_CD
			) VALUES (
				rowLocaLock.LOCA_CD
			);
		END LOOP;
		CLOSE curLocaLock;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaLock%ISOPEN THEN
				CLOSE curLocaLock;
			END IF;
			RAISE;
	END InsLocaLock;
	/************************************************************/
	/*	ロケーションロック取得									*/
	/************************************************************/
	PROCEDURE GetLocaLock(
		iLocaCD 		IN	VARCHAR2
	) AS 
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLocaLock';
		CURSOR curLocaLock(LocaCd TA_LOCALOCK.LOCA_CD%TYPE) IS
			SELECT
				LOCA_CD
			FROM
				TA_LOCALOCK
			WHERE
				LOCA_CD = LocaCd
			FOR UPDATE;
		rowLocaLock curLocaLock%ROWTYPE;
		numDeadLockCount	NUMBER(2,0) := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START ' || iLocaCD);
		numDeadLockCount := 0;
		LOOP
			BEGIN
				OPEN curLocaLock(iLocaCD);
				FETCH curLocaLock INTO rowLocaLock;
				IF curLocaLock%NOTFOUND THEN
					RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：ロケーションロックテーブルのデータが見つかりません。');
				END IF;
				CLOSE curLocaLock;
				EXIT;
			EXCEPTION
				WHEN PS_DEFINE.DEADLOCK_DETECTED THEN
					numDeadLockCount := numDeadLockCount + 1;
					PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' DEADLOCK_DETECTED ' || iLocaCD);
					IF numDeadLockCount > 3 THEN
						RAISE;
					END IF;
				WHEN OTHERS THEN
					RAISE;
			END;
		END LOOP;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaLock%ISOPEN THEN
				CLOSE curLocaLock;
			END IF;
			RAISE;
	END GetLocaLock;
	/************************************************************/
	/*	引当済み在庫取得										*/
	/************************************************************/
	PROCEDURE GetLockLocaStockC(
		iLocaStockCID	IN	NUMBER				,
		orowLocaStockC	OUT	TA_LOCASTOCKC%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLockLocaStockC';
		
		CURSOR curLocaStockC(LocaStockCID TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKC
			WHERE
				LOCASTOCKC_ID = LocaStockCID
			FOR UPDATE;
		rowLocaStockC curLocaStockC%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curLocaStockC(iLocaStockCID);
		FETCH curLocaStockC INTO rowLocaStockC;
		IF curLocaStockC%FOUND THEN
			orowLocaStockC := rowLocaStockC;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：引当済数テーブルのデータが見つかりません。');
		END IF;
		CLOSE curLocaStockC;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockC%ISOPEN THEN
				CLOSE curLocaStockC;
			END IF;
			RAISE;
	END GetLockLocaStockC;
	/************************************************************/
	/*	引当可能在庫取得										*/
	/************************************************************/
	PROCEDURE GetLockLocaStockProwid(
		iRowID			IN	UROWID				,
		orowLocaStockP	OUT	TA_LOCASTOCKP%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLockLocaStockProwid';
		
		CURSOR curLocaStockP(iiRowID UROWID) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKP
			WHERE
				ROWID = iiRowID
			FOR UPDATE;
		rowLocaStockP curLocaStockP%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START1 ROWID = [' || iRowID || ']');
		OPEN curLocaStockP(iRowID);
		FETCH curLocaStockP INTO rowLocaStockP;
		IF curLocaStockP%FOUND THEN
			orowLocaStockP := rowLocaStockP;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'予期せぬエラー：引当可能在庫がありません。 ROWID = [' || iRowID || ']');
		END IF;
		CLOSE curLocaStockP;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN
				CLOSE curLocaStockP;
			END IF;
			RAISE;
	END GetLockLocaStockProwid;
	/************************************************************************************/
	/*	引当可能在庫取得																*/
	/************************************************************************************/
	PROCEDURE GetLockLocaStockP(
		iLocaCD			IN	VARCHAR2			,
		iSkuCD			IN	VARCHAR2			,
		orowLocaStockP	OUT	TA_LOCASTOCKP%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLockLocaStockP';
		
		CURSOR curLocaStockP(LocaCD		TA_LOCASTOCKP.LOCA_CD%TYPE,
								SkuCD	TA_LOCASTOCKP.SKU_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD = LocaCD	AND
				SKU_CD	= SkuCD
			FOR UPDATE;
		rowLocaStockP curLocaStockP%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START2');
		OPEN curLocaStockP(iLocaCD,iSkuCD);
		FETCH curLocaStockP INTO rowLocaStockP;
		IF curLocaStockP%FOUND THEN
			orowLocaStockP := rowLocaStockP;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'予期せぬエラー：引当可能在庫がありません。');
		END IF;
		CLOSE curLocaStockP;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN
				CLOSE curLocaStockP;
			END IF;
			RAISE;
	END GetLockLocaStockP;
	/************************************************************************************/
	/*	引当可能在庫取得																*/
	/************************************************************************************/
	PROCEDURE GetLockLocaStockP(
		iLocaCD 		IN VARCHAR2		,
		orowLocaStockP	OUT TA_LOCASTOCKP%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLockLocaStockP';
		
		CURSOR curLocaStockP(LocaCD		TA_LOCASTOCKP.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD = LocaCD
			FOR UPDATE;
		rowLocaStockP curLocaStockP%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START3');
		OPEN curLocaStockP(iLocaCD);
		FETCH curLocaStockP INTO rowLocaStockP;
		IF curLocaStockP%FOUND THEN
			orowLocaStockP := rowLocaStockP;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'予期せぬエラー：引当可能在庫がありません。');
		END IF;
		CLOSE curLocaStockP;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN
				CLOSE curLocaStockP;
			END IF;
			RAISE;
	END GetLockLocaStockP;
	/************************************************************/
	/*	引当可能在庫取得										*/
	/************************************************************/
	PROCEDURE GetLocaStockP(
		iLocaCD			IN	VARCHAR2			,
		orowLocaStockP	OUT TA_LOCASTOCKP%ROWTYPE
	) AS 
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLocaStockP';
		CURSOR curLocaStockP(LocaCD TA_LOCASTOCKP.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD = LocaCD
			FOR UPDATE;
		rowLocaStockP curLocaStockP%ROWTYPE;
		
		numCount	 	NUMBER := 0;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START3');
		OPEN curLocaStockP(iLocaCD);
		FETCH curLocaStockP INTO rowLocaStockP;
		IF curLocaStockP%FOUND THEN
			orowLocaStockP := rowLocaStockP;
		ELSE
			orowLocaStockP.LOCA_CD			:= iLocaCD;
			orowLocaStockP.SKU_CD			:= ' ';
			orowLocaStockP.USELIMIT_DT		:= PS_DEFINE.USELIMITDT_DEFAULT;
			orowLocaStockP.PCS_NR			:= 0;
		END IF;
		CLOSE curLocaStockP;
		--一意性チェック
		SELECT
			COUNT(*)
		INTO
			numCount
		FROM
			TA_LOCASTOCKP
		WHERE
			LOCA_CD = iLocaCD;
		IF numCount > 1 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'予期せぬエラー：在庫が一意に特定できません。');
		END IF;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN
				CLOSE curLocaStockP;
			END IF;
			RAISE;
	END GetLocaStockP;
	/************************************************************/
	/*	引当可能在庫取得										*/
	/************************************************************/
	PROCEDURE GetLocaStockP(
		iLocaCD			IN	VARCHAR2			,
		iSkuCD			IN	VARCHAR2			,
		orowLocaStockP	OUT TA_LOCASTOCKP%ROWTYPE
	) AS 
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLocaStockP';
		CURSOR curLocaStockP(
			LocaCD	TA_LOCASTOCKP.LOCA_CD%TYPE,
			SkuCD	TA_LOCASTOCKP.SKU_CD%TYPE
		) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD	= LocaCD	AND
				SKU_CD	= SkuCD
			FOR UPDATE;
		rowLocaStockP curLocaStockP%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START3');
		OPEN curLocaStockP(iLocaCD, iSkuCD);
		FETCH curLocaStockP INTO rowLocaStockP;
		IF curLocaStockP%FOUND THEN
			orowLocaStockP := rowLocaStockP;
		ELSE
			orowLocaStockP.LOCA_CD			:= iLocaCD;
			orowLocaStockP.SKU_CD			:= ' ';
			orowLocaStockP.USELIMIT_DT		:= PS_DEFINE.USELIMITDT_DEFAULT;
			orowLocaStockP.PCS_NR			:= 0;
		END IF;
		CLOSE curLocaStockP;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN
				CLOSE curLocaStockP;
			END IF;
			RAISE;
	END GetLocaStockP;
	/************************************************************/
	/*	引当済み在庫取得										*/
	/************************************************************/
	PROCEDURE GetLocaStockC(
		iLocaCD			IN	VARCHAR2			,
		orowLocaStockC	OUT	TA_LOCASTOCKC%ROWTYPE
	) AS 
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLocaStockC';
		CURSOR curLocaStockC(LocaCD TA_LOCASTOCKP.LOCA_CD%TYPE) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKC
			WHERE
				LOCA_CD = LocaCD;
		rowLocaStockC curLocaStockC%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curLocaStockC(iLocaCD);
		FETCH curLocaStockC INTO rowLocaStockC;
		IF curLocaStockC%FOUND THEN
			orowLocaStockC := rowLocaStockC;
		ELSE
			orowLocaStockC.LOCASTOCKC_ID	:= 0;
			orowLocaStockC.TRN_ID			:= 0;
			orowLocaStockC.STSTOCK_ID		:= -1;
			orowLocaStockC.LOCA_CD 			:= iLocaCD;
			orowLocaStockC.SKU_CD 			:= ' ';
			orowLocaStockC.USELIMIT_DT 		:= PS_DEFINE.USELIMITDT_DEFAULT;
			orowLocaStockC.PCS_NR 			:= 0;
		END IF;
		CLOSE curLocaStockC;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockC%ISOPEN THEN
				CLOSE curLocaStockC;
			END IF;
			RAISE;
	END GetLocaStockC;
	/************************************************************/
	/*	引当済み在庫取得										*/
	/************************************************************/
	PROCEDURE GetLocaStockC(
		iLocaCD			IN	VARCHAR2			,
		iSkuCD			IN	VARCHAR2			,
		orowLocaStockC	OUT	TA_LOCASTOCKC%ROWTYPE
	) AS 
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLocaStockC';
		CURSOR curLocaStockC(
			LocaCD	TA_LOCASTOCKP.LOCA_CD%TYPE,
			SkuCD	TA_LOCASTOCKP.SKU_CD%TYPE
		) IS
			SELECT
				*
			FROM
				TA_LOCASTOCKC
			WHERE
				LOCA_CD	= LocaCD	AND
				SKU_CD	= SkuCD		;
		rowLocaStockC curLocaStockC%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curLocaStockC(iLocaCD, iSkuCD);
		FETCH curLocaStockC INTO rowLocaStockC;
		IF curLocaStockC%FOUND THEN
			orowLocaStockC := rowLocaStockC;
		ELSE
			orowLocaStockC.LOCASTOCKC_ID	:= 0;
			orowLocaStockC.TRN_ID			:= 0;
			orowLocaStockC.STSTOCK_ID		:= -1;
			orowLocaStockC.LOCA_CD 			:= iLocaCD;
			orowLocaStockC.SKU_CD 			:= ' ';
			orowLocaStockC.USELIMIT_DT 		:= PS_DEFINE.USELIMITDT_DEFAULT;
			orowLocaStockC.PCS_NR 			:= 0;
		END IF;
		CLOSE curLocaStockC;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockC%ISOPEN THEN
				CLOSE curLocaStockC;
			END IF;
			RAISE;
	END GetLocaStockC;
	/************************************************************/
	/*	在庫取得												*/
	/************************************************************/
	PROCEDURE GetLocaStock(
		iLocaCD			IN	VARCHAR2			,
		orowLocaStockP	OUT	TA_LOCASTOCKP%ROWTYPE
	) AS 
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLocaStock';
		CURSOR curLocaStock(LocaCD TA_LOCASTOCKP.LOCA_CD%TYPE) IS
			SELECT
				LOCA_CD          			,
				SKU_CD           			,
				USELIMIT_DT      			,
				SUM(PCS_NR) 	PCS_NR     	
			FROM
				(SELECT
					LP.LOCA_CD		,
					LP.SKU_CD		,
					LP.USELIMIT_DT	,
					LP.PCS_NR   	
				FROM
					TA_LOCASTOCKP LP
				UNION ALL
				SELECT
					LC.LOCA_CD						,
					LC.SKU_CD						,
					LC.USELIMIT_DT					,
					SUM(LC.PCS_NR) 	PCS_NR  		
				FROM
					TA_LOCASTOCKC LC
				GROUP BY
					LC.LOCA_CD			,
					LC.SKU_CD			,
					LC.USELIMIT_DT		
				) LSK
			WHERE
				LOCA_CD	= LocaCD
			GROUP BY
				LOCA_CD          			,
				SKU_CD           			,
				USELIMIT_DT      			;
		rowLocaStock curLocaStock%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		DELETE
			TA_LOCASTOCKP
		WHERE
			PCS_NR = 0 AND
			LOCA_CD = iLocaCD;
		OPEN curLocaStock(iLocaCD);
		FETCH curLocaStock INTO rowLocaStock;
		IF curLocaStock%FOUND THEN
			orowLocaStockP.LOCA_CD 			:= rowLocaStock.LOCA_CD;
			orowLocaStockP.SKU_CD 			:= rowLocaStock.SKU_CD;
			orowLocaStockP.USELIMIT_DT 		:= rowLocaStock.USELIMIT_DT;
			orowLocaStockP.PCS_NR 			:= rowLocaStock.PCS_NR;
		ELSE
			orowLocaStockP.LOCA_CD 			:= iLocaCD;
			orowLocaStockP.SKU_CD 			:= ' ';
			orowLocaStockP.USELIMIT_DT 		:= PS_DEFINE.USELIMITDT_DEFAULT;
			orowLocaStockP.PCS_NR 			:= 0;
		END IF;
		CLOSE curLocaStock;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStock%ISOPEN THEN
				CLOSE curLocaStock;
			END IF;
			RAISE;
	END GetLocaStock;
	/************************************************************/
	/*	在庫取得(ロケーション全在庫)							*/
	/************************************************************/
	PROCEDURE GetLocaStockAll (
		iLocaCD			IN	VARCHAR2			,
		orowLocaStockP	OUT	TA_LOCASTOCKP%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLocaStockAll';
		--
		CURSOR curLocaStockP(LocaCD		TA_LOCASTOCKP.LOCA_CD%TYPE) IS
			SELECT
				LP.LOCA_CD			,
				LP.SKU_CD			,
				LP.USELIMIT_DT		,
				LP.PCS_NR			
			FROM
				TA_LOCASTOCKP LP
			WHERE
				LP.LOCA_CD = LocaCD;
		rowLocaStockP curLocaStockP%ROWTYPE;
		--
		CURSOR curLocaStockC(LocaCD		TA_LOCASTOCKC.LOCA_CD%TYPE) IS
			SELECT
				LC.LOCASTOCKC_ID	,
				LC.LOCA_CD			,
				LC.SKU_CD			,
				LC.USELIMIT_DT		,
				LC.PCS_NR			
			FROM
				TA_LOCASTOCKC LC
			WHERE
				LC.LOCA_CD = LocaCD;
		rowLocaStockC curLocaStockC%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		orowLocaStockP.LOCA_CD 			:= iLocaCD;
		orowLocaStockP.SKU_CD 			:= ' ';
		orowLocaStockP.USELIMIT_DT 		:= PS_DEFINE.USELIMITDT_DEFAULT;
		orowLocaStockP.PCS_NR 			:= 0;
		--引当可能在庫数
		OPEN curLocaStockP(iLocaCD);
		LOOP
			FETCH curLocaStockP INTO rowLocaStockP;
			EXIT WHEN curLocaStockP%NOTFOUND;
			orowLocaStockP.SKU_CD 			:= rowLocaStockP.SKU_CD;
			orowLocaStockP.USELIMIT_DT 		:= rowLocaStockP.USELIMIT_DT;
			orowLocaStockP.PCS_NR 			:= orowLocaStockP.PCS_NR + rowLocaStockP.PCS_NR;
		END LOOP;
		CLOSE curLocaStockP;
		--引当済み在庫数
		OPEN curLocaStockC(iLocaCD);
		LOOP
			FETCH curLocaStockC INTO rowLocaStockC;
			EXIT WHEN curLocaStockC%NOTFOUND;
			orowLocaStockP.SKU_CD 			:= rowLocaStockC.SKU_CD;
			orowLocaStockP.USELIMIT_DT 		:= rowLocaStockC.USELIMIT_DT;
			orowLocaStockP.PCS_NR 			:= orowLocaStockP.PCS_NR + rowLocaStockC.PCS_NR;
		END LOOP;
		CLOSE curLocaStockC;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN
				CLOSE curLocaStockP;
			END IF;
			IF curLocaStockC%ISOPEN THEN
				CLOSE curLocaStockC;
			END IF;
			RAISE;
	END GetLocaStockAll;
	/************************************************************/
	/*	在庫取得（ＳＫＵ指定）									*/
	/************************************************************/
	PROCEDURE GetLocaStock(
		iLocaCD 		IN	VARCHAR2			,
		iSkuCD			IN	VARCHAR2			,
		orowLocaStockP	OUT	TA_LOCASTOCKP%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLocaStock';
		CURSOR curLocaStock(LocaCD		TA_LOCASTOCKP.LOCA_CD%TYPE,
							SkuCD		TA_LOCASTOCKP.SKU_CD%TYPE) IS
			SELECT
				LOCA_CD						,
				SKU_CD						,
				USELIMIT_DT					,
				SUM(PCS_NR) 	PCS_NR		
			FROM
				(SELECT
					LP.LOCA_CD		,
					LP.SKU_CD		,
					LP.USELIMIT_DT	,
					LP.PCS_NR   	
				FROM
					TA_LOCASTOCKP LP
				UNION ALL
				SELECT
					LC.LOCA_CD						,
					LC.SKU_CD						,
					LC.USELIMIT_DT					,
					SUM(LC.PCS_NR) 	PCS_NR  		
				FROM
					TA_LOCASTOCKC LC
				GROUP BY
					LC.LOCA_CD			,
					LC.SKU_CD			,
					LC.USELIMIT_DT		
				) LSK
			WHERE
				LOCA_CD	= LocaCD	AND
				SKU_CD	= SkuCD
			GROUP BY
				LOCA_CD          			,
				SKU_CD           			,
				USELIMIT_DT      			;
		rowLocaStock curLocaStock%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curLocaStock(iLocaCD,iSkuCD);
		FETCH curLocaStock INTO rowLocaStock;
		IF curLocaStock%FOUND THEN
			orowLocaStockP.LOCA_CD 			:= rowLocaStock.LOCA_CD;
			orowLocaStockP.SKU_CD 			:= rowLocaStock.SKU_CD;
			orowLocaStockP.USELIMIT_DT 		:= rowLocaStock.USELIMIT_DT;
			orowLocaStockP.PCS_NR 			:= rowLocaStock.PCS_NR;
		ELSE
			orowLocaStockP.LOCA_CD 			:= iLocaCD;
			orowLocaStockP.SKU_CD 			:= iSkuCD;
			orowLocaStockP.USELIMIT_DT 		:= PS_DEFINE.USELIMITDT_DEFAULT;
			orowLocaStockP.PCS_NR 			:= 0;
		END IF;
		CLOSE curLocaStock;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStock%ISOPEN THEN
				CLOSE curLocaStock;
			END IF;
			RAISE;
	END GetLocaStock;
	/************************************************************/
	/*	在庫比率取得											*/
	/************************************************************/
	PROCEDURE GetLocaStockRate(
		iSkuCD			IN	VARCHAR2			,
		iLotTX			IN	VARCHAR2			,
		oNestanerRateNR	OUT MA_SKU.NESTAINERINSTOCKRATE_NR%TYPE	,
		oCaseRateNR		OUT MA_SKU.CASESTOCKRATE_NR%TYPE		,
		oPalletRateNR	OUT MA_SKU.NESTAINERINSTOCKRATE_NR%TYPE	,
		oPcsNR			OUT NUMBER				
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLocaStockRate';
		-- 総在庫
		CURSOR curLocaStock(
			SkuCD	TA_LOCASTOCKP.SKU_CD%TYPE,
			LotTX	TA_LOCASTOCKP.LOT_TX%TYPE
		) IS
			SELECT
				NVL(SUM(PCS_NR),0) 	PCS_NR
			FROM
				(SELECT
					LP.LOCA_CD		,
					LP.SKU_CD		,
					LP.LOT_TX		,
					LP.PCS_NR   	
				FROM
					TA_LOCASTOCKP LP
				UNION ALL
				SELECT
					LC.LOCA_CD				,
					LC.SKU_CD				,
					LC.LOT_TX				,
					SUM(LC.PCS_NR) 	PCS_NR  
				FROM
					TA_LOCASTOCKC LC
				WHERE
					STSTOCK_ID = PS_DEFINE.ST_STOCK1_INAPPLY
				GROUP BY
					LC.LOCA_CD			,
					LC.SKU_CD			,
					LC.LOT_TX
				) LSK,
				MA_LOCA ML
			WHERE
				LSK.LOCA_CD			= ML.LOCA_CD	AND
				LSK.SKU_CD			= SkuCD			AND
				LSK.LOT_TX			= LotTX
				;
		rowLocaStock curLocaStock%ROWTYPE;
		-- ネス在庫
		CURSOR curLocaStockNestaner(
			SkuCD	TA_LOCASTOCKP.SKU_CD%TYPE,
			LotTX	TA_LOCASTOCKP.LOT_TX%TYPE
		) IS
			SELECT
				NVL(SUM(PCS_NR),0) 	PCS_NR
			FROM
				(SELECT
					LP.LOCA_CD		,
					LP.SKU_CD		,
					LP.LOT_TX		,
					LP.PCS_NR   	
				FROM
					TA_LOCASTOCKP LP
				UNION ALL
				SELECT
					LC.LOCA_CD				,
					LC.SKU_CD				,
					LC.LOT_TX				,
					SUM(LC.PCS_NR) 	PCS_NR  
				FROM
					TA_LOCASTOCKC LC
				WHERE
					STSTOCK_ID = PS_DEFINE.ST_STOCK1_INAPPLY
				GROUP BY
					LC.LOCA_CD			,
					LC.SKU_CD			,
					LC.LOT_TX			
				) LSK,
				MA_LOCA ML
			WHERE
				LSK.LOCA_CD			= ML.LOCA_CD	AND
				LSK.SKU_CD			= SkuCD			AND
				LSK.LOT_TX			= LotTX			AND
				ML.SHIPVOLUME_CD	= PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER;
		rowLocaStockNestaner curLocaStockNestaner%ROWTYPE;
		-- ケースエリア在庫
		CURSOR curLocaStockCase(
			SkuCD	TA_LOCASTOCKP.SKU_CD%TYPE,
			LotTX	TA_LOCASTOCKP.LOT_TX%TYPE
		) IS
			SELECT
				NVL(SUM(PCS_NR),0) 	PCS_NR
			FROM
				(SELECT
					LP.LOCA_CD		,
					LP.SKU_CD		,
					LP.LOT_TX		,
					LP.PCS_NR   	
				FROM
					TA_LOCASTOCKP LP
				UNION ALL
				SELECT
					LC.LOCA_CD				,
					LC.SKU_CD				,
					LC.LOT_TX				,
					SUM(LC.PCS_NR) 	PCS_NR  
				FROM
					TA_LOCASTOCKC LC
				WHERE
					STSTOCK_ID = PS_DEFINE.ST_STOCK1_INAPPLY
				GROUP BY
					LC.LOCA_CD			,
					LC.SKU_CD			,
					LC.LOT_TX
				) LSK,
				MA_LOCA ML
			WHERE
				LSK.LOCA_CD			= ML.LOCA_CD	AND
				LSK.SKU_CD			= SkuCD			AND
				LSK.LOT_TX			= LotTX			AND
				ML.SHIPVOLUME_CD	= PS_DEFINE.TYPE_SHIPVOLUME1_CASE;
		rowLocaStockCase curLocaStockCase%ROWTYPE;
		-- パレットエリア在庫
		CURSOR curLocaStockPallet(
			SkuCD	TA_LOCASTOCKP.SKU_CD%TYPE,
			LotTX	TA_LOCASTOCKP.LOT_TX%TYPE
		) IS
			SELECT
				NVL(SUM(PCS_NR),0) 	PCS_NR
			FROM
				(SELECT
					LP.LOCA_CD		,
					LP.SKU_CD		,
					LP.LOT_TX		,
					LP.PCS_NR   	
				FROM
					TA_LOCASTOCKP LP
				UNION ALL
				SELECT
					LC.LOCA_CD				,
					LC.SKU_CD				,
					LC.LOT_TX				,
					SUM(LC.PCS_NR) 	PCS_NR  
				FROM
					TA_LOCASTOCKC LC
				WHERE
					STSTOCK_ID = PS_DEFINE.ST_STOCK1_INAPPLY
				GROUP BY
					LC.LOCA_CD			,
					LC.SKU_CD			,
					LC.LOT_TX			
				) LSK,
				MA_LOCA ML
			WHERE
				LSK.LOCA_CD			= ML.LOCA_CD	AND
				LSK.SKU_CD			= SkuCD			AND
				LSK.LOT_TX			= LotTX			AND
				ML.SHIPVOLUME_CD	= PS_DEFINE.TYPE_SHIPVOLUME2_PALLET;
		rowLocaStockPallet curLocaStockPallet%ROWTYPE;
		numTotalPcsNR		NUMBER;
		numPalletPcsNR		NUMBER;
		numCasePcsNR		NUMBER;
		numNestanerPcsNR	NUMBER;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 初期化
		numTotalPcsNR := 0;
		numPalletPcsNR := 0;
		numCasePcsNR := 0;
		numNestanerPcsNR := 0;
		-- 総在庫
		OPEN curLocaStock(iSkuCD,iLotTX);
		FETCH curLocaStock INTO rowLocaStock;
			numTotalPcsNR := rowLocaStock.PCS_NR;
			oPcsNR := rowLocaStock.PCS_NR;
		CLOSE curLocaStock;
		-- ネスエリア
		OPEN curLocaStockNestaner(iSkuCD,iLotTX);
		FETCH curLocaStockNestaner INTO rowLocaStockNestaner;
			numNestanerPcsNR := rowLocaStockNestaner.PCS_NR;
			-- 比率
			IF numTotalPcsNR <> 0 THEN
				oNestanerRateNR := TRUNC((numNestanerPcsNR / numTotalPcsNR),2);
			ELSE
				oNestanerRateNR := 0;
			END IF;
		CLOSE curLocaStockNestaner;
		-- ケースエリア
		OPEN curLocaStockCase(iSkuCD,iLotTX);
		FETCH curLocaStockCase INTO rowLocaStockCase;
			numCasePcsNR := rowLocaStockCase.PCS_NR;
			-- 比率
			IF numTotalPcsNR <> 0 THEN
				oCaseRateNR := TRUNC((numCasePcsNR / numTotalPcsNR),2);
			ELSE
				oCaseRateNR := 0;
			END IF;
		CLOSE curLocaStockCase;
		-- パレットエリア
		OPEN curLocaStockPallet(iSkuCD,iLotTX);
		FETCH curLocaStockPallet INTO rowLocaStockPallet;
			numPalletPcsNR := rowLocaStockPallet.PCS_NR;
			-- 比率
			IF numTotalPcsNR <> 0 THEN
				oPalletRateNR := TRUNC((numPalletPcsNR / numTotalPcsNR),2);
			ELSE
				oPalletRateNR := 0;
			END IF;
		CLOSE curLocaStockPallet;
		
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStock%ISOPEN THEN CLOSE curLocaStock; END IF;
			IF curLocaStockNestaner%ISOPEN THEN CLOSE curLocaStockNestaner; END IF;
			IF curLocaStockCase%ISOPEN THEN CLOSE curLocaStockCase; END IF;
			IF curLocaStockPallet%ISOPEN THEN CLOSE curLocaStockPallet; END IF;
			RAISE;
	END GetLocaStockRate;
END PS_LOCA;
/
SHOW ERRORS
