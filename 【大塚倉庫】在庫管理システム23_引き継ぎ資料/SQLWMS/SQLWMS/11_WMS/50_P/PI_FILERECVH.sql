-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PI_FILERECV AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PI_FILERECV';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************/
	/*	トランザクションID新規取得								*/
	/************************************************************/
	FUNCTION GetNewTrnId RETURN NUMBER;
	/************************************************************/
	/* 一時テーブル初期化（TMP_HEAD1,HEAD2,BODY）               */
	/************************************************************/
	PROCEDURE InitTmpHead;
	/************************************************************/
	/* 一時テーブル初期化（TMP_SKU）                            */
	/************************************************************/
	PROCEDURE InitTmpSku;
	/************************************************************/
	/* 一時テーブル初期化（TMP_STOCKRESULT）                    */
	/************************************************************/
	PROCEDURE InitTmpStockResult;
	/************************************************************/
	/* 一時テーブル初期化（TMP_SUPPLIER）                       */
	/************************************************************/
	PROCEDURE InitTmpSupplier;
	/************************************************************/
	/* 一時テーブル初期化（TMP_TRANSPORTER）                    */
	/************************************************************/
	PROCEDURE InitTmpTransPorter;
	/************************************************************/
	/* 一時テーブル初期化（TMP_ARRIVWEB）                       */
	/************************************************************/
	PROCEDURE InitTmpArrivWeb;
	/************************************************************/
	/* アップロード（HEAD1）                                    */
	/************************************************************/
	PROCEDURE UploadHead1(
		iTRN_ID					IN TMP_HEAD1.TRN_ID%TYPE		,	-- トランザクションID
		iHBKBN					IN TMP_HEAD1.HBKBN%TYPE			,	-- HB区分　「H1」固定
		iSPSUCD					IN TMP_HEAD1.SPSUCD%TYPE		,	-- SP業者CD
		iSPSEQ					IN TMP_HEAD1.SPSEQ%TYPE			,	-- SPシーケンスNO1
		iDKBN					IN TMP_HEAD1.DKBN%TYPE			,	-- データ区分　HS:入荷、SH:出荷、
		iENTKBN					IN TMP_HEAD1.ENTKBN%TYPE		,	-- エントリー区分
		iSHIFT					IN TMP_HEAD1.SHIFT%TYPE			,	-- シフト区分
		iHNNINUSHI				IN TMP_HEAD1.HNNINUSHI%TYPE		,	-- 販売荷主
		iNYMD					IN TMP_HEAD1.NYMD%TYPE			,	-- 荷主会計日付
		iSYMD					IN TMP_HEAD1.SYMD%TYPE			,	-- 倉庫会計日付
		iTYUNO					IN TMP_HEAD1.TYUNO%TYPE			,	-- 納品日
		iDENNO					IN TMP_HEAD1.DENNO%TYPE			,	-- 伝票番号
		iWKBN					IN TMP_HEAD1.WKBN%TYPE			,	-- 伝票作成区分
		iGYO					IN TMP_HEAD1.GYO%TYPE			,	-- 明細行
		iIYMD					IN TMP_HEAD1.IYMD%TYPE			,	-- 伝票印字日付
		iNID					IN TMP_HEAD1.NID%TYPE			,	-- 荷主取引区分
		iNID_NM					IN TMP_HEAD1.NID_NM%TYPE		,	-- 荷主取引名称
		iSKID					IN TMP_HEAD1.SKID%TYPE			,	-- 倉庫取引区分
		iSKID_NM				IN TMP_HEAD1.SKID_NM%TYPE		,	-- 倉庫取引名称
		iATSUKAI_NM				IN TMP_HEAD1.ATSUKAI_NM%TYPE	,	-- 荷扱会社
		iSKECD					IN TMP_HEAD1.SKECD%TYPE			,	-- 倉庫営業所コード
		iSKECD_NM				IN TMP_HEAD1.SKECD_NM%TYPE		,	-- 倉庫営業所名称
		iSKECD_KCD				IN TMP_HEAD1.SKECD_KCD%TYPE		,	-- 倉庫営業所地区コード
		iSKECD_KEN1				IN TMP_HEAD1.SKECD_KEN1%TYPE	,	-- 倉庫営業所県コード
		iSKECD_KEN2				IN TMP_HEAD1.SKECD_KEN2%TYPE	,	-- 倉庫営業所離島区分
		iSKECD_JYUSYO			IN TMP_HEAD1.SKECD_JYUSYO%TYPE	,	-- 倉庫営業所住所
		iSKECD_TEL				IN TMP_HEAD1.SKECD_TEL%TYPE		,	-- 倉庫営業所TEL
		iSYUKACD1				IN TMP_HEAD1.SYUKACD1%TYPE		,	-- 出荷場所コード
		iSYUKACD2				IN TMP_HEAD1.SYUKACD2%TYPE		,	-- 出荷場所コード2
		iSUCD1					IN TMP_HEAD1.SUCD1%TYPE			,	-- 業者コード1
		iSUCD1_NM				IN TMP_HEAD1.SUCD1_NM%TYPE		,	-- 業者名1
		iSUCD2					IN TMP_HEAD1.SUCD2%TYPE			,	-- 業者コード2
		iSUCD2_NM				IN TMP_HEAD1.SUCD2_NM%TYPE		,	-- 業者コード2
		iKYOHAICD				IN TMP_HEAD1.KYOHAICD%TYPE		,	-- 共配CD
		iPKEN_FLG				IN TMP_HEAD1.PKEN_FLG%TYPE		,	-- Pケンフラグ
		iPKENCD					IN TMP_HEAD1.PKENCD%TYPE		,	-- Pケンコード
		iPKENCD2				IN TMP_HEAD1.PKENCD2%TYPE		,	-- Pケンコード2
		iHATNO1					IN TMP_HEAD1.HATNO1%TYPE		,	-- 発注NO1
		iHATNO2					IN TMP_HEAD1.HATNO2%TYPE		,	-- 発注NO2
		iBIKO1					IN TMP_HEAD1.BIKO1%TYPE			,	-- 備考1
		iBIKO2					IN TMP_HEAD1.BIKO2%TYPE			,	-- 備考2
		iBIKO3					IN TMP_HEAD1.BIKO3%TYPE			,	-- 備考3
		iBIKO_NM1				IN TMP_HEAD1.BIKO_NM1%TYPE		,	-- 備考1全角
		iBIKO_NM2				IN TMP_HEAD1.BIKO_NM2%TYPE		,	-- 備考2全角
		iSYOHEAD				IN TMP_HEAD1.SYOHEAD%TYPE		,	-- 商品コード識別
		iTIME					IN TMP_HEAD1.TIME%TYPE			,	-- 着荷時間
		iSBIKO					IN TMP_HEAD1.SBIKO%TYPE			,	-- 倉庫備考
		iSBIKO_NM				IN TMP_HEAD1.SBIKO_NM%TYPE		,	-- 倉庫備考全角
		iSYABAN					IN TMP_HEAD1.SYABAN%TYPE		,	-- 車番
		iKIZAI1					IN TMP_HEAD1.KIZAI1%TYPE		,	-- 機材1
		iKIZAI2					IN TMP_HEAD1.KIZAI2%TYPE		,	-- 機材2
		iDEN_G_SRY				IN TMP_HEAD1.DEN_G_SRY%TYPE		,	-- 伝票合計数量梱数
		iG_SRY					IN TMP_HEAD1.G_SRY%TYPE			,	-- 納品合計梱数
		iDEN_G_WGT				IN TMP_HEAD1.DEN_G_WGT%TYPE		,	-- 伝票合計重量
		iG_WGT					IN TMP_HEAD1.G_WGT%TYPE			,	-- 納品合計重量
		iJYMD					IN TMP_HEAD1.JYMD%TYPE			,	-- 実着日
		iSNINUSHI				IN TMP_HEAD1.SNINUSHI%TYPE		,	-- 生産荷主
		iHEAD1_FIL				IN TMP_HEAD1.HEAD1_FIL%TYPE		,	-- 余白
		iWYMD					IN TMP_HEAD1.WYMD%TYPE			,	-- データ作成日
		iWTIME					IN TMP_HEAD1.WTIME%TYPE			 	-- データ作成時間
	);
	/************************************************************/
	/* アップロード（HEAD2）                                    */
	/************************************************************/
	PROCEDURE UploadHead2(
		iTRN_ID					IN TMP_HEAD2.TRN_ID%TYPE		,	-- トランザクションID
		iHBKBN					IN TMP_HEAD2.HBKBN%TYPE			,	-- HB区分　「H2」固定
		iSPSUCD					IN TMP_HEAD2.SPSUCD%TYPE		,	-- SP業者CD
		iSPSEQ					IN TMP_HEAD2.SPSEQ%TYPE			,	-- SPシーケンスNO1
		iDKBN					IN TMP_HEAD2.DKBN%TYPE			,	-- データ区分　HS:入荷、SH:出荷、
		iENTKBN					IN TMP_HEAD2.ENTKBN%TYPE		,	-- エントリー区分
		iSHIFT					IN TMP_HEAD2.SHIFT%TYPE			,	-- シフト区分
		iHNNINUSHI				IN TMP_HEAD2.HNNINUSHI%TYPE		,	-- 販売荷主
		iNYMD					IN TMP_HEAD2.NYMD%TYPE			,	-- 荷主会計日付
		iSYMD					IN TMP_HEAD2.SYMD%TYPE			,	-- 倉庫会計日付
		iTYUNO					IN TMP_HEAD2.TYUNO%TYPE			,	-- 納品日
		iDENNO					IN TMP_HEAD2.DENNO%TYPE			,	-- 伝票番号
		iMAKERCD				IN TMP_HEAD2.MAKERCD%TYPE		,	-- メーカーコード
		iMAKER_NM				IN TMP_HEAD2.MAKER_NM%TYPE		,	-- メーカー名称
		iCOMCD					IN TMP_HEAD2.COMCD%TYPE			,	-- 会社コード
		iCOM_NM					IN TMP_HEAD2.COM_NM%TYPE		,	-- 会社名称
		iSITENCD				IN TMP_HEAD2.SITENCD%TYPE		,	-- 支店コード
		iSITEN_NM				IN TMP_HEAD2.SITEN_NM%TYPE		,	-- 支店名称
		iSYUTYOCD				IN TMP_HEAD2.SYUTYOCD%TYPE		,	-- 出張所コード
		iSYUTYO_NM				IN TMP_HEAD2.SYUTYO_NM%TYPE		,	-- 出張所名称
		iSYUTYO_KA				IN TMP_HEAD2.SYUTYO_KA%TYPE		,	-- 出張所課
		iSYUTYO_FIL				IN TMP_HEAD2.SYUTYO_FIL%TYPE	,	-- 出張所余白
		iSITEN_JYUSYO			IN TMP_HEAD2.SITEN_JYUSYO%TYPE	,	-- 支店住所
		iSITEN_TEL				IN TMP_HEAD2.SITEN_TEL%TYPE		,	-- 支店TEL
		iUTIKAE_FLG				IN TMP_HEAD2.UTIKAE_FLG%TYPE	,	-- 打ち変えフラグ
		iCCD					IN TMP_HEAD2.CCD%TYPE			,	-- 直送先CD
		iNOHCD					IN TMP_HEAD2.NOHCD%TYPE			,	-- 納品先CD
		iNOHCD_NM1				IN TMP_HEAD2.NOHCD_NM1%TYPE		,	-- 納品先名称1
		iNOHCD_NM2				IN TMP_HEAD2.NOHCD_NM2%TYPE		,	-- 納品先名称2
		iNOHCD_NM3				IN TMP_HEAD2.NOHCD_NM3%TYPE		,	-- 納品先名称3
		iKCD					IN TMP_HEAD2.KCD%TYPE			,	-- 地区コード
		iKEN1					IN TMP_HEAD2.KEN1%TYPE			,	-- 県コード
		iKEN2					IN TMP_HEAD2.KEN2%TYPE			,	-- 離島区分
		iYUBIN					IN TMP_HEAD2.YUBIN%TYPE			,	-- 郵便番号
		iNOHCD_JYUSYO1			IN TMP_HEAD2.NOHCD_JYUSYO1%TYPE	,	-- 納品先住所1
		iNOHCD_JYUSYO2			IN TMP_HEAD2.NOHCD_JYUSYO2%TYPE	,	-- 納品先住所2
		iNOHCD_JYUSYO3			IN TMP_HEAD2.NOHCD_JYUSYO3%TYPE	,	-- 納品先住所3
		iTEL					IN TMP_HEAD2.TEL%TYPE			,	-- 納品先TEL
		iDAICD					IN TMP_HEAD2.DAICD%TYPE			,	-- 代理店コード
		iDAICD_NM1				IN TMP_HEAD2.DAICD_NM1%TYPE		,	-- 代理店名称1
		iDAICD_NM2				IN TMP_HEAD2.DAICD_NM2%TYPE		,	-- 代理店名称2
		iDAICD_NM3				IN TMP_HEAD2.DAICD_NM3%TYPE		,	-- 代理店名称3
		iNIJICD					IN TMP_HEAD2.NIJICD%TYPE		,	-- 二次店コード
		iNIJICD_NM1				IN TMP_HEAD2.NIJICD_NM1%TYPE	,	-- 二次店名称1
		iNIJICD_NM2				IN TMP_HEAD2.NIJICD_NM2%TYPE	,	-- 二次店名称2
		iNIJICD_NM3				IN TMP_HEAD2.NIJICD_NM3%TYPE	,	-- 二次店名称3
		iSANCD					IN TMP_HEAD2.SANCD%TYPE			,	-- 三次店コード
		iSANCD_NM1				IN TMP_HEAD2.SANCD_NM1%TYPE		,	-- 三次店名称1
		iSANCD_NM2				IN TMP_HEAD2.SANCD_NM2%TYPE		,	-- 三次店名称2
		iSANCD_NM3				IN TMP_HEAD2.SANCD_NM3%TYPE		,	-- 三次店名称3
		iCVS_FLG				IN TMP_HEAD2.CVS_FLG%TYPE		,	-- コンビニフラグ　CV:コンビニ卸
		iYUSEN1					IN TMP_HEAD2.YUSEN1%TYPE		,	-- 優先フラグ1
		iYUSEN2					IN TMP_HEAD2.YUSEN2%TYPE		,	-- 優先フラグ2
		iHEAD2_FIL				IN TMP_HEAD2.HEAD2_FIL%TYPE      	-- 余白
	);
	/************************************************************/
	/* アップロード（BODY）                                     */
	/************************************************************/
	PROCEDURE UploadBody(
		iTRN_ID					IN TMP_BODY.TRN_ID%TYPE		,	-- トランザクションID
		iHBKBN					IN TMP_BODY.HBKBN%TYPE		,	-- HB区分
		iSPSUCD					IN TMP_BODY.SPSUCD%TYPE		,	-- SP業者CD
		iSPSEQ					IN TMP_BODY.SPSEQ%TYPE		,	-- SPシーケンスNO1
		iDKBN					IN TMP_BODY.DKBN%TYPE		,	-- データ区分　HS:入荷、SH:出荷、
		iENTKBN					IN TMP_BODY.ENTKBN%TYPE		,	-- エントリー区分
		iSHIFT					IN TMP_BODY.SHIFT%TYPE		,	-- シフト区分
		iHNNINUSHI				IN TMP_BODY.HNNINUSHI%TYPE	,	-- 販売荷主
		iNYMD					IN TMP_BODY.NYMD%TYPE		,	-- 荷主会計日付
		iSYMD					IN TMP_BODY.SYMD%TYPE		,	-- 倉庫会計日付
		iTYUNO					IN TMP_BODY.TYUNO%TYPE		,	-- 納品日
		iDENNO					IN TMP_BODY.DENNO%TYPE		,	-- 伝票番号
		iGYO					IN TMP_BODY.GYO%TYPE		,	-- 明細行
		iL_RETU					IN TMP_BODY.L_RETU%TYPE		,	-- 明細行列
		iBCD					IN TMP_BODY.BCD%TYPE		,	-- 物流コード
		iNSYOCD					IN TMP_BODY.NSYOCD%TYPE		,	-- 荷主商品コード
		iDRK					IN TMP_BODY.DRK%TYPE		,	-- 電略
		iSEINAK					IN TMP_BODY.SEINAK%TYPE		,	-- 商品名（ｶﾅ）
		iSEINAN					IN TMP_BODY.SEINAN%TYPE		,	-- 商品名
		iKIKAKUK				IN TMP_BODY.KIKAKUK%TYPE	,	-- 規格（ｶﾅ）
		iKIKAKUN				IN TMP_BODY.KIKAKUN%TYPE	,	-- 規格
		iKHKBN					IN TMP_BODY.KHKBN%TYPE		,	-- 梱/端数区分
		iKHKBN_NM				IN TMP_BODY.KHKBN_NM%TYPE	,	-- 単位名称
		iMGKBN					IN TMP_BODY.MGKBN%TYPE		,	-- 未合格区分
		iMITATSU				IN TMP_BODY.MITATSU%TYPE	,	-- 未達区分
		iSRY					IN TMP_BODY.SRY%TYPE		,	-- 数量
		iSRYK					IN TMP_BODY.SRYK%TYPE		,	-- 梱数量
		iSRYH					IN TMP_BODY.SRYH%TYPE		,	-- 端数
		iJSRY					IN TMP_BODY.JSRY%TYPE		,	-- 受注数量
		iHTYUNO1				IN TMP_BODY.HTYUNO1%TYPE	,	-- 発注NO1
		iHTYUNO2				IN TMP_BODY.HTYUNO2%TYPE	,	-- 発注NO2
		iSYUKA1					IN TMP_BODY.SYUKA1%TYPE		,	-- 出荷場所1
		iSYUKA2					IN TMP_BODY.SYUKA2%TYPE		,	-- 出荷場所2
		iSKCD1					IN TMP_BODY.SKCD1%TYPE		,	-- 倉庫コード1
		iSKCD_NM1				IN TMP_BODY.SKCD_NM1%TYPE	,	-- 倉庫名称1
		iSKCD2					IN TMP_BODY.SKCD2%TYPE		,	-- 倉庫コード2
		iSKCD_NM2				IN TMP_BODY.SKCD_NM2%TYPE	,	-- 倉庫名称2
		iLOTSRY					IN TMP_BODY.LOTSRY%TYPE		,	-- ロット数量
		iLOT_FLG				IN TMP_BODY.LOT_FLG%TYPE	,	-- ロット表示フラグ　1:ロット表示あり
		iLOT					IN TMP_BODY.LOT%TYPE		,	-- ロット
		iLOTSUB					IN TMP_BODY.LOTSUB%TYPE		,	-- サブロット
		iLOTL					IN TMP_BODY.LOTL%TYPE		,	-- ラインロット
		iLOTC					IN TMP_BODY.LOTC%TYPE		,	-- キャンペーンフラグ
		iLOTP					IN TMP_BODY.LOTP%TYPE		,	-- 生産プラント
		iFCKBN					IN TMP_BODY.FCKBN%TYPE		,	-- 工場記号
		iFCKBNN					IN TMP_BODY.FCKBNN%TYPE		,	-- 工場略号
		iLOTK					IN TMP_BODY.LOTK%TYPE		,	-- 管理区分
		iLOTS					IN TMP_BODY.LOTS%TYPE		,	-- 出荷区分
		iKAHIKBN				IN TMP_BODY.KAHIKBN%TYPE	,	-- 可否区分
		iLOTDT					IN TMP_BODY.LOTDT%TYPE		,	-- ロット記帳義務
		iKIGEN					IN TMP_BODY.KIGEN%TYPE		,	-- 賞味期限
		iZSRYK					IN TMP_BODY.ZSRYK%TYPE		,	-- 残庫梱数量
		iZSRYH					IN TMP_BODY.ZSRYH%TYPE		,	-- 残庫端数
		iKAN1					IN TMP_BODY.KAN1%TYPE		,	-- 入目１(ケースの中のバラ数)
		iKAN2					IN TMP_BODY.KAN2%TYPE		,	-- 入目２(バラ)
		iKAN3					IN TMP_BODY.KAN3%TYPE		,	-- 入目３(中間の中のバラ数)
		iKAN4					IN TMP_BODY.KAN4%TYPE		,	-- 実入目
		iSKBN					IN TMP_BODY.SKBN%TYPE		,	-- 請求区分
		iHOREI					IN TMP_BODY.HOREI%TYPE		,	-- 保冷区分
		iBUNRUI1				IN TMP_BODY.BUNRUI1%TYPE	,	-- 商品分類1
		iBUNRUI2				IN TMP_BODY.BUNRUI2%TYPE	,	-- 商品分類2
		iKATA					IN TMP_BODY.KATA%TYPE		,	-- 型コード
		iTATE					IN TMP_BODY.TATE%TYPE		,	-- 縦
		iYOKO					IN TMP_BODY.YOKO%TYPE		,	-- 横
		iTAKA					IN TMP_BODY.TAKA%TYPE		,	-- 高さ
		iWGT					IN TMP_BODY.WGT%TYPE		,	-- 梱重量(ケース重量)g
		iMWGT					IN TMP_BODY.MWGT%TYPE		,	-- 明細重量(ロット単位の明細重量)kg
		iYOSEKI					IN TMP_BODY.YOSEKI%TYPE		,	-- 容積
		iUNWGT					IN TMP_BODY.UNWGT%TYPE		,	-- 運賃重量(ケースあたり重量)g
		iJANCD					IN TMP_BODY.JANCD%TYPE		,	-- JANコード
		iSSYOCD					IN TMP_BODY.SSYOCD%TYPE		,	-- 卸商品コード
		iSYOCD1					IN TMP_BODY.SYOCD1%TYPE		,	-- 商品コード1
		iGSYOCD					IN TMP_BODY.GSYOCD%TYPE		,	-- 外装商品コード
		iPLSRY					IN TMP_BODY.PLSRY%TYPE		,	-- ＰＬ積数
		iLFLOOR					IN TMP_BODY.LFLOOR%TYPE		,	-- フロア
		iLZONE					IN TMP_BODY.LZONE%TYPE		,	-- ゾーン
		iLAREA					IN TMP_BODY.LAREA%TYPE		,	-- エリア
		iLRETSU					IN TMP_BODY.LRETSU%TYPE		,	-- 列
		iLGYO					IN TMP_BODY.LGYO%TYPE		,	-- 行
		iLDAN					IN TMP_BODY.LDAN%TYPE		,	-- 段
		iSEIKBN1				IN TMP_BODY.SEIKBN1%TYPE	,	-- 製造区分1
		iSEIKBN2				IN TMP_BODY.SEIKBN2%TYPE	,	-- 製造区分2
		iSEIKBN3				IN TMP_BODY.SEIKBN3%TYPE	,	-- 製造区分3
		iSEIFLG1				IN TMP_BODY.SEIFLG1%TYPE	,	-- 製造フラグ1
		iSEIFLG2				IN TMP_BODY.SEIFLG2%TYPE	,	-- 製造フラグ2
		iSEIFLG3				IN TMP_BODY.SEIFLG3%TYPE	,	-- 製造フラグ3
		iMBIKO					IN TMP_BODY.MBIKO%TYPE		,	-- 明細備考
		iMBIKO_NM				IN TMP_BODY.MBIKO_NM%TYPE	,	-- 明細備考
		iFLG1					IN TMP_BODY.FLG1%TYPE		,	-- 予備フラグ1
		iFLG2					IN TMP_BODY.FLG2%TYPE		,	-- 予備フラグ2
		iFLG3					IN TMP_BODY.FLG3%TYPE        	-- 予備フラグ3
	);
	/************************************************************/
	/* アップロード（SKU）                                      */
	/************************************************************/
	PROCEDURE UploadSku(
		iTRN_ID					IN TMP_SKU.TRN_ID%TYPE		,	-- トランザクションID
		iNINUSHI				IN TMP_SKU.NINUSHI%TYPE		,	-- 荷主コード
		iSYOCD					IN TMP_SKU.SYOCD%TYPE		,	-- 商品コード
		iFCKBN					IN TMP_SKU.FCKBN%TYPE		,	-- 工場区分
		iBCD					IN TMP_SKU.BCD%TYPE			,	-- 物流コード
		iPL_T					IN TMP_SKU.PL_T%TYPE		,	-- パレット積数
		iPL_M					IN TMP_SKU.PL_M%TYPE		,	-- パレット回し数
		iPL_D					IN TMP_SKU.PL_D%TYPE		,	-- パレット段数
		iB_PL_T					IN TMP_SKU.B_PL_T%TYPE		,	-- パレット積数
		iB_PL_M					IN TMP_SKU.B_PL_M%TYPE		,	-- パレット回し数
		iB_PL_D					IN TMP_SKU.B_PL_D%TYPE		,	-- パレット段数
		iPL_H					IN TMP_SKU.PL_H%TYPE		,	-- 保管段数
		iPL_G					IN TMP_SKU.PL_G%TYPE		,	-- パレット面グループ
		iNISUGATA				IN TMP_SKU.NISUGATA%TYPE	,	-- 荷姿
		iYOHAKU					IN TMP_SKU.YOHAKU%TYPE		,	-- 余白
		iNINUSHI2				IN TMP_SKU.NINUSHI2%TYPE	,	-- 荷主コード2　未使用
		iSYOCD2					IN TMP_SKU.SYOCD2%TYPE		,	-- 商品コード2　未使用
		iGSYOCD					IN TMP_SKU.GSYOCD%TYPE		,	-- 外装コード
		iSEINAN					IN TMP_SKU.SEINAN%TYPE		,	-- 商品名称
		iSEINAK					IN TMP_SKU.SEINAK%TYPE		,	-- 商品名称ｶﾅ
		iKIKAKUK				IN TMP_SKU.KIKAKUK%TYPE		,	-- 規格容量・ケース
		iKIKAKUB				IN TMP_SKU.KIKAKUB%TYPE		,	-- 規格容量・ボール
		iKIKAKUH				IN TMP_SKU.KIKAKUH%TYPE		,	-- 規格容量・バラ
		iKIKAKUKK				IN TMP_SKU.KIKAKUKK%TYPE	,	-- 規格容量ｶﾅ・ケース
		iKIKAKUBK				IN TMP_SKU.KIKAKUBK%TYPE	,	-- 規格容量ｶﾅ・ボール
		iKIKAKUHK				IN TMP_SKU.KIKAKUHK%TYPE	,	-- 規格容量ｶﾅ・バラ
		iKAN1					IN TMP_SKU.KAN1%TYPE		,	-- 入目・ケース
		iKAN2					IN TMP_SKU.KAN2%TYPE		,	-- 入目・ボール
		iKAN3					IN TMP_SKU.KAN3%TYPE		,	-- 入目・バラ
		iTATEK					IN TMP_SKU.TATEK%TYPE		,	-- 縦・ケース
		iYOKOK					IN TMP_SKU.YOKOK%TYPE		,	-- 横・ケース
		iTAKAK					IN TMP_SKU.TAKAK%TYPE		,	-- 高さ・ケース
		iTATEB					IN TMP_SKU.TATEB%TYPE		,	-- 縦・ボール
		iYOKOB					IN TMP_SKU.YOKOB%TYPE		,	-- 横・ボール
		iTAKAB					IN TMP_SKU.TAKAB%TYPE		,	-- 高さ・ボール
		iTATEH					IN TMP_SKU.TATEH%TYPE		,	-- 縦・バラ
		iYOKOH					IN TMP_SKU.YOKOH%TYPE		,	-- 横・バラ
		iTAKAH					IN TMP_SKU.TAKAH%TYPE		,	-- 高さ・バラ
		iWGT					IN TMP_SKU.WGT%TYPE			,	-- 重量・ケース
		iWGTB					IN TMP_SKU.WGTB%TYPE		,	-- 重量・ボール
		iWGTH					IN TMP_SKU.WGTH%TYPE		,	-- 重量・バラ
		iJANCD_K				IN TMP_SKU.JANCD_K%TYPE		,	-- ＪＡＮコード　ケース
		iJANCD_B				IN TMP_SKU.JANCD_B%TYPE		,	-- ＪＡＮコード　ボール
		iJANCD_H				IN TMP_SKU.JANCD_H%TYPE		,	-- ＪＡＮコード　バラ
		iITFCD					IN TMP_SKU.ITFCD%TYPE		,	-- ＩＴＦコード
		iGS1CD					IN TMP_SKU.GS1CD%TYPE		,	-- ＧＳ１コード
		iYOBI1CD				IN TMP_SKU.YOBI1CD%TYPE		,	-- 予備コード１
		iYOBI2CD				IN TMP_SKU.YOBI2CD%TYPE		,	-- 予備コード２
		iBCD2					IN TMP_SKU.BCD2%TYPE		,	-- 物流コード２　未使用
		iNSYOCD					IN TMP_SKU.NSYOCD%TYPE		,	-- 荷主商品コード
		iDRK					IN TMP_SKU.DRK%TYPE			,	-- 電略
		iSKBN					IN TMP_SKU.SKBN%TYPE		,	-- 請求区分
		iWGT_G					IN TMP_SKU.WGT_G%TYPE		,	-- 重量グループ
		iYOSEKI_RITSU			IN TMP_SKU.YOSEKI_RITSU%TYPE,	-- 容積比率
		iYUKO					IN TMP_SKU.YUKO%TYPE		,	-- 有効期限
		iHOK1					IN TMP_SKU.HOK1%TYPE		,	-- 保管料甲
		iHOK2					IN TMP_SKU.HOK2%TYPE		,	-- 保管料乙
		iHOK3					IN TMP_SKU.HOK3%TYPE		,	-- 保管料丙
		iNYK1					IN TMP_SKU.NYK1%TYPE		,	-- 荷役料甲
		iNYK2					IN TMP_SKU.NYK2%TYPE		,	-- 荷役料乙
		iNYK3					IN TMP_SKU.NYK3%TYPE		,	-- 荷役料丙
		iKITAKU					IN TMP_SKU.KITAKU%TYPE		,	-- 寄託金額
		iTON					IN TMP_SKU.TON%TYPE			,	-- 容積トン
		iSAISU					IN TMP_SKU.SAISU%TYPE		,	-- 才数
		iFCKBN2					IN TMP_SKU.FCKBN2%TYPE		,	-- 工場区分
		iHOREI					IN TMP_SKU.HOREI%TYPE		,	-- 保冷区分
		iKIKEN					IN TMP_SKU.KIKEN%TYPE		,	-- 危険区分
		iYOBI_KBN1				IN TMP_SKU.YOBI_KBN1%TYPE	,	-- 予備区分１
		iYOBI_KBN2				IN TMP_SKU.YOBI_KBN2%TYPE	,	-- 予備区分２
		iYOBI_KBN3				IN TMP_SKU.YOBI_KBN3%TYPE	,	-- 予備区分３
		iKATA					IN TMP_SKU.KATA%TYPE		,	-- 型コード
		iKATA2					IN TMP_SKU.KATA2%TYPE		,	-- 型コード２
		iYOHAKU2				IN TMP_SKU.YOHAKU2%TYPE		,	-- 余白
		iWYMD					IN TMP_SKU.WYMD%TYPE		,	-- 登録日
		iRYMD					IN TMP_SKU.RYMD%TYPE		,	-- 修正日
		iDYMD					IN TMP_SKU.DYMD%TYPE		,	-- 削除日
		iMAN 					IN TMP_SKU.MAN%TYPE			 	-- 登録者
	);
	/************************************************************/
	/* アップロード（在庫実績）                                 */
	/************************************************************/
	PROCEDURE UploadStockResult(
		iTRN_ID					IN TMP_STOCKRESULT.TRN_ID%TYPE		,	-- トランザクションID
		iMITA					IN TMP_STOCKRESULT.MITA%TYPE		,	-- 未達区分　M:補給中（予定、現物在庫なし）
		iNINUSHI				IN TMP_STOCKRESULT.NINUSHI%TYPE		,	-- 荷主コード　KK1：大塚製薬、FD1：大塚食品
		iSYOCD					IN TMP_STOCKRESULT.SYOCD%TYPE		,	-- 商品コード
		iSKECD					IN TMP_STOCKRESULT.SKECD%TYPE		,	-- 事業所コード　04:東京支店
		iMGKBN					IN TMP_STOCKRESULT.MGKBN%TYPE		,	-- 未合区分　0:合格、1:未合格
		iLOT					IN TMP_STOCKRESULT.LOT%TYPE			,	-- ロット
		iLOTW					IN TMP_STOCKRESULT.LOTW%TYPE		,	-- 詰合せ区分　"1"固定
		iLOTK					IN TMP_STOCKRESULT.LOTK%TYPE		,	-- 管理区分
		iLOTS					IN TMP_STOCKRESULT.LOTS%TYPE		,	-- 出荷止め区分　△：良品
		iFCKBN					IN TMP_STOCKRESULT.FCKBN%TYPE		,	-- 工場区分
		iSKCD					IN TMP_STOCKRESULT.SKCD%TYPE		,	-- 倉庫コード
		iBCD					IN TMP_STOCKRESULT.BCD%TYPE			,	-- 倉庫商品コード
		iDRK					IN TMP_STOCKRESULT.DRK%TYPE			,	-- 電略　商品略称
		iKAN1					IN TMP_STOCKRESULT.KAN1%TYPE		,	-- 入目　ケース入目
		iNYMD					IN TMP_STOCKRESULT.NYMD%TYPE		,	-- 入庫日　未使用
		iZZANK					IN TMP_STOCKRESULT.ZZANK%TYPE		,	-- 前日在庫ケース
		iZZANH					IN TMP_STOCKRESULT.ZZANH%TYPE		,	-- 前日在庫バラ
		iINK					IN TMP_STOCKRESULT.INK%TYPE			,	-- 入庫ケース当日入庫数ケース
		iINH					IN TMP_STOCKRESULT.INH%TYPE			,	-- 入庫バラ　当日入庫数バラ
		iOUTK					IN TMP_STOCKRESULT.OUTK%TYPE		,	-- 出庫ケース　当日出庫数ケース
		iOUTH					IN TMP_STOCKRESULT.OUTH%TYPE		,	-- 出庫バラ　当日出庫数バラ
		iTZANK					IN TMP_STOCKRESULT.TZANK%TYPE		,	-- 当日在庫ケース　前日在庫ケース＋入庫ケース−出庫ケース
		iTZANH					IN TMP_STOCKRESULT.TZANH%TYPE		,	-- 当日在庫　バラ前日在庫バラ＋入庫バラ−出庫バラ
		iOUTY1K					IN TMP_STOCKRESULT.OUTY1K%TYPE		,	-- 翌日出庫ケース
		iOUTY1H					IN TMP_STOCKRESULT.OUTY1H%TYPE		,	-- 翌日出庫バラ
		iOUTY2K					IN TMP_STOCKRESULT.OUTY2K%TYPE		,	-- 翌々日出庫ケース
		iOUTY2H					IN TMP_STOCKRESULT.OUTY2H%TYPE		,	-- 翌々日出庫バラ
		iKZANK					IN TMP_STOCKRESULT.KZANK%TYPE		,	-- 残庫ケース　引当可能数ケース
		iKZANH					IN TMP_STOCKRESULT.KZANH%TYPE		,	-- 残庫バラ　引当可能数バラ
		iKCD_NM					IN TMP_STOCKRESULT.KCD_NM%TYPE		,	-- 倉庫名
		iSYORIYMD				IN TMP_STOCKRESULT.SYORIYMD%TYPE	,	-- ホスト処理日
		iSYORITMS				IN TMP_STOCKRESULT.SYORITMS%TYPE	,	-- ホスト処理時間
		iYOHAKU					IN TMP_STOCKRESULT.YOHAKU%TYPE			-- 余白
	);
	/************************************************************/
	/* アップロード（仕入先）                                   */
	/************************************************************/
	PROCEDURE UploadSupplier(
		iTRN_ID					IN TMP_SUPPLIER.TRN_ID%TYPE		,	-- トランザクションID
		iSKCHKCD				IN TMP_SUPPLIER.SKCHKCD%TYPE	,	-- 仕入先コード
		iNAME					IN TMP_SUPPLIER.NAME%TYPE		,	-- 仕入先名称
		iJYUSYO					IN TMP_SUPPLIER.JYUSYO%TYPE		,	-- 仕入先住所
		iYUBIN					IN TMP_SUPPLIER.YUBIN%TYPE		,	-- 郵便番号
		iKCD					IN TMP_SUPPLIER.KCD%TYPE		,	-- 地区コード
		iCCDK					IN TMP_SUPPLIER.CCDK%TYPE		,	-- 管理事業所
		iKBN_FLG				IN TMP_SUPPLIER.KBN_FLG%TYPE	,	-- 管理フラグ
		iSKECD					IN TMP_SUPPLIER.SKECD%TYPE		,	-- 管理フラグが"1"の時の事業所コード
		iSKCD					IN TMP_SUPPLIER.SKCD%TYPE		,	-- 管理フラグが"1"の時の倉庫コード
		iSAGYO_FLG				IN TMP_SUPPLIER.SAGYO_FLG%TYPE	,	-- 作業フラグ
		iYOBIFLG1				IN TMP_SUPPLIER.YOBIFLG1%TYPE	,	-- 予備フラグ2
		iYOBIFLG2				IN TMP_SUPPLIER.YOBIFLG2%TYPE	,	-- 予備フラグ2
		iYOBIFLG3				IN TMP_SUPPLIER.YOBIFLG3%TYPE	,	-- 予備フラグ3
		iYOBIFLG4				IN TMP_SUPPLIER.YOBIFLG4%TYPE	,	-- 予備フラグ4
		iYOBIFLG5				IN TMP_SUPPLIER.YOBIFLG5%TYPE	,	-- 予備フラグ5
		iYOHAKU					IN TMP_SUPPLIER.YOHAKU%TYPE			-- 余白
	);
	/************************************************************/
	/* アップロード（配送業者）                                 */
	/************************************************************/
	PROCEDURE UploadTransPorter(
		iTRN_ID					IN TMP_TRANSPORTER.TRN_ID%TYPE		,	-- トランザクションID
		iSUCD					IN TMP_TRANSPORTER.SUCD%TYPE		,	-- 業者コード
		iSUCD_NM				IN TMP_TRANSPORTER.SUCD_NM%TYPE		,	-- 業者名
		iSUCD_S					IN TMP_TRANSPORTER.SUCD_S%TYPE		,	-- 集約業者コード
		iSUCD_S_NM				IN TMP_TRANSPORTER.SUCD_S_NM%TYPE	,	-- 集約業者名
		iKIZAICD				IN TMP_TRANSPORTER.KIZAICD%TYPE		,	-- 機材コード
		iSORT					IN TMP_TRANSPORTER.SORT%TYPE		,	-- １〜999　定義されていない業者は999
		iSTIME					IN TMP_TRANSPORTER.STIME%TYPE		,	-- 接車時間
		iYOBI_FLG1				IN TMP_TRANSPORTER.YOBI_FLG1%TYPE	,	-- 予備フラグ１
		iYOBI_FLG2				IN TMP_TRANSPORTER.YOBI_FLG2%TYPE	,	-- 予備フラグ２
		iYOBI_FLG3				IN TMP_TRANSPORTER.YOBI_FLG3%TYPE	,	-- 予備フラグ３
		iYOHAKU					IN TMP_TRANSPORTER.YOHAKU%TYPE
	);
	/************************************************************/
	/* アップロード（入荷工程前Web）                            */
	/************************************************************/
	PROCEDURE UploadArrivWeb(
		iTRN_ID					IN TMP_ARRIVWEB.TRN_ID%TYPE				,	-- トランザクションID
		iARRIV_ID				IN TMP_ARRIVWEB.ARRIV_ID%TYPE			,	-- 入荷予定ID
		iARRIVORDERNO_CD		IN TMP_ARRIVWEB.ARRIVORDERNO_CD%TYPE	,	-- 入荷予定番号（発注番号） 返品時はオーダー番号
		iARRIVORDERDETNO_CD		IN TMP_ARRIVWEB.ARRIVORDERDETNO_CD%TYPE	,	-- 入荷予定明細番号（行No） 返品時はオーダー明細番号
		iTYPEARRIV_CD			IN TMP_ARRIVWEB.TYPEARRIV_CD%TYPE		,	-- 入荷予定タイプコード 10:通常  20:即出荷  30:返品
		iSTARRIV_ID				IN TMP_ARRIVWEB.STARRIV_ID%TYPE			,	-- 入荷予定状態ID
		iSKU_CD					IN TMP_ARRIVWEB.SKU_CD%TYPE				,	-- SKUコード
		iLOT_TX					IN TMP_ARRIVWEB.LOT_TX%TYPE				,	-- ロット
		iARRIVPLAN_DT			IN TMP_ARRIVWEB.ARRIVPLAN_DT%TYPE		,	-- 入荷予定日
		iARRIVPLANCASE_NR		IN TMP_ARRIVWEB.ARRIVPLANCASE_NR%TYPE	,	-- 入荷予定ケース数
		iARRIVPLANPCS_NR		IN TMP_ARRIVWEB.ARRIVPLANPCS_NR%TYPE	,	-- 入荷予定数
		iCARNUMBER_CD			IN TMP_ARRIVWEB.CARNUMBER_CD%TYPE		,	-- 車番
		iSLIPNO_TX				IN TMP_ARRIVWEB.SLIPNO_TX%TYPE			,	-- 入荷伝票番号
		iARRIVPLAN2_DT			IN TMP_ARRIVWEB.ARRIVPLAN2_DT%TYPE		,	-- 入荷予定時刻
		iDRIVERTEL_TX			IN TMP_ARRIVWEB.DRIVERTEL_TX%TYPE		,	-- ドライバー電話番号
		iDRIVER_TX				IN TMP_ARRIVWEB.DRIVER_TX%TYPE			,	-- ドライバー名
		iBERTH_TX				IN TMP_ARRIVWEB.BERTH_TX%TYPE			,	-- 入荷指定バース
		iSAMEDAYSHIP_FL			IN TMP_ARRIVWEB.SAMEDAYSHIP_FL%TYPE			-- 即出荷フラグ
	);
END PI_FILERECV;
/
SHOW ERRORS
