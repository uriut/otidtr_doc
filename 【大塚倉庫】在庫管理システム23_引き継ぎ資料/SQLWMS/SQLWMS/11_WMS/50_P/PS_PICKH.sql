-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_PICK IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_PICK';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************************************/
	/*	ピック指示書追加																*/
	/************************************************************************************/
	PROCEDURE InsPick(
		iTrnID				IN	NUMBER	,
		iPickNoCD			IN	VARCHAR2,	-- 
		iPalletDetNoCD		IN	VARCHAR2,	-- パレット明細番号
		iTypePickCD			IN	VARCHAR2,
		irowSLocaStockC		IN	TA_LOCASTOCKC%ROWTYPE,
		irowShipH			IN	TA_SHIPH%ROWTYPE,
		irowShipD			IN	TA_SHIPD%ROWTYPE,
		iTotalPicFL			IN	NUMBER	, 	--1:トータルピック
		iTypeCarryCD		IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);
	/************************************************************************************/
	/*	ピック指示書追加(パレット分割)															*/
	/************************************************************************************/
	PROCEDURE DevPickPallet(
		iTrnID				IN	NUMBER	,
		iPickID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);
	/************************************************************************************/
	/*	ピック指示書追加(ケース分割)															*/
	/************************************************************************************/
	PROCEDURE DevPickCase(
		iTrnID				IN	NUMBER	,
		iPickID				IN	NUMBER	, --元ピックデータ
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2,
		oPickID				OUT	NUMBER	  --新ピックデータ
	);
	/************************************************************************************/
	/*	ピッキングデータ有効															*/
	/************************************************************************************/
	PROCEDURE PickDataOK(
		iTrnID				IN	NUMBER
	);
	/************************************************************************************/
	/*	作成中ピッキングデータ削除														*/
	/************************************************************************************/
	PROCEDURE DelPick(
		iTrnID				IN	NUMBER
	);
	/************************************************************************************/
	/*	作成中ピッキングデータ削除（指示書指定）										*/
	/************************************************************************************/
	PROCEDURE DelPick(
		iTrnID				IN	NUMBER	,
		iPickNoCD			IN	VARCHAR2
	);
	/************************************************************************************/
	/*	作成中ピッキングデータ削除（明細指定）											*/
	/************************************************************************************/
	PROCEDURE DelPick(
		iTrnID				IN	NUMBER,
		iShipHID			IN	NUMBER,
		iShipDID			IN	NUMBER
	);
	/************************************************************************************/
	/*	ピック指示書完了																*/
	/************************************************************************************/
	PROCEDURE EndPick(
		iPickNoCD			IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);
	/************************************************************************************/
	/*	ピッキングデータ・補充元消し込み												*/
	/************************************************************************************/
	PROCEDURE UpdPickSrcSupp(
		iPickID				IN	NUMBER	,
		iOldLocaStockCID	IN	NUMBER	,
		iNewLocaStockCID	IN	NUMBER	,
		iLocaCD				IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2
	);
	/************************************************************************************/
	/*	ピッキングデータ・補充先消し込み												*/
	/************************************************************************************/
	PROCEDURE UpdPickDestSupp(
		iPickID				IN	NUMBER	,
		iOldLocaStockCID	IN	NUMBER	,
		iNewLocaStockCID	IN	NUMBER	,
		iLocaCD				IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2
	);
	/************************************************************************************/
	/*	ピッキングデータ・補充消し込み													*/
	/************************************************************************************/
	PROCEDURE UpdPickEndSupp(
		iSuppNoCD			IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2
	);
END PS_PICK;
/
SHOW ERRORS
