-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_INSTOCK AS
	/************************************************************************************/
	/*	入庫指示書追加																	*/
	/************************************************************************************/
	PROCEDURE InsInStock(
		iTrnID			IN NUMBER	,
		iInStockNoCD	IN VARCHAR2	,
		iTypeInStockCD	IN VARCHAR2	,
		iTypeApplyCD	IN VARCHAR2	,
		irowLocaStockC	IN TA_LOCASTOCKC%ROWTYPE,
		iArrivID		IN NUMBER	,
		iSkuCD			IN VARCHAR2	,
		iLotTX			IN VARCHAR2	,
		iKanbanCD		IN VARCHAR2	,
		iShipVolumeCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2	,
		oInStockID		OUT NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsInStock';
		numInStockID	TA_INSTOCK.INSTOCK_ID%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_INSTOCKID.NEXTVAL INTO numInStockID
		FROM
			DUAL;
		INSERT INTO TA_INSTOCK (
			INSTOCK_ID					,
			TRN_ID						,
			INSTOCKNO_CD				,
			TYPEINSTOCK_CD				,
			TYPESTOCK_CD				,
			START_DT					,
			END_DT						,
			STINSTOCK_ID				,
			-- 入荷予定
			ARRIV_ID					,
			-- 計上
			APPLYINSTOCKNPALLET_NR		,
			APPLYINSTOCKNCASE_NR		,
			APPLYINSTOCKNBARA_NR		,
			APPLYINSTOCKNPCS_NR			,
			APPLYINSTOCK_DT				,
			APPLYINSTOCKUSER_CD			,
			APPLYINSTOCKUSER_TX			,
			SKU_CD						,
			LOT_TX						,
			KANBAN_CD					,
			CASEPERPALLET_NR			,
			PALLETLAYER_NR				,
			PALLETCAPACITY_NR			,
			STACKINGNUMBER_NR			,
			TYPEPLALLET_CD				,
			STOCK_DT					,
			AMOUNT_NR					,
			PALLET_NR					,
			CASE_NR						,
			BARA_NR						,
			PCS_NR						,
			-- 先ロケ
			DESTTYPESTOCK_CD			,
			DESTLOCA_CD					,
			RESULTLOCA_CD				,
			SHIPVOLUME_CD				,
			DESTSTART_DT				,
			DESTEND_DT					,
			DESTHT_ID					,
			DESTHTUSER_CD				,
			DESTHTUSER_TX				,
			DESTLOCASTOCKC_ID			,
			-- 印刷
			PRTCOUNT_NR					,
			PRT_DT						,
			PRTUSER_CD					,
			PRTUSER_TX					,
			BATCHNO_CD					,
			--
			TYPEAPPLY_CD				,
			TYPEBLOCK_CD				,
			TYPECARRY_CD				,
			-- 入荷実績（発注システム向
			ARRIVNEEDSEND_FL			,
			ARRIVSENDEND_FL				,
			ARRIVSEND_DT				,
			--
			INSTOCKNEEDSEND_FL			,
			INSTOCKSENDEND_FL			,
			INSTOCKSEND_DT				,
			-- 管理項目
			UPD_DT						,
			UPDUSER_CD					,
			UPDUSER_TX					,
			ADD_DT						,
			ADDUSER_CD					,
			ADDUSER_TX					,
			UPDPROGRAM_CD				,
			UPDCOUNTER_NR				,
			STRECORD_ID
		) VALUES (
			numInStockID				,
			iTrnID						,
			iInStockNoCD				,
			iTypeInStockCD				,
			irowLocaStockC.TYPESTOCK_CD	,
			NULL						,
			NULL						,
			PS_DEFINE.ST_INSTOCKM1_CREATE,
			--入荷予定
			iArrivID					,
			-- 計上
			irowLocaStockC.PALLET_NR	,
			irowLocaStockC.CASE_NR		,
			irowLocaStockC.BARA_NR		,
			irowLocaStockC.PCS_NR		,
			SYSDATE						,
			iUserCD						,
			iUserTX						,
			iSkuCD						,
			iLotTX						,
			iKanbanCD					,
			irowLocaStockC.CASEPERPALLET_NR,
			irowLocaStockC.PALLETLAYER_NR,
			irowLocaStockC.PALLETCAPACITY_NR,
			irowLocaStockC.STACKINGNUMBER_NR,
			irowLocaStockC.TYPEPLALLET_CD,
			irowLocaStockC.STOCK_DT		,
			irowLocaStockC.AMOUNT_NR	,
			irowLocaStockC.PALLET_NR	,
			irowLocaStockC.CASE_NR		,
			irowLocaStockC.BARA_NR		,
			irowLocaStockC.PCS_NR		,
			--先ロケ
			irowLocaStockC.TYPESTOCK_CD	,
			irowLocaStockC.LOCA_CD		,
			' '							,	-- RESULTLOCA_CD
			iShipVolumeCD				,	-- SHIPVOLUME_CD
			NULL						,
			NULL						,
			0							,
			' '							,
			' '							,
			irowLocaStockC.LOCASTOCKC_ID,
			-- 印刷
			0							,
			NULL						,
			' '							,
			' '							,
			' '							,	-- BATCHNO_CD
			--
			iTypeApplyCD				,	-- TYPEAPPLY_CD
			' '							,	-- TYPEBLOCK_CD
			' '							,	-- TYPECARRY_CD
			-- 入荷実績（発注システム向)
			1							,	--TODO
			0							,
			NULL						,
			-- 
			1							,	--TODO
			0							,
			NULL						,
			-- 管理項目
			SYSDATE						,
			iUserCD						,
			iUserTX						,
			SYSDATE						,
			iUserCD						,
			iUserTX						,
			iProgramCD					,
			0							,
			PS_DEFINE.ST_RECORD0_NORMAL
		);
		--入庫ＩＤを返す
		oInStockID := numInStockID;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsInStock;
	/************************************************************************************/
	/*	入庫指示書ステータス更新（未入庫）												*/
	/************************************************************************************/
	PROCEDURE InStockOK(
		iTrnID		IN NUMBER	,
		iUserCD		IN VARCHAR2	,
		iUserTX		IN VARCHAR2	,
		iProgramCD	IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InStockOK';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE
			TA_INSTOCK
		SET
			STINSTOCK_ID	= PS_DEFINE.ST_INSTOCK0_NOTINSTOCK
		WHERE
			TRN_ID			= iTrnID AND
			STINSTOCK_ID	= PS_DEFINE.ST_INSTOCKM1_CREATE;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InStockOK;
	/************************************************************************************/
	/*	かんばん登録																	*/
	/************************************************************************************/
	PROCEDURE RegKanban(
		iArrivID		IN NUMBER	,
		iKanbanCD		IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RegKanban';
		-- マスタ
		CURSOR curKanban(KanbanCD MA_KANBAN.KANBAN_CD%TYPE) IS
			SELECT
				*
			FROM
				MA_KANBAN
			WHERE
				KANBAN_CD = KanbanCD;
		rowKanban curKanban%ROWTYPE;
		-- 使用済みかんばん
		CURSOR curUseKanban(KanbanCD TA_KANBANP.KANBAN_CD%TYPE) IS
			SELECT
				KANBAN_CD
			FROM
				TA_KANBANP
			WHERE
				KANBAN_CD = KanbanCD;
		rowUseKanban curUseKanban%ROWTYPE;
		-- 格納テーブル
		CURSOR curInStock(ArrivID TA_INSTOCK.ARRIV_ID%TYPE) IS
			SELECT
				TI.SKU_CD			,
				TI.LOT_TX			,
				TI.CASEPERPALLET_NR	,
				TI.PALLETLAYER_NR	,
				TI.STACKINGNUMBER_NR,
				TI.TYPEPLALLET_CD	,
				TI.DESTLOCA_CD		,
				ML.WH_CD			,
				ML.FLOOR_CD			,
				ML.AREA_CD			,
				ML.LINE_CD			,
				TI.DESTLOCASTOCKC_ID AS LOCASTOCKC_ID
			FROM
				TA_INSTOCK TI,
				MA_LOCA ML
			WHERE
				TI.DESTLOCA_CD	= ML.LOCA_CD	AND
				TI.ARRIV_ID = ArrivID			AND
				TI.STINSTOCK_ID = 0;
		rowInStock curInStock%ROWTYPE;
		CURSOR curArea(AreaCD MB_AREA.AREA_CD%TYPE) IS
			SELECT
				*
			FROM
				MB_AREA
			WHERE
				AREA_CD = AreaCD;
		rowArea curArea%ROWTYPE;
		numKanbanPID NUMBER(10,0);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curKanban(iKanbanCD);
		FETCH curKanban INTO rowKanban;
		IF curKanban%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんがマスタに登録されていません。KANBAN_CD=[' || iKanbanCD || ']');
		END IF;
		CLOSE curKanban;
		OPEN curUseKanban(iKanbanCD);
		FETCH curUseKanban INTO rowUseKanban;
		IF curUseKanban%FOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'かんばんが使用されています。KANBAN_CD=[' || iKanbanCD || ']');
		END IF;
		CLOSE curUseKanban;
		OPEN curArea(rowKanban.AREA_TX);
		FETCH curArea INTO rowArea;
		IF curArea%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'エリアが違います。KANBAN_CD=[' || iKanbanCD || ']');
		END IF;
		CLOSE curArea;
		OPEN curInStock(iArrivID);
		FETCH curInStock INTO rowInStock;
			-- 更新
			-- TA_INSTOCK
			UPDATE
				TA_INSTOCK
			SET
				KANBAN_CD		= iKanbanCD			,
				--
				UPD_DT			= SYSDATE			,
				UPDUSER_CD		= iUserCD			,
				UPDUSER_TX		= iUserTX			,
				UPDPROGRAM_CD	= iProgramCD		,
				UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
			WHERE
				ARRIV_ID = iArrivID;
			UPDATE
				TA_LOCASTOCKC
			SET
				KANBAN_CD		= iKanbanCD			,
				--
				UPD_DT			= SYSDATE			,
				UPDUSER_CD		= iUserCD			,
				UPDUSER_TX		= iUserTX			,
				UPDPROGRAM_CD	= iProgramCD		,
				UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
			WHERE
				LOCASTOCKC_ID	= rowInStock.LOCASTOCKC_ID;
			
			-- ID取得
			SELECT
				NVL(MAX(KANBANP_ID),0)
			INTO
				numKanbanPID
			FROM
				TA_KANBANP;
			numKanbanPID := numKanbanPID + 1;
			-- 引当済かんばん登録
			INSERT INTO TA_KANBANP (
				KANBANP_ID			,
				STKANBAN_CD			,
				TYPEPKANBAN_CD		,
				KANBAN_CD			,
				WH_CD				,
				FLOOR_CD			,
				AREA_CD				,
				LINE_CD				,
				SKU_CD				,
				LOT_TX				,
				CASEPERPALLET_NR	,
				PALLETLAYER_NR		,
				STACKINGNUMBER_NR	,
				TYPEPLALLET_CD		,
				-- 管理項目
				UPD_DT				,
				UPDUSER_CD			,
				UPDUSER_TX			,
				ADD_DT				,
				ADDUSER_CD			,
				ADDUSER_TX			,
				UPDPROGRAM_CD		,
				UPDCOUNTER_NR		,
				STRECORD_ID			
			) VALUES (
				numKanbanPID				,
				'01'						,
				'00'						,
				iKanbanCD					,
				rowInstock.WH_CD			,
				rowInstock.FLOOR_CD			,
				rowInstock.AREA_CD			,
				rowInstock.LINE_CD			,
				rowInstock.SKU_CD			,
				rowInstock.LOT_TX			,
				rowInstock.CASEPERPALLET_NR	,
				rowInstock.PALLETLAYER_NR	,
				rowInstock.STACKINGNUMBER_NR,
				rowInstock.TYPEPLALLET_CD	,
				--
				SYSDATE						,
				iUserCD						,
				iUserTX						,
				SYSDATE						,
				iUserCD						,
				iUserTX						,
				FUNCTION_NAME				,
				0							,
				0
			);
		CLOSE curInStock;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curKanban%ISOPEN THEN
				CLOSE curKanban;
			END IF;
			IF curUseKanban%ISOPEN THEN
				CLOSE curUseKanban;
			END IF;
			IF curInStock%ISOPEN THEN
				CLOSE curInStock;
			END IF;
			IF curArea%ISOPEN THEN
				CLOSE curArea;
			END IF;
			RAISE;
	END RegKanban;
END PS_INSTOCK;
/
SHOW ERRORS
