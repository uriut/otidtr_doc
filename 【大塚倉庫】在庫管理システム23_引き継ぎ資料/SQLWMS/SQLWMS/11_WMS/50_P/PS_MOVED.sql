-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_MOVE AS
	/************************************************************************************/
	/*	移動データ作成																	*/
	/************************************************************************************/
	PROCEDURE InsMove(
		iTrnID			IN	NUMBER	,
		iMoveNoCD		IN	VARCHAR2,
		iTypeMoveCD		IN	VARCHAR2,
		irowSLocaStockC	IN	TA_LOCASTOCKC%ROWTYPE,
		irowDLocaStockC	IN	TA_LOCASTOCKC%ROWTYPE,
		iUserCD			IN	VARCHAR2,
		iUserTX			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'InsMove';
		
		numMoveID		TA_MOVE.MOVE_ID%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		SELECT
			SEQ_MOVEID.NEXtVAL INTO numMoveID
		FROM
			DUAL;
		INSERT INTO TA_MOVE (
			MOVE_ID				,	-- 補充ID
			TRN_ID				,	-- トランザクションID
			MOVENO_CD			,	-- 補充指示NO
			TYPEMOVE_CD			,	-- 補充タイプコード
			STMOVE_ID			,	-- 補充ステータス
			-- SKU
			SKU_CD				,	-- SKUコード
			LOT_TX				,	-- ロット
			SRCKANBAN_CD		,	-- かんばん番号
			CASEPERPALLET_NR	,	-- 
			PALLETLAYER_NR		,	-- 
			PALLETCAPACITY_NR	,	-- 
			STACKINGNUMBER_NR	,	-- 
			TYPEPLALLET_CD		,	-- 
			USELIMIT_DT			,	-- 使用期限
			STOCK_DT			,	-- 入庫日
			AMOUNT_NR			,	-- 入数 標準ケース・メーカー箱の入数
			PALLET_NR			,	-- パレット数
			CASE_NR				,	-- ケース数 
			BARA_NR				,	-- バラ数
			PCS_NR				,	-- ＰＣＳ
			-- 元ロケ
			SRCTYPESTOCK_CD		,	-- 在庫タイプコード
			SRCLOCA_CD			,	-- 元ロケーションコード
			SRCSTART_DT			,	-- 元開始時刻
			SRCEND_DT			,	-- 元終了時刻
			SRCHT_ID			,	-- 元HT
			SRCHTUSER_CD		,	-- 元HT社員コード
			SRCHTUSER_TX		,	-- 元HT社員名
			SRCLOCASTOCKC_ID	,	-- 元引当済み在庫ID
			-- 先ロケ
			DESTTYPESTOCK_CD	,	-- 在庫タイプコード
			DESTLOCA_CD			,	-- 先ロケーションコード
			DESTSTART_DT		,	-- 先開始時刻
			DESTEND_DT			,	-- 先終了時刻
			DESTHT_ID			,	-- 先HT
			DESTHTUSER_CD		,	-- 先HT社員コード
			DESTHTUSER_TX		,	-- 先HT社員名
			DESTLOCASTOCKC_ID	,	-- 先引当済み在庫ID
			-- 印刷
			PRTCOUNT_NR			,	-- 印刷回数
			PRT_DT				,	-- 印刷日
			PRTUSER_CD			,	-- 印刷社員コード
			PRTUSER_TX			,	-- 印刷社員名
			-- 
			BATCHNO_CD			,	-- バッチNO
			TYPEAPPLY_CD		,	-- 引当タイプコード
			TYPEBLOCK_CD		,	-- ブロックタイプ
			TYPECARRY_CD		,	-- 運搬タイプコード
			-- 管理項目
			UPD_DT				,	-- 更新日時
			UPDUSER_CD			,	-- 更新社員コード
			UPDUSER_TX			,	-- 更新社員名
			ADD_DT				,	-- 登録日時
			ADDUSER_CD			,	-- 登録社員コード
			ADDUSER_TX			,	-- 登録社員名
			UPDPROGRAM_CD		,	-- 更新プログラムコード
			UPDCOUNTER_NR		,	-- 更新カウンター
			STRECORD_ID				-- レコード状態（-2:削除、-1:作成中、0:通常）
		) VALUES (
			numMoveID						,	-- MOVE_ID
			iTrnID							,	-- TRN_ID
			iMoveNoCD						,	-- MOVENO_CD
			iTypeMoveCD						,	-- TYPEMOVE_CD
			PS_DEFINE.ST_MOVEM1_CREATE		,	-- STMOVE_ID
			-- SKU
			irowSLocaStockC.SKU_CD			,	-- SKU_CD
			irowSLocaStockC.LOT_TX			,	-- ロット
			irowSLocaStockC.KANBAN_CD		,	-- かんばん番号
			irowSLocaStockC.CASEPERPALLET_NR,	-- 
			irowSLocaStockC.PALLETLAYER_NR	,	-- 
			irowSLocaStockC.PALLETCAPACITY_NR,	-- 
			irowSLocaStockC.STACKINGNUMBER_NR,	-- 
			irowSLocaStockC.TYPEPLALLET_CD	,	-- 
			irowSLocaStockC.USELIMIT_DT		,	-- USELIMIT_DT
			irowSLocaStockC.STOCK_DT		,	-- STOCK_DT
			irowSLocaStockC.AMOUNT_NR		,	-- AMOUNT_NR
			irowSLocaStockC.PALLET_NR		,	-- PALLET_NR
			irowSLocaStockC.CASE_NR			,	-- CASE_NR
			irowSLocaStockC.BARA_NR			,	-- BARA_NR
			irowSLocaStockC.PCS_NR			,	-- PCS_NR
			-- 元ロケ
			irowSLocaStockC.TYPESTOCK_CD	,	-- SRCTYPESTOCK_CD
			irowSLocaStockC.LOCA_CD			,	-- SRCLOCA_CD
			NULL							,	-- SRCSTART_DT
			NULL							,	-- SRCEND_DT
			0								,	-- SRCHT_ID
			' '								,	-- SRCHTUSER_CD
			' '								,	-- SRCHTUSER_TX
			irowSLocaStockC.LOCASTOCKC_ID	,	-- SRCLOCASTOCKC_ID
			-- 先ロケ
			irowDLocaStockC.TYPESTOCK_CD	,	-- DESTTYPESTOCK_CD
			irowDLocaStockC.LOCA_CD			,	-- DESTLOCA_CD
			NULL							,	-- DESTSTART_DT
			NULL							,	-- DESTEND_DT
			0								,	-- DESTHT_ID
			' '								,	-- DESTHTUSER_CD
			' '								,	-- DESTHTUSER_TX
			irowDLocaStockC.LOCASTOCKC_ID	,	-- DESTLOCASTOCKC_ID
			-- 印刷
			0								,	-- PRTCOUNT_NR
			NULL							,	-- PRT_DT
			' '								,	-- PRTEMP_CD
			' '								,	-- PRTEMP_TX
			--
			' '								,	-- BATCHNO_CD
			' '								,	-- TYPEAPPLY_CD
			' '								,	-- TYPEBLOCK_CD
			' '								,	-- TYPECARRY_CD
			-- 管理項目
			NULL							,	-- UPD_DT
			' '								,	-- UPDUSER_CD
			' '								,	-- UPDUSER_TX
			SYSDATE							,	-- ADD_DT
			iUserCD							,	-- ADDUSER_CD
			iUserTX							,	-- ADDUSER_TX
			iProgramCD						,	-- UPDPROGRAM_CD
			0								,	-- UPDCOUNTER_NR
			PS_DEFINE.ST_RECORD0_NORMAL			-- STRECORD_ID
		);
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END InsMove;
	/************************************************************************************/
	/*	移動指示有効																	*/
	/************************************************************************************/
	PROCEDURE MoveDataOK(
		iTrnID			IN	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'MoveDataOK';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE TA_MOVE
		SET
			STMOVE_ID	= PS_DEFINE.ST_MOVE0_NOTMOVE
		WHERE
			TRN_ID		= iTrnID						AND
			STMOVE_ID	= PS_DEFINE.ST_MOVEM1_CREATE	;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END MoveDataOK;

END PS_MOVE;
/
SHOW ERRORS
