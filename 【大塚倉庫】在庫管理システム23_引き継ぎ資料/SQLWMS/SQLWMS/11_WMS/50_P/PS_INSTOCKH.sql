-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_INSTOCK IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_INSTOCK';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************************************/
	/*	入庫指示書追加																	*/
	/************************************************************************************/
	PROCEDURE InsInStock(
		iTrnID			IN NUMBER	,
		iInStockNoCD	IN VARCHAR2	,
		iTypeInStockCD	IN VARCHAR2	,
		iTypeApplyCD	IN VARCHAR2	,
		irowLocaStockC	IN TA_LOCASTOCKC%ROWTYPE,
		iArrivID		IN NUMBER	,
		iSkuCD			IN VARCHAR2	,
		iLotTX			IN VARCHAR2	,
		iKanbanCD		IN VARCHAR2	,
		iShipVolumeCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2	,
		oInStockID		OUT NUMBER
	);
	/************************************************************************************/
	/*	入庫指示書印刷可																*/
	/************************************************************************************/
	PROCEDURE InStockOK(
		iTrnID			IN NUMBER	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
	/************************************************************************************/
	/*	かんばん登録																	*/
	/************************************************************************************/
	PROCEDURE RegKanban(
		iArrivID		IN NUMBER	,
		iKanbanCD		IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	);
END PS_INSTOCK;
/
SHOW ERRORS
