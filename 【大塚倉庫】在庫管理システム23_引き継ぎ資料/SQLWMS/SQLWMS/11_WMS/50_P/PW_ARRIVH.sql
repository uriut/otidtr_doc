-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PW_ARRIV IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PW_ARRIV';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************************************/
	/*	入荷受付																		*/
	/************************************************************************************/
	PROCEDURE EndArriv(
		iArrivOrderNoCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/************************************************************************************/
	/*	入荷受付キャンセル																*/
	/************************************************************************************/
	PROCEDURE CanArriv(
		iArrivOrderNoCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/************************************************************************************/
	/*	入荷検品中キャンセル															*/
	/************************************************************************************/
	PROCEDURE CancelCheck(
		iArrivID			IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/************************************************************************************/
	/*	入荷予定キャンセル																*/
	/************************************************************************************/
	PROCEDURE CanArrivPlan(
		iArrivOrderNoCD		IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
END PW_ARRIV;
/
SHOW ERRORS
