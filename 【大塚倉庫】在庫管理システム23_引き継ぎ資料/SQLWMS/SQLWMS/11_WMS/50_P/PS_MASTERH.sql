-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_MASTER IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_MASTER';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************/
	/*	ユーザーマスタ取得										*/
	/************************************************************/
	PROCEDURE GetUser(
		iUserCD		IN 	VARCHAR2		,
		orowUser	OUT VA_USER%ROWTYPE
	);
	/************************************************************/
	/*	SKUマスタ取得											*/
	/************************************************************/
	PROCEDURE GetSku(
		iSkuCD		IN 	VARCHAR2		,
		orowSku		OUT	MA_SKU%ROWTYPE
	);
	/************************************************************/
	/*	SKUマスタ取得（バーコード経由）							*/
	/************************************************************/
	PROCEDURE GetSkuFromBarcode(
		iSkuBarcodeTX	IN 	VARCHAR2		,
		orowSku			OUT	MA_SKU%ROWTYPE
	);
	/************************************************************/
	/*	SKUマスタ取得（ITFバーコード経由）						*/
	/************************************************************/
	PROCEDURE GetSkuFromBarcodeITF(
		iSkuBarcodeTX	IN 	VARCHAR2		,
		orowSku			OUT	MA_SKU%ROWTYPE
	);
	/************************************************************/
	/*	SKUパレットマスタ取得									*/
	/************************************************************/
	PROCEDURE GetSkuPallet(
		iSkuCD			IN 	VARCHAR2		,
		iManuFacturerCD	IN 	VARCHAR2		,
		orowSkuPallet	OUT	MA_SKUPALLET%ROWTYPE
	);
	/************************************************************/
	/*	SKUパレットマスタ取得									*/
	/************************************************************/
	PROCEDURE GetSkuPallet(
		iSkuCD			IN 	VARCHAR2		,
		orowSkuPallet	OUT	MA_SKUPALLET%ROWTYPE
	);
	/************************************************************/
	/*	ロケーションマスタ取得									*/
	/************************************************************/
	PROCEDURE GetLoca(
		iLocaCd		IN	VARCHAR2		,
		orowLoca	OUT	MA_LOCA%ROWTYPE
	);
	/************************************************************/
	/*	ロケーションブロックマスタ取得							*/
	/************************************************************/
	PROCEDURE GetTypeLocaBlock(
		iTypeLocaBlockCD	IN 	VARCHAR2,
		orowTypeLocaBlock	OUT MB_TYPELOCABLOCK%ROWTYPE
	);
	/************************************************************/
	/*	ロケーションタイプマスタ取得							*/
	/************************************************************/
	PROCEDURE GetTypeLoca(
		iTypeLocaCD		IN 	VARCHAR2,
		orowTypeLoca	OUT MB_TYPELOCA%ROWTYPE
	);
	/************************************************************/
	/*	業務区分取得											*/
	/************************************************************/
	PROCEDURE GetTypeOperation(
		iTypeOperationCD	IN 	VARCHAR2,
		orowTypeOperation	OUT MB_TYPEOPERATION%ROWTYPE
	);
	/************************************************************/
	/*	エリアマスタ取得										*/
	/************************************************************/
	PROCEDURE GetArea(
		iAreaCD		IN 	VARCHAR2,
		orowArea	OUT MB_AREA%ROWTYPE
	);
END PS_MASTER;
/
SHOW ERRORS
