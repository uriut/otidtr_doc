-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PI_ARRIV AS
	
	/*----------------------------------------------------------*/
	/* Private（行単位チェック用）                              */
	/*----------------------------------------------------------*/
	
	/************************************************************/
	/* 項目チェック                                             */
	/************************************************************/
	PROCEDURE CheckColumn(
		inumRow IN NUMBER,
		irowData IN TA_ARRIV%ROWTYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'CheckColumn';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 1項目ずつチェックしたい処理をここへ記述
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END CheckColumn;
	/************************************************************/
	/* SKUのマスタ存在チェック                                  */
	/************************************************************/
	PROCEDURE CheckSkuCd(
		iTrnID						IN TMP_HEAD1.TRN_ID%TYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'CheckSkuCd';
		CURSOR curError(TrnID	VI_ARRIVD.TRN_ID%TYPE) IS
			SELECT
				S.SKU_CD
			FROM
				VI_ARRIVD S
					LEFT OUTER JOIN MA_SKU MS
						ON S.SKU_CD = MS.SKU_CD
			WHERE
				S.TRN_ID	= TrnID	AND
				MS.SKU_CD	IS NULL
			ORDER BY
				S.SKU_CD;
		rowError curError%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curError(iTrnID);
		FETCH curError INTO rowError;
		IF curError%FOUND THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EXCEPTION_SYS,'商品マスタが存在しません。商品コード=[' || rowError.SKU_CD || ']');
		END IF;
		CLOSE curError;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curError%ISOPEN THEN
				CLOSE curError;
			END IF;
			RAISE;
	END CheckSkuCd;
	/************************************************************/
	/* ファイル単位チェック                                     */
	/************************************************************/
	PROCEDURE CheckByFile(
		iTrnID						IN TMP_HEAD1.TRN_ID%TYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'CheckByFile';
		-- アップロードされたデータの重複分
		CURSOR curFrom(TrnID	TMP_HEAD1.TRN_ID%TYPE) IS
			SELECT
				HD.ORDERNO_CD AS ARRIVORDERNO_CD,
				BD.GYO AS ARRIVORDERDETNO_CD,
				BD.LOT AS LOT_TX
			FROM
				VI_ARRIVH		HD,
				VI_ARRIVD		BD
			WHERE
				HD.ORDERNO_CD	= BD.ORDERNO_CD	AND
				HD.TRN_ID = TrnID
			GROUP BY
				HD.ORDERNO_CD	,
				BD.GYO			,
				BD.LOT
			HAVING
				COUNT(*) > 1;
		rowFrom curFrom%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 重複チェック
		OPEN curFrom(iTrnID);
		-- 対象行を取得
		FETCH curFrom INTO rowFrom;
		-- 重複行が存在した場合はエラー
		IF curFrom%FOUND THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EXCEPTION_SYS,'入荷予定明細の同一ファイル内でキーが重複しています。');
		END IF;
		CLOSE curFrom;
		-- 在庫SKUのマスタ存在チェック
		CheckSkuCd(iTrnID);
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curFrom%ISOPEN THEN
				CLOSE curFrom;
			END IF;
			RAISE;
	END CheckByFile;
	/*----------------------------------------------------------*/
	/* Public                                                   */
	/*----------------------------------------------------------*/
	/************************************************************/
	/* アップロード完了（入荷予定・通常）                       */
	/************************************************************/
	PROCEDURE EndUploadArriv(
		iTrnID						IN NUMBER
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'EndUploadArriv';
		--ヘッダ一覧
		CURSOR curTmpArriv (
			TrnID	VI_ARRIVH.TRN_ID%TYPE
		) IS
			SELECT
				HD.ORDERNO_CD AS ARRIVORDERNO_CD				,	-- 倉庫取引区分+伝票番号+伝票日付+販売荷主
				HD.SKID AS TYPEARRIV_CD							,	-- 倉庫取引区分
				HD.DENNO AS SLIPNO_TX							,	-- 伝票番号
				TO_DATE(HD.SYMD) AS SLIPDATE_DT					,	-- 伝票日付
				HD.HNNINUSHI AS OWNER_CD						,	-- 販売荷主
				BD.GYO AS ARRIVORDERDETNO_CD					,
				HD.SKECD AS SUPPLIER_CD							,
				RTRIM(SKECD_NM) AS SUPPLIER_TX					,
				HD.CCD AS OFFICE_CD								,
				RTRIM(HD.NOHCD_NM1) || RTRIM(HD.NOHCD_NM2) || RTRIM(HD.NOHCD_NM3) AS OFFICE_TX	,
				BD.FCKBN AS MANUFACTURER_CD						,
				HD.SUCD1 AS TRANSPORTER_CD						,
				HD.SUCD1_NM AS TRANSPORTER_TX					,
				'' AS CARNUMBER_CD								,	-- todo
				BD.SKU_CD AS SKU_CD								,
				BD.LOT AS LOT_TX								,
				BD.KAN1 AS AMOUNT_NR							,
				BD.KHKBN AS BARA_FL								,
				NVL(MSP.CASEPERPALLET_NR,0) AS CASEPERPALLET_NR	,
				NVL(MSP.PALLETLAYER_NR,0) AS PALLETLAYER_NR		,
				NVL(MSP.CASEPERLAYER_NR,0) AS CASEPERLAYER_NR	,
				NVL(MSP.PALLETCAPACITY_NR,0) AS PALLETCAPACITY_NR,
				NVL(MSP.STACKINGNUMBER_NR,0) AS STACKINGNUMBER_NR,
				BD.FLG1 AS TYPEPLALLET_CD						,
				TO_DATE(BD.TYUNO) AS ARRIVPLAN_DT				,
				BD.SRY AS PCS_NR								,	-- 
				--
				CASE
					WHEN NVL(MSP.PALLETCAPACITY_NR,0) <> 0 THEN
						(BD.CASE_NR - MOD(BD.CASE_NR,NVL(MSP.PALLETCAPACITY_NR,0))) / NVL(MSP.PALLETCAPACITY_NR,0)
					ELSE 0
				END AS ARRIVPLANPALLET_NR						,
				CASE
					WHEN BD.KHKBN = 0 THEN
						CASE
							WHEN NVL(MSP.PALLETCAPACITY_NR,0) <> 0 THEN
								MOD(BD.CASE_NR,NVL(MSP.PALLETCAPACITY_NR,0))
							ELSE BD.CASE_NR
						END
					WHEN BD.KHKBN = 1 THEN
						CASE
							WHEN NVL(MSP.PALLETCAPACITY_NR,0) <> 0 THEN
								MOD(BD.CASE_NR,NVL(MSP.PALLETCAPACITY_NR,0))
							ELSE BD.CASE_NR
						END
					ELSE BD.CASE_NR
				END AS ARRIVPLANCASE_NR							,
				BD.BARA_NR AS ARRIVPLANBARA_NR					,
				BD.PCS_NR AS ARRIVPLANPCS_NR					,
				BD.MBIKO AS MEMO_TX
			FROM
				VI_ARRIVH		HD,
				VI_ARRIVD		BD,
				MA_SKUPALLET MSP
			WHERE
				HD.ORDERNO_CD	= BD.ORDERNO_CD				AND
				BD.SKU_CD		= MSP.SKU_CD (+)			AND
				BD.FCKBN		= MSP.MANUFACTURER_CD (+)	AND
				HD.TRN_ID		= TrnID						AND
				HD.DKBN			= 'HS'						AND	-- 入荷予定
				HD.END_FL		= 0
			ORDER BY
				HD.ORDERNO_CD	,
				BD.GYO			;
		rowTmpArriv		curTmpArriv%ROWTYPE;
		-- 取込済み
		CURSOR curArriv (
			TypeArrivCD			TA_ARRIV.TYPEARRIV_CD%TYPE		,
			SlipNoTX			TA_ARRIV.SLIPNO_TX%TYPE			,
			SlipDateDT			TA_ARRIV.SLIPDATE_DT%TYPE		,
			OwnerCD				TA_ARRIV.OWNER_CD%TYPE			,
			ArrivOrderDetNoCD	TA_ARRIV.ARRIVORDERDETNO_CD%TYPE,
			LotTx				TA_ARRIV.LOT_TX%TYPE
		) IS
			SELECT
				ARRIVORDERNO_CD		,
				ARRIVORDERDETNO_CD	,
				LOT_TX
			FROM
				TA_ARRIV
			WHERE
				TYPEARRIV_CD		= TypeArrivCD		AND
				SLIPNO_TX			= SlipNoTX			AND
				SLIPDATE_DT			= SlipDateDT		AND
				OWNER_CD			= OwnerCD			AND
				ARRIVORDERDETNO_CD	= ArrivOrderDetNoCD	AND
				LOT_TX				= LotTx				AND
				STARRIV_ID			<> PS_DEFINE.ST_ARRIVM2_CANCEL;
		rowArriv			curArriv%ROWTYPE;
		numArrivID		NUMBER(10,0);
		numRowNR		NUMBER;
		numPcsNR		NUMBER;
		numTotalPcsNR	NUMBER;
		
		chrCurOrderNoCD	TA_ARRIV.ARRIVORDERNO_CD%TYPE;
		chrPreOrderNoCD	TA_ARRIV.ARRIVORDERNO_CD%TYPE;
		chrArrivNoCD	TA_ARRIV.ARRIVORDERNO_CD%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		
		-- ファイル単位チェック todo
		CheckByFile(iTrnID);
		-- テーブル単位チェックはWMS側で実施
		-- 1ヶ月前の未処理データは、キャンセルにする
		-- todo 仕様再確認の為、一時的にコメントアウト
		PI_ARRIV.CancelArriv;
		
		chrCurOrderNoCD := ' ';
		chrPreOrderNoCD := ' ';
		chrArrivNoCD := ' ';
		
		OPEN curTmpArriv(iTrnID);
		LOOP
			FETCH curTmpArriv INTO rowTmpArriv;
			EXIT WHEN curTmpArriv%NOTFOUND;
			PLOG.DEBUG(logCtx, 
				'ARRIVORDERNO_CD=[' || rowTmpArriv.ARRIVORDERNO_CD || '], ' ||
				'ARRIVORDERDETNO_CD=[' || rowTmpArriv.ARRIVORDERDETNO_CD || ']'
			);
			-- 取込済み
			OPEN curArriv(
					rowTmpArriv.TYPEARRIV_CD		,
					rowTmpArriv.SLIPNO_TX			,
					rowTmpArriv.SLIPDATE_DT			,
					rowTmpArriv.OWNER_CD			,
					rowTmpArriv.ARRIVORDERDETNO_CD	,
					rowTmpArriv.LOT_TX
					);
			FETCH curArriv INTO rowArriv;
			IF curArriv%FOUND THEN
				RAISE_APPLICATION_ERROR (PS_DEFINE.EXCEPTION_SYS,
									'既に取込済みです。倉庫取引区分=[' || rowTmpArriv.TYPEARRIV_CD || '], ' ||
									'伝票番号=[' || rowTmpArriv.SLIPNO_TX || '], ' ||
									'伝票日付=[' || TO_CHAR(rowTmpArriv.SLIPDATE_DT) || '] ,' ||
									'販売荷主=[' || rowTmpArriv.OWNER_CD || ']'
									);
			END IF;
			CLOSE curArriv;
			-- 初期化
			numRowNR := 0;
			numTotalPcsNR := 0;
			
			-- 当面は、バラ入荷はしないので、エラーとする
			IF rowTmpArriv.ARRIVPLANBARA_NR <> 0 THEN
				RAISE_APPLICATION_ERROR (PS_DEFINE.EXCEPTION_SYS,'バラ入荷予定が含まれています。ARRIVORDERNO_CD=[' || rowTmpArriv.ARRIVORDERNO_CD || ']');
			END IF;
			
			chrCurOrderNoCD := rowTmpArriv.ARRIVORDERNO_CD;
				PLOG.DEBUG(logCtx, 
					'chrCurOrderNoCD=[' || chrCurOrderNoCD || '] ,' ||
					'chrPreOrderNoCD=[' || chrPreOrderNoCD || ']'
				);
			IF ( chrCurOrderNoCD != chrPreOrderNoCD ) THEN
				PS_NO.GetArrivNo(chrArrivNoCD);
				PLOG.DEBUG(logCtx, 
					'ARRIVORDERNO_CD=[' || chrArrivNoCD || '] '
				);
			END IF;
			-- 1パレ単位に登録
			-- パレット
			numRowNR := rowTmpArriv.ARRIVPLANPALLET_NR;
			FOR i IN 1..numRowNR LOOP
				-- 初期化
				numPcsNR := 0;
				-- ID取得
				SELECT
					SEQ_ARRIVID.NEXTVAL INTO numArrivID
				FROM
					DUAL;
				-- PCS数
				numPcsNR := rowTmpArriv.PALLETCAPACITY_NR * rowTmpArriv.AMOUNT_NR;
				--登録
				INSERT INTO TA_ARRIV (
					ARRIV_ID					,
					TRN_ID						,
					ARRIVORDERNO_CD				,
					ARRIVORDERDETNO_CD			,
					TYPEARRIV_CD				,
					STARRIV_ID					,
					-- 仕入先
					SUPPLIER_CD					,
					SUPPLIER_TX					,
					OFFICE_CD					,
					OFFICE_TX					,
					MANUFACTURER_CD				,
					TRANSPORTER_CD				,
					TRANSPORTER_TX				,
					CARNUMBER_CD				,
					SLIPNO_TX					,
					SLIPDATE_DT					,
					OWNER_CD					,
					MEMO_TX						,
					-- SKU
					SKU_CD						,
					LOT_TX						,
					CASEPERPALLET_NR			,
					PALLETLAYER_NR				,
					CASEPERLAYER_NR				,
					PALLETCAPACITY_NR			,
					STACKINGNUMBER_NR			,
					TYPEPLALLET_CD				,
					ARRIVPLANPALLET_NR			,
					ARRIVPLANCASE_NR			,
					ARRIVPLANBARA_NR			,
					ARRIVPLANPCS_NR				,
					ARRIVPLAN_DT				,
					--ARRIVPLAN2_DT				,
					TYPEENTRY_CD				,
					-- 管理項目
					UPD_DT						,
					UPDUSER_CD					,
					UPDUSER_TX					,
					ADD_DT						,
					ADDUSER_CD					,
					ADDUSER_TX					,
					UPDPROGRAM_CD				,
					UPDCOUNTER_NR				,
					STRECORD_ID					
				) VALUES (
						numArrivID						,
						iTrnID							,
						--
						chrArrivNoCD					,
						rowTmpArriv.ARRIVORDERDETNO_CD	,	-- ARRIVORDERDETNO_CD
						rowTmpArriv.TYPEARRIV_CD		,	-- TYPEARRIV_CD
						PS_DEFINE.ST_ARRIV0_NOTSEND		,
						-- 仕入先
						rowTmpArriv.SUPPLIER_CD			,	-- SUPPLIER_CD
						rowTmpArriv.SUPPLIER_TX			,	-- SUPPLIER_TX
						rowTmpArriv.OFFICE_CD			,	-- OFFICE_CD
						rowTmpArriv.OFFICE_TX			,	-- OFFICE_TX
						rowTmpArriv.MANUFACTURER_CD		,	-- MANUFACTURER_CD
						rowTmpArriv.TRANSPORTER_CD		,	-- TRANSPORTER_CD
						rowTmpArriv.TRANSPORTER_TX		,	-- TRANSPORTER_TX
						''								,	-- CARNUMBER_CD
						rowTmpArriv.SLIPNO_TX			,	-- SLIPNO_TX
						rowTmpArriv.SLIPDATE_DT			,	-- SLIPDATE_DT
						rowTmpArriv.OWNER_CD			,	-- OWNER_CD
						rowTmpArriv.MEMO_TX				,	-- MEMO_TX
						-- SKU
						rowTmpArriv.SKU_CD				,	-- SKU_CD
						rowTmpArriv.LOT_TX				,	-- LOT_TX
						rowTmpArriv.CASEPERPALLET_NR	,	-- CASEPERPALLET_NR
						rowTmpArriv.PALLETLAYER_NR		,	-- PALLETLAYER_NR
						rowTmpArriv.CASEPERLAYER_NR		,	-- CASEPERLAYER_NR
						rowTmpArriv.PALLETCAPACITY_NR	,	-- PALLETCAPACITY_NR
						rowTmpArriv.STACKINGNUMBER_NR	,	-- STACKINGNUMBER_NR
						rowTmpArriv.TYPEPLALLET_CD		,	-- TYPEPLALLET_CD
						1								,	-- ARRIVPLANPALLET_NR
						0								,	-- ARRIVPLANCASE_NR
						0								,	-- ARRIVPLANBARA_NR
						numPcsNR						,	-- ARRIVPLANPCS_NR
						rowTmpArriv.ARRIVPLAN_DT		,	-- ARRIVPLAN_DT
						--rowTmpArriv.ARRIVPLAN2_DT		,	-- ARRIVPLAN2_DT
						'01'							,	-- TYPEENTRY_CD
						--
						SYSDATE							,
						'BATCH'							,
						'BATCH'							,
						SYSDATE							,
						'BATCH'							,
						'BATCH'							,
						'BATCH'							,
						0								,
						0							
				);
				numTotalPcsNR := numTotalPcsNR + numPcsNR;
			END LOOP;
			-- 端数
			IF numTotalPcsNR <> rowTmpArriv.ARRIVPLANPCS_NR THEN
				-- ID取得
				SELECT
					SEQ_ARRIVID.NEXTVAL INTO numArrivID
				FROM
					DUAL;
				--登録
				INSERT INTO TA_ARRIV (
					ARRIV_ID					,
					TRN_ID						,
					ARRIVORDERNO_CD				,
					ARRIVORDERDETNO_CD			,
					TYPEARRIV_CD				,
					STARRIV_ID					,
					-- 仕入先
					SUPPLIER_CD					,
					SUPPLIER_TX					,
					OFFICE_CD					,
					OFFICE_TX					,
					MANUFACTURER_CD				,
					TRANSPORTER_CD				,
					TRANSPORTER_TX				,
					CARNUMBER_CD				,
					SLIPNO_TX					,
					SLIPDATE_DT					,
					OWNER_CD					,
					MEMO_TX						,
					-- SKU
					SKU_CD						,
					LOT_TX						,
					CASEPERPALLET_NR			,
					PALLETLAYER_NR				,
					CASEPERLAYER_NR				,
					PALLETCAPACITY_NR			,
					STACKINGNUMBER_NR			,
					TYPEPLALLET_CD				,
					ARRIVPLANPALLET_NR			,
					ARRIVPLANCASE_NR			,
					ARRIVPLANBARA_NR			,
					ARRIVPLANPCS_NR				,
					ARRIVPLAN_DT				,
					TYPEENTRY_CD				,
					-- 管理項目
					UPD_DT						,
					UPDUSER_CD					,
					UPDUSER_TX					,
					ADD_DT						,
					ADDUSER_CD					,
					ADDUSER_TX					,
					UPDPROGRAM_CD				,
					UPDCOUNTER_NR				,
					STRECORD_ID					
				) VALUES (
						numArrivID						,
						iTrnID							,
						--
						chrArrivNoCD					,
						rowTmpArriv.ARRIVORDERDETNO_CD	,	-- ARRIVORDERDETNO_CD
						rowTmpArriv.TYPEARRIV_CD		,	-- TYPEARRIV_CD
						PS_DEFINE.ST_ARRIV0_NOTSEND		,
						-- 仕入先
						rowTmpArriv.SUPPLIER_CD			,	-- SUPPLIER_CD
						rowTmpArriv.SUPPLIER_TX			,	-- SUPPLIER_TX
						rowTmpArriv.OFFICE_CD			,	-- OFFICE_CD
						rowTmpArriv.OFFICE_TX			,	-- OFFICE_TX
						rowTmpArriv.MANUFACTURER_CD		,	-- MANUFACTURER_CD
						rowTmpArriv.TRANSPORTER_CD		,	-- TRANSPORTER_CD
						rowTmpArriv.TRANSPORTER_TX		,	-- TRANSPORTER_TX
						''								,	-- CARNUMBER_CD
						rowTmpArriv.SLIPNO_TX			,	-- SLIPNO_TX
						rowTmpArriv.SLIPDATE_DT			,	-- SLIPDATE_DT
						rowTmpArriv.OWNER_CD			,	-- OWNER_CD
						rowTmpArriv.MEMO_TX				,	-- MEMO_TX
						-- SKU
						rowTmpArriv.SKU_CD				,	-- SKU_CD
						rowTmpArriv.LOT_TX				,	-- LOT_TX
						rowTmpArriv.CASEPERPALLET_NR	,	-- CASEPERPALLET_NR
						rowTmpArriv.PALLETLAYER_NR		,	-- PALLETLAYER_NR
						rowTmpArriv.CASEPERLAYER_NR		,	-- CASEPERLAYER_NR
						rowTmpArriv.PALLETCAPACITY_NR	,	-- PALLETCAPACITY_NR
						rowTmpArriv.STACKINGNUMBER_NR	,	-- STACKINGNUMBER_NR
						rowTmpArriv.TYPEPLALLET_CD		,	-- TYPEPLALLET_CD
						0								,	-- ARRIVPLANPALLET_NR
						rowTmpArriv.ARRIVPLANCASE_NR	,	-- ARRIVPLANCASE_NR
						rowTmpArriv.ARRIVPLANBARA_NR	,	-- ARRIVPLANBARA_NR
						rowTmpArriv.ARRIVPLANPCS_NR - numTotalPcsNR,	-- ARRIVPLANPCS_NR
						rowTmpArriv.ARRIVPLAN_DT		,	-- ARRIVPLAN_DT
						'01'							,	-- TYPEENTRY_CD
						--
						SYSDATE							,
						'BATCH'							,
						'BATCH'							,
						SYSDATE							,
						'BATCH'							,
						'BATCH'							,
						'BATCH'							,
						0								,
						0							
				);
			END IF;
			chrPreOrderNoCD := chrCurOrderNoCD;
		END LOOP;
		CLOSE curTmpArriv;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTmpArriv%ISOPEN THEN CLOSE curTmpArriv; END IF;
			IF curArriv%ISOPEN THEN CLOSE curArriv; END IF;
			RAISE;
	END EndUploadArriv;
	/************************************************************/
	/* アップロード完了（入荷工程前Web）                        */
	/************************************************************/
	PROCEDURE EndUploadArrivWeb(
		iTrnID				IN NUMBER,
		oUpdRowNR			OUT	NUMBER
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'EndUploadArrivWeb';
		-- 取込データ
		CURSOR curTmpArrivWeb (
			TrnID	TMP_ARRIVWEB.TRN_ID%TYPE
		) IS
			SELECT
				ARRIV_ID		,
				CARNUMBER_CD	,
				SLIPNO_TX		,
				ARRIVPLAN2_DT	,
				DRIVERTEL_TX	,
				BERTH_TX		,
				SAMEDAYSHIP_FL
			FROM
				TMP_ARRIVWEB
			WHERE
				TRN_ID		= TrnID
			ORDER BY
				ARRIV_ID;
		rowTmpArrivWeb		curTmpArrivWeb%ROWTYPE;
		--入荷予定
		CURSOR curArriv (
			ArrivID	TA_ARRIV.ARRIV_ID%TYPE
		) IS
			SELECT
				ARRIV_ID		,
				STARRIV_ID		,
				CARNUMBER_CD	,
				SLIPNO_TX		,
				ARRIVPLAN2_DT	,
				DRIVERTEL_TX	,
				BERTH_TX		,
				SAMEDAYSHIP_FL
			FROM
				TA_ARRIV
			WHERE
				ARRIV_ID	= ArrivID AND
				(
				STARRIV_ID	= PS_DEFINE.ST_ARRIV1_SEND OR
				STARRIV_ID	= PS_DEFINE.ST_ARRIV2_NOTARRIV 
				);
		rowArriv		curArriv%ROWTYPE;
		numUpdNR		NUMBER;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 初期化
		numUpdNR := 0;
		-- テーブル単位チェックはWMS側で実施
		OPEN curTmpArrivWeb(iTrnID);
		LOOP
			FETCH curTmpArrivWeb INTO rowTmpArrivWeb;
			EXIT WHEN curTmpArrivWeb%NOTFOUND;
			-- 取込済み
			OPEN curArriv(rowTmpArrivWeb.ARRIV_ID);
			FETCH curArriv INTO rowArriv;
			IF curArriv%FOUND THEN
				IF ( rowArriv.STARRIV_ID = PS_DEFINE.ST_ARRIV1_SEND OR
					 rowArriv.STARRIV_ID = PS_DEFINE.ST_ARRIV2_NOTARRIV ) THEN
					--更新
					UPDATE
						TA_ARRIV
					SET
						--
						STARRIV_ID		= PS_DEFINE.ST_ARRIV2_NOTARRIV	,
						-- Web登録データ
						CARNUMBER_CD	= rowTmpArrivWeb.CARNUMBER_CD	,
						ARRIVPLAN_DT	= TRUNC(rowTmpArrivWeb.ARRIVPLAN2_DT),
						ARRIVPLAN2_DT	= rowTmpArrivWeb.ARRIVPLAN2_DT	,
						DRIVERTEL_TX	= rowTmpArrivWeb.DRIVERTEL_TX	,
						BERTH_TX		= rowTmpArrivWeb.BERTH_TX		,
						SAMEDAYSHIP_FL	= rowTmpArrivWeb.SAMEDAYSHIP_FL	,
						-- 管理項目
						UPD_DT				= SYSDATE					,
						UPDUSER_CD			= 'BATCH'					,
						UPDUSER_TX			= 'BATCH'					,
						UPDPROGRAM_CD		= FUNCTION_NAME				,
						UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
					WHERE
						ARRIV_ID	= rowTmpArrivWeb.ARRIV_ID	AND
						--STARRIV_ID	= PS_DEFINE.ST_ARRIV1_SEND;
						STARRIV_ID IN (
										PS_DEFINE.ST_ARRIV1_SEND	,
										PS_DEFINE.ST_ARRIV2_NOTARRIV
									);
					numUpdNR := numUpdNR + 1;
				END IF;
			END IF;
			CLOSE curArriv;
		END LOOP;
		CLOSE curTmpArrivWeb;
		oUpdRowNR := numUpdNR;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTmpArrivWeb%ISOPEN THEN CLOSE curTmpArrivWeb; END IF;
			IF curArriv%ISOPEN THEN CLOSE curArriv; END IF;
			RAISE;
	END EndUploadArrivWeb;
	/************************************************************/
	/* 入荷予定のキャンセル（1ヶ月前までの未処理データ）        */
	/************************************************************/
	PROCEDURE CancelArriv
	IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'CancelArriv';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		UPDATE
			TA_ARRIV
		SET
			STARRIV_ID	= PS_DEFINE.ST_ARRIVM2_CANCEL		,
			-- 管理項目
			UPD_DT				= SYSDATE					,
			UPDUSER_CD			= 'BATCH'					,
			UPDUSER_TX			= 'BATCH'					,
			UPDPROGRAM_CD		= FUNCTION_NAME				,
			UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
		WHERE
			STARRIV_ID IN (
							PS_DEFINE.ST_ARRIV0_NOTSEND	,
							PS_DEFINE.ST_ARRIV1_SEND	,
							PS_DEFINE.ST_ARRIV2_NOTARRIV
						) AND
			ADD_DT < SYSDATE - 15; --7/15ステータス変更対象データについて、経過日数を15日に変更
			--ADD_DT < ADD_MONTHS(SYSDATE, -1); 
		COMMIT;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END CancelArriv;
END PI_ARRIV;
/
SHOW ERRORS
