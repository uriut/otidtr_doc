-- Copyright (C) Seaos Corporation 2014 All rights reserved
CREATE OR REPLACE PACKAGE BODY PW_INSTOCK IS 
	/************************************************************************************/
	/*	格納指示書取消																	*/
	/************************************************************************************/
	PROCEDURE CanInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CanInStock';
		-- 格納指示
		CURSOR curInStock(InStockNoCD	TA_INSTOCK.INSTOCKNO_CD%TYPE) IS
			SELECT
				INSTOCK_ID			,
				TYPEINSTOCK_CD		,
				STINSTOCK_ID		,
				ARRIV_ID			,
				KANBAN_CD			,
				APPLYINSTOCKNPCS_NR	PCS_NR,
				DESTTYPESTOCK_CD	TYPESTOCK_CD,
				DESTEND_DT
			FROM
				TA_INSTOCK
			WHERE
				INSTOCKNO_CD = InStockNoCD
			ORDER BY
				DESTEND_DT
			FOR UPDATE;
		rowInStock		curInStock%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
		numCount		NUMBER;
		numGoodPcsNR	TA_ARRIV.GOODPCS_NR%TYPE;
		numNGPcsNR		TA_ARRIV.NGPCS_NR%TYPE;
		numSamplePcsNR	TA_ARRIV.SAMPLEPCS_NR%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		-- 入庫指示チェック
		OPEN curInStock(iInStockNoCD);
		FETCH curInStock INTO rowInStock;
		IF curInStock%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowInStock.STINSTOCK_ID = PS_DEFINE.ST_INSTOCKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowInStock.STINSTOCK_ID = PS_DEFINE.ST_INSTOCK4_INSTOCK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowInStock.DESTEND_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入庫済みのロケーションがあるのでキャンセルできません。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 通常入庫でない
		IF rowInStock.TYPEINSTOCK_CD = PS_DEFINE.TYPE_INSTOCK_RETOK OR
			 rowInStock.TYPEINSTOCK_CD = PS_DEFINE.TYPE_INSTOCK_RETNG THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '返品入庫はキャンセルできません。');
		END IF;
		-- 指示書キャンセル
		PS_APPLYINSTOCK.CanInStock(
			iInStockNoCD	,
			iUserCD			,
			rowUser.USER_TX	,
			iProgramCD
		);
		-- 入荷予定修正
		numGoodPcsNR	:= 0;
		numNGPcsNR		:= 0;
		numSamplePcsNR	:= 0;
		IF rowInStock.TYPESTOCK_CD = PS_DEFINE.TYPE_STOCK_OK THEN
			numGoodPcsNR := rowInStock.PCS_NR;
		ELSIF rowInStock.TYPESTOCK_CD = PS_DEFINE.TYPE_STOCK_NG THEN
			numNGPcsNR := rowInStock.PCS_NR;
		ELSIF rowInStock.TYPESTOCK_CD = PS_DEFINE.TYPE_STOCK_SAMPLE THEN
			numSamplePcsNR := rowInStock.PCS_NR;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：在庫状態が不明です。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		UPDATE TA_ARRIV
		SET
			KANBAN_CD		= NULL							,
			CHECK_DT		= NULL							,
			CHECKUSER_CD	= NULL							,
			CHECKUSER_TX	= NULL							,
			CHECKHT_ID		= 0								,
			GOODPCS_NR		= GOODPCS_NR - numGoodPcsNR		,
			NGPCS_NR		= NGPCS_NR - numNGPcsNR			,
			SAMPLEPCS_NR	= SAMPLEPCS_NR - numSamplePcsNR	,
			UPD_DT			= SYSDATE						,
			UPDUSER_CD		= iUserCD						,
			UPDUSER_TX		= rowUser.USER_TX				,
			UPDPROGRAM_CD	= iProgramCD					,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			ARRIV_ID	= rowInStock.ARRIV_ID;
		IF SQL%ROWCOUNT <> 1 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入荷予定データが存在しません。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 整合性チェック
		SELECT
			COUNT(*)
		INTO
			numCount
		FROM
			TA_ARRIV
		WHERE
			ARRIV_ID	= rowInStock.ARRIV_ID AND
			(
				GOODPCS_NR		< 0 OR
				NGPCS_NR		< 0 OR
				SAMPLEPCS_NR	< 0
			);
		IF numCount > 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入荷予定データの計上数が不足しています。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 未計上状態ならステータスを更新
		UPDATE TA_ARRIV
		SET
			STARRIV_ID			= PS_DEFINE.ST_ARRIV3_ARRIV_E	,
			--
			UPD_DT				= SYSDATE						,
			UPDUSER_CD			= iUserCD						,
			UPDUSER_TX			= rowUser.USER_TX				,
			UPDPROGRAM_CD		= iProgramCD					,
			UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
		WHERE
			ARRIV_ID			= rowInStock.ARRIV_ID 				AND
			STARRIV_ID			= PS_DEFINE.ST_ARRIV4_INSTOCKCHK_E	AND
			GOODPCS_NR			= 0									AND
			NGPCS_NR			= 0									AND
			SAMPLEPCS_NR		= 0									;
		CLOSE curInStock;
		-- ロケが空いた場合はかんばん番号を解放する
		PS_STOCK.DelKanban(rowInStock.KANBAN_CD);
		
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curInStock%ISOPEN THEN CLOSE curInStock; END IF;
			RAISE;
	END CanInStock;
	/************************************************************************************/
	/*	入庫登録																		*/
	/************************************************************************************/
	PROCEDURE RegInstock(
		iArrivID		IN NUMBER	,
		iSkuCD			IN VARCHAR2	,
		iLotTX			IN VARCHAR2	,
		iSameDayShipFL	IN NUMBER	,
		iPalletNR		IN NUMBER	,
		iCaseNR			IN NUMBER	,
		iBaraNR			IN NUMBER	,
		iPcsNR			IN NUMBER	,
		iUseLimitDT		IN DATE		,
		iCheckArrivUseLimit	IN NUMBER,	--0:入荷可能期限をチェックしない、1：チェックする
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2	,
		oInStockNoCD	OUT TA_INSTOCK.INSTOCKNO_CD%TYPE,
		oKanbanCD		OUT TA_INSTOCK.KANBAN_CD%TYPE
	) AS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'RegInstock';
		numCount			NUMBER(9,0) := 0;
		rowUser				VA_USER%ROWTYPE;
		numTrnID			NUMBER(10,0);
		rowSku				MA_SKU%ROWTYPE;
		CURSOR curTarget(ArrivID TA_ARRIV.ARRIV_ID%TYPE) IS
			SELECT
				*
			FROM
				TA_ARRIV
			WHERE
				ARRIV_ID = ArrivID
			FOR UPDATE;
		rowTarget	curTarget%ROWTYPE;
		rowArea				MB_AREA%ROWTYPE;
		chrInStockNoCD		TA_INSTOCK.INSTOCKNO_CD%TYPE;
		chrKanbanCD			TA_INSTOCK.KANBAN_CD%TYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START, ' ||
			'iArrivID=[' || iArrivID || '], ' ||
			'iSkuCD=[' || iSkuCD || '], ' ||
			'iPcsNR=[' || iPcsNR || '], ' ||
			'iUseLimitDT=[' || iUseLimitDT || '], ' ||
			'iCheckArrivUseLimit=[' || iCheckArrivUseLimit || ']'
		);
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		PS_MASTER.GetSku(iSkuCD, rowSku);
		-- 入荷データ取得
		OPEN curTarget(iArrivID);
		FETCH curTarget INTO rowTarget;
		IF curTarget%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'入荷予定がありません。');
		END IF;
		IF rowTarget.SKU_CD <> iSkuCD THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：商品コードが不適切です。');
		END IF;
		-- ステータスチェック
		IF rowTarget.STARRIV_ID < PS_DEFINE.ST_ARRIV3_ARRIV_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入荷受付が完了していません。');
		END IF;
		IF rowTarget.STARRIV_ID < PS_DEFINE.ST_ARRIVM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入荷予定がキャンセルされています。');
		END IF;
		IF rowTarget.ARRIVPLANPCS_NR - ( rowTarget.GOODPCS_NR + rowTarget.NGPCS_NR + rowTarget.SAMPLEPCS_NR ) <= 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入荷予定がありません（完了済み）。');
		END IF;
		IF rowTarget.ARRIVPLANPCS_NR - ( rowTarget.GOODPCS_NR + rowTarget.NGPCS_NR + rowTarget.SAMPLEPCS_NR )  < iPcsNR THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,
				'予定数（' || rowTarget.ARRIVPLANPCS_NR - ( rowTarget.GOODPCS_NR + rowTarget.NGPCS_NR + rowTarget.SAMPLEPCS_NR ) || '）より多い入庫です。確認してください。'
			);
		END IF;
		-- TRNID
		PS_NO.GetTrnId(numTrnID);
		-- 入庫引当、実績作成
		PS_APPLYINSTOCK.ApplyInstock(
			numTrnID			,
			rowSku				,
			rowTarget			,
			iSameDayShipFL		,
			iPalletNR			,
			iCaseNR				,
			iBaraNR				,
			iPcsNR				,
			iUseLimitDT			,
			CASE WHEN iCheckArrivUseLimit = 1 THEN 0 ELSE 1 END,	--管理者棚指定フラグ
			'10'				,	-- TYPEINSTOCK_CD
			rowTarget.ARRIV_ID	,
			rowTarget.SKU_CD	,
			iLotTX				,	-- 入力されたロット
			rowTarget.TYPEPLALLET_CD,
			iUserCD				,
			rowUser.USER_TX		,
			iProgramCD			,
			chrInStockNoCD		,
			chrKanbanCD			,
			rowArea
		);
		-- 入庫指示書印刷可
		PS_INSTOCK.InStockOK(
			numTrnID		,
			iUserCD			,
			rowUser.USER_TX	,
			iProgramCD
		);
		
		-- 格納処理		todo
		
		
		--入荷予定更新
		UPDATE
			TA_ARRIV
		SET
			STARRIV_ID			= PS_DEFINE.ST_ARRIV4_INSTOCKCHK_E	,
			GOODPCS_NR			= GOODPCS_NR + iPcsNR				,
			--
			UPD_DT				= SYSDATE							,
			UPDUSER_CD			= iUserCD							,
			UPDUSER_TX			= rowUser.USER_TX					,
			UPDPROGRAM_CD		= iProgramCD						,
			UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
		WHERE
			ARRIV_ID			= iArrivID;
		--入荷予定更新（全完了）
		UPDATE
			TA_ARRIV
		SET
			STARRIV_ID			= PS_DEFINE.ST_ARRIV4_INSTOCKCHK_E	,
			
			--
			UPD_DT				= SYSDATE							,
			UPDUSER_CD			= iUserCD							,
			UPDUSER_TX			= rowUser.USER_TX					,
			UPDPROGRAM_CD		= iProgramCD						,
			UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
		WHERE
			ARRIV_ID			= iArrivID						AND
			STARRIV_ID			= PS_DEFINE.ST_ARRIV3_ARRIV_E	AND
			ARRIVPLANPCS_NR		<= GOODPCS_NR + NGPCS_NR + SAMPLEPCS_NR;
		CLOSE curTarget;
		PLOG.DEBUG(logCtx,
			'oInStockNoCD=[' || oInStockNoCD || '], ' ||
			'chrInStockNoCD=[' || chrInStockNoCD || ']'
		);
		oInStockNoCD := chrInStockNoCD;
		oKanbanCD := chrKanbanCD;
		COMMIT;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END, ' ||
			'oInStockNoCD=[' || oInStockNoCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTarget%ISOPEN THEN
				CLOSE curTarget;
			END IF;
			RAISE;
	END RegInstock;
	/************************************************************************************/
	/*	格納保留(iPadで保留する)														*/
	/************************************************************************************/
	PROCEDURE DeferInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'DeferInStock';
		-- 格納指示
		CURSOR curInStock(InStockNoCD	TA_INSTOCK.INSTOCKNO_CD%TYPE) IS
			SELECT
				INSTOCK_ID			,
				TYPEINSTOCK_CD		,
				STINSTOCK_ID		,
				ARRIV_ID			,
				APPLYINSTOCKNPCS_NR	PCS_NR,
				DESTTYPESTOCK_CD	TYPESTOCK_CD,
				DESTEND_DT
			FROM
				TA_INSTOCK
			WHERE
				INSTOCKNO_CD = InStockNoCD
			ORDER BY
				DESTEND_DT
			FOR UPDATE;
		rowInStock		curInStock%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
		numCount		NUMBER;
		numGoodPcsNR	TA_ARRIV.GOODPCS_NR%TYPE;
		numNGPcsNR		TA_ARRIV.NGPCS_NR%TYPE;
		numSamplePcsNR	TA_ARRIV.SAMPLEPCS_NR%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		-- 入庫指示チェック
		OPEN curInStock(iInStockNoCD);
		FETCH curInStock INTO rowInStock;
		IF curInStock%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowInStock.STINSTOCK_ID = PS_DEFINE.ST_INSTOCKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowInStock.STINSTOCK_ID = PS_DEFINE.ST_INSTOCK4_INSTOCK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowInStock.DESTEND_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入庫済みのロケーションがあるので保留できません。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 指示書保留
		PS_APPLYINSTOCK.DeferInStock(
			iInStockNoCD	,
			iUserCD			,
			rowUser.USER_TX	,
			iProgramCD
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curInStock%ISOPEN THEN CLOSE curInStock; END IF;
			RAISE;
	END DeferInStock;

	/************************************************************************************/
	/*	格納保留キャンセル																*/
	/************************************************************************************/
	PROCEDURE CanDeferInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CanDeferInStock';
		-- 格納指示
		CURSOR curInStock(InStockNoCD	TA_INSTOCK.INSTOCKNO_CD%TYPE) IS
			SELECT
				INSTOCK_ID			,
				TYPEINSTOCK_CD		,
				STINSTOCK_ID		,
				ARRIV_ID			,
				APPLYINSTOCKNPCS_NR	PCS_NR,
				DESTTYPESTOCK_CD	TYPESTOCK_CD,
				DESTEND_DT
			FROM
				TA_INSTOCK
			WHERE
				INSTOCKNO_CD = InStockNoCD
			ORDER BY
				DESTEND_DT
			FOR UPDATE;
		rowInStock		curInStock%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
		numCount		NUMBER;
		numGoodPcsNR	TA_ARRIV.GOODPCS_NR%TYPE;
		numNGPcsNR		TA_ARRIV.NGPCS_NR%TYPE;
		numSamplePcsNR	TA_ARRIV.SAMPLEPCS_NR%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		-- 入庫指示チェック
		OPEN curInStock(iInStockNoCD);
		FETCH curInStock INTO rowInStock;
		IF curInStock%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowInStock.STINSTOCK_ID = PS_DEFINE.ST_INSTOCKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowInStock.STINSTOCK_ID = PS_DEFINE.ST_INSTOCK4_INSTOCK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了しています。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowInStock.DESTEND_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '入庫済みのロケーションがあるので保留できません。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowInStock.STINSTOCK_ID != PS_DEFINE.ST_INSTOCK_INSTOCK_D THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が保留されていません。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 指示書保留キャンセル
		PS_APPLYINSTOCK.CanDeferInStock(
			iInStockNoCD	,
			iUserCD			,
			rowUser.USER_TX	,
			iProgramCD
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curInStock%ISOPEN THEN CLOSE curInStock; END IF;
			--IF curRslt%ISOPEN THEN CLOSE curRslt; END IF;
			RAISE;
	END CanDeferInStock;

	/************************************************************/
	/*	入庫登録完了（iPadで登録）								*/
	/************************************************************/
	PROCEDURE EndInStock(
		iInStockNoCD	IN	VARCHAR2,
		iLocaCD			IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		iLotTX			IN	VARCHAR2,
		iAmountNR		IN	NUMBER	,
		iPalletNR		IN	NUMBER	,
		iCaseNR			IN	NUMBER	,
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'EndInStock';
		numRowPoint 	NUMBER := 0;
		
		CURSOR curInStock(InStockNoCD TA_INSTOCK.INSTOCKNO_CD%TYPE) IS
			SELECT
				DESTHT_ID		,
				DESTEND_DT		,
				STINSTOCK_ID	,
				DESTLOCASTOCKC_ID,
				ARRIV_ID
			FROM
				TA_INSTOCK
			WHERE
				INSTOCKNO_CD	= InStockNoCD
			ORDER BY
				STINSTOCK_ID	,
				DESTEND_DT DESC
			FOR UPDATE;
		rowInStock			curInStock%ROWTYPE;
		CURSOR curLocaStockP(
			LocaCd TA_LOCASTOCKP.LOCA_CD%TYPE,
			SkuCd TA_LOCASTOCKP.SKU_CD%TYPE
		) IS
			SELECT
				ROWID
			FROM
				TA_LOCASTOCKP
			WHERE
				LOCA_CD 		= LocaCd		AND
				SKU_CD			= SkuCd			
			FOR UPDATE;
		rowLocaStockP 	curLocaStockP%ROWTYPE;
		rowLocaStockC	TA_LOCASTOCKC%ROWTYPE;
		rowLoca			MA_LOCA%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
		rowSku			MA_SKU%ROWTYPE;
		numPalletNR		NUMBER;
		numCaseNR		NUMBER;
		numBaraNR		NUMBER;
		numPcsNR		NUMBER;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD,rowUser);
		PS_MASTER.GetSku(iSkuCD,rowSku);
		
		OPEN curInStock(iInStockNoCD);
		FETCH curInStock INTO rowInStock;
		-- 指示書存在チェック
		IF curInStock%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01,' 指示書番号が存在しません。');
		END IF;
		CLOSE curInStock;
		-- 指示書キャンセルチェック
		IF rowInStock.STINSTOCK_ID = PS_DEFINE.ST_INSTOCKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01,' 指示書がキャンセルされています。');
		END IF;
		-- 指示書完了チェック
		IF rowInStock.DESTEND_DT IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01,' 指示書が完了しています。');
		END IF;
		-- 指示書完了チェック
		IF rowInStock.STINSTOCK_ID >= PS_DEFINE.ST_INSTOCK4_INSTOCK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EX_HT01,' 指示書が完了しています。');
		END IF;
		-- 引当済み在庫
		PS_LOCA.GetLockLocaStockC(rowInStock.DESTLOCASTOCKC_ID, rowLocaStockC);
		-- ロケーションロック
		PS_LOCA.GetLocaLock(rowLocaStockC.LOCA_CD);
		-- ロケーション取得
		PS_MASTER.GetLoca(rowLocaStockC.LOCA_CD, rowLoca);
		-- 引当済み在庫更新
		PS_STOCK.DelC(rowInStock.DESTLOCASTOCKC_ID, iUserCD, rowUser.USER_TX, iProgramCD);
		-- 数量チェック
		numPcsNR := (iPalletNR * rowLocaStockC.PALLETCAPACITY_NR * rowLocaStockC.AMOUNT_NR) + (iCaseNR * rowLocaStockC.AMOUNT_NR) + iBaraNR;
		IF numPcsNR <> iPcsNR THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, 'PCS数が不適切です。');
		END IF;
		-- 
		PS_STOCK.GetUnitPcsNr(
			iSkuCD		,
			iPcsNR		,
			rowLocaStockC.PALLETCAPACITY_NR	,
			numPalletNR	,
			numCaseNR	,
			numBaraNR
			);
		-- 引当可能数更新
		OPEN curLocaStockP(
			iLocaCD,
			iSkuCD
		);
		FETCH curLocaStockP INTO rowLocaStockP;
		IF curLocaStockP%FOUND THEN
			-- 引当可能数・更新
			PS_STOCK.UpdateP(
				rowLocaStockP.ROWID			,
				iAmountNR					,
				rowLocaStockC.PALLETCAPACITY_NR,
				numPalletNR					,
				numCaseNR					,
				numBaraNR					,
				iPcsNR						,
				iUserCD						,
				rowUser.USER_TX				,
				iProgramCD
			);
		ELSE
			-- 引当可能数・登録
			PS_STOCK.InsertP(
				rowLocaStockC.LOCA_CD		,
				rowLocaStockC.SKU_CD		,
				rowLocaStockC.LOT_TX		,
				rowLocaStockC.KANBAN_CD		,
				rowLocaStockC.CASEPERPALLET_NR,
				rowLocaStockC.PALLETLAYER_NR,
				rowLocaStockC.PALLETCAPACITY_NR,
				rowLocaStockC.STACKINGNUMBER_NR,
				rowLocaStockC.TYPEPLALLET_CD,
				rowLocaStockC.USELIMIT_DT	,
				rowLocaStockC.BATCHNO_CD	,
				rowLocaStockC.STOCK_DT		,
				rowLocaStockC.AMOUNT_NR		,
				rowLocaStockC.PALLET_NR		,
				rowLocaStockC.CASE_NR		,
				rowLocaStockC.BARA_NR		,
				rowLocaStockC.PCS_NR		,
				rowLocaStockC.TYPESTOCK_CD	,
				iUserCD						,
				rowUser.USER_TX				,
				iProgramCD
			);
		END IF;
		-- 在庫受払
		PS_STOCK.InsStockInOut(	' '							,
								' '							,
								rowLocaStockC.LOCA_CD		,
								rowLocaStockC.SKU_CD		,
								rowLocaStockC.LOT_TX		,
								rowLocaStockC.KANBAN_CD		,
								rowLocaStockC.CASEPERPALLET_NR,
								rowLocaStockC.PALLETLAYER_NR,
								rowLocaStockC.PALLETCAPACITY_NR,
								rowLocaStockC.STACKINGNUMBER_NR,
								rowLocaStockC.TYPEPLALLET_CD	,	-- iTypePalletCD
								rowLocaStockC.USELIMIT_DT		,
								rowLocaStockC.BATCHNO_CD		,
								rowLocaStockC.STOCK_DT			,
								rowLocaStockC.AMOUNT_NR			,
								rowLocaStockC.PALLET_NR			,
								rowLocaStockC.CASE_NR			,
								rowLocaStockC.BARA_NR			,
								rowLocaStockC.PCS_NR			,
								rowLocaStockC.TYPESTOCK_CD		,
								rowLocaStockC.TYPESTOCK_CD		,	-- todo iTypeLocaCD
								'100'							,	-- 入荷
								'ARRIV'							,
								0			  					,
								''								,
								1								,	-- iNeedSendFL
								iUserCD							,
								rowUser.USER_TX					,
								iProgramCD
							);
		CLOSE curLocaStockP;
		--
		UPDATE
			TA_INSTOCK
		SET
			STINSTOCK_ID		= PS_DEFINE.ST_INSTOCK4_INSTOCK_E	,
			--
			DESTSTART_DT		= SYSDATE							,
			DESTEND_DT			= SYSDATE							,
			DESTHTUSER_CD		= iUserCD							,
			DESTHTUSER_TX		= rowUser.USER_TX					,
			--
			UPD_DT				= SYSDATE							,
			UPDUSER_CD			= iUserCD							,
			UPDUSER_TX			= rowUser.USER_TX					,
			UPDPROGRAM_CD		= iProgramCD						,
			UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
		WHERE
			INSTOCKNO_CD		= iInStockNoCD;
			
		UPDATE
			TA_ARRIV
		SET
			STARRIV_ID			= PS_DEFINE.ST_ARRIV6_INSTOCK_E		,
			--
			ARRIVRSLT_DT		= SYSDATE							,	-- todo 計上日で良いのか。
			ARRIVRSLTUSER_CD	= iUserCD							,	-- todo
			ARRIVRSLTUSER_TX	= rowUser.USER_TX					,	-- todo
			--
			UPD_DT				= SYSDATE							,
			UPDUSER_CD			= iUserCD							,
			UPDUSER_TX			= rowUser.USER_TX					,
			UPDPROGRAM_CD		= iProgramCD						,
			UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
		WHERE
			ARRIV_ID			= rowInStock.ARRIV_ID;
			
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ':' || SQLERRM);
			END IF;
			RAISE;
	END EndInStock;
	/************************************************************************************/
	/*	格納紐付解除																	*/
	/************************************************************************************/
	PROCEDURE RstiPad(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstiPad';
		-- 格納指示
		CURSOR curInstock(InstockNoCD	TA_INSTOCK.INSTOCKNO_CD%TYPE) IS
			SELECT
				INSTOCKNO_CD			,
				STINSTOCK_ID
			FROM
				TA_INSTOCK
			WHERE
				INSTOCKNO_CD = InstockNoCD
			;
		rowInstock			curInstock%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		OPEN curInstock(iInStockNoCD);
		FETCH curInstock INTO rowInstock;
		IF curInstock%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowInstock.STINSTOCK_ID = PS_DEFINE.ST_INSTOCKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowInstock.STINSTOCK_ID >= PS_DEFINE.ST_INSTOCK4_INSTOCK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了済みです。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		CLOSE curInstock;
		UPDATE TA_INSTOCK
		SET
			STINSTOCK_ID	= PS_DEFINE.ST_INSTOCK0_NOTINSTOCK,
			START_DT		= NULL				,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			INSTOCKNO_CD	= iInStockNoCD;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curInstock%ISOPEN THEN CLOSE curInstock; END IF;
			RAISE;
	END RstiPad;
	/************************************************************************************/
	/*	格納開始(iPad)																	*/
	/************************************************************************************/
	PROCEDURE StartInStock(
		iInStockNoCD	IN VARCHAR2	,
		iUserCD			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'StartInStock';
		-- 格納指示
		CURSOR curInstock(InstockNoCD	TA_INSTOCK.INSTOCKNO_CD%TYPE) IS
			SELECT
				INSTOCKNO_CD			,
				STINSTOCK_ID
			FROM
				TA_INSTOCK
			WHERE
				INSTOCKNO_CD = InstockNoCD
			;
		rowInstock		curInstock%ROWTYPE;
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		OPEN curInstock(iInStockNoCD);
		FETCH curInstock INTO rowInstock;
		IF curInstock%NOTFOUND THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が存在しません。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- キャンセルチェック
		IF rowInstock.STINSTOCK_ID = PS_DEFINE.ST_INSTOCKM2_CANCEL THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書がキャンセルされています。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- 完了チェック
		IF rowInstock.STINSTOCK_ID >= PS_DEFINE.ST_INSTOCK4_INSTOCK_E THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が完了済みです。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		-- ロックチェック
		IF rowInstock.STINSTOCK_ID > PS_DEFINE.ST_INSTOCK0_NOTINSTOCK THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '指示書が既にロックされています。' ||
																'指示書番号 = [' || iInStockNoCD || ']');
		END IF;
		CLOSE curInstock;
		UPDATE TA_INSTOCK
		SET
			STINSTOCK_ID	= PS_DEFINE.ST_INSTOCK3_INSTOCK_S,
			START_DT		= SYSDATE			,
			UPD_DT			= SYSDATE			,
			UPDUSER_CD		= iUserCD			,
			UPDUSER_TX		= rowUser.USER_TX	,
			UPDPROGRAM_CD	= iProgramCD		,
			UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
		WHERE
			INSTOCKNO_CD	= iInStockNoCD;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curInstock%ISOPEN THEN CLOSE curInstock; END IF;
			RAISE;
	END StartInStock;
END PW_INSTOCK;
/
SHOW ERRORS
