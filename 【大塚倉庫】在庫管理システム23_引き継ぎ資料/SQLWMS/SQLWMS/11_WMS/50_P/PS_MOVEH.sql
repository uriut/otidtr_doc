-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PS_MOVE AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_MOVE';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************************************/
	/*	移動データ作成																	*/
	/************************************************************************************/
	PROCEDURE InsMove(
		iTrnID			IN	NUMBER	,
		iMoveNoCD		IN	VARCHAR2,
		iTypeMoveCD		IN	VARCHAR2,
		irowSLocaStockC	IN	TA_LOCASTOCKC%ROWTYPE,
		irowDLocaStockC	IN	TA_LOCASTOCKC%ROWTYPE,
		iUserCD			IN	VARCHAR2,
		iUserTX			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************************************/
	/*	移動指示有効																	*/
	/************************************************************************************/
	PROCEDURE MoveDataOK(
		iTrnID			IN	NUMBER
	);
END PS_MOVE;
/
SHOW ERRORS
