-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_DEFINE AS 
	
	FUNCTION LOT_DEFAULT 		RETURN VARCHAR2 IS BEGIN RETURN 'DEFAULT'; END ;
	FUNCTION USELIMITDT_DEFAULT RETURN DATE IS BEGIN RETURN TO_DATE('20990101','YYYYMMDD'); END ;
	FUNCTION STOCKDT_DEFAULT 	RETURN DATE IS BEGIN RETURN TO_DATE('20000101','YYYYMMDD'); END ;

	----------------------------------------------------------------------------------------------
	--配送キャリア
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_CARRIER_OTHER		RETURN CHAR IS BEGIN RETURN '1000'; END ;	-- その他
	FUNCTION TYPE_CARRIER_YAMATO	RETURN CHAR IS BEGIN RETURN '1001'; END ;	-- ヤマト
	FUNCTION TYPE_CARRIER_SAGAWA	RETURN CHAR IS BEGIN RETURN '1002'; END ;	-- 佐川
	----------------------------------------------------------------------------------------------
	--配送種別
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_DELI_NORMAL	RETURN CHAR IS BEGIN RETURN '100'; END ;	-- 宅配
	FUNCTION TYPE_DELI_MAIL_N	RETURN CHAR IS BEGIN RETURN '200'; END ;	-- メール便
	FUNCTION TYPE_DELI_MAIL_S	RETURN CHAR IS BEGIN RETURN '210'; END ;	-- メール便（速達）
	FUNCTION TYPE_DELI_OTHER	RETURN CHAR IS BEGIN RETURN '900'; END ;	-- その他
	----------------------------------------------------------------------------------------------
	--決済種別
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_CHARGE_CASH	RETURN CHAR IS BEGIN RETURN '100'; END ;	-- 代金引換
	FUNCTION TYPE_CHARGE_CREDET	RETURN CHAR IS BEGIN RETURN '800'; END ;	-- クレジット
	FUNCTION TYPE_CHARGE_OTHER	RETURN CHAR IS BEGIN RETURN '900'; END ;	-- その他
	----------------------------------------------------------------------------------------------
	--入荷タイプ
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_ARRIV_ARRIV 	RETURN VARCHAR2 IS BEGIN RETURN '10'; END ;
	----------------------------------------------------------------------------------------------
	--入庫タイプ
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_INSTOCK_NOR 	RETURN VARCHAR2 IS BEGIN RETURN '01'; END ;
	FUNCTION TYPE_INSTOCK_NG 	RETURN VARCHAR2 IS BEGIN RETURN '02'; END ;
	FUNCTION TYPE_INSTOCK_RETOK	RETURN VARCHAR2 IS BEGIN RETURN '03'; END ;
	FUNCTION TYPE_INSTOCK_RETNG	RETURN VARCHAR2 IS BEGIN RETURN '04'; END ;
	FUNCTION TYPE_INSTOCK_DIR 	RETURN VARCHAR2 IS BEGIN RETURN '05'; END ;
	----------------------------------------------------------------------------------------------
	--在庫タイプ
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_STOCK_OK			RETURN VARCHAR2 IS BEGIN RETURN  '10'; END ;	-- 良品
	FUNCTION TYPE_STOCK_NG			RETURN VARCHAR2 IS BEGIN RETURN  '20'; END ;	-- 不良品
	FUNCTION TYPE_STOCK_SAMPLE		RETURN VARCHAR2 IS BEGIN RETURN  '80'; END ;	-- サンプル品
	FUNCTION TYPE_STOCK_OTHER		RETURN VARCHAR2 IS BEGIN RETURN  '90'; END ;	-- その他
	----------------------------------------------------------------------------------------------
	--出荷タイプ
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_SHIP_SHIP 	RETURN VARCHAR2 IS BEGIN RETURN '01'; END ;
	FUNCTION TYPE_SHIP_DIRECT 	RETURN VARCHAR2 IS BEGIN RETURN '02'; END ;
	----------------------------------------------------------------------------------------------
	--ピックタイプ
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_PICK_ORDER	RETURN VARCHAR2 IS BEGIN RETURN '10'; END ;	-- オーダーピック
	FUNCTION TYPE_PICK_SKU		RETURN VARCHAR2 IS BEGIN RETURN '20'; END ;	-- SKU別ピック
	FUNCTION TYPE_PICK_CASE		RETURN VARCHAR2 IS BEGIN RETURN '30'; END ;	-- ケースピック
	FUNCTION TYPE_PICK_PALLET_N	RETURN VARCHAR2 IS BEGIN RETURN '40'; END ;	-- パレットピック（通常）
	FUNCTION TYPE_PICK_PALLET_S	RETURN VARCHAR2 IS BEGIN RETURN '50'; END ;	-- パレットピック（集約）
	----------------------------------------------------------------------------------------------
	--移動タイプ
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_MOVE_YBK		RETURN VARCHAR2 IS BEGIN RETURN '01'; END ;		-- Yブロック
	FUNCTION TYPE_MOVE_NOR		RETURN VARCHAR2 IS BEGIN RETURN '02'; END ;		-- 候補
	FUNCTION TYPE_MOVE_ADD		RETURN VARCHAR2 IS BEGIN RETURN '03'; END ;		-- 積増
	FUNCTION TYPE_MOVE_DIR		RETURN VARCHAR2 IS BEGIN RETURN '04'; END ;		-- ダイレクト
	FUNCTION TYPE_MOVE_AUTOA	RETURN VARCHAR2 IS BEGIN RETURN '05'; END ;		-- 自動積増
	----------------------------------------------------------------------------------------------
	--補充タイプ
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_SUPP_EMG		RETURN VARCHAR2 IS BEGIN RETURN '10'; END ;		-- 緊急
	FUNCTION TYPE_SUPP_DYN		RETURN VARCHAR2 IS BEGIN RETURN '20'; END ;		-- 一次ピック
	----------------------------------------------------------------------------------------------
	--ロケーションブロック
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_LOCABLOCK_T1		RETURN VARCHAR2 IS BEGIN RETURN  'T1'; END ;	--Ｔブロック
	--FUNCTION TYPE_LOCABLOCK_P1		RETURN VARCHAR2 IS BEGIN RETURN  'P1'; END ;	--Pブロック
	FUNCTION TYPE_LOCABLOCK_P1		RETURN VARCHAR2 IS BEGIN RETURN  '02'; END ;	--平積
	FUNCTION TYPE_LOCABLOCK_C1		RETURN VARCHAR2 IS BEGIN RETURN  'C1'; END ;	--セルブロック
	FUNCTION TYPE_LOCABLOCK_C2		RETURN VARCHAR2 IS BEGIN RETURN  'C2'; END ;	--日替わり
	FUNCTION TYPE_LOCABLOCK_N1		RETURN VARCHAR2 IS BEGIN RETURN  '01'; END ;	--ネステナ
	----------------------------------------------------------------------------------------------
	--出荷荷姿
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_SHIPVOLUME1_CASE		RETURN VARCHAR2 IS BEGIN RETURN  '01'; END ;	-- ケース成り
	FUNCTION TYPE_SHIPVOLUME2_PALLET	RETURN VARCHAR2 IS BEGIN RETURN  '02'; END ;	-- パレット成り
	FUNCTION TYPE_SHIPVOLUME3_NESTANER	RETURN VARCHAR2 IS BEGIN RETURN  '03'; END ;	-- ネステナ
	FUNCTION TYPE_SHIPVOLUME4_SAMEDAYSHIP	RETURN VARCHAR2 IS BEGIN RETURN  '04'; END ;	-- 即出荷
	----------------------------------------------------------------------------------------------
	--棚種別
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_LOCA_NORMAL		RETURN VARCHAR2 IS BEGIN RETURN  '10'; END ;	-- 通常棚
	----------------------------------------------------------------------------------------------
	--引当タイプ
	----------------------------------------------------------------------------------------------
	FUNCTION TYPE_APPLY_PALLET_N	RETURN VARCHAR2 IS BEGIN RETURN  'PN'; END ;	-- １パレット 
	FUNCTION TYPE_APPLY_PALLET_S	RETURN VARCHAR2 IS BEGIN RETURN  'PS'; END ;	-- 集約パレット
	FUNCTION TYPE_APPLY_CASE_LOCA	RETURN VARCHAR2 IS BEGIN RETURN  'CL'; END ;	-- ３５ケース１ロケ
	FUNCTION TYPE_APPLY_CASE_VOL	RETURN VARCHAR2 IS BEGIN RETURN  'CV'; END ;	-- 最大積載率
	FUNCTION TYPE_APPLY_CASE_NUM	RETURN VARCHAR2 IS BEGIN RETURN  'CN'; END ;	-- ３５ケース
	FUNCTION TYPE_APPLY_SKU_MIX		RETURN VARCHAR2 IS BEGIN RETURN  'SM'; END ;	-- 商品混載

	----------------------------------------------------------------------------------------------
	-- 状態
	----------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------
	--レコード状態
	----------------------------------------------------------------------------------------------
	FUNCTION ST_RECORDM2_DELETE		RETURN NUMBER IS BEGIN RETURN  -2; END;	-- レコード削除
	FUNCTION ST_RECORDM1_CREATE		RETURN NUMBER IS BEGIN RETURN  -1; END;	-- レコード作成中
	FUNCTION ST_RECORD0_NORMAL		RETURN NUMBER IS BEGIN RETURN   0; END;	-- レコード通常
	----------------------------------------------------------------------------------------------
	--引当済み在庫状態
	----------------------------------------------------------------------------------------------
	FUNCTION ST_STOCKM1_CREATE	RETURN NUMBER IS BEGIN RETURN  -1; END ;	--作成中
	FUNCTION ST_STOCK0_NOR		RETURN NUMBER IS BEGIN RETURN   0; END ;	--通常
	FUNCTION ST_STOCK1_INAPPLY	RETURN NUMBER IS BEGIN RETURN   1; END ;	--入庫引当
	FUNCTION ST_STOCK2_NGIN		RETURN NUMBER IS BEGIN RETURN   2; END ;	--不良品入庫
	FUNCTION ST_STOCK3_RETIN	RETURN NUMBER IS BEGIN RETURN   3; END ;	--返品入庫
	FUNCTION ST_STOCK4_DIRIN	RETURN NUMBER IS BEGIN RETURN   4; END ;	--ダイレクト入庫
	FUNCTION ST_STOCK11_SUPP	RETURN NUMBER IS BEGIN RETURN  11; END ;	--補充中
	FUNCTION ST_STOCK12_MOVE	RETURN NUMBER IS BEGIN RETURN  12; END ;	--移動中
	FUNCTION ST_STOCK19_PICKE	RETURN NUMBER IS BEGIN RETURN  19; END ;	--ピッキング完了
	FUNCTION ST_STOCK31_SUPPA	RETURN NUMBER IS BEGIN RETURN  31; END ;	--補充ロケ引当（在庫計上しない）
	FUNCTION ST_STOCK32_MOVEA	RETURN NUMBER IS BEGIN RETURN  32; END ;	--移動ロケ引当（在庫計上しない）
	----------------------------------------------------------------------------------------------
	--入荷状態
	----------------------------------------------------------------------------------------------
/*
	FUNCTION ST_ARRIVM2_CANCEL		RETURN NUMBER IS BEGIN RETURN  -2; END ;	--	-2	キャンセル
	FUNCTION ST_ARRIVM1_RECEIVE		RETURN NUMBER IS BEGIN RETURN  -1; END ;	--	-1	データ作成中
	FUNCTION ST_ARRIV0_NOTARRIV		RETURN NUMBER IS BEGIN RETURN   0; END ;	--	0	未入荷
	FUNCTION ST_ARRIV1_ARRIV_RST	RETURN NUMBER IS BEGIN RETURN   1; END ;	--	1	入荷取消
	FUNCTION ST_ARRIV2_ARRIV_CHG	RETURN NUMBER IS BEGIN RETURN   2; END ;	--	2	入荷数量変更
	FUNCTION ST_ARRIV3_ARRIV_ENT	RETURN NUMBER IS BEGIN RETURN   3; END ;	--	3	入荷受付中
	FUNCTION ST_ARRIV4_ARRIV_E		RETURN NUMBER IS BEGIN RETURN   4; END ;	--	4	入荷受付完了
	FUNCTION ST_ARRIV5_INSTOCKCHK_S	RETURN NUMBER IS BEGIN RETURN   5; END ;	--	5	入庫検品中
	FUNCTION ST_ARRIV6_INSTOCKCHK_E	RETURN NUMBER IS BEGIN RETURN   6; END ;	--	6	入庫検品完了
	FUNCTION ST_ARRIV7_INSTOCK_E	RETURN NUMBER IS BEGIN RETURN   7; END ;	--	7	入庫完了
*/
	FUNCTION ST_ARRIVM2_CANCEL		RETURN NUMBER IS BEGIN RETURN  -2; END ;	--	-2	キャンセル
	FUNCTION ST_ARRIVM1_RECEIVE		RETURN NUMBER IS BEGIN RETURN  -1; END ;	--	-1	データ作成中
	FUNCTION ST_ARRIV0_NOTSEND		RETURN NUMBER IS BEGIN RETURN   0; END ;	--	0	未送信
	FUNCTION ST_ARRIV1_SEND			RETURN NUMBER IS BEGIN RETURN   1; END ;	--	1	送信済
	FUNCTION ST_ARRIV2_NOTARRIV		RETURN NUMBER IS BEGIN RETURN   2; END ;	--	2	入荷待ち
	FUNCTION ST_ARRIV3_ARRIV_E		RETURN NUMBER IS BEGIN RETURN   3; END ;	--	3	入荷受付完了
	FUNCTION ST_ARRIV4_ARRIV_CHK	RETURN NUMBER IS BEGIN RETURN   4; END ;	--	4	入庫検品中
	FUNCTION ST_ARRIV4_INSTOCKCHK_E	RETURN NUMBER IS BEGIN RETURN   5; END ;	--	5	入庫検品完了
	FUNCTION ST_ARRIV5_INSTOCKCHK_P	RETURN NUMBER IS BEGIN RETURN   6; END ;	--	6	入庫中
	FUNCTION ST_ARRIV6_INSTOCK_E	RETURN NUMBER IS BEGIN RETURN   7; END ;	--	7	入庫完了
	FUNCTION ST_ARRIV7_SENDRSLT_E	RETURN NUMBER IS BEGIN RETURN   8; END ;	--	8	入庫実績送信済
	----------------------------------------------------------------------------------------------
	--入庫状態
	----------------------------------------------------------------------------------------------
	FUNCTION ST_INSTOCKM2_CANCEL		RETURN NUMBER IS BEGIN RETURN  -2; END ;	--	-2	キャンセル
	FUNCTION ST_INSTOCKM1_CREATE		RETURN NUMBER IS BEGIN RETURN  -1; END ;	--	-1	データ作成中
	FUNCTION ST_INSTOCK0_NOTINSTOCK		RETURN NUMBER IS BEGIN RETURN   0; END ;	--	0	未入庫
	FUNCTION ST_INSTOCK1_PRINTOK		RETURN NUMBER IS BEGIN RETURN   1; END ;	--	1	印刷可
	FUNCTION ST_INSTOCK2_PRINT_E		RETURN NUMBER IS BEGIN RETURN   2; END ;	--	2	印刷完了
	FUNCTION ST_INSTOCK3_INSTOCK_S		RETURN NUMBER IS BEGIN RETURN   3; END ;	--	3	入庫開始
	FUNCTION ST_INSTOCK_INSTOCK_D 		RETURN NUMBER IS BEGIN RETURN   4; END ;	--	4	保留
	FUNCTION ST_INSTOCK4_INSTOCK_E 		RETURN NUMBER IS BEGIN RETURN   5; END ;	--	5	入庫完了
	FUNCTION ST_INSTOCK5_INSTOCK_EPLAS	RETURN NUMBER IS BEGIN RETURN   6; END ;	--	6	ＥＰＬＡＳ送信済み

	----------------------------------------------------------------------------------------------
	--出荷状態
	----------------------------------------------------------------------------------------------
	FUNCTION ST_SHIPM2_CANCEL		RETURN NUMBER IS BEGIN RETURN  -2; END ;	--	-2	キャンセル
	FUNCTION ST_SHIP0_NOTAPPLY		RETURN NUMBER IS BEGIN RETURN   0; END ;	--	0	未引当
	FUNCTION ST_SHIP2_SFAILITEM		RETURN NUMBER IS BEGIN RETURN   2; END ;	--	2	欠品有り
	FUNCTION ST_SHIP8_PAPPLY_S		RETURN NUMBER IS BEGIN RETURN   8; END ;	--	8	ピック引当中
	FUNCTION ST_SHIP9_PAPPLY_E		RETURN NUMBER IS BEGIN RETURN   9; END ;	--	9	ピック引当完了
	FUNCTION ST_SHIP10_PICKPRINT_E	RETURN NUMBER IS BEGIN RETURN  10; END ;	--	10	ピッキング指示書印刷完了
	FUNCTION ST_SHIP11_PICK_S		RETURN NUMBER IS BEGIN RETURN  11; END ;	--	11	検品中
	FUNCTION ST_SHIP12_PICKOK_E		RETURN NUMBER IS BEGIN RETURN  12; END ;	--	12	検品正完了          
	FUNCTION ST_SHIP14_DELIOK_S		RETURN NUMBER IS BEGIN RETURN  14; END ;	--	14	エフ発行中（正）
	FUNCTION ST_SHIP16_DELIOK_E		RETURN NUMBER IS BEGIN RETURN  16; END ;	--	16	エフ発行完了（正）
	FUNCTION ST_SHIP18_REGCASEOK_S	RETURN NUMBER IS BEGIN RETURN  18; END ;	--	18	出荷ケース登録開始
	FUNCTION ST_SHIP20_REGCASEOK_E	RETURN NUMBER IS BEGIN RETURN  20; END ;	--	20	出荷ケース登録完了
	FUNCTION ST_SHIP22_SHIPOK		RETURN NUMBER IS BEGIN RETURN  22; END ;  	--	22	出荷完了（正）
	FUNCTION ST_SHIP29_SHIPSTOP		RETURN NUMBER IS BEGIN RETURN  29; END ;  	--	29	出荷停止
	FUNCTION ST_SHIP31_SHIPSEND_E	RETURN NUMBER IS BEGIN RETURN  31; END ;  	--	31	送信完了
	----------------------------------------------------------------------------------------------
	--ギフトカード状態
	----------------------------------------------------------------------------------------------
	FUNCTION ST_GIFTCARD0_NOTCHECKED	RETURN NUMBER IS BEGIN RETURN 0; END ;	--	0	未校正
	FUNCTION ST_GIFTCARD1_CHECK_S		RETURN NUMBER IS BEGIN RETURN 1; END ;	--	1	校正中
	FUNCTION ST_GIFTCARD2_CHECK_E		RETURN NUMBER IS BEGIN RETURN 2; END ;	--	2	校正完了
	FUNCTION ST_GIFTCARD9_UNCHECKED		RETURN NUMBER IS BEGIN RETURN 9; END ;	--	9	校正対象外
	----------------------------------------------------------------------------------------------
	--のし状態
	----------------------------------------------------------------------------------------------
	FUNCTION ST_WRAP0_NOTCHECKED		RETURN NUMBER IS BEGIN RETURN 0; END ;	--	0	未校正
	FUNCTION ST_WRAP1_CHECK_S			RETURN NUMBER IS BEGIN RETURN 1; END ;	--	1	校正中
	FUNCTION ST_WRAP2_CHECK_E			RETURN NUMBER IS BEGIN RETURN 2; END ;	--	2	校正完了
	FUNCTION ST_WRAP9_UNCHECKED			RETURN NUMBER IS BEGIN RETURN 9; END ;	--	9	校正対象外
	----------------------------------------------------------------------------------------------
	--ピック状態
	----------------------------------------------------------------------------------------------
	FUNCTION ST_PICKM2_CANCEL		RETURN NUMBER  IS BEGIN RETURN  -2; END ;	-- キャンセル
	FUNCTION ST_PICKM1_CREATE		RETURN NUMBER  IS BEGIN RETURN  -1; END ;	-- データ作成中
	FUNCTION ST_PICK0_PRINTNG		RETURN NUMBER  IS BEGIN RETURN  0; END ;	-- 未補充
	FUNCTION ST_PICK1_PRINTOK		RETURN NUMBER  IS BEGIN RETURN  1; END ;	-- 印刷可
	FUNCTION ST_PICK2_PRINT_E		RETURN NUMBER  IS BEGIN RETURN  2; END ;	-- 印刷完了
	FUNCTION ST_PICK3_PICK_S		RETURN NUMBER  IS BEGIN RETURN  3; END ;	-- ピック中
	FUNCTION ST_PICK4_PICKOK_E		RETURN NUMBER  IS BEGIN RETURN  4; END ;	-- ピック完了
	FUNCTION ST_PICK6_PRINTOK_E		RETURN NUMBER  IS BEGIN RETURN  6; END ;	-- エフ印刷完了
	FUNCTION ST_PICK8_SHIPOK_E		RETURN NUMBER  IS BEGIN RETURN  8; END ;	-- 送信済み
	----------------------------------------------------------------------------------------------
	--移動状態
	----------------------------------------------------------------------------------------------
	FUNCTION ST_MOVEM2_CANCEL		RETURN NUMBER  IS BEGIN RETURN  -2; END ;		-- -2	キャンセル
	FUNCTION ST_MOVEM1_CREATE		RETURN NUMBER  IS BEGIN RETURN  -1; END ;		-- -1	データ作成中
	FUNCTION ST_MOVE0_NOTMOVE		RETURN NUMBER  IS BEGIN RETURN  0; END ;		-- 0	未移動
	FUNCTION ST_MOVE1_PRINT_E		RETURN NUMBER  IS BEGIN RETURN  1; END ;		-- 1	印刷完了
	FUNCTION ST_MOVE2_MOVEOUT_S		RETURN NUMBER  IS BEGIN RETURN  2; END ;		-- 2	移動元出庫開始
	FUNCTION ST_MOVE3_MOVEOUTOK_E	RETURN NUMBER  IS BEGIN RETURN  3; END ;		-- 3	移動元出庫完了
	FUNCTION ST_MOVE4_MOVEOUTNG_E	RETURN NUMBER  IS BEGIN RETURN  4; END ;		-- 4	移動元出庫異常
	FUNCTION ST_MOVE5_MOVEIN_S		RETURN NUMBER  IS BEGIN RETURN  5; END ;		-- 5	移動先入庫開始
	FUNCTION ST_MOVE6_MOVEINOK_E	RETURN NUMBER  IS BEGIN RETURN  6; END ;		-- 6	移動先入庫完了
	FUNCTION ST_MOVE7_MOVEINNG_E	RETURN NUMBER  IS BEGIN RETURN  7; END ;		-- 7	移動先入庫異常
	----------------------------------------------------------------------------------------------
	--補充状態
	----------------------------------------------------------------------------------------------
	FUNCTION ST_SUPPM2_CANCEL		RETURN NUMBER  IS BEGIN RETURN  -2; END ;		-- -2	キャンセル
	FUNCTION ST_SUPPM1_CREATE		RETURN NUMBER  IS BEGIN RETURN  -1; END ;		-- -1	データ作成中
	FUNCTION ST_SUPP0_NOTSUPP		RETURN NUMBER  IS BEGIN RETURN  0; END ;		-- 0	未補充
	FUNCTION ST_SUPP1_PRINT_E		RETURN NUMBER  IS BEGIN RETURN  1; END ;		-- 1	印刷完了
	FUNCTION ST_SUPP2_SUPPOUT_S		RETURN NUMBER  IS BEGIN RETURN  2; END ;		-- 2	補充元出庫開始
	FUNCTION ST_SUPP3_SUPPOUT_ENT	RETURN NUMBER  IS BEGIN RETURN  3; END ;		-- 3	補充元出庫中
	FUNCTION ST_SUPP4_SUPPOUTOK_E	RETURN NUMBER  IS BEGIN RETURN  4; END ;		-- 4	補充元出庫完了
	FUNCTION ST_SUPP5_SUPPOUTNG_E	RETURN NUMBER  IS BEGIN RETURN  5; END ;		-- 5	補充元出庫異常
	FUNCTION ST_SUPP6_SUPPIN_S		RETURN NUMBER  IS BEGIN RETURN  6; END ;		-- 6	補充先入庫開始
	FUNCTION ST_SUPP7_SUPPIN_ENT	RETURN NUMBER  IS BEGIN RETURN  7; END ;		-- 7	補充先入庫中
	FUNCTION ST_SUPP8_SUPPINOK_E	RETURN NUMBER  IS BEGIN RETURN  8; END ;		-- 8	補充先入庫完了
	FUNCTION ST_SUPP9_SUPPINNG_E	RETURN NUMBER  IS BEGIN RETURN  9; END ;		-- 9	補充先入庫異常
	----------------------------------------------------------------------------------------------
	--出荷ケース状態
	----------------------------------------------------------------------------------------------
	FUNCTION ST_SHIPCASEM2_CANCEL	RETURN NUMBER IS BEGIN RETURN  -2; END ;		-- -2	キャンセル
	FUNCTION ST_SHIPCASEM1_CREATE	RETURN NUMBER IS BEGIN RETURN  -1; END ;		-- -1	データ作成中
	FUNCTION ST_SHIPCASE0_NOTPRINT	RETURN NUMBER  IS BEGIN RETURN  0; END ;		-- 0	エフ未印刷
	FUNCTION ST_SHIPCASE1_PRINT_E	RETURN NUMBER  IS BEGIN RETURN  1; END ;		-- 1	
	FUNCTION ST_SHIPCASE2_REGIST_E	RETURN NUMBER  IS BEGIN RETURN  2; END ;		-- 2	エフ印刷完了
	FUNCTION ST_SHIPCASE3_REGCASE_E	RETURN NUMBER  IS BEGIN RETURN  3; END ;		-- 3	出荷ケース登録完了
	FUNCTION ST_SHIPCASE4_SEND_E	RETURN NUMBER  IS BEGIN RETURN  4; END ;		-- 4	送信済み
	
	
END PS_DEFINE;
/
SHOW ERRORS
