-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_SHIP AS
	/************************************************************************************/
	/*	出荷引当前チェック																*/
	/************************************************************************************/
	PROCEDURE ChkApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ChkApplyShip';
		-- 対象オーダー
		CURSOR curTarget(TrnID TMP_SELECTID.TRN_ID%TYPE) IS
			SELECT
				TSH.ORDERNO_CD		,
				TSH.ORDERNOSUB_CD	,
				TSH.STSHIP_ID
			FROM
				TMP_SELECTID	TMP,
				TA_SHIPH		TSH
			WHERE
				TMP.SELECT_ID	= TSH.SHIPH_ID	AND
				TMP.TRN_ID		= TrnID
			ORDER BY
				TSH.SHIPH_ID;
		rowTarget		curTarget%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		--
		OPEN curTarget(iTrnID);
		LOOP
			FETCH curTarget INTO rowTarget;
			EXIT WHEN curTarget%NOTFOUND;
			-- ステータスチェック
			IF rowTarget.STSHIP_ID NOT IN (PS_DEFINE.ST_SHIP0_NOTAPPLY,PS_DEFINE.ST_SHIP2_SFAILITEM) THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '未引当以外の出荷指示データが選択されています。' || 
					'受注番号 = [' || rowTarget.ORDERNO_CD || '], ' ||
					'枝番 = [' || rowTarget.ORDERNOSUB_CD || ']'
				);
			END IF;
		END LOOP;
		CLOSE curTarget;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTarget%ISOPEN THEN CLOSE curTarget; END IF;
			RAISE;
	END ChkApplyShip;
	/************************************************************************************/
	/*	出荷引当開始更新																*/
	/************************************************************************************/
	PROCEDURE StartApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'StartApplyShip';
		-- 対象オーダー
		CURSOR curTarget(TrnID TMP_SELECTID.TRN_ID%TYPE) IS
			-- todo キャンセルは？
			SELECT
				TSH.SHIPH_ID									,
				TSH.OPERATORCOMMENT_TX	AS OPERATORCOMMENT_TX	,	-- セットされている場合は「贈答オーダー」
				TSH.GIFTTYPE_CD									,	-- NONEでない場合は「ギフトオーダー」
				COUNT(TSD.SHIPD_ID)		AS DETCOUNT_NR				-- 複数明細オーダーの判別に利用
			FROM
				TMP_SELECTID	TMP,
				TA_SHIPH		TSH,
				TA_SHIPD		TSD,
				MA_SKU			MSK
			WHERE
				TMP.SELECT_ID	= TSH.SHIPH_ID					AND
				TSH.SHIPH_ID	= TSD.SHIPH_ID					AND
				TSD.SKU_CD		= MSK.SKU_CD					AND
				TMP.TRN_ID		= TrnID							AND
				(
					TSH.STSHIP_ID	= PS_DEFINE.ST_SHIP0_NOTAPPLY	OR
					TSH.STSHIP_ID	= PS_DEFINE.ST_SHIP2_SFAILITEM
				)
			GROUP BY
				TSH.SHIPH_ID			,
				TSH.OPERATORCOMMENT_TX,
				TSH.GIFTTYPE_CD
			ORDER BY
				TSH.SHIPH_ID;
		rowTarget	curTarget%ROWTYPE;
		--
		numCount		NUMBER;
		typeApplyCD		TA_SHIPD.TYPEAPPLY_CD%TYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 初期化
		DELETE TMP_APPLYSHIP;
		numCount := 0;
		-- 選択されたオーダー単位に処理
		OPEN curTarget(iTrnID);
		LOOP
			FETCH curTarget INTO rowTarget;
			EXIT WHEN curTarget%NOTFOUND;
			--IF rowTarget.SKUSHIPSTOP_FL = 0 THEN
				numCount := numCount + 1;
				-- 引当順で利用する種別を判定
				-- TODO 業務要件に合わせて要修正。
				typeApplyCD := '00';
				-- 出荷指示明細更新
				-- セット品の親商品も更新
				UPDATE TA_SHIPD
				SET
					APPLY_DT		= SYSDATE		,
					APPLYUSER_CD	= iUserCD		,
					APPLYUSER_TX	= iUserTX		,
					--
					BATCHNO_CD		= iTrnID		,
					TYPEAPPLY_CD	= typeApplyCD	,
					--
					UPD_DT			= SYSDATE		,
					UPDUSER_CD		= iUserCD		,
					UPDUSER_TX		= iUserTX		,
					UPDPROGRAM_CD	= iProgramCD	,
					UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
				WHERE
					SHIPH_ID		= rowTarget.SHIPH_ID;
				-- 一時テーブルへ登録
				INSERT INTO TMP_APPLYSHIP (
					TRN_ID			,
					ADD_DT			,
					--
					SHIPH_ID		,
					SHIPD_ID		,
					SKU_CD			,
					LOT_TX			,
					ORDERPALLET_NR	,
					ORDERCASE_NR	,
					ORDERBARA_NR	,
					ORDERPCS_NR		,
					SHORTPALLET_NR	,
					SHORTCASE_NR	,
					SHORTBARA_NR	,
					SHORTPCS_NR		,
					SHORT_FL
				) SELECT
					iTrnID			,
					SYSDATE			,
					--
					TSH.SHIPH_ID	,
					TSD.SHIPD_ID	,
					TSD.SKU_CD		,
					TSD.LOT_TX		,
					TSD.ORDERPALLET_NR	,
					TSD.ORDERCASE_NR	,
					TSD.ORDERBARA_NR	,
					TSD.ORDERPCS_NR	,
					0				,
					0				,
					0				,
					0				,
					0
				FROM
					TA_SHIPH	TSH,
					TA_SHIPD	TSD
				WHERE
					TSH.SHIPH_ID	= TSD.SHIPH_ID		AND
					TSH.SHIPH_ID	= rowTarget.SHIPH_ID;
			--END IF;
		END LOOP;
		CLOSE curTarget;
		IF numCount = 0 THEN
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '引当対象データがありません。');
		END IF;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTarget%ISOPEN THEN CLOSE curTarget; END IF;
			RAISE;
	END StartApplyShip;
	/************************************************************************************/
	/*	出荷引当終了更新																*/
	/************************************************************************************/
	PROCEDURE EndApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'EndApplyShip';
		-- 対象オーダー
		CURSOR curTarget(TrnID TMP_SELECTID.TRN_ID%TYPE) IS
			SELECT
				TSH.SHIPH_ID,
				TMP.SHORT_FL
			FROM
				TMP_APPLYSHIP	TMP,
				TA_SHIPH		TSH
			WHERE
				TMP.SHIPH_ID	= TSH.SHIPH_ID					AND
				TMP.TRN_ID		= TrnID							AND
				TSH.STSHIP_ID	>= PS_DEFINE.ST_SHIP0_NOTAPPLY
			GROUP BY
				TSH.SHIPH_ID,
				TMP.SHORT_FL
			ORDER BY
				TSH.SHIPH_ID,
				TMP.SHORT_FL;
		rowTarget		curTarget%ROWTYPE;
		
		numStShipID		NUMBER;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 選択されたオーダー単位に処理
		OPEN curTarget(iTrnID);
		LOOP
			FETCH curTarget INTO rowTarget;
			EXIT WHEN curTarget%NOTFOUND;
			IF rowTarget.SHORT_FL = 0 THEN
				numStShipID	:= PS_DEFINE.ST_SHIP9_PAPPLY_E;
			ELSE
				numStShipID	:= PS_DEFINE.ST_SHIP2_SFAILITEM;
			END IF;
			-- 出荷指示明細更新
			-- セット品の親商品も更新
			UPDATE TA_SHIPD
			SET
				START_DT		= SYSDATE		,
				END_DT			= NULL			,
				STSHIP_ID		= numStShipID	,
				--
				UPD_DT			= SYSDATE		,
				UPDUSER_CD		= iUserCD		,
				UPDUSER_TX		= iUserTX		,
				UPDPROGRAM_CD	= iProgramCD	,
				UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
			WHERE
				SHIPH_ID		= rowTarget.SHIPH_ID;
			-- 出荷指示ヘッダー更新
			UPDATE TA_SHIPH
			SET
				START_DT		= SYSDATE		,
				END_DT			= NULL			,
				STSHIP_ID		= numStShipID	,
				--
				UPD_DT			= SYSDATE		,
				UPDUSER_CD		= iUserCD		,
				UPDUSER_TX		= iUserTX		,
				UPDPROGRAM_CD	= iProgramCD	,
				UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
			WHERE
				SHIPH_ID		= rowTarget.SHIPH_ID;
		END LOOP;
		CLOSE curTarget;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTarget%ISOPEN THEN CLOSE curTarget; END IF;
			RAISE;
	END EndApplyShip;
	
END PS_SHIP;
/
SHOW ERRORS
