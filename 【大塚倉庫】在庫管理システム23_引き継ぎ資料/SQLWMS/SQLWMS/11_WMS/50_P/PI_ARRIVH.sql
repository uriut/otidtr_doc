-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PI_ARRIV AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PI_ARRIV';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************/
	/* アップロード完了（入荷予定・通常）                       */
	/************************************************************/
	PROCEDURE EndUploadArriv(
		iTrnID				IN NUMBER
	);
	/************************************************************/
	/* アップロード完了（入荷工程前Web）                        */
	/************************************************************/
	PROCEDURE EndUploadArrivWeb(
		iTrnID				IN NUMBER,
		oUpdRowNR			OUT	NUMBER
	);
	/************************************************************/
	/* 入荷予定のキャンセル（前日までの未処理データ）           */
	/************************************************************/
	PROCEDURE CancelArriv;
END PI_ARRIV;
/
SHOW ERRORS
