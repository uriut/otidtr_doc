-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PI_SHIP IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PI_SHIP';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************************************/
	/*	出荷指示登録																	*/
	/************************************************************************************/
	PROCEDURE EndUploadShip (
		iTrnID				IN	NUMBER
	);
	/************************************************************************************/
	/*	選択ID追加																		*/
	/************************************************************************************/
	PROCEDURE InsSelectID(
		iTrnID				IN NUMBER,
		iSelectID			IN NUMBER
	);
	/************************************************************************************/
	/*	出荷引当																		*/
	/************************************************************************************/
	PROCEDURE ApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	);
END PI_SHIP;
/
SHOW ERRORS
