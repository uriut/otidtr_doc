-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PW_SHIP IS
	/************************************************************************************/
	/*																					*/
	/*									Public											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************************************/
	/*	出荷引当																		*/
	/************************************************************************************/
	PROCEDURE ApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyShip';
		
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || '], ' || 'iUserCD = [' || iUserCD || ']'
		);
		-- 暫定対応　20140630
		-- ネス2,3段目の飲料（完パレ）をパレット成りへ
		UPDATE
			MA_LOCA
		SET
			TYPELOCABLOCK_CD	= '02'			,	--平積み
			SHIPVOLUME_CD		= '02'			,	--パレット成り
			UPD_DT				= SYSDATE		,
			UPDPROGRAM_CD		= FUNCTION_NAME
		WHERE
			LOCA_CD IN (
						SELECT
							LP.LOCA_CD
						FROM
							TA_LOCASTOCKP LP
								LEFT OUTER JOIN MA_LOCA ML
									ON LP.LOCA_CD = ML.LOCA_CD
								LEFT OUTER JOIN MA_SKU MS
									ON LP.SKU_CD = MS.SKU_CD
						WHERE
							MS.ABCSUB_ID = 2			AND	-- 飲料のみ
							NVL(LP.CASE_NR,0) = 0		AND
							(
							ML.LOCA_CD LIKE '%21' OR
							ML.LOCA_CD LIKE '%31'
							)
						);

		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		-- 引当状態確認
		PS_STATUS.ChkApply;
		-- 引当状態更新
		PS_STATUS.SetShipApply;
		COMMIT;
		BEGIN
--			-- マスターチェック（不整合）TODO
--			PS_APPLY.ChkMaster;
			-- 出荷ステータスチェック
			PS_SHIP.ChkApplyShip(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
			-- 出荷状態開始更新（出荷引当用一時テーブル作成）
			PS_SHIP.StartApplyShip(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
--			-- マスターチェック（データとの紐付き）TODO
--			PS_APPLY.ChkMaster2(iTrnID);
--			-- 引当
			PS_APPLYSHIP.ApplyShipMain(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
			-- ピッキング指示番号付与
			PS_APPLYPICK.ApplyPickMain(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
			-- 緊急補充引当
			PS_APPLYSUPP.ApplyESupp(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
			-- 補充指示書有効
			PS_SUPP.SuppDataOK(iTrnID);
			-- ピック指示書有効
			PS_PICK.PickDataOK(iTrnID);
			-- 出荷状態完了更新
			PS_SHIP.EndApplyShip(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
			-- 引当状態更新
			PS_STATUS.RstShipApply;
			-- 暫定対応　20140630
			-- ネス2,3段目の飲料（完パレ）をネスへ戻す
			UPDATE
				MA_LOCA
			SET
				TYPELOCABLOCK_CD	= '01'		,	--ネステナ
				SHIPVOLUME_CD		= '03'		,	--ネステナ
				UPDPROGRAM_CD		= 'ADMIN'
			WHERE
				UPDPROGRAM_CD = FUNCTION_NAME;
			
			COMMIT;
		EXCEPTION
			WHEN OTHERS THEN
				ROLLBACK;
				-- 暫定対応　20140630
				-- ネス2,3段目の飲料（完パレ）をネスへ戻す
				UPDATE
					MA_LOCA
				SET
					TYPELOCABLOCK_CD	= '01'		,	--ネステナ
					SHIPVOLUME_CD		= '03'		,	--ネステナ
					UPDPROGRAM_CD		= 'ADMIN'
				WHERE
					UPDPROGRAM_CD = FUNCTION_NAME;
				-- 引当状態更新
				PS_STATUS.RstShipApply;
				COMMIT;
				RAISE;
		END;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || '], ' || 'iUserCD = [' || iUserCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyShip;
	/************************************************************************************/
	/*	出荷引当取消																	*/
	/************************************************************************************/
	PROCEDURE CanApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CanApplyShip';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID=[' || iTrnID || '], iUserCD=[' || iUserCD || ']'
		);
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID=[' || iTrnID || '], iUserCD=[' || iUserCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END CanApplyShip;
	/************************************************************************************/
	/*	出荷キャンセル																	*/
	/************************************************************************************/
	PROCEDURE CanShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CanShip';
		-- 対象オーダー
		CURSOR curTarget(TrnID TMP_SELECTID.TRN_ID%TYPE) IS
			SELECT
				TSH.ORDERNO_CD	,
				TSH.SHIPH_ID	,
				TSH.STSHIP_ID
			FROM
				TMP_SELECTID	TMP,
				TA_SHIPH		TSH
			WHERE
				TMP.SELECT_ID	= TSH.SHIPH_ID AND
				TMP.TRN_ID		= TrnID;
		rowTarget		curTarget%ROWTYPE;
		
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || '], iUserCD = [' || iUserCD || ']'
		);
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD,rowUser);
		-- 選択されたオーダー単位に処理
		OPEN curTarget(iTrnID);
		LOOP
			FETCH curTarget INTO rowTarget;
			EXIT WHEN curTarget%NOTFOUND;
			-- 出荷ステータス確認：取消でなく、引当前であること
			IF rowTarget.STSHIP_ID NOT IN (PS_DEFINE.ST_SHIP0_NOTAPPLY, PS_DEFINE.ST_SHIP2_SFAILITEM) THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'未引当、または欠品以外のデータが選択されています。' || 
					'受注番号 = [' || rowTarget.ORDERNO_CD || ']'
				);
			END IF;
			--
			UPDATE TA_SHIPH
			SET
				START_DT		= NULL			,
				END_DT			= NULL			,
				STSHIP_ID		= PS_DEFINE.ST_SHIPM2_CANCEL,
				--
				UPD_DT			= SYSDATE		,
				UPDUSER_CD		= iUserCD		,
				UPDUSER_TX		= rowUser.USER_TX,
				UPDPROGRAM_CD	= iProgramCD	,
				UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
			WHERE
				SHIPH_ID		= rowTarget.SHIPH_ID;
		END LOOP;
		CLOSE curTarget;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTarget%ISOPEN THEN CLOSE curTarget; END IF;
			RAISE;
	END CanShip;
	/************************************************************************************/
	/*	出荷キャンセル解除																*/
	/************************************************************************************/
	PROCEDURE RstCanShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstCanShip';
		-- 対象オーダー
		CURSOR curTarget(TrnID TMP_SELECTID.TRN_ID%TYPE) IS
			SELECT
				TSH.ORDERNO_CD	,
				TSH.SHIPH_ID	,
				TSH.STSHIP_ID
			FROM
				TMP_SELECTID	TMP,
				TA_SHIPH		TSH
			WHERE
				TMP.SELECT_ID	= TSH.SHIPH_ID AND
				TMP.TRN_ID		= TrnID;
		rowTarget		curTarget%ROWTYPE;
		
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || '], iUserCD = [' || iUserCD || ']'
		);
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD,rowUser);
		-- 選択されたオーダー単位に処理
		OPEN curTarget(iTrnID);
		LOOP
			FETCH curTarget INTO rowTarget;
			EXIT WHEN curTarget%NOTFOUND;
			-- 出荷ステータス確認：取消であること
			IF rowTarget.STSHIP_ID != PS_DEFINE.ST_SHIPM2_CANCEL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'キャンセル以外のデータが選択されています。' || 
					'受注番号 = [' || rowTarget.ORDERNO_CD || ']'
				);
			END IF;
			--
			UPDATE TA_SHIPH
			SET
				START_DT		= NULL			,
				END_DT			= NULL			,
				STSHIP_ID		= PS_DEFINE.ST_SHIP0_NOTAPPLY,
				--
				UPD_DT			= SYSDATE		,
				UPDUSER_CD		= iUserCD		,
				UPDUSER_TX		= rowUser.USER_TX,
				UPDPROGRAM_CD	= iProgramCD	,
				UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
			WHERE
				SHIPH_ID		= rowTarget.SHIPH_ID;
		END LOOP;
		CLOSE curTarget;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTarget%ISOPEN THEN CLOSE curTarget; END IF;
			RAISE;
	END RstCanShip;
	/************************************************************************************/
	/*	欠品照会メインルーチン															*/
	/************************************************************************************/
	PROCEDURE StockOutMain(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'StockOutMain';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID=[' || iTrnID || '], iUserCD=[' || iUserCD || ']'
		);

		--欠品対象を算出
		CreStockOut(
			iTrnID	  ,
			iUserCD	  ,
			iProgramCD
		);

		--対象をテーブルに登録する
		ChkStockOut(
			iTrnID	  ,
			iUserCD	  ,
			iProgramCD
		);
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID=[' || iTrnID || '], iUserCD=[' || iUserCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END StockOutMain;
	/************************************************************************************/
	/*	欠品対象作成																	*/
	/************************************************************************************/
	PROCEDURE CreStockOut(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CreStockOut';

		--実欠品
		CURSOR curStockOutReal IS
			SELECT 
				SD.SKU_CD	  ,
				SD.LOT_TX	  ,
				SD.SHIPPCS_NR ,
				NVL(LP.STOCKPCS_NR, 0) AS STOCKPCS_NR
			FROM
				(SELECT 
					SD.SKU_CD ,
					SD.LOT_TX ,
					SUM(SD.ORDERPCS_NR) AS SHIPPCS_NR
				FROM 
					TA_SHIPD SD
						LEFT OUTER JOIN TA_SHIPH SH
							ON SD.SHIPH_ID = SH.SHIPH_ID
				WHERE 
					SH.STSHIP_ID = 0 OR --未引当
					SH.STSHIP_ID = 2	--欠品
				GROUP BY 
					SD.SKU_CD ,
					SD.LOT_TX
				) SD
				LEFT OUTER JOIN
					(SELECT
						SKU_CD,
						LOT_TX,
						SUM(PCS_NR) AS STOCKPCS_NR
					FROM
						(SELECT 
							SKU_CD ,
							LOT_TX ,
							PCS_NR
						FROM 
							TA_LOCASTOCKP
						UNION ALL
						SELECT
							SKU_CD,
							LOT_TX,
							PCS_NR
						FROM
							TA_INSTOCK
						WHERE
							STINSTOCK_ID >= 0 AND --未入庫のもの
							STINSTOCK_ID < 5
						UNION ALL
						SELECT
							SKU_CD,
							LOT_TX,
							PCS_NR
						FROM
							TA_SUPP
						WHERE
							STSUPP_ID >= 0 AND 
							STSUPP_ID < 8  AND
							SHIPH_ID = 0 --在庫になるもの
						UNION ALL
						SELECT
							SKU_CD,
							LOT_TX,
							PCS_NR
						FROM
							TA_MOVE
						WHERE
							STMOVE_ID >= 0 AND --移動中のもの
							STMOVE_ID < 6
						) TMP
					GROUP BY
						SKU_CD,
						LOT_TX
					) LP 
						ON SD.SKU_CD = LP.SKU_CD AND 
						   SD.LOT_TX = LP.LOT_TX
			WHERE 
				SD.SHIPPCS_NR > NVL(LP.STOCKPCS_NR, 0);

		rowStockOutReal		curStockOutReal%ROWTYPE;

		--引当時欠品
		CURSOR curStockOutApply IS
			SELECT 
				SD.SKU_CD	  ,
				SD.LOT_TX	  ,
				SD.SHIPPCS_NR ,
				NVL(LP.STOCKPCS_NR, 0) AS STOCKPCS_NR
			FROM
				(SELECT 
					SD.SKU_CD ,
					SD.LOT_TX ,
					SUM(SD.ORDERPCS_NR) AS SHIPPCS_NR
				FROM 
					TA_SHIPD SD
						LEFT OUTER JOIN TA_SHIPH SH
							ON SD.SHIPH_ID = SH.SHIPH_ID
				WHERE 
					SH.STSHIP_ID = 0 OR --未引当
					SH.STSHIP_ID = 2	--欠品
				GROUP BY 
					SD.SKU_CD ,
					SD.LOT_TX
				) SD
				LEFT OUTER JOIN
					(SELECT
						LP.SKU_CD,
						LP.LOT_TX,
						SUM(LP.PCS_NR) AS STOCKPCS_NR
					FROM
						TA_LOCASTOCKP LP
							LEFT OUTER JOIN MA_LOCA ML
								ON LP.LOCA_CD = ML.LOCA_CD
					WHERE
						( --ロケマスタの引当可能となる条件を加味                          
							( ML.TYPELOCABLOCK_CD = '02' AND ML.SHIPVOLUME_CD = '02' ) OR 
							( ML.TYPELOCABLOCK_CD = '01' AND ML.SHIPVOLUME_CD = '03' ) OR 						
							( ML.TYPELOCABLOCK_CD = '02' AND ML.SHIPVOLUME_CD = '01' )    
						) AND                                                             
						ML.NOTSHIP_FL = 0                                                 
					GROUP BY
						LP.SKU_CD,
						LP.LOT_TX
					) LP 
					ON SD.SKU_CD = LP.SKU_CD AND 
					   SD.LOT_TX = LP.LOT_TX
			WHERE 
				SD.SHIPPCS_NR > NVL(LP.STOCKPCS_NR, 0);

		rowStockOutApply	curStockOutApply%ROWTYPE;

		numCount			NUMBER := 0;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID=[' || iTrnID || '], iUserCD=[' || iUserCD || ']'
		);

		--実欠品の算出
		OPEN curStockOutReal;
		LOOP
			FETCH curStockOutReal INTO rowStockOutReal;
			EXIT WHEN curStockOutReal%NOTFOUND;

			INSERT INTO TMP_STOCKOUT
			(
				TRN_ID		,
				ADD_DT		,
				--          
				SKU_CD		,
				LOT_TX		,
				SHIPPCS_NR	,
				STOCKPCS_NR ,
				STOCKOUT_CD
			)
			VALUES
			(
				iTrnID		, 
				SYSDATE		, 
				--
				rowStockOutReal.SKU_CD	   ,
				rowStockOutReal.LOT_TX	   ,
				rowStockOutReal.SHIPPCS_NR ,
				rowStockOutReal.STOCKPCS_NR,
				'01' --在庫なし
			);

		END LOOP;
		CLOSE curStockOutReal;

		--引当時欠品の算出
		OPEN curStockOutApply;
		LOOP
			FETCH curStockOutApply INTO rowStockOutApply;
			EXIT WHEN curStockOutApply%NOTFOUND;
			
			--すでに実欠品となっているか確認
			SELECT
				COUNT(*) INTO numCount
			FROM
				TMP_STOCKOUT
			WHERE
				TRN_ID = iTrnID AND
				SKU_CD = rowStockOutApply.SKU_CD AND
				LOT_TX = rowStockOutApply.LOT_TX;

			--実欠品の場合は、すでに登録済みなのでスキップ
			IF numCount = 0 THEN

				INSERT INTO TMP_STOCKOUT
				(
					TRN_ID		,
					ADD_DT		,
					--          
					SKU_CD		,
					LOT_TX		,
					SHIPPCS_NR	,
					STOCKPCS_NR ,
					STOCKOUT_CD
				)
				VALUES
				(
					iTrnID		, 
					SYSDATE		, 
					--
					rowStockOutApply.SKU_CD		,
					rowStockOutApply.LOT_TX		,
					rowStockOutApply.SHIPPCS_NR ,
					rowStockOutApply.STOCKPCS_NR,
					'00'
				);

			END IF;

		END LOOP;
		CLOSE curStockOutApply;
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID=[' || iTrnID || '], iUserCD=[' || iUserCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END CreStockOut;
	/************************************************************************************/
	/*	欠品理由確認																	*/
	/************************************************************************************/
	PROCEDURE ChkStockOut(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ChkStockOut';

		CURSOR curStockOut(
			TrnID TMP_STOCKOUT.TRN_ID%TYPE
		) IS
			SELECT
				SKU_CD		,
				LOT_TX		,
				SHIPPCS_NR	,
				STOCKPCS_NR
			FROM
				TMP_STOCKOUT
			WHERE
				TRN_ID = TrnID AND
				STOCKOUT_CD = '00'
			ORDER BY
				SKU_CD ,
				LOT_TX ;

		rowStockOut		curStockOut%ROWTYPE;

		CURSOR curLocaS(
			SkuCD TMP_STOCKOUT.SKU_CD%TYPE,
			LotTX TMP_STOCKOUT.LOT_TX%TYPE
		) IS
			SELECT 
				LP.SKU_CD ,
				LP.LOT_TX ,
				LP.PCS_NR
			FROM 
				TA_LOCASTOCKP LP
					LEFT OUTER JOIN MA_LOCA ML
						ON LP.LOCA_CD = ML.LOCA_CD
			WHERE 
				LP.SKU_CD = SkuCD AND
				LP.LOT_TX = LotTX AND
				( --ロケマスタの引当可能となる条件を加味                          
					( ML.TYPELOCABLOCK_CD = '02' AND ML.SHIPVOLUME_CD = '02' ) OR 
					( ML.TYPELOCABLOCK_CD = '01' AND ML.SHIPVOLUME_CD = '03' ) OR 						
					( ML.TYPELOCABLOCK_CD = '02' AND ML.SHIPVOLUME_CD = '01' ) 
				) AND                                                             
				ML.NOTSHIP_FL = 1 AND --出荷不可
				LP.PCS_NR > 0;

		rowLocaS	curLocaS%ROWTYPE;

		CURSOR curLocaM(
			SkuCD TMP_STOCKOUT.SKU_CD%TYPE,
			LotTX TMP_STOCKOUT.LOT_TX%TYPE
		) IS
			SELECT 
				LP.SKU_CD ,
				LP.LOT_TX ,
				LP.PCS_NR
			FROM 
				TA_LOCASTOCKP LP
					LEFT OUTER JOIN MA_LOCA ML
						ON LP.LOCA_CD = ML.LOCA_CD
			WHERE 
				LP.SKU_CD = SkuCD AND
				LP.LOT_TX = LotTX AND
				NOT ( --ロケマスタの引当可能とならない条件を加味                          
					( ML.TYPELOCABLOCK_CD = '02' AND ML.SHIPVOLUME_CD = '02' ) OR 
					( ML.TYPELOCABLOCK_CD = '01' AND ML.SHIPVOLUME_CD = '03' ) OR 						
					( ML.TYPELOCABLOCK_CD = '02' AND ML.SHIPVOLUME_CD = '01' ) 
				) AND                                                             
				ML.NOTSHIP_FL = 0 AND --出荷可
				LP.PCS_NR > 0;

		rowLocaM	curLocaM%ROWTYPE;

		CURSOR curInstock(
			SkuCD TMP_STOCKOUT.SKU_CD%TYPE,
			LotTX TMP_STOCKOUT.LOT_TX%TYPE
		) IS
			SELECT
				SKU_CD,
				LOT_TX,
				SUM(PCS_NR) PCS_NR
			FROM
				TA_INSTOCK
			WHERE
				SKU_CD = SkuCD	  AND
				LOT_TX = LotTX	  AND
				STINSTOCK_ID >= 0 AND --未入庫のもの
				STINSTOCK_ID < 5
			GROUP BY
				SKU_CD,
                LOT_TX;

		rowInstock	curInstock%ROWTYPE;

		CURSOR curSupp(
			SkuCD TMP_STOCKOUT.SKU_CD%TYPE,
			LotTX TMP_STOCKOUT.LOT_TX%TYPE
		) IS
			SELECT
				SKU_CD,
				LOT_TX,
				SUM(PCS_NR) PCS_NR
			FROM
				TA_SUPP
			WHERE
				SKU_CD = SkuCD AND
				LOT_TX = LotTX AND
				STSUPP_ID >= 0 AND 
				STSUPP_ID < 8  AND
				SHIPH_ID = 0 --在庫になるもの
			GROUP BY
				SKU_CD,
				LOT_TX;

		rowSupp		curSupp%ROWTYPE;

		CURSOR curMove(
			SkuCD TMP_STOCKOUT.SKU_CD%TYPE,
			LotTX TMP_STOCKOUT.LOT_TX%TYPE
		) IS
			SELECT
				SKU_CD,
				LOT_TX,
				SUM(PCS_NR) PCS_NR
			FROM
				TA_MOVE
			WHERE
				SKU_CD = SkuCD AND
				LOT_TX = LotTX AND
				STMOVE_ID >= 0 AND --移動中のもの
				STMOVE_ID < 6
			GROUP BY
				SKU_CD,
				LOT_TX;

		rowMove		curMove%ROWTYPE;

		reqPcsNR	NUMBER := 0;
		numArriv	NUMBER := 0;
		numSupp		NUMBER := 0;
		numMove		NUMBER := 0;
		numFound	NUMBER := 0;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID=[' || iTrnID || '], iUserCD=[' || iUserCD || ']'
		);

		OPEN curStockOut(iTrnID);
		LOOP
			FETCH curStockOut INTO rowStockOut;
			EXIT WHEN curStockOut%NOTFOUND;

			reqPcsNR := rowStockOut.SHIPPCS_NR - rowStockOut.STOCKPCS_NR;
			numArriv := 0;
			numSupp	 := 0;
			numMove	 := 0;
			numFound := 0;

			IF numFound = 0 THEN
				OPEN curInstock(rowStockOut.SKU_CD, rowStockOut.LOT_TX);
				FETCH curInstock INTO rowInstock;
				IF curInstock%FOUND THEN
					IF rowInstock.PCS_NR >= reqPcsNR THEN
						UPDATE 
							TMP_STOCKOUT
						SET
							STOCKOUT_CD = '02' --入荷中
						WHERE
							TRN_ID = iTrnID 		 AND	
							SKU_CD = rowInstock.SKU_CD AND
							LOT_TX = rowInstock.LOT_TX;			

						numFound := 1;
					ELSE
						numArriv := rowInstock.PCS_NR;
					END IF;
				END IF;
				CLOSE curInstock;
			END IF;

			IF numFound = 0 THEN
				OPEN curSupp(rowStockOut.SKU_CD, rowStockOut.LOT_TX);
				FETCH curSupp INTO rowSupp;
				IF curSupp%FOUND THEN
					IF rowSupp.PCS_NR >= reqPcsNR THEN
						UPDATE 
							TMP_STOCKOUT
						SET
							STOCKOUT_CD = '03' --補充中
						WHERE
							TRN_ID = iTrnID 		 AND	
							SKU_CD = rowSupp.SKU_CD AND
							LOT_TX = rowSupp.LOT_TX;			

						numFound := 1;
					ELSE
						numSupp := rowSupp.PCS_NR;
					END IF;
				END IF;
				CLOSE curSupp;
			END IF;

			IF numFound = 0 THEN
				OPEN curMove(rowStockOut.SKU_CD, rowStockOut.LOT_TX);
				FETCH curMove INTO rowMove;
				IF curMove%FOUND THEN
					IF rowMove.PCS_NR >= reqPcsNR THEN
						UPDATE 
							TMP_STOCKOUT
						SET
							STOCKOUT_CD = '04' --移動中
						WHERE
							TRN_ID = iTrnID 		 AND	
							SKU_CD = rowMove.SKU_CD AND
							LOT_TX = rowMove.LOT_TX;			

						numFound := 1;
					ELSE
						numMove := rowMove.PCS_NR;
					END IF;
				END IF;
				CLOSE curMove;
			END IF;

			IF numFound = 0 THEN
				IF numArriv + numSupp + numMove >= reqPcsNR THEN
					UPDATE 
						TMP_STOCKOUT
					SET
						STOCKOUT_CD = '99' --複数あり
					WHERE
						TRN_ID = iTrnID 		 	AND	
						SKU_CD = rowStockOut.SKU_CD AND
						LOT_TX = rowStockOut.LOT_TX;

					numFound := 1;
				END IF;
			END IF;

			IF numFound = 0 THEN
				OPEN curLocaS(rowStockOut.SKU_CD, rowStockOut.LOT_TX);
				FETCH curLocaS INTO rowLocaS;
				IF curLocaS%FOUND THEN
					UPDATE 
						TMP_STOCKOUT
					SET
						STOCKOUT_CD = '05' --出荷不可
					WHERE
						TRN_ID = iTrnID 		 AND	
						SKU_CD = rowLocaS.SKU_CD AND
						LOT_TX = rowLocaS.LOT_TX;			

					numFound := 1;
				END IF;
				CLOSE curLocaS;
			END IF;

			IF numFound = 0 THEN
				OPEN curLocaM(rowStockOut.SKU_CD, rowStockOut.LOT_TX);
				FETCH curLocaM INTO rowLocaM;
				IF curLocaM%FOUND THEN
					UPDATE 
						TMP_STOCKOUT
					SET
						STOCKOUT_CD = '06' --ロケ設定
					WHERE
						TRN_ID = iTrnID 		 AND	
						SKU_CD = rowLocaM.SKU_CD AND
						LOT_TX = rowLocaM.LOT_TX;			

					numFound := 1;
				END IF;
				CLOSE curLocaM;
			END IF;

		END LOOP;
		CLOSE curStockOut;
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID=[' || iTrnID || '], iUserCD=[' || iUserCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ChkStockOut;

END PW_SHIP;
/
SHOW ERRORS
