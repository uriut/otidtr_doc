-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PW_MOVE AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PW_MOVE';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************/
	/*	移動指示作成											*/
	/************************************************************/
	PROCEDURE RegistMove(
		iSkuCD			IN	VARCHAR2,
		iKanbanCD		IN	VARCHAR2,
		iSrcLocaCD		IN	VARCHAR2, --元ロケ
		iDestLocaCD		IN	VARCHAR2, --先ロケ
		iPalletNR		IN	NUMBER	, --移動パレット数
		iCaseNR			IN	NUMBER	, --移動ケース数
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	移動指示取消											*/
	/************************************************************/
	PROCEDURE CanMove(
		iMoveNoCD		IN	VARCHAR2,
		iUserCD			IN  VARCHAR2,
		iProgramCD		IN  VARCHAR2
	);
	/************************************************************/
	/*	移動元ピック開始										*/
	/************************************************************/
	PROCEDURE StartSrcMove(
		iMoveNoCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	移動元ピック完了										*/
	/************************************************************/
	PROCEDURE EndSrcMove(
		iMoveNoCD		IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		iPalletNR		IN	NUMBER	,
		iCaseNR			IN	NUMBER	,
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iSrcLocaCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	移動先入庫開始											*/
	/************************************************************/
	PROCEDURE StartDestMove(
		iMoveNoCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	移動先入庫完了											*/
	/************************************************************/
	PROCEDURE EndDestMove(
		iMoveNoCD		IN	VARCHAR2,
		iSkuCD			IN	VARCHAR2,
		iKanbanCD		IN	VARCHAR2,
		iPalletNR		IN	NUMBER	,
		iCaseNR			IN	NUMBER	,
		iBaraNR			IN	NUMBER	,
		iPcsNR			IN	NUMBER	,
		iDestLocaCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
	/************************************************************/
	/*	移動紐付解除											*/
	/************************************************************/
	PROCEDURE RstiPad(
		iMoveNoCD		IN	VARCHAR2,
		iUserCD			IN	VARCHAR2,
		iProgramCD		IN	VARCHAR2
	);
END PW_MOVE;
/
SHOW ERRORS
