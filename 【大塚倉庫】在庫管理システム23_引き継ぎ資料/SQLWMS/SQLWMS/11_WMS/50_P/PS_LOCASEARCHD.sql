-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_LOCASEARCH AS
	/************************************************************************************/
	/*	ＰＣＳ検索（パレット）															*/
	/************************************************************************************/
	PROCEDURE PalletPcs(
		iSkuCD				IN	VARCHAR2				,
		iLotTx				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iPalletCapacityNR	IN	NUMBER					, --Add
		iPalletNR			IN	NUMBER					,
		iCaseNR				IN	NUMBER					,
		iBaraNR				IN	NUMBER					,
		iPcsNR				IN	NUMBER					,
		irowSku				IN	MA_SKU%ROWTYPE			,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP 		OUT	TA_LOCASTOCKP%ROWTYPE	,
		oTypeCarryCD		OUT	VARCHAR2				,
		oSerchOK			OUT	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'PalletPcs';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iSkuCD=[' || iSkuCD || '], iPcsNR=[' || iPcsNR || ']'
		);
		oSerchOK := 0;
		/*
		■引当順
		�@即出荷エリア（完パレ）
		�A即出荷エリア（完パレでない）
		�Bパレット成り（完パレ）
		�Cパレット成り（完パレでない）
		�Dネステナエリア（完パレ）
		�Eネステナエリア（完パレでない）
		�Fケース成りエリア（完パレ）
		�Gケース成りエリア（完パレでない）
		*/
		-- �@即出荷エリア（完パレ）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsPalletCapa(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				iPalletCapacityNR			, --Add
				PS_DEFINE.TYPE_LOCABLOCK_P1	,
				PS_DEFINE.TYPE_SHIPVOLUME4_SAMEDAYSHIP	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �A即出荷エリア（完パレでない）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsNotPalletCapa(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				iPalletCapacityNR			, --Add
				PS_DEFINE.TYPE_LOCABLOCK_P1	,
				PS_DEFINE.TYPE_SHIPVOLUME4_SAMEDAYSHIP	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �Bパレット成り（完パレ）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsPalletCapa(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				iPalletCapacityNR			, --Add
				PS_DEFINE.TYPE_LOCABLOCK_P1	,
				PS_DEFINE.TYPE_SHIPVOLUME2_PALLET	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �Cパレット成り（完パレでない）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsNotPalletCapa(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				iPalletCapacityNR			, --Add
				PS_DEFINE.TYPE_LOCABLOCK_P1	,
				PS_DEFINE.TYPE_SHIPVOLUME2_PALLET	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �Dネステナエリア（完パレ）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsPalletCapa(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				iPalletCapacityNR			, --Add
				PS_DEFINE.TYPE_LOCABLOCK_N1	,
				PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �Eネステナエリア（完パレでない）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsNotPalletCapa(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				iPalletCapacityNR			, --Add
				PS_DEFINE.TYPE_LOCABLOCK_N1	,
				PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �Fケース成りエリア（完パレ）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsPalletCapa(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				iPalletCapacityNR			, --Add
				PS_DEFINE.TYPE_LOCABLOCK_P1	,
				PS_DEFINE.TYPE_SHIPVOLUME1_CASE	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �Gケース成りエリア（完パレでない）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsNotPalletCapa(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				iPalletCapacityNR			, --Add
				PS_DEFINE.TYPE_LOCABLOCK_P1	,
				PS_DEFINE.TYPE_SHIPVOLUME1_CASE	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iSkuCD = [' || iSkuCD || '], iPcsNR = [' || iPcsNR || '], ' ||
			'oTypeCarryCD = [' || oTypeCarryCD || '], oSerchOK = [' || oSerchOK || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END PalletPcs;
	/************************************************************************************/
	/*	ＰＣＳ検索（ケース）															*/
	/************************************************************************************/
	PROCEDURE CasePcs(
		iSkuCD				IN	VARCHAR2				,
		iLotTx				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iPalletNR			IN	NUMBER					,
		iCaseNR				IN	NUMBER					,
		iBaraNR				IN	NUMBER					,
		iPcsNR				IN	NUMBER					,
		irowSku				IN	MA_SKU%ROWTYPE			,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP 		OUT	TA_LOCASTOCKP%ROWTYPE	,
		oTypeCarryCD		OUT	VARCHAR2				,
		oSerchOK			OUT	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CasePcs';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iSkuCD=[' || iSkuCD || '], iPcsNR=[' || iPcsNR || ']'
		);
		oSerchOK := 0;
		/*
		■引当順
		�@即出荷エリア（完パレでない）
		�A即出荷エリア（完パレ）
		�Bケース成りエリア（完パレでない）
		�Cケース成りエリア（完パレ）
		�Dネステナエリア（完パレでない）
		�Eネステナエリア（完パレ）
		�F緊急補充
		*/
		-- �@即出荷エリア（完パレでない）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsNotPallet(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				PS_DEFINE.TYPE_LOCABLOCK_P1	,
				PS_DEFINE.TYPE_SHIPVOLUME4_SAMEDAYSHIP	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �A即出荷エリア（完パレ）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsPallet(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				PS_DEFINE.TYPE_LOCABLOCK_P1	,
				PS_DEFINE.TYPE_SHIPVOLUME4_SAMEDAYSHIP	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �Bケース成りエリア（完パレでない）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsNotPallet(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				PS_DEFINE.TYPE_LOCABLOCK_P1	,
				PS_DEFINE.TYPE_SHIPVOLUME1_CASE	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �Cケース成りエリア（完パレ）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsPallet(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				PS_DEFINE.TYPE_LOCABLOCK_P1	,
				PS_DEFINE.TYPE_SHIPVOLUME1_CASE	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �Dネステナエリア（完パレでない）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsNotPallet(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				PS_DEFINE.TYPE_LOCABLOCK_N1	,
				PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �Eネステナエリア（完パレ）
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '00';	-- 補充なし
			PS_LOCASEARCH.PcsPallet(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				PS_DEFINE.TYPE_LOCABLOCK_N1	,
				PS_DEFINE.TYPE_SHIPVOLUME3_NESTANER	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		-- �F緊急補充
		IF oSerchOK = 0 THEN
			oTypeCarryCD := '10';	-- 補充あり
			PS_LOCASEARCH.PcsSupp(
				iSkuCd						,
				iLotTx						,
				iTypeStockCD				,
				PS_DEFINE.TYPE_LOCABLOCK_P1	,
				PS_DEFINE.TYPE_SHIPVOLUME2_PALLET	,
				orowLoca					,
				orowTypeLoca				,
				orowLocaStockP				,
				oSerchOK
			);
		END IF;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iSkuCD = [' || iSkuCD || '], iPcsNR = [' || iPcsNR || '], ' ||
			'oTypeCarryCD = [' || oTypeCarryCD || '], oSerchOK = [' || oSerchOK || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END CasePcs;
	/************************************************************************************/
	/*	ＰＣＳ検索・完パレ（パレット積載数同一）										*/
	/************************************************************************************/
	PROCEDURE PcsPalletCapa(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iPalletCapacityNR	IN	NUMBER					, --Add
		iLocaBlockCD		IN	VARCHAR2				,
		iShipVolumeCD		IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP	 	OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'PcsPalletCapa';
		CURSOR curLocaStockP(
			SkuCd				TA_LOCASTOCKP.SKU_CD%TYPE,
			LotTx				TA_LOCASTOCKP.LOT_TX%TYPE,
			TypeStockCD			TA_LOCASTOCKP.TYPESTOCK_CD%TYPE,
			PalletCapacityNR	TA_LOCASTOCKP.PALLETCAPACITY_NR%TYPE,
			TypeLocaBlockCd		MB_TYPELOCABLOCK.TYPELOCABLOCK_CD%TYPE,
			ShipVolumeCD		MA_LOCA.SHIPVOLUME_CD%TYPE
		) IS
			SELECT
				LSP.LOCA_CD		,
				LSP.SKU_CD		,
				LSP.ROWID
			FROM
				TA_LOCASTOCKP 		LSP	,
				MA_LOCA 			MLC	,
				MB_TYPELOCA			MTL	
			WHERE
				LSP.LOCA_CD				= MLC.LOCA_CD			AND
				MLC.TYPELOCA_CD			= MTL.TYPELOCA_CD		AND
				LSP.SKU_CD 				= SkuCd 				AND
				LSP.LOT_TX 				= LotTx 				AND
				LSP.TYPESTOCK_CD		= TypeStockCD			AND
				LSP.PALLETCAPACITY_NR	= PalletCapacityNR		AND --Add
				MLC.TYPELOCABLOCK_CD	= TypeLocaBlockCd		AND
				MLC.SHIPVOLUME_CD		= ShipVolumeCD			AND
				LSP.PCS_NR				> 0						AND
				LSP.CASE_NR				= 0						AND
				LSP.BARA_NR				= 0						AND
				MLC.NOTSHIP_FL			= 0 --出荷引当可
			ORDER BY
				LSP.PALLET_NR		,
				MLC.LINEUNIT_NR DESC,
				MLC.LOCA_CD			;
		rowLocaStockP	curLocaStockP%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iSkuCD = [' || iSkuCD || '], iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iLocaBlockCD = [' || iLocaBlockCD || ']'
		);
		oSerchOK := 0;
		OPEN curLocaStockP(iSkuCD, iLotTX, iTypeStockCD, iPalletCapacityNR, iLocaBlockCD, iShipVolumeCD);
		LOOP
			FETCH curLocaStockP INTO rowLocaStockP;
			EXIT WHEN curLocaStockP%NOTFOUND;
			--ロケーションデータ取得
			PS_LOCASEARCH.GetLocaData(
				rowLocaStockP.ROWID		,
				rowLocaStockP.LOCA_CD	,
				orowLoca				,
				orowTypeLoca			,
				orowLocaStockP 			,
				oSerchOK
			);
			EXIT WHEN oSerchOK = 1;
			ROLLBACK;	--ロック解放
		END LOOP;
		CLOSE curLocaStockP;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iSkuCD = [' || iSkuCD || '], iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iLocaBlockCD = [' || iLocaBlockCD || '], oSerchOK = [' || oSerchOK || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN CLOSE curLocaStockP; END IF;
			RAISE;
	END PcsPalletCapa;
	/************************************************************************************/
	/*	ＰＣＳ検索・完パレでない（パレット積載数同一）									*/
	/************************************************************************************/
	PROCEDURE PcsNotPalletCapa(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iPalletCapacityNR	IN	NUMBER					, --Add
		iLocaBlockCD		IN	VARCHAR2				,
		iShipVolumeCD		IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP	 	OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'PcsNotPalletCapa';
		CURSOR curLocaStockP(
			SkuCd				TA_LOCASTOCKP.SKU_CD%TYPE,
			LotTx				TA_LOCASTOCKP.LOT_TX%TYPE,
			TypeStockCD			TA_LOCASTOCKP.TYPESTOCK_CD%TYPE,
			PalletCapacityNR	TA_LOCASTOCKP.PALLETCAPACITY_NR%TYPE,
			TypeLocaBlockCd		MB_TYPELOCABLOCK.TYPELOCABLOCK_CD%TYPE,
			ShipVolumeCD		MA_LOCA.SHIPVOLUME_CD%TYPE
		) IS
			SELECT
				LSP.LOCA_CD		,
				LSP.SKU_CD		,
				LSP.ROWID
			FROM
				TA_LOCASTOCKP 		LSP	,
				MA_LOCA 			MLC	,
				MB_TYPELOCA			MTL	,
				MA_SKU				MSK
			WHERE
				LSP.LOCA_CD				= MLC.LOCA_CD			AND
				MLC.TYPELOCA_CD			= MTL.TYPELOCA_CD		AND
				LSP.SKU_CD				= MSK.SKU_CD		 	AND
				LSP.SKU_CD 				= SkuCd 				AND
				LSP.LOT_TX 				= LotTx 				AND
				LSP.TYPESTOCK_CD		= TypeStockCD			AND
				LSP.PALLETCAPACITY_NR	= PalletCapacityNR		AND --Add
				MLC.TYPELOCABLOCK_CD	= TypeLocaBlockCd		AND
				MLC.SHIPVOLUME_CD		= ShipVolumeCD			AND
				LSP.PCS_NR				> 0						AND
				TRUNC( LSP.PCS_NR / MSK.PCSPERCASE_NR / LSP.PALLETCAPACITY_NR ) > 0 AND
				MLC.NOTSHIP_FL			= 0 --出荷引当可
			ORDER BY
				LSP.PALLET_NR		,
				MLC.LINEUNIT_NR DESC,
				MLC.LOCA_CD			;
		rowLocaStockP	curLocaStockP%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iSkuCD = [' || iSkuCD || '], iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iLocaBlockCD = [' || iLocaBlockCD || ']'
		);
		oSerchOK := 0;
		OPEN curLocaStockP(iSkuCD, iLotTX, iTypeStockCD, iPalletCapacityNR, iLocaBlockCD, iShipVolumeCD);
		LOOP
			FETCH curLocaStockP INTO rowLocaStockP;
			EXIT WHEN curLocaStockP%NOTFOUND;
			--ロケーションデータ取得
			PS_LOCASEARCH.GetLocaData(
				rowLocaStockP.ROWID		,
				rowLocaStockP.LOCA_CD	,
				orowLoca				,
				orowTypeLoca			,
				orowLocaStockP 			,
				oSerchOK
			);
			EXIT WHEN oSerchOK = 1;
			ROLLBACK;	--ロック解放
		END LOOP;
		CLOSE curLocaStockP;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iSkuCD = [' || iSkuCD || '], iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iLocaBlockCD = [' || iLocaBlockCD || '], oSerchOK = [' || oSerchOK || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN CLOSE curLocaStockP; END IF;
			RAISE;
	END PcsNotPalletCapa;
	/************************************************************************************/
	/*	ＰＣＳ検索・完パレ																*/
	/************************************************************************************/
	PROCEDURE PcsPallet(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iLocaBlockCD		IN	VARCHAR2				,
		iShipVolumeCD		IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP	 	OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'PcsPallet';
		CURSOR curLocaStockP(
			SkuCd				TA_LOCASTOCKP.SKU_CD%TYPE,
			LotTx				TA_LOCASTOCKP.LOT_TX%TYPE,
			TypeStockCD			TA_LOCASTOCKP.TYPESTOCK_CD%TYPE,
			TypeLocaBlockCd		MB_TYPELOCABLOCK.TYPELOCABLOCK_CD%TYPE,
			ShipVolumeCD		MA_LOCA.SHIPVOLUME_CD%TYPE
		) IS
			SELECT
				LSP.LOCA_CD		,
				LSP.SKU_CD		,
				LSP.ROWID
			FROM
				TA_LOCASTOCKP 		LSP	,
				MA_LOCA 			MLC	,
				MB_TYPELOCA			MTL	
			WHERE
				LSP.LOCA_CD				= MLC.LOCA_CD			AND
				MLC.TYPELOCA_CD			= MTL.TYPELOCA_CD		AND
				LSP.SKU_CD 				= SkuCd 				AND
				LSP.LOT_TX 				= LotTx 				AND
				LSP.TYPESTOCK_CD		= TypeStockCD			AND
				MLC.TYPELOCABLOCK_CD	= TypeLocaBlockCd		AND
				MLC.SHIPVOLUME_CD		= ShipVolumeCD			AND
				LSP.PCS_NR				> 0						AND
				LSP.CASE_NR				= 0						AND
				LSP.BARA_NR				= 0						AND
				MLC.NOTSHIP_FL			= 0 --出荷引当可
			ORDER BY
				LSP.PALLET_NR		,
				MLC.LINEUNIT_NR DESC,
				MLC.LOCA_CD			;
		rowLocaStockP	curLocaStockP%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iSkuCD = [' || iSkuCD || '], iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iLocaBlockCD = [' || iLocaBlockCD || ']'
		);
		oSerchOK := 0;
		OPEN curLocaStockP(iSkuCD, iLotTX, iTypeStockCD, iLocaBlockCD, iShipVolumeCD);
		LOOP
			FETCH curLocaStockP INTO rowLocaStockP;
			EXIT WHEN curLocaStockP%NOTFOUND;
			--ロケーションデータ取得
			PS_LOCASEARCH.GetLocaData(
				rowLocaStockP.ROWID		,
				rowLocaStockP.LOCA_CD	,
				orowLoca				,
				orowTypeLoca			,
				orowLocaStockP 			,
				oSerchOK
			);
			EXIT WHEN oSerchOK = 1;
			ROLLBACK;	--ロック解放
		END LOOP;
		CLOSE curLocaStockP;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iSkuCD = [' || iSkuCD || '], iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iLocaBlockCD = [' || iLocaBlockCD || '], oSerchOK = [' || oSerchOK || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN CLOSE curLocaStockP; END IF;
			RAISE;
	END PcsPallet;
	/************************************************************************************/
	/*	ＰＣＳ検索・完パレでない														*/
	/************************************************************************************/
	PROCEDURE PcsNotPallet(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iLocaBlockCD		IN	VARCHAR2				,
		iShipVolumeCD		IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP	 	OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'PcsNotPallet';
		CURSOR curLocaStockP(
			SkuCd				TA_LOCASTOCKP.SKU_CD%TYPE,
			LotTx				TA_LOCASTOCKP.LOT_TX%TYPE,
			TypeStockCD			TA_LOCASTOCKP.TYPESTOCK_CD%TYPE,
			TypeLocaBlockCd		MB_TYPELOCABLOCK.TYPELOCABLOCK_CD%TYPE,
			ShipVolumeCD		MA_LOCA.SHIPVOLUME_CD%TYPE
		) IS
			SELECT
				LSP.LOCA_CD		,
				LSP.SKU_CD		,
				LSP.ROWID
			FROM
				TA_LOCASTOCKP 		LSP	,
				MA_LOCA 			MLC	,
				MB_TYPELOCA			MTL	
			WHERE
				LSP.LOCA_CD				= MLC.LOCA_CD			AND
				MLC.TYPELOCA_CD			= MTL.TYPELOCA_CD		AND
				LSP.SKU_CD 				= SkuCd 				AND
				LSP.LOT_TX 				= LotTx 				AND
				LSP.TYPESTOCK_CD		= TypeStockCD			AND
				MLC.TYPELOCABLOCK_CD	= TypeLocaBlockCd		AND
				MLC.SHIPVOLUME_CD		= ShipVolumeCD			AND
				LSP.PCS_NR				> 0						AND
				MLC.NOTSHIP_FL			= 0 --出荷引当可
			ORDER BY
				LSP.PALLET_NR		,
				MLC.LINEUNIT_NR DESC,
				MLC.LOCA_CD			;
		rowLocaStockP	curLocaStockP%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iSkuCD = [' || iSkuCD || '], iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iLocaBlockCD = [' || iLocaBlockCD || ']'
		);
		oSerchOK := 0;
		OPEN curLocaStockP(iSkuCD, iLotTX, iTypeStockCD, iLocaBlockCD, iShipVolumeCD);
		LOOP
			FETCH curLocaStockP INTO rowLocaStockP;
			EXIT WHEN curLocaStockP%NOTFOUND;
			--ロケーションデータ取得
			PS_LOCASEARCH.GetLocaData(
				rowLocaStockP.ROWID		,
				rowLocaStockP.LOCA_CD	,
				orowLoca				,
				orowTypeLoca			,
				orowLocaStockP 			,
				oSerchOK
			);
			EXIT WHEN oSerchOK = 1;
			ROLLBACK;	--ロック解放
		END LOOP;
		CLOSE curLocaStockP;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iSkuCD = [' || iSkuCD || '], iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iLocaBlockCD = [' || iLocaBlockCD || '], oSerchOK = [' || oSerchOK || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN CLOSE curLocaStockP; END IF;
			RAISE;
	END PcsNotPallet;
	/************************************************************************************/
	/*	ＰＣＳ検索・補充																*/
	/************************************************************************************/
	PROCEDURE PcsSupp(
		iSkuCD				IN	VARCHAR2				,
		iLotTX				IN	VARCHAR2				,
		iTypeStockCD		IN	VARCHAR2				,
		iLocaBlockCD		IN	VARCHAR2				,
		iShipVolumeCD		IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP	 	OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'PcsSupp';
		CURSOR curLocaStockP(
			SkuCd				TA_LOCASTOCKP.SKU_CD%TYPE,
			LotTx				TA_LOCASTOCKP.LOT_TX%TYPE,
			TypeStockCD			TA_LOCASTOCKP.TYPESTOCK_CD%TYPE,
			TypeLocaBlockCd		MB_TYPELOCABLOCK.TYPELOCABLOCK_CD%TYPE,
			ShipVolumeCD		MA_LOCA.SHIPVOLUME_CD%TYPE
		) IS
			SELECT
				LSP.LOCA_CD		,
				LSP.SKU_CD		,
				LSP.ROWID
			FROM
				TA_LOCASTOCKP 		LSP	,
				MA_LOCA 			MLC	,
				MB_TYPELOCA			MTL	
			WHERE
				LSP.LOCA_CD				= MLC.LOCA_CD			AND
				MLC.TYPELOCA_CD			= MTL.TYPELOCA_CD		AND
				LSP.SKU_CD 				= SkuCd 				AND
				LSP.LOT_TX 				= LotTx 				AND
				LSP.TYPESTOCK_CD		= TypeStockCD			AND
				MLC.TYPELOCABLOCK_CD	= TypeLocaBlockCd		AND
				MLC.SHIPVOLUME_CD		= ShipVolumeCD			AND
				LSP.PCS_NR				> 0						AND
				MLC.NOTSHIP_FL			= 0 --出荷引当可
			ORDER BY
				LSP.PALLET_NR		,
				MLC.LINEUNIT_NR		,
				MLC.LOCA_CD			;
		rowLocaStockP	curLocaStockP%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iSkuCD = [' || iSkuCD || '], iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iLocaBlockCD = [' || iLocaBlockCD || ']'
		);
		oSerchOK := 0;
		OPEN curLocaStockP(iSkuCD, iLotTX, iTypeStockCD, iLocaBlockCD, iShipVolumeCD);
		LOOP
			FETCH curLocaStockP INTO rowLocaStockP;
			EXIT WHEN curLocaStockP%NOTFOUND;
			--ロケーションデータ取得
			PS_LOCASEARCH.GetLocaData(
				rowLocaStockP.ROWID		,
				rowLocaStockP.LOCA_CD	,
				orowLoca				,
				orowTypeLoca			,
				orowLocaStockP 			,
				oSerchOK
			);
			EXIT WHEN oSerchOK = 1;
			ROLLBACK;	--ロック解放
		END LOOP;
		CLOSE curLocaStockP;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iSkuCD = [' || iSkuCD || '], iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iLocaBlockCD = [' || iLocaBlockCD || '], oSerchOK = [' || oSerchOK || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curLocaStockP%ISOPEN THEN CLOSE curLocaStockP; END IF;
			RAISE;
	END PcsSupp;
	/************************************************************************************/
	/*	ロケーションデータ取得															*/
	/************************************************************************************/
	PROCEDURE GetLocaData(
		iRowID				IN	UROWID					,
		iLocaCD				IN	VARCHAR2				,
		orowLoca			OUT	MA_LOCA%ROWTYPE			,
		orowTypeLoca		OUT	MB_TYPELOCA%ROWTYPE		,
		orowLocaStockP	 	OUT	TA_LOCASTOCKP%ROWTYPE	,
		oSerchOK			OUT	NUMBER
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetLocaData';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iLocaCD=[' || iLocaCD || ']'
		);
		oSerchOK := 0;
		-- ロケーションロック取得
		PS_LOCA.GetLocaLock(iLocaCD);
		-- ロケーション取得
		PS_MASTER.GetLoca(iLocaCD,orowLoca);
		-- ロケーションタイプ取得
		PS_MASTER.GetTypeLoca(orowLoca.TYPELOCA_CD,orowTypeLoca);

		--IF orowTypeLoca.LOCANOTSHIP_FL = 0 THEN
			BEGIN
				-- 引当可能在庫ロック取得
				PS_LOCA.GetLockLocaStockProwid(iRowID,orowLocaStockP);
				IF orowLocaStockP.PCS_NR > 0 THEN
					oSerchOK := 1;
				END IF;
			EXCEPTION
				--引当対象在庫のロックが取得できなかった場合
				--発生頻度が稀と思われるので、今回は異常終了させる
--				WHEN PS_DEFINE.EX_DATA_NOTFOUND THEN
--					ROLLBACK;
				WHEN OTHERS THEN
					RAISE;
			END;
		--END IF;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iLocaCD=[' || iLocaCD || '], oSerchOK=[' || oSerchOK || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetLocaData;

END PS_LOCASEARCH;
/
SHOW ERRORS
