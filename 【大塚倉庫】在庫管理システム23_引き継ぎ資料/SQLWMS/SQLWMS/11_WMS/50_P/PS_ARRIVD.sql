-- Copyright (C) Seaos Corporation 2014 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_ARRIV AS 
	/************************************************************************************/
	/*	入荷前情報取得																	*/
	/************************************************************************************/
	PROCEDURE GetArrivCount(
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iArrivplanDT		IN DATE		,
		iShipVolumeCD		IN VARCHAR2	,
		oNumLineUnit		OUT NUMBER	
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetArrivCount';

		CURSOR curArrivCount(
			iSkuCD	TA_ARRIV.SKU_CD%TYPE,
			iLotTX	TA_ARRIV.LOT_TX%TYPE
		) IS
			SELECT
				TA_ARRIV.ARRIV_ID					,
				TA_ARRIV.CASEPERPALLET_NR			,
				TA_ARRIV.STACKINGNUMBER_NR			
			FROM
				TA_ARRIV
			WHERE
				STARRIV_ID	<= PS_DEFINE.ST_ARRIV3_ARRIV_E 	AND
				STARRIV_ID	>= PS_DEFINE.ST_ARRIV0_NOTSEND	AND
				SKU_CD		= iSkuCD		AND
				LOT_TX		= iLotTX;
		rowArrivCount			curArrivCount%ROWTYPE;
		rowSku					MA_SKU%ROWTYPE;							--商品マスタ
		numArrivPcsNR			number;									--当日の残入荷予定件数
		numMaxLineUnit			MA_LOCA.LINEUNIT_NR%TYPE;				--ライニユニットの上限
		numNestanerStockRate	NUMBER;									--ネステナの格納比率
		numCaseStockRate		NUMBER;									--ケース成りエリアの格納比率
		numPalletStockRate		NUMBER;									--パレット成りエリアの格納比率
		numPcsNR				NUMBER;									--総在庫数
		numArrivplanLineUnit	NUMBER;									--入荷予定ラインユニット
		endFL					int;									--当日の残入荷予定の有無
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' iSkuCD:'||iSkuCD);
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' iLotTX:'||iLotTX);
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' iArrivplanDT:'||iArrivplanDT);
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' iShipVolumeCD:'||iShipVolumeCD);
		
		OPEN curArrivCount(iSkuCD,iLotTX);
		
		LOOP
			FETCH curArrivCount INTO rowArrivCount;
			EXIT WHEN curArrivCount%NOTFOUND;
			EXIT WHEN endFL = 1;
			endFL := 1;
		END LOOP;
		
		-- 商品マスタ取得
		PS_MASTER.GetSku(
			iSkuCD,
			rowSku
		);
		-- 在庫比率取得（引当可能、引当済（格納待ち）の比率を確認）
		PS_LOCA.GetLocaStockRate(
			iSkuCD				,
			iLotTX				,
			numNestanerStockRate,
			numCaseStockRate	,
			numPalletStockRate	,
			numPcsNR			
		);
		
		IF  endFL = 1 THEN
			-- 入荷予定ピース数を取得する
			SELECT
				SUM(ARRIVPLANPCS_NR)
			INTO
				numArrivPcsNR
			FROM
				TA_ARRIV TA
			WHERE
				STARRIV_ID	<= PS_DEFINE.ST_ARRIV3_ARRIV_E 	AND
				STARRIV_ID	>= PS_DEFINE.ST_ARRIV0_NOTSEND	AND
				SKU_CD		= iSkuCD		AND
				LOT_TX		= iLotTX		
			GROUP BY
				SKU_CD				,
				LOT_TX				
			;
			PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
			
			--ラインユニット数の上限を取得する
			SELECT 
				MAX(LINEUNIT_NR)
			INTO
				numMaxLineUnit
			FROM
				MA_LOCA
			WHERE
				NOTUSE_FL = 0
			;
			
			-- ケース成りエリアの場合
			IF ( iShipVolumeCD = PS_DEFINE.TYPE_SHIPVOLUME1_CASE) THEN
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' getting lineunit for caseship area ');
				-- ((在庫数 + 入荷予定数) * ケース成り割合 - ケース成りエリアの在庫数)/(入数 * パレット積み付け数 * 積み付け段数)
				numArrivplanLineUnit := ceil(((numPcsNR + numArrivPcsNR) * rowSku.CASESTOCKRATE_NR - numPcsNR * numCaseStockRate ) / (rowSku.PCSPERCASE_NR * rowArrivCount.CASEPERPALLET_NR *rowArrivCount.STACKINGNUMBER_NR));
				
				IF numArrivplanLineUnit >= numMaxLineUnit THEN
					oNumLineUnit := numMaxLineUnit;
				ELSE
					oNumLineUnit := numArrivplanLineUnit;
				END IF;
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' numArrivplanLineUnit:'||numArrivplanLineUnit);
			-- パレット成りエリアの場合
			ELSIF ( iShipVolumeCD = PS_DEFINE.TYPE_SHIPVOLUME2_PALLET) THEN
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' getting lineunit for palletship area ');
				-- ((在庫数 + 入荷予定数) * パレット成り割合 - パレット成りエリアの在庫数)/(入数 * パレット積み付け数 * 積み付け段数)
				numArrivplanLineUnit := floor(((numPcsNR + numArrivPcsNR) * (1 - rowSku.CASESTOCKRATE_NR - rowSku.NESTAINERINSTOCKRATE_NR) - numPcsNR * numPalletStockRate ) / (rowSku.PCSPERCASE_NR * rowArrivCount.CASEPERPALLET_NR *rowArrivCount.STACKINGNUMBER_NR));
				IF numArrivplanLineUnit >= numMaxLineUnit THEN
					oNumLineUnit := numMaxLineUnit;
				ELSE
					oNumLineUnit := numArrivplanLineUnit;
				END IF;
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' numArrivplanLineUnit:'||numArrivplanLineUnit);
			-- ネステナの場合=ラインユニットは必ず1
			ELSE
				PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' getting lineunit for nestainership area ');
				oNumLineUnit := 1;
			END IF;
		ELSE
			PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' getting lineunit for not planned lot_tx ');
			oNumLineUnit := 1;
		END IF;
		
		CLOSE curArrivCount;
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' oNumLineUnit:' || oNumLineUnit);
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			CLOSE curArrivCount;
			PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
		RAISE;
	END GetArrivCount;
END PS_ARRIV;
/
SHOW ERRORS
