-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PW_SHIP IS
	/************************************************************************************/
	/*																					*/
	/*									Public											*/
	/*																					*/
	/************************************************************************************/
	/************************************************************************************/
	/*	出荷引当																		*/
	/************************************************************************************/
	PROCEDURE ApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyShip';
		
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || '], ' || 'iUserCD = [' || iUserCD || ']'
		);
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD, rowUser);
		-- 引当状態確認
		PS_STATUS.ChkApply;
		-- 引当状態更新
		PS_STATUS.SetShipApply;
		COMMIT;
		BEGIN
--			-- マスターチェック（不整合）TODO
--			PS_APPLY.ChkMaster;
			-- 出荷ステータスチェック
			PS_SHIP.ChkApplyShip(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
			-- 出荷状態開始更新（出荷引当用一時テーブル作成）
			PS_SHIP.StartApplyShip(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
--			-- マスターチェック（データとの紐付き）TODO
--			PS_APPLY.ChkMaster2(iTrnID);
--			-- 引当
			PS_APPLYSHIP.ApplyShipMain(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
			-- ピッキング指示番号付与
			PS_APPLYPICK.ApplyPickMain(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
			-- 緊急補充引当
			PS_APPLYSUPP.ApplyESupp(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
			-- 補充指示書有効
			PS_SUPP.SuppDataOK(iTrnID);
			-- ピック指示書有効
			PS_PICK.PickDataOK(iTrnID);
			-- 出荷状態完了更新
			PS_SHIP.EndApplyShip(iTrnID, iUserCD, rowUser.USER_TX, iProgramCD);
			-- 引当状態更新
			PS_STATUS.RstShipApply;
			COMMIT;
		EXCEPTION
			WHEN OTHERS THEN
				ROLLBACK;
				-- 引当状態更新
				PS_STATUS.RstShipApply;
				COMMIT;
				RAISE;
		END;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || '], ' || 'iUserCD = [' || iUserCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyShip;
	/************************************************************************************/
	/*	出荷引当取消																	*/
	/************************************************************************************/
	PROCEDURE CanApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CanApplyShip';
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID=[' || iTrnID || '], iUserCD=[' || iUserCD || ']'
		);
		
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID=[' || iTrnID || '], iUserCD=[' || iUserCD || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END CanApplyShip;
	/************************************************************************************/
	/*	出荷キャンセル																	*/
	/************************************************************************************/
	PROCEDURE CanShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'CanShip';
		-- 対象オーダー
		CURSOR curTarget(TrnID TMP_SELECTID.TRN_ID%TYPE) IS
			SELECT
				TSH.ORDERNO_CD	,
				TSH.SHIPH_ID	,
				TSH.STSHIP_ID
			FROM
				TMP_SELECTID	TMP,
				TA_SHIPH		TSH
			WHERE
				TMP.SELECT_ID	= TSH.SHIPH_ID AND
				TMP.TRN_ID		= TrnID;
		rowTarget		curTarget%ROWTYPE;
		
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || '], iUserCD = [' || iUserCD || ']'
		);
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD,rowUser);
		-- 選択されたオーダー単位に処理
		OPEN curTarget(iTrnID);
		LOOP
			FETCH curTarget INTO rowTarget;
			EXIT WHEN curTarget%NOTFOUND;
			-- 出荷ステータス確認：取消でなく、引当前であること
			IF rowTarget.STSHIP_ID NOT IN (PS_DEFINE.ST_SHIP0_NOTAPPLY, PS_DEFINE.ST_SHIP2_SFAILITEM) THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'未引当、または欠品以外のデータが選択されています。' || 
					'受注番号 = [' || rowTarget.ORDERNO_CD || ']'
				);
			END IF;
			--
			UPDATE TA_SHIPH
			SET
				START_DT		= NULL			,
				END_DT			= NULL			,
				STSHIP_ID		= PS_DEFINE.ST_SHIPM2_CANCEL,
				--
				UPD_DT			= SYSDATE		,
				UPDUSER_CD		= iUserCD		,
				UPDUSER_TX		= rowUser.USER_TX,
				UPDPROGRAM_CD	= iProgramCD	,
				UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
			WHERE
				SHIPH_ID		= rowTarget.SHIPH_ID;
		END LOOP;
		CLOSE curTarget;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTarget%ISOPEN THEN CLOSE curTarget; END IF;
			RAISE;
	END CanShip;
	/************************************************************************************/
	/*	出荷キャンセル解除																*/
	/************************************************************************************/
	PROCEDURE RstCanShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'RstCanShip';
		-- 対象オーダー
		CURSOR curTarget(TrnID TMP_SELECTID.TRN_ID%TYPE) IS
			SELECT
				TSH.ORDERNO_CD	,
				TSH.SHIPH_ID	,
				TSH.STSHIP_ID
			FROM
				TMP_SELECTID	TMP,
				TA_SHIPH		TSH
			WHERE
				TMP.SELECT_ID	= TSH.SHIPH_ID AND
				TMP.TRN_ID		= TrnID;
		rowTarget		curTarget%ROWTYPE;
		
		rowUser			VA_USER%ROWTYPE;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || '], iUserCD = [' || iUserCD || ']'
		);
		-- マスタ取得
		PS_MASTER.GetUser(iUserCD,rowUser);
		-- 選択されたオーダー単位に処理
		OPEN curTarget(iTrnID);
		LOOP
			FETCH curTarget INTO rowTarget;
			EXIT WHEN curTarget%NOTFOUND;
			-- 出荷ステータス確認：取消であること
			IF rowTarget.STSHIP_ID != PS_DEFINE.ST_SHIPM2_CANCEL THEN
				RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS,'キャンセル以外のデータが選択されています。' || 
					'受注番号 = [' || rowTarget.ORDERNO_CD || ']'
				);
			END IF;
			--
			UPDATE TA_SHIPH
			SET
				START_DT		= NULL			,
				END_DT			= NULL			,
				STSHIP_ID		= PS_DEFINE.ST_SHIP0_NOTAPPLY,
				--
				UPD_DT			= SYSDATE		,
				UPDUSER_CD		= iUserCD		,
				UPDUSER_TX		= rowUser.USER_TX,
				UPDPROGRAM_CD	= iProgramCD	,
				UPDCOUNTER_NR	= UPDCOUNTER_NR + 1
			WHERE
				SHIPH_ID		= rowTarget.SHIPH_ID;
		END LOOP;
		CLOSE curTarget;
		COMMIT;
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curTarget%ISOPEN THEN CLOSE curTarget; END IF;
			RAISE;
	END RstCanShip;
END PW_SHIP;
/
SHOW ERRORS
