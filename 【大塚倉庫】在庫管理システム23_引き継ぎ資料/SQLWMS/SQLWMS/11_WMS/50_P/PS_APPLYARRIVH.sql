-- Copyright (C) Seaos Corporation 2014 All rights reserved
CREATE OR REPLACE PACKAGE PS_APPLYARRIV IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PS_APPLYARRIV';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	/************************************************************/
	/*	入荷予定消込											*/
	/************************************************************/
	PROCEDURE ApplyArriv(
		iTrnID				IN NUMBER			,
		irowSku				IN MA_SKU%ROWTYPE	,
		irowArriv			IN TA_ARRIV%ROWTYPE	,
		iPcsNR				IN NUMBER			, --計上数
		iUserCD				IN VARCHAR2			,
		iUserTX				IN VARCHAR2			,
		iProgramCD			IN VARCHAR2			
	);
	/************************************************************/
	/*	入荷予定分割											*/
	/************************************************************/
	PROCEDURE DevArriv(
		iTrnID				IN NUMBER			,
		irowArriv			IN TA_ARRIV%ROWTYPE	,
		iUserCD				IN VARCHAR2			,
		iUserTX				IN VARCHAR2			,
		iProgramCD			IN VARCHAR2			
	);
END PS_APPLYARRIV;
/
SHOW ERRORS
