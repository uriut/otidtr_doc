-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PW_STOCK IS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PW_STOCK';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************************************/
	/*	在庫追加																		*/
	/************************************************************************************/
	--空きロケへの在庫追加
	PROCEDURE InsStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iCASEPERPALLET_NR	IN NUMBER	,
		iPALLETLAYER_NR		IN NUMBER	,
		iPALLETCAPACITY_NR	IN NUMBER	,
		iSTACKINGNUMBER_NR	IN NUMBER	,
		iAMOUNT_NR			IN NUMBER	,
		iTypeOperationCD	IN VARCHAR2	,
		iMemoTX				IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2
	);
	/************************************************************************************/
	/*	在庫修正																		*/
	/************************************************************************************/
	--既存ロケへの在庫修正
	PROCEDURE UpdStock(
		iLocaCD 			IN VARCHAR2	,
		iSkuCD 				IN VARCHAR2	,
		iLotTX				IN VARCHAR2	,
		iKanbanCD			IN VARCHAR2	,
		iPalletNR			IN NUMBER	,
		iCaseNR				IN NUMBER	,
		iBaraNR				IN NUMBER	,
		iPcsNR				IN NUMBER	,
		iTypeOperationCD	IN VARCHAR2	,
		iMemoTX				IN VARCHAR2	,
		iUserCD				IN VARCHAR2	,
		iProgramCD			IN VARCHAR2	
	);
END PW_STOCK;
/
SHOW ERRORS
