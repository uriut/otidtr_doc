-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PS_APPLYSHIP AS

	PROCEDURE ApplyShipMain(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyShipMain';

		numApplyFL		NUMBER := 1;
	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		WHILE ( numApplyFL = 1 ) LOOP

			--完パレット引当
			PS_APPLYSHIP.ApplyShipOnePallet(
				iTrnID					,
				iUserCD					,
				iUserTX					,
				iProgramCD
			);

			--集約パレット引当
			PS_APPLYSHIP.ApplyShipSumPallet(
				iTrnID					,
				iUserCD					,
				iUserTX					,
				iProgramCD
			);

			--ケース引当
			PS_APPLYSHIP.ApplyShipCase(
				iTrnID					,
				iUserCD					,
				iUserTX					,
				iProgramCD
			);

			--再引当チェック
			PS_APPLYSHIP.ReApplyMain(
				iTrnID					,
				iUserCD					,
				iUserTX					,
				iProgramCD				,
				numApplyFL --集約解除の場合は再引当
			);

		END LOOP;

		--引当結果チェック
		PS_APPLYSHIP.ApplyShipCheck(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD
		);

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyShipMain;

	PROCEDURE ApplyShipCheck(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyShipCheck';

		CURSOR curCheck(
			TrnID	TA_PICK.TRN_ID%TYPE
		) IS
			SELECT
				TB.SHIPD_ID,  
				TB.SKU_CD,    
				TB.LOT_TX,    
				TB.SKU_TX,    
				TB.ORDERPCS_NR,
				TA.PCS_NR
			FROM
				(SELECT
					SHIPD_ID,
					SUM(PCS_NR) PCS_NR
				FROM
					TA_PICK
				WHERE
					TRN_ID = TrnID
				GROUP BY
					SHIPD_ID) TA
				INNER JOIN (
					SELECT
						SHIPD_ID,
						SKU_CD,
						LOT_TX,
						SKU_TX,
						ORDERPCS_NR
					FROM
						TA_SHIPD) TB
				ON TA.SHIPD_ID = TB.SHIPD_ID
			WHERE
				TA.PCS_NR <> TB.ORDERPCS_NR
			ORDER BY
				TB.SHIPD_ID;
				
		rowCheck		curCheck%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curCheck(iTrnID);
		LOOP
			FETCH curCheck INTO rowCheck;
			EXIT WHEN curCheck%NOTFOUND;

			--出荷指示とピッキング指示が不一致となりました。
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：出荷指示とピッキング指示が不一致となりました。' ||
																'SHIPD_ID = [' || rowCheck.SHIPD_ID || '], ' ||
																'SKU_CD = [' || rowCheck.SKU_CD || '], ' ||
																'LOT_TX = [' || rowCheck.LOT_TX || '], ' ||
																'SKU_TX = [' || rowCheck.SKU_TX || '], ' ||
																'ORDERPCS_NR = [' || rowCheck.ORDERPCS_NR || '], ' ||
																'PICKPCS_NR = [' || rowCheck.PCS_NR || ']');
		END LOOP;
		CLOSE curCheck;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyShipCheck;

	PROCEDURE ApplyShipOnePallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyShipOnePallet';

		CURSOR curOnePallet(
			TrnID	TMP_APPLYSHIP.TRN_ID%TYPE
		) IS
			SELECT
				TSH.ORDERDATE_DT,
				TMP.SHIPH_ID,	
				TMP.SHIPD_ID,
				TMP.SKU_CD,      
				TMP.LOT_TX,      
				TMP.TYPESTOCK_CD,
				TSD.PALLETCAPACITY_NR,
				TMP.ORDERPCS_NR - TMP.APPLYPCS_NR AS PCS_NR,
				NVL(MTP.TOTALPIC_FL, 1) AS TOTALPIC_FL
			FROM
				TMP_APPLYSHIP	TMP,
				TA_SHIPH		TSH,
				TA_SHIPD		TSD,
				MA_SKU			MSK,
				MA_TRANSPORTER	MTP
			WHERE
				TMP.TRN_ID			= TrnID					AND
				TMP.SHIPH_ID		= TSH.SHIPH_ID			AND
				TMP.SHIPD_ID		= TSD.SHIPD_ID			AND
				TMP.SKU_CD			= MSK.SKU_CD			AND
				TSH.TRANSPORTER2_CD = MTP.TRANSPORTER_CD(+) AND
				TMP.ORDERPCS_NR - TMP.APPLYPCS_NR > 0		AND
				TMP.SHORT_FL	= 0							AND
				TRUNC( TRUNC( ( TMP.ORDERPCS_NR - TMP.APPLYPCS_NR ) / MSK.PCSPERCASE_NR ) / TSD.PALLETCAPACITY_NR ) > 0
			ORDER BY
				NVL(MTP.PICKORDER_NR, 0),
				TSH.TRANSPORTER2_CD, --配送業者別
				TMP.SKU_CD		   ,      
				TMP.LOT_TX		   ,      
				TMP.TYPESTOCK_CD   ;

		rowOnePallet		curOnePallet%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curOnePallet(iTrnID);
		LOOP
			FETCH curOnePallet INTO rowOnePallet;
			EXIT WHEN curOnePallet%NOTFOUND;

			PLOG.DEBUG(logCtx, 'SHIPD_ID = [' || rowOnePallet.SHIPD_ID || ']');

			ApplyStockOnePallet(
				iTrnID                         , 
				rowOnePallet.SHIPD_ID		   ,
				' '							   , --パレット明細番号
				rowOnePallet.SKU_CD            , 
				rowOnePallet.LOT_TX            , 
				rowOnePallet.TYPESTOCK_CD      , 
				rowOnePallet.PALLETCAPACITY_NR , 
				rowOnePallet.PCS_NR            , 
				rowOnePallet.TOTALPIC_FL	   , --1:トータルピック
				iUserCD                        , 
				iUserTX                        , 
				iProgramCD					   
			);

		END LOOP;
		CLOSE curOnePallet;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyShipOnePallet;


	PROCEDURE ApplyShipSumPallet(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyShipSumPallet';

		CURSOR curOrder(                                  
			TrnID	TMP_APPLYSHIP.TRN_ID%TYPE             
		) IS                                              
			SELECT                                        
				TMP.SKU_CD,                                   
				TMP.LOT_TX,                                   
				TMP.TYPESTOCK_CD                              
			FROM                                          
				TMP_APPLYSHIP TMP
			WHERE                                         
				TMP.TRN_ID = TrnID	AND                       
				TMP.ORDERPCS_NR - TMP.APPLYPCS_NR > 0 AND 
				TMP.SHORT_FL = 0
			GROUP BY                                      
				TMP.SKU_CD,                                   
				TMP.LOT_TX,                                   
				TMP.TYPESTOCK_CD                              
			ORDER BY                                      
				TMP.SKU_CD,                                   
				TMP.LOT_TX,                                   
				TMP.TYPESTOCK_CD;                             
														  
		rowOrder	curOrder%ROWTYPE;                     

		CURSOR curStock(
			SkuCD		TA_LOCASTOCKP.SKU_CD%TYPE,
			LotTX		TA_LOCASTOCKP.LOT_TX%TYPE,
			TypeStockCD	TA_LOCASTOCKP.TYPESTOCK_CD%TYPE
		) IS
			SELECT
				PALLETCAPACITY_NR
			FROM
				TA_LOCASTOCKP
			WHERE
				SKU_CD		 = SkuCD	   AND
				LOT_TX		 = LotTX	   AND
				TYPESTOCK_CD = TypeStockCD AND
				PCS_NR 		 > 0
			GROUP BY
				PALLETCAPACITY_NR
			ORDER BY
				PALLETCAPACITY_NR DESC;

		rowStock	curStock%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		--出荷指示のパレット積載数で引当
		PS_APPLYSHIP.ApplyShipSumPalletOrder( 
			iTrnID		   ,                  
			iUserCD		   ,                  
			iUserTX		   ,                  
			iProgramCD                        
		);                                    

		--在庫のパレット積載数で引当
		OPEN curOrder(iTrnID);
		LOOP               
			FETCH curOrder INTO rowOrder; 
			EXIT WHEN curOrder%NOTFOUND;        

			PLOG.DEBUG(logCtx, 'SKU_CD = [' || rowOrder.SKU_CD || '], ' ||
							   'LOT_TX = [' || rowOrder.LOT_TX || '], ' ||
							   'TYPESTOCK_CD = [' || rowOrder.TYPESTOCK_CD || ']');

			OPEN curStock(rowOrder.SKU_CD, rowOrder.LOT_TX, rowOrder.TYPESTOCK_CD);                  
			LOOP                                          
				FETCH curStock INTO rowStock; 
				EXIT WHEN curStock%NOTFOUND;        

				PLOG.DEBUG(logCtx, 'PALLETCAPACITY_NR = [' || rowStock.PALLETCAPACITY_NR || ']');
														  
				PS_APPLYSHIP.ApplyShipSumPalletStock(     
					iTrnID					   ,                      
					rowOrder.SKU_CD			   ,
					rowOrder.LOT_TX			   ,
					rowOrder.TYPESTOCK_CD	   ,
					rowStock.PALLETCAPACITY_NR ,
					iUserCD					   ,                      
					iUserTX					   ,                      
					iProgramCD                            
				);                                        
														  
			END LOOP;                                     
			CLOSE curStock;                         

		END LOOP;                                     
		CLOSE curOrder;                         

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyShipSumPallet;

	PROCEDURE ApplyShipSumPalletOrder(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyShipSumPalletOrder';

		CURSOR curSumPallet(
			TrnID	TMP_APPLYSHIP.TRN_ID%TYPE
		) IS
			SELECT
				TMP.SKU_CD,      
				TMP.LOT_TX,      
				TMP.TYPESTOCK_CD,
				MSK.PCSPERCASE_NR,
				TSH.TRANSPORTER2_CD, --配送業者別
				TSD.PALLETCAPACITY_NR,
				SUM( TMP.ORDERPCS_NR - TMP.APPLYPCS_NR ) AS PCS_NR,
				' ' AS DTNAME_TX,
				' ' AS DTADDRESS1_TX,
				' ' AS DTADDRESS2_TX,
				' ' AS DTADDRESS3_TX,
				' ' AS DTADDRESS4_TX,
				' ' AS DIRECTDELIVERY_CD,
				' ' AS DELIDATE_TX,      
				NVL(MTP.TOTALPIC_FL, 1)  AS TOTALPIC_FL,
				NVL(MTP.PICKORDER_NR, 0) AS PICKORDER_NR
			FROM
				TMP_APPLYSHIP	TMP,
				TA_SHIPH		TSH,
				TA_SHIPD		TSD,
				MA_SKU			MSK,
				MA_TRANSPORTER	MTP
			WHERE
				TMP.TRN_ID			= TrnID					AND
				TMP.SHIPH_ID		= TSH.SHIPH_ID			AND
				TMP.SHIPD_ID		= TSD.SHIPD_ID			AND
				TMP.SKU_CD			= MSK.SKU_CD			AND
				TSH.TRANSPORTER2_CD = MTP.TRANSPORTER_CD(+) AND
				TMP.ORDERPCS_NR - TMP.APPLYPCS_NR > 0 	 	AND
				TMP.SHORT_FL		= 0						AND
				MTP.TOTALPIC_FL		= 1							--トータルピック対象
			HAVING
				TRUNC( SUM( TRUNC( ( TMP.ORDERPCS_NR - TMP.APPLYPCS_NR ) / MSK.PCSPERCASE_NR ) ) / TSD.PALLETCAPACITY_NR ) > 0
			GROUP BY
				TMP.SKU_CD		   ,      
				TMP.LOT_TX		   ,      
				TMP.TYPESTOCK_CD   ,
				MSK.PCSPERCASE_NR  ,
				MTP.PICKORDER_NR   ,
				TSH.TRANSPORTER2_CD, --配送業者別
				TSD.PALLETCAPACITY_NR,
				MTP.TOTALPIC_FL
			UNION ALL
			SELECT
				TMP.SKU_CD,      
				TMP.LOT_TX,      
				TMP.TYPESTOCK_CD,
				MSK.PCSPERCASE_NR,
				TSH.TRANSPORTER2_CD, --配送業者別
				TSD.PALLETCAPACITY_NR,
				SUM( TMP.ORDERPCS_NR - TMP.APPLYPCS_NR ) AS PCS_NR,
				TSH.DTNAME_TX,
				TSH.DTADDRESS1_TX,
				TSH.DTADDRESS2_TX, 
				TSH.DTADDRESS3_TX, 
				TSH.DTADDRESS4_TX, 
				TSH.DIRECTDELIVERY_CD,
				TSH.DELIDATE_TX,
				NVL(MTP.TOTALPIC_FL, 1)  AS TOTALPIC_FL,
				NVL(MTP.PICKORDER_NR, 0) AS PICKORDER_NR
			FROM
				TMP_APPLYSHIP	TMP,
				TA_SHIPH		TSH,
				TA_SHIPD		TSD,
				MA_SKU			MSK,
				MA_TRANSPORTER	MTP
			WHERE
				TMP.TRN_ID			= TrnID					AND
				TMP.SHIPH_ID		= TSH.SHIPH_ID			AND
				TMP.SHIPD_ID		= TSD.SHIPD_ID			AND
				TMP.SKU_CD			= MSK.SKU_CD			AND
				TSH.TRANSPORTER2_CD = MTP.TRANSPORTER_CD(+) AND
				TMP.ORDERPCS_NR - TMP.APPLYPCS_NR > 0 	 	AND
				TMP.SHORT_FL		= 0						AND
				MTP.TOTALPIC_FL		= 0							--軒先ピック対象
			HAVING
				TRUNC( SUM( TRUNC( ( TMP.ORDERPCS_NR - TMP.APPLYPCS_NR ) / MSK.PCSPERCASE_NR ) ) / TSD.PALLETCAPACITY_NR ) > 0
			GROUP BY
				TMP.SKU_CD		   ,      
				TMP.LOT_TX		   ,      
				TMP.TYPESTOCK_CD   ,
				MSK.PCSPERCASE_NR  ,
				MTP.PICKORDER_NR   ,
				TSH.TRANSPORTER2_CD, --配送業者別
				TSD.PALLETCAPACITY_NR,
				TSH.DTNAME_TX,
				TSH.DTADDRESS1_TX,
				TSH.DTADDRESS2_TX, 
				TSH.DTADDRESS3_TX, 
				TSH.DTADDRESS4_TX, 
				TSH.DIRECTDELIVERY_CD,
				TSH.DELIDATE_TX,
				MTP.TOTALPIC_FL
			ORDER BY
				PICKORDER_NR   ,
				TRANSPORTER2_CD, --配送業者別
				SKU_CD		   ,      
				LOT_TX		   ,      
				TYPESTOCK_CD   ;

		rowSumPallet		curSumPallet%ROWTYPE;
		rowSku				MA_SKU%ROWTYPE;
		rowLoca				MA_LOCA%ROWTYPE;
		rowTypeLoca			MB_TYPELOCA%ROWTYPE;
		rowLocaStockP		TA_LOCASTOCKP%ROWTYPE;
		chrTypeCarryCD		TA_PICK.TYPECARRY_CD%TYPE;
		numPalletDetNoCD	NUMBER := 0;
		numSearchOK			NUMBER := 0;
		numNextSearch		NUMBER := 1;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		--パレット明細仮番号取得
		SELECT
			NVL(MAX(TO_NUMBER(PALLETDETNO_CD)), 0) INTO numPalletDetNoCD
		FROM
			TA_PICK
		WHERE
			TRN_ID = iTrnID AND
			TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_S; --集約

		WHILE ( numNextSearch = 1 ) LOOP

			OPEN curSumPallet(iTrnID);
			FETCH curSumPallet INTO rowSumPallet;
			EXIT WHEN curSumPallet%NOTFOUND;

			PLOG.DEBUG(logCtx, 'SKU_CD = [' || rowSumPallet.SKU_CD || ']');

			PS_MASTER.GetSku(rowSumPallet.SKU_CD, rowSku);

			PS_LOCASEARCH.PalletPcs(
				rowSumPallet.SKU_CD,
				rowSumPallet.LOT_TX,
				rowSumPallet.TYPESTOCK_CD,
				rowSumPallet.PALLETCAPACITY_NR,
				0				, 	-- iPalletNR
				0				, 	-- iCaseNR
				0				, 	-- iBaraNR
				0				, 	-- iPcsNR
				rowSku			,	-- irowSku
				rowLoca			,	-- orowLoca
				rowTypeLoca		,	-- orowTypeLoca
				rowLocaStockP	,	-- orowLocaStockP
				chrTypeCarryCD	,	-- oTypeCarryCD
				numSearchOK			-- oSerchOK
			);
			IF numSearchOK = 1 AND --パレット在庫があるか事前チェック
			   TRUNC( rowLocaStockP.PCS_NR / rowSumPallet.PCSPERCASE_NR / rowLocaStockP.PALLETCAPACITY_NR ) > 0 THEN

				numPalletDetNoCD := numPalletDetNoCD + 1;

				PLOG.DEBUG(logCtx, 'SKU_CD = [' || rowSumPallet.SKU_CD || '], ' ||
								   'LOT_TX = [' || rowSumPallet.LOT_TX || '], ' ||
								   'PALLETCAPACITY_NR = [' || rowLocaStockP.PALLETCAPACITY_NR || '], ' ||
								   'PCS_NR = [' || rowSumPallet.PCS_NR || '], ' ||
								   'TRANSPORTER2_CD = [' || rowSumPallet.TRANSPORTER2_CD || '], ' ||
								   'DTNAME_TX = [' || rowSumPallet.DTNAME_TX || '], ' ||
								   'DTADDRESS1_TX = [' || rowSumPallet.DTADDRESS1_TX || '], ' ||
								   'DTADDRESS2_TX = [' || rowSumPallet.DTADDRESS2_TX || '], ' ||
								   'DTADDRESS3_TX = [' || rowSumPallet.DTADDRESS3_TX || '], ' ||
								   'DTADDRESS4_TX = [' || rowSumPallet.DTADDRESS4_TX || '], ' ||
								   'DIRECTDELIVERY_CD = [' || rowSumPallet.DIRECTDELIVERY_CD || '], ' ||
								   'DELIDATE_TX = [' || rowSumPallet.DELIDATE_TX || '], ' ||
								   'TOTALPIC_FL = [' || rowSumPallet.TOTALPIC_FL || ']');

				IF rowSumPallet.TOTALPIC_FL = 1 THEN
					--トータルピック
					ApplyStockSumPalletTotal(
						iTrnID                         , 
						rowSumPallet.SKU_CD            , 
						rowSumPallet.LOT_TX            , 
						rowSumPallet.TYPESTOCK_CD      , 
						rowSumPallet.PALLETCAPACITY_NR , 
						rowSumPallet.PCS_NR            , 
						rowSumPallet.TRANSPORTER2_CD   ,
						numPalletDetNoCD			   ,
						rowLocaStockP				   ,
						rowSumPallet.TOTALPIC_FL	   , --1:トータルピック
						chrTypeCarryCD				   ,
						iUserCD                        , 
						iUserTX                        , 
						iProgramCD					   
					);
				ELSE
					--軒先ピック
					ApplyStockSumPalletOrder(
						iTrnID                         , 
						rowSumPallet.SKU_CD            , 
						rowSumPallet.LOT_TX            , 
						rowSumPallet.TYPESTOCK_CD      , 
						rowSumPallet.PALLETCAPACITY_NR , 
						rowSumPallet.PCS_NR            , 
						rowSumPallet.TRANSPORTER2_CD   ,
						rowSumPallet.DTNAME_TX		   ,    
						rowSumPallet.DTADDRESS1_TX	   ,
						rowSumPallet.DTADDRESS2_TX	   ,
						rowSumPallet.DTADDRESS3_TX	   ,
						rowSumPallet.DTADDRESS4_TX	   ,
						rowSumPallet.DIRECTDELIVERY_CD , --直送先コード
						rowSumPallet.DELIDATE_TX	   , --着荷指定日
						numPalletDetNoCD			   ,
						rowLocaStockP				   ,
						rowSumPallet.TOTALPIC_FL	   , --1:トータルピック
						chrTypeCarryCD				   ,
						iUserCD                        , 
						iUserTX                        , 
						iProgramCD					   
					);
				END IF;
			ELSE
				numNextSearch := 0;
			END IF;
			CLOSE curSumPallet;

		END LOOP;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyShipSumPalletOrder;


	PROCEDURE ApplyShipSumPalletStock(
		iTrnID				IN	NUMBER	,
		iSkuCD	   			IN	VARCHAR2,
		iLotTX	   			IN	VARCHAR2,
		iTypeStockCD		IN	VARCHAR2,
		iPalletCapacityNR	IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyShipSumPalletStock';

		CURSOR curSumPallet(
			TrnID				TMP_APPLYSHIP.TRN_ID%TYPE,
			SkuCD				TMP_APPLYSHIP.SKU_CD%TYPE,
			LotTX				TMP_APPLYSHIP.LOT_TX%TYPE,
			TypeStockCD			TMP_APPLYSHIP.TYPESTOCK_CD%TYPE,
			PalletCapacityNR	TA_LOCASTOCKP.PALLETCAPACITY_NR%TYPE
		) IS
			SELECT
				TMP.SKU_CD,      
				TMP.LOT_TX,      
				TMP.TYPESTOCK_CD,
				MSK.PCSPERCASE_NR,
				TSH.TRANSPORTER2_CD, --配送業者別
				SUM( TMP.ORDERPCS_NR - TMP.APPLYPCS_NR ) AS PCS_NR,
				' ' AS DTNAME_TX,
				' ' AS DTADDRESS1_TX,
				' ' AS DTADDRESS2_TX,
				' ' AS DTADDRESS3_TX,
				' ' AS DTADDRESS4_TX,
				' ' AS DIRECTDELIVERY_CD,
				' ' AS DELIDATE_TX,      
				NVL(MTP.TOTALPIC_FL, 1)  AS TOTALPIC_FL,
				NVL(MTP.PICKORDER_NR, 0) AS PICKORDER_NR
			FROM
				TMP_APPLYSHIP	TMP,
				TA_SHIPH		TSH,
				MA_SKU			MSK,
				MA_TRANSPORTER	MTP
			WHERE
				TMP.TRN_ID			= TrnID					AND
				TMP.SKU_CD			= SkuCD					AND --add
				TMP.LOT_TX			= LotTX					AND --add
				TMP.TYPESTOCK_CD	= TypeStockCD			AND --add
				TMP.SHIPH_ID		= TSH.SHIPH_ID			AND
				TMP.SKU_CD			= MSK.SKU_CD			AND
				TSH.TRANSPORTER2_CD = MTP.TRANSPORTER_CD(+) AND
				TMP.ORDERPCS_NR	 - TMP.APPLYPCS_NR > 0 	 	AND
				TMP.SHORT_FL	 	= 0						AND
				MTP.TOTALPIC_FL 	= 1							--トータルピック対象
			HAVING
				TRUNC( SUM( TRUNC( ( TMP.ORDERPCS_NR - TMP.APPLYPCS_NR ) / MSK.PCSPERCASE_NR ) ) / PalletCapacityNR ) > 0
			GROUP BY
				TMP.SKU_CD		   ,      
				TMP.LOT_TX		   ,      
				TMP.TYPESTOCK_CD   ,
				MSK.PCSPERCASE_NR  ,
				MTP.PICKORDER_NR   ,
				TSH.TRANSPORTER2_CD, --配送業者別
				MTP.TOTALPIC_FL
			UNION ALL
			SELECT
				TMP.SKU_CD,      
				TMP.LOT_TX,      
				TMP.TYPESTOCK_CD,
				MSK.PCSPERCASE_NR,
				TSH.TRANSPORTER2_CD, --配送業者別
				SUM( TMP.ORDERPCS_NR - TMP.APPLYPCS_NR ) AS PCS_NR,
				TSH.DTNAME_TX,
				TSH.DTADDRESS1_TX,
				TSH.DTADDRESS2_TX, 
				TSH.DTADDRESS3_TX, 
				TSH.DTADDRESS4_TX, 
				TSH.DIRECTDELIVERY_CD,
				TSH.DELIDATE_TX,
				NVL(MTP.TOTALPIC_FL, 1)  AS TOTALPIC_FL,
				NVL(MTP.PICKORDER_NR, 0) AS PICKORDER_NR
			FROM
				TMP_APPLYSHIP	TMP,
				TA_SHIPH		TSH,
				MA_SKU			MSK,
				MA_TRANSPORTER	MTP
			WHERE
				TMP.TRN_ID			= TrnID					AND
				TMP.SKU_CD			= SkuCD					AND --add
				TMP.LOT_TX			= LotTX					AND --add
				TMP.TYPESTOCK_CD	= TypeStockCD			AND --add
				TMP.SHIPH_ID		= TSH.SHIPH_ID			AND
				TMP.SKU_CD			= MSK.SKU_CD			AND
				TSH.TRANSPORTER2_CD = MTP.TRANSPORTER_CD(+) AND
				TMP.ORDERPCS_NR	 - TMP.APPLYPCS_NR > 0 	 	AND
				TMP.SHORT_FL	 	= 0						AND
				MTP.TOTALPIC_FL		= 0							--軒先ピック対象
			HAVING
				TRUNC( SUM( TRUNC( ( TMP.ORDERPCS_NR - TMP.APPLYPCS_NR ) / MSK.PCSPERCASE_NR ) ) / PalletCapacityNR ) > 0
			GROUP BY
				TMP.SKU_CD		   ,      
				TMP.LOT_TX		   ,      
				TMP.TYPESTOCK_CD   ,
				MSK.PCSPERCASE_NR  ,
				MTP.PICKORDER_NR   ,
				TSH.TRANSPORTER2_CD, --配送業者別
				TSH.DTNAME_TX,
				TSH.DTADDRESS1_TX,
				TSH.DTADDRESS2_TX, 
				TSH.DTADDRESS3_TX, 
				TSH.DTADDRESS4_TX, 
				TSH.DIRECTDELIVERY_CD,
				TSH.DELIDATE_TX,
				MTP.TOTALPIC_FL
			ORDER BY
				PICKORDER_NR   ,
				TRANSPORTER2_CD, --配送業者別
				SKU_CD		   ,      
				LOT_TX		   ,      
				TYPESTOCK_CD   ;

		rowSumPallet		curSumPallet%ROWTYPE;
		rowSku				MA_SKU%ROWTYPE;
		rowLoca				MA_LOCA%ROWTYPE;
		rowTypeLoca			MB_TYPELOCA%ROWTYPE;
		rowLocaStockP		TA_LOCASTOCKP%ROWTYPE;
		chrTypeCarryCD		TA_PICK.TYPECARRY_CD%TYPE;
		numPalletDetNoCD	NUMBER := 0;
		numSearchOK			NUMBER := 0;
		numNextSearch		NUMBER := 1;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		--パレット明細仮番号取得
		SELECT
			NVL(MAX(TO_NUMBER(PALLETDETNO_CD)), 0) INTO numPalletDetNoCD
		FROM
			TA_PICK
		WHERE
			TRN_ID = iTrnID AND
			TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_S; --集約

		WHILE ( numNextSearch = 1 ) LOOP

			PLOG.DEBUG(logCtx, 'SKU_CD = [' || iSkuCD || '], ' ||
							   'LOT_TX = [' || iLotTX || '], ' ||
							   'PALLETCAPACITY_NR = [' || iPalletCapacityNR || ']');

			OPEN curSumPallet(iTrnID, iSkuCD, iLotTX, iTypeStockCD, iPalletCapacityNR);
			FETCH curSumPallet INTO rowSumPallet;
			EXIT WHEN curSumPallet%NOTFOUND;

			PS_MASTER.GetSku(rowSumPallet.SKU_CD, rowSku);

			PS_LOCASEARCH.PalletPcs(
				rowSumPallet.SKU_CD,
				rowSumPallet.LOT_TX,
				rowSumPallet.TYPESTOCK_CD,
				iPalletCapacityNR,
				0				, 	-- iPalletNR
				0				, 	-- iCaseNR
				0				, 	-- iBaraNR
				0				, 	-- iPcsNR
				rowSku			,	-- irowSku
				rowLoca			,	-- orowLoca
				rowTypeLoca		,	-- orowTypeLoca
				rowLocaStockP	,	-- orowLocaStockP
				chrTypeCarryCD	,	-- oTypeCarryCD
				numSearchOK			-- oSerchOK
			);
			IF numSearchOK = 1 AND --パレット在庫があるか事前チェック
			   TRUNC( rowLocaStockP.PCS_NR / rowSumPallet.PCSPERCASE_NR / rowLocaStockP.PALLETCAPACITY_NR ) > 0 THEN

				numPalletDetNoCD := numPalletDetNoCD + 1;

				PLOG.DEBUG(logCtx, 'SKU_CD = [' || rowSumPallet.SKU_CD || '], ' ||
								   'LOT_TX = [' || rowSumPallet.LOT_TX || '], ' ||
								   'PALLETCAPACITY_NR = [' || rowLocaStockP.PALLETCAPACITY_NR || '], ' ||
								   'PCS_NR = [' || rowSumPallet.PCS_NR || '], ' ||
								   'TRANSPORTER2_CD = [' || rowSumPallet.TRANSPORTER2_CD || '], ' ||
								   'DTNAME_TX = [' || rowSumPallet.DTNAME_TX || '], ' ||
								   'DTADDRESS1_TX = [' || rowSumPallet.DTADDRESS1_TX || '], ' ||
								   'DTADDRESS2_TX = [' || rowSumPallet.DTADDRESS2_TX || '], ' ||
								   'DTADDRESS3_TX = [' || rowSumPallet.DTADDRESS3_TX || '], ' ||
								   'DTADDRESS4_TX = [' || rowSumPallet.DTADDRESS4_TX || '], ' ||
								   'DIRECTDELIVERY_CD = [' || rowSumPallet.DIRECTDELIVERY_CD || '], ' ||
								   'DELIDATE_TX = [' || rowSumPallet.DELIDATE_TX || '], ' ||
								   'TOTALPIC_FL = [' || rowSumPallet.TOTALPIC_FL || ']');

				IF rowSumPallet.TOTALPIC_FL = 1 THEN
					--トータルピック
					ApplyStockSumPalletTotal(
						iTrnID                         , 
						rowSumPallet.SKU_CD            , 
						rowSumPallet.LOT_TX            , 
						rowSumPallet.TYPESTOCK_CD      , 
						iPalletCapacityNR			   , --在庫のパレット積載数を利用する
						rowSumPallet.PCS_NR            , 
						rowSumPallet.TRANSPORTER2_CD   ,
						numPalletDetNoCD			   ,
						rowLocaStockP				   ,
						rowSumPallet.TOTALPIC_FL	   , --1:トータルピック
						chrTypeCarryCD				   ,
						iUserCD                        , 
						iUserTX                        , 
						iProgramCD					   
					);
				ELSE
					--軒先ピック
					ApplyStockSumPalletOrder(
						iTrnID                         , 
						rowSumPallet.SKU_CD            , 
						rowSumPallet.LOT_TX            , 
						rowSumPallet.TYPESTOCK_CD      , 
						iPalletCapacityNR			   , --在庫のパレット積載数を利用する
						rowSumPallet.PCS_NR            , 
						rowSumPallet.TRANSPORTER2_CD   ,
						rowSumPallet.DTNAME_TX		   ,    
						rowSumPallet.DTADDRESS1_TX	   ,
						rowSumPallet.DTADDRESS2_TX	   ,
						rowSumPallet.DTADDRESS3_TX	   ,
						rowSumPallet.DTADDRESS4_TX	   ,
						rowSumPallet.DIRECTDELIVERY_CD , --直送先コード
						rowSumPallet.DELIDATE_TX	   , --着荷指定日
						numPalletDetNoCD			   ,
						rowLocaStockP				   ,
						rowSumPallet.TOTALPIC_FL	   , --1:トータルピック
						chrTypeCarryCD				   ,
						iUserCD                        , 
						iUserTX                        , 
						iProgramCD					   
					);
				END IF;
			ELSE
				numNextSearch := 0;
			END IF;
			CLOSE curSumPallet;

		END LOOP;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyShipSumPalletStock;

	PROCEDURE ApplyShipCase(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyShipCase';

		CURSOR curCase(
			TrnID	TMP_APPLYSHIP.TRN_ID%TYPE
		) IS
			SELECT
				TSH.ORDERDATE_DT,
				TMP.SHIPH_ID,	
				TMP.SHIPD_ID,	
				TMP.SKU_CD,      
				TMP.LOT_TX,      
				TMP.TYPESTOCK_CD,
				TSD.PALLETCAPACITY_NR,
				TMP.ORDERPCS_NR - TMP.APPLYPCS_NR AS PCS_NR,
				NVL(MTP.TOTALPIC_FL, 1)  AS TOTALPIC_FL
			FROM
				TMP_APPLYSHIP	TMP,
				TA_SHIPH		TSH,
				TA_SHIPD		TSD,
				MA_SKU			MSK,
				MA_TRANSPORTER	MTP
			WHERE
				TMP.TRN_ID			= TrnID					AND
				TMP.SHIPH_ID		= TSH.SHIPH_ID			AND
				TMP.SHIPD_ID		= TSD.SHIPD_ID			AND
				TMP.SKU_CD			= MSK.SKU_CD			AND
				TSH.TRANSPORTER2_CD = MTP.TRANSPORTER_CD(+) AND
				TMP.ORDERPCS_NR - TMP.APPLYPCS_NR > 0 	 	AND
				TMP.SHORT_FL	    = 0	
			ORDER BY
				NVL(MTP.PICKORDER_NR, 0),
				TSH.TRANSPORTER2_CD, --配送業者別
				TMP.SKU_CD		   ,      
				TMP.LOT_TX		   ,      
				TMP.TYPESTOCK_CD   ,
				TMP.SHIPH_ID	   ,
				TMP.SHIPD_ID	   ;

		rowCase		curCase%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curCase(iTrnID);
		LOOP
			FETCH curCase INTO rowCase;
			EXIT WHEN curCase%NOTFOUND;

			PLOG.DEBUG(logCtx, 'SHIPD_ID = [' || rowCase.SHIPD_ID || '], ' ||
							   'SKU_CD = [' || rowCase.SKU_CD || '], ' ||
							   'LOT_TX = [' || rowCase.LOT_TX || '], ' ||
							   'PALLETCAPACITY_NR = [' || rowCase.PALLETCAPACITY_NR || '], ' ||
							   'PCS_NR = [' || rowCase.PCS_NR || '], ' ||
							   'TOTALPIC_FL = [' || rowCase.TOTALPIC_FL || ']');

			ApplyStockCase(
				iTrnID                    , 
				rowCase.SHIPD_ID          , 
				rowCase.SKU_CD            , 
				rowCase.LOT_TX            , 
				rowCase.TYPESTOCK_CD      , 
				rowCase.PALLETCAPACITY_NR , 
				rowCase.PCS_NR            , 
				rowCase.TOTALPIC_FL		  , --1:トータルピック
				iUserCD                   , 
				iUserTX                   , 
				iProgramCD
			);

		END LOOP;
		CLOSE curCase;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyShipCase;


	PROCEDURE GetShipData(
		iShipDID			IN	NUMBER			,
		orowShipH			OUT TA_SHIPH%ROWTYPE,
		orowShipD			OUT TA_SHIPD%ROWTYPE
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'GetShipData';

		CURSOR curShipH(
			ShipHID TA_SHIPH.SHIPH_ID%TYPE
		) IS
			SELECT
				*
			FROM
				TA_SHIPH
			WHERE
				SHIPH_ID = ShipHID	AND
				(
					STSHIP_ID	= PS_DEFINE.ST_SHIP0_NOTAPPLY	OR
					STSHIP_ID	= PS_DEFINE.ST_SHIP2_SFAILITEM
				);
		rowShipH	curShipH%ROWTYPE;
		
		CURSOR curShipD(
			ShipDID TA_SHIPD.SHIPD_ID%TYPE
		) IS
			SELECT
				*
			FROM
				TA_SHIPD
			WHERE
				SHIPD_ID = ShipDID AND
				(
					STSHIP_ID	= PS_DEFINE.ST_SHIP0_NOTAPPLY	OR
					STSHIP_ID	= PS_DEFINE.ST_SHIP2_SFAILITEM
				); 
		rowShipD	curShipD%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iShipDID = [' || iShipDID || ']'
		);

		OPEN curShipD(iShipDID);
		FETCH curShipD INTO rowShipD;
		IF curShipD%FOUND THEN
			orowShipD := rowShipD;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：出荷指示明細が存在しません。' ||
																'出荷指示明細ID = [' || iShipDID || ']');
		END IF;
		CLOSE curShipD;

		OPEN curShipH(rowShipD.SHIPH_ID);
		FETCH curShipH INTO rowShipH;
		IF curShipH%FOUND THEN
			orowShipH := rowShipH;
		ELSE
			RAISE_APPLICATION_ERROR(PS_DEFINE.EXCEPTION_SYS, '予期せぬエラー：出荷指示ヘッダーが存在しません。' ||
																'出荷指示ID = [' || rowShipD.SHIPH_ID || ']');
		END IF;
		CLOSE curShipH;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iShipDID = [' || iShipDID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END GetShipData;


	PROCEDURE ApplyStockOnePallet(
		iTrnID				IN	NUMBER	,
		iShipDID			IN	NUMBER	,
		iPalletDetNoCD		IN	VARCHAR2,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTypeStockCD		IN	VARCHAR2,
		iPallerCapacityNR	IN  NUMBER  ,
		iPcsNR				IN	NUMBER	,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'ApplyStockOnePallet';

		rowSku				MA_SKU%ROWTYPE;
		rowShipH			TA_SHIPH%ROWTYPE;
		rowShipD			TA_SHIPD%ROWTYPE;
		rowLoca				MA_LOCA%ROWTYPE;
		rowTypeLoca			MB_TYPELOCA%ROWTYPE;
		rowLocaStockP		TA_LOCASTOCKP%ROWTYPE;
		rowLocaStockC		TA_LOCASTOCKC%ROWTYPE;
		chrTypeCarryCD		TA_PICK.TYPECARRY_CD%TYPE;

		numSearchOK			NUMBER		:= 0;
		numNextSearch		NUMBER 		:= 1;
		numApplyPcsNR		NUMBER(9,0) := 0;
		numPalletNR			NUMBER(5,0) := 0;
		numCaseNR			NUMBER(5,0) := 0;
		numBaraNR			NUMBER(9,0) := 0;
		numPcsNR			NUMBER(9,0) := 0;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		PS_MASTER.GetSku(iSkuCD, rowSku);

		PLOG.DEBUG(logCtx, 'SHIPD_ID = [' || iShipDID || '], ' ||
						   'PCS_NR = [' || iPcsNR || ']');

		GetShipData(iShipDID, rowShipH, rowShipD);

		numApplyPcsNR	:= iPcsNR;

		WHILE ( numNextSearch = 1 ) LOOP
			EXIT WHEN numApplyPcsNR	<= 0;
			numPcsNR	:= numApplyPcsNR;

			PS_LOCASEARCH.PalletPcs(
				iSkuCD			,
				iLotTX			,
				iTypeStockCD	,
				iPallerCapacityNR,
				0				, 	-- iPalletNR
				0				, 	-- iCaseNR
				0				, 	-- iBaraNR
				0				, 	-- iPcsNR
				rowSku			,	-- irowSku
				rowLoca			,	-- orowLoca
				rowTypeLoca		,	-- orowTypeLoca
				rowLocaStockP	,	-- orowLocaStockP
				chrTypeCarryCD	,	-- oTypeCarryCD
				numSearchOK			-- oSerchOK
			);
			IF numSearchOK = 1 AND --パレット在庫があるか事前チェック
			   TRUNC( rowLocaStockP.PCS_NR / rowSku.PCSPERCASE_NR / rowLocaStockP.PALLETCAPACITY_NR ) > 0 THEN

				-- 在庫引当
				-- 引当数が引当可能在庫数より多い場合、引当可能在庫数を全て引当
				IF numPcsNR > rowLocaStockP.PCS_NR THEN
					numPcsNR	:= rowLocaStockP.PCS_NR;
				END IF;

				PS_STOCK.GetUnitPcsNr(
					iSkuCD						,
					numPcsNR					,
					rowLocaStockP.PALLETCAPACITY_NR,
					numPalletNR					,
					numCaseNR					,
					numBaraNR
				);

				PLOG.DEBUG(logCtx, 'LOCA_CD = [' || rowLocaStockP.LOCA_CD || '], ' ||
								   'SKU_CD = [' || rowLocaStockP.SKU_CD || '], ' ||
								   'LOT_TX = [' || rowLocaStockP.LOT_TX || '], ' ||
								   'PALLETCAPACITY_NR = [' || rowLocaStockP.PALLETCAPACITY_NR || '], ' ||
								   'PCS_NR = [' || numPcsNR || '], ' ||
								   'TOTALPIC_FL = [' || iTotalPicFL || ']');

				PS_STOCK.ApplyStock(
					iTrnID						,
					rowLocaStockP.LOCA_CD		,
					rowLocaStockP.SKU_CD		,
					rowLocaStockP.LOT_TX		,
					rowLocaStockP.USELIMIT_DT	,
					' '							,	--BATCHNO_CD
					rowLocaStockP.STOCK_DT		,
					rowSku.PCSPERCASE_NR		,
					rowLocaStockP.PALLETCAPACITY_NR,
					numPalletNR 				,
					numCaseNR 					,
					numBaraNR					,
					numPcsNR					,
					rowLocaStockP.TYPESTOCK_CD	,
					PS_DEFINE.ST_STOCK0_NOR		,
					iUserCD						,
					iUserTX						,
					rowLocaStockC
				);
				-- 引当数残
				numApplyPcsNR := numApplyPcsNR - numPcsNR;

				PLOG.DEBUG(logCtx, 'numApplyPcsNR = [' || numApplyPcsNR || ']');

				-- ピッキング追加
				PS_PICK.InsPick(
					iTrnID						,
					' '							, --PickNoCD
					iPalletDetNoCD				, --仮付番用
					PS_DEFINE.TYPE_PICK_PALLET_N, --通常パレット					,
					rowLocaStockC				,
					rowShipH					,
					rowShipD					,
					iTotalPicFL					, --1:トータルピック
					chrTypeCarryCD				,
					iUserCD						,
					iUserTX						,
					iProgramCD
				);

				UPDATE
					TMP_APPLYSHIP
				SET
					APPLYPCS_NR = APPLYPCS_NR + numPcsNR
				WHERE
					TRN_ID		= iTrnID			AND
					SHIPH_ID	= rowShipH.SHIPH_ID	AND
					SHIPD_ID	= rowShipD.SHIPD_ID;
			ELSE
				numNextSearch := 0;
			END IF;

		END LOOP;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyStockOnePallet;


	PROCEDURE ApplyStockSumPalletTotal(
		iTrnID				IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTypeStockCD		IN	VARCHAR2,
		iPallerCapacityNR	IN  NUMBER  ,
		iSumPcsNR			IN	NUMBER	,
		iTransporter2CD		IN	VARCHAR2, --配送業者
		iPalletDetNoCD		IN	NUMBER	, --パレット明細番号を仮付番
		irowLocaStockP		IN  TA_LOCASTOCKP%ROWTYPE,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iTypeCarryCD		IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyStockSumPalletTotal';

		CURSOR curSumPallet(
			TrnID			TMP_APPLYSHIP.TRN_ID%TYPE,
			SkuCD			TMP_APPLYSHIP.SKU_CD%TYPE,	
			LotTX			TMP_APPLYSHIP.LOT_TX%TYPE,
			TypeStockCD		TMP_APPLYSHIP.TYPESTOCK_CD%TYPE,
			Transporter2CD	TA_SHIPH.TRANSPORTER2_CD%TYPE
		) IS
			SELECT
				TSH.ORDERDATE_DT,
				TMP.SHIPH_ID,	
				TMP.SHIPD_ID,
				TMP.SKU_CD,      
				TMP.LOT_TX,      
				TMP.TYPESTOCK_CD,
				MSK.PCSPERCASE_NR,
				TSD.PALLETCAPACITY_NR,
				TMP.ORDERPCS_NR - TMP.APPLYPCS_NR AS PCS_NR
			FROM
				TMP_APPLYSHIP	TMP,
				TA_SHIPH		TSH,
				TA_SHIPD		TSD,
				MA_SKU			MSK
			WHERE
				TMP.TRN_ID			  = TrnID				AND
				TMP.SHIPH_ID		  = TSH.SHIPH_ID		AND
				TMP.SHIPD_ID		  = TSD.SHIPD_ID		AND
				TMP.SKU_CD			  = MSK.SKU_CD			AND
				TMP.SKU_CD			  = SkuCD				AND
				TMP.LOT_TX			  = LotTX				AND
				TMP.TYPESTOCK_CD	  = TypeStockCD			AND
				--TSD.PALLETCAPACITY_NR = iPallerCapacityNR AND --在庫のパレット積載数となる場合がありコメント
				TSH.TRANSPORTER2_CD	  = Transporter2CD	  	AND
				TMP.ORDERPCS_NR 	  - TMP.APPLYPCS_NR > 0 AND
				TMP.SHORT_FL		  = 0	
			ORDER BY
				TSH.ORDERDATE_DT,
				TMP.SHIPH_ID	;

		rowSumPallet		curSumPallet%ROWTYPE;

		rowShipH			TA_SHIPH%ROWTYPE;
		rowShipD			TA_SHIPD%ROWTYPE;
		rowLocaStockC		TA_LOCASTOCKC%ROWTYPE;

		numApplyPcsNR		NUMBER(9,0) := 0;
		numPalletNR			NUMBER(5,0) := 0;
		numCaseNR			NUMBER(5,0) := 0;
		numBaraNR			NUMBER(9,0) := 0;
		numPcsNR			NUMBER(9,0) := 0;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || '], ' ||
			'iSkuCD = [' || iSkuCD || '], ' ||  
			'iLotTX = [' || iLotTX || '], '	||	  
			'iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iPallerCapacityNR = [' || iPallerCapacityNR || '], ' ||
			'iSumPcsNR = [' || iSumPcsNR || '], ' ||		  
			'iTransporter2CD = [' || iTransporter2CD || '], ' ||
			'iPalletDetNoCD = [' || iPalletDetNoCD || '], ' ||	  
			'iTotalPicFL = [' || iTotalPicFL || ']');

		OPEN curSumPallet(iTrnID, iSkuCD, iLotTX, iTypeStockCD, iTransporter2CD);
		LOOP
			FETCH curSumPallet INTO rowSumPallet;
			EXIT WHEN curSumPallet%NOTFOUND;
			EXIT WHEN numApplyPcsNR >= rowSumPallet.PCSPERCASE_NR * iPallerCapacityNR;

			PLOG.DEBUG(logCtx, 'ApplyPcsNR = [' || numApplyPcsNR || '], ' ||
							   'PcsNR = [' || rowSumPallet.PCS_NR || '], ' ||
							   'PcsCase = [' || rowSumPallet.PCSPERCASE_NR || ']');

			IF TRUNC( ( numApplyPcsNR + rowSumPallet.PCS_NR ) / rowSumPallet.PCSPERCASE_NR ) < iPallerCapacityNR THEN

				numPcsNR := rowSumPallet.PCS_NR;

				PLOG.DEBUG(logCtx, 'numPcsNR = [' || numPcsNR || ']');

			ELSIF TRUNC( ( numApplyPcsNR + rowSumPallet.PCS_NR ) / rowSumPallet.PCSPERCASE_NR ) >= iPallerCapacityNR THEN

				numCaseNR := TRUNC( ( numApplyPcsNR + rowSumPallet.PCS_NR ) / rowSumPallet.PCSPERCASE_NR ) - iPallerCapacityNR;
				numBaraNR :=   MOD( ( numApplyPcsNR + rowSumPallet.PCS_NR ) , rowSumPallet.PCSPERCASE_NR );
				numPcsNR  := rowSumPallet.PCS_NR - ( rowSumPallet.PCSPERCASE_NR * numCaseNR ) - numBaraNR;

				PLOG.DEBUG(logCtx, 'numCaseNR = [' || numCaseNR || '], ' ||
								   'numBaraNR = [' || numBaraNR || '], ' ||
								   'numPcsNR = [' || numPcsNR || ']');
			END IF;

			PLOG.DEBUG(logCtx, 'SHIPD_ID = [' || rowSumPallet.SHIPD_ID || ']');

			GetShipData(rowSumPallet.SHIPD_ID, rowShipH, rowShipD);

			PS_STOCK.GetUnitPcsNr(
				iSkuCD						,
				numPcsNR					,
				irowLocaStockP.PALLETCAPACITY_NR,
				numPalletNR					,
				numCaseNR					,
				numBaraNR
			);

			PLOG.DEBUG(logCtx, 'LOCA_CD = [' || irowLocaStockP.LOCA_CD || '], ' ||
							   'SKU_CD = [' || irowLocaStockP.SKU_CD || '], ' ||
							   'LOT_TX = [' || irowLocaStockP.LOT_TX || '], ' ||
							   'PALLETCAPACITY_NR = [' || irowLocaStockP.PALLETCAPACITY_NR || '], ' ||
							   'PCS_NR = [' || numPcsNR || '], ' ||
							   'TRANSPORTER2_CD = [' || iTransporter2CD || '], ' ||
							   'TOTALPIC_FL = [' || iTotalPicFL || ']');

			PS_STOCK.ApplyStock(
				iTrnID						,
				irowLocaStockP.LOCA_CD		,
				irowLocaStockP.SKU_CD		,
				irowLocaStockP.LOT_TX		,
				irowLocaStockP.USELIMIT_DT	,
				' '							,	--BATCHNO_CD
				irowLocaStockP.STOCK_DT		,
				rowSumPallet.PCSPERCASE_NR	,
				irowLocaStockP.PALLETCAPACITY_NR,
				numPalletNR 				,
				numCaseNR 					,
				numBaraNR					,
				numPcsNR					,
				irowLocaStockP.TYPESTOCK_CD	,
				PS_DEFINE.ST_STOCK0_NOR		,
				iUserCD						,
				iUserTX						,
				rowLocaStockC
			);

			-- ピッキング追加
			PS_PICK.InsPick(
				iTrnID						,
				' '							, --PickNoCD
				iPalletDetNoCD				, --仮付番用
				PS_DEFINE.TYPE_PICK_PALLET_S, --集約パレット					,
				rowLocaStockC				,
				rowShipH					,
				rowShipD					,
				iTotalPicFL					, --1:トータルピック
				iTypeCarryCD				,
				iUserCD						,
				iUserTX						,
				iProgramCD
			);

			UPDATE
				TMP_APPLYSHIP
			SET
				APPLYPCS_NR = APPLYPCS_NR + numPcsNR
			WHERE
				TRN_ID		= iTrnID			AND
				SHIPH_ID	= rowShipH.SHIPH_ID	AND
				SHIPD_ID	= rowShipD.SHIPD_ID;

			numApplyPcsNR := numApplyPcsNR + numPcsNR;

		END LOOP;
		CLOSE curSumPallet;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyStockSumPalletTotal;


	PROCEDURE ApplyStockSumPalletOrder(
		iTrnID				IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTypeStockCD		IN	VARCHAR2,
		iPallerCapacityNR	IN  NUMBER  ,
		iSumPcsNR			IN	NUMBER	,
		iTransporter2CD		IN	VARCHAR2, --配送業者
		iDtNameTX			IN	VARCHAR2,    
		iDtAddress1TX		IN	VARCHAR2,
		iDtAddress2TX		IN	VARCHAR2,
		iDtAddress3TX		IN	VARCHAR2,
		iDtAddress4TX		IN	VARCHAR2,
		iDirectDeliveryCD 	IN	VARCHAR2, --直送先コード
		iDeliDateTX			IN	VARCHAR2, --着荷指定日
		iPalletDetNoCD		IN	NUMBER	, --パレット明細番号を仮付番
		irowLocaStockP		IN  TA_LOCASTOCKP%ROWTYPE,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iTypeCarryCD		IN	VARCHAR2,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyStockSumPalletOrder';

		CURSOR curSumPallet(
			TrnID				TMP_APPLYSHIP.TRN_ID%TYPE,
			SkuCD				TMP_APPLYSHIP.SKU_CD%TYPE,	
			LotTX				TMP_APPLYSHIP.LOT_TX%TYPE,
			TypeStockCD			TMP_APPLYSHIP.TYPESTOCK_CD%TYPE,
			Transporter2CD		TA_SHIPH.TRANSPORTER2_CD%TYPE,
			DtNameTX	 	 	TA_SHIPH.DTNAME_TX%TYPE,
			DtAddress1TX	 	TA_SHIPH.DTADDRESS1_TX%TYPE,
			DtAddress2TX	 	TA_SHIPH.DTADDRESS2_TX%TYPE,
			DtAddress3TX	 	TA_SHIPH.DTADDRESS3_TX%TYPE,
			DtAddress4TX	 	TA_SHIPH.DTADDRESS4_TX%TYPE,
			DirectDeliveryCD	TA_SHIPH.DIRECTDELIVERY_CD%TYPE,
			DeliDateTX		 	TA_SHIPH.DELIDATE_TX%TYPE
		) IS
			SELECT
				TSH.ORDERDATE_DT,
				TMP.SHIPH_ID,	
				TMP.SHIPD_ID,
				TMP.SKU_CD,      
				TMP.LOT_TX,      
				TMP.TYPESTOCK_CD,
				MSK.PCSPERCASE_NR,
				TSD.PALLETCAPACITY_NR,
				TMP.ORDERPCS_NR - TMP.APPLYPCS_NR AS PCS_NR
			FROM
				TMP_APPLYSHIP	TMP,
				TA_SHIPH		TSH,
				TA_SHIPD		TSD,
				MA_SKU			MSK
			WHERE
				TMP.TRN_ID			  = TrnID			  AND
				TMP.SHIPH_ID		  = TSH.SHIPH_ID	  AND
				TMP.SHIPD_ID		  = TSD.SHIPD_ID	  AND
				TMP.SKU_CD			  = MSK.SKU_CD		  AND
				TMP.SKU_CD			  = SkuCD			  AND
				TMP.LOT_TX			  = LotTX			  AND
				TMP.TYPESTOCK_CD	  = TypeStockCD	  	  AND
				--TSD.PALLETCAPACITY_NR = iPallerCapacityNR AND --在庫のパレット積載数となる場合がありコメント
				TSH.TRANSPORTER2_CD	  = Transporter2CD	  AND
				TSH.DTNAME_TX		  = DtNameTX	 	  AND
				TSH.DTADDRESS1_TX	  = DtAddress1TX	  AND
				TSH.DTADDRESS2_TX	  = DtAddress2TX	  AND
				TSH.DTADDRESS3_TX	  = DtAddress3TX	  AND
				TSH.DTADDRESS4_TX	  = DtAddress4TX	  AND
				TSH.DIRECTDELIVERY_CD = DirectDeliveryCD  AND
				TSH.DELIDATE_TX	  	  = DeliDateTX		  AND
				TMP.ORDERPCS_NR 	  - TMP.APPLYPCS_NR > 0 AND
				TMP.SHORT_FL		  = 0	
			ORDER BY
				TSH.ORDERDATE_DT,
				TMP.SHIPH_ID	;

		rowSumPallet		curSumPallet%ROWTYPE;

		rowShipH			TA_SHIPH%ROWTYPE;
		rowShipD			TA_SHIPD%ROWTYPE;
		rowLocaStockC		TA_LOCASTOCKC%ROWTYPE;

		numApplyPcsNR		NUMBER(9,0) := 0;
		numPalletNR			NUMBER(5,0) := 0;
		numCaseNR			NUMBER(5,0) := 0;
		numBaraNR			NUMBER(9,0) := 0;
		numPcsNR			NUMBER(9,0) := 0;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || '], ' ||
			'iSkuCD = [' || iSkuCD || '], ' ||  
			'iLotTX = [' || iLotTX || '], '	||	  
			'iTypeStockCD = [' || iTypeStockCD || '], ' ||
			'iPallerCapacityNR = [' || iPallerCapacityNR || '], ' ||
			'iSumPcsNR = [' || iSumPcsNR || '], ' ||		  
			'iTransporter2CD = [' || iTransporter2CD || '], ' ||
			'iPalletDetNoCD = [' || iPalletDetNoCD || '], '	||  
			'iDtNameTX = [' || iDtNameTX || '], ' ||	 
			'iDtAddress1TX = [' || iDtAddress1TX || '], ' ||
			'iDtAddress2TX = [' || iDtAddress2TX || '], ' ||
			'iDtAddress3TX = [' || iDtAddress3TX || '], ' ||
			'iDtAddress4TX = [' || iDtAddress4TX || '], ' ||
		    'iDirectDeliveryCD = [' || iDirectDeliveryCD || '], ' ||
		    'iDeliDateTX = [' || iDeliDateTX || '], ' ||
			'iTotalPicFL = [' || iTotalPicFL || ']');

		OPEN curSumPallet(iTrnID, iSkuCD, iLotTX, iTypeStockCD, iTransporter2CD, iDtNameTX, iDtAddress1TX,		
							iDtAddress2TX, iDtAddress3TX, iDtAddress4TX, iDirectDeliveryCD, iDeliDateTX);
		LOOP
			FETCH curSumPallet INTO rowSumPallet;
			EXIT WHEN curSumPallet%NOTFOUND;
			EXIT WHEN numApplyPcsNR >= rowSumPallet.PCSPERCASE_NR * iPallerCapacityNR;

			PLOG.DEBUG(logCtx, 'ApplyPcsNR = [' || numApplyPcsNR || '], ' ||
							   'PcsNR = [' || rowSumPallet.PCS_NR || '], ' ||
							   'PcsCase = [' || rowSumPallet.PCSPERCASE_NR || ']');

			IF TRUNC( ( numApplyPcsNR + rowSumPallet.PCS_NR ) / rowSumPallet.PCSPERCASE_NR ) < iPallerCapacityNR THEN

				numPcsNR := rowSumPallet.PCS_NR;

				PLOG.DEBUG(logCtx, 'numPcsNR = [' || numPcsNR || ']');

			ELSIF TRUNC( ( numApplyPcsNR + rowSumPallet.PCS_NR ) / rowSumPallet.PCSPERCASE_NR ) >= iPallerCapacityNR THEN

				numCaseNR := TRUNC( ( numApplyPcsNR + rowSumPallet.PCS_NR ) / rowSumPallet.PCSPERCASE_NR ) - iPallerCapacityNR;
				numBaraNR :=   MOD( ( numApplyPcsNR + rowSumPallet.PCS_NR ) , rowSumPallet.PCSPERCASE_NR );
				numPcsNR  := rowSumPallet.PCS_NR - ( rowSumPallet.PCSPERCASE_NR * numCaseNR ) - numBaraNR;

				PLOG.DEBUG(logCtx, 'numCaseNR = [' || numCaseNR || '], ' ||
								   'numBaraNR = [' || numBaraNR || '], ' ||
								   'numPcsNR = [' || numPcsNR || ']');
			END IF;

			PLOG.DEBUG(logCtx, 'SHIPD_ID = [' || rowSumPallet.SHIPD_ID || ']');

			GetShipData(rowSumPallet.SHIPD_ID, rowShipH, rowShipD);

			PS_STOCK.GetUnitPcsNr(
				iSkuCD						,
				numPcsNR					,
				irowLocaStockP.PALLETCAPACITY_NR,
				numPalletNR					,
				numCaseNR					,
				numBaraNR
			);

			PLOG.DEBUG(logCtx, 'LOCA_CD = [' || irowLocaStockP.LOCA_CD || '], ' ||
							   'SKU_CD = [' || irowLocaStockP.SKU_CD || '], ' ||
							   'LOT_TX = [' || irowLocaStockP.LOT_TX || '], ' ||
							   'PALLETCAPACITY_NR = [' || irowLocaStockP.PALLETCAPACITY_NR || '], ' ||
							   'PCS_NR = [' || numPcsNR || '], ' ||
							   'TRANSPORTER2_CD = [' || iTransporter2CD || '], ' ||
							   'iDtNameTX = [' || iDtNameTX || '], ' ||	 
							   'iDtAddress1TX = [' || iDtAddress1TX || '], ' ||
							   'iDtAddress2TX = [' || iDtAddress2TX || '], ' ||
							   'iDtAddress3TX = [' || iDtAddress3TX || '], ' ||
							   'iDtAddress4TX = [' || iDtAddress4TX || '], ' ||
							   'iDirectDeliveryCD = [' || iDirectDeliveryCD || '], ' ||
							   'iDeliDateTX = [' || iDeliDateTX || '], ' ||
							   'iTotalPicFL = [' || iTotalPicFL || ']');

			PS_STOCK.ApplyStock(
				iTrnID						,
				irowLocaStockP.LOCA_CD		,
				irowLocaStockP.SKU_CD		,
				irowLocaStockP.LOT_TX		,
				irowLocaStockP.USELIMIT_DT	,
				' '							,	--BATCHNO_CD
				irowLocaStockP.STOCK_DT		,
				rowSumPallet.PCSPERCASE_NR	,
				irowLocaStockP.PALLETCAPACITY_NR,
				numPalletNR 				,
				numCaseNR 					,
				numBaraNR					,
				numPcsNR					,
				irowLocaStockP.TYPESTOCK_CD	,
				PS_DEFINE.ST_STOCK0_NOR		,
				iUserCD						,
				iUserTX						,
				rowLocaStockC
			);

			-- ピッキング追加
			PS_PICK.InsPick(
				iTrnID						,
				' '							, --PickNoCD
				iPalletDetNoCD				, --仮付番用
				PS_DEFINE.TYPE_PICK_PALLET_S, --集約パレット					,
				rowLocaStockC				,
				rowShipH					,
				rowShipD					,
				iTotalPicFL					, --0:軒先ピック
				iTypeCarryCD				,
				iUserCD						,
				iUserTX						,
				iProgramCD
			);

			UPDATE
				TMP_APPLYSHIP
			SET
				APPLYPCS_NR = APPLYPCS_NR + numPcsNR
			WHERE
				TRN_ID		= iTrnID			AND
				SHIPH_ID	= rowShipH.SHIPH_ID	AND
				SHIPD_ID	= rowShipD.SHIPD_ID;

			numApplyPcsNR := numApplyPcsNR + numPcsNR;

		END LOOP;
		CLOSE curSumPallet;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyStockSumPalletOrder;


	PROCEDURE ApplyStockCase(
		iTrnID				IN	NUMBER	,
		iShipDID			IN	NUMBER	,
		iSkuCD				IN	VARCHAR2,
		iLotTX				IN	VARCHAR2,
		iTypeStockCD		IN	VARCHAR2,
		iPallerCapacityNR	IN  NUMBER  ,
		iPcsNR				IN	NUMBER	,
		iTotalPicFL			IN	NUMBER	, --1:トータルピック
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ApplyStockCase';

		rowSku				MA_SKU%ROWTYPE;
		rowShipH			TA_SHIPH%ROWTYPE;
		rowShipD			TA_SHIPD%ROWTYPE;
		rowLoca				MA_LOCA%ROWTYPE;
		rowTypeLoca			MB_TYPELOCA%ROWTYPE;
		rowLocaStockP		TA_LOCASTOCKP%ROWTYPE;
		rowLocaStockC		TA_LOCASTOCKC%ROWTYPE;
		chrTypeCarryCD		TA_PICK.TYPECARRY_CD%TYPE;

		numSearchOK			NUMBER		:= 0;
		numApplyPcsNR		NUMBER(9,0) := 0;
		numPalletNR			NUMBER(5,0) := 0;
		numCaseNR			NUMBER(5,0) := 0;
		numBaraNR			NUMBER(9,0) := 0;
		numPcsNR			NUMBER(9,0) := 0;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		PS_MASTER.GetSku(iSkuCD, rowSku);

		PLOG.DEBUG(logCtx, 'SHIPD_ID = [' || iShipDID || '], ' ||
						   'PCS_NR = [' || iPcsNR || ']');

		GetShipData(iShipDID, rowShipH, rowShipD);

		numSearchOK		:= 1;
		numApplyPcsNR	:= iPcsNR;
		LOOP
			EXIT WHEN numSearchOK	= 0;
			EXIT WHEN numApplyPcsNR	<= 0;
			numPcsNR	:= numApplyPcsNR;

			PS_LOCASEARCH.CasePcs(
				iSkuCD			,
				iLotTX			,
				iTypeStockCD	,
				0				, 	-- iPalletNR
				0				, 	-- iCaseNR
				0				, 	-- iBaraNR
				0				, 	-- iPcsNR
				rowSku			,	-- irowSku
				rowLoca			,	-- orowLoca
				rowTypeLoca		,	-- orowTypeLoca
				rowLocaStockP	,	-- orowLocaStockP
				chrTypeCarryCD	,	-- oTypeCarryCD
				numSearchOK			-- oSerchOK
			);
			IF numSearchOK = 1 THEN

				-- 在庫引当
				-- 引当数が引当可能在庫数より多い場合、引当可能在庫数を全て引当
				IF numPcsNR > rowLocaStockP.PCS_NR THEN
					numPcsNR	:= rowLocaStockP.PCS_NR;
				END IF;

				PS_STOCK.GetUnitPcsNr(
					iSkuCD						,
					numPcsNR					,
					rowLocaStockP.PALLETCAPACITY_NR,
					--rowShipD.PALLETCAPACITY_NR,
					numPalletNR					,
					numCaseNR					,
					numBaraNR
				);

				PLOG.DEBUG(logCtx, 'LOCA_CD = [' || rowLocaStockP.LOCA_CD || '], ' ||
								   'SKU_CD = [' || rowLocaStockP.SKU_CD || '], ' ||
								   'LOT_TX = [' || rowLocaStockP.LOT_TX || '], ' ||
								   'PALLETCAPACITY_NR = [' || rowLocaStockP.PALLETCAPACITY_NR || '], ' ||
								   'PCS_NR = [' || numPcsNR || '], ' ||
								   'TOTALPIC_FL = [' || iTotalPicFL || ']');

				PS_STOCK.ApplyStock(
					iTrnID						,
					rowLocaStockP.LOCA_CD		,
					rowLocaStockP.SKU_CD		,
					rowLocaStockP.LOT_TX		,
					rowLocaStockP.USELIMIT_DT	,
					' '							,	--BATCHNO_CD
					rowLocaStockP.STOCK_DT		,
					rowSku.PCSPERCASE_NR		,
					rowLocaStockP.PALLETCAPACITY_NR,
					numPalletNR 				,
					numCaseNR 					,
					numBaraNR					,
					numPcsNR					,
					rowLocaStockP.TYPESTOCK_CD	,
					PS_DEFINE.ST_STOCK0_NOR		,
					iUserCD						,
					iUserTX						,
					rowLocaStockC
				);
				-- 引当数残
				numApplyPcsNR := numApplyPcsNR - numPcsNR;

				PLOG.DEBUG(logCtx, 'numApplyPcsNR = [' || numApplyPcsNR || ']');

				-- ピッキング追加
				PS_PICK.InsPick(
					iTrnID						,
					' '							, --PICKNO_CD
					' '							, --PALLETDETNO_CD
					PS_DEFINE.TYPE_PICK_CASE	,
					rowLocaStockC				,
					rowShipH					,
					rowShipD					,
					iTotalPicFL					, --1:トータルピック
					chrTypeCarryCD				,
					iUserCD						,
					iUserTX						,
					iProgramCD
				);

				UPDATE
					TMP_APPLYSHIP
				SET
					APPLYPCS_NR = APPLYPCS_NR + numPcsNR
				WHERE
					TRN_ID		= iTrnID			AND
					SHIPH_ID	= rowShipH.SHIPH_ID	AND
					SHIPD_ID	= rowShipD.SHIPD_ID;

			END IF;

		END LOOP;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ApplyStockCase;

	PROCEDURE ReApplyMain(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2,
		oApplyFL			OUT NUMBER --再引当フラグ
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ReApplyMain';

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		--欠品データ更新
		PS_APPLYSHIP.ReApplyShip(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD
		);

		--ピッキング指示削除
		PS_APPLYSHIP.ReApplyPick(
			iTrnID					,
			iUserCD					,
			iUserTX					,
			iProgramCD				,
			oApplyFL
		);

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ReApplyMain;

	PROCEDURE ReApplyShip(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ReApplyShip';

		CURSOR curReApplyShip(
			TrnID	TMP_APPLYSHIP.TRN_ID%TYPE
		) IS
			SELECT
				SHIPH_ID
			FROM
				TMP_APPLYSHIP
			WHERE
				TRN_ID   = TrnID AND
				SHORT_FL = 0 --欠品でないもの
			GROUP BY
				SHIPH_ID
			HAVING
				SUM(ORDERPCS_NR - APPLYPCS_NR) > 0
			ORDER BY
				SHIPH_ID;

		rowReApplyShip		curReApplyShip%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curReApplyShip(iTrnID);
		LOOP
			FETCH curReApplyShip INTO rowReApplyShip;
			EXIT WHEN curReApplyShip%NOTFOUND;

			PLOG.DEBUG(logCtx, 'SHIPH_ID = [' || rowReApplyShip.SHIPH_ID || ']');

			--オーダー単位で欠品とする
			UPDATE
				TMP_APPLYSHIP
			SET
				SHORTPCS_NR = ORDERPCS_NR - APPLYPCS_NR,
				SHORT_FL	= 1
			WHERE
				TRN_ID		= iTrnID AND
				SHIPH_ID	= rowReApplyShip.SHIPH_ID;

		END LOOP;
		CLOSE curReApplyShip;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ReApplyShip;

	PROCEDURE ReApplyPick(
		iTrnID				IN	NUMBER	,
		iUserCD				IN	VARCHAR2,
		iUserTX				IN	VARCHAR2,
		iProgramCD			IN	VARCHAR2,
		oApplyFL			OUT NUMBER --再引当フラグ
	) AS
		FUNCTION_NAME	CONSTANT VARCHAR2(40)	:= 'ReApplyPick';

		--集約パレット解除
		CURSOR curApplyStockS(
			TrnID	TMP_APPLYSHIP.TRN_ID%TYPE
		) IS
			SELECT
				SRCLOCASTOCKC_ID,
				SUM(PCS_NR) AS PCS_NR
			FROM
				TA_PICK
			WHERE
				TRN_ID = TrnID AND
				PALLETDETNO_CD IN (
					SELECT
						PALLETDETNO_CD
					FROM
						TA_PICK
					WHERE
						TRN_ID = TrnID AND
						TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_S AND --集約
						SHIPH_ID IN (
							SELECT
								SHIPH_ID
							FROM
								TMP_APPLYSHIP
							WHERE
								TRN_ID   = TrnID AND
								SHORT_FL = 1 --欠品オーダー
							GROUP BY
								SHIPH_ID )
					GROUP BY
						PALLETDETNO_CD
				)
			GROUP BY
				SRCLOCASTOCKC_ID
			ORDER BY
				SRCLOCASTOCKC_ID;

		rowApplyStockS		curApplyStockS%ROWTYPE;

		--集約パレット以外
		CURSOR curApplyStockN(
			TrnID	TMP_APPLYSHIP.TRN_ID%TYPE
		) IS
			SELECT
				SRCLOCASTOCKC_ID,
				SUM(PCS_NR) AS PCS_NR
			FROM
				TA_PICK
			WHERE
				TRN_ID = TrnID AND
				TYPEPICK_CD = PS_DEFINE.TYPE_PICK_PALLET_N AND --パレット
				SHIPH_ID IN (
					SELECT
						SHIPH_ID
					FROM
						TMP_APPLYSHIP
					WHERE
						TRN_ID   = TrnID AND
						SHORT_FL = 1 --欠品オーダー
					GROUP BY
						SHIPH_ID )
			GROUP BY
				SRCLOCASTOCKC_ID
			ORDER BY
				SRCLOCASTOCKC_ID;

		rowApplyStockN		curApplyStockN%ROWTYPE;

		--ケースは商品別でも解除
		CURSOR curApplyStockC(
			TrnID	TMP_APPLYSHIP.TRN_ID%TYPE
		) IS
			SELECT
				SKU_CD,
				LOT_TX 
			FROM
				TA_PICK
			WHERE
				TRN_ID = TrnID AND
				TYPEPICK_CD = PS_DEFINE.TYPE_PICK_CASE AND
				SHIPH_ID IN (
					SELECT
						SHIPH_ID
					FROM
						TMP_APPLYSHIP
					WHERE
						TRN_ID   = TrnID AND
						SHORT_FL = 1 --欠品オーダー
					GROUP BY
						SHIPH_ID )
			GROUP BY       
				SKU_CD, 
				LOT_TX  
			ORDER BY       
				SKU_CD, 
				LOT_TX; 

		rowApplyStockC		curApplyStockC%ROWTYPE;

		CURSOR curApplyPick(
			TrnID	TMP_APPLYSHIP.TRN_ID%TYPE,
			SkuCD	TMP_APPLYSHIP.SKU_CD%TYPE,
			LotTX	TMP_APPLYSHIP.LOT_TX%TYPE
		) IS
			SELECT
				SRCLOCASTOCKC_ID,
				SUM(PCS_NR) AS PCS_NR
			FROM
				TA_PICK
			WHERE
				TRN_ID = TrnID AND
				TYPEPICK_CD = PS_DEFINE.TYPE_PICK_CASE AND --ケース
				SKU_CD = SkuCD AND
				LOT_TX = LotTX
			GROUP BY
				SRCLOCASTOCKC_ID
			ORDER BY
				SRCLOCASTOCKC_ID;

		rowApplyPick		curApplyPick%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		oApplyFL := 0;

		OPEN curApplyStockS(iTrnID);
		LOOP
			FETCH curApplyStockS INTO rowApplyStockS;
			EXIT WHEN curApplyStockS%NOTFOUND;

			PLOG.DEBUG(logCtx, 'curApplyStockS ' ||
							   'SRCLOCASTOCKC_ID = [' || rowApplyStockS.SRCLOCASTOCKC_ID || ']');

			UPDATE
				TMP_APPLYSHIP
			SET
				APPLYPCS_NR = APPLYPCS_NR - rowApplyStockS.PCS_NR
			WHERE
				TRN_ID   = iTrnID AND
				SHORT_FL = 0 	  AND --欠品でない
				SHIPD_ID IN (
					SELECT
						SHIPD_ID
					FROM
						TA_PICK
					WHERE
						TRN_ID = iTrnID AND
						SRCLOCASTOCKC_ID = rowApplyStockS.SRCLOCASTOCKC_ID
					GROUP BY
						SHIPD_ID);

			ReApplyStock(
				iTrnID		 ,
				rowApplyStockS.SRCLOCASTOCKC_ID,
				iUserCD		 ,
				iUserTX		 ,
				iProgramCD	 
			);

			DELETE
				TA_PICK
			WHERE
				TRN_ID = iTrnID AND
				SRCLOCASTOCKC_ID = rowApplyStockS.SRCLOCASTOCKC_ID;
				
			oApplyFL := 1; --再引当必要

		END LOOP;
		CLOSE curApplyStockS;

		OPEN curApplyStockN(iTrnID);
		LOOP
			FETCH curApplyStockN INTO rowApplyStockN;
			EXIT WHEN curApplyStockN%NOTFOUND;

			PLOG.DEBUG(logCtx, 'curApplyStockN ' ||
							   'SRCLOCASTOCKC_ID = [' || rowApplyStockN.SRCLOCASTOCKC_ID || ']');

			UPDATE
				TMP_APPLYSHIP
			SET
				APPLYPCS_NR = APPLYPCS_NR - rowApplyStockN.PCS_NR
			WHERE
				TRN_ID   = iTrnID AND
				SHORT_FL = 0 	  AND --欠品でない
				SHIPD_ID IN (
					SELECT
						SHIPD_ID
					FROM
						TA_PICK
					WHERE
						TRN_ID = iTrnID AND
						SRCLOCASTOCKC_ID = rowApplyStockN.SRCLOCASTOCKC_ID
					GROUP BY
						SHIPD_ID);

			ReApplyStock(
				iTrnID		 ,
				rowApplyStockN.SRCLOCASTOCKC_ID,
				iUserCD		 ,
				iUserTX		 ,
				iProgramCD	 
			);

			DELETE
				TA_PICK
			WHERE
				TRN_ID = iTrnID AND
				SRCLOCASTOCKC_ID = rowApplyStockN.SRCLOCASTOCKC_ID;

		END LOOP;
		CLOSE curApplyStockN;

		--ケースの場合、再引当後ロケが割れないように商品でまとめて戻す
		OPEN curApplyStockC(iTrnID);
		LOOP
			FETCH curApplyStockC INTO rowApplyStockC;
			EXIT WHEN curApplyStockC%NOTFOUND;

			PLOG.DEBUG(logCtx, 'curApplyStockC ' ||
							   'iTrnID = [' || iTrnID || '], ' ||
							   'SKU_CD = [' || rowApplyStockC.SKU_CD || '], ' ||
							   'LOT_TX = [' || rowApplyStockC.LOT_TX || ']');

			OPEN curApplyPick(iTrnID, rowApplyStockC.SKU_CD, rowApplyStockC.LOT_TX);
			LOOP
				FETCH curApplyPick INTO rowApplyPick;
				EXIT WHEN curApplyPick%NOTFOUND;

				PLOG.DEBUG(logCtx, 'curApplyStockC ' ||
								   'SRCLOCASTOCKC_ID = [' || rowApplyPick.SRCLOCASTOCKC_ID || ']');

				UPDATE
					TMP_APPLYSHIP
				SET
					APPLYPCS_NR = APPLYPCS_NR - rowApplyPick.PCS_NR
				WHERE
					TRN_ID   = iTrnID AND
					SHORT_FL = 0 	  AND --欠品でない
					SHIPD_ID IN (
						SELECT
							SHIPD_ID
						FROM
							TA_PICK
						WHERE
							TRN_ID = iTrnID AND
							SRCLOCASTOCKC_ID = rowApplyPick.SRCLOCASTOCKC_ID
						GROUP BY
							SHIPD_ID);

				ReApplyStock(
					iTrnID		 ,
					rowApplyPick.SRCLOCASTOCKC_ID,
					iUserCD		 ,
					iUserTX		 ,
					iProgramCD	 
				);

				DELETE
					TA_PICK
				WHERE
					TRN_ID = iTrnID AND
					SRCLOCASTOCKC_ID = rowApplyPick.SRCLOCASTOCKC_ID;

				oApplyFL := 1; --再引当必要

			END LOOP;
			CLOSE curApplyPick;

		END LOOP;
		CLOSE curApplyStockC;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ReApplyPick;

	PROCEDURE ReApplyStock(
		iTrnID			IN NUMBER	,
		iLocaStockCID	IN NUMBER	,
		iUserCD			IN VARCHAR2	,
		iUserTX			IN VARCHAR2	,
		iProgramCD		IN VARCHAR2	
	) AS
		FUNCTION_NAME 	CONSTANT VARCHAR2(40)	:= 'ReApplyStock';

		CURSOR curLocaStockC(
			LocaStockCID	TA_LOCASTOCKC.LOCASTOCKC_ID%TYPE
		) IS
			SELECT
				LSC.LOCASTOCKC_ID	,
				LSC.LOCA_CD			,
				SKU.PCSPERCASE_NR AS AMOUNT_NR
			FROM
				TA_LOCASTOCKC	LSC,
				MA_SKU			SKU
			WHERE
				LSC.SKU_CD			= SKU.SKU_CD AND
				LSC.LOCASTOCKC_ID	= LocaStockCID;

		rowLocaStockC	curLocaStockC%ROWTYPE;

	BEGIN
		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START:' ||
			'iTrnID = [' || iTrnID || ']'
		);

		OPEN curLocaStockC(iLocaStockCID);
		LOOP
			FETCH curLocaStockC INTO rowLocaStockC;
			EXIT WHEN curLocaStockC%NOTFOUND;
			--ロケーションロック取得
			PS_LOCA.GetLocaLock(rowLocaStockC.LOCA_CD);

			PLOG.DEBUG(logCtx, 'LOCA_CD = [' || rowLocaStockC.LOCA_CD || ']' ||
							   'LOCASTOCKC_ID = [' || rowLocaStockC.LOCASTOCKC_ID || ']' ||
							   'AMOUNT_NR = [' || rowLocaStockC.AMOUNT_NR || ']');

			--戻し処理
			PS_STOCK.ReApplyStock(
				rowLocaStockC.LOCASTOCKC_ID	,
				rowLocaStockC.AMOUNT_NR		,
				iUserCD						,
				iUserTX						
			);
		END LOOP;
		CLOSE curLocaStockC;

		PLOG.INFO(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END:' ||
			'iTrnID = [' || iTrnID || ']'
		);
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END ReApplyStock;

END PS_APPLYSHIP;
/
SHOW ERRORS
