-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE BODY PI_SKU AS
	
	/*----------------------------------------------------------*/
	/* Private（行単位チェック用）                              */
	/*----------------------------------------------------------*/
	
	/************************************************************/
	/* 項目チェック                                             */
	/************************************************************/
	PROCEDURE CheckColumn(
		inumRow IN NUMBER,
		irowData IN MA_SKU%ROWTYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'CheckColumn';
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 1項目ずつチェックしたい処理をここへ記述
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END CheckColumn;
	/************************************************************/
	/* ファイル単位チェック                                     */
	/************************************************************/
	PROCEDURE CheckByFile(
		iTrnID						IN TMP_SKU.TRN_ID%TYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'CheckByFile';
		-- アップロードされたデータの重複分
		CURSOR curFrom(TrnID	TMP_SKU.TRN_ID%TYPE) IS
			SELECT
				NINUSHI || SYOCD AS SKU_CD,
				NVL(FCKBN,'') AS MANUFACTURER_CD
			FROM
				TMP_SKU
			WHERE
				TRN_ID = TrnID
			GROUP BY
				NINUSHI || SYOCD	,
				NVL(FCKBN,'')
			HAVING
				COUNT(*) > 1;
		rowFrom curFrom%ROWTYPE;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- 重複チェック
		OPEN curFrom(iTrnID);
		-- 対象行を取得
		FETCH curFrom INTO rowFrom;
		-- 重複行が存在した場合はエラー
		IF curFrom%FOUND THEN
			RAISE_APPLICATION_ERROR (PS_DEFINE.EXCEPTION_SYS,'入荷予定明細の同一ファイル内でキーが重複しています。');
		END IF;
		CLOSE curFrom;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			IF curFrom%ISOPEN THEN
				CLOSE curFrom;
			END IF;
			RAISE;
	END CheckByFile;
	/*----------------------------------------------------------*/
	/* Public                                                   */
	/*----------------------------------------------------------*/
	/************************************************************/
	/* アップロード完了（SKU）                                  */
	/************************************************************/
	PROCEDURE EndUploadSku(
		iTrnID						IN TMP_SKU.TRN_ID%TYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'EndUploadSku';
		-- TMP
		CURSOR curTmp(TrnID	TMP_SKU.TRN_ID%TYPE) IS
			SELECT
				NINUSHI || SYOCD AS SKU_CD			,
				BCD			AS WAREHOUSESKU_CD		,
				GSYOCD		AS EXTERIOR_CD			,
				KIKAKUKK	AS SIZE_TX				,
				JANCD_K		AS SKUBARCODE_TX		,
				JANCD_B		AS SKUBARCODEBOARD_TX	,
				JANCD_H		AS SKUBARCODEBARA_TX	,
				ITFCD		AS ITFCODE_TX			,
				SEINAN		AS SKU_TX				,
				SEINAK		AS SKU2_TX				,
				DRK			AS MANUFACTURERSKU_TX	,
				NINUSHI		AS OWNER_CD				,
				SKBN		AS TYPEBILL1_CD			,
				' '			AS TYPEBILL2_CD			,	-- 予備項目
				NVL(GS1CD,' ') AS GS1_CD			,
				KAN1 AS PCSPERCASE_NR				,
				KITAKU AS DEPOISTPRICE_NR			,
				HOK1 AS STORAGECHARGEA_NR			,
				HOK2 AS STORAGECHARGEB_NR			,
				NYK1 AS STEVEDORAGEA_NR				,
				NYK2 AS STEVEDORAGEB_NR				,
				TATEK AS LENGTH_NR					,
				YOKOK AS WIDTH_NR					,
				TAKAK AS HEIGHT_NR					,
				WGT AS WEIGHT_NR					
			FROM
				TMP_SKU
			WHERE
				TRN_ID = TrnID
			ORDER BY
				NINUSHI	,
				SYOCD;
		rowTmp curTmp%ROWTYPE;
		numCount			NUMBER;
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		-- ファイル単位チェック
		CheckByFile(iTrnID);
		-- テーブル単位チェックはWMS側で実施
		OPEN curTmp(iTrnID);
		LOOP
			FETCH curTmp INTO rowTmp;
			EXIT WHEN curTmp%NOTFOUND;
			-- SKU
			SELECT
				COUNT(SKU_CD)
			INTO
				numCount
			FROM
				MA_SKU
			WHERE
				SKU_CD = rowTmp.SKU_CD;
			IF numCount = 0 THEN
				--登録
				INSERT INTO MA_SKU (
					SKU_CD				,
					WAREHOUSESKU_CD		,
					EXTERIOR_CD			,
					SIZE_TX				,
					SKUBARCODE_TX		,
					SKUBARCODEBOARD_TX	,
					SKUBARCODEBARA_TX	,
					ITFCODE_TX			,
					SKU_TX				,
					SKU2_TX				,
					MANUFACTURERSKU_TX	,
					OWNER_CD			,
					TYPEBILL1_CD		,
					TYPEBILL2_CD		,
					GS1_CD				,
					PCSPERCASE_NR		,
					DEPOISTPRICE_NR		,
					STORAGECHARGEA_NR	,
					STORAGECHARGEB_NR	,
					STEVEDORAGEA_NR		,
					STEVEDORAGEB_NR		,
					LENGTH_NR			,
					WIDTH_NR			,
					HEIGHT_NR			,
					WEIGHT_NR			,
					ABCSUB_ID			,
					--
					UPD_DT				,
					UPDUSER_CD			,
					UPDUSER_TX			,
					ADD_DT				,
					ADDUSER_CD			,
					ADDUSER_TX			,
					UPDPROGRAM_CD		,
					UPDCOUNTER_NR		,
					STRECORD_ID			
				) VALUES (
					rowTmp.SKU_CD				,
					rowTmp.WAREHOUSESKU_CD		,
					rowTmp.EXTERIOR_CD			,
					rowTmp.SIZE_TX				,
					rowTmp.SKUBARCODE_TX		,
					rowTmp.SKUBARCODEBOARD_TX	,
					rowTmp.SKUBARCODEBARA_TX	,
					rowTmp.ITFCODE_TX			,
					rowTmp.SKU_TX				,
					rowTmp.SKU2_TX				,
					rowTmp.MANUFACTURERSKU_TX	,
					rowTmp.OWNER_CD				,
					rowTmp.TYPEBILL1_CD			,
					rowTmp.TYPEBILL2_CD			,
					rowTmp.GS1_CD				,
					rowTmp.PCSPERCASE_NR		,
					rowTmp.DEPOISTPRICE_NR		,
					rowTmp.STORAGECHARGEA_NR	,
					rowTmp.STORAGECHARGEB_NR	,
					rowTmp.STEVEDORAGEA_NR		,
					rowTmp.STEVEDORAGEB_NR		,
					rowTmp.LENGTH_NR			,
					rowTmp.WIDTH_NR				,
					rowTmp.HEIGHT_NR			,
					rowTmp.WEIGHT_NR			,
					CASE
						WHEN rowTmp.WEIGHT_NR >= 6000 THEN 2	-- 6kg以上が下段
						ELSE 1									-- 6kg未満が上段
					END							,
					--
					SYSDATE						,
					'BATCH'						,
					'BATCH'						,
					SYSDATE						,
					'BATCH'						,
					'BATCH'						,
					'BATCH'						,
					0							,
					0							
				);
			ELSE
				--更新
				UPDATE
					MA_SKU
				SET
					WAREHOUSESKU_CD		= rowTmp.WAREHOUSESKU_CD	,
					EXTERIOR_CD			= rowTmp.EXTERIOR_CD		,
					SIZE_TX				= rowTmp.SIZE_TX			,
					SKUBARCODE_TX		= rowTmp.SKUBARCODE_TX		,
					SKUBARCODEBOARD_TX	= rowTmp.SKUBARCODEBOARD_TX	,
					SKUBARCODEBARA_TX	= rowTmp.SKUBARCODEBARA_TX	,
					ITFCODE_TX			= rowTmp.ITFCODE_TX			,
					SKU_TX				= rowTmp.SKU_TX				,
					SKU2_TX				= rowTmp.SKU2_TX			,
					MANUFACTURERSKU_TX	= rowTmp.MANUFACTURERSKU_TX	,
					OWNER_CD			= rowTmp.OWNER_CD			,
					TYPEBILL1_CD		= rowTmp.TYPEBILL1_CD		,
					TYPEBILL2_CD		= rowTmp.TYPEBILL2_CD		,
					GS1_CD				= rowTmp.GS1_CD				,
					PCSPERCASE_NR		= rowTmp.PCSPERCASE_NR		,
					DEPOISTPRICE_NR		= rowTmp.DEPOISTPRICE_NR	,
					STORAGECHARGEA_NR	= rowTmp.STORAGECHARGEA_NR	,
					STORAGECHARGEB_NR	= rowTmp.STORAGECHARGEB_NR	,
					STEVEDORAGEA_NR		= rowTmp.STEVEDORAGEA_NR	,
					STEVEDORAGEB_NR		= rowTmp.STEVEDORAGEB_NR	,
					LENGTH_NR			= rowTmp.LENGTH_NR			,
					WIDTH_NR			= rowTmp.WIDTH_NR			,
					HEIGHT_NR			= rowTmp.HEIGHT_NR			,
					WEIGHT_NR			= rowTmp.WEIGHT_NR			,
					--ABCSUB_ID			= CASE
					--							WHEN rowTmp.WEIGHT_NR >= 6000 THEN 2	-- 6kg以上が下段
					--							ELSE 1									-- 6kg未満が上段
					--					  END						,
					-- 管理項目
					UPD_DT				= SYSDATE					,
					UPDUSER_CD			= 'BATCH'					,
					UPDUSER_TX			= 'BATCH'					,
					UPDPROGRAM_CD		= 'BATCH'					,
					UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
				WHERE
					SKU_CD = rowTmp.SKU_CD;
			END IF;
		END LOOP;
		CLOSE curTmp;
		-- SKUパレット登録
		UploadSkuPallet(iTrnID);
		COMMIT;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END EndUploadSku;
	/************************************************************/
	/* アップロード（MA_SKUPALLET）                             */
	/************************************************************/
	PROCEDURE UploadSkuPallet(
		iTrnID						IN TMP_SKU.TRN_ID%TYPE
	) IS
		FUNCTION_NAME		CONSTANT VARCHAR2(40)	:= 'UploadSkuPallet';
		-- TMP
		CURSOR curTmp(TrnID	TMP_SKU.TRN_ID%TYPE) IS
			SELECT
				NINUSHI || SYOCD AS SKU_CD		,
				NVL(FCKBN,' ') AS MANUFACTURER_CD,
				PL_T AS CASEPERPALLET_NR		,	-- パレット積み付け数
				PL_D AS PALLETLAYER_NR			,	-- パレット段数
				PL_M AS CASEPERLAYER_NR			,	-- パレット回し数
				PL_H AS STACKINGNUMBER_NR			-- 保管段数
			FROM
				TMP_SKU
			WHERE
				TRN_ID = TrnID
			GROUP BY
				NINUSHI			,
				SYOCD			,
				NVL(FCKBN,' ')	,
				PL_T			,
				PL_D			,
				PL_M			,
				PL_H
			ORDER BY
				NINUSHI,
				SYOCD;
		rowTmp curTmp%ROWTYPE;
		numCount			NUMBER;
		numID				NUMBER(10,0);
	BEGIN
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' START');
		OPEN curTmp(iTrnID);
		LOOP
			FETCH curTmp INTO rowTmp;
			EXIT WHEN curTmp%NOTFOUND;
			-- SKU
			SELECT
				COUNT(SKU_CD)
			INTO
				numCount
			FROM
				MA_SKUPALLET
			WHERE
				SKU_CD			= rowTmp.SKU_CD AND
				MANUFACTURER_CD	= rowTmp.MANUFACTURER_CD;
			IF numCount = 0 THEN
				SELECT
					NVL(MAX(ID),0)
				INTO
					numID
				FROM
					MA_SKUPALLET;
				numID := numID + 1;
				--登録
				INSERT INTO MA_SKUPALLET (
					ID					,
					SKU_CD				,
					MANUFACTURER_CD		,
					CASEPERPALLET_NR	,
					PALLETLAYER_NR		,
					CASEPERLAYER_NR		,
					PALLETCAPACITY_NR	,
					STACKINGNUMBER_NR	,
					--
					UPD_DT				,
					UPDUSER_CD			,
					UPDUSER_TX			,
					ADD_DT				,
					ADDUSER_CD			,
					ADDUSER_TX			,
					UPDPROGRAM_CD		,
					UPDCOUNTER_NR		,
					STRECORD_ID			
				) VALUES (
					numID						,
					rowTmp.SKU_CD				,
					rowTmp.MANUFACTURER_CD		,
					rowTmp.CASEPERPALLET_NR		,
					rowTmp.PALLETLAYER_NR		,
					rowTmp.CASEPERLAYER_NR		,
					rowTmp.PALLETLAYER_NR * rowTmp.CASEPERLAYER_NR,
					rowTmp.STACKINGNUMBER_NR	,
					--
					SYSDATE						,
					'BATCH'						,
					'BATCH'						,
					SYSDATE						,
					'BATCH'						,
					'BATCH'						,
					'BATCH'						,
					0							,
					0							
				);
			ELSE
				--更新
				UPDATE
					MA_SKUPALLET
				SET
					CASEPERPALLET_NR	= rowTmp.CASEPERPALLET_NR	,
					PALLETLAYER_NR		= rowTmp.PALLETLAYER_NR		,
					CASEPERLAYER_NR		= rowTmp.CASEPERLAYER_NR	,
					PALLETCAPACITY_NR	=rowTmp.PALLETLAYER_NR * rowTmp.CASEPERLAYER_NR,
					STACKINGNUMBER_NR	= rowTmp.STACKINGNUMBER_NR	,
					-- 管理項目
					UPD_DT				= SYSDATE					,
					UPDUSER_CD			= 'BATCH'					,
					UPDUSER_TX			= 'BATCH'					,
					UPDPROGRAM_CD		= 'BATCH'					,
					UPDCOUNTER_NR		= UPDCOUNTER_NR + 1
				WHERE
					SKU_CD			= rowTmp.SKU_CD			AND
					MANUFACTURER_CD	= rowTmp.MANUFACTURER_CD;
			END IF;
		END LOOP;
		CLOSE curTmp;
		PLOG.DEBUG(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || ' END');
	EXCEPTION
		WHEN OTHERS THEN
			IF SQLCODE > PS_DEFINE.MIN_APP_ERR_NUM AND SQLCODE < PS_DEFINE.MAX_APP_ERR_NUM THEN
				PLOG.WARN(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			ELSE
				PLOG.ERROR(logCtx, PACKAGE_NAME || '.' || FUNCTION_NAME || SQLERRM);
			END IF;
			RAISE;
	END UploadSkuPallet;
END PI_SKU;
/
SHOW ERRORS
