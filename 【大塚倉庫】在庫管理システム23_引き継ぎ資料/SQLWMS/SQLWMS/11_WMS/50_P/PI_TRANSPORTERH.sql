-- Copyright (C) Seaos Corporation 2013 All rights reserved
CREATE OR REPLACE PACKAGE PI_TRANSPORTER AS
	/************************************************************/
	/*	共通													*/
	/************************************************************/
	--パッケージ名
	PACKAGE_NAME CONSTANT VARCHAR2(40)	:= 'PI_TRANSPORTER';
	-- ログレベル
	logCtx PLOG.LOG_CTX := PLOG.init(pLEVEL => PS_DEFINE.LOG_LEVEL);
	
	/************************************************************/
	/* アップロード完了（配送業者）                             */
	/************************************************************/
	PROCEDURE EndUploadTransPorter(
		iTrnID						IN TMP_TRANSPORTER.TRN_ID%TYPE
	);
	
END PI_TRANSPORTER;
/
SHOW ERRORS
