-- phpMyAdmin SQL Dump
-- version 4.2.8
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2014 at 05:06 AM
-- Server version: 5.6.20
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `OTIDTR`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_login`
--

CREATE TABLE IF NOT EXISTS `mst_login` (
`USER_ID` int(10) NOT NULL COMMENT 'User id',
  `DRIVER_ID` varchar(64) NOT NULL COMMENT 'Email address',
  `PASSWORD` varchar(128) DEFAULT NULL COMMENT 'Password',
  `SALT` varchar(128) DEFAULT NULL COMMENT 'salt',
  `STATUS_FLG` int(1) NOT NULL DEFAULT '0' COMMENT 'Delete flag',
  `ADDUSER_DT` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of creation',
  `ADDUSER_TX` varchar(50) NOT NULL DEFAULT 'ADMIN' COMMENT 'Responsible'
) ENGINE=InnoDB AUTO_INCREMENT=100004 DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_login`
--
ALTER TABLE `mst_login`
 ADD PRIMARY KEY (`USER_ID`), ADD UNIQUE KEY `USER_ID` (`USER_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_login`
--
ALTER TABLE `mst_login`
MODIFY `USER_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'User id',AUTO_INCREMENT=100004;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
